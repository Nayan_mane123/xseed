
angular.module('services-config', [])

    .constant('configuration', {

      GET_METADATA_API_URL : '@@GET_METADATA_API_URL',
      API_KEY : '@@API_KEY',   
      CLIENT_ID: '@@CLIENT_ID',
      ACCESS_TOKEN_API_URL: '@@ACCESS_TOKEN_API_URL',
      TIMEOUT_UI_IN_SEC:'@@TIMEOUT_UI_IN_SEC'    

    });


