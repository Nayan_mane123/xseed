'use strict';

XSEED_APILOG_APP.config(AppConfig);
/**
 * @ngInject
 */

function AppConfig($compileProvider, configuration, $modalProvider, $asideProvider, blockUIConfig, $translateProvider, KeepaliveProvider, IdleProvider, $collapseProvider, localStorageServiceProvider) {

    $compileProvider.debugInfoEnabled = false;
    blockUIConfig.message = 'Processing...';
    blockUIConfig.autoBlock = true;
    blockUIConfig.templateUrl = 'views/templates/block-ui-overlay.html';
    angular.extend($modalProvider.defaults, {
        html: true
    });

    angular.extend($asideProvider.defaults, {
        container: 'body',
        html: true
    });
    localStorageServiceProvider.setPrefix('XSEED_APILOG_APP');
    $translateProvider.useSanitizeValueStrategy('sanitize');
    $translateProvider.useStaticFilesLoader({
       /* files: [{
            prefix: 'bower_components/angular-validation-ghiscoding/locales/validation/',
            suffix: '.json'
        },
         {*/
             prefix: 'i18n/',
             suffix: '.json'
        /*    }]*/

    });
    // Tell the module what language to use by default
    $translateProvider.preferredLanguage('en_US'); 


    IdleProvider.idle(configuration.TIMEOUT_UI_IN_SEC);
    IdleProvider.timeout(60);
    KeepaliveProvider.interval(10);



    angular.extend($collapseProvider.defaults, {
        animation: 'am-fade',
        startCollapsed: true
    });

};



XSEED_APILOG_APP.run(['Idle', function (Idle) {
    Idle.watch();
}]);
