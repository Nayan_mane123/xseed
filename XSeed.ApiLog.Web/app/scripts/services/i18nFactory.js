'use strict';


XSEED_APILOG_APP.factory('i18nFactory', i18nFactory);
/**
 * @ngInject
 */
function i18nFactory($translate) {
    var i18n = {};

    $translate('STATE_NAMES').then(function (translatedValue) {
        i18n.STATE_NAMES = translatedValue;
    });

    $translate('ERROR_TITLE').then(function (translatedValue) {
        i18n.ERROR_TITLE = translatedValue;
    });

    return i18n;
};
