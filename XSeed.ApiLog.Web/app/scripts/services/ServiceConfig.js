angular.module('services-config', [])
  .constant('configuration', {
      //Dev-
      GET_METADATA_API_URL: 'data/MetaData.json',

      XSEED_APP_NAME: 'XSEED APILOG APP',
      XSEED_APP_LOGO_PATH: 'images/logo-b.png',

      XSEED_LOGIN_API_URL: 'http://localhost:51655/token',

      XSEED_API_URL: 'http://localhost:51655/api/',
      XSEED_ODATA_API_URL: 'http://localhost:51655/odata/',

      /*Constants*/
      XSEED_ORG_IMAGE_PATH: 'http://10.33.2.176:81/API/Dev/Documents/Images/Organization/',
      XSEED_COMPANY_IMAGE_PATH: 'http://10.33.2.176:81/API/Dev/Documents/Images/Company/',
      XSEED_ORGUSER_IMAGE_PATH: 'http://10.33.2.176:81/API/Dev/Documents/Images/User/',
      XSEED_COMPANYCONTACT_IMAGE_PATH: 'http://10.33.2.176:81/API/Dev/Documents/Images/CompanyContact/',
      XSEED_CANDIDATE_IMAGE_PATH: 'http://10.33.2.176:81/API/Dev/Documents/Images/Candidate/',
      XSEED_NO_IMAGE_PATH: 'http://10.33.2.176:81/web/dev/app/images/noImage.jpg',
      /*Constants*/

      TIMEOUT_UI_IN_SEC: '1200'



      ////Staging-
      //GET_METADATA_API_URL: 'data/MetaData.json',

      //XSEED_LOGIN_API_URL: 'http://10.33.2.176:81/API/Staging/token',

      //XSEED_API_URL: 'http://10.33.2.176:81/API/Staging/api/',

      ///*Constants*/
      //XSEED_ORG_IMAGE_PATH: 'http://10.33.2.176:81/API/Staging/Documents/Images/Organization/',
      //XSEED_COMPANY_IMAGE_PATH: 'http://10.33.2.176:81/API/Staging/Documents/Images/Company/',
      //XSEED_ORGUSER_IMAGE_PATH: 'http://10.33.2.176:81/API/Staging/Documents/Images/User/',
      //XSEED_COMPANYCONTACT_IMAGE_PATH: 'http://10.33.2.176:81/API/Staging/Documents/Images/CompanyContact/',
      //XSEED_CANDIDATE_IMAGE_PATH: 'http://10.33.2.176:81/API/Staging/Documents/Images/Candidate/',
      //XSEED_NO_IMAGE_PATH: 'http://10.33.2.176:81/web/Staging/app/images/noImage.jpg',
      ///*Constants*/

      //TIMEOUT_UI_IN_SEC: '1200'

       

      ////Local-
      //GET_METADATA_API_URL: 'data/MetaData.json',     

      //XSEED_LOGIN_API_URL: 'http://localhost:51655/token',

      //XSEED_API_URL: 'http://localhost:51655/api/',

      ///*Constants*/
      //XSEED_ORG_IMAGE_PATH: 'http://localhost:51655/Documents/Images/Organization/',
      //XSEED_COMPANY_IMAGE_PATH: 'http://localhost:51655/Documents/Images/Company/',
      //      XSEED_ORGUSER_IMAGE_PATH: 'http://localhost:51655/Documents/Images/User/',
      //      XSEED_COMPANYCONTACT_IMAGE_PATH: 'http://localhost:51655/Documents/Images/CompanyContact/',
      ///*Constants*/

      //TIMEOUT_UI_IN_SEC: '1200'








  })
    .run(function ($rootScope, configuration) {
        $rootScope.configuration = configuration;
    });