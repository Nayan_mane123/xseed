'use strict';

/**
 * @ngdoc overview
 * @name xseed-app
 * @description
 * # xseed-apilog-app
 *
 * Main module of the application.
 */


var XSEED_APILOG_APP = angular.module('xseed-apilog-app', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ipCookie',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'mgcrea.ngStrap',
    'blockUI',
    'pascalprecht.translate',
    'services-config',
    'ui.select',
    'ui.select2',
    'xseed-app-routing',
    'xseed-app-wizard',
    'vAccordion',
    'green.inputmask4angular',
    'ngTable',
    'ngTableExport',
    'ngIdle',
    'selector',
    'ngclipboard',
    'sun.scrollable',
    'toggle-switch',
    'focus-if',
    'LocalStorageModule',
    'ui.bootstrap',
    'ghiscoding.validation',
    'pascalprecht.translate',
    'ui.mask',
    '720kb.datepicker'
]);
