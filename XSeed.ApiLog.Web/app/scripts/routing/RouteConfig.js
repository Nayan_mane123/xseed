/// <reference path="../controllers/companyController.js" />
/// <reference path="../controllers/companyController.js" />

//'use strict';

XSEED_APILOG_APP.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.caseInsensitiveMatch = true;
        $routeProvider
            .when('/', {
                //action: "landing"
                action: "standard.home",
                pageTitle: "API Logs"
            })
            .when('/home', {
                action: "standard.home",
                pageTitle: "API Logs"
            })
            .when('/apiLogDetail', {
                action: "standard.apiLogDetail",
                pageTitle: "API Log Detail"
            })
            .when('/login', {
                action: "landing.login"
            })
            .when('/register', {
                action: "landing.register"
            })
            .when('/candidateRegistration', {
                action: "landing.candidateRegistration"
            })
            .when('/changePassword', {
                action: "standard.changePassword",
                pageTitle: "Change Password"
            })
            .when('/forgotPassword', {
                action: "landing.forgotPassword",
            })
            .when('/resetPassword', {
                action: "landing.resetPassword",
            })
            .when('/createCandidate', {
                action: "standard.candidate.createCandidate",
                pageTitle: "Add Candidate"
            })
            .when('/candidateDetail', {
                action: "standard.candidate.candidateDetail",
                pageTitle: "Candidate Details"
            })
            .when('/editCandidate', {
                action: "standard.candidate.createCandidate",
                pageTitle: "Edit Candidate"
            })
            .when('/switchView', {
                action: "standard.switchView"
            })
            .otherwise({
                redirectTo: '/'
                //action: "landing.login"

            });
    }
]);
