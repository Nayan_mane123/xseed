'use strict';

XSEED_APILOG_APP.directive('sideNavigation', sideNavigation);


function sideNavigation() {
    return {
        link: function(scope, element) {
            var controller = element.parent().controller();
            element.find('a').on('click', function(e) {
                var $this = angular.element(this),
                    links = $this.parents('li'),
                    parentLink = $this.closest('li'),
                    otherLinks = angular.element('.sidebar-panel nav li').not(links),
                    subMenu = $this.next();
                if (!subMenu.hasClass('sub-menu')) {
                    controller.menu = false;
                    return;
                }
                otherLinks.removeClass('open');
                if (subMenu.is('ul') && (subMenu.height() === 0)) {
                    parentLink.addClass('open');
                } else if (subMenu.is('ul') && (subMenu.height() !== 0)) {
                    parentLink.removeClass('open');
                }
                if (subMenu.is('ul')) {
                    return false;
                }
                e.stopPropagation();
                return true;
            });
            element.find('> li > .sub-menu').each(function() {
                if (angular.element(this).find('ul.sub-menu').length > 0) {
                    angular.element(this).addClass('multi-level');
                }
            });
        }
    };
}

XSEED_APILOG_APP.directive('progressBar', progressBar);

function progressBar ($timeout) {
    return {
        restrict: "EA",
        scope: {
            total: '=total',
            complete: '=complete',
            barClass: '@barClass',
            completedClass: '=?'
        },
        transclude: true,
        link: function (scope, elem, attrs) {

            scope.label = attrs.label;
            scope.completeLabel = attrs.completeLabel;
            scope.showPercent = (attrs.showPercent) || false;
            scope.completedClass = (scope.completedClass) || 'progress-bar-danger';

            scope.$watch('complete', function () {

                //change style at 100%
                var progress = scope.complete/scope.total;
                if (progress >= 1) {
                    $(elem).find('.progress-bar').addClass(scope.completedClass);
                }
                else if (progress < 1) {
                    $(elem).find('.progress-bar').removeClass(scope.completedClass);
                }

            });

        },
        template:
            "<div class='progress'>"+
            "   <div class='progress-bar {{barClass}}' title='{{complete/total * 100 | number:0 }}%' style='width:{{complete/total * 100}}%;'>{{showPercent ? (complete/total*100) : complete | number:0}} {{completeLabel}}</div>" +
            "</div>"
    };
};

XSEED_APILOG_APP.directive('ngEnter', ngEnter);

function ngEnter () {

    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
};

XSEED_APILOG_APP.directive('icheck', icheck);

function icheck($timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function($scope, element, $attrs, ngModel) {
            return $timeout(function() {
                var value;
                value = $attrs['value'];

                $scope.$watch($attrs['ngModel'], function(newValue){
                    $(element).iCheck('update');
                })

                return $(element).iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    radioClass: 'iradio_flat-blue'

                }).on('ifChanged', function(event) {
                    if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
                        $scope.$apply(function() {
                            return ngModel.$setViewValue(event.target.checked);
                        });
                    }
                    if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
                        return $scope.$apply(function() {
                            return ngModel.$setViewValue(value);
                        });
                    }
                });
            });
        }
    };
}

//Number Only
XSEED_APILOG_APP.directive('numberOnly', function () {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {
            if (!ngModel) return;

            ngModel.$parsers.unshift(function (inputValue) {
                if (!inputValue) return;
                else {
                    var digits = inputValue.split('').filter(function (s) {
                        return (!isNaN(s) && s != ' ');
                    }).join('');
                    ngModel.$viewValue = digits;
                    ngModel.$render();
                    return digits;
                }
            });
        }
    };
});

XSEED_APILOG_APP.directive('tabs', tabs);

function tabs() {
    return {
        restrict: 'E',
        transclude: true,
        scope: {},
        controller: [ "$scope", function($scope) {
            var panes = $scope.panes = [];

            $scope.select = function(pane) {
                angular.forEach(panes, function(pane) {
                    pane.selected = false;
                });
                pane.selected = true;
            }

            this.addPane = function(pane) {
                if (panes.length == 0) $scope.select(pane);
                panes.push(pane);
            }
        }],
        template:
            '<div class="tabbable">' +
            '<ul class="nav nav-tabs">' +
            '<li ng-repeat="pane in panes" ng-class="{active:pane.selected}">'+
            '<a href="" ng-click="select(pane)">{{pane.title}}</a>' +
            '</li>' +
            '</ul>' +
            '<div class="tab-content" ng-transclude></div>' +
            '</div>',
        replace: true
    };
};

XSEED_APILOG_APP.directive('pane', pane);

function pane() {
    return {
        require: '^tabs',
        restrict: 'E',
        transclude: true,
        scope: { title: '@' },
        link: function(scope, element, attrs, tabsCtrl) {
            tabsCtrl.addPane(scope);
        },
        template:
            '<div class="tab-pane" ng-class="{active: selected}" ng-transclude>' +
            '</div>',
        replace: true
    };
};

XSEED_APILOG_APP.directive('sidenavbar',function() {

    return {
        restrict: 'A',
        link: function(scope, element) {

            var dnl                = $(".dash-navbar-left"),
                dnlBtnToggle       = element,
                dnlBtnCollapse     = $(".dnl-btn-collapse"),
                contentWrap        = $(".content-wrap"),
                contentWrapEffect  = contentWrap.data("effect"),
                windowHeight       = $(window).height() - 61,
                windowWidth        = $(window).width() < 767;


            element.click( function() {
                if( dnl.hasClass("dnl-hide") ) {
                    dnlShow();
                } else {
                    dnlHide();
                }
            });


            // Functions
            function cwShowOverflow() {
                if ( windowWidth ) {
                    contentWrap.css({
                        height : windowHeight ,
                        overflow : 'hidden'
                    });
                    $( 'html, body' ).css( 'overflow', 'hidden' );
                }
            }

            function cwHideOverflow() {
                if ( windowWidth ) {
                    contentWrap.css({
                        height : 'auto' ,
                        overflow : 'auto'
                    });
                    $( 'html, body' ).css( 'overflow', 'auto' );
                }
            }

            function dnlShow() {
                dnl.addClass("dnl-show").removeClass("dnl-hide");
                contentWrap.addClass(contentWrapEffect);
                cwShowOverflow();
                dnlBtnToggle.find("span").removeClass("fa-bars").addClass("fa-arrow-left");
            }

            function dnlHide() {
                dnl.removeClass("dnl-show").addClass("dnl-hide");
                contentWrap.removeClass(contentWrapEffect);
                cwHideOverflow();
                dnlBtnToggle.find("span").removeClass("fa-arrow-left").addClass("fa-bars");
            }

            // Toggle the edge navbar left
            dnl.addClass("dnl-hide");
            dnlBtnCollapse.click( function(e) {
                e.preventDefault();
                if( dnl.hasClass("dnl-collapsed") ) {
                    dnl.removeClass("dnl-collapsed");
                    contentWrap.removeClass("dnl-collapsed");
                    $(this).find(".dnl-link-icon").removeClass("fa-arrow-right").addClass("fa-arrow-left");
                } else {
                    dnl.addClass("dnl-collapsed");
                    contentWrap.addClass("dnl-collapsed");
                    $(this).find(".dnl-link-icon").removeClass("fa-arrow-left").addClass("fa-arrow-right");
                }
            });

            // Close left navbar when top navbar opens
            $( '.navbar-toggle' ).click( function() {
                if ( $( this ).hasClass( 'collapsed' ) ) {
                    dnlHide();
                }
            });

            // Close top navbar when left navbar opens
            dnlBtnToggle.click( function() {
                $( '.navbar-toggle' ).addClass( 'collapsed' );
                $( '.navbar-collapse' ).removeClass( 'in' );
            });

            // Code credit: https://tr.im/CZzf4
            function isMobile() {
                try { document.createEvent("TouchEvent"); return true; }
                catch(e){ return false; }
            }

            // Swipe the navbar
            if ( isMobile() == true ) {
                $(window).swipe( {
                    swipeRight:function() {
                        dnlShow();
                        $( '.navbar-collapse' ).removeClass( 'in' );
                    },
                    swipeLeft:function() {
                        dnlHide();
                    },
                    threshold: 75
                });
            }

            // Collapse navbar on content click
            $( '.content-wrap' ).click( function() {
                dnlHide();
            });

        }
    };



})

XSEED_APILOG_APP.directive('matchModel', matchModel);

function matchModel () {
    return {
        require: 'ngModel',
        restrict: 'A',
        scope: {
            matchModel: '='
        },
        link: function (scope, elem, attrs, ctrl) {
            scope.$watch(function () {
                var modelValue = ctrl.$modelValue || ctrl.$$invalidModelValue;
                var matchModel = scope.matchModel;
                var viewModel = ctrl.$viewValue;

                if(matchModel) {
                    matchModel = matchModel.toLowerCase();
                }
                if(viewModel) {
                    viewModel = viewModel.toLowerCase();
                }
                if(modelValue) {
                    modelValue = modelValue.toLowerCase();
                }
                return (ctrl.$pristine && angular.isUndefined(modelValue)) || matchModel === modelValue || matchModel === viewModel;
            }, function (currentValue) {
                ctrl.$setValidity('matchModel', currentValue);
            });
        }
    };
}

XSEED_APILOG_APP.directive('blockClipboard', blockClipboard);

function blockClipboard () {
    return {
        require: '?ngModel',
        link: function (scope, elm, attrs, ngModel) {
            if (!ngModel) {
                return;
            }
            ;


            elm.on('cut copy paste', function (e) {
                e.preventDefault();
            });
        }
    }
}

XSEED_APILOG_APP.directive('sliderToggle', function() {
    return {
        restrict: 'AE',
        link: function(scope, element, attrs) {
            var target = angular.element($("[slider]"))[0];

            var slideT = scope.$eval(attrs.sliderToggle);

            scope.$watch(attrs.sliderToggle, function(v) {
                var content = target.querySelector('#slideable_content');
                //console.log('Slide Toggle -->'  +v);
                if(angular.equals(false,v)) {
                    target.style.height = '0px';
                }
                else {
                    content.style.border = '1px solid rgba(0,0,0,0)';
                    var y = content.clientHeight;
                    content.style.border = 0;
                    target.style.height = y + 'px';
                }
            });

        }
    }
});

XSEED_APILOG_APP.directive('slider', function () {
    return {
        restrict:'A',
        compile: function (element, attr) {
            // wrap tag
            var contents = element.html();
            element.html('<div id="slideable_content" style="margin:0 !important; padding:0 !important;clear:both!important;" >' + contents + '</div>');

            return function postLink(scope, element, attrs) {
                // default properties
                attrs.duration = (!attrs.duration) ? '1s' : attrs.duration;
                attrs.easing = (!attrs.easing) ? 'ease-in-out' : attrs.easing;
                element.css({
                    'overflow': 'hidden',
                    'height': '0px',
                    'transitionProperty': 'height',
                    'transitionDuration': attrs.duration,
                    'transitionTimingFunction': attrs.easing
                });
            };
        }
    };
});

XSEED_APILOG_APP.directive('validationNoAndWord', ['$log',
    function($log) {

        return {

            restrict: 'A',
            require: 'ngModel',
            link: function(scope, ele, attrs, ctrl){
              ele.bind('blur', function () {
                ctrl.$validators.validationNoAndWord = function(modelValue, viewValue) {
                  return validateAnd(viewValue);
                }
              });

              var validateAnd = function (val){

                var regex =/\band\b/i;
                return !regex.test(val);
              };

            }
        }
    }
]);

XSEED_APILOG_APP.directive('validationWords', ['$log',
    function($log) {

        return {

            restrict: 'A',
            require: 'ngModel',
            link: function(scope, ele, attrs, ctrl){
              ele.bind('blur', function () {
                ctrl.$validators.validationWords = function(modelValue, viewValue) {
                  return validateAnd(viewValue);
                }
              });

              var validateAnd = function (val){
                var valid =  true;
                if(val) {
                  var nameLength = val.match(/\S+/g).length;
                  if( nameLength > 3)
                  {
                    valid =  false;
                  }
                }
                return valid;
              };

            }
        }
    }
]);

XSEED_APILOG_APP.directive('autoTabTo', [function () {
  return {
    restrict: "A",
    link: function (scope, el, attrs) {
      el.bind('keyup', function(e) {
        if (this.value.length === this.maxLength) {
          var element = document.getElementById(attrs.autoTabTo);
          if (element)
            element.focus();
        }
      });
    }
  }
}]);

XSEED_APILOG_APP.directive('autofocus', ['$timeout', function($timeout) {
  return {
    restrict: 'A',
    link : function($scope, $element) {
      $timeout(function() {
        $element[0].focus();
      });
    }
  }
}]);

XSEED_APILOG_APP.directive('uiBlurFocus', function () {
    return {
        restrict: 'AE',
        require: 'ngModel',
        link: function (scope, elem, attrs) {
            elem.bind('blur', function () {
                var field = scope.$eval(attrs.uiBlurFocus); //ctrl.blurField;
                field.$onblurInvalid = field.$invalid;
                //console.log(field.$onblurInvalid);
            });
            elem.bind('focus', function () {
                var field = scope.$eval(attrs.uiBlurFocus); //ctrl.focusField;
                field.$onblurInvalid = false;
                //console.log(field.$onblurInvalid);
            });
        }};
});

function menuPanel($window) {
    return {
        scope: {
            show: '=',
        },
        link: function(scope, element, attr) {
            scope.$watch('show', function(newVal, oldVal) {
                var win = angular.element($window);
                if (win.width() < 991 || angular.element('.app').hasClass('offcanvas')) {
                    if (newVal && !oldVal) {
                        angular.element('.app').addClass('offscreen move-right');
                    } else {
                        angular.element('.app').removeClass('offscreen move-left move-right');
                    }
                }
            });
        }
    };
}

XSEED_APILOG_APP.directive('menuPanel', menuPanel);



XSEED_APILOG_APP.directive("ngFileModel", [function () {
    return {
        scope: {
            ngFileModel: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.ngFileModel = {
                            lastModified: changeEvent.target.files[0].lastModified,
                            lastModifiedDate: changeEvent.target.files[0].lastModifiedDate,
                            name: changeEvent.target.files[0].name,
                            size: changeEvent.target.files[0].size,
                            type: changeEvent.target.files[0].type,
                            data: loadEvent.target.result
                        };
                    });
                }
                reader.readAsDataURL(changeEvent.target.files[0]);
            });
        }
    }
}]);