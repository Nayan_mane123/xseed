'use strict';

XSEED_APILOG_APP.controller('StandardCtrl', StandardCtrl);

/**
 * @ngInject
 */
function StandardCtrl($rootScope, $scope, RequestContext, $log, ExceptionHandler, i18nFactory, localStorageService, $window, $location, blockUI, XSeedApiFactory, ModalFactory, XSeedAlert, $filter, ValidationService, $timeout) {

    $log.info("StandardCtrl");

    $scope.changePassword = function (changePasswordForm) {

        if (new ValidationService().checkFormValidity(changePasswordForm)) {
            blockUI.start();

            $scope.changePasswordModel.Id = $rootScope.userDetails.UserId;

            var promise = XSeedApiFactory.changePassword($scope.changePasswordModel);
            promise.then(
                function (response) {
                    blockUI.stop();

                    XSeedAlert.swal({
                        title: 'Success!',
                        text: 'Your password has been changed successfully!',
                        type: "success",
                        customClass: "xseed-error-alert",
                        //showCancelButton: true,
                        //confirmButtonText: "OK",
                        //closeOnConfirm: true
                    }).then(function () {
                            $scope.changePasswordModel = {}
                            $timeout(function () {
                                $location.path('home');
                            }, 0);
                        }, function (dismiss) {
                            //console.log('dismiss: ' + dismiss);
                            // dismiss can be 'cancel', 'overlay', 'close', 'timer'
                            //if (dismiss == 'cancel'){alert('Close fired');}
                        });
                },
                function (httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerPopup('Warning', httpError.data.Message, 'Warning!', 'error');
                });
        };
    }

    // Get the render context local to this controller (and relevant params).
    var renderContext = RequestContext.getRenderContext("standard");
    // The subview indicates which view is going to be rendered on the page.
    $scope.subview = renderContext.getNextSection();
    $scope.$on(
        "requestContextChanged",
        function () {
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {
                return;
            }
            // Update the view that is being rendered.
            $scope.subview = renderContext.getNextSection();
        }
    );

};
