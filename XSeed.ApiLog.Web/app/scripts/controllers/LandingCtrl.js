'use strict';

XSEED_APILOG_APP.controller('LandingCtrl', LandingCtrl);

/**
 * @ngInject
 */
function LandingCtrl($scope, $rootScope, $route, $routeParams, RequestContext, localStorageService, $log, ExceptionHandler, $location, blockUI, XSeedApiFactory, ModalFactory, XSeedAlert, $filter, ValidationService, configuration, $timeout, $cookies) {
    $log.info("LandingCtrl");

    // Get the render context local to this controller (and relevant params).
    var renderContext = RequestContext.getRenderContext("landing");
    // The subview indicates which view is going to be rendered on the page.
    $scope.subview = renderContext.getNextSection();
    $scope.$on(
        "requestContextChanged",
        function () {
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {
                return;
            }
            // Update the view that is being rendered.
            $scope.subview = renderContext.getNextSection();
        }
    );

    $scope.login = function (loginForm) {
        $location.path('home');
    };


};
