'use strict';

XSEED_APILOG_APP.controller('ApplicationCtrl', ApplicationCtrl);

/**
 * @ngInject
 */
function ApplicationCtrl($scope, $rootScope, $route, $routeParams, RequestContext, $timeout, $q, $location, $window, configuration, i18nFactory, ExceptionHandler, EventBus, $log, _, ipCookie, blockUI, ModalFactory, XSeedAlert, localStorageService, $uibModal, XSeedApiFactory, ngTableParams, $filter, ValidationService) {
    $log.info("ApplicationCtrl");

    $rootScope.token = {};
    $rootScope.userDetails = {};
    $rootScope.metadata = {};
    $scope.filter = {};
    $scope.flag = { requirementLookupLoaded: false, candidateLookupLoaded: false };
    $scope.requirementSearchData = "";
    $scope.requirementModel = {};

    var vm = this;
    vm.menu = false;
    vm.toggleMenu = function () {
        vm.menu = !vm.menu;
    };

    XSeedApiFactory.setConfiguration(configuration);

    $scope.$watch('location.search()', function () {
        init();
    }, true);


    //Initialization Function
    function init() {
        getMetaData();
        if ($scope.isUndefinedOrNull($scope.requirementModel._id) || $scope.isEmpty($scope.requirementModel._id)) {
            $location.path('home');
        }
        $scope.pageSizeShow = 10;
        $scope.pageNumberShow = 1;
    };

    function getMetaData() {
        blockUI.start();
        var promise = XSeedApiFactory.getMetaData();
        promise.then(
            function (response) {
                blockUI.stop();
                $rootScope.metadata = response[0].data;
            },
            function (httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('AccessTokenRequest', null, httpError);
            });
    };

    /*************************** API Log *************************************/

    $scope.apiLogList = function () {
        blockUI.start();

        var firstPageFlag = 0;
        var promise = XSeedApiFactory.apiLogList($scope.pageSizeShow, $scope.pageNumberShow);
        promise.then(
          function (response) {
              $scope.apiLogList = response[0].data.ApiLog;
              $scope.totalCount = response[0].data.TotalCount;
              $scope.totalPages = response[0].data.TotalPages;
              firstPageFlag = 1;
              populateRequirementList($scope.apiLogList, $scope.totalCount, $scope.totalPages, firstPageFlag);
              blockUI.stop();
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
          });
    };

    function populateRequirementList(apiLogList, totalCount, totalPages, pageFlag) {
        if (angular.isDefined($scope.tblApiLogList)) {
            $scope.tblApiLogList.reload();
        }
        else {
            $scope.tblApiLogList = new ngTableParams({
                page: 1,
                count: 10,
                sorting: {}
            }, {
                counts: [],
                total: totalCount,
                getData: function ($defer, params) {

                    $scope.pageSizeShow = params.count();
                    $scope.pageNumberShow = params.page();

                    if (pageFlag != 1) {
                        var promise = XSeedApiFactory.apiLogList($scope.pageSizeShow, $scope.pageNumberShow);
                        promise.then(
                          function (response) {
                              $scope.apiLogList = response[0].data.ApiLog;
                              $scope.totalCount = response[0].data.TotalCount;
                              $scope.totalPages = response[0].data.TotalPages;
                              blockUI.stop();

                          },
                          function (httpError) {
                              blockUI.stop();
                              ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                          });

                    }
                    pageFlag = 0;
                    params.total($scope.totalCount);
                    $defer.resolve($scope.apiLogList);
                }
                , $scope: $scope
            });
        }
    }

    $scope.apilogDetailView = function (logId) {
        blockUI.start();
        //var organizationId = $rootScope.userDetails.OrganizationId;
        var promise = XSeedApiFactory.getApiLogDetail(logId);
        promise.then(
          function (response) {
              //for Request Headers 
              $scope.apiLogDetailModel = response[0].data;
              if ($scope.apiLogDetailModel.RequestHeaders != null) {
                  $scope.apiLogDetailModel.RequestHeaders = $scope.apiLogDetailModel.RequestHeaders.split(",");
              }
              //for Request Content Body
              if ($scope.apiLogDetailModel.RequestContentBody != null) {
                  $scope.apiLogDetailModel.RequestContentBody = $scope.apiLogDetailModel.RequestContentBody.split(",");
              }
              //for Response Content Body 
              if ($scope.apiLogDetailModel.ResponseContentBody != null) {
                  $scope.apiLogDetailModel.ResponseContentBody = $scope.apiLogDetailModel.ResponseContentBody.split(",");
              }
              blockUI.stop();
              $location.path('apiLogDetail');
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
          });
    };
    /*************************** API Log *************************************/

    $scope.getStateListByCountry = function (countryId) {
        console.log("countryId=> " + countryId);
        var promise = XSeedApiFactory.getStateListByCountry(countryId);
        promise.then(
            function (response) {
                $scope.stateList = response[0].data;

            },
            function (httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
            });
    };

    $scope.getCityListByState = function (stateId) {
        var promise = XSeedApiFactory.getCityListByState(stateId);
        promise.then(
            function (response) {
                $scope.cityList = response[0].data;
            },
            function (httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
            });
    };

    $scope.isUndefinedOrNull = function (val) {
        return angular.isUndefined(val) || val === null
    }

    $scope.isEmpty = function (obj) {
        //for (var prop in obj) {
        //    if (obj.hasOwnProperty(prop))
        //        return false;
        //}
        //return true;

        if (obj.length === 0) {
            return true;
        }
        else {
            return false;
        }
    }

    $scope.onBlurValidFunc = function (val) {
        val.$onblurInvalid = val.$invalid;
    };

    $scope.onFocusFunc = function (val) {
        val.$onblurInvalid = false;
    };

    $scope.sessionTimeout = function (location) {
        $window.location.href = location;
    };

    function isRouteRedirect(route) {
        return (!route.current.action);
    };

    // Fires after Exception handler call
    $scope.$on('handleError', function () {
        var exceptionMessage = ExceptionHandler.ExceptionMessage;
        $scope.locationPath = exceptionMessage.location;
        $scope.ErrorMessage = exceptionMessage.message;
        showErrorModal();
    });

    // Fires after Exception handler call
    $scope.$on('handlePopup', function () {
        $scope.popupTitle = ExceptionHandler.ExceptionMessage.title;
        $scope.ErrorMessage = ExceptionHandler.ExceptionMessage.message;
        $scope.popupType = ExceptionHandler.ExceptionMessage.type;
        showPopup();
    });

    // Fires after Exception handler call
    $scope.$on('handleAccountException', function () {
        var exceptionMessage = ExceptionHandler.ExceptionMessage;
        $scope.sessionTimeout(exceptionMessage.location);
    });

    function showPopup() {
        //errorModal.$promise.then(errorModal.show);
        XSeedAlert.swal(
            {
                title: $scope.popupTitle,
                text: $scope.ErrorMessage,
                type: $scope.popupType,
                customClass: 'xseed-error-alert',
                allowOutsideClick: false,
                allowEscapeKey: false
            });
    };

    function showErrorModal() {
        //errorModal.$promise.then(errorModal.show);
        XSeedAlert.swal(
            {
                title: i18nFactory.ERROR_TITLE,
                text: $scope.ErrorMessage,
                type: 'error',
                customClass: 'xseed-error-alert',
                allowOutsideClick: false,
                allowEscapeKey: false
            });
    };

    $scope.showNoty = function (msg) {
        var type = 'success';
        var position = 'topRight';
        noty({
            theme: 'app-noty',
            text: msg,
            type: type,
            timeout: 3000,
            layout: position,
            closeWith: ['button', 'click'],
            animation: {
                open: 'animated fadeInDown', // Animate.css class names
                close: 'animated fadeOutUp', // Animate.css class names
            }
        });
    };

    $scope.hideErrorModal = function () {
        $location.$$search = {};
    };

    $scope.UserIdlestarted = false;
    var timeoutWarningModal = ModalFactory.timeoutWarningModal($scope);
    var timeoutModal = ModalFactory.timeoutModal($scope);

    function closeTimeoutModals() {
        timeoutWarningModal.$promise.then(timeoutWarningModal.hide);
        timeoutModal.$promise.then(timeoutModal.hide);
    };

    $scope.$on('IdleStart', function () {
        closeTimeoutModals();
        timeoutWarningModal.$promise.then(timeoutWarningModal.show);
    });

    $scope.$on('IdleEnd', function () {
        closeTimeoutModals();
        $log.warn('Idle time ending');
    });

    $scope.$on('IdleTimeout', function () {
        closeTimeoutModals();
        ModalFactory.hideAllOpenedModals();
        $scope.sessionTimeout("sessionTimeout.html");
        timeoutModal.$promise.then(timeoutModal.show);
    });


    //---------------------------------------------------------------------------//

    // Get the render context local to this controller (and relevant params).

    var renderContext = RequestContext.getRenderContext();

    // The subview indicates which view is going to be rendered on the page.

    $scope.subview = renderContext.getNextSection();

    $scope.$on("requestContextChanged",

        function () {

            $log.info('requestContextChanged=> ');
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {

                $log.info('requestContextChanged=> Return ');
                return;
            }
            // Update the view that is being rendered.
            $log.info('requestContextChanged=> subview ');
            $scope.subview = renderContext.getNextSection();
        }
    );

    // Listen for route changes so that we can trigger request-context change events.
    $scope.$on("$routeChangeSuccess", function (event) {
        // If this is a redirect directive, then there's no taction to be taken.

        if (isRouteRedirect($route)) {
            return;
        }

        $scope.currentLocation = $route.current.action;

        $log.info('action=> ' + $route.current.action);
        $log.info('title ' + $route.current.pageTitle);
        $scope.pageTitle = $route.current.pageTitle;
        // Update the current request action change.
        $log.info("$routeParams=>" + $routeParams);
        RequestContext.setContext($route.current.action, $routeParams);

        // Announce the change in render conditions.
        $scope.$broadcast("requestContextChanged", RequestContext);

    });

    //---------------------------------------------------------------------------//
};
