'use strict';

XSEED_CANDIDATE_APP.filter('titleCase', function () {
    return function (val) {
        if (!val)
            return val;
        else {
            return val.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
        }
    };
});

XSEED_CANDIDATE_APP.filter('phoneNumber', function () {
    return function (val) {
        if (!val)
            return val;
        else {
            var formattedVal = val.substr(0, 3) + '-' + val.substr(3, 3) + '-' + val.substr(6, 4)
            return formattedVal;
        }
    };
});

XSEED_CANDIDATE_APP.filter('getCountryName', function () {
    return function (id, countryList) { 
        if (!id)
            return id;
        else {
            var selectedCountry = _.filter(countryList, function (country) { 
                return country.Id === id;
            });
           
            return selectedCountry[0] == undefined ? "" : selectedCountry[0].Name;
        }
    };
});


XSEED_CANDIDATE_APP.filter('addHttpOrHttps', function () {
    return function (link) { 
        if (!link)
            return link;
        var result;
        var startingUrl = "http://";
        var httpsStartingUrl = "https://";
        if (link.indexOf(startingUrl)==0 || link.indexOf(httpsStartingUrl)==0) {
            result = link;
        }
        else {
            result = startingUrl + link;
        }
        return result;
    };
}); 


XSEED_CANDIDATE_APP.filter('range', function () {
    return function (n) {
        var res = [];
        for (var i = 1; i <= n; i++) {
            res.push(i);
        }
        return res;
    };
});