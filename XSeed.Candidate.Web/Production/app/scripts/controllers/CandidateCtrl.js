﻿'use strict';

XSEED_CANDIDATE_APP.controller('CandidateCtrl', CandidateCtrl);

/**
 * @ngInject
 */
function CandidateCtrl($scope, $rootScope, $route, $routeParams, RequestContext, $timeout, $q, $location, $window, configuration, i18nFactory, ExceptionHandler, EventBus, $log, _, ipCookie, blockUI, ModalFactory, XSeedAlert, localStorageService, $uibModal, XSeedApiFactory, ngTableParams, $filter, ValidationService, $sce) {

    $log.info("CandidateCtrl");
    $scope.dataUrl;
    $scope.showVideoRecorder = false;

    $scope.flagVideoMaxSize = false;
    $scope.flagVideoInvalid = false;

    init();
    function init() {
        $scope.candidateModel = {};

        if ($scope.isUndefinedOrNull($scope.candidateModel._id) || $scope.isEmpty($scope.candidateModel._id)) {
            $location.path('candidateProfile');
        }
        var date = new Date();
        $scope.maxDate = ("0" + (date.getMonth() + 1).toString()).substr(-2) + "/" + ("0" + date.getDate().toString()).substr(-2) + "/" + (date.getFullYear().toString());
        $scope.minDate = ("0" + (date.getMonth() + 1).toString()).substr(-2) + "/" + ("0" + date.getDate().toString()).substr(-2) + "/" + (date.getFullYear().toString());
        $scope.availabilityDateLimit = ("0" + (date.getMonth() + 1).toString()).substr(-2) + "/" + ("0" + date.getDate().toString()).substr(-2) + "/" + ((date.getFullYear() + 1).toString());
        $scope.passportVisaValidDate = ("0" + (date.getMonth() + 1).toString()).substr(-2) + "/" + ("0" + date.getDate().toString()).substr(-2) + "/" + ((date.getFullYear() + 10).toString());
    };

    $scope.createFunctionDegree = function (input) {
        // format the option and return it
        return {
            value: $scope.lookup.degreeListData.length,
            label: input
        };
    };

    $scope.getCandidateDetail = function () {
        $scope.WorkExperience = [];
        var EmployementHistoryList = [];
        var EducationalList = [];
        blockUI.start();
        var promise = XSeedApiFactory.getCandidateDetail($rootScope.userDetails._id);
        promise.then(
          function (response) {
              $scope.candidateModel = response[0].data;

              if ($scope.candidateModel.CountryId) {
                  $scope.getStateListByCountry($scope.candidateModel.CountryId);
              }

              if ($scope.candidateModel.StateId) {
                  $scope.getCityListByState($scope.candidateModel.StateId);
              }

              $scope.candidateModel.BirthDate = $filter('date')($scope.candidateModel.BirthDate, "MM/dd/yyyy");
              $scope.candidateModel.PassportValidUpto = $filter('date')($scope.candidateModel.PassportValidUpto, "MM/dd/yyyy");
              $scope.candidateModel.VisaValidUpto = $filter('date')($scope.candidateModel.VisaValidUpto, "MM/dd/yyyy");
              if ($scope.candidateModel.ResumeList != null && $scope.candidateModel.ResumeList.length > 0) {
                  $scope.candidateModel.Resume = $scope.candidateModel.ResumeList[$scope.candidateModel.ResumeList.length - 1];
                  $scope.candidateModel.ResumeContent = $scope.candidateModel.ResumeList[$scope.candidateModel.ResumeList.length - 1].data.includes(",") ? $scope.candidateModel.ResumeList[$scope.candidateModel.ResumeList.length - 1].data : ("data:;base64," + $scope.candidateModel.ResumeList[$scope.candidateModel.ResumeList.length - 1].data);
              }

              if ($scope.candidateModel.Educations) {
                  for (var i = 0; i < $scope.candidateModel.Educations.length; i++) {
                      if ($scope.candidateModel.Educations[i].CountryId) {
                          $scope.setStateListByCountry($scope.candidateModel.Educations[i]);
                      }

                      if ($scope.candidateModel.Educations[i].StateId) {
                          $scope.setCityListByState($scope.candidateModel.Educations[i]);
                      }
                      EducationalList.push($scope.candidateModel.Educations[i]);
                  }
                  $scope.candidateModel.Educations = EducationalList;
              }

              if ($scope.candidateModel.EmployementHistory) {
                  for (var i = 0; i < $scope.candidateModel.EmployementHistory.length; i++) {
                      if ($scope.candidateModel.EmployementHistory[i].CountryId) {
                          $scope.setStateListByCountry($scope.candidateModel.EmployementHistory[i]);
                      }

                      if ($scope.candidateModel.EmployementHistory[i].StateId) {
                          $scope.setCityListByState($scope.candidateModel.EmployementHistory[i]);
                      }
                      EmployementHistoryList.push($scope.candidateModel.EmployementHistory[i]);
                  }
                  $scope.candidateModel.EmployementHistory = EmployementHistoryList;
              }
              candidateLookupProcess();

              blockUI.stop();
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Candidate', httpError.data.Status, httpError.data.Message);
          });
    };

    $scope.editProfileView = function () {
        blockUI.start();
        $scope.flagResumePresent = false;
        $scope.flagInvalid = false;
        $scope.flagMaxSize = false;
        $scope.flagVideoInvalid = false;
        $scope.flagVideoMaxSize = false;
        $scope.flagInvalidResume = false;
        $scope.flagMaxResumeSize = false;
        $scope.maxProfileImgFileSize = false;
        $scope.common.invalidProfileImgFile = false;
        $location.path('editProfile');
        blockUI.stop();
    };

    $scope.uploadResume = function (files) {
        $scope.flagResumePresent = false;
        $scope.flagMaxResumeSize = false;
        $scope.flagInvalidResume = false;
        if (files.length != 0) {
            $scope.resumeFileName = files[0].name;
            var ext = files[0].name.match(/(?:\.([^.]+))?$/)[1];
            if (files[0].size >= 2000000) {
                $scope.flagMaxResumeSize = true;
            }
            else {
                if (angular.lowercase(ext) === 'pdf' || angular.lowercase(ext) === 'doc' || angular.lowercase(ext) === 'docx' || angular.lowercase(ext) === 'txt') {
                    $scope.flagMaxResumeSize = false;
                    $scope.flagInvalidResume = false;
                }
                else {
                    $scope.flagInvalidResume = true;
                    $scope.candidateModel.Resume = undefined;
                    $scope.flagResumePresent = false;
                }
            }
        }
        else {
            $scope.flagInvalidResume = false;
            $scope.flagMaxResumeSize = false;
            $scope.enableButtonResume();
        }
    }

    $scope.enableButtonResume = function (resume) {
        if (resume && resume.name) {
            $scope.flagMaxSize = false;
            var ext = resume.name.match(/\.(.+)$/)[1];
            if (angular.lowercase(ext) === 'pdf' || angular.lowercase(ext) === 'doc' || angular.lowercase(ext) === 'docx' || angular.lowercase(ext) === 'txt') {
                if ($scope.candidateModel.Resume) {
                    $scope.candidateModel.Resume.data = resume.data;
                    $scope.flagInvalidResume = false;
                }
                else {
                    $scope.candidateModel.Resume.data = $scope.candidateModel.Resume.data;
                }
                $scope.flagInvalidResume = false;
                //$scope.candidateModel.Resume.data = "";
            }

        }
        else {
            $scope.flagResumePresent = true;
            $scope.flagResumePresent = false;
            $scope.candidateModel.Resume = undefined;
        }
    }

    $scope.enableUploadResume = function (resume) {
        if (resume && resume.name) {
            $scope.flagVideoMaxSize = false;
            var ext = resume.name.match(/\.(.+)$/)[1];
            if (angular.lowercase(ext) === 'mp4' || angular.lowercase(ext) === 'webm') {
                if ($scope.candidateModel.VideoResume) {

                    $scope.candidateModel.VideoResume.data = resume.data;
                    $scope.flagInvalidVideoResume = false;
                }
                else {

                    $scope.candidateModel.VideoResume.data = $scope.candidateModel.VideoResume.data;
                }
                $scope.flagInvalidVideoResume = false;

            }

        }
        else {

            $scope.flagVideoResumePresent = false;
            $scope.candidateModel.VideoResume = undefined;
        }
    }

    $scope.uploadVideo = function (files) {
        blockUI.start();
        $scope.candidateModel.isResumeCreated = false;
        $scope.dataUrl = undefined;
        if (files.length != 0) {
            $scope.videoresumeFileName = files[0].name;
            var ext = files[0].name.match(/(?:\.([^.]+))?$/)[1];
            // 20 MB Max size
            if (files[0].size >= 20000000) {
                blockUI.stop();
                $scope.flagVideoMaxSize = true;
            }
            else {
                if (angular.lowercase(ext) === 'webm' || angular.lowercase(ext) === 'mp4') {
                    blockUI.stop();
                    $scope.flagVideoMaxSize = false;
                    $scope.flagVideoInvalid = false;
                }
                else {
                    blockUI.stop();
                    $scope.candidateModel.VideoResume = "";
                    $scope.flagVideoInvalid = true;
                }
            }
        }
        else {
            blockUI.stop();
            $scope.flagVideoInvalid = false;
            $scope.flagVideoMaxSize = false;
        }
    };

    $scope.uploadImage = function (files) {
        if (files.length != 0) {
            $scope.orgLogoFileName = files[0].name;
            var ext = files[0].name.match(/(?:\.([^.]+))?$/)[1];
            if (files[0].size >= 2000000) {
                $scope.flagMaxSize = true;
            }
            else {
                if (angular.lowercase(ext) === 'jpg' || angular.lowercase(ext) === 'jpeg' || angular.lowercase(ext) === 'png') {
                    $scope.flagMaxSize = false;
                    $scope.flagInvalid = false;
                }
                else {
                    $scope.candidateModel.ProfileImageFile = "";
                    $scope.flagInvalid = true;
                }
            }
        }
        else {
            $scope.flagInvalid = false;
            $scope.flagMaxSize = false;
        }
    };


    //$scope.enableButton = function (img) {
    //    if (img) {
    //        $scope.flagMaxSize = false;
    //        var ext = img.name.match(/\.(.+)$/)[1];
    //        if (angular.lowercase(ext) === 'jpg' || angular.lowercase(ext) === 'jpeg' || angular.lowercase(ext) === 'png') {
    //            if ($scope.candidateModel.ProfileImageFile) {
    //                $scope.candidateModel.ProfileImageFile.data = img.data;
    //                $scope.flagInvalid = false;
    //            }
    //            else {
    //                $scope.candidateModel.ProfileImageFile.data = $scope.candidateModel.ProfileImageFile.data;
    //            }
    //            $scope.flagInvalid = false;
    //            $scope.candidateModel.ProfileImageFile.data = "";
    //        }

    //    }
    //};


    $scope.enableFileButton = function (file, field) {
        if (file) {
            if (field == 'profileImage') {
                $scope.candidateModel.ProfileImageFile = null;
            }
            if (field == 'resumeFile') {
            }
        }
        $scope.common.maxProfileImgFileSize = false;
        $scope.common.invalidProfileImgFile = false;
    };

    $scope.editCandidateProcess = function (editCandidateForm) {
        if (new ValidationService().checkFormValidity(editCandidateForm) &&
                               $scope.candidateModel.Resume &&
                               $scope.candidateModel.Resume.name &&
                               !$scope.common.invalidProfileImgFile &&
                               !$scope.common.maxProfileImgFileSize &&
                                    !$scope.flagVideoMaxSize && !$scope.flagVideoInvalid
            ) {
            $scope.displayValidationSummary = false;
            $scope.common.maxProfileImgFileSize = false;
            $scope.common.invalidProfileImgFile = false;
            blockUI.start();
            $scope.cityValuebyId($scope.candidateModel.CityId);
            $scope.stateValuebyId($scope.candidateModel.StateId);
            $scope.countryValuebyId($scope.candidateModel.CountryId);
            $scope.candidateModel.BirthDate = $filter('date')($scope.candidateModel.BirthDate, "MM/dd/yyyy");
            $scope.candidateModel.PassportValidUpto = $filter('date')($scope.candidateModel.PassportValidUpto, "MM/dd/yyyy");
            $scope.candidateModel.VisaValidUpto = $filter('date')($scope.candidateModel.VisaValidUpto, "MM/dd/yyyy");
            $rootScope.skillSet = $scope.candidateModel.Skills;

            //image file path
            if ($scope.candidateModel.ProfileImageFile == null) {
                $scope.candidateModel.ProfileImagePath = null;
            }

            //To add resume
            if ($scope.candidateModel.Resume) {
                $scope.candidateModel.ResumeList = [];
                $scope.candidateModel.ResumeList.push($scope.candidateModel.Resume);
            }

            //To add resume
            if ($scope.candidateModel.VideoResume == null)
                $scope.candidateModel.VideoResume = {};

            if ($scope.dataUrl) {
                $scope.candidateModel.VideoResume.data = $scope.dataUrl;
                $scope.candidateModel.VideoResumeList = [];
                $scope.candidateModel.VideoResumeList.push($scope.candidateModel.VideoResume);
            }
            else {
                if ($scope.candidateModel.VideoResume) {
                    $scope.candidateModel.VideoResumeList = [];
                    $scope.candidateModel.VideoResumeList.push($scope.candidateModel.VideoResume);
                }

            }

            $scope.candidateModel.IsCandidate = true;
            var promise = XSeedApiFactory.editProfile($scope.candidateModel);
            promise.then(
              function (response) {
                  blockUI.stop();
                  XSeedAlert.swal({
                      title: 'Success!',
                      text: 'Your details updated successfully!',
                      type: "success",
                      customClass: "xseed-error-alert",
                      allowOutsideClick: false
                  }).then(function () {
                      $timeout(function () {
                          $scope.showVideoRecorder = false;
                          $location.path('candidateProfile');
                      }, 0);
                  }, function (dismiss) {
                      blockUI.stop();
                  });
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Candidate', httpError.data.Status, httpError.data.Message);
              });
        }
        else {
            //if (!$scope.candidateModel.Resume.data)
            if ($scope.candidateModel.Resume === undefined) {
                $scope.flagResumePresent = true;
                $scope.candidateModel.Resume = {};
            }
            $scope.displayValidationSummary = true;
        }
    };

    $scope.checkImage = function () {
        var dd = document.getElementById('showimage')
        //nude.init()
        nude.load(dd);
        nude.scan(function (result) { if (!result) document.getElementById('result2').innerHTML = 'No nude!!'; });
    };

    $scope.addEducationRecord = function () {
        if (!$scope.candidateModel.Educations)
            $scope.candidateModel.Educations = [];
        $scope.candidateModel.Educations.push({ "StartDate": "", "EndDate": "", "DegreeDate": "", "SchoolName": "", "SchoolType": "", "DegreeName": "", "DegreeType": "", "Major": "" });
    }

    $scope.removeEducationRecord = function (index) {
        $scope.candidateModel.Educations.splice(index, 1);
    }

    $scope.addEmploymentHistoryRecord = function () {
        if (!$scope.candidateModel.EmployementHistory)
            $scope.candidateModel.EmployementHistory = [];
        $scope.candidateModel.EmployementHistory.push({ "StartDate": "", "EndDate": "", "CompanyName": "", "JobTitle": "", "Salary": "", "SalaryType": "", "AdditionalSalary": "", "AdditionalSalaryDescription": "", "ChangeReason": "", "IsCurrent": "", "EmployementType": "" });
    }

    $scope.removeEmploymentHistoryRecord = function (index) {
        $scope.candidateModel.EmployementHistory.splice(index, 1);
    }

    function candidateLookupProcess() {
        if (!$scope.isUndefinedOrNull($scope.flag.candidateLookupLoaded) && $scope.flag.candidateLookupLoaded == false) {
            candidatePageLookupList();
        }
    };

    function candidatePageLookupList() {
        var promise = XSeedApiFactory.candidatePageLookupCall();
        promise.then(
          function (response) {
              $scope.lookup.skill = response[0].data;
              $scope.lookup.maritalStatus = response[1].data;
              $scope.lookup.visaType = response[2].data;
              $scope.lookup.title = response[3].data;
              $scope.lookup.gender = response[4].data;
              $scope.lookup.languages = response[5].data;
              $scope.lookup.candidateStatus = response[6].data;
              //$scope.lookup.degree = $rootScope.metadata.GetAncillaryInformationResponse.DegreeList.Degree;
              $scope.lookup.jobType = $rootScope.metadata.GetAncillaryInformationResponse.jobTypeList.jobType;

              buildSkillStructuredData();
              buildLanguageStructuredData();
              $scope.flag.candidateLookupLoaded = true;
          },
          function (httpError) {
              ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
          });
    };

    $scope.candidateCancel = function () {
        $location.path('candidateProfile');
    };

    $scope.getCandidateResumeParserResult = function () {
        blockUI.start();
        var candidateDetail = $scope.candidateModel;
        var resume = $scope.candidateModel.Resume;
        var promise = XSeedApiFactory.getCandidateResumeParserCall($scope.candidateModel.Resume, $scope.candidateModel._id);
        promise.then(
        function (response) {
            $scope.candidateModel = response[0].data;

            //to keep this field as before parse resume
            $scope.candidateModel._id = candidateDetail._id;
            $scope.candidateModel.UserId = candidateDetail.UserId;
            $scope.candidateModel.CandidateId = candidateDetail.CandidateId;
            $scope.candidateModel.PrimaryEmail = candidateDetail.PrimaryEmail;

            if ($scope.candidateModel.CountryId) {
                $scope.getStateListByCountry($scope.candidateModel.CountryId);
            }

            if ($scope.candidateModel.StateId) {
                $scope.getCityListByState($scope.candidateModel.StateId);
            }

            var res = $scope.candidateModel.Skills;
            angular.forEach(res, function (val, index) {
                $scope.lookup.skillListData.push({ value: val, label: val });
            });
            $scope.candidateModel.Resume = resume;
            $scope.candidateModel.ResumeContent = $scope.candidateModel.Resume.data;

            $location.path("editProfile");
            blockUI.stop();
        },
        function (httpError) {
            blockUI.stop();
            ExceptionHandler.handlerHTTPException('ResumeParser', httpError.data.Status, httpError.data.Message);
        });
    }

    function buildSkillStructuredData() {
        $scope.skills = [{}];
        var arrSkill = [];
        var res = $scope.lookup.skill;
        angular.forEach(res, function (val, index) {
            arrSkill.push({ value: val.Name, label: val.Name });
        });

        blockUI.stop();
        $scope.lookup.skillListData = arrSkill;
    }

    function buildLanguageStructuredData() {
        //manpulation for languages
        $scope.lookup.languageList = [];
        angular.forEach($scope.lookup.languages, function (val, index) {
            $scope.lookup.languageList.push({ value: val.Name, label: val.Name });
        });
    };

    $scope.setStateListByCountry = function (object) {
        var promise = XSeedApiFactory.getStateListByCountry(object.CountryId);
        promise.then(
          function (response) {
              object.stateList = response[0].data;
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
          });
    };

    $scope.setCityListByState = function (object) {
        var promise = XSeedApiFactory.getCityListByState(object.StateId);
        promise.then(
          function (response) {
              object.cityList = response[0].data;
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
          });
    };

    $scope.cityValuebyId = function (Id) {
        angular.forEach($scope.cityList, function (val, index) {
            if (val.Id == Id) {
                $scope.candidateModel.City = val.Name;
            }
        });
    };

    $scope.stateValuebyId = function (Id) {
        angular.forEach($scope.stateList, function (val, index) {
            if (val.Id == Id) {
                $scope.candidateModel.State = val.Name;
            }
        });
    };


    //  Video Check Function called from editProfile Page - init();
    $scope.videoCheck = function () {

        $scope.isVideoPresentAlready = false;

        if ($scope.candidateModel.VideoResume.filePath != null || $scope.candidateModel.VideoResume.filePath != undefined) {
            $scope.isVideoPresentAlready = true;
        }
        else {
            $scope.isVideoPresentAlready = false;
        }


    };

    $scope.Videojs = function () {
        $scope.deviceNotFound = null;
        $scope.showVideoRecorder = true;
        $scope.hideVideoFlag = false;
        $scope.candidateModel.isResumeCreated = true;
        var oldPlayer = document.getElementById('myVideo');
        if (oldPlayer != null) {
            var player = videojs(oldPlayer, {
                controls: true,
                width: 320,
                height: 240,
                fluid: false,
                plugins: {
                    record: {
                        audio: true,
                        video: true,
                        maxLength: 60 * 2,
                        debug: true,
                        videoMimeType: 'video/webm'
                    }
                }
            });
            //, function () {
            //    // print version information at startup
            //    videojs.log('Using video.js', videojs.VERSION,
            //        'with videojs-record', videojs.getPluginVersion('record'),
            //        'and recordrtc', RecordRTC.version);
            //});
            // error handling
            player.on('deviceError', function () {
                $scope.deviceNotFound = player.deviceErrorCode.message;
            });
            player.on('error', function (error) {
            });
            // user clicked the record button and started recording
            player.on('startRecord', function () {
            });
            // user completed recording and stream is available
            //player.on('finishRecord', function () {
            //    // the blob object contains the recorded data that
            //    // can be downloaded by the user, stored on server etc.
            //    var data = player.recordedData;
            //    $scope.flagVideoMaxSize = false;
            //    if (data.video) {
            //        if (data.video.size <= configuration.XSEED_CANDIDATE_VIDEORESUME_SIZE) {
            //            // for chrome (when recording audio+video)
            //            blockUI.start();
            //            blobToBase64Callback(data.video, function (result) {
            //                if (result) {
            //                    blockUI.stop();
            //                }
            //            });
            //        }
            //        else {
            //            $scope.flagVideoMaxSize = true;
            //        }
            //    }
            //});
            // user completed recording and stream is available
            player.on('finishRecord', function () {
                // the blob object contains the recorded data that
                // can be downloaded by the user, stored on server etc.
                var data = player.recordedData;
                $scope.flagVideoMaxSize = false;

                //// For Chrome only
                if (data.video) {
                    if (data.video.size <= configuration.XSEED_CANDIDATE_VIDEORESUME_SIZE) {
                        // for chrome (when recording audio+video)
                        blockUI.start();
                        blobToBase64Callback(data.video, function (result) {
                            if (result) {
                                blockUI.stop();
                            }
                        });
                    }
                    else {
                        $scope.flagVideoMaxSize = true;
                    }
                }
                    //// For browsers other than Chrome 
                else {
                    if (data.size <= configuration.XSEED_CANDIDATE_VIDEORESUME_SIZE) {
                        blockUI.start();
                        blobToBase64Callback(data, function (result) {
                            if (result) {
                                blockUI.stop();
                            }
                        });
                    }
                    else {
                        $scope.flagVideoMaxSize = true;
                    }
                }
            });
        }
    }




    // blob To Base64 Callback
    function blobToBase64Callback(data, callback) {
        var reader = new FileReader();
        reader.onload = function () {
            $scope.dataUrl = reader.result;
            console.log($scope.dataUrl);
            callback($scope.dataUrl);
        };
        reader.readAsDataURL(data);

    }

    $scope.CreateVideoHide = function () {
        $scope.showVideoRecorder = false;
        var oldPlayer = document.getElementById('myVideo');
        videojs(oldPlayer).dispose();

        if ($scope.candidateModel.VideoResume != null) {
            $scope.hideVideoFlag = true;
        }
    };

    //For iframe
    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }

    $scope.countryValuebyId = function (Id) {
        angular.forEach($rootScope.metadata.GetAncillaryInformationResponse.countryList.country, function (val, index) {
            if (val.Id == Id) {
                $scope.candidateModel.Country = val.Name;
            }
        });
    };

    //***************************************************************************************//

    // Get the render context local to this controller (and relevant params).
    var renderContext = RequestContext.getRenderContext("standard.candidate");
    // The subview indicates which view is going to be rendered on the page.
    $scope.subview = renderContext.getNextSection();
    $scope.$on(
        "requestContextChanged",
        function () {
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {
                return;
            }
            // Update the view that is being rendered.
            $scope.subview = renderContext.getNextSection();
        }
    );
};