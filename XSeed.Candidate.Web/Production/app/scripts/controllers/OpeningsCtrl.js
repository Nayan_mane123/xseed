﻿'use strict';

XSEED_CANDIDATE_APP.controller('OpeningsCtrl', OpeningsCtrl);

/**
 * @ngInject
 */
function OpeningsCtrl($scope, $rootScope, $route, $routeParams, RequestContext, $timeout, $q, $location, $window, configuration, i18nFactory, ExceptionHandler, EventBus, $log, _, ipCookie, blockUI, ModalFactory, XSeedAlert, localStorageService, $uibModal, XSeedApiFactory, ngTableParams, $filter, ValidationService) {
    init();
    function init() {
        var applicationStausPath = '/applicationStatus';
        $scope.requirementModel = {};

        if (($scope.isUndefinedOrNull($scope.requirementModel._id) || $scope.isEmpty($scope.requirementModel._id)) && $location.path().toLowerCase() != applicationStausPath.toLowerCase()) {
            $location.path('openings');
        }
        $scope.requirementFilter = {};
        $scope.searchModel = {};
        $scope.appliedJobStatusFilter = {};
        $scope.todaysDate = new Date();
    };

    $scope.$watch("requirementFilter.$", function () {
        if (angular.isDefined($scope.tblRequirementList)) {
            $scope.tblRequirementList.reload();
            $scope.tblRequirementList.page(1);
        }
    });

    $scope.$watch("appliedJobStatusFilter.$", function () {
        if (angular.isDefined($scope.tblRequirementStatus)) {
            $scope.tblRequirementStatus.reload();
            $scope.tblRequirementStatus.page(1);
        }
    });

    $scope.setSelectedTabIndex = function (tabIndex) {
        $scope.selectedTabIndex = tabIndex;
    };

    $scope.getJobOpenings = function () {
        blockUI.start();
        $scope.jobList = {};
        $scope.requirementFilter = {};
        var recordCount = configuration.XSEED_CANDIDATE_MATCHINGJOBS_COUNT;
        var promise = XSeedApiFactory.getJobOpenings($rootScope.userDetails._id, recordCount);
        promise.then(
          function (response) {
              $scope.jobList = response[0].data;
              //manupulation of response
              angular.forEach($scope.jobList, function (val, index) {
                  if (val.TechnicalSkills) {
                      val.TechnicalSkills = val.TechnicalSkills.split('|');
                  }
              });
              checkAlreadyAppliedJobs();
              populateRequirementList();
              blockUI.stop();
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
          });
    };

    $scope.getDefaultJobOpenings = function () {
        if ($scope.selectedTabIndex == 1) {
            blockUI.start();
            $scope.jobList = {};
            $scope.requirementFilter = {};
            var recordCount = configuration.XSEED_CANDIDATE_TOPJOBS_COUNT;
            var promise = XSeedApiFactory.getDefaultJobOpenings(recordCount);
            promise.then(
              function (response) {
                  $scope.jobList = response[0].data;
                  //manupulation of response
                  angular.forEach($scope.jobList, function (val, index) {
                      if (val.TechnicalSkills) {
                          val.TechnicalSkills = val.TechnicalSkills.split('|');
                      }
                  });
                  checkAlreadyAppliedJobs();
                  populateRequirementList();
                  blockUI.stop();
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
              });
        }
    };

    function populateRequirementList() {
        if (angular.isDefined($scope.tblRequirementList)) {
            $scope.tblRequirementList.reload();
        }
        else {
            $scope.tblRequirementList = new ngTableParams({
                page: 1,
                count: 10,
                sorting: {}
            }, {
                counts: [],
                total: $scope.jobList.length,
                getData: function ($defer, params) {
                    var filteredData = $filter('filter')($scope.jobList, $scope.requirementFilter);
                    $scope.dataRequirementList = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
                    params.total($scope.dataRequirementList.length);
                    $scope.dataRequirementList = $scope.dataRequirementList.slice((params.page() - 1) * params.count(), params.page() * params.count());

                    $defer.resolve($scope.dataRequirementList);
                }
                , $scope: $scope
            });
        }
    }

    $scope.jobDetailView = function (jobId) {
        blockUI.start();
        var promise = XSeedApiFactory.getJobDetail(jobId);
        promise.then(
          function (response) {
              $scope.requirementModel = response[0].data;
              //check if already applied
              if ($scope.selectedTabIndex != 3) {
                  checkAlreadyAppliedJobs();
              }
              else {
                  checkAlreadyAppliedSearchedJobs();
              }

              if ($scope.requirementModel.CountryId) {
                  $scope.getStateListByCountry($scope.requirementModel.CountryId);
              }

              if ($scope.requirementModel.StateId) {
                  $scope.getCityListByState($scope.requirementModel.StateId);
              }

              $scope.requirementModel.DriveFromDate = $filter('date')($scope.requirementModel.DriveFromDate, "MM/dd/yyyy");
              $scope.requirementModel.DriveToDate = $filter('date')($scope.requirementModel.DriveToDate, "MM/dd/yyyy");
              $scope.requirementModel.JobPostedDate = $filter('date')($scope.requirementModel.JobPostedDate, "MM/dd/yyyy");
              $scope.requirementModel.JobClosedDate = $filter('date')($scope.requirementModel.JobClosedDate, "MM/dd/yyyy");
              $scope.requirementModel.JobExpiryDate = $filter('date')($scope.requirementModel.JobExpiryDate, "MM/dd/yyyy");

              if ($scope.requirementModel.TechnicalSkills) {
                  $scope.requirementModel.SkillList = [];
                  $scope.requirementModel.SkillList = $scope.requirementModel.TechnicalSkills.split("|");
              }


              blockUI.stop();
              $location.path('jobDetail');
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
          });
    };

    $scope.applyToSelectedJob = function (job) {
        var Id = $rootScope.userDetails._id;
        XSeedAlert.swal({
            title: "Are you sure?",
            text: "You want to apply for " + job.JobTitle,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Apply",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function () {
            var jobApplication = {};
            job.isApplied = true;
            jobApplication.Company = job.CompanyName;
            jobApplication.JobId = job.Id;
            jobApplication.JobStatus = job.JobStatus;
            jobApplication.JobTitle = job.JobTitle;
            jobApplication.Status = "Applied";
            jobApplication.AppliedDate = $filter('date')($scope.todaysDate, "MM/dd/yyyy");
            var promise = XSeedApiFactory.applyToSelectedJob(jobApplication, Id);
            promise.then(
              function (response) {
                  blockUI.stop();
                  XSeedAlert.swal({
                      title: 'Success!',
                      text: 'Applied successfully!',
                      type: "success",
                      customClass: "xseed-error-alert",
                      allowOutsideClick: false
                  }).then(function () {
                      $timeout(function () {
                          changeJobStatusOnJobList(job);
                          $location.path('openings');
                      }, 0);
                  }, function (dismiss) {
                      blockUI.stop();
                  });
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
              });
        }, function (dismiss) { });
    };

    function changeJobStatusOnJobList(job) {
        if ($scope.selectedTabIndex == 3) {
            angular.forEach($scope.dataSearchedRequirementList, function (val, index) {
                if (val.Id == job.Id) {
                    val.isApplied = true;
                }
            });
        }
        else {
            angular.forEach($scope.dataRequirementList, function (val, index) {
                if (val.Id == job.Id) {
                    val.isApplied = true;
                }
            });
        }
    }

    $scope.getAppliedJobStatus = function () {
        var userId = $rootScope.userDetails.UserId;
        blockUI.start();
        var promise = XSeedApiFactory.getAppliedJobStatus(userId);
        promise.then(
          function (response) {
              $scope.appliedJobStatus = response[0].data;
              if ($scope.appliedJobStatus) {
                  populateRequirementStatus();
              }
              blockUI.stop();
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
          });
    };

    function populateRequirementStatus() {
        if (angular.isDefined($scope.tblRequirementStatus)) {
            $scope.tblRequirementStatus.reload();
        }
        else {
            $scope.tblRequirementStatus = new ngTableParams({
                page: 1,
                count: 10,
                sorting: {}
            }, {
                counts: [],
                total: $scope.appliedJobStatus.length,
                getData: function ($defer, params) {
                    var filteredData = $filter('filter')($scope.appliedJobStatus, $scope.appliedJobStatusFilter);
                    $scope.dataAppliedJobStatus = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
                    params.total($scope.dataAppliedJobStatus.length);
                    $scope.dataAppliedJobStatus = $scope.dataAppliedJobStatus.slice((params.page() - 1) * params.count(), params.page() * params.count());

                    $defer.resolve($scope.dataAppliedJobStatus);
                }
                , $scope: $scope
            });
        }
    }

    function checkAlreadyAppliedJobs() {
        blockUI.start();
        var userId = $rootScope.userDetails.UserId;
        var promise = XSeedApiFactory.getAppliedJobStatus(userId);
        promise.then(
          function (response) {
              $scope.appliedJobStatus = response[0].data;
              if ($scope.appliedJobStatus) {
                  for (var i = 0 ; i < $scope.jobList.length; i++) {
                      for (var j = 0 ; j < $scope.appliedJobStatus.length; j++) {
                          if ($scope.jobList[i].Id === $scope.appliedJobStatus[j].JobId) {
                              $scope.jobList[i].isApplied = true;
                          }
                      }
                  }
                  if ($scope.requirementModel) {
                      for (var j = 0 ; j < $scope.appliedJobStatus.length; j++) {
                          if ($scope.requirementModel.Id === $scope.appliedJobStatus[j].JobId) {
                              $scope.requirementModel.isApplied = true;
                          }
                      }
                  }
              }
              blockUI.stop();
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
          });

    };

    $scope.goToOpenings = function () {
        $location.path("openings");
    }


    //**********************************************  SEARCH JOBS *****************************************//
    $scope.getJobSearchOptions = function () {
        $scope.lookup.degree = $rootScope.metadata.GetAncillaryInformationResponse.DegreeList.Degree;
        getSkillLookUp();
    };

    function getSkillLookUp() {
        var promise = XSeedApiFactory.candidateSkillLookupCall();
        promise.then(
          function (response) {
              $scope.lookup.skill = response[0].data;
              buildSkillStructuredData();
          },
          function (httpError) {
              ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
          });

    };

    function buildSkillStructuredData() {
        $scope.skills = [{}];
        var arrSkill = [];
        var res = $scope.lookup.skill;
        angular.forEach(res, function (val, index) {
            arrSkill.push({ value: val.Name, label: val.Name });
        });

        blockUI.stop();
        $scope.lookup.skillListData = arrSkill;
    }

    $scope.searchJobs = function (searchJobsForm) {
        if (new ValidationService().checkFormValidity(searchJobsForm)) {
            $scope.searchModel.JobTitle = $scope.searchModel.JobTitle == "" ? undefined : $scope.searchModel.JobTitle;
            $scope.searchModel.Location = $scope.searchModel.Location == "" ? undefined : $scope.searchModel.Location;

            var promise = XSeedApiFactory.searchJobOpenings($scope.searchModel);
            promise.then(
              function (response) {
                  $scope.searchedJobList = response[0].data;
                  //manupulation of response
                  angular.forEach($scope.searchedJobList, function (val, index) {
                      if (val.TechnicalSkills) {
                          val.TechnicalSkills = val.TechnicalSkills.split('|');
                      }
                  });
                  checkAlreadyAppliedSearchedJobs();
                  populateSearchedRequirementList();
                  blockUI.stop();
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
              });
        }
    };

    function checkAlreadyAppliedSearchedJobs() {
        blockUI.start();
        var userId = $rootScope.userDetails.UserId;
        var promise = XSeedApiFactory.getAppliedJobStatus(userId);
        promise.then(
          function (response) {
              $scope.appliedJobStatus = response[0].data;
              if ($scope.appliedJobStatus) {
                  for (var i = 0 ; i < $scope.searchedJobList.length; i++) {
                      for (var j = 0 ; j < $scope.appliedJobStatus.length; j++) {
                          if ($scope.searchedJobList[i].Id === $scope.appliedJobStatus[j].JobId) {
                              $scope.searchedJobList[i].isApplied = true;
                          }
                      }
                  }
                  if ($scope.requirementModel) {
                      for (var j = 0 ; j < $scope.appliedJobStatus.length; j++) {
                          if ($scope.requirementModel.Id === $scope.appliedJobStatus[j].JobId) {
                              $scope.requirementModel.isApplied = true;
                          }
                      }
                  }
              }
              blockUI.stop();
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
          });

    };

    function populateSearchedRequirementList() {
        if (angular.isDefined($scope.tblSearchedRequirementList)) {
            $scope.tblSearchedRequirementList.reload();
        }
        else {
            $scope.requirementFilter = {};
            $scope.tblSearchedRequirementList = new ngTableParams({
                page: 1,
                count: 10,
                sorting: {}
            }, {
                counts: [],
                total: $scope.searchedJobList.length,
                getData: function ($defer, params) {
                    var filteredData = $filter('filter')($scope.searchedJobList, $scope.requirementFilter);
                    $scope.dataSearchedRequirementList = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
                    params.total($scope.dataSearchedRequirementList.length);
                    $scope.dataSearchedRequirementList = $scope.dataSearchedRequirementList.slice((params.page() - 1) * params.count(), params.page() * params.count());

                    $defer.resolve($scope.dataSearchedRequirementList);
                }
                , $scope: $scope
            });
        }
    };

    //**********************************************  SEARCH JOBS *****************************************//

    //***************************************************************************************//

    // Get the render context local to this controller (and relevant params).
    var renderContext = RequestContext.getRenderContext("standard.opening");
    // The subview indicates which view is going to be rendered on the page.
    $scope.subview = renderContext.getNextSection();
    $scope.$on(
        "requestContextChanged",
        function () {
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {
                return;
            }
            // Update the view that is being rendered.
            $scope.subview = renderContext.getNextSection();
        }
    );
};