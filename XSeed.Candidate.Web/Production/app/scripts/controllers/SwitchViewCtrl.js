﻿'use strict';

XSEED_CANDIDATE_APP.controller('SwitchViewCtrl', SwitchViewCtrl);

/**
 * @ngInject
 */
function SwitchViewCtrl($scope, blockUI, $location, $timeout, $log, RequestContext, EventBus) {
    $log.info("SwitchViewCtrl");

    function init() {
        blockUI.start();
        $timeout(function () {
            var view = EventBus.switchView;
            $location.path(view).replace();
            blockUI.stop();
            EventBus.switchView = undefined;
        }, 0);
    }

    init();

    var renderContext = RequestContext.getRenderContext("standard.switchView");

    // The subview indicates which view is going to be rendered on the page.
    $scope.subview = renderContext.getNextSection();
    $scope.$on(
      "requestContextChanged",
      function () {
          // Make sure this change is relevant to this controller.
          if (!renderContext.isChangeRelevant()) {
              return;
          }

          // Update the view that is being rendered.
          $scope.subview = renderContext.getNextSection();

      }
    );
};