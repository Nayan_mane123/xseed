'use strict';

XSEED_CANDIDATE_APP.controller('LandingCtrl', LandingCtrl);

/**
 * @ngInject
 */
function LandingCtrl($scope, $rootScope, $route, $routeParams, RequestContext, localStorageService, $log, ExceptionHandler, $location, blockUI, XSeedApiFactory, ModalFactory, XSeedAlert, $filter, ValidationService, configuration, $timeout, $cookies) {
    $log.info("LandingCtrl");

    $scope.forgotPasswordModel = {};
    $scope.resetPasswordModel = {};
    $scope.loginRequest = {};
    $scope.registerCandidateRequest = {};
    $scope.registerCandidateRequest.Provider = null;
    $scope.registerCandidateRequest = {};
    $scope.emailVerifyModel = {};
    $scope.toggleLoginRegistrationFlag = 1;
    getCookieStayLoggedIn();
    // Get the render context local to this controller (and relevant params).
    var renderContext = RequestContext.getRenderContext("landing");
    // The subview indicates which view is going to be rendered on the page.
    $scope.subview = renderContext.getNextSection();
    $scope.$on(
        "requestContextChanged",
        function () {
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {
                return;
            }
            // Update the view that is being rendered.
            $scope.subview = renderContext.getNextSection();
        }
    );

    $scope.gotoForgotPassword = function () {
        $scope.forgotPasswordModel = {};
        $scope.resetPasswordModel = {};
        $scope.loginRequest = {};
        $scope.registerCandidateRequest = {};
        $location.path('forgotPassword');
    }

    $scope.gotoRegisterCandidate = function () {
        $scope.forgotPasswordModel = {};
        $scope.resetPasswordModel = {};
        $scope.loginRequest = {};
        $scope.registerCandidateRequest = {};
        $location.path('candidateRegistration');
    }

    $scope.gotoLogin = function () {
        $scope.forgotPasswordModel = {};
        $scope.resetPasswordModel = {};
        $scope.loginRequest = {};
        $scope.registerCandidateRequest = {};
        $location.path('candidateLogin');
    }


    $scope.registerCandidate = function (candidateRegistrationForm) {
        if (new ValidationService().checkFormValidity(candidateRegistrationForm)) {

            if (candidateRegistrationForm.$name == 'associateForm') {
                $scope.registerCandidateRequest =
                    {
                        CandidateUserName: $rootScope.externalAuthData.userName,
                        Provider: $rootScope.externalAuthData.provider,
                        //ExternalToken: $rootScope.externalAuthData.externalAccessToken,
                        UserName: $rootScope.externalAuthData.email,
                        UniqueId: $rootScope.externalAuthData.uniqueId,
                        FirstName: $rootScope.externalAuthData.firstName,
                        LastName: $rootScope.externalAuthData.lastName,
                        Mobile: $rootScope.externalAuthData.mobile
                    }
            }

            blockUI.start();

            $scope.registrationCopyModel = {};
            $scope.registrationCopyModel = angular.copy($scope.registerCandidateRequest);
            $scope.registrationCopyModel.IsCandidate = true;

            //Dummy model
            if ($scope.registrationCopyModel.Provider == null || $scope.registrationCopyModel.Provider == undefined) {
                /********************************************** Ecrypt Password ****************************************/

                /* Code for Encryption UserName Start */
                var key = CryptoJS.enc.Utf8.parse('4263193851707158');
                var iv = CryptoJS.enc.Utf8.parse('4263193851707158');

                $scope.userNameEncrypted = $scope.registrationCopyModel.userName;
                var timestamp = Date.parse(new Date());

                var encryptedlogin = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.userNameEncrypted), key,
               {
                   keySize: 128 / 8,
                   iv: iv,
                   mode: CryptoJS.mode.CBC,
                   padding: CryptoJS.pad.Pkcs7
               });

                $scope.userNameEncrypted = encryptedlogin;
                /* Code for Encryption UserName End */

                /* Code for Encryption Password Start */
                $scope.passwordEncrypted = $scope.registrationCopyModel.password;
                var encryptedpassword = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.passwordEncrypted), key,
                {
                    keySize: 128 / 8,
                    iv: iv,
                    mode: CryptoJS.mode.CBC,
                    padding: CryptoJS.pad.Pkcs7
                });

                $scope.passwordEncrypted = encryptedpassword;

                var tempUserName = '' + $scope.userNameEncrypted + '';
                var tempPassword = '' + $scope.passwordEncrypted + '';

                $scope.registrationCopyModel.userName = tempUserName;
                $scope.registrationCopyModel.password = tempPassword;
                $scope.registrationCopyModel.confirmPassword = tempPassword;

                /********************************************** Ecrypt Password ****************************************/
            }
            var promise = XSeedApiFactory.registerCandidate($scope.registrationCopyModel);
            promise.then(
              function (response) {

                  blockUI.stop();
                  var successMessage = $scope.registerCandidateRequest.Provider ? "Account created successfully!" : "Account created successfully! Verification email sent to registered email."
                  $scope.registerCandidateResponse = response[0].data;

                  XSeedAlert.swal({
                      title: 'Success!',
                      text: successMessage,
                      type: "success",
                      customClass: "xseed-error-alert",
                  }).then(function () {
                      //$scope.registerCandidateRequest = {};
                      $timeout(function () {
                          if (candidateRegistrationForm.$name == 'associateForm') {
                              //$rootScope.prevLocPath = $location.path();
                              //$location.path('candidateProfile');

                              var request = 'uniqueId=' + $rootScope.externalAuthData.uniqueId + '&provider=' + $rootScope.externalAuthData.provider;

                              blockUI.start();
                              socialLoginPromisCall(request);
                          }
                          else {
                              $location.path('resendEmailVerification');
                          }
                      }, 0);
                  });
              },
              function (httpError) {
                  blockUI.stop();
                  if (httpError.status == 400) {
                      ExceptionHandler.handlerPopup('Account', httpError.data, 'Warning', 'error');
                  }
                  else {
                      blockUI.stop();
                      var data = httpError.data.Message;
                      //Merge Account - Rohit
                      XSeedAlert.swal({
                          title: "Want to merge accounts?",
                          text: data,
                          type: "warning",
                          customClass: "xseed-error-alert",
                          showCancelButton: true,
                          cancelButtonText: "No",
                          confirmButtonText: "Yes",
                          allowOutsideClick: false
                      }).then(function () {
                          //Merge Call
                          callMergeAllAccounts($scope.registrationCopyModel, candidateRegistrationForm.$name);
                      }, function (dismiss) {
                          // $scope.registerCandidateRequest = {};
                          $location.path('candidateLogin');
                      });
                  }
              });
        }
    };

    function socialLoginPromisCall(request) {
        var promise = XSeedApiFactory.socialLogin1(request, $rootScope.externalAuthData.uniqueId, $rootScope.externalAuthData.provider);
        promise.then(
          function (response) {
              blockUI.stop();
              $rootScope.token.key = response[0].data.access_token;
              localStorageService.set("token", response[0].data.access_token);

              $rootScope.userDetails = response[0].data;
              localStorageService.set("userDetails", response[0].data);
              setOrRemoveCookieStayLoggedIn($scope.loginRequest.userName);

              $rootScope.prevLocPath = $location.path();
              $location.path('candidateProfile');
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerPopup('Login', httpError.data.error_description, 'Warning', 'info');
          });
    };

    function callMergeAllAccounts(registrationModel, formName) {
        $scope.mergeAccountsModel = {};
        blockUI.start();
        var promise = XSeedApiFactory.MergeAccounts(registrationModel);
        promise.then(
          function (response) {
              blockUI.stop();
              $scope.mergeAccountsModel = response[0].data;
              XSeedAlert.swal({
                  title: "Success",
                  text: 'Account merged successfully!',
                  type: "success",
                  customClass: "xseed-error-alert",
                  confirmButtonText: "Ok",
                  allowOutsideClick: false
              }).then(function () {
                  //Redirect to Dashboard page.
                  blockUI.start();
                  var request = 'uniqueId=' + $rootScope.externalAuthData.uniqueId + '&provider=' + $rootScope.externalAuthData.provider;
                  $scope.registerCandidateRequest = {};
                  if (formName == 'associateForm') {
                      socialLoginPromisCall(request);
                      blockUI.stop();
                  }
                  else {
                      $location.path('resendEmailVerification');
                      blockUI.stop();
                  }
              });
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerPopup('Login', httpError.data.Message, 'Warning', 'info');
          });
    };


    $scope.verifyEmail = function () {
        blockUI.start();
        var userId = $routeParams.userId;
        var code = $routeParams.code;

        if (userId && code) {
            var promise = XSeedApiFactory.verifyEmail(userId, code);
            promise.then(
              function (response) {
                  blockUI.stop();
                  $scope.verifyEmailMsg = response[0].data;
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Account', httpError.data.Status, httpError.data.Message);
              });
        }
    };

    $scope.resendEmailVerification = function (userName) {
        if (userName) {
            blockUI.start();
            var promise = XSeedApiFactory.resendEmailVerification(userName);
            promise.then(
              function (response) {
                  blockUI.stop();
                  XSeedAlert.swal({
                      title: 'Success!',
                      text: 'An email has been sent to help you to verify your username for XSeed.Com!',
                      type: "success",
                      customClass: "xseed-error-alert",
                  });
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerPopup('Account', 'Please check username.', 'Unsuccessful!', 'info');
              });
        }
    };

    $scope.login = function (loginForm) {
        if (new ValidationService().checkFormValidity(loginForm)) {

            /********************************************************** Encryption ***********************************************************/

            /* Code for Encryption UserName Start */
            var key = CryptoJS.enc.Utf8.parse('4263193851707158');
            var iv = CryptoJS.enc.Utf8.parse('4263193851707158');

            $scope.userNameEncrypted = $scope.loginRequest.userName;
            var timestamp = Date.parse(new Date());

            var encryptedlogin = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.userNameEncrypted), key,
            {
                keySize: 128 / 8,
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });

            $scope.userNameEncrypted = encryptedlogin;
            /* Code for Encryption UserName End */

            /* Code for Encryption Password Start */
            $scope.passwordEncrypted = $scope.loginRequest.password;
            var encryptedpassword = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.passwordEncrypted), key,
            {
                keySize: 128 / 8,
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });

            $scope.passwordEncrypted = encryptedpassword;
            //var request = 'userName=' + $scope.loginRequest.userName + '&password=' + $scope.loginRequest.password + '&grant_type=password&userType=Candidate';
            var request = 'userName=' + $scope.userNameEncrypted + '&password=' + $scope.passwordEncrypted;

            blockUI.start();

            // Encrypted Username and Password
            $scope.tempUserNameEncrypt = '' + $scope.userNameEncrypted + '';
            $scope.tempPAsswordEncrypt = '' + $scope.passwordEncrypted + '';

            /********************************************************** Encryption ***********************************************************/

            var promise = XSeedApiFactory.login(request, $scope.tempUserNameEncrypt, $scope.tempPAsswordEncrypt);
            promise.then(
              function (response) {
                  blockUI.stop();
                  $rootScope.token.key = response[0].data.access_token;
                  localStorageService.set("token", response[0].data.access_token);

                  $rootScope.userDetails = response[0].data;
                  //$rootScope.userDetails.ProfileImage = $rootScope.userDetails.ProfileImage.length == 0 ? "" : $rootScope.userDetails.ProfileImage.data;
                  localStorageService.set("userDetails", response[0].data);
                  setOrRemoveCookieStayLoggedIn($scope.loginRequest.userName);
                  $location.path('openings');
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerPopup('Account', httpError.data.Message, 'Warning', 'info');
              });
        }
    };

    $scope.socialLogin = function (provider) {

        var redirectUri = location.protocol + '//' + location.host + configuration.XSEED_SOCIAL_LOGIN_REDIRECTURI;

        var externalProviderUrl = configuration.XSEED_SOCIAL_LOGIN_URL + "?provider=" + provider
                                                                    + "&response_type=token&client_id=self"
                                                                    + "&redirect_uri=" + redirectUri;
        window.$windowScope = $scope;

        var oauthWindow = window.open(externalProviderUrl, "Authenticate Account", "location=0,status=0,width=600,height=750");
    };

    $scope.authCompletedCB = function (fragment) {
        $rootScope.myFragmentData = fragment;
        var username = fragment.external_user_name.split(" ");
        $rootScope.externalAuthData = {
            provider: fragment.provider,
            userName: fragment.external_user_name,
            firstName: username[0],
            lastName: username[1],
            email: fragment.external_user_email,
            uniqueId: fragment.external_user_uniqueId,
            externalAccessToken: fragment.external_access_token
        };

        localStorageService.set("externalAuthData", $rootScope.externalAuthData);

        $scope.$apply(function () {

            if (fragment.haslocalaccount == 'False') {
                //Create local account and proceed
                $location.path('associate');
            }
            else {
                //Obtain access token and redirect with functionality
                var request = 'uniqueId=' + $rootScope.externalAuthData.uniqueId + '&provider=' + $rootScope.externalAuthData.provider;

                blockUI.start();
                var promise = XSeedApiFactory.socialLogin1(request, $rootScope.externalAuthData.uniqueId, $rootScope.externalAuthData.provider);
                promise.then(
                  function (response) {
                      blockUI.stop();
                      $rootScope.token.key = response[0].data.access_token;
                      localStorageService.set("token", response[0].data.access_token);

                      $rootScope.userDetails = response[0].data;
                      localStorageService.set("userDetails", response[0].data);
                      setOrRemoveCookieStayLoggedIn($scope.loginRequest.userName);

                      $rootScope.prevLocPath = $location.path();
                      $location.path('candidateProfile');
                  },
                  function (httpError) {
                      blockUI.stop();
                      ExceptionHandler.handlerPopup('Login', httpError.data.error_description, 'Warning', 'error');
                  });
            }
        });
    }



    $scope.forgotPassword = function (forgotPasswordForm) {
        if (new ValidationService().checkFormValidity(forgotPasswordForm)) {
            blockUI.start();

            var promise = XSeedApiFactory.forgotPassword($scope.forgotPasswordModel);
            promise.then(
              function (response) {
                  blockUI.stop();

                  XSeedAlert.swal({
                      title: 'Success!',
                      text: 'Please check your email. We\'ve sent you an email that contains your username information.If you do not see the email, check your "Spam" or "Junk" email folder.',
                      type: "success",
                      customClass: "xseed-error-alert",
                  }).then(function () {
                      $scope.forgotPasswordModel = {};
                      $timeout(function () {
                          $location.path('candidateLogin');
                      }, 0);
                  }, function (dismiss) {
                  });
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerPopup('Account', 'Please check username.', 'Unsuccessful!', 'info');
              });
        };
    }

    $scope.resetPassword = function (resetPasswordForm) {
        if (new ValidationService().checkFormValidity(resetPasswordForm)) {
            blockUI.start();

            $scope.resetPasswordModel.UserId = $routeParams.userId;
            $scope.resetPasswordModel.Token = $routeParams.code;

            // Dummy model
            $scope.tempResetPasswordModel = {};
            $scope.tempResetPasswordModel = angular.copy($scope.resetPasswordModel);
            /********************************************** Ecrypt Password ****************************************/

            /* Code for Encryption UserName Start */
            var key = CryptoJS.enc.Utf8.parse('4263193851707158');
            var iv = CryptoJS.enc.Utf8.parse('4263193851707158');

            $scope.userNameEncrypted = $scope.tempResetPasswordModel.NewPassword;
            var timestamp = Date.parse(new Date());

            var encryptedlogin = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.userNameEncrypted), key,
           {
               keySize: 128 / 8,
               iv: iv,
               mode: CryptoJS.mode.CBC,
               padding: CryptoJS.pad.Pkcs7
           });

            $scope.userNameEncrypted = encryptedlogin;
            /* Code for Encryption UserName End */

            /* Code for Encryption Password Start */
            $scope.passwordEncrypted = $scope.tempResetPasswordModel.ConfirmPassword;
            var encryptedpassword = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.passwordEncrypted), key,
            {
                keySize: 128 / 8,
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });

            $scope.passwordEncrypted = encryptedpassword;

            var tempUserName = '' + $scope.userNameEncrypted + '';
            var tempPassword = '' + $scope.passwordEncrypted + '';

            $scope.tempResetPasswordModel.NewPassword = tempUserName;
            $scope.tempResetPasswordModel.ConfirmPassword = tempPassword;
            //$scope.registerCandidateRequest.confirmPassword = tempPassword;

            /********************************************** Ecrypt Password ****************************************/

            var promise = XSeedApiFactory.resetPassword($scope.tempResetPasswordModel);
            promise.then(
              function (response) {
                  blockUI.stop();

                  XSeedAlert.swal({
                      title: 'Success!',
                      text: 'Your password has been reset successfully!',
                      type: "success",
                      customClass: "xseed-error-alert",
                  }).then(function () {
                      $scope.resetPasswordModel = {};
                      $location.search({});
                      $timeout(function () {
                          $location.path('login');
                      }, 0);
                  }, function (dismiss) {
                  });
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerPopup('Account', 'Reset password link is expired or not valid', 'Unsuccessful!', 'info');
              });
        };
    }

    $scope.toggleLoginRegistration = function (pageId) {
        $scope.toggleLoginRegistrationFlag = pageId;
    }

    function setOrRemoveCookieStayLoggedIn(userName) {
        if ($scope.loginRequest.stayLoggedIn) {
            var expireDate = new Date();
            expireDate.setDate(expireDate.getDate() + 15);
            $cookies.put('userName', userName, { 'expires': expireDate });
        }
        else {
            $cookies.remove('userName');
        }
    };

    function getCookieStayLoggedIn() {
        $scope.loginRequest.userName = $cookies.get('userName');
        if ($scope.loginRequest.userName) {
            $scope.loginRequest.stayLoggedIn = true;
        }
    };
};
