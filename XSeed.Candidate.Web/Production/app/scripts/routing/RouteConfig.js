/// <reference path="../controllers/companyController.js" />
/// <reference path="../controllers/companyController.js" />

//'use strict';

XSEED_CANDIDATE_APP.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.caseInsensitiveMatch = true;
        $routeProvider
            .when('/', {
                //action: "landing"
                action: "landing.candidateLogin"
            })
            //.when('/home', {
            //    action: "standard.home",
            //    pageTitle: "Dashboard"
            //})
            .when('/candidate', {
                action: "standard.candidatesMain"
            })
            .when('/candidateLogin', {
                action: "landing.candidateLogin"
            })
            .when('/verifyEmail', {
                action: "landing.verifyEmail"
            })
            //.when('/candidateRegistration', {
            //    action: "landing.candidateRegistration"
            //})
            .when('/changePassword', {
                action: "standard.changePassword",
                pageTitle: "Change Password"
            })
            .when('/forgotPassword', {
                action: "landing.forgotPassword",
            })
            .when('/resetPassword', {
                action: "landing.resetPassword",
            })
            .when('/resendEmailVerification', {
                action: "landing.resendEmailVerification",
            })
            .when('/associate', {
                action: "landing.associate",
            })
            .when('/candidateProfile', {
                action: "standard.candidate.candidateProfile",
                pageTitle: "Profile"
            })
            .when('/editProfile', {
                action: "standard.candidate.editProfile",
                pageTitle: "Edit Profile"
            })
            .when('/opening', {
                action: "standard.opening"
            })
            .when('/openings', {
                action: "standard.opening.openings",
                pageTitle: "Openings"
            })
            .when('/jobDetail', {
                action: "standard.opening.jobDetail",
                pageTitle: "Job Details"
            })
            .when('/applicationStatus', {
                action: "standard.opening.applicationStatus",
                pageTitle: "Application Status"
            })
            .when('/switchView', {
                action: "standard.switchView"
            })
            .otherwise({
                redirectTo: '/'
                //action: "landing.login"

            });
    }
]);
