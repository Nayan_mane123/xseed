angular.module('services-config', [])
  .constant('configuration', {
      //Dev-
      //GET_METADATA_API_URL: 'data/MetaData.json',
      //XSEED_API_URL: 'http://10.33.2.176:8100/API/Dev/api/',
      //XSEED_SOCIAL_LOGIN_URL: 'http://10.33.2.176:8100/API/Dev/api/Account/ExternalLogin',
      //XSEED_SOCIAL_LOGIN_REDIRECTURI: '/candidateweb/dev/authComplete.html',

      ///*Constants*/
      //XSEED_NO_IMAGE_PATH: 'http://10.33.2.176:8100/web/dev/app/images/noImage.jpg',
      //XSEED_HOME_LINK: 'http://10.33.2.176:8100/web/dev/app/#/',
      ///*Constants*/

      //TIMEOUT_UI_IN_SEC: '1200000'

      ////Staging-
      //GET_METADATA_API_URL: 'data/MetaData.json',
      //XSEED_API_URL: 'http://10.33.2.176:81/API/Staging/api/',
      //XSEED_SOCIAL_LOGIN_URL: 'http://10.33.2.176:81/api/Account/ExternalLogin',
      //XSEED_SOCIAL_LOGIN_REDIRECTURI: '/app/authComplete.html',

      ///*Constants*/
      //XSEED_NO_IMAGE_PATH: 'http://10.33.2.176:81/web/Staging/app/images/noImage.jpg',
      //XSEED_HOME_LINK: 'http://10.33.2.176:81/web/Staging/app/#/',
      ///*Constants*/

      //TIMEOUT_UI_IN_SEC: '1200'



      //Local-
      GET_METADATA_API_URL: 'data/MetaData.json',
      XSEED_API_URL: 'http://localhost:51655/api/',
      XSEED_SOCIAL_LOGIN_URL: 'http://localhost:51655/api/Account/ExternalLogin',
      XSEED_SOCIAL_LOGIN_REDIRECTURI: '/app/authComplete.html',

      /*Constants*/
      XSEED_NO_IMAGE_PATH: 'http://localhost:63087/app/images/noImage.png',
      XSEED_CANDIDATE_TOPJOBS_COUNT: 50,
      XSEED_CANDIDATE_VIDEORESUME_SIZE: 20000000,
      XSEED_CANDIDATE_MATCHINGJOBS_COUNT: 50,
      //XSEED_HOME_LINK: 'http://localhost:63086/app/#/',
      /*Constants*/

      // Sample Video Resume Link      
      XSEED_SAMPLE_RESUME_VIDEO_PATH: 'http://localhost:63087/app/images/abcd.mp4',

      TIMEOUT_UI_IN_SEC: '120000'

  })
    .run(function ($rootScope, configuration) {
        $rootScope.configuration = configuration;
    });