'use strict';


XSEED_CANDIDATE_APP.factory('ModalFactory', ModalFactory);
/**
 * @ngInject
 */
function ModalFactory ($rootScope, $modal, configuration, $translate) {
    var xseedModals = {};


    xseedModals.getModalInstance = function(template, scope) {
        return $modal({
            animation: 'am-fade-and-scale',
            backdropAnimation: 'slide-bottom',
            templateUrl: template,
            show: false,
            backdrop: false,
            keyboard: false,
            scope: scope
        });
    };

    var modals = [];

    $rootScope.$on('modal.show',function(e, $modal){
        // if modal is not already in list
        if(modals.indexOf($modal) === -1) {
            modals.push($modal);
        }
    });

    $rootScope.$on('modal.hide',function(e, $modal){
        var modalIndex = modals.indexOf($modal);
        modals.splice(modalIndex, 1);
    });


    xseedModals.hideAllOpenedModals = function () {
        if(modals.length) {
            angular.forEach(modals, function($modal) {
                $modal.$promise.then($modal.hide);
            });
            modals = [];
        }
    };

    xseedModals.addressSuggestionsModal = function (scope) {
        return this.getModalInstance('views/modals/addressSuggestions.html',scope);
    };

    xseedModals.timeoutWarningModal = function (scope) {
        return this.getModalInstance('views/templates/timeOutWarningModal.html', scope);
    };

    xseedModals.timeoutModal = function (scope) {
        return this.getModalInstance('views/templates/timedOutModal.html', scope);
    };

    xseedModals.candidateSubmissionModal = function (scope) {
        return this.getModalInstance('views/candidate/submissionModal.html', scope);
    };
     
    xseedModals.requirementMoreModal = function (scope) {
        return this.getModalInstance('views/requirement/requirementMoreModal.html', scope);
    };

    xseedModals.companyContactModal = function (scope) {
        return this.getModalInstance('views/company/companyContactModal.html', scope);
    };
    return xseedModals;

};
