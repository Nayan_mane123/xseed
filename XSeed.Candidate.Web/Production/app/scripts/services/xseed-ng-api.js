function XSeedRequest() {

    this.AddVehicleRequest = function () {
        var addVehicleRequest = {
            "vin": "",
            "nickName": "",
            "whereAddedFrom": "string"

        }
        return (addVehicleRequest);
    };

    this.ProductRequest = function () {
        var addProductRequest = { "Id": 0, "Name": "", "Category": "", "Price": 0 };
        return (addProductRequest);

    }

    this.RegisterRequest = function () {
        var registerRequest = {
            "firstName": "", "lastName": "", "userName": "", "password": "", "confirmPassword": "", "organizationName": "",
            "website": "", "logo": "", "countryId": "", "address1": "",
            "address2": "", "address3": "", "phoneNumber": "", "linkedInURL": "",
            "twitterURL": ""
        };
        return (registerRequest);
    }

    this.LoginRequest = function () {
        var loginRequest = { "userName": "", "password": "", "grant_type": "password" };
        return (loginRequest);
    }

    this.CompanyRequest = function () {
        var createCompanyRequest = {
            "OrganizationId": "",
            "Name": "", "Description": "", "Website": "", "CareerPageURL": "",
            "LinkedInURL": "", "TwitterURL": "", "FacebookURL": "", "GooglePlusURL": "", "Size": "",
            "Phone": "", "Fax": "", "Address1": "", "Address2": "",
            "Address3": "", "CountryId": "", "StateId": "", "CityId": "", "Zip": "",
            "CompanySourceId": "", "Via": "", "ViaWebsite": "", "IndustryTypes": "", "ProfileImageFile": "",
            "ProfileImage": "",

            "Id": ""
        };
        return (createCompanyRequest);
    }


    this.CompanyContactRequest = function () {
        var createCompanyContactRequest = {
            "CompanyId": "", "TitleId": "", "FirstName": "", "MiddleName": "", "LastName": "", "BirthDate": "", "AnniversaryDate": "", "ProfileImagePath": "", "ReportingTo": "", "Designation": "", "PracticeLine": "", "PrimaryEmail": "", "SecondaryEmail": "", "Mobile": "", "Phone": "", "Fax": "", "Address1": "", "Address2": "", "Address3": "", "CountryId": "", "StateId": "", "CityId": "", "Zip": ""
        };
        return (createCompanyContactRequest);
    }

    this.JobRequest = function () {
        var jobRequest = {
            "JobTitleId": "", "CompanyId": "", "OrganizationUserIdList": [],
            "OrganizationUser": "",
            "ClientJobCode": "", "CompanyContactId": "", "JobDescription": "",
            "Priority": "", "RequisitionId": "", "InterviewType": "",
            "DriveFromDate": "", "DriveToDate": "", "JobPostedDate": "", "JobClosedDate": "",
            "JobExpiryDate": "", "JobTypeId": "", "CountryId": "", "StateId": "", "CityId": "",
            "Location": "", "TotalPositions": "", "JobStatusId": "",
            "DegreeList": "", "SkillList": "", "ExperienceInYear": "", "ExperienceInMonth": ""
        };
        return (jobRequest);
    }

    this.CandidateRequest = function () {
        var candidateRequest = {
            "UserId": "",
            "TitleId": "", "FirstName": "", "MiddleName": "", "LastName": "",
            "BirthDate": "", "ProfileImage": "", "ProfileImageFile": "",
            "GenderId": "", "MaritalStatusId": "", "PrimaryEmail": "", "SecondaryEmail": "",
            "Mobile": "", "Phone": "", "Fax": "", "Address1": "", "Address2": "",
            "Address3": "", "CountryId": "", "StateId": "", "CityId": "",
            "Zip": "", "HavePassport": "", "PassportValidUpto": "", "VisaTypeId": "",
            "VisaValidUpto": "", "SendJobAlert": "", "IsExperienced": "", "DegreeList": "",
            "SkillList": "",

            "logoImagePath": "", "FullName": "", "SkillIdList": "", "DegreeIdList": "",
            "Gender": "", "MaritalStatus": ""
        };
        return (candidateRequest);
    }


    this.CandidateSubmissionRequest = function () {
        var candidateRequest = {
            "CandidateId": "", "CompanyId": "", "JobId": ""
        };
        return (candidateRequest);
    }



}






var API = new XSeedRequest();

/**********************************************************************************************/

'use strict';

XSEED_CANDIDATE_APP.service('XSeedHTTPHeaderBuilder', XSeedHTTPHeaderBuilder);
/**
 * @ngInject
 */
function XSeedHTTPHeaderBuilder(configuration, $rootScope) {

    /*-----------------------------------------------------------------------------------*/
    // Build Token Service Headers
    function getHeadersForAccessToken() {
        var headers = {
            'Content-Type': 'application/json'
            , 'Accept': 'application/json'
            , 'UserId': $rootScope.userDetails.UserId
            , 'Token': $rootScope.userDetails.Token
            //, 'Authorization': 'Basic' + ' ' + $rootScope.userDetails.Token
            //, 'Authorization': 'Bearer' + ' ' + $rootScope.token.key
            //, 'Authorization': 'Basic' + ' ' + 'f6d6ccdc-36db-4acf-9307-7da9e4102936'            
            //'CV-AppType': 'WEB'
            //'CV-ApiKey': configuration.API_KEY
        };
        return headers;
    };

    function getHeadersForFileUpload() {
        var headers = {
            'Content-Type': undefined,
            'Accept': 'application/json'

            //'CV-AppType': 'WEB'
            //'CV-ApiKey': configuration.API_KEY
        };
        return headers;
    };
    /*-----------------------------------------------------------------------------------*/
    // Build MOBILE Api Headers
    function getHeaders() {
        var headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
            //'CV-AppType': 'WEB'
            //'CV-ApiKey': configuration.API_KEY,

            //'Authorization': 'Bearer' + ' ' + accessToken
            , 'Authorization': 'Bearer' + ' ' + $rootScope.token.key

        };
        return headers;
    };

    function getPDFHeaders() {
        var headers = {
            'Content-Type': 'application/pdf',
            'Accept': 'application/pdf',
            //'CV-AppType': 'WEB'
            //'CV-ApiKey': configuration.API_KEY,

            //'Authorization': 'Bearer' + ' ' + accessToken
            'Authorization': 'Bearer' + ' ' + $rootScope.token.key
        };
        return headers;
    };

    var getDeleteHeaders = function () {
        var headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            //'CV-AppType': 'WEB',
            'X-Http-Method-Override': 'DELETE'
            //'CV-ApiKey': services.configuration.API_KEY,

            //,'Authorization': 'Bearer' + ' ' + accessToken
            , 'Authorization': 'Bearer' + ' ' + $rootScope.token.key
        };
        return headers;
    };

    /*-----------------------------------------------------------------------------------*/

    return ({
        getHeadersForAccessToken: getHeadersForAccessToken,
        getHeaders: getHeaders,
        getPDFHeaders: getPDFHeaders,
        getDeleteHeaders: getDeleteHeaders,
        getHeadersForFileUpload: getHeadersForFileUpload,        
    });
}

XSeedHTTPHeaderBuilder.$inject = ["configuration", "$rootScope"];


/**********************************************************************************************/

'use strict';

XSEED_CANDIDATE_APP.service('XSeedApi', XSeedApi);
/**
 * @ngInject
 */
function XSeedApi($http, configuration) {

    function GET(url, headers) {
        return $http({
            method: 'GET',
            url: url,
            dataType: "json",
            headers: headers,
            withCredentials: true,
            transformResponse: $http.defaults.transformResponse.concat([
                function (data, headersGetter) {

                    return data;
                }
            ])
        })
    };

    function GetPDF(url, headers) {
        return $http({
            method: 'GET',
            url: url,
            headers: headers,
            responseType: 'arraybuffer'
        })
    };

    function POST(url, headers, request) {
        return $http({
            method: 'POST',
            url: url,
            dataType: "json",
            headers: headers,
            withCredentials: true,
            transformResponse: $http.defaults.transformResponse.concat([
                function (data, headersGetter) {
                    return data;
                }
            ]),
            data: request
        })
    };

    function PUT(url, headers, request) {
        return $http({
            method: 'PUT',
            url: url,
            dataType: "json",
            headers: headers,
            withCredentials: true,
            transformResponse: $http.defaults.transformResponse.concat([
                function (data, headersGetter) {
                    return data;
                }
            ]),
            data: request
        })
    };

    function DELETE(url, headers) {
        return $http({
            method: 'DELETE',
            url: url,
            dataType: "json",
            headers: headers,
            withCredentials: true,
            transformResponse: $http.defaults.transformResponse.concat([
                function (data, headersGetter) {
                    return data;
                }
            ])
        })
    };

    return ({
        GET: GET,
        GetPDF: GetPDF,
        POST: POST,
        PUT: PUT,
        DELETE: DELETE
    });
}

XSeedApi.$inject = ["$http", "configuration"];

/**********************************************************************************************/

'use strict';

XSEED_CANDIDATE_APP.service('XSeedExceptionHandler', XSeedExceptionHandler);
/**
 * @ngInject
 */
function XSeedExceptionHandler($rootScope, $http, $translate, $timeout, $log) {

    var handler = {};
    handler.ExceptionMessage = {};
    handler.handlerHTTPException = function (apiName, status, responseMessage) {

        this.setErrorMessage(apiName, status, responseMessage);
        this.broadcastError();
    };

    handler.handleValidationException = function (type, val) {

        handler.ExceptionMessage.location = '/badRequest';
        this.broadcastError();
    };

    handler.handleNotSupportException = function (errorMessage, status) {

        handler.ExceptionMessage.location = '/notSupported';
        this.broadcastError();

    };

    handler.setErrorMessage = function (apiName, status, responseMessage) {
        handler.ExceptionMessage.message = "There was an error processing your request ( Error code : " +
            responseMessage.data.code + ", Error :" + responseMessage.data.message + " )";
    };

    handler.broadcastError = function () {
        $timeout(function () {
            $rootScope.$broadcast('handleError');
        }, 1000);
    };
    return handler;
}

XSeedExceptionHandler.$inject = ["$rootScope", "$http", "$translate", "$timeout", "$log"];


/**********************************************************************************************/

'use strict';

XSEED_CANDIDATE_APP.factory('XSeedApiFactory', XSeedApiFactory);

function XSeedApiFactory($q, _, $log, XSeedApi, XSeedHTTPHeaderBuilder, $rootScope) {



    var services = {};

    services.configuration = {};

    services.setConfiguration = function (config) {
        services.configuration = config;
    }


    /*-----------------------------------------------------------------------------------*/
    services.getTokenInformation = function (requestParam) {
        var url = services.configuration.GET_TOKEN_INFO_API_URL + requestParam;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.updateVehicle = function (token, accountId, vin, request) {
        var url = services.configuration.ACCOUNT_API_URL + accountId + '/vehicles/' + vin;

        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeaders(), request)
            ]
        );
        return promise;

    };
    /*-----------------------------------------------------------------------------------*/
    services.deletePaymentMethodToAccount = function (token, accountId, paymentMethodId) {
        var url = services.configuration.ACCOUNT_API_URL + accountId + '/paymentMethods/' + paymentMethodId;

        var promise = $q.all(
            [
              XSeedApi.DELETE(url, XSeedHTTPHeaderBuilder.getHeaders())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/





    //Added  
    services.getMetaData = function () {
        var url = services.configuration.GET_METADATA_API_URL;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    //Register
    services.register = function (request) {
        var url = services.configuration.XSEED_API_URL + 'Account/Register';

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    //Register Candidate
    services.registerCandidate = function (request) {
        var url = services.configuration.XSEED_API_URL + 'Account/RegisterCandidate';

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/
        
    //Merge Accounts - XSeed Application and Social Media Log In
    services.MergeAccounts = function (request) {
        var url = services.configuration.XSEED_API_URL + 'Account/MergeAccounts';

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    /****************************************************************************/

    //Login
    services.login = function (request, username, password) {

        //var url = services.configuration.XSEED_LOGIN_API_URL;
        var url = services.configuration.XSEED_API_URL + 'Account/CandidateLogin?username=' + username + '&password=' + password;

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    //Login
    services.socialLogin1 = function (request, uniqueId, provider) {

        //var url = services.configuration.XSEED_LOGIN_API_URL;
        var url = services.configuration.XSEED_API_URL + 'Account/SocialLogin?uniqueId=' + uniqueId + '&provider=' + provider;

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/


    //Login
    services.socialLogin = function (provider, redirectUri) {

        var url = services.configuration.XSEED_API_URL + "/Account/ExternalLogin?provider=" + provider + "&response_type=token&client_id=self&redirect_uri=" + redirectUri;
        var headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/x-www-form-urlencoded'
        };
        var promise = $q.all(
            [
              XSeedApi.GET(url, headers)
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.getUserDetails = function (userName) {

        var url = services.configuration.XSEED_API_URL + "OrganizationUser/Get?userName=" + userName;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    //Company module
    services.getCompanyList = function (id) {

        var url = services.configuration.XSEED_API_URL + "Company/Get?organizationId=" + id;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.createCompany = function (request) {

        var url = services.configuration.XSEED_API_URL + "Company/Post";
        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request),
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.createCompanySource = function (companySources, organizationId) {

        var companySourceUrl = services.configuration.XSEED_API_URL + "Company/PostCompanySource/" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.POST(companySourceUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), companySources)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getCompanyDetail = function (organizationId, companyId) {

        var url = services.configuration.XSEED_API_URL + "Company/Get/" + companyId +
            "?organizationId=" + organizationId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getContactDetail = function (contactId, companyId) {

        var url = services.configuration.XSEED_API_URL + "CompanyContact/Get/" + contactId + "?companyId=" + companyId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.editCompany = function (request) {

        var url = services.configuration.XSEED_API_URL + "Company/Put";
        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/


    services.getCompanyContacts = function (companyId) {

        var url = services.configuration.XSEED_API_URL + "CompanyContact/Get?companyId=" + companyId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.createContact = function (request) {
        var url = services.configuration.XSEED_API_URL + "CompanyContact/Post";
        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    services.updateContact = function (request) {
        var url = services.configuration.XSEED_API_URL + "CompanyContact/Put";
        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getReportingAuthorities = function (organizationId) {

        var url = services.configuration.XSEED_API_URL + "LookUp/GetReportingAuthorities?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getReportingAuthoritiesByRole = function (organizationId, roleId) {

        var url = services.configuration.XSEED_API_URL + "LookUp/GetReportingAuthorities?organizationId=" + organizationId + "&roleId=" + roleId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getOrganizationUsers = function (organizationId) {

        var getOrganizationUsersurl = services.configuration.XSEED_API_URL + "OrganizationUser/Get?organizationId=" + organizationId;
        var getUserReportingToAuthurl = services.configuration.XSEED_API_URL + "LookUp/GetReportingAuthorities?organizationId=" + organizationId;
        var getTitleurl = services.configuration.XSEED_API_URL + "LookUp/GetTitles";
        var getUserRolesurl = services.configuration.XSEED_API_URL + "LookUp/GetUserRoles?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(getOrganizationUsersurl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getUserReportingToAuthurl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getTitleurl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getUserRolesurl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getOrganizationUserDetails = function (email, organizationId, userId, roleId) {

        var getUserDetailUrl = services.configuration.XSEED_API_URL + "OrganizationUser/Get?organizationId=" + organizationId + "&Id=" + userId;
        var getUserReportingToAuthurl = services.configuration.XSEED_API_URL + "LookUp/GetReportingAuthorities?organizationId=" + organizationId + "&roleId=" + roleId;

        var promise = $q.all(
            [
              XSeedApi.GET(getUserDetailUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getUserReportingToAuthurl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.createNewOrganizationUser = function (request) {

        var url = services.configuration.XSEED_API_URL + "OrganizationUser/Post";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.forgotPassword = function (request) {

        var url = services.configuration.XSEED_API_URL + "Account/ForgotPasswordCandidate";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.resetPassword = function (request) {

        var url = services.configuration.XSEED_API_URL + "Account/SetPasswordCandidate";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.verifyEmail = function (userId, code) {

        var url = services.configuration.XSEED_API_URL + "Account/VerifyCandidateEmail?userId=" + userId + "&code=" + code;

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.resendEmailVerification = function (userName) {

        var url = services.configuration.XSEED_API_URL + "Account/ResendCandidateVerificationEmail?UserName=" + userName;

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.changePassword = function (request) {

        var url = services.configuration.XSEED_API_URL + "Account/ChangeCandidatePassword";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/
    services.logout = function () {       
        var url = services.configuration.XSEED_API_URL + "Account/Logout";

        var headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };

        var promise = $q.all(
            [
              XSeedApi.POST(url, headers)              
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.saveEditOrgUserDetails = function (request) {

        var url = services.configuration.XSEED_API_URL + "OrganizationUser/Put";
        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    /*-----------------------------------------------------------------------------------*/

    services.saveUserEmailConfiguration = function (request) {

        var url = services.configuration.XSEED_API_URL + "User/SaveUserEmailConfiguration";
        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.editUserEmailConfiguration = function (request) {

        var url = services.configuration.XSEED_API_URL + "User/UpdateUserEmailConfiguration";
        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    services.getUserEmailConfiguration = function (request) {

        var url = services.configuration.XSEED_API_URL + "User/GetUserEmailConfiguration/" + request;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getProductList = function () {

        var url = services.configuration.XSEED_API_URL + "Getlist";
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getProduct = function (id) {

        var url = services.configuration.XSEED_API_URL + "Getproduct/" + id;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.createProduct = function (request) {

        var url = services.configuration.XSEED_API_URL + 'InsertProduct';
        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.updateProduct = function (request) {

        var url = services.configuration.XSEED_API_URL + 'UpdateProduct';
        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.deleteProduct = function (id) {

        var url = services.configuration.XSEED_API_URL + 'DeleteProduct/' + id;
        var promise = $q.all(
            [
              XSeedApi.DELETE(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/



    // Download Agreement PDF
    services.getAgreementPDF = function (token, accountId, vin) {
        var url = services.configuration.ACCOUNT_API_URL + accountId + '/vehicles/' + vin + '/subscriptions';
        var promise = $q.all(
            [
              XSeedApi.GetPDF(url, XSeedHTTPHeaderBuilder.getPDFHeaders(token))
            ]
        );
        return promise;
    };



    /*---------------------------------------Orgnaization API Service--------------------------------------------*/

    services.getOrganizationDetails = function (organizationId) {

        var url = services.configuration.XSEED_API_URL + "Organization/Get?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.updateOrganization = function (frmUpdateOrganization) {
        var url = services.configuration.XSEED_API_URL + "Organization/Put";
        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), frmUpdateOrganization)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    /*---------------------------------Job/Requirements Start--------------------------------------------------*/
    services.getJobOpenings = function (candidateId, recordCount) {
        recordCount = recordCount == undefined ? 100 : recordCount;
        var url = services.configuration.XSEED_API_URL + "CandidateUser/ListJobOpenings?candidateId=" + candidateId + "&recordCount=" + recordCount;
        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), null)
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/
    services.getDefaultJobOpenings = function (recordCount) {
        var url = services.configuration.XSEED_API_URL + "CandidateUser/ListJobOpenings?recordCount=" + recordCount;
        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), null)
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/
    services.searchJobOpenings = function (request) {
        var url = services.configuration.XSEED_API_URL + "CandidateUser/SearchJobOpenings";
        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/
    services.getJobDetail = function (Id) {
        var url = services.configuration.XSEED_API_URL + "CandidateUser/GetJobDetail/" + Id;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/
    services.applyToSelectedJob = function (request, Id) {
        var url = services.configuration.XSEED_API_URL + "CandidateUser/Apply/" + Id;
        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/
    services.getAppliedJobStatus = function (userId) {
        var url = services.configuration.XSEED_API_URL + "CandidateUser/GetCandidateJobApplications/" + userId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/
    //Job Lookup Call
    services.jobPagePrimaryLookupCall = function () {
        var getJobTitleUrl = services.configuration.XSEED_API_URL + "LookUp/GetJobTitles";
        var getJobTypeUrl = services.configuration.XSEED_API_URL + "LookUp/GetJobTypes";
        var getJobStatusUrl = services.configuration.XSEED_API_URL + "LookUp/GetJobStatus";
        var getDegreeUrl = services.configuration.XSEED_API_URL + "LookUp/GetDegrees";
        var getSkillUrl = services.configuration.XSEED_API_URL + "LookUp/GetSkills";

        var promise = $q.all(
            [
              XSeedApi.GET(getJobTitleUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getJobTypeUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getJobStatusUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getDegreeUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getSkillUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.jobPageSecondaryLookupCall = function (organizationId) {
        var getCompanyUrl = services.configuration.XSEED_API_URL + "LookUp/GetCompanies?organizationId=" + organizationId;
        var getOrganizationUserUrl = services.configuration.XSEED_API_URL + "LookUp/GetOrganizationUsers?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(getCompanyUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getOrganizationUserUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.createJob = function (request) {
        var url = services.configuration.XSEED_API_URL + "Job/Post";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.editJob = function (request) {
        var url = services.configuration.XSEED_API_URL + "Job/Put";

        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    //services.getJobDetail = function (requirementId, organizationId) {
    //    var url = services.configuration.XSEED_API_URL + "Job/Get/" + requirementId + "?organizationId=" + organizationId;
    //    var getAssociatedCandidateListUrl = services.configuration.XSEED_API_URL + "Job/GetAssociatedCandidateList?jobId=" + requirementId;

    //    var promise = $q.all(
    //        [
    //          XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
    //          XSeedApi.GET(getAssociatedCandidateListUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
    //        ]
    //    );
    //    return promise;
    //};

    /*-----------------------------------------------------------------------------------*/

    services.getAssociatedCandidateList = function (requirementId) {
        var getAssociatedCandidateListUrl = services.configuration.XSEED_API_URL + "Job/GetAssociatedCandidateList?jobId=" + requirementId;

        var promise = $q.all(
            [
              XSeedApi.GET(getAssociatedCandidateListUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.getRequirementAdvanceSearchResult = function (organizationId, request) {
        var url = services.configuration.XSEED_API_URL + "Search/SearchRequirements/" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.getCompanyContactsLookup = function (companyId) {
        var url = services.configuration.XSEED_API_URL + "LookUp/GetCompanyContacts?companyId=" + companyId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*---------------------------------Job/Requirements End--------------------------------------------------*/


    /*---------------------------------Candidate Start--------------------------------------------------*/
    //Candidate Lookup Call
    services.candidatePageLookupCall = function () {
        var getTitleUrl = services.configuration.XSEED_API_URL + "LookUp/GetTitles";
        var getMaritalStatusUrl = services.configuration.XSEED_API_URL + "LookUp/GetMaritalStatus";
        var getVisaTypeUrl = services.configuration.XSEED_API_URL + "LookUp/GetVisaTypes";
        //var getDegreeUrl = services.configuration.XSEED_API_URL + "LookUp/GetDegrees";
        var getSkillUrl = services.configuration.XSEED_API_URL + "LookUp/GetSkills";
        var getGenderUrl = services.configuration.XSEED_API_URL + "LookUp/GetGenders";
        var getLanguageUrl = services.configuration.XSEED_API_URL + "LookUp/GetLanguages";
        var getCandidateStatusUrl = services.configuration.XSEED_API_URL + "LookUp/GetCandidateStatus";

        var promise = $q.all(
            [
              XSeedApi.GET(getSkillUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getMaritalStatusUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getVisaTypeUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              //XSeedApi.GET(getDegreeUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getTitleUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getGenderUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getLanguageUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getCandidateStatusUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.candidateSkillLookupCall = function () {
        var getSkillUrl = services.configuration.XSEED_API_URL + "LookUp/GetSkills";
        var promise = $q.all(
            [
              XSeedApi.GET(getSkillUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getCandidateList = function () {
        var url = services.configuration.XSEED_API_URL + "Candidate/Get";
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.createCandidate = function (request) {
        var url = services.configuration.XSEED_API_URL + "Candidate/Post";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.editProfile = function (request) {
        var url = services.configuration.XSEED_API_URL + "CandidateUser/Put";

        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    //services.getCandidateDetail = function (id, isSocialMediaLogin, provider) {

    //    var url;
    //    if (isSocialMediaLogin)
    //    {
    //        url = services.configuration.XSEED_API_URL + "CandidateUser/Get?uniqueId=" + id + "&provider=" + provider;
    //    }
    //    else
    //    {
    //        url = services.configuration.XSEED_API_URL + "CandidateUser/Get/" + id;            
    //    }

    //    var promise = $q.all(
    //        [
    //          XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
    //        ]
    //    );
    //    return promise;
    //};
    ///*-----------------------------------------------------------------------------------*/

    services.getCandidateDetail = function (id) {

        var url;
        url = services.configuration.XSEED_API_URL + "CandidateUser/Get/" + id;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.submitCandidate = function (request) {
        var url = services.configuration.XSEED_API_URL + "Submission/Post";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.submissionPopupLookupCall = function (organizationId) {
        var getCompanyUrl = services.configuration.XSEED_API_URL + "LookUp/GetCompanies?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(getCompanyUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getRequirementListByCompany = function (companyId) {
        var getRequirementUrl = services.configuration.XSEED_API_URL + "Job/GetJobsByCompany?CompanyId=" + companyId;

        var promise = $q.all(
            [
              XSeedApi.GET(getRequirementUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.getCandidateAdvanceSearchResult = function (request) {
        var url = services.configuration.XSEED_API_URL + "Search/SearchCandidates/";
        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.getCandidateResumeParserCall = function (request) {
        var url = services.configuration.XSEED_API_URL + "CandidateUser/ParseResume/";
        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    /*---------------------------------Candidate End--------------------------------------------------*/

    /*---------------------------------Query Builder Start--------------------------------------------------*/

    services.getSkillList = function () {
        var getSkillUrl = services.configuration.XSEED_API_URL + "LookUp/GetSkills";

        var promise = $q.all(
            [
              XSeedApi.GET(getSkillUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.getBooleanQueries = function (organizationId) {

        var url = services.configuration.XSEED_API_URL + "BooleanSearch/Get?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.saveBooleanQueries = function (request) {

        var url = services.configuration.XSEED_API_URL + "BooleanSearch/Post";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    /*---------------------------------Query Builder End--------------------------------------------------*/


    /*---------------------------------Submission Start--------------------------------------------------*/
    services.getSubmissions = function (organizationId) {

        var getSubmissionsurl = services.configuration.XSEED_API_URL + "Submission/Get?organizationId=" + organizationId;
        var getSubmissionFeedbackStatusurl = services.configuration.XSEED_API_URL + "LookUp/GetSubmissionFeedbackStatus";
        var getClientFeedbackStatusurl = services.configuration.XSEED_API_URL + "LookUp/GetClientFeedbackStatus";
        var getCandidateFeedbackStatusurl = services.configuration.XSEED_API_URL + "LookUp/GetCandidateFeedbackStatus";

        var promise = $q.all(
            [
              XSeedApi.GET(getSubmissionsurl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getSubmissionFeedbackStatusurl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getClientFeedbackStatusurl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getCandidateFeedbackStatusurl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.saveFeedback = function (feedbackType, request) {
        var url;
        if (feedbackType == 'Submission Feedback') {
            url = services.configuration.XSEED_API_URL + "SubmissionStatus/Post";
        }
        else if (feedbackType == 'Candidate Feedback') {
            url = services.configuration.XSEED_API_URL + "CandidateFeedback/Post";
        }
        else if (feedbackType == 'Client Feedback') {
            url = services.configuration.XSEED_API_URL + "ClientFeedback/Post";
        }

        var promise = $q.all(
                [
                  XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
                ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/
    /*---------------------------------Submission End--------------------------------------------------*/



    /*---------------------------------Lookup Start--------------------------------------------------*/
    services.getStateListByCountry = function (countryId) {

        var url = services.configuration.XSEED_API_URL + "LookUp/GetStates?countryId=" + countryId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getCityListByState = function (stateId) {

        var url = services.configuration.XSEED_API_URL + "LookUp/GetCities?stateId=" + stateId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getCompanySourceList = function (organizationId) {

        var url = services.configuration.XSEED_API_URL + "LookUp/GetCompanySources/" + organizationId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.getCompanySourceListByCompanyId = function (companyId, organizationId) {

        var url = services.configuration.XSEED_API_URL + "Company/GetCompanySource/" + companyId + "?organizationId=" + organizationId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*---------------------------------Lookup End--------------------------------------------------*/



    /*---------------------------------Reports--------------------------------------------------*/

    services.getCandidateAddedReport = function (OrganizationId) {

        var url = services.configuration.XSEED_API_URL + "Report/getCandidateAddedReport?OrganizationId=" + OrganizationId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    services.getRequirementTrackerReport = function (OrganizationId) {

        var url = services.configuration.XSEED_API_URL + "Report/getRequirementTrackerReport?OrganizationId=" + OrganizationId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    services.getSubmissionTrackerReport = function (OrganizationId) {

        var url = services.configuration.XSEED_API_URL + "Report/getSubmissionTrackerReport?OrganizationId=" + OrganizationId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.getAdvanceSearchResultForReports = function (OrganizationId, request) {

        var url = services.configuration.XSEED_API_URL + "Search/SearchReports/" + OrganizationId;
        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*---------------------------------Reports--------------------------------------------------*/


    //----------------------------------UserRoles-----------------------------------------------

    services.getUserRoles = function (organizationId) {

        //var url = services.configuration.XSEED_API_URL + "User/GetAllUserRoles";
        var url = services.configuration.XSEED_API_URL + "User/GetAllUserRoles?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.createNewOrganizationUserRole = function (request) {

        var url = services.configuration.XSEED_API_URL + "User/CreateUserRole";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.deleteUserRole = function (id) {

        var url = services.configuration.XSEED_API_URL + 'User/DeleteUserRole?Id=' + id;
        var promise = $q.all(
            [
              XSeedApi.DELETE(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.updateUserRole = function (request) {

        var url = services.configuration.XSEED_API_URL + 'User/UpdateUserRole';

        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/


    //Company search
    //services.companySearch = function (search) {
    //    console.log('Search-->' + search);

    //    //var url = services.configuration.XSEED_API_URL + "Company/Get?organizationId=" + id;
    //    var url = "http://10.33.2.204:3000/api/companySearch/info";        

    //    var promise = $q.all(
    //        [
    //          XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
    //        ]
    //    );
    //    return promise;
    //};
    /*-----------------------------------------------------------------------------------*/

    return services;
}




XSeedApiFactory.$inject = ["$q", "_", "$log", "XSeedApi", "XSeedHTTPHeaderBuilder", "$rootScope"];

