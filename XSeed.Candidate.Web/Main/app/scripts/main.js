/**
 * Main scripts file
 */
(function($) {
  'use strict';
  /* Define some variables */
  var app = $('.app'),
   // searchState = false,
    menuState = false;

  function toggleMenu() {
    if (menuState) {
      app.removeClass('move-left move-right');
      setTimeout(function() {
        app.removeClass('offscreen');
      }, 150);
    } else {
      app.addClass('offscreen move-right');
    }
    menuState = !menuState;
  }

  /******** Open messages ********/

  /******** Open contact ********/


  /******** Toggle expanding menu ********/


  /******** Card collapse control ********/
 

  /******** Card refresh control ********/
 

  /******** Card remove control ********/

  ///******** Search form ********/


  ///******** Sidebar toggle menu ********/


  ///******** Sidebar menu ********/


  ///******** Demo only ********/
 

})(jQuery);
