        var specialKeys = new Array();
        specialKeys.push(8);
        specialKeys.push(32);
        specialKeys.push(46);

        function onlyCharacter(f) {
            var d = f.which ? f.which : f.keyCode;
            var e = ((d >= 65 && d <= 90) || (d >= 97 && d <= 122) || (d == 32) || specialKeys.indexOf(d) != -1);
            return e
        }

        function onlyNumber(a) {
            var c = a.which ? a.which : a.keyCode;
            var b = ((c >= 48 && c <= 57) || specialKeys.indexOf(c) != -1);
            return b
        }

        function charge(a) {
            var c = a.which ? a.which : a.keyCode;
            var b = ((c >= 48 && c <= 57) || specialKeys.indexOf(c) != -1 || specialKeys.keyCode == 46);
            return b
        }

        function isValidEmailAddress(c) {

            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(c).toLowerCase());

            // var d = new onlytextp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            // return d.test(c)
        }

        function validateEmail(mail, inputid, errorlblid, btnid) {

            if (!isValidEmailAddress(mail)) {
                document.getElementById(errorlblid).innerHTML = "Please enter valid email address";
                // document.getElementById(inputid).value = ""
                $('#' + btnid).prop('disabled', true);
            } else {
                document.getElementById(errorlblid).innerHTML = "";
                $('#' + btnid).prop('disabled', false);
            }

        }

        function checkcapson(a) {

           
                var input = a;
                var x=0;
                var text = document.getElementById("text");
                input.addEventListener("keyup", function (event) {

                    if (event.getModifierState("CapsLock")) {
                        
                        if (x == 0) {
                            x = 1;
                            text.style.display = "block";
                            //noty({
                            //    theme: 'app-noty',
                            //    text: 'Capslock is ON',
                            //    type: 'warning',
                            //    timeout: 3000,
                            //    layout: 'topRight',
                            //    closeWith: ['button', 'click'],
                            //    animation: {
                            //        open: 'animated fadeInDown',
                            //        close: 'animated fadeOutUp'
                            //    }
                            //});
                        }
                    } else {
                        text.style.display = "none";
                        x=0;
                    }
                });

        }

        function singlespace(a) {
            var input = a;
            input.addEventListener('keydown', function (e) {
                const val = input.value;
                var end = input.selectionEnd;
                if (e.keyCode == 32 && (val[end - 1] == " " || val[end] == " ")) {
                    e.preventDefault();
                }
            });

        }
        function nospacevalidation(a) {
            var input = a;
            input.addEventListener('keydown', function (e) {
                const val = input.value;
                var end = input.selectionEnd;
                if (e.keyCode == 32) {
                    e.preventDefault();
                }
            });

        }