/// <reference path="../controllers/companyController.js" />
/// <reference path="../controllers/companyController.js" />

//'use strict';

XSEED_CANDIDATE_APP.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.caseInsensitiveMatch = true;
        $routeProvider
            .when('/', {
                //action: "landing"
                action: "landing.candidateLogin",
            })
            //.when('/home', {
            //    action: "standard.home",
            //    pageTitle: "Dashboard"
            //})
            .when('/candidate', {
                action: "standard.candidatesMain"
            })
            .when('/candidateLogin', {
                action: "landing.candidateLogin",
                pageTitle: "Login"
            })
            //.when('/candidateRegistration', {
            //    action: "landing.candidateRegistration"
            //})
            .when('/changePassword', {
                action: "standard.changePassword",
                pageTitle: "Change Password",
                resolve: {
                    "check": function () {
                        toggleSidebar('out');
                    }
                }
            })
            .when('/forgotPassword', {
                action: "landing.forgotPassword",
                pageTitle: "Forget password"
            })
            .when('/resetPassword', {
                action: "landing.resetPassword",
                pageTitle: "Reset password"
            })
            .when('/resendEmailVerification', {
                action: "landing.resendEmailVerification",
                pageTitle: "Email varification"
            })
            .when('/associate', {
                action: "landing.associate",
                pageTitle: "Registration"
            })
            .when('/candidateProfile', {
                action: "standard.candidate.candidateProfile",
                pageTitle: "Profile",
                resolve: {
                    "check": function () {
                        toggleSidebar('out');
                    }
                }
            })
            .when('/editProfile', {
                action: "standard.candidate.editProfile",
                pageTitle: "Edit Profile",
                resolve: {
                    "check": function () {
                        toggleSidebar('out');
                    }
                }
            })
            .when('/opening', {
                action: "standard.opening"
            })
            .when('/openings', {
                action: "standard.opening.openings",
                pageTitle: "Openings",
                resolve: {
                    "check": function () {
                        toggleSidebar('out');
                    }
                }
            })
            .when('/jobDetail', {
                action: "standard.opening.jobDetail",
                pageTitle: "Job Details"
            })
            .when('/applicationStatus', {
                action: "standard.opening.applicationStatus",
                pageTitle: "Application Status",
                resolve: {
                    "check": function () {
                        toggleSidebar('out');
                    }
                }
            })
            .when('/switchView', {
                action: "standard.switchView"
            })
            .otherwise({
                redirectTo: '/'
                    //action: "landing.login"

            });
    }
]);