'use strict';

XSEED_CANDIDATE_APP.controller('ApplicationCtrl', ApplicationCtrl);

/**
 * @ngInject
 */
function ApplicationCtrl($scope, $rootScope, $route, $routeParams, RequestContext, $timeout, $q, $location, $window, configuration, i18nFactory, ExceptionHandler, EventBus, $log, _, ipCookie, blockUI, ModalFactory, XSeedAlert, localStorageService, $uibModal, XSeedApiFactory, ngTableParams, $filter, ValidationService) {
    $rootScope.token = {};
    $rootScope.userDetails = {};
    $rootScope.skillSet = {};
   

    $rootScope.metadata = {};
    $scope.filter = {};
    $scope.flag = { requirementLookupLoaded: false, candidateLookupLoaded: false };

    var vm = this;
    // Offcanvas and horizontal menu
    vm.menu = false;
    vm.toggleMenu = function() {
        vm.menu = !vm.menu;
    };

    $scope.lookup = {
        company: '',
        jobType: '',
        jobStatus: '',
        organizationUser: '', //Requirement
        skill: '',
        maritalStatus: '',
        visaType: '',
        degree: '',
        jobTitle: '',
        gender: '',
        requirement: '',
        skillListData: '',
        degreeListData: '' //Candidate
    };

    if (angular.isUndefined(localStorageService.get("token"))) {
        $rootScope.token = {};
    } else {
        $rootScope.token.key = localStorageService.get("token");
    }

    if (angular.isUndefined(localStorageService.get("userDetails")) || localStorageService.get("userDetails") == null) {
        $rootScope.userDetails = {};
    } else {
        $rootScope.userDetails = localStorageService.get("userDetails");
    }

    if (angular.isUndefined(localStorageService.get("externalAuthData")) || localStorageService.get("externalAuthData") == null) {
        $rootScope.externalAuthData = {};
    } else {
        $rootScope.externalAuthData = localStorageService.get("externalAuthData");
    }

    $scope.REGX_P = XSEED_HELPER.ValidationPatterns();
    XSeedApiFactory.setConfiguration(configuration);

    $scope.registerRequest = {};
    $scope.loginRequest = {};

    $scope.$watch('location.search()', function() {
        init();
    }, true);
    //Chat modal
    $scope.openChat = function() {
        var chatModalInstance = $uibModal.open({
            templateUrl: 'chatModal.html',
            controller: 'chatModalInstanceCtrl',
            controllerAs: 'chatmodal',
            windowClass: 'sidebar-modal chat-panel',
            backdropClass: 'chat-backdrop'
        });
    };

    //Initialization Function
    function init() {
        $scope.date = new Date();
        getMetaData();
        $scope.selectedTabIndex = 1;
        $scope.common = {};
        $scope.common.step = 'one';
    }




    function getMetaData() {
        blockUI.start();
        var promise = XSeedApiFactory.getMetaData();
        promise.then(
            function(response) {
                blockUI.stop();
                $rootScope.metadata = response[0].data;
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('AccessTokenRequest', null, httpError);
            });
    }

    $scope.createFunction = function(input) {
        // format the option and return it
        return {
            value: $scope.lookup.skillListData.length,
            label: input
        };
    };

    //currency options
    $scope.currencyOptions = {
        aSign: '$'
    };

    //for unauthorized access
    $scope.checkIsUserAuthorized = function() {
        if (!$rootScope.userDetails || !$rootScope.userDetails.Token) {
            $location.path('candidateLogin');
        }
    }

    $scope.getStateListByCountry = function(countryId) {
        var promise = XSeedApiFactory.getStateListByCountry(countryId);
        promise.then(
            function(response) {
                $scope.stateList = response[0].data;

            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
            });
    };

    $scope.getCityListByState = function(stateId) {
        var promise = XSeedApiFactory.getCityListByState(stateId);
        promise.then(
            function(response) {
                $scope.cityList = response[0].data;
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
            });
    };

    $scope.isUndefinedOrNull = function(val) {
        return angular.isUndefined(val) || val === null
    }


    $scope.GetDashboardCounts = function() {
        var userId = $rootScope.userDetails.UserId;
        blockUI.start();
        var promise = XSeedApiFactory.getDashoboardCounts(userId);
        promise.then(
            function(response) {
                $scope.AppliedCount = response[0].data.appliedJobCount;
                $scope.MatchingJobCount = response[0].data.matchingJobCount;
                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });
    };


    $scope.isEmpty = function(obj) {
        
      return (obj.length === 0);
    }

    $scope.onBlurValidFunc = function(val) {
        val.$onblurInvalid = val.$invalid;
    };

    $scope.onFocusFunc = function(val) {
        val.$onblurInvalid = false;
    };

    $scope.sessionTimeout = function(location) {
        $window.location.href = location;
    };

    function isRouteRedirect(route) {
        return (!route.current.action);
    }

    // Fires after Exception handler call
    $scope.$on('handleError', function() {
        var exceptionMessage = ExceptionHandler.ExceptionMessage;
        $scope.locationPath = exceptionMessage.location;
        $scope.ErrorMessage = exceptionMessage.message;
      
        showErrorModal();
    });

    // Fires after Exception handler call
    $scope.$on('handlePopup', function() {
        $scope.popupTitle = ExceptionHandler.ExceptionMessage.title;
        $scope.ErrorMessage = ExceptionHandler.ExceptionMessage.message;
        $scope.popupType = ExceptionHandler.ExceptionMessage.type;
        showPopup();
    });

    // Fires after Exception handler call
    $scope.$on('handleAccountException', function() {
        
        $scope.sessionTimeout("sessionTimeout.html");
    });

    function showPopup() {
   
        XSeedAlert.swal({
            title: $scope.popupTitle,
            text: $scope.ErrorMessage,
            type: $scope.popupType,
            customClass: 'xseed-error-alert',
            allowOutsideClick: false,
            allowEscapeKey: false
        });
    }

    function showErrorModal() {
 
        XSeedAlert.swal({
            title: i18nFactory.ERROR_TITLE,
            text: $scope.ErrorMessage,
            type: 'error',
            customClass: 'xseed-error-alert',
            allowOutsideClick: false,
            allowEscapeKey: false
        });
    }

    $scope.showNoty = function(msg) {
        var type = 'success';
        var position = 'topRight';
        noty({
            theme: 'app-noty',
            text: msg,
            type: type,
            timeout: 3000,
            layout: position,
            closeWith: ['button', 'click'],
            animation: {
                open: 'animated fadeInDown', // Animate.css class names
                close: 'animated fadeOutUp', // Animate.css class names
            }
        });
    };

    $scope.hideErrorModal = function() {
        $location.$$search = {};
    };

    $scope.UserIdlestarted = false;
    var timeoutWarningModal = ModalFactory.timeoutWarningModal($scope);
    var timeoutModal = ModalFactory.timeoutModal($scope);

    function closeTimeoutModals() {
        timeoutWarningModal.$promise.then(timeoutWarningModal.hide);
        timeoutModal.$promise.then(timeoutModal.hide);
    }

    $scope.$on('IdleStart', function() {
        closeTimeoutModals();
        timeoutWarningModal.$promise.then(timeoutWarningModal.show);
    });

    $scope.$on('IdleEnd', function() {
        closeTimeoutModals();
        $log.warn('Idle time ending');
    });

    $scope.$on('IdleTimeout', function() {
        closeTimeoutModals();
        ModalFactory.hideAllOpenedModals();
        $scope.sessionTimeout("sessionTimeout.html");
 
    });

    /******************************************Initialize Candidate Ratings ************************************/

    $scope.initializeCandidateRatings = function() {
            setTimeout(function() {
                var $input = $('input.rating');
                if ($input.length) {
                    $input.removeClass('rating-loading').addClass('rating-loading').rating();
                }
            }, 500);
        }
        //---------------------------------------------------------------------------//

    // Get the render context local to this controller (and relevant params).

    var renderContext = RequestContext.getRenderContext();

    // The subview indicates which view is going to be rendered on the page.

    $scope.subview = renderContext.getNextSection();

    $scope.$on("requestContextChanged",

        function() {

            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {


                return;
            }
            // Update the view that is being rendered.

            $scope.subview = renderContext.getNextSection();
        }
    );

    // Listen for route changes so that we can trigger request-context change events.
    $scope.$on("$routeChangeSuccess", function(event) {
        // If this is a redirect directive, then there's no taction to be taken.

        if (isRouteRedirect($route)) {
            return;
        }

        $scope.currentLocation = $route.current.action;


        $scope.pageTitle = $route.current.pageTitle;
        // Update the current request action change.

        RequestContext.setContext($route.current.action, $routeParams);

        // Announce the change in render conditions.
        $scope.$broadcast("requestContextChanged", RequestContext);

    });

    //---------------------------------------------------------------------------//


    //---------------------------------------------------------------------------//

    $rootScope.$on("$includeContentLoaded", function(event, templateName) {

        $('[data-toggle="popover"]').popover();
        $('[data-toggle="tooltip"]').tooltip({
            trigger: 'hover'
        })
        $("select.select2").select2({ allowClear: true });
        $(".dropdown-toggle").dropdown();

        $(".nav-tabs .nav-item  a").click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });
    });




    $scope.ChangeAppClass = function() {

        $('.app').removeClass('expanding fixed-header');
        $('.app').addClass('no-padding no-footer layout-static');

    }

    $scope.InializePopoverAndTooltip = function() {

        $('[data-toggle="popover"]').popover();
        $('[data-toggle="tooltip"]').tooltip({
            trigger: 'hover'
        })
        $('.dropdown-toggle').dropdown();
    }


    $scope.CallSelect2 = function() {
        setTimeout(function() {
            $("select.select2").select2({ allowClear: true });
        }, 50);

    }
    $scope.InitDragScroll = function() {
        StartDragScroll();
    }




}