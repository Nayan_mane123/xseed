'use strict';

XSEED_CANDIDATE_APP.controller('OpeningsCtrl', OpeningsCtrl);

/**
 * @ngInject
 */
function OpeningsCtrl($scope, $rootScope, $route, $routeParams, RequestContext, $timeout, $q, $location, $window, configuration, i18nFactory, ExceptionHandler, EventBus, $log, _, ipCookie, blockUI, ModalFactory, XSeedAlert, localStorageService, $uibModal, XSeedApiFactory, ngTableParams, $filter, ValidationService) {

    var RequirementDetailsModal = ModalFactory.RequirementDetailsModal($scope);
    init();

  function init() {
        var applicationStausPath = '/applicationStatus';
        $scope.requirementModel = {};
        $scope.candidateRating = 1.5;
        if (($scope.isUndefinedOrNull($scope.requirementModel._id) || $scope.isEmpty($scope.requirementModel._id)) && $location.path().toLowerCase() != applicationStausPath.toLowerCase()) {
            $location.path('openings');
        }
        $scope.requirementFilter = {};
        $scope.searchModel = {};
        $scope.appliedJobStatusFilter = {};
        $scope.todaysDate = new Date();
        $scope.searchTopJobs = null;
        $scope.isSearched = false;

      
        if (localStorage.getItem("isUserFromSocial") == "yes")
        {
          var job = JSON.parse(localStorage.getItem('selectedJob'));  
          applyToSelectedJob(job);
          localStorage.removeItem("isUserFromSocial");
          localStorage.removeItem("selectedJob");
        }
    }

    $scope.$watch("requirementFilter.$", function() {
        if (angular.isDefined($scope.tblRequirementList)) {
            $scope.tblRequirementList.reload();
            $scope.tblRequirementList.page(1);
        }
    });

    $scope.$watch("appliedJobStatusFilter.$", function() {
        if (angular.isDefined($scope.tblRequirementStatus)) {
            $scope.tblRequirementStatus.reload();
            $scope.tblRequirementStatus.page(1);
        }
    });

    $scope.setSelectedTabIndex = function(tabIndex) {
        $scope.selectedTabIndex = tabIndex;
    };

    $scope.getJobOpenings = function () {
        $scope.common.step = 'two';
        if ($scope.dataMatchingRequirementList)
        {
            return;
        }
        
        blockUI.start();
        $scope.jobList = {};
        $scope.requirementFilter = {};
        $scope.pageSizeShow=8;
        $scope.pageNumberShow=undefined;
        var recordCount = configuration.XSEED_CANDIDATE_MATCHINGJOBS_COUNT;
        var promise = XSeedApiFactory.getJobOpenings($rootScope.userDetails._id, recordCount,$scope.pageSizeShow, $scope.pageNumberShow);
        promise.then(
            function(response) {
             
                $scope.jobList = response[0].data.jobList;
                $scope.totalCount = response[0].data.TotalCount;
                $scope.totalPages = response[0].data.TotalPages;
                $scope.dataMatchingRequirementList = $scope.jobList;

                //manupulation of response
                angular.forEach($scope.jobList, function(val, index) {
                    if (val.TechnicalSkills) {
                        val.TechnicalSkills = val.TechnicalSkills.split('|');
                    }
                });
                
                
                var firstPageFlag =1;
                populateJobOpenings($scope.jobList,$scope.totalCount,$scope.totalPages,firstPageFlag);

                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });
    };

    $scope.onKeydown = function ($event) {
        if ($event.key == 'Backspace') {
            $scope.tblRequirementList = undefined;
            $scope.dataRequirementList = undefined;
            $scope.getDefaultJobOpenings();
        }
    };

    $scope.searchMatchingJobClick = function (searchJobsInput)
    {
        
            $scope.searchMatchingJobsInput = searchJobsInput;
            $scope.common.step = 'two';
            if (!$scope.searchMatchingJobsInput) {
               
                return;
            }

            blockUI.start();
            $scope.jobList = {};
            $scope.requirementFilter = {};
            $scope.pageSizeShow = 8;
            $scope.pageNumberShow = undefined;
            var recordCount = configuration.XSEED_CANDIDATE_MATCHINGJOBS_COUNT;
            var searchString = $scope.searchMatchingJobsInput;
            var promise = XSeedApiFactory.getMatchingSearchJobOpenings($rootScope.userDetails._id, recordCount, $scope.pageSizeShow, $scope.pageNumberShow, searchString);
            promise.then(
                function (response) {
               
                    $scope.jobList = response[0].data.jobList;
                    $scope.totalCount = response[0].data.TotalCount;
                    $scope.totalPages = response[0].data.TotalPages;
                    $scope.dataMatchingRequirementList = $scope.jobList;

                    //manupulation of response
                    angular.forEach($scope.jobList, function (val, index) {
                        if (val.TechnicalSkills) {
                            val.TechnicalSkills = val.TechnicalSkills.split('|');
                        }
                    });

                    $scope.isMatchSearched = true;
                    var firstPageFlag = 1;
                    populateMatchingJobOpenings($scope.jobList, $scope.totalCount, $scope.totalPages, firstPageFlag, searchString);

                    blockUI.stop();
                },
                function (httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                });
        
    }


    $scope.searchClearClick = function () {
        
        if (!$scope.isSearched || !$scope.searchJobsInput)
        {
            return;
        }
        $scope.isSearched = false;
        $scope.tblRequirementList = undefined;
        $scope.dataRequirementList = undefined;
        $scope.searchJobsInput = "";
        $scope.getDefaultJobOpenings();
        
    }


    $scope.searchMatchClearClick = function () {
        if (!$scope.searchMatchingJobsInput) {
            return;
        }
        $scope.isMatchSearched = false;
        $scope.tblMatchingRequirementList = undefined;
        $scope.dataMatchingRequirementList = undefined;
        $scope.searchMatchingJobsInput = "";
        $scope.getJobOpenings();

    }
    // .............................................filter for default job openings....................................................

    $scope.searchTopJobsClick = function (searchJobsInput) {
        $scope.searchJobsInput = searchJobsInput;
        
        if (!$scope.searchJobsInput)
        {  
            return;
        }
        $scope.jobList = {};
        $scope.requirementFilter = {};
        $scope.pageSizeShow = 8;
        $scope.pageNumberShow = undefined;
        
        var recordCount = configuration.XSEED_CANDIDATE_TOPJOBS_COUNT;
        var searchString = $scope.searchJobsInput;
        
        var promise = XSeedApiFactory.getDefaultSearchJobOpenings($rootScope.userDetails._id, recordCount, $scope.pageSizeShow, $scope.pageNumberShow, searchString);
        promise.then(
            function (response) {

                $scope.jobList = response[0].data.jobList;
                
                $scope.dataRequirementList = $scope.jobList;
                $scope.totalCount = response[0].data.TotalCount;
                $scope.totalPages = response[0].data.TotalPages;

                //manupulation of response
                angular.forEach($scope.jobList, function (val, index) {
                    if (val.TechnicalSkills) {
                        val.TechnicalSkills = val.TechnicalSkills.split('|');
                    }
                });

                var firstPageFlag = 1;
                $scope.isSearched = true;
                populateDefaultSearchJobOpenings($scope.jobList, $scope.totalCount, $scope.totalPages, firstPageFlag, searchString);
                blockUI.stop();
            },
            function (httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });
    }

    //...........................................Populate search default job openings........................................


    function populateDefaultSearchJobOpenings(jobList, totalCount, totalPages, pageFlag, searchString) {
        {
            $scope.tblRequirementList = new ngTableParams({
                page: 1,
                count: 8,
                sorting: {}
            }, {
                counts: 8,
                total: totalCount,
                getData: function ($defer, params) {
                   

                    $scope.pageSizeShow = params.count();
                    $scope.pageNumberShow = params.page();
                    $scope.sortBy = params.sorting();

                    if (pageFlag != 1) {
                        var recordCount = configuration.XSEED_CANDIDATE_TOPJOBS_COUNT;
                        var promise = XSeedApiFactory.getDefaultSearchJobOpenings($rootScope.userDetails._id, recordCount, $scope.pageSizeShow, $scope.pageNumberShow, searchString);
                        promise.then(
                            function (response) {

                                $scope.jobList = response[0].data.jobList;
                                $scope.totalCount = response[0].data.TotalCount;
                                $scope.totalPages = response[0].data.TotalPages;
                                $scope.dataRequirementList = $scope.jobList;

                                //manupulation of response
                                angular.forEach($scope.jobList, function (val, index) {
                                    if (val.TechnicalSkills) {
                                        val.TechnicalSkills = val.TechnicalSkills.split('|');
                                    }
                                });
                                $scope.dataRequirementList = $scope.jobList;

                                blockUI.stop();

                            },
                            function (httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('jobList', httpError.data.Status, httpError.data.Message);
                            });

                    }

                    pageFlag = 0;
                    params.total($scope.totalCount);
                    $defer.resolve($scope.jobList);
                },
                $scope: $scope
            });
        }
    }

    //...................................................................................................................................

    $scope.getDefaultJobOpenings = function () {
        $scope.isApplied = false;
        $scope.common.step = 'one';
        if ($scope.dataRequirementList) {
            return;
        }

        if ($scope.selectedTabIndex == 1) {
            blockUI.start();
            $scope.jobList = {};
            $scope.requirementFilter = {};
            $scope.pageSizeShow=8;
            $scope.pageNumberShow=undefined;
           
            var recordCount = configuration.XSEED_CANDIDATE_TOPJOBS_COUNT;
            var promise = XSeedApiFactory.getDefaultJobOpenings($rootScope.userDetails._id,recordCount, $scope.pageSizeShow, $scope.pageNumberShow);
            promise.then(
                function (response) {
                   
                    $scope.jobList = response[0].data.jobList;
                    $scope.dataRequirementList=$scope.jobList;
                    $scope.totalCount = response[0].data.TotalCount;
                    $scope.totalPages = response[0].data.TotalPages;
                    
                    //manupulation of response
                    angular.forEach($scope.jobList, function(val, index) {
                        if (val.TechnicalSkills) {
                            val.TechnicalSkills = val.TechnicalSkills.split('|');
                        }
                    });

                    var firstPageFlag = 1;
                    populateDefaultJobOpenings($scope.jobList, $scope.totalCount, $scope.totalPages, firstPageFlag);
                    blockUI.stop();
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                });
        }
    };


    /******************************************populate default job openings ******************************************/
    function populateDefaultJobOpenings(jobList, totalCount, totalPages, pageFlag) {

        if (angular.isDefined($scope.tblRequirementList)) {
            $scope.tblRequirementList.reload();
        } else {
            $scope.tblRequirementList = new ngTableParams({
                page: 1,
                count: 8,
                sorting: {}
            }, {
                counts: 8,
                total: totalCount,
                getData: function ($defer, params) {
                    

                    $scope.pageSizeShow = params.count();
                    $scope.pageNumberShow = params.page();
                    $scope.sortBy = params.sorting();
                   
                    if (pageFlag != 1) {
                        var recordCount = configuration.XSEED_CANDIDATE_TOPJOBS_COUNT;
                        var promise = XSeedApiFactory.getDefaultJobOpenings($rootScope.userDetails._id,recordCount, $scope.pageSizeShow, $scope.pageNumberShow);
                        promise.then(
                            function (response) {

                                $scope.jobList = response[0].data.jobList;
                                $scope.totalCount = response[0].data.TotalCount;
                                $scope.totalPages = response[0].data.TotalPages;
                                $scope.dataRequirementList=$scope.jobList;

                                //manupulation of response
                                angular.forEach($scope.jobList, function(val, index) {
                                    if (val.TechnicalSkills) {
                                        val.TechnicalSkills = val.TechnicalSkills.split('|');
                                    }
                                });
                                $scope.dataRequirementList=$scope.jobList;



                                blockUI.stop();

                            },
                            function(httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('jobList', httpError.data.Status, httpError.data.Message);
                            });

                    }
                    
                    pageFlag = 0;
                    params.total($scope.totalCount);
                    $defer.resolve($scope.jobList);
                },
                $scope: $scope
            });
        }

    }

    // populate matching job openings
    function populateJobOpenings(jobList, totalCount, totalPages, pageFlag) {
        if (angular.isDefined($scope.tblMatchingRequirementList)) {
            $scope.tblMatchingRequirementList.reload();
        } else {
            $scope.tblMatchingRequirementList = new ngTableParams({
                page: 1,
                count: 8,
                sorting: {}
            }, {
                counts: 8,
                total: totalCount,
                getData: function($defer, params) {

                    $scope.pageSizeShow = params.count();
                    $scope.pageNumberShow = params.page();
                    $scope.sortBy = params.sorting();
                   
                    if (pageFlag != 1) {
                        var recordCount = configuration.XSEED_CANDIDATE_TOPJOBS_COUNT;
                        var promise = XSeedApiFactory.getJobOpenings($rootScope.userDetails._id,recordCount,$scope.pageSizeShow, $scope.pageNumberShow);
                        promise.then(
                            function (response) {
                                

                                $scope.jobList = response[0].data.jobList;
                                $scope.totalCount = response[0].data.TotalCount;
                                $scope.totalPages = response[0].data.TotalPages;

                                //manupulation of response
                                angular.forEach($scope.jobList, function(val, index) {
                                    if (val.TechnicalSkills) {
                                        val.TechnicalSkills = val.TechnicalSkills.split('|');
                                    }
                                });
                                $scope.dataMatchingRequirementList = $scope.jobList;

                                blockUI.stop();

                            },
                            function(httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('jobList', httpError.data.Status, httpError.data.Message);
                            });

                    }
                    pageFlag = 0;
                    params.total($scope.totalCount);
                    $defer.resolve($scope.jobList);
                },
                $scope: $scope
            });
        }

    }


    //....................populate searhed matching job openings.........................................................

    // populate matching job openings
    function populateMatchingJobOpenings(jobList, totalCount, totalPages, pageFlag, searchString) {

            $scope.tblMatchingRequirementList = new ngTableParams({
                page: 1,
                count: 8,
                sorting: {}
            }, {
                counts: 8,
                total: totalCount,
                getData: function ($defer, params) {

                    $scope.pageSizeShow = params.count();
                    $scope.pageNumberShow = params.page();
                    $scope.sortBy = params.sorting();

                    if (pageFlag != 1) {
                        var recordCount = configuration.XSEED_CANDIDATE_TOPJOBS_COUNT;
                        var promise = XSeedApiFactory.getMatchingSearchJobOpenings($rootScope.userDetails._id, recordCount, $scope.pageSizeShow, $scope.pageNumberShow, searchString);
                        promise.then(
                            function (response) {


                                $scope.jobList = response[0].data.jobList;
                                $scope.totalCount = response[0].data.TotalCount;
                                $scope.totalPages = response[0].data.TotalPages;

                                //manupulation of response
                                angular.forEach($scope.jobList, function (val, index) {
                                    if (val.TechnicalSkills) {
                                        val.TechnicalSkills = val.TechnicalSkills.split('|');
                                    }
                                });
                                $scope.dataMatchingRequirementList = $scope.jobList;


                                blockUI.stop();

                            },
                            function (httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('jobList', httpError.data.Status, httpError.data.Message);
                            });

                    }
                    pageFlag = 0;
                    params.total($scope.totalCount);
                    $defer.resolve($scope.jobList);
                },
                $scope: $scope
            });
   //     }

    }

    //...................................................................................................................



    //.....................................................................................................................

    $scope.jobDetailView = function(jobId) {
        blockUI.start();
        var promise = XSeedApiFactory.getJobDetail(jobId);
        promise.then(
            function(response) {
                $scope.requirementModel = response[0].data;
                $scope.requirementModel.isApplied = true;   

                $scope.requirementModel.DriveFromDate = $filter('date')($scope.requirementModel.DriveFromDate, "MM/dd/yyyy");
                $scope.requirementModel.DriveToDate = $filter('date')($scope.requirementModel.DriveToDate, "MM/dd/yyyy");
                $scope.requirementModel.JobPostedDate = $filter('date')($scope.requirementModel.JobPostedDate, "MM/dd/yyyy");
                $scope.requirementModel.JobClosedDate = $filter('date')($scope.requirementModel.JobClosedDate, "MM/dd/yyyy");
                $scope.requirementModel.JobExpiryDate = $filter('date')($scope.requirementModel.JobExpiryDate, "MM/dd/yyyy");

                if ($scope.requirementModel.TechnicalSkills) {
                    $scope.requirementModel.SkillList = [];
                    $scope.requirementModel.SkillList = $scope.requirementModel.TechnicalSkills.split("|");
                }


                blockUI.stop();

                // reqmo
                RequirementDetailsModal.$promise.then(RequirementDetailsModal.show);

           
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });
    };

    $scope.candidateJobDetailView = function (Job) {
       
        $scope.requirementModel = Job;                
                
                $scope.requirementModel.DriveFromDate = $filter('date')($scope.requirementModel.DriveFromDate, "MM/dd/yyyy");
                $scope.requirementModel.DriveToDate = $filter('date')($scope.requirementModel.DriveToDate, "MM/dd/yyyy");
                $scope.requirementModel.JobPostedDate = $filter('date')($scope.requirementModel.JobPostedDate, "MM/dd/yyyy");
                $scope.requirementModel.JobClosedDate = $filter('date')($scope.requirementModel.JobClosedDate, "MM/dd/yyyy");
                $scope.requirementModel.JobExpiryDate = $filter('date')($scope.requirementModel.JobExpiryDate, "MM/dd/yyyy");

                if ($scope.requirementModel.TechnicalSkills) {
                    $scope.requirementModel.SkillList = [];
                    $scope.requirementModel.SkillList = $scope.requirementModel.TechnicalSkills;//.split("|");
                }

                // reqmo
                RequirementDetailsModal.$promise.then(RequirementDetailsModal.show);
    };

    $scope.closeRequirementDetailModal = function() {

        RequirementDetailsModal.$promise.then(RequirementDetailsModal.hide);
    };

  $scope.applyToSelectedJob = function (job) {
    applyToSelectedJob(job);
  }

  function applyToSelectedJob(job) {

      var Id = $rootScope.userDetails._id;
      var promise = XSeedApiFactory.getCandidateDetail($rootScope.userDetails._id);
      var isCompleteProfile = false;
      promise.then(
        function (response) {

          $scope.candidateModel = response[0].data;
          if ($scope.candidateModel.ResumeList == null) {
            isCompleteProfile = true;
          }
          if (!isCompleteProfile) {

            XSeedAlert.swal({
              title: "Are you sure?",
              text: "You want to apply for " + job.JobTitle,
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-danger",
              confirmButtonText: "Apply",
              cancelButtonText: "Cancel",
              closeOnConfirm: false,
              closeOnCancel: false
            }).then(function() {
              var jobApplication = {};
              job.isApplied = true;
              jobApplication.Company = job.CompanyName;
              jobApplication.JobId = job.Id;
              jobApplication.JobStatus = job.JobStatus;
              jobApplication.JobTitle = job.JobTitle;
              jobApplication.Status = "Applied";
              jobApplication.AppliedDate = $filter('date')($scope.todaysDate, "MM/dd/yyyy");
              promise = XSeedApiFactory.applyToSelectedJob(jobApplication, Id);
              promise.then(
                function(responses) {
                  blockUI.stop();
                  XSeedAlert.swal({
                    title: 'Success!',
                    text: 'Applied successfully!',
                    type: "success",
                    customClass: "xseed-error-alert",
                    allowOutsideClick: false
                  }).then(function() {
                    $timeout(function () {
                      changeJobStatusOnJobList(job);
                      $location.path('openings');
                    }, 0);
                  }, function(dismiss) {
                    blockUI.stop();
                  });
                },
                function(httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                });
            }, function (dismiss) {
                localStorage.removeItem("isUserFromSocial");
                localStorage.removeItem("selectedJob");
            });
          }
          else {
            XSeedAlert.swal({
              title: "Please complete your profile first then apply!!",
              text: "You want to apply for " + job.JobTitle,
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-danger",
              confirmButtonText: "Continue to complete your profile",
              cancelButtonText: "Cancel",
              closeOnConfirm: false,
              closeOnCancel: false
            }).then(function () {
              blockUI.start();
              var job1 = job;
              window.localStorage.setItem("isUserFromSocial1", "yes");
              window.localStorage.setItem('selectedJob1', JSON.stringify(job1));
              $location.path("/candidateProfile");
              
            }, function (dismiss) {
                localStorage.removeItem("isUserFromSocial1");
                localStorage.removeItem("selectedJob1");
            });
          }
        },
        function (httpError) {
          blockUI.stop();
          ExceptionHandler.handlerHTTPException('Candidate', httpError.data.Status, httpError.data.Message);
        });
      
    }

    function changeJobStatusOnJobList(job) {
        if ($scope.selectedTabIndex == 3) {
            angular.forEach($scope.dataSearchedRequirementList, function(val, index) {
                if (val.Id == job.Id) {
                    val.isApplied = true;
                }
            });
        } else {
            angular.forEach($scope.dataRequirementList, function(val, index) {
                if (val.Id == job.Id) {
                    val.isApplied = true;
                }
            });
        }
    }

    $scope.getAppliedJobStatus = function() {
        var userId = $rootScope.userDetails.UserId;
        blockUI.start();
        var promise = XSeedApiFactory.getAppliedJobStatus(userId);
        promise.then(
            function(response) {
                $scope.appliedJobStatus = response[0].data;
                if ($scope.appliedJobStatus) {
                    populateRequirementStatus();
                }
                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });
    };

    function populateRequirementStatus() {
        if (angular.isDefined($scope.tblRequirementStatus)) {
            $scope.tblRequirementStatus.reload();
        } else {
            $scope.tblRequirementStatus = new ngTableParams({
                page: 1,
                count: 10,
                sorting: {}
            }, {
                counts: [],
                total: $scope.appliedJobStatus.length,
                getData: function($defer, params) {
                    var filteredData = $filter('filter')($scope.appliedJobStatus, $scope.appliedJobStatusFilter);
                    $scope.dataAppliedJobStatus = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
                    params.total($scope.dataAppliedJobStatus.length);
                    $scope.dataAppliedJobStatus = $scope.dataAppliedJobStatus.slice((params.page() - 1) * params.count(), params.page() * params.count());

                    $defer.resolve($scope.dataAppliedJobStatus);
                },
                $scope: $scope
            });
        }
    }

   

    $scope.goToOpenings = function() {
        $location.path("openings");
    }


    //**********************************************  SEARCH JOBS *****************************************//
    $scope.getJobSearchOptions = function () {
        $scope.common.step = 'three';
        $scope.lookup.degree = $rootScope.metadata.GetAncillaryInformationResponse.DegreeList.Degree;
        getSkillLookUp();
    };

    function getSkillLookUp() {
       // alert()
        var promise = XSeedApiFactory.candidateSkillLookupCall();
        promise.then(
            function(response) {
                $scope.lookup.skill = response[0].data;
                buildSkillStructuredData();
            },
            function(httpError) {
                ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
            });

    }

    function buildSkillStructuredData() {
        $scope.skills = [{}];
        var arrSkill = [];
        var res = $scope.lookup.skill;
        angular.forEach(res, function(val, index) {
            arrSkill.push({ value: val.Name, label: val.Name });
        });

        blockUI.stop();

        $scope.lookup.skillListData = arrSkill;
    }

    $scope.initOpenings=function()
    {
        $scope.dataRequirementList = null;
        $scope.dataMatchingRequirementList = null;
        if ($scope.common.step=='two')
        {
            $scope.getJobOpenings();
        }
        else 
        {
            $scope.getDefaultJobOpenings();
        }
        
    }

   

    $scope.searchJobs = function (searchJobsForm) {
        
        if (new ValidationService().checkFormValidity(searchJobsForm)) {
            $scope.searchModel.JobTitle = $scope.searchModel.JobTitle == "" ? undefined : $scope.searchModel.JobTitle;
            $scope.searchModel.Location = $scope.searchModel.Location == "" ? undefined : $scope.searchModel.Location;

            var promise = XSeedApiFactory.searchJobOpenings($scope.searchModel);
            promise.then(
                function(response) {
                    $scope.searchedJobList = response[0].data;

                    //manupulation of response
                    angular.forEach($scope.searchedJobList, function(val, index) {
                        if (val.TechnicalSkills) {
                            val.TechnicalSkills = val.TechnicalSkills.split('|');
                        }
                    });
                    checkAlreadyAppliedSearchedJobs();
                    populateSearchedRequirementList();
                    blockUI.stop(); 
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                });
        }
    };

    function checkAlreadyAppliedSearchedJobs() {
        blockUI.start();
        var userId = $rootScope.userDetails.UserId;
        var promise = XSeedApiFactory.getAppliedJobStatus(userId);
        promise.then(
            function(response) {
                $scope.appliedJobStatus = response[0].data;
                if ($scope.appliedJobStatus) {
                    for (var i = 0; i < $scope.searchedJobList.length; i++) {
                        for (var j = 0; j < $scope.appliedJobStatus.length; j++) {
                            if ($scope.searchedJobList[i].Id === $scope.appliedJobStatus[j].JobId) {
                                $scope.searchedJobList[i].isApplied = true;
                            }
                        }
                    }
                    if ($scope.requirementModel) {
                        for (var p = 0; p < $scope.appliedJobStatus.length; p++) {
                            if ($scope.requirementModel.Id === $scope.appliedJobStatus[p].JobId) {
                                $scope.requirementModel.isApplied = true;
                            }
                        }
                    }
                }
                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });

    }

    function populateSearchedRequirementList() {
        if (angular.isDefined($scope.tblSearchedRequirementList)) {
            $scope.tblSearchedRequirementList.reload();
        } else {
            $scope.requirementFilter = {};
            $scope.tblSearchedRequirementList = new ngTableParams({
                page: 1,
                count: 10,
                sorting: {}
            }, {
                counts: [],
                total: $scope.searchedJobList.length,
                getData: function($defer, params) {
                    var filteredData = $filter('filter')($scope.searchedJobList, $scope.requirementFilter);
                    $scope.dataSearchedRequirementList = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
                    params.total($scope.dataSearchedRequirementList.length);
                    $scope.dataSearchedRequirementList = $scope.dataSearchedRequirementList.slice((params.page() - 1) * params.count(), params.page() * params.count());

                    $defer.resolve($scope.dataSearchedRequirementList);
                },
                $scope: $scope
            });
        }
    }

    //**********************************************  SEARCH JOBS *****************************************//

    //***************************************************************************************//

    // Get the render context local to this controller (and relevant params).
    var renderContext = RequestContext.getRenderContext("standard.opening");
    // The subview indicates which view is going to be rendered on the page.
    $scope.subview = renderContext.getNextSection();
    $scope.$on(
        "requestContextChanged",
        function() {
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {
                return;
            }
            // Update the view that is being rendered.
            $scope.subview = renderContext.getNextSection();
        }
    );
}
