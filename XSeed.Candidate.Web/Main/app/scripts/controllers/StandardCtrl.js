'use strict';

XSEED_CANDIDATE_APP.controller('StandardCtrl', StandardCtrl);

/**
 * @ngInject
 */
function StandardCtrl($rootScope, $scope, RequestContext, $log, ExceptionHandler, i18nFactory, localStorageService, $window, $location, blockUI, XSeedApiFactory, ModalFactory, XSeedAlert, $filter, ValidationService, $timeout) {
    $scope.changePasswordModel = {};

    $scope.clearChangePasswordModel = function () {
        $scope.changePasswordModel = {};
    };

    $scope.changePassword = function (changePasswordForm) {

        if (new ValidationService().checkFormValidity(changePasswordForm)) {

            // Temporary Model and variable
            $scope.tempChangePasswordModel = {};         

            blockUI.start();

            $scope.changePasswordModel.CandidateUserId = $rootScope.userDetails.UserId;
            //Dummy model
            $scope.tempChangePasswordModel = angular.copy($scope.changePasswordModel);
            /********************************************** Ecrypt Password ****************************************/

            /* Code for Encryption Old Password Start */
            var key = CryptoJS.enc.Utf8.parse('4263193851707158');
            var iv = CryptoJS.enc.Utf8.parse('4263193851707158');

            $scope.oldPasswordE = $scope.changePasswordModel.OldPassword;            

            var encryptedlogin = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.oldPasswordE), key,
           {
               keySize: 128 / 8,
               iv: iv,
               mode: CryptoJS.mode.CBC,
               padding: CryptoJS.pad.Pkcs7
           });

            $scope.oldPasswordE = encryptedlogin;
            /* Code for Encryption Old Password End */

            /* Code for Encryption New Password Start */
            $scope.NewpasswordEncrypted = $scope.changePasswordModel.NewPassword;
            var encryptedpassword = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewpasswordEncrypted), key,
            {
                keySize: 128 / 8,
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });

            $scope.NewpasswordEncrypted = encryptedpassword;
            /* Code for Encryption New Password End */

            /* Code for Encryption Confirm Password Start */
            $scope.ConfirmpasswordEncrypted = $scope.changePasswordModel.NewPassword;
            var encryptedConfirmpassword = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.ConfirmpasswordEncrypted), key,
            {
                keySize: 128 / 8,
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });

            $scope.ConfirmpasswordEncrypted = encryptedConfirmpassword;
            /* Code for Encryption Confirm Password End */

            $scope.tempChangePasswordModel.OldPassword = '' + $scope.oldPasswordE + '';
            $scope.tempChangePasswordModel.NewPassword = '' + $scope.NewpasswordEncrypted + '';
            $scope.tempChangePasswordModel.ConfirmPassword = '' + $scope.ConfirmpasswordEncrypted + '';

            /********************************************** Ecrypt Password ****************************************/

            var promise = XSeedApiFactory.changePassword($scope.tempChangePasswordModel);
            promise.then(
            function (response) {

                blockUI.stop();

                XSeedAlert.swal({
                    title: 'Success!',
                    text: 'Your password has been changed successfully!',
                    type: "success",
                    customClass: "xseed-error-alert",
                    //showCancelButton: true,
                    //confirmButtonText: "OK",
                    //closeOnConfirm: true
                }).then(function () {
                    $scope.changePasswordModel = {}
                    $timeout(function () {
                        $location.path('candidateProfile');
                    }, 0);
                }, function (dismiss) {
                    //console.log('dismiss: ' + dismiss);
                    // dismiss can be 'cancel', 'overlay', 'close', 'timer'
                    //if (dismiss == 'cancel'){alert('Close fired');}
                });
            },
        function (httpError) {
            blockUI.stop();
            ExceptionHandler.handlerPopup('Warning', httpError.data.Message, 'Warning!', 'error');
        });
        }
    }

    $scope.gotoDashboard = function () {
        $scope.changePasswordModel = {};
        $location.path('candidateProfile');
    };

    $scope.logOut = function () {
        localStorageService.clearAll();

        blockUI.start();
        var promise = XSeedApiFactory.logout();
        promise.then(
            function (response) {
                $rootScope.userDetails = undefined;
                blockUI.stop();
            },
            function (httpError) {
                blockUI.stop();
                ExceptionHandler.handlerPopup('Warning', "logout successfully", 'Warning!', 'error');
            });

        $window.location.href = 'index.html';
    };

    // Get the render context local to this controller (and relevant params).
    var renderContext = RequestContext.getRenderContext("standard");
    // The subview indicates which view is going to be rendered on the page.
    $scope.subview = renderContext.getNextSection();
    $scope.$on(
        "requestContextChanged",
        function () {
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {
                return;
            }
            // Update the view that is being rendered.
            $scope.subview = renderContext.getNextSection();
        }
    );

}
