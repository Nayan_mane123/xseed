'use strict';
XSEED_CANDIDATE_APP.service('ExceptionHandler',ExceptionHandler);
/**
 * @ngInject
 */
function ExceptionHandler ($rootScope, $http, $translate, $timeout, $log, blockUI) {

        var handler = {};
        handler.ExceptionMessage = {};

        handler.handlerHTTPException = function (apiName, status, responseMessage) {
            blockUI.stop();
            this.setErrorMessage(apiName, status, responseMessage);
            this.broadcastError();

        };

        handler.broadcastError = function () {
            $timeout(function () {
                $rootScope.$broadcast('handleError');
            }, 1000);

        };

        handler.setErrorMessage = function (apiName, status, responseMessage) {
            //if(responseMessage && responseMessage.data && (angular.equals(responseMessage.data.status, 400)|| angular.equals(responseMessage.data.status, 404))) {
            //  handler.ExceptionMessage.message = "There was an error processing your request, Invalid data in the request" ;
            //}
            //else {
            //  handler.ExceptionMessage.message = "There was an error processing your request, Please try later." ;
            //}

            if (!angular.isUndefined(responseMessage) && responseMessage != null) {
                handler.ExceptionMessage.message = responseMessage;
            }
            else {
                handler.ExceptionMessage.message = "There was an error processing your request, Please try later.";
            }
        };

        handler.broadcastPopup = function () {
            $timeout(function () {
                $rootScope.$broadcast('handlePopup');
            }, 1000);
        };

        handler.broadcastAccountException = function () {
          $timeout(function () {
            $rootScope.$broadcast('handleAccountException');
          }, 1000);

        };

        handler.handlerPopup = function(apiName, message, title, type) {
            blockUI.stop();
            handler.ExceptionMessage.message = message ;
            handler.ExceptionMessage.title = title ;
            handler.ExceptionMessage.type = type ;
            this.broadcastPopup();
        }

        handler.setValidationErrorMessage = function (apiName, message) {
            handler.ExceptionMessage.message = message ;
        };



        return handler;

    };
