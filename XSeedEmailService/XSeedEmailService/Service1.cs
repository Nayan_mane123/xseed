﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace XSeedEmailService
{
    public partial class Service1 : ServiceBase
    {
        private Timer timer1 = null;
        public bool notificationSent = false;
        public bool firstCall = true;

        public Service1()
        {
            InitializeComponent();
        }

        public void Debug()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            timer1 = new Timer();
            this.timer1.Interval = 5000; //every 5 secs
            this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.timer1_Tick);
            timer1.Enabled = true;
            Library.WriteErrorLog("Xseed email service started.");

            //while (true)
            //{
            //    List<EmailNotificationModel> emailsInQueue = EmailNotificationModel.GetEmailsInQueue();

            //    foreach (var email in emailsInQueue)
            //    {
            //        EmailNotificationModel.SendEmailNotification(email);
            //    }
            //}    
        }

        private void timer1_Tick(object sender, ElapsedEventArgs e)
        {
            //Write code here to do some job depends on your requirement
            try
            {
                if (firstCall || notificationSent)
                {
                    notificationSent = false;
                    List<EmailNotificationModel> emailsInQueue = EmailNotificationModel.GetEmailsInQueue();

                    foreach (var email in emailsInQueue)
                    {
                        EmailNotificationModel.SendEmailNotification(email);
                    }
                    firstCall = false;
                    notificationSent = true;
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }

            Library.WriteErrorLog("Xseed Email Notifications Service running successfully.");
        }

        protected override void OnStop()
        {
            timer1.Enabled = false;
            Library.WriteErrorLog("Xseed email service stopped.");
        }
    }
}
