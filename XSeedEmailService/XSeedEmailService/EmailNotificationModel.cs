﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace XSeedEmailService
{
    public class EmailNotificationModel
    {
        static string url = ConfigurationManager.AppSettings["MongoURL"].ToString();
        static string dbName = ConfigurationManager.AppSettings["MongoDB"].ToString();
        static string emailCollection = ConfigurationManager.AppSettings["MongoEmailCollection"].ToString();

        #region Property Declaration

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }

        //[Required]
        public string TypeOfNotification { get; set; }

        //[Required]
        public string FromEmailID { get; set; }
        public string FromDisplayName { get; set; }

        //[Required]
        public string FromEmailPassword { get; set; }

        //[Required]
        public List<string> ToEmailID { get; set; }
        public List<string> ToCC { get; set; }
        public List<string> ToBCC { get; set; }
        public string MailSubject { get; set; }

        //[Required]
        public string MailBody { get; set; }
        public string CallBackUrl { get; set; }
        public List<XSeedFileEntity> MailAttachment { get; set; }

        public string ExtraData { get; set; }
        public int Priority { get; set; }
        public bool IsSent { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Sends Email Notification
        /// </summary>
        /// <param name="emailNotificationModel">EmailNotificationModel</param>
        public static bool SendEmailNotification(EmailNotificationModel model)
        {
            try
            {
                if (model.ToEmailID != null)
                {
                    MailMessage objMail = new MailMessage();

                    SmtpClient objSMTP = new SmtpClient();

                    if (!string.IsNullOrWhiteSpace(model.FromDisplayName))
                    {
                        objMail.From = new MailAddress(model.FromEmailID, model.FromDisplayName);
                    }
                    else
                    {
                        objMail.From = new MailAddress(model.FromEmailID);
                    }

                    foreach (string toEmail in model.ToEmailID)
                    {
                        objMail.To.Add(toEmail);
                    }

                    if (model.ToCC != null)
                    {
                        foreach (string email in model.ToCC)
                        {
                            objMail.CC.Add(email);
                        }
                    }
                    if (model.ToBCC != null)
                    {
                        foreach (string email in model.ToBCC)
                        {
                            objMail.Bcc.Add(email);
                        }
                    }

                    //Code for get Mail template according to type of notication
                    objMail.IsBodyHtml = true;


                    //string mailTemplate = "";
                    //switch (model.TypeOfNotification)
                    //{
                    //    case "ResetPassword":
                    //        model.MailSubject = Constants.ResetPasswordSubject;
                    //        mailTemplate = File.ReadAllText(Path.Combine(ConfigurationManager.AppSettings["EmailTemplateFolderPath"], Constants.ResetPasswordEmailTemplateFile));
                    //        //mailTemplate = mailTemplate.Replace("action_url", model.CallBackUrl.ToString());
                    //        model.MailBody = mailTemplate;
                    //        break;
                    //    case "PasswordMail":
                    //        model.MailSubject = Constants.PasswordMailSubject;
                    //        mailTemplate = File.ReadAllText(Path.Combine(ConfigurationManager.AppSettings["EmailTemplateFolderPath"], Constants.PasswordMailEmailTemplateFile));
                    //        //mailTemplate = mailTemplate.Replace("userName", model.ToEmailID.FirstOrDefault());
                    //        //mailTemplate = mailTemplate.Replace("userPassword", model.ExtraData.ToString());
                    //        //mailTemplate = mailTemplate.Replace("action_url", model.CallBackUrl.ToString());
                    //        model.MailBody = mailTemplate;
                    //        break;

                    //    case "ResumeAttachment":
                    //        model.MailSubject = Constants.resumeAttachmentSubject;
                    //        mailTemplate = File.ReadAllText(Path.Combine(ConfigurationManager.AppSettings["EmailTemplateFolderPath"], Constants.ResetResumeAttachmentTemplateFile));
                    //        model.MailBody = mailTemplate;
                    //        break;
                    //    default:
                    //        break;
                    //}

                    objMail.Subject = model.MailSubject;
                    objMail.Body = model.MailBody;


                    if (model.MailAttachment != null && model.MailAttachment.Count > 0)
                    {
                        /* Convert resumes to attachments */
                        List<Attachment> attachments = AttachCandidateResumes(model.MailAttachment);
                        foreach (var attachment in attachments)
                        {
                            objMail.Attachments.Add(attachment);
                        }
                    }

                    objSMTP.Host = "smtp.gmail.com";
                    objSMTP.UseDefaultCredentials = false;
                    objSMTP.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
                    objSMTP.EnableSsl = true;
                    objSMTP.DeliveryMethod = SmtpDeliveryMethod.Network;
                    //objSMTP.Credentials = new NetworkCredential(model.FromEmailID, model.FromEmailPassword);
                    //objSMTP.Credentials = new NetworkCredential("xseedtest@tcognition.com", "bMX4&ZZX");
                    objSMTP.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailFromUserName"], ConfigurationManager.AppSettings["EmailFromPassword"]);

                    objMail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                    objSMTP.Send(objMail);

                    return UpdateEmailQueue(model);
                }

                return false;
            }
            catch (Exception ex)
            {
                //string tempExMessage = ex.Message;
                //tempExMessage += ex.ToString();

                Library.WriteErrorLog(ex);
                return false;
            }
        }

        private static List<Attachment> AttachCandidateResumes(List<XSeedFileEntity> attchmentlist)
        {
            List<Attachment> list = new List<Attachment>();

            if (attchmentlist.Count > 0)
            {

                foreach (var resume in attchmentlist)
                {
                    if (resume != null)
                    {
                        /* Get file data */
                        byte[] data = resume.data.Contains(',') ? System.Convert.FromBase64String(resume.data.Split(',')[1]) : System.Convert.FromBase64String(resume.data);

                        using (MemoryStream stream = new MemoryStream(data, true))
                        {
                            stream.Write(data, 0, data.Length);

                            //string mimeType = MimeMapping.GetMimeMapping(model.Resume.name);


                            System.IO.MemoryStream ms = new System.IO.MemoryStream(data, true);

                            System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(ms, resume.name);
                            attachment.ContentDisposition.FileName = resume.name;
                            list.Add(attachment);
                        }
                    }
                }
            }
            return list;
        }

        private static bool UpdateEmailQueue(EmailNotificationModel model)
        {
            MongoClient client = new MongoClient(url);

            try
            {
                var database = client.GetDatabase(dbName);

                var filter = Builders<EmailNotificationModel>.Filter.Eq("_id", ObjectId.Parse(model._id));

                var collection = database.GetCollection<EmailNotificationModel>(emailCollection);

                model.IsSent = true;
                model.ModifiedDate = DateTime.UtcNow;

                collection.ReplaceOne(filter, model);

                return true;
            }
            catch (Exception ex)
            {
                //throw ex;   
                Library.WriteErrorLog(ex);
                return false;
            }
            finally
            {
                client = null;
            }
        }

        #endregion

        internal static List<EmailNotificationModel> GetEmailsInQueue()
        {
            List<EmailNotificationModel> emails = new List<EmailNotificationModel>();
            try
            {
                MongoClient client = new MongoClient(url);
                var database = client.GetDatabase(dbName);

                var filter = Builders<EmailNotificationModel>.Filter.Eq("IsSent", false);
                var sort = Builders<EmailNotificationModel>.Sort.Ascending("Priority");

                var collection = database.GetCollection<EmailNotificationModel>(emailCollection);
                //var documents = collection.Find(Builders<EmailNotificationModel>.Filter.Empty).ToList();
                var documents = collection.Find(filter).Sort(sort).Limit(Convert.ToInt32(ConfigurationManager.AppSettings["RecordsLimit"])).ToList();

                return documents;
            }
            catch (Exception ex)
            {
                //throw ex;     
                Library.WriteErrorLog(ex);
                return emails;
            }
        }
    }

    public class XSeedFileEntity
    {
        #region Property Declaration

        public string lastModified { get; set; }
        public string lastModifiedDate { get; set; }
        public string name { get; set; }
        public string size { get; set; }
        public string type { get; set; }
        public string data { get; set; }
        public string filePath { get; set; }

        #endregion

        //#region User Defined Functions

        ///// <summary>
        ///// Save Profile Image
        ///// </summary>
        ///// <param name="fileEntity">XSeed File Entity</param>
        ///// <param name="request">Request Type</param>
        ///// <returns>File Name</returns>
        //public static string SaveProfileImage(XSeedFileEntity fileEntity, string request)
        //{
        //    string fileName = string.Empty;

        //    if (fileEntity != null)
        //    {
        //        /* Rename file & Update Model */
        //        XSeedFileEntity file = fileEntity;
        //        fileName = FileUtility.RenameFile(file.name);

        //        /* Save profile image */
        //        FileUtility.SaveImageFile(fileName, file.type, file.data, request);
        //    }
        //    return fileName;
        //}

        ///// <summary>
        ///// Save candidate resume file
        ///// </summary>
        ///// <param name="resume">XSeed File Entity</param>
        ///// <param name="request">Request Type</param>
        ///// <returns>File Name</returns>
        //public static string SaveResumeFile(XSeedFileEntity resume, string request)
        //{
        //    string fileName = string.Empty;

        //    if (resume != null)
        //    {
        //        /* Rename file & Update Model */
        //        XSeedFileEntity file = resume;
        //        fileName = FileUtility.RenameFile(file.name);

        //        /* Save profile image */
        //        fileName = FileUtility.SaveResumeFile(fileName, file.type, file.data, request);
        //    }
        //    return fileName;
        //}

        ///// <summary>
        ///// Delete candidate resume file after parsing 
        ///// </summary>
        ///// <param name="fileName">File Name</param>
        //public static void DeleteResumeFile(string fileName)
        //{
        //    /* Delete profile image */
        //    FileUtility.DeleteResumeFile(fileName);
        //}

        //#endregion
    }
}

