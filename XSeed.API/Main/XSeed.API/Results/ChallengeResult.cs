﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace XSeed.API.Results
{
    /// <summary>
    /// Challenge Result
    /// </summary>
    public class ChallengeResult : IHttpActionResult
    {
        /// <summary>
        /// Challenge Result
        /// </summary>
        public ChallengeResult(string loginProvider, ApiController controller)
        {
            LoginProvider = loginProvider;
            Request = controller.Request;
        }

        /// <summary>
        /// Login Provider
        /// </summary>
        public string LoginProvider { get; set; }
        /// <summary>
        /// Request
        /// </summary>
        public HttpRequestMessage Request { get; set; }

        /// <summary>
        /// Execute Async
        /// </summary>
        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            Request.GetOwinContext().Authentication.Challenge(LoginProvider);

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            response.RequestMessage = Request;
            return Task.FromResult(response);
        }
    }
}
