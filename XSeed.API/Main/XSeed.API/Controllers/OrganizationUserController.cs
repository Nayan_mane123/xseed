﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using XSeed.API.Models;
using XSeed.Business.User.OrganizationUserInfo;
using XSeed.Data.ViewModel.User.OrganizationUser;
using XSeed.Utility;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Organization user API
    /// </summary>
    public class OrganizationUserController : BaseController
    {

        /// <summary>
        /// Get organization user(s) belongs to an organization
        /// </summary>        
        /// <param name="organizationId">Organization Id</param>
        /// <param name="Id">Id</param>
        /// <param name="isMobileRequest"></param>
        /// <remarks>Get user/ users of the organization 
        /// - Organization Id is required parameter
        /// - Id is an optional parameter
        /// - Id with value as zero will return list of users belongs to an organization
        /// - Id with value as positive integer will return single users details
        ///</remarks>
        /// <returns>Organization User(s)</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Organization_Read", ClaimValue = "True")]
        public IHttpActionResult Get(Guid organizationId, Guid? Id = null, Boolean isMobileRequest = false)
        {
            OrganizationUserInfo model = new OrganizationUserInfo();

            if (Id == null)
            {
                if (isMobileRequest)
                {
                    return Ok(ResponseUtility.SuccessResponseData(model.ListOrganizationUsers(organizationId)));
                }
                return Ok(model.ListOrganizationUsers(organizationId)); // return user list
            }
            else
            {
                if (isMobileRequest)
                {
                    return Ok(ResponseUtility.SuccessResponseData(model.GetUser(Id, organizationId)));
                }
                return Ok(model.GetUser(Id, organizationId)); // return single user
            }
        }

        ///// <summary>
        ///// Get particular user detail
        ///// </summary>
        ///// <param name="userId">User Id</param>
        ///// <param name="organizationId">Organization Id</param>
        ///// <returns>Organization user</returns>


        ///// <summary>
        ///// Get particular user credentials
        ///// </summary>
        ///// <param name="userName">User name</param>
        //public IHttpActionResult GetGetUserDetailByName(string userName)


        /// <summary>
        /// Create a new user for on organization
        /// </summary>
        /// <param name="organizationUserModel">organization user model </param>
        /// <param name="isMobileRequest"></param>
        /// <remarks>Create a new user for organization</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>   
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Organization_Create", ClaimValue = "True")]
        public async Task<IHttpActionResult> Post(OrganizationUserModel organizationUserModel, Boolean isMobileRequest = false)
        {

            if (!ModelState.IsValid)
            {
                throw new ArgumentException("Invalid Model State");
            }

            //To send email to created users with Admin email configuration
            IEnumerable<string> headerUserId;
            Guid UserId = Guid.Empty;
            if (Request.Headers.TryGetValues("UserId", out headerUserId))
            {
                UserId = Guid.Parse(headerUserId.FirstOrDefault());
            }

            Guid organizationUserID;
            string password = string.Empty;

            // Genrate random password for new organization user
            password = PasswordUtility.RandomPassword(Convert.ToInt32(ConfigurationManager.AppSettings["RandomPasswordLenth"]));

            // Add to User Login table
            OrganizationUserInfo model = new OrganizationUserInfo();
            organizationUserID = model.CreateOrganizationUser(organizationUserModel, UserId, password);

            // Create AspNet User
            AccountController account = new AccountController();
            IdentityResult result = await (account.CreateAspNetUser(organizationUserModel.PrimaryEmail, password, organizationUserModel.RoleId));
            IHttpActionResult errorResult = account.GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessResponseData(organizationUserID));
            }
            return Ok(organizationUserID);
        }

        /// <summary>
        /// Update the details for an organization user
        /// </summary>
        /// <param name="organizationUserModel">organization userM model </param>
        /// <param name="isMobileRequest"></param>
        /// <remarks>Update the details about the organization user</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPut]
        [ClaimsAuthorization(ClaimType = "Organization_Update", ClaimValue = "True")]
        // [ResponseType(typeof(OrganizationUserModel))]
        public IHttpActionResult Put(OrganizationUserModel organizationUserModel, Boolean isMobileRequest = false)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            OrganizationUserInfo model = new OrganizationUserInfo();
            OrganizationUserModel updatedDetails = model.UpdateOrganizationUser(organizationUserModel);

            if (organizationUserModel.UpdateUserPermissions)
            {
                AccountController account = new AccountController();
                account.UpdateUserClaims(organizationUserModel.PrimaryEmail, organizationUserModel.RoleId);
            }

            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessResponseData(updatedDetails));
            }
            return Ok(updatedDetails);
        }

        /// <summary>
        /// Enable/Disable Organization User
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="oraganizationId"></param>
        /// <param name="isActive"></param>
        /// <param name="isMobileRequest"></param>
        /// <returns></returns>
        [HttpPut]
        [ClaimsAuthorization(ClaimType = "Organization_Update", ClaimValue = "True")]
        public IHttpActionResult EnableDisableUser(Guid userId, Guid oraganizationId, bool isActive, Boolean isMobileRequest = false)
        {
            OrganizationUserInfo model = new OrganizationUserInfo();
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessResponseData(model.EnableDisableUser(userId, isActive)));
            }
            return Ok(model.EnableDisableUser(userId, isActive));

        }

        /// <summary>
        /// Transfer Requirements
        /// </summary>
        /// <param name="userId">Current recruiter Id</param>
        /// <param name="TransferedUserId">new recruiter Id</param>
        /// <returns></returns>
        [HttpPut]
        [ClaimsAuthorization(ClaimType = "Organization_Update", ClaimValue = "True")]
        public IHttpActionResult TransferRequirements(Guid userId, Guid TransferedUserId)
        {
            OrganizationUserInfo model = new OrganizationUserInfo();
            model.TransferRequirements(userId, TransferedUserId);
            model.EnableDisableUser(userId, false);
            return Ok();
        }

        #region --- Check Valid User ---

        /// <summary>
        /// Check valid user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="isMobileRequest"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult CheckValidUser(Guid? userId, Boolean isMobileRequest = false)
        {
            if (userId != null)
            {
                if (isMobileRequest)
                {
                    return Ok(ResponseUtility.SuccessResponseData(OrganizationUserInfo.CheckValidUser(userId.Value)));
                }
                return Ok(OrganizationUserInfo.CheckValidUser(userId.Value));
            }
            else
            {
                return Ok();
            }
        }

        #endregion
    }
}
