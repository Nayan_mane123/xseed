﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XSeed.Business.Parser;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Parser API
    /// </summary>
    [AllowAnonymous]
    public class ParserController : BaseController
    {
        /// <summary>
        /// Get list of unparsed email
        /// </summary>        
        /// <returns>Unparsed Email list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpGet]
        public IHttpActionResult GetParsedJob(Guid OrganizationUserId)
        {
            ParserInfo model = new ParserInfo();

            return Ok(model.GetParsedJob(OrganizationUserId)); // return parsed jobs
        }


        /// <summary>
        /// Get list of parsed jobs using server side pagination
        /// </summary>
        /// <param name="OrganizationUserId"></param>
      
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>        
        /// <returns>Parsed job List</returns>
        // GET: api/Jobs/pageSize/pageNumber/
        //[Route("{pageSize:int}/{pageNumber:int}")]
        public IHttpActionResult GetParsedJobList(Guid OrganizationUserId, int pageSize, int pageNumber)
        {
            ParserInfo model = new ParserInfo();

            /* Set Pagination Info */
            var paginationInfo = model.GetPaginationInfo(OrganizationUserId, pageSize);

            /* return job list */
            var result = new
            {
                TotalCount = paginationInfo.TotalCount,
                totalPages = paginationInfo.TotalPages,
                parsedJobs = model.ListAllParsedJobs(OrganizationUserId, pageSize, pageNumber)
            };

            return Ok(result);
        }

        /// <summary>
        /// Get Parsed Job List Count
        /// </summary>
        /// <param name="OrganizationUserId">Organization UserId</param>
        /// <returns>Parsed Job List Count</returns>
        public IHttpActionResult GetParsedJobListCount(Guid OrganizationUserId)
        {
            ParserInfo model = new ParserInfo();

            /* Set Pagination Info */
            int parsedJobCount = model.GetParsedJobListCount(OrganizationUserId);

            return Ok(parsedJobCount);
        }

        /// <summary>
        /// Get list of accepted parsed job
        /// </summary>        
        /// <returns>Accepted parsed job list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>        
        [HttpGet]
        public IHttpActionResult GetAcceptedParsedJob(Guid OrganizationUserId)
        {
            ParserInfo model = new ParserInfo();

            return Ok(model.GetAcceptedParsedJob(OrganizationUserId)); // return Unparsed Emails
        }

        /// <summary>
        /// Accept Parsed Job and add to Job Detail
        /// </summary>
        /// <param name="Id">Id</param>
        /// <param name="JobId"></param>
        /// <param name="OrganizationUserId">Organization User Id</param>
        /// <param name="isReviewed">IsReviewed flag</param>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>   
        [HttpPut]
        public IHttpActionResult AcceptParsedJob(Guid Id, Guid JobId, Guid OrganizationUserId, bool isReviewed)
        {
            ParserInfo model = new ParserInfo();
            model.AcceptParsedJob(Id, JobId, OrganizationUserId, isReviewed);
            return Ok();
        }

        /// <summary>
        /// Get Parsed Job Detail information for review.
        /// </summary>
        /// <param name="Id">Id</param>
        /// <param name="OrganizationUserId">Organization User Id</param>
        /// <returns>Job Detail Model</returns>
        [HttpPut]
        public IHttpActionResult ReviewParsedJob(Guid Id, Guid OrganizationUserId)
        {
            ParserInfo model = new ParserInfo();
            return Ok(model.ReviewParsedJob(Id, OrganizationUserId));
        }

        /// <summary>
        /// Mark Parsed Job as rejected
        /// </summary>
        /// <param name="Id">Id</param>        
        [HttpPut]
        public IHttpActionResult RejectParsedJob(Guid Id)
        {
            ParserInfo model = new ParserInfo();
            model.RejectParsedJob(Id);
            return Ok();
        }
    }
}
