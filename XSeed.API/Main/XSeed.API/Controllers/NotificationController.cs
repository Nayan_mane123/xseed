﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XSeed.Business.Notification;
using XSeed.Data.ViewModel.Notification;
using XSeed.Utility;

namespace XSeed.API.Controllers
{
    /// <summary>
    ///Notification Controller
    /// </summary>
    public class NotificationController : BaseController
    {
        /// <summary>
        /// Sends Email Notification
        /// </summary>
        /// <param name="emailNotificationModel">EmailNotificationModel</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>Status Ok - 200</returns>
        public IHttpActionResult SendEmailNotification(EmailNotificationModel emailNotificationModel, Boolean isMobileRequest = false)
        {
            if (ModelState.IsValid)
            {
                NotificationInfo.SendEmailNotification(emailNotificationModel);
                if (isMobileRequest)
                {
                    return Ok(ResponseUtility.SuccessEmptyResponseData("Email Send Successfully"));
                }

                return Ok();
            }
            return BadRequest(ModelState);
        }

        /// <summary>
        /// Sends Email Notification related to job
        /// </summary>
        /// <param name="emailNotificationModel">EmailNotificationModel</param>
        /// /// <param name="jobId">Job Id</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>Status Ok - 200</returns>
        public IHttpActionResult SendJobEmailNotification(EmailNotificationModel emailNotificationModel, Nullable<Guid> jobId,Boolean isMobileRequest=false)
        {
            if (ModelState.IsValid)
            {
                NotificationInfo.SendJobEmailNotification(emailNotificationModel, jobId);
                if (isMobileRequest)
                { 
                    return Ok(ResponseUtility.SuccessEmptyResponseData("Email Send Successfully"));
                }
                return Ok();
            }
            return BadRequest(ModelState);
        }

        /// <summary>
        /// Sends Email Notification related to candidate
        /// </summary>
        /// <param name="emailNotificationModel">EmailNotificationModel</param>
        /// <param name="candidateId"></param>
        /// <param name="isMobileRequest"></param>
      
        /// <returns>Status Ok - 200</returns>
        public IHttpActionResult SendCandidateEmailNotification(EmailNotificationModel emailNotificationModel, string candidateId, Boolean isMobileRequest = false)
        {
            if (ModelState.IsValid)
            {
                NotificationInfo.SendCandidateEmailNotification(emailNotificationModel, candidateId);
                if (isMobileRequest)
                {
                    return Ok(ResponseUtility.SuccessEmptyResponseData("Email Send Successfully"));
                }
                return Ok();
            }
            return BadRequest(ModelState);
        }
    }
}
