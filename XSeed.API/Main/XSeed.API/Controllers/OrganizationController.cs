﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XSeed.API.Models;
using XSeed.Business.Organization;
using XSeed.Data.ViewModel.Organization;
using XSeed.Utility;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Organization API
    /// </summary>
    public class OrganizationController : BaseController
    {
        /// <summary>
        /// Get detail of the organization
        /// </summary>        
        /// <param name="organizationId">Organization Id</param>
        /// <param name="isMobileRequest"></param>
        /// <remarks>Get the details of the organization
        ///</remarks>
        /// <returns>Organization detail</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Organization_Read", ClaimValue = "True")]
        public IHttpActionResult Get(Guid organizationId,Boolean isMobileRequest=false)
        {
            OrganizationInfo model = new OrganizationInfo();
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessResponseData(model.GetOrganizationDetail(organizationId)));
            }
            return Ok(model.GetOrganizationDetail(organizationId));
        }

        /// <summary>
        /// Update the details for the organization
        /// </summary>
        /// <param name="organizationDetailModel">Organization detail model</param>
        /// <param name="isMobileRequest"></param>
        /// <remarks>Update the details about the organization</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPut]
        [ClaimsAuthorization(ClaimType = "Organization_Update", ClaimValue = "True")]
        public IHttpActionResult Put(OrganizationDetailModel organizationDetailModel,Boolean isMobileRequest=false)
        {
            OrganizationInfo model = new OrganizationInfo();
            model.UpdateOrganizationDetail(organizationDetailModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Updated Successfully"));
            }
            
            return Ok();
        }

    }
}