﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XSeed.API.Models;
using XSeed.Business.Submission;
using XSeed.Data.ViewModel.Submission;
using XSeed.Utility;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Submission Status API
    /// </summary>
    public class SubmissionStatusController : BaseController
    {
        /// <summary>
        /// Get submission feedback status list for particular submission
        /// </summary>
        /// <param name="submissionId">Submission Id</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>Submission feedback list</returns>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Submission_Read", ClaimValue = "True")]
        public IHttpActionResult Get(Guid submissionId, Boolean isMobileRequest = false)
        {
            SubmissionFeedbackInfo model = new SubmissionFeedbackInfo();
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessResponseData(model.GetSubmissionFeedbackList(submissionId)));
            }
            return Ok(model.GetSubmissionFeedbackList(submissionId));
        }

        /// <summary>
        /// Add submission feedback
        /// </summary>
        /// <param name="submissionFeedbackModel">Submission feedback model </param>
        /// <param name="isMobileRequest"></param>
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Submission_Update", ClaimValue = "True")]
        public IHttpActionResult Post(SubmissionFeedbackModel submissionFeedbackModel, Boolean isMobileRequest = false)
        {
            SubmissionFeedbackInfo model = new SubmissionFeedbackInfo();
            model.AddsubmissionFeedback(submissionFeedbackModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Saved Successfully"));
            }
            return Ok();
        }
    }
}
