﻿using System;
using System.Web.Http;
using XSeed.Business.Dashboard;
using XSeed.Utility;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Dashboard API
    /// </summary>    
    public class DashboardController : BaseController
    {
        /// <summary>
        /// Get Dashboard Detail
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="userId">user Id</param>
        /// <returns>Dashboard Model</returns>
        [HttpGet]
        public IHttpActionResult Get(Guid organizationId, Guid? userId = null)
        {
            DashboardInfo model = new DashboardInfo();
            return Ok(model.GetDashboardDetail(organizationId, userId)); // return dashboard detail           
        }

        /// <summary>
        /// Get Candidate Chart Detail
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="userId">user Id</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>Candidate Chart Detail</returns>
        [HttpGet]
        public IHttpActionResult GetCandidateChartDetail(Guid organizationId, Guid? userId = null, Boolean isMobileRequest = false)
        {
            DashboardInfo model = new DashboardInfo();
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessResponseData(model.GetCandidateChartDetail(organizationId, userId)));
            }
            return Ok(model.GetCandidateChartDetail(organizationId, userId)); // return Candidate Chart Detail
        }

        /// <summary>
        /// Get Submission Chart Detail
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="userId">user Id</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>Submission Chart Detail</returns>
        [HttpGet]
        public IHttpActionResult GetSubmissionChartDetail(Guid organizationId, Guid? userId = null, Boolean isMobileRequest = false)
        {
            DashboardInfo model = new DashboardInfo();
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessResponseData(model.GetSubmissionChartDetail(organizationId, userId)));
            }
            return Ok(model.GetSubmissionChartDetail(organizationId, userId)); // return Submission Chart Detail
        }

        /// <summary>
        /// Get Job Status Chart Detail
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="userId">user Id</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>Job Status Chart Detail</returns>
        [HttpGet]
        public IHttpActionResult GetJobStatusChartDetail(Guid organizationId, Guid? userId = null, Boolean isMobileRequest = false)
        {
            DashboardInfo model = new DashboardInfo();
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessResponseData(model.GetJobStatusChartDetail(organizationId, userId)));
            }
            return Ok(model.GetJobStatusChartDetail(organizationId, userId)); // return Submission Chart Detail
        }


        /// <summary>
        /// Get job Submission Chart Chart Detail
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="userId">user Id</param>
        /// <param name="type"></param>
        /// <param name="isMobileRequest"></param>
        /// <returns>job Submission Chart Detail</returns>
        [HttpGet]
        public IHttpActionResult GetjobSubmissionChartDetail(Guid organizationId, Guid? userId = null, string type = "Y", Boolean isMobileRequest = false)
        {
            DashboardInfo model = new DashboardInfo();
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessResponseData(model.GetjobSubmissionChartDetail(organizationId, userId, type)));
            }
            return Ok(model.GetjobSubmissionChartDetail(organizationId, userId, type)); // return Job Submission Chart Detail
        }

        /// <summary>
        /// Get dashboard chart periodic data
        /// </summary>
        /// <param name="organizationId"></param>
        /// <param name="userId"></param>
        /// <param name="isMobileRequest"></param>
        /// <returns>dashboard chart periodic data</returns>
        [HttpGet]
        public IHttpActionResult GetDashboardPeriodicData(Guid organizationId, Guid? userId = null, Boolean isMobileRequest = false)
        {
            DashboardInfo model = new DashboardInfo();
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessResponseData(model.GetDashboardPeriodicData(organizationId, userId)));
            }
            return Ok(model.GetDashboardPeriodicData(organizationId, userId));
        }

        /// <summary>
        /// Get Dashboard All Counts
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="isMobileRequest"></param>
        [HttpGet]
        public IHttpActionResult GetAllCounts(Guid organizationId, Boolean isMobileRequest = false)
        {
            DashboardInfo model = new DashboardInfo();
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessResponseData(model.GetAllCounts(organizationId)));
            }
            return Ok(model.GetAllCounts(organizationId)); // return dashboard detail           
        }

        /// <summary>
        /// Get Dashboard All Counts
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        [HttpGet]
        public IHttpActionResult GetCandidateCounts(Guid organizationId)
        {
            DashboardInfo model = new DashboardInfo();
            return Ok(model.GetCandidateCounts(organizationId)); // return dashboard detail           
        }




        /// <summary>
        /// Get Dashboard Activity Log 
        /// </summary>
        /// <returns>Activity Logs</returns>
        [HttpGet]
        public IHttpActionResult GetDashboardJobActivityLog(Guid organizationId, Boolean isMobileRequest = false)
        {
            XSeed.Business.Job.JobInfo model = new XSeed.Business.Job.JobInfo();
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessResponseData(model.GetDashboardJobActivityLog(organizationId)));
            }
            return Ok(model.GetDashboardJobActivityLog(organizationId)); //
        }

        /// <summary>
        /// Get Dashboard Quick Notes
        /// </summary>
        [HttpGet]
        public IHttpActionResult GetDashboardQuickNotes(Guid organizationId, Boolean isMobileRequest = false)
        {
            XSeed.Business.Job.JobInfo model = new XSeed.Business.Job.JobInfo();
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessResponseData(model.GetDashboardQuickNotes(organizationId)));
            }
            return Ok(model.GetDashboardQuickNotes(organizationId)); //
        }
    }
}
