﻿using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using XSeed.Business.Common;
using XSeed.Data.ViewModel.Common;
using XSeed.Utility;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Lookup API
    /// </summary>
    [AllowAnonymous]
    public class LookUpController : BaseController
    {
        #region Common LookUp

        /// <summary>
        /// Get MetaData
        /// </summary>     
        public IHttpActionResult GetMetaData()
        {

            JObject metadata = JObject.Parse(File.ReadAllText(ConfigurationManager.AppSettings["MetadataFilePath"]));
            return Ok(ResponseUtility.SuccessResponseData(metadata));

        }
        /// <summary>
        /// /// Get list of all countries
        /// </summary>        
        /// <remarks>list of all countries
        ///</remarks>
        /// <returns>Country list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult GetCountries(Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    Countries = LookUp.GetCountries().OrderBy(l => l.Name)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetCountries().OrderBy(l => l.Name));
        }
        ///
        /// <summary>
        /// Get state list by country
        /// </summary>        
        /// <param name="countryId">Country Id</param>
        /// <param name="isMobileRequest"></param>
        /// <remarks>list of all states by country
        ///</remarks>
        /// <returns>State list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult GetStates(Guid countryId, Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    States = LookUp.GetStates(countryId).OrderBy(l => l.Name)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetStates(countryId).OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get city list by state
        /// </summary>        
        /// <param name="stateId">State Id</param>
        /// <param name="isMobileRequest"></param>
        /// <remarks>list of all cities by state
        ///</remarks>
        /// <returns>City list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult GetCities(Guid stateId, Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    Cities = LookUp.GetCities(stateId).OrderBy(l => l.Name)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetCities(stateId).OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get industry type list
        /// </summary>        
        /// <remarks>Get industry type list
        ///</remarks>
        /// <returns>Industry type list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult GetIndustryTypes(Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    IndustryTypes = LookUp.GetIndustryTypes()
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetIndustryTypes());
        }

        /// <summary>
        /// Get user roles list
        /// </summary>        
        /// <remarks>Get user roles list
        ///</remarks>
        /// <returns>User role list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult GetUserRoles(Guid organizationId, Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {

                var result = new
                {
                    UserRoles = LookUp.GetUserRoles(organizationId).OrderBy(l => l.Name)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetUserRoles(organizationId).OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get company types
        /// </summary>        
        /// <remarks>Get company types
        ///</remarks>
        /// <returns>Company types</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult GetCompanyTypes(Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    CompanyTypes = LookUp.GetCompanyTypes().OrderBy(l => l.Name)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetCompanyTypes().OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get company sources
        /// </summary>        
        /// <remarks>Get company sources who brought the reference of a company
        ///</remarks>        
        /// <returns>Company sources</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult GetCompanySources(Guid organizationId, Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    CompanySources = LookUp.GetCompanySources(organizationId)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetCompanySources(organizationId));
        }

        /// <summary>
        /// Get title/ suffix list
        /// </summary>        
        /// <remarks>Get title/ suffix list
        ///</remarks>
        /// <returns>Title/ Suffix list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult GetTitles(Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    Titles = LookUp.GetTitles().OrderBy(l => l.Name)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetTitles().OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get reporting authorities
        /// </summary>        
        /// <remarks> Get reporting authorities
        ///</remarks>
        /// <returns>Reporting authorities</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult GetReportingAuthorities(Guid organizationId, Guid? roleId = null, Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    ReportingAuthorities = LookUp.GetReportingAuthorities(organizationId, roleId).OrderBy(l => l.Name)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetReportingAuthorities(organizationId, roleId).OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get job status
        /// </summary>        
        /// <remarks> Get job status like a job is active or closed
        ///</remarks>
        /// <returns>List of job status</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult GetJobStatus(Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    JobStatus = LookUp.GetJobStatus().OrderBy(l => l.Name)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetJobStatus().OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get job priority
        /// </summary>        
        /// <remarks> Get job priority like a job has priority p1,p2,p3
        ///</remarks>
        /// <returns>List of job prorities</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult GetJobPriority(Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    JobPriority = LookUp.GetJobPriority()
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }

            return Ok(LookUp.GetJobPriority());
        }

        /// <summary>
        /// Get job title
        /// </summary>        
        /// <remarks> Get job title which are active or closed
        ///</remarks>
        /// <returns>List of job titles</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult GetJobTitles(Guid organizationId, Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    JobTitle = LookUp.GetJobTitles(organizationId)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetJobTitles(organizationId));
        }
        /// <summary>
        /// Create job title
        /// </summary>
        [HttpPost]
        public IHttpActionResult CreateJobTitles(LookUpModel lookUpModel, Boolean isMobileRequest = false)
        {
            LookUp.CreateJobTitles(lookUpModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Created Successfully"));
            }
            return Ok();
        }
        /// <summary>
        /// Update job title
        /// </summary>
        [HttpPut]
        public IHttpActionResult UpdateJobTitles(LookUpModel lookUpModel, Boolean isMobileRequest = false)
        {
            LookUp.UpdateJobTitles(lookUpModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Updated Successfully"));
            }
            return Ok();
        }
        /// <summary>
        /// delete job title
        /// </summary>
        [HttpDelete]
        public IHttpActionResult DeleteJobTitles(Guid genericLookupId, Boolean isMobileRequest = false)
        {
            if (genericLookupId == Guid.Empty)
            {
                throw new ArgumentNullException("genericLookupId");
            }

            LookUp.DeleteJobTitles(genericLookupId);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Deleted Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Get job type
        /// </summary>        
        /// <remarks> Get job type of like a job is full time or part time
        ///</remarks>
        /// <returns>List of job type</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult GetJobTypes(Guid organizationId, Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    JobTypes = LookUp.GetJobTypes(organizationId)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetJobTypes(organizationId));
        }

        /// <summary>
        /// create job type
        /// </summary>
        /// <param name="lookUpModel"></param>
        /// <param name="isMobileRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult CreateJobTypes(LookUpModel lookUpModel, Boolean isMobileRequest = false)
        {
            LookUp.CreateJobTypes(lookUpModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Created Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lookUpModel"></param>
        /// <param name="isMobileRequest"></param>
        /// <returns></returns>
        [HttpPut]
        public IHttpActionResult UpdateJobTypes(LookUpModel lookUpModel, Boolean isMobileRequest = false)
        {
            LookUp.UpdateJobTypes(lookUpModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Updated Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="genericLookupId"></param>
        /// <param name="isMobileRequest"></param>
        /// <returns></returns>
        [HttpDelete]
        public IHttpActionResult DeleteJobTypes(Guid genericLookupId, Boolean isMobileRequest = false)
        {
            if (genericLookupId == Guid.Empty)
            {
                throw new ArgumentNullException("genericLookupId");
            }

            LookUp.DeleteJobTypes(genericLookupId);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Deleted Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Get skills
        /// </summary>        
        /// <remarks> Get list of skills
        ///</remarks>
        /// <returns>List of skills</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult GetSkills(Boolean isMobileRequest = false)
        {

            if (isMobileRequest)
            {
                var result = new
                {
                    Skills = LookUp.GetSkills()
                };
                return Ok(ResponseUtility.SuccessResponseData(result));

            }
            else
            {
                return Ok(LookUp.GetSkills()); // return all Job opening list
            }
        }

        /// <summary>
        /// Get degrees
        /// </summary>        
        /// <remarks> Get list of degrees 
        ///</remarks>
        /// <returns>List of degrees</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult GetDegrees(Guid organizationId, Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    Degrees = LookUp.GetDegrees(organizationId)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetDegrees(organizationId));
        }

        /// <summary>
        /// Create Job Degrees
        /// </summary>
        /// <param name="lookUpModel"></param>
        /// <param name="isMobileRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult CreateJobDegrees(LookUpModel lookUpModel, Boolean isMobileRequest = false)
        {
            LookUp.CreateJobDegrees(lookUpModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Created Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Create Job Visa Types
        /// </summary>
        /// <param name="lookUpModel">Visa Types </param>
        /// <param name="isMobileRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult CreateJobVisaType(LookUpModel lookUpModel, Boolean isMobileRequest = false)
        {
            LookUp.CreateJobVisaType(lookUpModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Created Successfully"));
            }
            return Ok();
        }


        /// <summary>
        /// Update Job Degrees
        /// </summary>
        /// <param name="lookUpModel"></param>
        /// <param name="isMobileRequest"></param>
        /// <returns></returns>
        [HttpPut]
        public IHttpActionResult UpdateJobDegrees(LookUpModel lookUpModel, Boolean isMobileRequest = false)
        {
            LookUp.UpdateJobDegrees(lookUpModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Updated Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Update Job Visa Type
        /// </summary>
        /// <param name="lookUpModel"></param>
        /// <param name="isMobileRequest"></param>
        /// <returns></returns>
        [HttpPut]
        public IHttpActionResult UpdateJobVisaType(LookUpModel lookUpModel, Boolean isMobileRequest = false)
        {
            LookUp.UpdateJobVisaType(lookUpModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Updated Successfully"));
            }
            return Ok();
        }


        /// <summary>
        /// Delete Job Degrees
        /// </summary>
        /// <param name="genericLookupId"></param>
        /// <param name="isMobileRequest"></param>
        /// <returns></returns>
        [HttpDelete]
        public IHttpActionResult DeleteJobDegrees(Guid genericLookupId, Boolean isMobileRequest = false)
        {
            if (genericLookupId == Guid.Empty)
            {
                throw new ArgumentNullException("genericLookupId");
            }

            LookUp.DeleteJobDegrees(genericLookupId);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Deleted Successfully"));
            }
            return Ok();
        }


        /// <summary>
        /// Check for Duplicate Lookup Name
        /// </summary>
        /// <param name="lookupName"> lookup Name</param>
        /// <param name="lookupType"> lookup Type</param>
        /// <param name="organizationId"> organization Id</param>
        /// <param name="editedValue"></param>
        /// <param name="existingValue"></param>
        /// <param name="isMobileRequest"></param>
        /// <returns>True/ False</returns>
        [HttpGet]
        public IHttpActionResult IsDuplicateLookupName(string lookupName, string lookupType, Guid organizationId, bool editedValue, string existingValue = "", Boolean isMobileRequest = false)
        {
            LookUp model = new LookUp();
            if (isMobileRequest)
            {
                var result = new
                {
                    IsDuplicateLookup = model.IsDuplicateLookupName(lookupName, lookupType, organizationId, editedValue, existingValue)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(model.IsDuplicateLookupName(lookupName, lookupType, organizationId, editedValue, existingValue));
        }

        /// <summary>
        /// Get genders
        /// </summary>        
        /// <remarks> Get list of genders 
        ///</remarks>
        /// <returns>List of genders</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult GetGenders(Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    Genders = LookUp.GetGenders().OrderBy(l => l.Name)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetGenders().OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get marital status
        /// </summary>        
        /// <remarks> Get list of marital status
        ///</remarks>
        /// <returns>List of marital status</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        public IHttpActionResult GetMaritalStatus(Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    MaritalStatus = LookUp.GetMaritalStatus().OrderBy(l => l.Name)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetMaritalStatus().OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get visa types
        /// </summary>        
        /// <remarks> Get list of visa types which are required to work in other countries
        ///</remarks>
        /// <returns>List of visa types</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        public IHttpActionResult GetVisaTypes(Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    VisaTypes = LookUp.GetVisaTypes().OrderBy(l => l.Name)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetVisaTypes().OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get submission feedback status
        /// </summary>        
        /// <remarks> Get list submission feedback status required to fill the feedback of submission
        ///</remarks>
        /// <returns>Submission feedback status list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        public IHttpActionResult GetSubmissionFeedbackStatus(Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    SubmissionFeedbackStatus = LookUp.GetSubmissionFeedbackStatus().OrderBy(l => l.Name)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetSubmissionFeedbackStatus().OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get client feedback Status
        /// </summary>        
        /// <remarks> Get list feedback status required to fill the feedback of client
        ///</remarks>
        /// <returns>Client feedback Status list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        public IHttpActionResult GetClientFeedbackStatus(Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    ClientFeedbackStatus = LookUp.GetClientFeedbackStatus().OrderBy(l => l.Name)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetClientFeedbackStatus().OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get candidate feedback status
        /// </summary>        
        /// <remarks> Get list feedback status required to fill the feedback of candidate
        ///</remarks>
        /// <returns>Candidate feedback status list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        public IHttpActionResult GetCandidateFeedbackStatus(Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    CandidateFeedbackStatus = LookUp.GetCandidateFeedbackStatus().OrderBy(l => l.Name)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetCandidateFeedbackStatus().OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get company name for lookup
        /// </summary>        
        /// <remarks> Get company names of organization
        ///</remarks>
        /// <param name="organizationId">organization Id</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>Company names</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        public IHttpActionResult GetCompanies(Guid organizationId, Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    Companies = LookUp.GetCompanies(organizationId).OrderBy(l => l.Name)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetCompanies(organizationId).OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get Organization Users
        /// </summary>  
        /// <param name="organizationId">Organization Id</param>
        /// <param name="isMobileRequest"></param>
        /// <remarks> Get list of users belongs to an Organization
        ///</remarks>
        /// <returns>Organization user list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        public IHttpActionResult GetOrganizationUsers(Guid organizationId, Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    OrganizationUsers = LookUp.GetOrganizationUsers(organizationId).OrderBy(l => l.Name)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetOrganizationUsers(organizationId).OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get Business Units
        /// </summary>        
        /// <returns>Business Units</returns>
        public IHttpActionResult GetBusinessUnits(Guid organizationId, Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    BusinessUnits = LookUp.GetBusinessUnits(organizationId)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetBusinessUnits(organizationId));
        }

        /// <summary>
        /// Get candidate status
        /// </summary>        
        /// <remarks> Get candidate status 
        ///</remarks>
        /// <returns>List of candidate status</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult GetCandidateStatus(Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    CandidateStatus = LookUp.GetCandidateStatus().OrderBy(l => l.Name)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetCandidateStatus().OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get candidate categories
        /// </summary>        
        /// <remarks>Get candidate categories
        ///</remarks>        
        /// <returns>categories</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult GetCandidateCategories(Guid organizationId, Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    CandidateCategories = LookUp.GetCandidateCategories(organizationId)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetCandidateCategories(organizationId));
        }

        /// <summary>
        /// Create New Candidate Status
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpPost]
        public IHttpActionResult CreateCandidateStatus(LookUpModel lookUpModel, Boolean isMobileRequest = false)
        {
            LookUp.CreateCandidateStatus(lookUpModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Created Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Update Candidate Status
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpPut]
        public IHttpActionResult UpdateCandidateStatus(LookUpModel lookUpModel, Boolean isMobileRequest = false)
        {
            LookUp.UpdateCandidateStatus(lookUpModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Updated Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Deletes Candidate Status
        /// </summary>
        /// <param name="genericLookupId"></param>
        /// <param name="isMobileRequest"></param>        
        /// <returns>HTTP OK Resopnse</returns>
        [HttpDelete]
        public IHttpActionResult DeleteCandidateStatus(Guid genericLookupId, Boolean isMobileRequest = false)
        {
            if (genericLookupId == Guid.Empty)
            {
                throw new ArgumentNullException("genericLookupId");
            }

            LookUp.DeleteCandidateStatus(genericLookupId);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Deleted Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Create New Candidate Category
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpPost]
        public IHttpActionResult CreateCandidateCategory(LookUpModel lookUpModel, Boolean isMobileRequest = false)
        {
            LookUp.CreateCandidateCategory(lookUpModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Created Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Update Candidate Category
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpPut]
        public IHttpActionResult UpdateCandidateCategory(LookUpModel lookUpModel, Boolean isMobileRequest = false)
        {
            LookUp.UpdateCandidateCategory(lookUpModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Updated Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Deletes Candidate Category
        /// </summary>
        /// <param name="genericLookupId"></param>
        /// <param name="isMobileRequest"></param>        
        /// <returns>HTTP OK Resopnse</returns>
        [HttpDelete]
        public IHttpActionResult DeleteCandidateCategory(Guid genericLookupId, Boolean isMobileRequest = false)
        {
            if (genericLookupId == Guid.Empty)
            {
                throw new ArgumentNullException("genericLookupId");
            }

            LookUp.DeleteCandidateCategory(genericLookupId);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Deleted Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Get candidate groups
        /// </summary>        
        /// <remarks>Get candidate groups
        ///</remarks>        
        /// <returns>Groups</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult GetCandidateGroups(Guid organizationId, Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    CandidateGroups = LookUp.GetCandidateGroups(organizationId)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetCandidateGroups(organizationId));
        }

        /// <summary>
        /// Create New Candidate Group
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpPost]
        public IHttpActionResult CreateCandidateGroup(LookUpModel lookUpModel, Boolean isMobileRequest = false)
        {
            LookUp.CreateCandidateGroup(lookUpModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Created Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Update Candidate Group
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpPut]
        public IHttpActionResult UpdateCandidateGroup(LookUpModel lookUpModel, Boolean isMobileRequest = false)
        {
            LookUp.UpdateCandidateGroup(lookUpModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Updated Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Deletes Candidate Group
        /// </summary>
        /// <param name="genericLookupId"></param>
        /// <param name="isMobileRequest"></param>      
        /// <returns>HTTP OK Resopnse</returns>
        [HttpDelete]
        public IHttpActionResult DeleteCandidateGroup(Guid genericLookupId, Boolean isMobileRequest = false)
        {
            if (genericLookupId == Guid.Empty)
            {
                throw new ArgumentNullException("genericLookupId");
            }

            LookUp.DeleteCandidateGroup(genericLookupId);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Deleted Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Get candidate sources
        /// </summary>        
        /// <returns>Sources</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult GetCandidateSources(Guid organizationId, Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    CandidateSources = LookUp.GetCandidateSources(organizationId)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }

            return Ok(LookUp.GetCandidateSources(organizationId));
        }

        /// <summary>
        /// Create New Candidate Source
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpPost]
        public IHttpActionResult CreateCandidateSource(LookUpModel lookUpModel, Boolean isMobileRequest = false)
        {
            LookUp.CreateCandidateSource(lookUpModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Created Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Update Candidate Source
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpPut]
        public IHttpActionResult UpdateCandidateSource(LookUpModel lookUpModel, Boolean isMobileRequest = false)
        {
            LookUp.UpdateCandidateSource(lookUpModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Updated Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Deletes Candidate Source
        /// </summary>
        /// <param name="genericLookupId"></param>
        /// <param name="isMobileRequest"></param>        
        /// <returns>HTTP OK Resopnse</returns>
        [HttpDelete]
        public IHttpActionResult DeleteCandidateSource(Guid genericLookupId, Boolean isMobileRequest = false)
        {
            if (genericLookupId == Guid.Empty)
            {
                throw new ArgumentNullException("genericLookupId");
            }

            LookUp.DeleteCandidateSource(genericLookupId);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Deleted Successfully"));
            }
            return Ok();
        }


        /// <summary>
        /// Get candidate Languages
        /// </summary>            
        /// <returns>Language</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult GetLanguages(Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    Languages = LookUp.GetLanguages().OrderBy(l => l.Name)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetLanguages().OrderBy(l => l.Name));
        }

        /// <summary>
        /// Create New Candidate Language
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpPost]
        public IHttpActionResult CreateLanguage(LookUpModel lookUpModel, Boolean isMobileRequest = false)
        {
            LookUp.CreateLanguage(lookUpModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Created Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Update Candidate Language
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpPut]
        public IHttpActionResult UpdateLanguage(LookUpModel lookUpModel, Boolean isMobileRequest = false)
        {
            LookUp.UpdateLanguage(lookUpModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Updated Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Deletes Candidate Language
        /// </summary>
        /// <param name="genericLookupId"></param>
        /// <param name="isMobileRequest"></param>        
        /// <returns>HTTP OK Resopnse</returns>
        [HttpDelete]
        public IHttpActionResult DeleteLanguage(Guid genericLookupId, Boolean isMobileRequest = false)
        {
            if (genericLookupId == Guid.Empty)
            {
                throw new ArgumentNullException("genericLookupId");
            }

            LookUp.DeleteLanguage(genericLookupId);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Deleted Successfully"));
            }
            return Ok();
        }


        /// <summary>
        /// Get Salary Types
        /// </summary>        
        /// <remarks> Get Salary Types
        ///</remarks>
        /// <returns>List of Salary Type</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult GetSalaryType(Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    SalaryType = LookUp.GetSalaryType()
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetSalaryType());
        }

        /// <summary>
        /// Get Currency Types
        /// </summary>        
        /// <remarks> Get Currency Types
        ///</remarks>
        /// <returns>List of Currency Type</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult GetCurrencyType(Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    CurrencyType = LookUp.GetCurrencyType()
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.GetCurrencyType());
        }
        /// <summary>
        /// Create City
        /// </summary>  
        [HttpPost]
        public IHttpActionResult CreateCity(string city, Guid stateId, Boolean isMobileRequest = false)
        {
            if (!string.IsNullOrEmpty(city) && stateId != null)
            {
                if (isMobileRequest)
                {
                    return Ok(ResponseUtility.SuccessResponseData(LookUp.CreateCity(city, stateId)));
                }
                return Ok(LookUp.CreateCity(city, stateId));
            }
            return BadRequest();
        }

        #endregion

        #region Conditional LookUp

        /// <summary>
        /// Get Company Contacts
        /// </summary>
        /// <param name="companyId">CompanyId</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>Company contact list</returns>
        public IHttpActionResult GetCompanyContacts(Guid companyId, Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    CompanyContacts = LookUp.ListCompanyContacts(companyId)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(LookUp.ListCompanyContacts(companyId)); // return company contact list
        }

        /// <summary>
        /// Get Company Contact emails
        /// </summary>
        /// <param name="companyId">CompanyId</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>Company contact email list</returns>
        public IHttpActionResult GetSubmissionContactEmail(Guid companyId, Boolean isMobileRequest = false)
        {
            if (isMobileRequest)
            {
                var result = new
                {
                    SubmissionContactEmail = LookUp.ListSubmissionContactEmail(companyId)
                };
                return Ok(ResponseUtility.SuccessResponseData(result)); // return company contact list

            }
            return Ok(LookUp.ListSubmissionContactEmail(companyId)); // return company contact list
        }

        /// <summary>
        /// Get City and State code based on country 
        /// </summary>
        /// <param name="country">country</param>
        /// <param name="city">city</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>City and State Code</returns>
        public IHttpActionResult GetCityStateCode(string country, string city, Boolean isMobileRequest = false)
        {
            var result = new
            {
                StateId = LookUp.GetStateCode(country, city),
                CityId = LookUp.GetCityCode(country, city)
            };
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(result); // return City and State code
        }

        #endregion
    }
}