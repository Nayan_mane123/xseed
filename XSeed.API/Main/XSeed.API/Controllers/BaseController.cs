﻿using Microsoft.Owin.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using XSeed.API.Models;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    [Authorize]
    [ValidateModelStateAttribute]
    [CustomAuthorizeAttribute]
    [CustomExceptionFilter]
    public class BaseController : ApiController
    {
        
    }
}
