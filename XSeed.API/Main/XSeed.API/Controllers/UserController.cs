﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XSeed.API.Models;
using XSeed.Business.User.UserCommon;
using XSeed.Data.ViewModel.User.UserCommon;
using XSeed.Utility;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// User role management API
    /// </summary>
    public class UserController : BaseController
    {

        /// <summary>
        /// Get list of all user roles belongs to an organization
        /// </summary>        
        /// <remarks>Get user roles of the organization 
        ///</remarks>
        /// <returns>User role list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Organization_Read", ClaimValue = "True")]
        public IHttpActionResult GetAllUserRoles(Guid organizationId,Boolean isMobileRequest=false)
        {
            UserCommon model = new UserCommon();
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessResponseData(model.GetAllUserRoles(organizationId).OrderBy(x => x.Precedence).ThenBy(x => x.Role).ToList()));
            }
            return Ok(model.GetAllUserRoles(organizationId).OrderBy(x => x.Precedence).ThenBy(x => x.Role).ToList());
        }

        /// <summary>
        /// Get user roles belongs to an organization by their Id
        /// </summary>        
        /// <param name="Id">Role Id</param>
        /// <remarks>Get user roles by their Id
        ///</remarks>
        /// <returns>user role detail </returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Organization_Read", ClaimValue = "True")]
        public IHttpActionResult GetUserRoleById(Guid Id)
        {
            UserCommon model = new UserCommon();
            return Ok(model.GetUserRoleById(Id));
        }

        /// <summary>
        /// Create user role for an organization
        /// </summary>
        /// <param name="userRoleModel">User role model</param>
        /// <param name="isMobileRequest"></param>
        /// <remarks>Create user role for an organization</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Organization_Create", ClaimValue = "True")]
        public IHttpActionResult CreateUserRole(UserRoleModel userRoleModel,Boolean isMobileRequest=false)
        {
            UserCommon model = new UserCommon();
            model.CreateUserRole(userRoleModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Created Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Update user role an organization user
        /// </summary>
        /// <param name="userRoleModel">User role model</param>
        /// <param name="isMobileRequest"></param>
        /// <remarks>Update a role of an user of an organization </remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPut]
        [ClaimsAuthorization(ClaimType = "Organization_Update", ClaimValue = "True")]
        public IHttpActionResult UpdateUserRole(UserRoleModel userRoleModel,Boolean isMobileRequest=false)
        {
            UserCommon model = new UserCommon();

            model.UpdateUserRole(userRoleModel);

            AccountController account = new AccountController();
            account.UpdateAllUserClaimsByUserRole(userRoleModel.Id);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Updated Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Delete user role an organization
        /// </summary>
        /// <param name="Id">User role id</param>
        /// <remarks>Update a role of an organization </remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpDelete]
        [ClaimsAuthorization(ClaimType = "Organization_Delete", ClaimValue = "True")]
        public IHttpActionResult DeleteUserRole(Guid Id)
        {
            UserCommon model = new UserCommon();
            model.DeleteUserRole(Id);
            return Ok();
        }

        /// <summary>
        /// Get User Email Configuration
        /// </summary>
        /// <param name="Id">UserId</param>
        /// <returns>Status Ok - 200</returns>
        [HttpGet]
        public IHttpActionResult GetUserEmailConfiguration(Guid Id)
        {
            return Ok(UserCommon.GetUserEmailConfiguration(Id));
        }

        /// <summary>
        /// Save User Email Configuration
        /// </summary>
        /// <param name="emailConfigurationDetailModel">Email Configuration Detail Model</param>
        /// <returns>Status Ok - 200</returns>
        [HttpPost]
        public IHttpActionResult SaveUserEmailConfiguration(EmailConfigurationDetailModel emailConfigurationDetailModel)
        {
            if (ModelState.IsValid)
            {
                UserCommon.SaveUserEmailConfiguration(emailConfigurationDetailModel);
            }
            else
            {
                return BadRequest(ModelState);
            }
            return Ok();
        }

        /// <summary>
        /// Update User Email Configuration
        /// </summary>
        /// <param name="emailConfigurationDetailModel">EmailConfigurationDetailModel</param>
        /// <returns>Status Ok - 200</returns>
        [HttpPut]
        public IHttpActionResult UpdateUserEmailConfiguration(EmailConfigurationDetailModel emailConfigurationDetailModel)
        {
            if (ModelState.IsValid)
            {
                UserCommon.UpdateUserEmailConfiguration(emailConfigurationDetailModel);
            }
            else
            {
                return BadRequest(ModelState);
            }
            return Ok();
        }
    }
}