﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using XSeed.Business.ResumeBlaster;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Notification;
using XSeed.Data.ViewModel.ResumeBlaster;
using XSeed.Utility;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Resume Blaster Controller
    /// </summary>
    public class ResumeBlasterController : BaseController
    {
        private readonly XSeedEntities db = new XSeedEntities();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortBy"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public IHttpActionResult Get(Guid? Id = null, int pageSize = 0, int pageNumber = 0, string sortBy = "ModifiedOn", string sortOrder = "desc")
        {
            ResumeBlasterInfo model = new ResumeBlasterInfo();

            if (Id == null)
            {
                var paginationInfo = model.GetPaginationInfo(pageSize);

                var result = new
                {
                    TotalCount = paginationInfo.TotalCount,
                    totalPages = paginationInfo.TotalPages,
                    Blasters = model.ListResumeBlasters(pageSize, pageNumber, sortBy, sortOrder)
                };

                return Ok(result); // return recruiter list

            }
            else
            {
                return Ok(model.GetResumeBlasterDetail(Id)); // return single recruiter
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public IHttpActionResult BlastMail(SendResumeMail model)
        {

            if (model.Resume != null)
            {


                foreach (var item in model.ResumeBlasterList)
                {
                    EmailNotificationModel emailNotificationModel = new EmailNotificationModel();
                    emailNotificationModel.ToEmailID = new List<string>();
                    emailNotificationModel.ToEmailID.Add(item);
                    emailNotificationModel.FromEmailID = ConfigurationManager.AppSettings["EmailFromUserName"];
                    emailNotificationModel.FromEmailPassword = ConfigurationManager.AppSettings["EmailFromPassword"];
                    emailNotificationModel.MailAttachment = new List<XSeedFileEntity>();
                    emailNotificationModel.MailAttachment.Add(model.Resume);
                    emailNotificationModel.TypeOfNotification = XSeed.Utility.Constants.typeOfNotificationResumeAttachment;
                    NotificationController notificationController = new NotificationController();
                    notificationController.SendEmailNotification(emailNotificationModel);
                }



            }

            return Ok(); // return single recruiter
        }

        /// <summary>
        /// POST api/ResumeBlaster
        /// </summary>

        [HttpPost]
        public IHttpActionResult Post(ResumeBlasterDetailModel resumeBlasterDetailModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ResumeBlasterInfo model = new ResumeBlasterInfo();
            return Ok(model.CreateResumeBlasterDetail(resumeBlasterDetailModel));

        }

        /// <summary>
        /// PUT api/ResumeBlaster/5
        /// </summary>
       
        [HttpPut]
        public IHttpActionResult Put(ResumeBlasterDetailModel resumeBlasterDetailModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ResumeBlasterInfo model = new ResumeBlasterInfo();
            model.UpdateResumeBlasterDetail(resumeBlasterDetailModel);
            return Ok();
        }

        private string GetMimeType(string fileName)
        {
            string mimeType = "application/unknown";
            string ext = System.IO.Path.GetExtension(fileName).ToLower();
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey != null && regKey.GetValue("Content Type") != null)
                mimeType = regKey.GetValue("Content Type").ToString();
            return mimeType;
        }


    }
}