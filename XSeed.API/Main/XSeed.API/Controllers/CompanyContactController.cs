﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XSeed.API.Models;
using XSeed.Business.Company;
using XSeed.Data.ViewModel.Company;
using XSeed.Utility;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Company contact API
    /// </summary>
    public class CompanyContactController : BaseController
    {

        /// <summary>
        /// Get contact(s) belongs to a company
        /// </summary>        
        /// <param name="companyId">Company Id</param>
        /// <param name="Id">Id</param>
        /// <param name="isMobileRequest"></param>
        /// <remarks>Get company contact/ Company contacts
        /// - Company Id is required parameter
        /// - Id is an optional parameter
        /// - Id with value as zero will return list of contacts belongs to a company
        /// - Id with value as positive integer will return single contact belongs to a company
        ///</remarks>
        /// <returns>Company contact(s)</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>  
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Company_Read", ClaimValue = "True")]
        public IHttpActionResult Get(Guid companyId, Guid? Id = null, Boolean isMobileRequest = false)
        {
            CompanyContactInfo model = new CompanyContactInfo();

            if (Id == null)
            {
                if (isMobileRequest)
                {
                    return Ok(ResponseUtility.SuccessResponseData(model.ListCompanyContacts(companyId))); // return company contact list
                }
                return Ok(model.ListCompanyContacts(companyId)); // return company contact list
            }
            else
            {
                if (isMobileRequest)
                {
                    return Ok(ResponseUtility.SuccessResponseData(model.GetCompanyContactModel(Id, companyId))); // return company contact list
                }
                return Ok(model.GetCompanyContactModel(Id, companyId)); // return single company contact
            }
        }

    

        /// <summary>
        /// Create a new contact for company 
        /// </summary>
        /// <param name="companyContactModel">Company contact model </param>
        /// <param name="isMobileRequest"></param>
        /// <remarks>Create a new contact company</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>  
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Company_Create", ClaimValue = "True")]
        public IHttpActionResult Post(CompanyContactModel companyContactModel, Boolean isMobileRequest = false)
        {
            CompanyContactInfo model = new CompanyContactInfo();
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessResponseData(model.CreateCompanyContact(companyContactModel)));
            }
            return Ok(model.CreateCompanyContact(companyContactModel));
        }

        /// <summary>
        /// Update the details for the organization
        /// </summary>
        /// <param name="companyContactModel">Company contact model </param>
        /// <param name="isMobileRequest"></param>
        /// 

        /// <summary>
        /// Update the details for the company contact
        /// </summary>
       
        /// <remarks>Update the details about the contact</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>

        public IHttpActionResult Put(CompanyContactModel companyContactModel, Boolean isMobileRequest = false)
        {
            CompanyContactInfo model = new CompanyContactInfo();
            model.UpdateCompanyContact(companyContactModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Updated Successfully"));
            }
            return Ok();
        }
    }
}