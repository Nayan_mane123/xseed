﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using XSeed.API.Models;
using XSeed.Business.Submission;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Job;
using XSeed.Data.ViewModel.Submission;
using XSeed.Data.ViewModel.Activity;
using XSeed.Utility;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Submission API
    /// </summary>
    public class SubmissionController : BaseController
    {

        /// <summary>
        /// Get submission(s)
        /// </summary>        
        /// <param name="organizationId">Organization Id</param>
        /// <param name="Id">Id</param>
        /// <param name="isMobileRequest"></param>
        /// <remarks>Get list of submission(s)
        /// - Organization Id is required parameter
        /// - Id is an optional parameter
        /// - Id with value as zero will return list submissions
        /// - Id with value as positive integer will return single submission details
        ///</remarks>
        /// <returns>Submission(s)</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Submission_Read", ClaimValue = "True")]
        public IHttpActionResult Get(Guid organizationId, Guid? Id = null, Boolean isMobileRequest = false)
        {
            SubmissionInfo model = new SubmissionInfo();

            if (Id == null)
            {
                if (isMobileRequest)
                {
                    var result = new
                    {
                        Submissions = model.GetSubmissions(organizationId)
                    };
                    return Ok(ResponseUtility.SuccessResponseData(result));
                }
                else
                {
                    return Ok(model.GetSubmissions(organizationId)); // return submission list
                }

            }
            else
            {
                if (isMobileRequest)
                {
                    var result = new
                    {
                        Submissions = model.GetSubmissionDetail(organizationId, Id)
                    };
                    return Ok(ResponseUtility.SuccessResponseData(result));
                }
                else
                {
                    return Ok(model.GetSubmissionDetail(organizationId, Id)); // return single submission
                }
            }

        }

        /// <summary>
        /// Get list of submission using server side pagination
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>
        /// <param name="sortBy"></param>
        /// <param name="sortOrder"></param>
        /// <param name="isExport"></param>
        /// <param name="loadPageData"></param>
        /// <param name="isMobileRequest"></param>        
        /// <returns>submission List</returns>
        // GET: api/submission/pageSize/pageNumber/
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Submission_Read", ClaimValue = "True")]
        public IHttpActionResult Get(Guid organizationId, int pageSize, int pageNumber, string sortBy = "ModifiedOn", string sortOrder = "desc", bool isExport = false, bool loadPageData = false, Boolean isMobileRequest = false)
        {
            SubmissionInfo model = new SubmissionInfo();

            if (loadPageData)
            {
                /* Set Pagination Info */
                var paginationInfo = model.GetPaginationInfo(organizationId, pageSize);
                HttpContext.Current.Response.AppendHeader("TotalCount", paginationInfo.TotalCount.ToString());
                HttpContext.Current.Response.AppendHeader("TotalPages", paginationInfo.TotalPages.ToString());


                var result = new
                {
                    TotalCount = paginationInfo.TotalCount,
                    totalPages = paginationInfo.TotalPages,
                    Submissions = model.GetSubmissions(organizationId, pageSize, pageNumber, sortBy, sortOrder, isExport)
                };
                if (isMobileRequest)
                {
                    return Ok(ResponseUtility.SuccessResponseData(result));
                }
                return Ok(result); // return candidate list
            }
            else
            {
                var result = new
                {
                    Submissions = model.GetSubmissions(organizationId, pageSize, pageNumber, sortBy, sortOrder, isExport)
                };
                if (isMobileRequest)
                {
                    return Ok(ResponseUtility.SuccessResponseData(result));
                }
                return Ok(result); // return candidate list
            }
        }

        /// <summary>
        /// Create new submission
        /// </summary>
        /// <param name="submissionDetailModel">Submission detail model </param>
        /// <param name="isMobileRequest"></param>
        /// <remarks>Create new submission</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Submission_Create", ClaimValue = "True")]
        public IHttpActionResult Post(SubmissionModel submissionDetailModel, Boolean isMobileRequest = false)
        {
            List<string> successCandidateList = new List<string>();
            string message = string.Empty;

            SubmissionInfo model = new SubmissionInfo();
            model.CreateSubmissionDetail(submissionDetailModel, out successCandidateList, out message);

            var result = new
            {
                successCandidates = successCandidateList,
                message = message
            };
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(result);
        }

        /// <summary>
        /// Send Submission E-Mails
        /// </summary>
        /// <param name="submissionMailModel">Submission Mail Model</param>
        /// <param name="isMobileRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Submission_Create", ClaimValue = "True")]
        public IHttpActionResult SendSubmissionMail(SubmissionMailModel submissionMailModel, Boolean isMobileRequest = false)
        {
            SubmissionInfo model = new SubmissionInfo();
            model.SendSubmissionMail(submissionMailModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Sent Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Save Quick Note related to submission
        /// </summary>
        /// <param name="note">Note</param>
        /// <param name="submissionId">Submission Id</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>Notes</returns>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Submission_Create", ClaimValue = "True")]
        public IHttpActionResult SaveQuickNote(string note, Guid submissionId, Boolean isMobileRequest = false)
        {
            SubmissionInfo model = new SubmissionInfo();
            if (isMobileRequest)
            {
                var result = new
                {
                    notes = model.SaveQuickNote(note, submissionId)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(model.SaveQuickNote(note, submissionId));
        }

        /// <summary>
        /// Get Quick Notes related to submission
        /// </summary>
        /// <param name="submissionId">Submission Id</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>Quick Notes</returns>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Submission_Read", ClaimValue = "True")]
        public IHttpActionResult GetQuickNotes(Guid submissionId, Boolean isMobileRequest = false)
        {
            SubmissionInfo model = new SubmissionInfo();
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessResponseData(model.GetQuickNotes(submissionId)));
            }
            return Ok(model.GetQuickNotes(submissionId));
        }
        /// <summary>
        /// Delete Quick Notes
        /// </summary>
        [HttpDelete]
        public IHttpActionResult DeleteQuickNotes(string qnoteId, Boolean isMobileRequest = false)
        {
            if (qnoteId == null)
            {
                throw new ArgumentNullException("qnoteId");
            }

            SubmissionInfo model = new SubmissionInfo();
            model.DeleteQuickNotes(qnoteId);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Deleted Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Update QNote Submission
        /// </summary>
        [HttpPut]
        public IHttpActionResult UpdateQNoteSubmission(QNoteModel request, string editid = null, Boolean isMobileRequest = false)
        {
            SubmissionInfo model = new SubmissionInfo();

            model.UpdateQNote(request, editid);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Updated Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Submit sourced candidates against selected Job.
        /// </summary>
        /// <param name="candidateSourceResultModelList">Candidate Source Result Model</param>
        /// <param name="isMobileRequest"></param>
        /// <remarks>Create new submission</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Submission_Create", ClaimValue = "True")]
        public IHttpActionResult SubmitSourcedCandidates(List<CandidateSourceResultModel> candidateSourceResultModelList, Boolean isMobileRequest = false)
        {
            SubmissionInfo model = new SubmissionInfo();
            List<MongoLookUpModel> candidates = model.SubmitSourcedCandidates(candidateSourceResultModelList);
            if (isMobileRequest)
            {
                var result = new
                {
                    SubmitSourcedCandidates = candidates
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(candidates);
        }

    }
}
