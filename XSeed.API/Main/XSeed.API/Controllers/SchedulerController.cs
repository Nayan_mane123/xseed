﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XSeed.Business.Common;
using XSeed.Data.ViewModel.Common;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Scheduler Controller
    /// </summary>
    public class SchedulerController : BaseController
    {
        /// <summary>
        /// Get list/ single scheduled log(s)
        /// </summary>
        /// <param name="OrganizationUserId">Organization User Id</param>
        /// <param name="Id"> Schedule Log Id</param>        
        /// <returns>Schedule Log(s)</returns>
        [HttpGet]
        public IHttpActionResult GetScheduleLog(Guid OrganizationUserId, string Id = null)
        {
            SchedulerInfo model = new SchedulerInfo();

            if (string.IsNullOrEmpty(Id))
            {
                return Ok(model.ListSchedules(OrganizationUserId)); // return schedule list
            }
            else
            {
                return Ok(model.GetScheduleDetail(OrganizationUserId, Id)); // return single schedules
            }
        }

        /// <summary>
        /// Update schedule detail
        /// </summary>
        /// <param name="scheduleModel">Scheduler Model</param>        
        [HttpPut]
        public IHttpActionResult PutScheduleLogDetail(SchedulerModel scheduleModel)
        {
            SchedulerInfo model = new SchedulerInfo();
            model.PutScheduleLogDetail(scheduleModel);
            return Ok();
        }

        /// <summary>
        /// Post Candidate Log Detail
        /// </summary>
        /// <param name="scheduleModel">Scheduler Model</param>
        [HttpPost]
        public IHttpActionResult PostScheduleLogDetail(SchedulerModel scheduleModel)
        {
            SchedulerInfo model = new SchedulerInfo();
            model.PostScheduleLogDetail(scheduleModel);
            return Ok();
        }
    }
}
