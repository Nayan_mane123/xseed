﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using XSeed.API.Models;
using XSeed.Business.Vendor;
using XSeed.Data.ViewModel.Vendors;
using XSeed.Utility;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Vendor Controller
    /// </summary>
    public class VendorController : BaseController
    {
        /// <summary>
        /// Get List of Vendors
        /// Get Vendor Detail
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="Id">Id</param>
        /// <returns></returns>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Vendor_Read", ClaimValue = "True")]
        public IHttpActionResult Get(Guid organizationId, Guid? Id = null)
        {
            VendorInfo model = new VendorInfo();

            if (Id == null)
            {
                return Ok(model.ListAllVendors(organizationId)); // return vendors list
            }
            else
            {
                return Ok(model.GetVendorDetail(organizationId, Id)); // return single vendor details
            }
        }

        /// <summary>
        /// Get Vendor Parsed Email
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>
        /// <param name="sortBy"></param>
        /// <param name="sortOrder"></param>
        /// <param name="isMobileRequest"></param>
        /// <returns>List of All Parsed Vendor Email</returns>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Vendor_Read", ClaimValue = "True")]
        public IHttpActionResult GetVendorParsedEmail(Guid organizationId, int pageSize, int pageNumber, string sortBy = "Date", string sortOrder = "desc",Boolean isMobileRequest=false)
        {
            VendorInfo model = new VendorInfo();

            /* Set Pagination Info */
            var paginationInfo = model.GetPaginationInfo(organizationId, pageSize);

            var result = new
            {
                TotalCount = paginationInfo.TotalCount,
                totalPages = paginationInfo.TotalPages,
                VendorEmail = model.ListAllParsedVendorEmail(organizationId, pageSize, pageNumber, sortBy, sortOrder)
            };
            /* return job list */
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            else
            {
                return Ok(result);
            }
            // return Ok(model.ListAllParsedVendorEmail(organizationId)); // return vendors parsed email list
        }

        /// <summary>
        /// Get Unread Mail Count 
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>Unread Mail Count</returns>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Vendor_Read", ClaimValue = "True")]
        public IHttpActionResult GetUnreadMailCount(Guid organizationId,Boolean isMobileRequest=false)
        {
            VendorInfo model = new VendorInfo();
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessResponseData(model.GetUnreadMailCount(organizationId)));
            }
            return Ok(model.GetUnreadMailCount(organizationId));
        }

        /// <summary>
        /// Update Vendor Parsed Email
        /// </summary>
        /// <param name="vendorEmailParserModel">Vendor Email Parser Model</param>
        /// <param name="isMobileRequest"></param>
        /// <returns></returns>
        [HttpPut]
        [ClaimsAuthorization(ClaimType = "Vendor_Read", ClaimValue = "True")]
        public IHttpActionResult UpdateVendorParsedEmail(VendorEmailParserModel vendorEmailParserModel,Boolean isMobileRequest=false)
        {
            if (!ModelState.IsValid)
            {
                throw new ArgumentException("Invalid Model state");
            }

            // Add Vendor
            VendorInfo model = new VendorInfo();
            model.UpdateVendorParsedEmail(vendorEmailParserModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Updated Successfully"));
            }else
            {
                return Ok();
            }
            
        }

        /// <summary>
        /// Save Vendor Detail
        /// </summary>
        /// <param name="vendorDetailModel">Vendor Detail Model</param>
        /// <returns>VendorId</returns>
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Vendor_Create", ClaimValue = "True")]
        public IHttpActionResult Post(VendorDetailModel vendorDetailModel)
        {
            if (!ModelState.IsValid)
            {
                throw new ArgumentException("Invalid Model state");
            }

            // Add Vendor
            VendorInfo model = new VendorInfo();
            return Ok(model.CreateVendor(vendorDetailModel));

        }

        /// <summary>
        /// Update Vendor Details
        /// </summary>
        /// <param name="vendorDetailModel">Vendor Detail Model</param>
        /// <returns></returns>
        [HttpPut]
        [ClaimsAuthorization(ClaimType = "Vendor_Update", ClaimValue = "True")]
        public IHttpActionResult Put(VendorDetailModel vendorDetailModel)
        {
            if (!ModelState.IsValid)
            {
                throw new ArgumentException("Invalid Model state");
            }

            // Add Vendor
            VendorInfo model = new VendorInfo();
            model.UpdateVendor(vendorDetailModel);
            return Ok();
        }
    }
}
