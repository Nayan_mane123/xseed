﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using XSeed.API.Models;
using XSeed.Business.Company;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Company;
using XSeed.Utility;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Company API
    /// </summary>        
    public class CompanyController : BaseController
    {
        /// <summary>
        /// Get companies belongs to an organization
        /// </summary>        
        /// <param name="organizationId">Organization Id</param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortBy"></param>
        /// <param name="sortOrder"></param>
        /// <param name="Id">Id</param>
        /// <param name="isMobileRequest"></param>
        /// <remarks>Get company/ Companies
        /// - Organization Id is required parameter
        /// - Id is an optional parameter
        /// - Id with value as zero will return list of companies belongs to an organization
        /// - Id with value as positive integer will return single company details
        ///</remarks>
        /// <returns>Companies</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>        
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Company_Read", ClaimValue = "True")]
        [ResponseType(typeof(List<CompanyDetailModel>))]
        public IHttpActionResult Get(Guid organizationId, int pageSize = 0, int pageNumber = 0, string sortBy = "ModifiedOn", string sortOrder = "desc", Guid? Id = null, Boolean isMobileRequest = false)
        {
            CompanyInfo model = new CompanyInfo();

            if (Id == null)
            {
                var paginationInfo = model.GetPaginationInfo(organizationId, pageSize);

                var result = new
                {
                    TotalCount = paginationInfo.TotalCount,
                    totalPages = paginationInfo.TotalPages,
                    companies = model.ListAllCompanies(organizationId, pageSize, pageNumber, sortBy, sortOrder)
                };
                if (isMobileRequest)
                {
                    return Ok(ResponseUtility.SuccessResponseData(result));
                }
                return Ok(result); // return company list
            }
            else
            {
                if (isMobileRequest)
                {
                    return Ok(ResponseUtility.SuccessResponseData(model.GetCompanyDetail(organizationId, Id)));
                }
                return Ok(model.GetCompanyDetail(organizationId, Id)); // return single company
            }
        }

        /// <summary>
        /// Create new company
        /// </summary>
        /// <param name="companyDetailModel">Company detail model </param>
        /// <param name="isMobileRequest"></param>
        /// <remarks>Insert new company</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>        
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Company_Create", ClaimValue = "True")]
        public IHttpActionResult Post(CompanyDetailModel companyDetailModel, Boolean isMobileRequest = false)
        {
            CompanyInfo model = new CompanyInfo();
            if (isMobileRequest)
            {
                return Ok(model.CreateCompanyDetail(companyDetailModel));
            }
            return Ok(model.CreateCompanyDetail(companyDetailModel));
        }

        /// <summary>
        /// Update company
        /// </summary>
        /// <param name="companyDetailModel">Company detail model </param>
        /// <param name="isMobileRequest"></param>
        /// <remarks>Update company</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPut]
        [ClaimsAuthorization(ClaimType = "Company_Update", ClaimValue = "True")]
        public IHttpActionResult Put(CompanyDetailModel companyDetailModel, Boolean isMobileRequest = false)
        {
            CompanyInfo model = new CompanyInfo();
            model.UpdateCompanyDetail(companyDetailModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Updated Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Create new company source
        /// </summary>
        /// <param name="lookUpModel">LookUp model </param>
        /// <param name="isMobileRequest"></param>
        /// <remarks>Insert new company</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Company_Create", ClaimValue = "True")]
        public IHttpActionResult PostCompanySource(LookUpModel lookUpModel, Boolean isMobileRequest = false)
        {
            CompanyInfo model = new CompanyInfo();
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessResponseData(model.CreateCompanySource(lookUpModel)));
            }
            return Ok(model.CreateCompanySource(lookUpModel));
        }


        /// <summary>
        /// Update company source
        /// </summary>
        /// <param name="lookUpModel">LookUp model</param>
        /// <param name="isMobileRequest"></param>       
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPut]
        [ClaimsAuthorization(ClaimType = "Company_Update", ClaimValue = "True")]
        public IHttpActionResult PutCompanySource(LookUpModel lookUpModel, Boolean isMobileRequest = false)
        {
            CompanyInfo.UpdateCompanySource(lookUpModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Updated Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Delete company source
        /// </summary>
        /// <param name="genericLookupId"></param>
        /// <param name="isMobileRequest"></param>
        /// <returns></returns>
        [HttpDelete]
        public IHttpActionResult DeleteCompanySource(Guid genericLookupId, Boolean isMobileRequest = false)
        {
            if (genericLookupId == Guid.Empty)
            {
                throw new ArgumentNullException("genericLookupId");
            }

            CompanyInfo.DeleteCompanySource(genericLookupId);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Deleted Successfully"));
            }
            return Ok();
        }

        /// <summary>
        /// Search Company Information.
        /// </summary>
        /// <param name="searchText">searchText</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>List of CompanyDataModel</returns>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Company_Read", ClaimValue = "True")]
        public IHttpActionResult SearchCompany(string searchText, Boolean isMobileRequest = false)
        {
            CompanyInfo model = new CompanyInfo();
            if (isMobileRequest)
            {
                return Ok(model.SearchCompanyData(searchText));
            }
            return Ok(model.SearchCompanyData(searchText));
        }


    }
}