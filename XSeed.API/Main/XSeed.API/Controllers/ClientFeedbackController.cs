﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XSeed.API.Models;
using XSeed.Business.Submission;
using XSeed.Data.ViewModel.Submission;
using XSeed.Utility;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Client feedback API
    /// </summary>
    public class ClientFeedbackController : BaseController
    {

        /// <summary>
        /// Get client feedback list for particular submission
        /// </summary>        
        /// <param name="submissionId">Submission Id</param>       
        /// <returns>Client feedback list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Submission_Read", ClaimValue = "True")]
        public IHttpActionResult Get(Guid submissionId)
        {
            ClientFeedbackInfo model = new ClientFeedbackInfo();
            return Ok(model.GetClientFeedbackList(submissionId));
        }

        /// <summary>
        /// Add new feedback of a client
        /// </summary>
        /// <param name="clientFeedbackModel">Client feedback model </param>
        /// <param name="isMobileRequest"></param>
        /// <remarks>Add new feedback of a client</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>   
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Submission_Update", ClaimValue = "True")]
        public IHttpActionResult Post(ClientFeedbackModel clientFeedbackModel, Boolean isMobileRequest = false)
        {
            ClientFeedbackInfo model = new ClientFeedbackInfo();
            model.AddClientFeedback(clientFeedbackModel);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Saved Successfully"));
            }
            return Ok();
        }

    }
}
