﻿using System;
using System.Web.Http;
using XSeed.API.Models;
using XSeed.Business.ResumeEmail;
using XSeed.Utility;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// ResumeEmailController
    /// </summary>
    public class ResumeEmailController : BaseController
    {
        /// <summary>
        /// Get List of Resume Emails
        /// Get Resume Email Detail
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>
        /// <param name="sortBy">sortBy</param>
        /// <param name="sortOrder">sortOrder</param>
        /// <param name="Id">Id</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>List of Resume Emails</returns>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Vendor_Read", ClaimValue = "True")]
        public IHttpActionResult Get(Guid organizationId, int pageSize, int pageNumber, string sortBy = "Date", string sortOrder = "desc", Guid? Id = null, Boolean isMobileRequest = false)
        {
            ResumeInfo model = new ResumeInfo();

            if (Id == null)
            {
                /* Set Pagination Info */
                var paginationInfo = model.GetPaginationInfo(organizationId, pageSize);

                /* return job list */
                var result = new
                {
                    TotalCount = paginationInfo.TotalCount,
                    totalPages = paginationInfo.TotalPages,
                    ResumeEmail = model.ListAllResumeEmails(organizationId, pageSize, pageNumber, sortBy, sortOrder)
                };
                if (isMobileRequest)
                {
                    return Ok(ResponseUtility.SuccessResponseData(result));
                }
                return Ok(result);
            }
            else
            {

                if (isMobileRequest)
                {
                    var result = new
                    {
                        ResumeEmailDetails = model.GetResumeEmailDetail(organizationId, Id)
                    };
                    return Ok(ResponseUtility.SuccessResponseData(result));
                }

                return Ok(model.GetResumeEmailDetail(organizationId, Id)); // return single resume email details
            }
        }

        /// <summary>
        /// Get Unread Mail Count 
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="isMobileRequest"></param>
        /// <returns>Unread Mail Count</returns>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Vendor_Read", ClaimValue = "True")]
        public IHttpActionResult GetUnreadMailCount(Guid organizationId, Boolean isMobileRequest = false)
        {
            ResumeInfo model = new ResumeInfo();
            if (isMobileRequest)
            {
                var result = new
                {
                    UnreadMailCount = model.GetUnreadMailCount(organizationId)
                };
                return Ok(ResponseUtility.SuccessResponseData(result));
            }
            return Ok(model.GetUnreadMailCount(organizationId));
        }

        /// <summary>
        /// Update Resume Parsed Email
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="isMobileRequest"></param>       
        /// <returns></returns>
        [HttpPut]
        [ClaimsAuthorization(ClaimType = "Vendor_Update", ClaimValue = "True")]
        public IHttpActionResult Put(Guid Id, Boolean isMobileRequest = false)
        {
            if (!ModelState.IsValid)
            {
                throw new ArgumentException("Invalid Model State");
            }

            ResumeInfo model = new ResumeInfo();
            model.UpdateResumeParsedEmail(Id);
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessEmptyResponseData("Updated Successfully"));
            }
            return Ok();
        }
    }
}
