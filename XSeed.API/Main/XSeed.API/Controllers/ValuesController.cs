﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using XSeed.API.Filters;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// ValuesController
    /// </summary>
    [Authorize]
    public class ValuesController : ApiController
    {


        /// <summary>
        /// GET api/values
        /// </summary>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        /// <summary>
        /// GET api/values/5
        /// </summary>
        public string Get(int id)
        {
            return "value";
        }

        /// <summary>
        /// Post api/values/5
        /// </summary>
        public void Post(string v)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Delete api/values/5
        /// </summary>
        public void Delete(int v)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Put api/values/5
        /// </summary>
        public void Put(int v1, string v2)
        {
            throw new NotImplementedException();
        }
    }
}
