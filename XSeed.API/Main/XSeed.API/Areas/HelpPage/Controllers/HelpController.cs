using System;
using System.Web.Http;
using System.Web.Mvc;
using XSeed.API.Areas.HelpPage.Models;

namespace XSeed.API.Areas.HelpPage.Controllers
{
    /// <summary>
    /// The controller that will handle requests for the help page.
    /// </summary>
    public class HelpController : Controller
    {
        /// <summary>
        ///Help Controller
        /// </summary>
        public HelpController()
            : this(GlobalConfiguration.Configuration)
        {
        }
        /// <summary>
        ///Help Controller
        /// </summary>
        public HelpController(HttpConfiguration config)
        {
            Configuration = config;
        }
        /// <summary>
        ///Help Configuration
        /// </summary>
        public HttpConfiguration Configuration { get; private set; }
        /// <summary>
        ///Index
        /// </summary>
        public ActionResult Index()
        {
            ViewBag.DocumentationProvider = Configuration.Services.GetDocumentationProvider();
            return View(Configuration.Services.GetApiExplorer().ApiDescriptions);
        }
        /// <summary>
        /// Help Api
        /// </summary>
        public ActionResult Api(string apiId)
        {
            if (!String.IsNullOrEmpty(apiId))
            {
                HelpPageApiModel apiModel = Configuration.GetHelpPageApiModel(apiId);
                if (apiModel != null)
                {
                    return View(apiModel);
                }
            }

            return View("Error");
        }
    }
}