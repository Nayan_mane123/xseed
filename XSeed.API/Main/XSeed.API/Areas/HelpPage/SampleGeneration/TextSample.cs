using System;

namespace XSeed.API.Areas.HelpPage
{
    /// <summary>
    /// This represents a preformatted text sample on the help page. There's a display template named TextSample associated with this class.
    /// </summary>
    public class TextSample
    {
        /// <summary>
        ///Text Sample
        /// </summary>
        public TextSample(string text)
        {
            if (text == null)
            {
                throw new ArgumentNullException("text");
            }
            Text = text;
        }
        /// <summary>
        ///Text 
        /// </summary>
        public string Text { get; private set; }
        /// <summary>
        ///Equals
        /// </summary>
        public override bool Equals(object obj)
        {
            TextSample other = obj as TextSample;
            return other != null && Text == other.Text;
        }
        /// <summary>
        ///Get Hash Code
        /// </summary>
        public override int GetHashCode()
        {
            return Text.GetHashCode();
        }
        /// <summary>
        ///Get String
        /// </summary>
        public override string ToString()
        {
            return Text;
        }
    }
}