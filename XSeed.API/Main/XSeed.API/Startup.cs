﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Logging;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin.Security.DataProtection;

[assembly: OwinStartup(typeof(XSeed.API.Startup))]

namespace XSeed.API
{
    public partial class Startup
    {
        internal static IDataProtectionProvider DataProtectionProvider { get; private set; }
        /// <summary>
        ///config
        /// </summary>
        public void Configuration(IAppBuilder app)
        {
            DataProtectionProvider = app.GetDataProtectionProvider();
            ConfigureCors(app);
            ConfigureAuth(app); 
        }

        
    }
     
}
