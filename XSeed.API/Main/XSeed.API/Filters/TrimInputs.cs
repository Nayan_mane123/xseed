﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace XSeed.API.Filters
{
    /// <summary>
    /// TrimInputs Attribute
    /// </summary>
    public class TrimInputsAttribute : ActionFilterAttribute, IActionFilter 
    {
        /// <summary>
        /// TrimInputs Attribute
        /// </summary>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
          
            Object obj = actionContext.ControllerContext.RouteData; 
            List<string> stringPropertyNamesAndValues = obj.GetType().GetProperties().Where(pi => (pi.PropertyType == typeof(string))).Select(pi => pi.Name).ToList();
            foreach (var propertyName in stringPropertyNamesAndValues)
            {
                var propertyValue = obj.GetType().GetProperty(propertyName).GetValue(obj, null);
                obj.GetType().GetProperty(propertyName).SetValue(obj, propertyValue.ToString().Trim());
            }
        }
    }
     
}