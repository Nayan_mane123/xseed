﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using XSeed.Business.User.OrganizationUserInfo;
using System.Reflection;
using XSeed.Business.User.UserCommon;
using XSeed.Business.User.CandidateUserInfo;
using Microsoft.Owin.Security.Facebook;
using System.Configuration;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.Twitter;
using Owin.Security.Providers.LinkedIn;
using XSeed.Utility;

namespace XSeed.API.Providers
{
    /// <summary>
    /// Application OAuth Provider
    /// </summary>
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;
        private readonly Func<UserManager<IdentityUser>> _userManagerFactory;

        /// <summary>
        /// Application OAuth Provider
        /// </summary>
        public ApplicationOAuthProvider(string publicClientId, Func<UserManager<IdentityUser>> userManagerFactory)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }

            if (userManagerFactory == null)
            {
                throw new ArgumentNullException("userManagerFactory");
            }

            _publicClientId = publicClientId;
            _userManagerFactory = userManagerFactory;
        }

        /// <summary>
        /// Grant Resource Owner Credentials
        /// </summary>
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {


            using (UserManager<IdentityUser> userManager = _userManagerFactory())
            {

                /* Call AESEncryptDecrypt to Decrypt Username */
                var username = Utility.AESEncryptDecrypt.DecryptStringAES(context.UserName);

                /* Call AESEncryptDecrypt to Decrypt Password */
                var password = Utility.AESEncryptDecrypt.DecryptStringAES(context.Password);

                IdentityUser user = await userManager.FindAsync(username, password);
                string userType = string.Empty;

                if (user == null)
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect!");
                    return;

                }
                else
                {
                    UserCommon userCommonModel = new UserCommon();
                    userType = userCommonModel.GetUserTypeByEmail(username);

                    if (string.IsNullOrEmpty(userType))
                    {
                        context.SetError("invalid_grant", "Invalid user!");
                        return;
                    }
                }

                ClaimsIdentity oAuthIdentity = await userManager.CreateIdentityAsync(user,
                    context.Options.AuthenticationType);
                ClaimsIdentity cookiesIdentity = await userManager.CreateIdentityAsync(user,
                    CookieAuthenticationDefaults.AuthenticationType);
                AuthenticationProperties properties = CreateProperties(user.UserName, userType, oAuthIdentity);
                AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
                context.Validated(ticket);
                context.Request.Context.Authentication.SignIn(cookiesIdentity);
            }
        }

        /// <summary>
        /// Token End point
        /// </summary>
        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        /// <summary>
        /// Validate Client Authentication
        /// </summary>
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // Resource owner password credentials does not provide a client ID.
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }

        /// <summary>
        /// Validate Client Authentication
        /// </summary>
        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");
                Uri redirecteduri = new Uri(context.RedirectUri, UriKind.Absolute);

                List<string> AllowedUri = ConfigurationManager.AppSettings["AllowedUri"].Split(',').ToList();

                

                if (expectedRootUri.Authority == redirecteduri.Authority || AllowedUri.Contains(redirecteduri.Authority))
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        /// <summary>
        /// Application OAuth Provider
        /// </summary>
        public static AuthenticationProperties CreateProperties(string userName, string userType, ClaimsIdentity oAuthIdentity)
        {
            IDictionary<string, string> userData = new Dictionary<string, string>();

            if (userType == "Recruiter")
            {
                OrganizationUserInfo model = new OrganizationUserInfo();
                var userModel = model.GetUser(userName);
                userData = userModel.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public).ToDictionary(prop => prop.Name, prop => prop.GetValue(userModel, null) == null ? "" : prop.GetValue(userModel, null).ToString());

                var userClaimsList = oAuthIdentity.Claims.Where(x => x.Type.Contains("Create")
                                                                    || x.Type.Contains("Update")
                                                                    || x.Type.Contains("Read")
                                                                    || x.Type.Contains("Delete"))
                                 .ToDictionary(x => x.Type, x => x.Value);

                userData = userData.Concat(userClaimsList)
                           .ToDictionary(x => x.Key, x => x.Value);
            }
            else if (userType == "Candidate")
            {
                CandidateUserInfo model = new CandidateUserInfo();
                userData = model.GetCandidateUser(userName);
            }

            return new AuthenticationProperties(userData);
        }
    }

    /// <summary>
    /// Application OAuth Provider
    /// </summary>
    public class FacebookAuthProvider : FacebookAuthenticationProvider
    {
        /// <summary>
        /// Application OAuth 
        /// </summary>
        public override Task Authenticated(FacebookAuthenticatedContext context)
        {
            context.Identity.AddClaim(new Claim("ExternalAccessToken", context.AccessToken));
            context.Identity.AddClaim(new Claim("UniqueId", context.Id));
            context.Identity.AddClaim(new Claim("Name", context.Name));
            context.Identity.AddClaim(new Claim("Email", context.Email != null ? context.Email : string.Empty));
            return Task.FromResult<object>(null);
        }
    }

    /// <summary>
    /// Google OAuth Provider
    /// </summary>
    public class GoogleAuthProvider : IGoogleOAuth2AuthenticationProvider
    {
        /// <summary>
        /// ApplyRedirect
        /// </summary>
        public void ApplyRedirect(GoogleOAuth2ApplyRedirectContext context)
        {
            context.Response.Redirect(context.RedirectUri);
        }

        /// <summary>
        /// ApplyRedirect
        /// </summary>
        public Task Authenticated(GoogleOAuth2AuthenticatedContext context)
        {
            context.Identity.AddClaim(new Claim("ExternalAccessToken", context.AccessToken));
            context.Identity.AddClaim(new Claim("UniqueId", context.Id));
            context.Identity.AddClaim(new Claim("Name", context.Name));
            context.Identity.AddClaim(new Claim("Email", context.Email != null ? context.Email : string.Empty));
            return Task.FromResult<object>(null);
        }

        /// <summary>
        /// ReturnEndpoint
        /// </summary>
        public Task ReturnEndpoint(GoogleOAuth2ReturnEndpointContext context)
        {
            return Task.FromResult<object>(null);
        }
    }

    /// <summary>
    /// Twitter AuthProvider
    /// </summary>
    public class TwitterAuthProvider : TwitterAuthenticationProvider
    {
        /// <summary>
        /// Authenticated
        /// </summary>
        public override Task Authenticated(TwitterAuthenticatedContext context)
        {
            context.Identity.AddClaim(new Claim("ExternalAccessToken", context.AccessToken));
            context.Identity.AddClaim(new Claim("UniqueId", context.UserId));
            context.Identity.AddClaim(new Claim("Name", context.ScreenName));
            context.Identity.AddClaim(new Claim("Email", string.Empty));

            return Task.FromResult<object>(null);
        }
    }

    /// <summary>
    /// Linked In AuthProvider
    /// </summary>
    public class LinkedInAuthProvider : LinkedInAuthenticationProvider
    {
        /// <summary>
        /// Linked In AuthProvider
        /// </summary>
        public override Task Authenticated(LinkedInAuthenticatedContext context)
        {
            context.Identity.AddClaim(new Claim("ExternalAccessToken", context.AccessToken));
            context.Identity.AddClaim(new Claim("UniqueId", context.Id));
            context.Identity.AddClaim(new Claim("Name", context.Name));
            context.Identity.AddClaim(new Claim("Email", context.Email != null ? context.Email : string.Empty));
            return Task.FromResult<object>(null);
        }
    }
}