﻿using System;
using System.Collections.Generic;

namespace XSeed.API.Models
{

    /// <summary>
    /// Models returned by AccountController actions.
    /// </summary>
    public class ExternalLoginViewModel
    {
        /// <summary>
        /// Name 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Url 
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// State 
        /// </summary>
        public string State { get; set; }
    }

    /// <summary>
    /// Manage Info
    /// </summary>
    public class ManageInfoViewModel
    {
        /// <summary>
        /// Login Provider 
        /// </summary>
        public string LocalLoginProvider { get; set; }

        /// <summary>
        /// User Name 
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Logins 
        /// </summary>
        public IEnumerable<UserLoginInfoViewModel> Logins { get; set; }

        /// <summary>
        /// External Login Providers 
        /// </summary>
        public IEnumerable<ExternalLoginViewModel> ExternalLoginProviders { get; set; }
    }

    /// <summary>
    /// User Info
    /// </summary>
    public class UserInfoViewModel
    {
        /// <summary>
        /// User Name 
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Registered 
        /// </summary>
        public bool HasRegistered { get; set; }

        /// <summary>
        /// LoginProvider 
        /// </summary>
        public string LoginProvider { get; set; }
    }

    /// <summary>
    /// User Login Info 
    /// </summary>
    public class UserLoginInfoViewModel
    {
        /// <summary>
        /// Login Provider 
        /// </summary>
        public string LoginProvider { get; set; }

        /// <summary>
        /// Provider Key
        /// </summary>
        public string ProviderKey { get; set; }
    }
}
