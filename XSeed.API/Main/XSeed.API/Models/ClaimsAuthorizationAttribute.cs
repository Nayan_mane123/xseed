﻿using System;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace XSeed.API.Models
{
    /// <summary>
    /// Claims Authorization Attribute
    /// </summary>
    public class ClaimsAuthorizationAttribute : AuthorizationFilterAttribute
    {
        /// <summary>
        /// Claim Type 
        /// </summary>
        public string ClaimType { get; set; }
        /// <summary>
        /// Claim Value
        /// </summary>
        public string ClaimValue { get; set; }

        /// <summary>
        /// Authorization Async
        /// </summary>
        public override Task OnAuthorizationAsync(HttpActionContext actionContext, System.Threading.CancellationToken cancellationToken)
        {

            var principal = actionContext.RequestContext.Principal as ClaimsPrincipal;

            if (!principal.Identity.IsAuthenticated)
            {
                throw new UnauthorizedAccessException();

            }

            if (!(principal.HasClaim(x => x.Type == ClaimType && x.Value == ClaimValue)))
            {
                throw new AccessViolationException();

            }

            //User is Authorized, complete execution
            return Task.FromResult<object>(null);

        }
    }
}