﻿using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace XSeed.API.Models
{
    /// <summary>
    /// Logging
    /// </summary>
    public static class Logging
    {
        /// <summary>
        /// Logging
        /// </summary>
        public static void LogMessage(string source, int userid, string process, string message)
        {
            //
        }

        /// <summary>
        /// Logging
        /// </summary>
        public static void LogException(int userId, Exception ex)
        {
            //
        }

        /*
        * We don't use priority or eventId
        * We get the calling method from the stackFrame (careful with inlining here)
        */
        private static string GetCallingMethod(StackFrame frame)
        {
            //don't calculate this if we aren't logging
            if (!Logger.IsLoggingEnabled()) return string.Empty;
            var method = frame.GetMethod();
            return method.DeclaringType.FullName + "." + method.Name;
        }

        /// <summary>
        /// Logs an informational message
        /// </summary>
        /// <param name="message">The message.</param>
        public static void Info(string message)
        {
            var category = GetCallingMethod(new StackFrame(1));
            Logger.Write(message, category, 0, 0, TraceEventType.Information);
        }

        /// <summary>
        /// Logs a warning message
        /// </summary>
        /// <param name="message">The message.</param>
        public static void Warn(string message)
        {
            var category = GetCallingMethod(new StackFrame(1));
            Logger.Write(message, category, 0, 0, TraceEventType.Warning);
        }

        /// <summary>
        /// Logs an error message
        /// </summary>
        /// <param name="message">The message.</param>
        public static void Error(string message)
        {
            var category = GetCallingMethod(new StackFrame(1));
            Logger.Write(message, category, 0, 0, TraceEventType.Error);
        }

        /// <summary>
        /// Log all parameters
        /// </summary>
      
        public static void LogInputParameters<T>(T Object)
        {
            var category = GetCallingMethod(new StackFrame(1));
            List<string> stringPropertyNamesAndValues = Object.GetType().GetProperties().Where(pi => (pi.PropertyType == typeof(string) || pi.PropertyType == typeof(DateTime) || pi.PropertyType == typeof(DateTime?) || pi.PropertyType == typeof(int) || pi.PropertyType == typeof(Int64) || pi.PropertyType == typeof(long))).Select(pi => pi.Name).ToList();
            string message = string.Empty;
            foreach (var propertyName in stringPropertyNamesAndValues)
            {
                var propertyValue = Object.GetType().GetProperty(propertyName).GetValue(Object, null);
                message += string.Format("\r\n{0} :=> {1}", propertyName, propertyValue);

            }
            Logger.Write(message, category, 0, 0, TraceEventType.Information);
            Logger.Write("\r\n###############################################################################################", category, 0, 0, TraceEventType.Information);
        }
    }
}