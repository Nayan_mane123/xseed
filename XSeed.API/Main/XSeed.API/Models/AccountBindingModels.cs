﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace XSeed.API.Models
{

    /// <summary>
    /// Models used as parameters to AccountController actions.
    /// </summary>
    public class AddExternalLoginBindingModel
    {
        /// <summary>
        /// ExternalAccessToken
        /// </summary>
        [Required]
        [Display(Name = "External access token")]
        public string ExternalAccessToken { get; set; }
    }

    /// <summary>
    /// Change Password BindingModel
    /// </summary>
    public class ChangePasswordBindingModel
    {
        /// <summary>
        /// OldPassword
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        /// <summary>
        /// New Password
        /// </summary>
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        /// <summary>
        /// Confirm Password
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    /// <summary>
    /// Register Binding Model
    /// </summary>
    public class RegisterBindingModel
    {

        /// <summary>
        /// User name
        /// </summary>
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        /// <summary>
        ///Password
        /// </summary>
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        /// <summary>
        ///Confirm password
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    /// <summary>
    ///Register External Binding Model
    /// </summary>
    public class RegisterExternalBindingModel
    {
        /// <summary>
        ///User name
        /// </summary>
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }
    }

    /// <summary>
    ///Remove Login Binding Model
    /// </summary>
    public class RemoveLoginBindingModel
    {
        /// <summary>
        ///Login provider
        /// </summary>
        [Required]
        [Display(Name = "Login provider")]
        public string LoginProvider { get; set; }

        /// <summary>
        ///Provider key
        /// </summary>
        [Required]
        [Display(Name = "Provider key")]
        public string ProviderKey { get; set; }
    }
    /// <summary>
    ///Set Password Binding Model
    /// </summary>
    public class SetPasswordBindingModel
    {
        /// <summary>
        ///New password
        /// </summary>
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        /// <summary>
        ///Confirm new password
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}
