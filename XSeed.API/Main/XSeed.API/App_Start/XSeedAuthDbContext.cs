﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XSeed.API.App_Start
{
    /// <summary>
    /// XSeed Auth DbContext class
    /// </summary>
    public class XSeedAuthDbContext : IdentityDbContext
    {
        /// <summary>
        ///XSeed Auth DbContext
        /// </summary>
        public XSeedAuthDbContext()
            : base("XSeedDbContext")
        {
        }
    }
}