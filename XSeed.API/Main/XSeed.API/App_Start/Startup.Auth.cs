﻿using System;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Owin;
using XSeed.API.Providers;
using XSeed.API.App_Start;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.Twitter;
using Microsoft.Owin.Security;
using Owin.Security.Providers.LinkedIn;
using Microsoft.Owin.Cors;
using System.Web.Cors;
using System.Threading.Tasks;
using System.Configuration;
using System.Net;

namespace XSeed.API
{
    /// <summary>
    ///Register Routes
    /// </summary>
    public partial class Startup
    {
        /// <summary>
        ///facebook AuthOptions
        /// </summary>
        public static FacebookAuthenticationOptions facebookAuthOptions { get; private set; }
        /// <summary>
        ///google AuthOptions
        /// </summary>
        public static GoogleOAuth2AuthenticationOptions googleAuthOptions { get; private set; }
        /// <summary>
        ///twitter AuthOptions
        /// </summary>
        public static TwitterAuthenticationOptions twitterAuthOptions { get; set; }
        /// <summary>
        ///linkedin Auth Options
        /// </summary>
        public static LinkedInAuthenticationOptions linkedinAuthOptions { get; set; }

        static Startup()
        {
            PublicClientId = "self";

            UserManagerFactory = () => new UserManager<IdentityUser>(new UserStore<IdentityUser>(new XSeedAuthDbContext()));

            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                Provider = new ApplicationOAuthProvider(PublicClientId, UserManagerFactory),
                AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                AllowInsecureHttp = true
            };
        }

        /// <summary>
        ///OAuth Options
        /// </summary>
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }
        /// <summary>
        ///User Manager Factory
        /// </summary>
        public static Func<UserManager<IdentityUser>> UserManagerFactory { get; set; }
        /// <summary>
        ///Public ClientId
        /// </summary>
        public static string PublicClientId { get; private set; }

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        /// <summary>
        ///Configure Auth
        /// </summary>
        public void ConfigureAuth(IAppBuilder app)
        {
            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            //linkedin has upgraded their security Transport layer to TLS 1.2
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);



            facebookAuthOptions = new FacebookAuthenticationOptions()
            {
                AppId = "545629832293995",
                AppSecret = "809c9c195ff6024d2554d24ab7f55f1c",
                Provider = new FacebookAuthProvider()
            };
            app.UseFacebookAuthentication(facebookAuthOptions);

            //Configure Google External Login
            googleAuthOptions = new GoogleOAuth2AuthenticationOptions()
            {
                ClientId = "278595804156-jth5uc024aa2lhdeqgp3s46f5brrihta.apps.googleusercontent.com",
                ClientSecret = "Xofwik0NMl-bSYR05r-NRBif",
                Provider = new GoogleAuthProvider()
            };
            app.UseGoogleAuthentication(googleAuthOptions);


            twitterAuthOptions = new TwitterAuthenticationOptions()
            {
                ConsumerKey = "kCwO7hxImX0ziwWqRBd8jlXma",
                ConsumerSecret = "UM0h9jYQIjjTmH8JGVAsCe83CEJiXzzKNqjJZoG6y5e7W9OqxN",
                BackchannelCertificateValidator = new CertificateSubjectKeyIdentifierValidator(new[]
                {
                    "A5EF0B11CEC04103A34A659048B21CE0572D7D47", // VeriSign Class 3 Secure Server CA - G2
                    "0D445C165344C1827E1D20AB25F40163D8BE79A5", // VeriSign Class 3 Secure Server CA - G3
                    "7FD365A7C2DDECBBF03009F34339FA02AF333133", // VeriSign Class 3 Public Primary Certification Authority - G5
                    "39A55D933676616E73A761DFA16A7E59CDE66FAD", // Symantec Class 3 Secure Server CA - G4
                    "‎add53f6680fe66e383cbac3e60922e3b4c412bed", // Symantec Class 3 EV SSL CA - G3
                    "4eb6d578499b1ccf5f581ead56be3d9b6744a5e5", // VeriSign Class 3 Primary CA - G5
                    "5168FF90AF0207753CCCD9656462A212B859723B", // DigiCert SHA2 High Assurance Server C‎A 
                    "B13EC36903F8BF4701D498261A0802EF63642BC3" // DigiCert High Assurance EV Root CA
                }),
                Provider = new TwitterAuthProvider()
            };
            app.UseTwitterAuthentication(twitterAuthOptions);

            linkedinAuthOptions = new LinkedInAuthenticationOptions()
            {
                ClientId = "81onscw7n60ylq",
                ClientSecret = "HmDHSx51iTGAIbT4",
                Provider = new LinkedInAuthProvider()
            };
            app.UseLinkedInAuthentication(linkedinAuthOptions);
        }

        /// <summary>
        /// Configure Cors
        /// </summary>
        /// <returns></returns>
        public void ConfigureCors(IAppBuilder app)
        {
            app.UseCors(new CorsOptions
            {
                PolicyProvider = new AuthCorsPolicy()
            });
        }

        /// <summary>
        /// Auth Cors Policy
        /// </summary>
        public class AuthCorsPolicy : ICorsPolicyProvider
        {
            /// <summary>
            ///Get Cors PolicyAsync
            /// </summary>
            //This method will get all the apps url in the database and add it in CORS and will only allow those url to connect to auth
            public Task<CorsPolicy> GetCorsPolicyAsync(IOwinRequest request)
            {


                var policy = new CorsPolicy
                {
                    AllowAnyMethod = true,
                    AllowAnyHeader = true
                };

                var allowedOrigins = ConfigurationManager.AppSettings["AllowedOrigins"].Split(',');
                var origin = request.Headers["Origin"];

                if (origin != null && allowedOrigins.Contains(origin))
                {
                    policy.Origins.Add(origin);
                }

                policy.SupportsCredentials = true;
                return Task.FromResult(policy);
            }
        }
    }
}
