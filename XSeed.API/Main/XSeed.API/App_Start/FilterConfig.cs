﻿using System.Web;
using System.Web.Mvc;

namespace XSeed.API
{
    /// <summary>
    /// Filter Config
    /// </summary>
    public static class FilterConfig
    {
        /// <summary>
        /// Register Global Filters
        /// </summary>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
