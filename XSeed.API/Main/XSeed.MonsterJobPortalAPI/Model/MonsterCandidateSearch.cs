﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XSeed.MonsterJobPortalAPI.Model
{
    public class MonsterCandidateSearch
    {
        public string Title { get; set; }
        public string Type { get; set; }
        public string Location { get; set; }
        public string ExperienceLevel { get; set; }
        public string MinSalary { get; set; }
        public string MaxSalary { get; set; }
        public string SalaryType { get; set; }
        public string CurrencyType { get; set; }
        public List<string> Skills { get; set; }
        public string MonsterBooleanQuery { get; set; }
        public string MonsterLocationMilesAway { get; set; }
        public string MonsterLocationZIPCode { get; set; }
        public List<string> MonsterUSLocationID { get; set; }
        public string LocationType { get; set; }
        public string MonsterTargetWorkLocation { get; set; }
        public string MonsterResumeMinDate { get; set; }
        public string MonsterResumeMaxDate { get; set; }
    }
}
