﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XSeed.MonsterJobPortalAPI.Model
{
    public class MonsterCandidate
    {
        public string ResumeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string CurrentJobTitle { get; set; }
        public string CurrentCompany { get; set; }
        public string Education { get; set; }
        public string Location { get; set; }

        public string Relevance { get; set; }
        public string Distance { get; set; }
        public string ResumeTitle { get; set; }
        public string Confidential { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string ServiceFlag { get; set; }
        public string MilitaryInvolvement { get; set; }
        public string TargetJobTitle { get; set; }
        public string TargetJobType { get; set; }
        public string TargetRelocation { get; set; }
        public string TargetMinSalary { get; set; }
        public string TargetMaxSalary { get; set; }
        public string TargetSalaryType { get; set; }
        public string TargetSalaryCurrency { get; set; }
        public string WorkAuthType { get; set; }
        public string WorkAuthCountry { get; set; }
        public string Board { get; set; }

        public string AlreadyDownloaded { get; set; }
        public string LocalResumePath { get; set; }
    }
}
