﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XSeed.MonsterJobPortalAPI.Model
{
    public class LicenseInfo
    {
        public string LicenseType { get; set; }
        public string TotalPurchasedQuantity { get; set; }
        public string TotalAvailableQuantity { get; set; }
        public string ResumeBoard { get; set; }
        public string LocationGroup { get; set; }
        public string OriginalQuantity { get; set; }
        public string InventoryActiveDate { get; set; }
        public string InventoryExpireDate { get; set; }
    }
}
