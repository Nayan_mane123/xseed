﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XSeed.MonsterJobPortalAPI
{
    //public class Constants
    //{
    //    public string monsterBGWUrl = null;
    //    public string monsterMongoDbUrl = null;
    //    public string monsterMongoDbName = null;
    //    public string mongoDbCollectionName = null;
    //    public string monsterResumeLocalCopyPath = null;
    //    public string monsterXmlPath = null;
    //    public string monsterRSXUrl = null;
    //    public string monsterRSXVersion = null;
    //    public string monsterTCogCAT = null;

    //    private static Constants instance;
    //    private static readonly object syncRoot = new object();

    //    private Constants()
    //    {
    //        if (monsterBGWUrl == null)
    //            monsterBGWUrl = ConfigurationManager.AppSettings["MonsterBGWUrl"].ToString();

    //        if (monsterMongoDbUrl == null)
    //            monsterMongoDbUrl = ConfigurationManager.AppSettings["MongoURL"].ToString();

    //        if (monsterMongoDbName == null)
    //            monsterMongoDbName = ConfigurationManager.AppSettings["MongoMonsterDB"].ToString();

    //        if (mongoDbCollectionName == null)
    //            mongoDbCollectionName = ConfigurationManager.AppSettings["MongoMonsterCollection"].ToString();

    //        if (monsterResumeLocalCopyPath == null)
    //            monsterResumeLocalCopyPath = ConfigurationManager.AppSettings["MonsterResumeLocalCopyPath"].ToString();

    //        if (monsterXmlPath == null)
    //            monsterXmlPath = ConfigurationManager.AppSettings["MonsterXMLPath"].ToString();

    //        if (monsterRSXUrl == null)
    //            monsterRSXUrl = ConfigurationManager.AppSettings["MonsterRSXUrl"].ToString();

    //        if (monsterRSXVersion == null)
    //            monsterRSXVersion = ConfigurationManager.AppSettings["MonsterRSXVersion"].ToString();

    //        if (monsterTCogCAT == null)
    //            monsterTCogCAT = ConfigurationManager.AppSettings["MonsterTCogCAT"].ToString();
    //    }

    //    public static Constants GetInstance()
    //    {
    //        if (instance == null)
    //        {
    //            lock (syncRoot)
    //            {
    //                if (instance == null)
    //                {
    //                    instance = new Constants();
    //                }
    //            }
    //        }

    //        return instance;
    //    }
    //}

    public class Constants
    {
        public static string monsterBGWUrl = ConfigurationManager.AppSettings["MonsterBGWUrl"].ToString();
        public static string monsterMongoDbUrl = ConfigurationManager.AppSettings["MongoURL"].ToString();
        public static string monsterMongoDbName = ConfigurationManager.AppSettings["MongoMonsterDB"].ToString();
        public static string mongoDbCollectionName = ConfigurationManager.AppSettings["MongoMonsterCollection"].ToString();
        public static string monsterResumeLocalCopyPath = ConfigurationManager.AppSettings["MonsterResumeLocalCopyPath"].ToString();
        public static string monsterLookUpPath = ConfigurationManager.AppSettings["MonsterLookUpPath"].ToString();
        public static string monsterXmlPath = ConfigurationManager.AppSettings["MonsterXMLPath"].ToString();
        public static string monsterRSXUrl = ConfigurationManager.AppSettings["MonsterRSXUrl"].ToString();
        public static string monsterRSXVersion = ConfigurationManager.AppSettings["MonsterRSXVersion"].ToString();
        public static string monsterTCogCAT = ConfigurationManager.AppSettings["MonsterTCogCAT"].ToString();
    }
}
