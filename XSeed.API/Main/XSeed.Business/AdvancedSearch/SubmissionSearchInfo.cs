﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.AdvancedSearch;
using XSeed.Data.ViewModel.Submission;

namespace XSeed.Business.AdvancedSearch
{
    public class SubmissionSearchInfo
    {
        /// <summary>
        /// Advance Search Submissions
        /// </summary>
        /// <param name="searchString">searchString</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>
        /// <param name="sortBy">sortBy</param>
        /// <param name="sortOrder">sortOrder</param>
        /// <param name="isExport">isExport</param>
        /// <returns>Submissions List</returns>
        public List<SubmissionModel> SearchSubmissions(SubmissionAdvancedSearchModel submissionAdvancedSearchModel, int pageSize, int pageNumber, string sortBy, string sortOrder, out int TotalCount, out double TotalPages, bool isExport = false)
        {
            return SubmissionAdvancedSearchModel.SearchSubmissions(submissionAdvancedSearchModel, pageSize, pageNumber, sortBy, sortOrder, out TotalCount, out TotalPages, isExport);
        }
    }
}
