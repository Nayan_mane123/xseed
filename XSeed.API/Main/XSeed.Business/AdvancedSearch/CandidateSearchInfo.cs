﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.AdvancedSearch;
using XSeed.Data.ViewModel.User.CandidateUser;

namespace XSeed.Business.AdvancedSearch
{
    public class CandidateSearchInfo
    {
        /// <summary>
        /// Candidate Advanced Search
        /// </summary>
        /// <param name="candidateSearchModel">Candidate Search Model</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>
        /// <param name="TotalCount">TotalCount</param>
        /// <param name="TotalPages">TotalPages</param>
        /// <returns>Candidate List</returns>
        public List<CandidateUserModel> SearchCandidates(CandidateSearchModel candidateSearchModel, int pageSize, int pageNumber, out int TotalCount, out double TotalPages)
        {
            return candidateSearchModel.SearchCandidates(candidateSearchModel, pageSize, pageNumber,out TotalCount,out TotalPages);
        }

        /// <summary>
        /// Candidate Advanced Search API
        /// </summary>
        /// <param name="candidateAdvancedSearchModel">Candidate Advanced Search Model</param>        
        /// <param name="pageSize">Page Size</param>
        /// <param name="pageNumber">Page Number</param>
        /// <param name="sortBy">Sort By</param>
        /// <param name="sortOrder">Sort Order</param>
        /// <returns>Candidate List</returns>
        public List<CandidateUserModel> SearchCandidates(CandidateAdvancedSearchModel candidateAdvancedSearchModel, int pageSize, int pageNumber, string sortBy, string sortOrder, out int TotalCount, out double TotalPages, bool isExport = false)
        {
            return candidateAdvancedSearchModel.SearchCandidates(candidateAdvancedSearchModel, sortBy, sortOrder, pageSize, pageNumber, out TotalCount, out TotalPages, isExport);
        }
    }
}
