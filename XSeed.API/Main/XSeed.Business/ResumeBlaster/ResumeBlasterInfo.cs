﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.ResumeBlaster;
using XSeed.Data.ViewModel.Common;

namespace XSeed.Business.ResumeBlaster
{
    public class ResumeBlasterInfo
    {
        /// <summary>
        /// Get list of ResumeBlasters reccruiter
        /// </summary>
        /// <returns>ResumeBlasters Detail Model List</returns>
        public List<ResumeBlasterDetailModel> ListResumeBlasters(int pageSize, int pageNumber, string sortBy = "ModifiedOn", string sortOrder = "desc")
        {
            ResumeBlasterDetailModel model = new ResumeBlasterDetailModel();
            return model.ListResumeBlasters(pageSize, pageNumber, sortBy, sortOrder);
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(int pageSize)
        {
            return ResumeBlasterDetailModel.GetPaginationInfo(pageSize);
        }

        /// <summary>
        /// Get particular recruiter
        /// </summary>
        /// <param name="Id">>recruiter Id</param>
        /// <returns>Resume Blaster Model</returns>

        public ResumeBlasterDetailModel GetResumeBlasterDetail(Guid? Id)
        {
            ResumeBlasterDetailModel model = new ResumeBlasterDetailModel();
            return model.GetResumeBlasterDetail(Id);
        }

        public Guid CreateResumeBlasterDetail(ResumeBlasterDetailModel resumeBlasterDetailModel)
        {
            return resumeBlasterDetailModel.ResumeBlasterDetail(resumeBlasterDetailModel);
        }

        public void UpdateResumeBlasterDetail(ResumeBlasterDetailModel resumeBlasterDetailModel)
        {

            resumeBlasterDetailModel.UpdateResumeBlasterDetail(resumeBlasterDetailModel);
        }
    }
}
