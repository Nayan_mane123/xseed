﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Account;
using XSeed.Data.ViewModel.User.UserCommon;

namespace XSeed.Business.User.UserCommon
{
    public class UserCommon
    {
        #region User Defined Functions

        /// <summary>
        /// Change User Password
        /// </summary>
        /// <param name="model">ChangePasswordModel</param>
        public void ChangeUserPassword(ChangePasswordModel model)
        {
            ChangePasswordModel.ChangeUserPassword(model);
        }

        /// <summary>
        /// Reset User Password
        /// </summary>
        /// <param name="userName">UserName</param>
        /// <param name="newPassword">NewPassword</param>
        public void ResetUserPassword(string userName, string newPassword)
        {
            SetPasswordModel.ResetUserPassword(userName, newPassword);
        }

        /// <summary>
        /// Get all roles list
        /// </summary>        
        /// <returns>User role list</returns>
        public List<UserRoleModel> GetAllUserRoles(Guid organizationId)
        {
            return UserRoleModel.GetAllUserRoles(organizationId);
        }

        /// <summary>
        /// Get user role detail by Id
        /// </summary>
        /// <param name="Id">Role Id</param>
        /// <returns>user role detail </returns>
        public UserRoleModel GetUserRoleById(Guid Id)
        {
            return UserRoleModel.GetUserRoleById(Id);
        }

        /// <summary>
        /// CreateUserRole
        /// </summary>
        /// <param name="userRoleModel">UserRoleModel</param>
        public void CreateUserRole(UserRoleModel model)
        {
            var existingUserRoles = UserRoleModel.GetAllUserRoles(model.OrganizationId);
            bool isRoleAlreadyExist = existingUserRoles.Where(x => x.Role.ToLower().Trim() == model.Role.ToLower().Trim()).Any();

            if (!isRoleAlreadyExist)
            {
                UserRoleModel.CreateUserRole(model);
            }
            else
            {
                throw new Exception("User role already exists.");
            }
        }

        /// <summary>
        /// Update user role
        /// </summary>
        /// <param name="userRoleModel">User role model</param>
        /// <returns>Status Ok - 200</returns>
        public void UpdateUserRole(UserRoleModel userRoleModel)
        {
            UserRoleModel.UpdateUserRole(userRoleModel);
        }

        /// <summary>
        /// Delete user role
        /// </summary>
        /// <param name="Id">User role id</param>
        /// <returns>Status Ok - 200</returns>
        public void DeleteUserRole(Guid userRoleId)
        {
            UserRoleModel.DeleteUserRole(userRoleId);
        }

        /// <summary>
        /// Save User Email Configuration
        /// </summary>
        /// <param name="model">Email Configuration Detail Model</param>
        public static void SaveUserEmailConfiguration(EmailConfigurationDetailModel model)
        {
            EmailConfigurationDetailModel.SaveUserEmailConfiguration(model);
        }

        public static EmailConfigurationDetailModel GetUserEmailConfiguration(Guid UserId)
        {
            return (EmailConfigurationDetailModel.GetUserEmailConfiguration(UserId));
        }

        /// <summary>
        /// Update User Email Configuration
        /// </summary>
        /// <param name="model">EmailConfigurationDetailModel</param>
        public static void UpdateUserEmailConfiguration(EmailConfigurationDetailModel model)
        {
            EmailConfigurationDetailModel.UpdateUserEmailConfiguration(model);
        }

        public string GetUserTypeByEmail(string userName)
        {
            return UserRoleModel.GetUserTypeByEmail(userName);
        }

        /// <summary>
        /// Change Candidate Password
        /// </summary>
        /// <param name="model">Change Password Model</param>
        public void ChangeCandidatePassword(ChangePasswordModel model)
        {
            ChangePasswordModel.ChangeCandidatePassword(model);
        }

        #endregion
    }
}