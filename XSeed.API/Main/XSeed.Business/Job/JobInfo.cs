﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Activity;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Job;

namespace XSeed.Business.Job
{
    public class JobInfo
    {
        /// <summary>
        /// Get List of All Jobs
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <returns>List of All Jobs</returns>
        public List<JobDetailModel> ListAllJobs(Guid organizationId)
        {
            return (JobDetailModel.ListAllJobs(organizationId));
        }

        /// <summary>
        /// Get list of jobs using server side pagination
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>        
        /// <returns>Jobs List</returns>
        public List<JobDetailModel> ListAllJobs(Guid organizationId, int pageSize, int pageNumber, string sortBy = "ModifiedOn", string sortOrder = "desc", bool isExport = false)
        {
            return (JobDetailModel.ListAllJobs(organizationId, pageSize, pageNumber, sortBy, sortOrder, isExport));
        }

        /// <summary>
        /// Get list of jobs by user and status using server side pagination
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <param name="userId">user Id</param>
        /// <param name="status">status</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>        
        /// <returns>Jobs List</returns>
        public List<JobDetailModel> ListAllJobs(Guid organizationId, Guid userId, string status, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", Boolean isPriority=false)
        {
            if (userId != null && userId != Guid.Empty)
            {
                return JobDetailModel.ListUserJobsByStatus(organizationId, userId, status, pageSize, pageNumber, sortBy, sortOrder, isPriority);
            }
            else
            {
                return JobDetailModel.ListAllJobs(organizationId, pageSize, pageNumber, sortBy, sortOrder);
            }

        }

        /// <summary>
        /// Get Job Details
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns>JobDetailModel</returns>
        public JobDetailModel GetJobDetail(Guid organizationId, Guid? jobId)
        {
            return (JobDetailModel.GetJobDetail(organizationId, jobId));
        }

        /// <summary>
        /// Get Job Details
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns>JobDetailModel</returns>
        public JobDetailModel GetCollapsingJobDetail(Guid organizationId, Guid? jobId)
        {
            return (JobDetailModel.GetCollapsingJobDetail(organizationId, jobId));
        }

        /// <summary>
        /// Create Job
        /// </summary>
        /// <param name="jobDetailModel">Job Detail Model</param>
        /// <returns>Id of Newly Added Job</returns>
        public Guid CreateJob(JobDetailModel jobDetailModel)
        {
            return (JobDetailModel.CreateJob(jobDetailModel));
        }

        /// <summary>
        /// Create Job
        /// </summary>
        /// <param name="jobDetailModel">Job Detail Model</param>
        /// <returns>Id of Newly Added Job</returns>
        public int TelegramJob(JobDetailModel jobDetailModel)
        {
            return (JobDetailModel.TelegramJob(jobDetailModel));
        }

        /// <summary>
        /// Update Job
        /// </summary>
        /// <param name="jobDetailModel">Job Detail Model</param>
        /// <returns></returns>
        public void UpdateJob(JobDetailModel jobDetailModel)
        {
            JobDetailModel.UpdateJob(jobDetailModel);
        }

        /// <summary>
        /// Update Job Status
        /// </summary>
        /// <param name="id">JobId</param>
        /// <param name="jobStatusId">JobStatusId</param>
        public void UpdateJobStatus(Guid jobId, Guid jobStatusId)
        {
            JobDetailModel.UpdateJobStatus(jobId, jobStatusId);
        }

        /// <summary>
        /// Get job list by company
        /// </summary>
        /// <param name="CompanyId">CompanyId</param>
        /// <returns>Job list</returns>
        public List<JobDetailModel> GetJobsByCompany(Guid CompanyId)
        {
            return JobDetailModel.GetJobsByCompany(CompanyId);
        }

        /// <summary>
        /// Get list of candidates associated with the job
        /// </summary>
        /// <param name="jobId">Job Id</param>
        /// <returns>List of associated candidates and their status</returns>
        public List<JobCandidateMapModel> GetAssociatedCandidateList(Guid jobId)
        {
            return JobCandidateMapModel.GetAssociatedCandidateList(jobId);
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid organizationId, int pageSize)
        {
            return JobDetailModel.GetPaginationInfo(organizationId, pageSize);
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid organizationId, Guid userId, string status, int pageSize, Boolean isPriority=false)
        {
            return JobDetailModel.GetPaginationInfo(organizationId, userId, status, pageSize, isPriority);
        }

        /// <summary>
        /// Add new business unit
        /// </summary>
        /// <param name="lookUpModel">LookUpModel</param>
        /// <param name="organizationId">organization Id</param>
        /// <returns>Business Unit Id</returns>
        public Guid AddBusinessUnit(LookUpModel lookUpModel)
        {
            return JobDetailModel.AddBusinessUnit(lookUpModel);
        }

        /// <summary>
        /// Save quick notes related to Job
        /// </summary>
        /// <param name="note">Note</param>
        /// <param name="jobId">Job Id</param>
        /// <returns>Notes</returns>
        public List<Note> SaveQuickNote(string note, Guid jobId)
        {
            return JobDetailModel.SaveQuickNote(note, jobId);
        }

        public void DeleteQuickNotes(string Id)
        {
            JobDetailModel.DeleteQuickNotes(Id);
        }

        public void UpdateQNote(QNoteModel request, string editid)
        {
            JobDetailModel.UpdateQNote(request, editid);
        }

        /// <summary>
        /// Get Quick Notes related to Job
        /// </summary>
        /// <param name="jobId">Job Id</param>
        /// <returns>Quick Notes</returns>
        public List<Note> GetQuickNotes(Guid jobId)
        {
            return JobDetailModel.GetQuickNotes(jobId);
        }

        /// <summary>
        /// Get Activity Log related to Job
        /// </summary>
        /// <param name="jobId">Job Id</param>
        /// <returns>Activity Logs</returns>
        public List<JobActivityLogModel> GetJobActivityLog(Guid jobId)
        {
            return JobDetailModel.GetJobActivityLog(jobId);
        }


        /// <summary>
        /// Get Requisition Id based on Organization
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <returns>Requisition Id</returns>
        public string GetRequisitionId(Guid organizationId)
        {
            JobDetailModel model = new JobDetailModel();
            return model.GetRequisitionId(organizationId);
        }

        /// <summary>
        /// Check for Duplicate Requisition Id
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <param name="requisitionId">Requisition Id</param>
        /// <returns>True/ False</returns>
        public bool IsDuplicateRequisitionId(Guid organizationId, string requisitionId)
        {
            JobDetailModel model = new JobDetailModel();
            return model.IsDuplicateRequisitionId(organizationId, requisitionId);
        }

        /// <summary>
        /// Update Business Unit
        /// </summary>
        /// <param name="lookUpModel"></param>
        /// <param name="organizationId"></param>
        public static void PutBusinessUnit(LookUpModel lookUpModel)
        {
            JobDetailModel.PutBusinessUnit(lookUpModel);
        }

        /// <summary>
        /// Delete Business Unit
        /// </summary>
        /// <param name="genericLookupId"></param>
        public static void DeleteBusinessUnit(Guid genericLookupId)
        {
            JobDetailModel.DeleteBusinessUnit(genericLookupId);
        }

        /// <summary>
        /// Source Candidates against search parameters
        /// </summary>
        /// <param name="searchModel">Candidate Source Search Model</param>
        /// <param name="pageSize">page Size</param>
        /// <param name="pageNumber">page Number</param>
        /// <param name="sortBy">sort By</param>
        /// <param name="sortOrder">sort Order</param>
        /// <param name="isExport">Is Export</param>
        /// <returns>Candidates</returns>
        public List<CandidateSourceResultModel> SourceCandidates(CandidateSourceSearchModel searchModel, int pageSize, int pageNumber, string sortBy, string sortOrder, bool isExport)
        {
            return JobDetailModel.SourceCandidates(searchModel, pageSize, pageNumber, sortBy, sortOrder, isExport);
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>
        /// <param name="jobId">Job Id</param>
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetSourceSearchPaginationInfo(CandidateSourceSearchModel searchModel, int pageSize)
        {
            return JobDetailModel.GetSourceSearchPaginationInfo(searchModel, pageSize);
        }

        /// <summary>
        /// Get Monster Lincense Info
        /// </summary>
        /// <returns>Portal License Info</returns>
        public PortalLicenseInfo GetMonsterLicenseInfo()
        {
            return JobDetailModel.GetMonsterLicenseInfo();
        }

        /// <summary>
        /// List Active Requirements to display on tCognition Website
        /// </summary>
        /// <returns>Active Jobs</returns>
        public object ListActiveRequirements(Nullable<Guid> Id = null, string countryId = "")
        {
            return JobDetailModel.ListActiveRequirements(Id, countryId);
        }

        /// <summary>
        /// Get Activity Log related to Job
        /// </summary>
        /// <param name="jobId">Job Id</param>
        /// <returns>Activity Logs</returns>
        public List<JobActivityLogModel> GetDashboardJobActivityLog(Guid organizationId)
        {
            return JobDetailModel.GetDashboardJobActivityLog(organizationId);
        }
        public List<Note> GetDashboardQuickNotes(Guid organizationId)
        {
            return JobDetailModel.GetDashboardQuickNotes(organizationId);
        }
    }
}
