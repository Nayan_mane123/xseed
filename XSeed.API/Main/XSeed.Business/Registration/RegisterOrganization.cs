﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Registration;
using XSeed.Data.ViewModel.User.OrganizationUser;
using XSeed.Data.ViewModel.User.UserCommon;
using XSeed.Utility;

namespace XSeed.Business.Registration
{
    public class RegisterOrganization
    {
        #region User Defined Functions

        /// <summary>
        /// function to Save Registration Info
        /// </summary>
        public List<OrganizationUserModel> SaveRegistrationInfo(RegisterOrganizationModel model, List<UserRoleModel> userRoleModelList)
        {
            var userTypes = UserRoleModel.GetUserTypes();

            /* Update Model with default values */
            model.UserType = userTypes.Where(x => x.Name.ToLower() == "Recruiter".ToLower()).Select(x => x.Id.Value).FirstOrDefault();

            /* Save Registration Info */
            var result = model.SaveRegistrationInfo(model, userRoleModelList);
            return result;


        }

          //get active subscription plan details
        public List<SubscriptionPlanModel> GetChildOrganizations(Guid OrganizationId)
        {
            RegisterOrganizationModel RegisterOrganization = new RegisterOrganizationModel();

            return RegisterOrganization.GetChildOrganizations(OrganizationId);
        }

         //get active subscription plan details
        public List<SubscriptionPlanModel> GetActivePlanFeatures(Guid ActivePlanId, Guid OrganizationId)
        {
            RegisterOrganizationModel RegisterOrganization = new RegisterOrganizationModel();

            return RegisterOrganization.GetActivePlanFeatures(ActivePlanId, OrganizationId);
        }

        // get subscription plans  
        public List<SubscriptionPlanModel> GetSubscriptionPlansFromDb()
        {
            RegisterOrganizationModel RegisterOrganization = new RegisterOrganizationModel();

            return  RegisterOrganization.GetSubscriptionPlans();
        }
        // update organization subscription plan
        public List<SubscriptionPlanModel> UpdateOrganizationSubscriptionPlan(SubscriptionPlanModel model)
        {
            RegisterOrganizationModel RegisterOrganization = new RegisterOrganizationModel();
            /* Save Registration Info */
            var result = RegisterOrganization.UpdateOrganizationSubscriptionPlans(model);
            return result;


        }
        //  save paypal payment details
        public List<SubscriptionPlanModel> SavePaypalPaymentDetails(SubscriptionPlanModel model)
        {
            RegisterOrganizationModel RegisterOrganization = new RegisterOrganizationModel();
            /* Save Registration Info */
            var result = RegisterOrganization.SavePaypalPaymentDetails(model);
            return result;


        }
        


        /// <summary>
        /// function to check if organization already exists
        /// </summary>
        public bool IsOrganizationAlreadyExist(RegisterOrganizationModel model)
        {            
            /* Save Registration Info */
            return model.IsOrganizationAlreadyExist(model);
        }

        #endregion
    }
}