﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Common;

namespace XSeed.Business.Common
{
    public class APIExceptionLoggerInfo
    {
        /// <summary>
        /// Log API Exception
        /// </summary>
        /// <param name="exception">Exception</param>
        public static void LogException(Exception exception)
        {
            APIExceptionLoggerModel exceptionModel = new APIExceptionLoggerModel();
            exceptionModel.ExceptionMessage = exception.Message;
            exceptionModel.ExceptionInnerException = exception.InnerException != null ? exception.InnerException.Message : null;
            exceptionModel.ExceptionStackTrace = exception.StackTrace;
            exceptionModel.CreatedOn = DateTime.UtcNow;
            exceptionModel.UserType = BaseModel.UserType;
            exceptionModel.UserId = BaseModel.UserType == "Candidate" ? BaseModel.CandidateInitiatedBy : BaseModel.InitiatedBy.ToString();

            exceptionModel.LogException(exceptionModel);
        }
    }
}
