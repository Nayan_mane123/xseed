﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Common;

namespace XSeed.Business.Common
{
    public class SchedulerInfo
    {
        /// <summary>
        /// Get list of Schedules
        /// </summary>        
        /// <param name="OrganizationUserId">Organization User Id</param>
        /// <returns>Schedules</returns>
        public List<SchedulerModel> ListSchedules(Guid organizationUserId)
        {
            return SchedulerModel.ListSchedules(organizationUserId);
        }

        /// <summary>
        /// Get single Schedule
        /// </summary>
        /// <param name="OrganizationUserId">Organization User Id</param>
        /// <param name="Id"> Schedule Log Id</param>
        /// <returns>Schedule Log</returns>
        public SchedulerModel GetScheduleDetail(Guid organizationUserId, string Id)
        {
            return SchedulerModel.GetScheduleDetail(organizationUserId, Id);
        }

        /// <summary>
        /// Update Schedule Log Detail
        /// </summary>
        /// <param name="scheduleModel">Schedule Log Model</param>
        public void PutScheduleLogDetail(Data.ViewModel.Common.SchedulerModel scheduleModel)
        {
            SchedulerModel.PutScheduleLogDetail(scheduleModel);
        }

        /// <summary>
        /// Post schedule Log Detail
        /// </summary>
        /// <param name="scheduleModel">schedule Model</param>
        public void PostScheduleLogDetail(Data.ViewModel.Common.SchedulerModel scheduleModel)
        {
            SchedulerModel.PostScheduleLogDetail(scheduleModel);
        }
    }
}