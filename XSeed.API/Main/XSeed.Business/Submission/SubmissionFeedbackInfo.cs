﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Submission;

namespace XSeed.Business.Submission
{
    public class SubmissionFeedbackInfo
    {
        /// <summary>
        /// Get submission feedback list for particular submission
        /// </summary>
        /// <param name="submissionId">Submission Id</param>
        /// <returns>List of feedback</returns>
        public List<SubmissionFeedbackModel> GetSubmissionFeedbackList(Guid submissionId)
        {
            SubmissionFeedbackModel model = new SubmissionFeedbackModel();
            return model.GetSubmissionFeedbackList(submissionId);
        }

        /// <summary>
        /// Add submission feedback
        /// </summary>
        /// <param name="submissionFeedbackModel">Submission Feedback Model </param>
        public void AddsubmissionFeedback(SubmissionFeedbackModel submissionFeedbackModel)
        {
            submissionFeedbackModel.AddSubmissionFeedback(submissionFeedbackModel);
        }
    }
}
