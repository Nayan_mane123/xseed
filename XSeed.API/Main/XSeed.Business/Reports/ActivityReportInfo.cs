﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Activity;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Reports;

namespace XSeed.Business.Reports
{
    public class ActivityReportInfo
    {
        /// <summary>
        /// Get Activity Report
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Activity Report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        public List<ActivityReportModel> GetActivityReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", string type = "W", bool isExport = false)
        {
            ActivityReportModel model = new ActivityReportModel();
            return model.GetActivityReport(organizationId, pageSize, pageNumber, sortBy, sortOrder, type, isExport);
        }

        public List<ActivityReportModel> getActivityReportGraphData(Guid organizationId, string type = "W")
        {
            ActivityReportModel model = new ActivityReportModel();
            return model.getActivityTrackerGraphReport(organizationId, type);
        }
        public List<JobActivityLogModel> OrganizationUserJobsActivityNote(Guid OrganizationUserId, string type = "W", int pageNo = 0, int pageLength = 10)
        {
            ActivityReportModel model = new ActivityReportModel();
            return model.OrganizationUserJobsActivityNote(OrganizationUserId, type, pageNo, pageLength);
        }

        public List<CandidateActivityLogModel> OrganizationUserCanidatesActivityNote(Guid OrganizationUserId, string type = "W", int pageNo = 0, int pageLength = 10)
        {
            ActivityReportModel model = new ActivityReportModel();
            return model.OrganizationUserCanidatesActivityNote(OrganizationUserId, type, pageNo, pageLength);
        }

        /// <summary>
        /// Get Pagination Info
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid organizationId, int pageSize,string type="W")
        {
            ActivityReportModel model = new ActivityReportModel();
            return model.GetPaginationInfo(organizationId, pageSize, type);
        }


        /// <summary>
        /// Get Activity Graph counts
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>Activity Graph counts</returns>
        public List<ActivityReportModel> getActivityTrackerGraphReport(Guid organizationId, string type)
        {
            ActivityReportModel model = new ActivityReportModel();
            return model.getActivityTrackerGraphReport(organizationId, type);
        }
    }
}
