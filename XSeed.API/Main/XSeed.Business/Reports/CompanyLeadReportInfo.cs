﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Reports;
using XSeed.Utility;

namespace XSeed.Business.Reports
{
    public class CompanyLeadReportInfo
    {
        /// <summary>
        /// Get Company Lead Report
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Company Lead Report</returns>
        public List<CompanyLeadReportModel> GetCompanyLeadReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", string type = "W", bool isExport = false)
        {
            CompanyLeadReportModel model = new CompanyLeadReportModel();
            return model.GetClientLeadReport(organizationId, pageSize, pageNumber, sortBy, sortOrder, type, isExport);
        }

        /// <summary>
        /// Get Pagination Info
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid organizationId, int pageSize, string type = "W")
        {
            CompanyLeadReportModel model = new CompanyLeadReportModel();
            return model.GetPaginationInfo(organizationId, pageSize, type);
        }

        /// <summary>
        /// Get Client Graph counts
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>Client Graph counts</returns>
        public List<GraphModel> getClientTrackerGraphReport(Guid organizationId, string type)
        {
            CompanyLeadReportModel model = new CompanyLeadReportModel();
            return model.getClientTrackerGraphReport(organizationId, type);
        }

        /// <summary>
        /// Get Client Counts
        /// </summary>
        /// <returns>Client All Count report</returns>
        public GraphRightCountModel getClientAllCounts(Guid organizationId)
        {
            CompanyLeadReportModel model = new CompanyLeadReportModel();
            return model.getClientAllCounts(organizationId);
        }
    }
}
