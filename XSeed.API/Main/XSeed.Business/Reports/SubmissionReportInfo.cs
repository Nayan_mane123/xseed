﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Reports;
using XSeed.Utility;

namespace XSeed.Business.Reports
{
    public class SubmissionReportInfo
    {
        /// <summary>
        /// Get Submission Report
        /// </summary>
        /// <returns>Submission report</returns>
        public List<SubmissionReportModel> GetSubmissionTrackerReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", string type = "W", bool isExport = false)
        {
            SubmissionReportModel model = new SubmissionReportModel();
            return model.GetSubmissionTrackerReport(organizationId, pageSize, pageNumber, sortBy, sortOrder, type, isExport);
        }

        /// <summary>
        /// Get Submission Counts
        /// </summary>
        /// <returns>Submission report</returns>
        public GraphRightCountModel GetSubmissionAllCount(Guid organizationId)
        {
            SubmissionReportModel model = new SubmissionReportModel();
            return model.GetSubmissionAllCounts(organizationId);
        }

        /// <summary>
        /// Get Pagination Info
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid organizationId, int pageSize, string type = "W")
        {
            SubmissionReportModel model = new SubmissionReportModel();
            return model.GetPaginationInfo(organizationId, pageSize, type);
        }

        /// <summary>
        /// Get Submission Graph counts
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>Submission Graph counts</returns>
        public List<GraphModel> getSubmissionTrackerGraphReport(Guid organizationId, string type)
        {
            SubmissionReportModel model = new SubmissionReportModel();
            return model.getSubmissionTrackerGraphReport(organizationId, type);
        }
    }
}