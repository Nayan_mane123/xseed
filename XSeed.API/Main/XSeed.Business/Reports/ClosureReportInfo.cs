﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Reports;
using XSeed.Utility;

namespace XSeed.Business.Reports
{
    public class ClosureReportInfo
    {
        /// <summary>
        /// Get Closure Report
        /// </summary>
        /// <returns>Closure report</returns>
        public List<ClosureReportModel> GetClosureReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", string type = "W", bool isExport = false)
        {
            ClosureReportModel model = new ClosureReportModel();
            return model.GetClosureReport(organizationId, pageSize, pageNumber, sortBy, sortOrder, type, isExport);
        }

        /// <summary>
        /// Get Pagination Info
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid organizationId, int pageSize, string type = "W")
        {
            ClosureReportModel model = new ClosureReportModel();
            return model.GetPaginationInfo(organizationId, pageSize, type);
        }

        /// <summary>
        /// Get Closure Graph counts
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>Closure Graph counts</returns>
        public List<GraphModel> getClosureTrackerGraphReport(Guid organizationId, string type)
        {
            ClosureReportModel model = new ClosureReportModel();
            return model.getClosureTrackerGraphReport(organizationId, type);
        }


        /// <summary>
        /// Get Closure Counts
        /// </summary>
        /// <returns>Closure All Count report</returns>
        public GraphRightCountModel getClosureAllCounts(Guid organizationId)
        {
            ClosureReportModel model = new ClosureReportModel();
            return model.getClosureAllCounts(organizationId);
        }
    }
}