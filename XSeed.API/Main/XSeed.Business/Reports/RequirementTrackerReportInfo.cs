﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Reports;
using XSeed.Utility;

namespace XSeed.Business.Reports
{
    public class RequirementTrackerReportInfo
    {
        /// <summary>
        /// Get Requirement Tracker Report
        /// </summary>
        /// <returns>Requirement tracker report</returns>
        public List<RequirementTrackerReportModel> GetRequirementTrackerReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", string type = "W", bool isExport = false)
        {
            RequirementTrackerReportModel model = new RequirementTrackerReportModel();
            return model.GetRequirementTrackerReport(organizationId, pageSize, pageNumber, sortBy, sortOrder, type, isExport);
        }

        /// <summary>
        /// Get Requirement All Count
        /// </summary>
        /// <returns>Get Requirement All Count</returns>
        public GraphRightCountModel GetRequirementTrackerReport(Guid organizationId)
        {
            RequirementTrackerReportModel model = new RequirementTrackerReportModel();
            return model.GetRequirementAllCount(organizationId);
        }

        /// <summary>
        /// Get Pagination Info
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid organizationId, int pageSize, string type = "W")
        {
            RequirementTrackerReportModel model = new RequirementTrackerReportModel();
            return model.GetPaginationInfo(organizationId, pageSize, type);
        }

        public List<GraphModel> GetRequirementTrackerGraphReport(Guid organizationId, string type)
        {
            RequirementTrackerReportModel model = new RequirementTrackerReportModel();
            return model.GetRequirementTrackerGraphReport(organizationId, type);
        }

        /// <summary>
        /// Get Requirement Graph Count
        /// </summary>
        /// <returns>Get Requirement Graph Count</returns>
        public List<RequirementTrackerReportModel> GetRequirementGraphCount(Guid organizationId)
        {
            RequirementTrackerReportModel model = new RequirementTrackerReportModel();
            return model.GetRequirementGraphCount(organizationId);
        }
    }
}
