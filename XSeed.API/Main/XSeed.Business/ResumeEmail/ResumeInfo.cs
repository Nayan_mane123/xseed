﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.ResumeEmail;

namespace XSeed.Business.ResumeEmail
{
    public class ResumeInfo
    {
        /// <summary>
        /// Get List of All Resume Emails
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <returns>List of All Resume Emails</returns>
        public List<ResumeEmailParserModel> ListAllResumeEmails(Guid organizationId, int pageSize, int pageNumber, string sortBy = "Date", string sortOrder = "desc")
        {
            return (ResumeEmailParserModel.ListAllResumeEmails(organizationId, pageSize, pageNumber, sortBy, sortOrder));
        }

        /// <summary>
        /// Get Job Details
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns>JobDetailModel</returns>
        public ResumeEmailParserModel GetResumeEmailDetail(Guid organizationId, Guid? resumeId)
        {
            return (ResumeEmailParserModel.GetResumeEmailDetail(organizationId, resumeId));
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid organizationId, int pageSize)
        {
            return ResumeEmailParserModel.GetPaginationInfo(organizationId, pageSize);
        }

        /// <summary>
        /// Update Resume Parsed Email
        /// </summary>
        /// <param name="resumeEmailParserModel">Resume Email Parser Model</param>
        /// <returns></returns>
        public void UpdateResumeParsedEmail(Guid Id)
        {
            ResumeEmailParserModel.UpdateResumeParsedEmail(Id);
        }

        /// <summary>
        /// Get Unread Mail Count 
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <returns>Unread Mail Count</returns>
        public int GetUnreadMailCount(Guid organizationId)
        {
            return ResumeEmailParserModel.GetUnreadMailCount(organizationId);
        }
    }
}
