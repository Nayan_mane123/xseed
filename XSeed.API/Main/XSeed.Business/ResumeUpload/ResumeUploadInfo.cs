﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.ResumeParser;

namespace XSeed.Business.ResumeUpload
{
    public class ResumeUploadInfo
    {
        /// <summary>
        /// Resume Bulk Upload
        /// </summary>
        /// <param name="Resumes">List of Resumes</param>
        /// <param name="Id">Id</param>
        /// <returns>Count of uploaded files</returns>
        public int SaveResumeUploadFiles(List<XSeedFileEntity> Resumes, string Id)
        {
            if (Resumes != null)
            {
                return (XSeedFileEntity.SaveResumeUploadFiles(Resumes, "ResumeUpload"));
            }
            return 0;
        }

        /// <summary>
        /// Get Parsed Resumes
        /// </summary>
        /// <returns>List of Parsed Resumes</returns>
        public List<ParsedResumeViewModel> GetParsedResumes(int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc")
        {
            return (ParsedResumeViewModel.GetParsedResumes(pageSize, pageNumber, sortBy, sortOrder));
        }

        /// <summary>
        /// Reject Parsed Resume
        /// </summary>
        /// <param name="Id">Parsed Resume Id</param>
        /// <returns></returns>
        public void AcceptRejectParsedResume(string parsedResumeId, bool IsValidResume = false)
        {
            ParsedResumeViewModel.AcceptRejectParsedResume(parsedResumeId, IsValidResume);
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>        
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(int pageSize)
        {
            return (ParsedResumeViewModel.GetPaginationInfo(pageSize));
        }
    }
}
