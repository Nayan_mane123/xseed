﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XSeed.Utility
{

     public class GraphModel
    {
        public int Count { get; set; }
        public string Label { get; set; }
    }

     public class GraphRightCountModel
     {
         public int YearlyCount { get; set; }
         public int MonthlyCount { get; set; }
         public int WeeklyCount { get; set; }
         public int HalfYearlyCount { get; set; }
         public int QuaterlyCount { get; set; }
     }

    public static class GraphReportDataUtility
    {
        public static DateTime GetUTCDate()
        {
            return DateTime.UtcNow; 
        }

        public static DateTime GetBeforeDate(string type)
        {
            DateTime beforeDate = DateTime.Now;
            if (type == "D")
            {
                beforeDate = DateTime.Today;
            }
            else if (type == "W")
            {
                beforeDate =  DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Monday);
            }
            else if (type == "M")
            {
                beforeDate = new DateTime(beforeDate.Year, beforeDate.Month, 1);
            }
            else if (type == "Y")
            {
                beforeDate = new DateTime(beforeDate.Year, 1, 1);
            }
            else if (type == "HY")
            {
               
                if (beforeDate.Month <= 6)
                {
                    beforeDate = new DateTime(beforeDate.Year, 1, 1);
                }
                else
                {
                    beforeDate = new DateTime(beforeDate.Year, 7, 1);
                }
            }
            else if (type == "Q")
            {
                int quarterNumber = (beforeDate.Month - 1) / 3 + 1;
                beforeDate = new DateTime(beforeDate.Year, (quarterNumber - 1) * 3 + 1, 1);
            }
            return beforeDate;
        }

        public static List<GraphModel> getreportdatabytype(List<GraphModel> graphData, string type ,bool isClient=false)
        {
                List<GraphModel> returnData = new List<GraphModel>();

            string[] labelNames = new string[]{};
                //WEEKLY DATA
                if (isClient) //QURTERLY DATA
                {
                    foreach (var graphdata in graphData)
                    {
                        GraphModel graphModelData = new GraphModel();
                        graphModelData.Label = graphdata.Label;
                        graphModelData.Count = graphdata.Count;
                        returnData.Add(graphModelData);
                    }
                    return returnData;
                }
                else if (type == "D")
                {
                    labelNames = new string[] { "Today"};
                }
                
                else if (type == "W" || type == "D")
                {
                    labelNames = new string[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
                }
                else if (type == "M") //MONTHLY DATA
                {
                    foreach (var graphdata in graphData)
                    {
                        GraphModel graphModelData = new GraphModel();
                        graphModelData.Label = graphdata.Label;
                        graphModelData.Count = graphdata.Count;
                        returnData.Add(graphModelData);
                    }
                    return returnData;
                }
               
                else if (type == "Q") //QURTERLY DATA
                {
                    labelNames = new string[] { "Quarter 1", "Quarter 2", "Quarter 3", "Quarter 4" };
                }
              
                else if (type == "HY") //HALF YEARLY DATA
                {
                    labelNames = new string[] { "1st Half", "2nd Half" };
                }
                else if (type == "Y") //YEARLY DATA
                {
                    labelNames = new string[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
                }

                // create master label data
                for (var i = 0; i < labelNames.Length; i++)
                {
                    GraphModel graphModelData = new GraphModel();
                    graphModelData.Label = labelNames[i];
                    returnData.Add(graphModelData);
                }
                // add graph data
                
                foreach (var graphdatacount in graphData)
                    returnData.Find(x => x.Label == graphdatacount.Label).Count = graphdatacount.Count;
                return returnData;
        }




    }

}
