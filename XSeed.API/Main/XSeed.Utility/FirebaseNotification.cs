﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace XSeed.Utility
{
    public static class FbaseNotification
    {
        private static Uri FireBasePushNotificationsURL = new Uri("https://fcm.googleapis.com/fcm/send");
        private static string ServerKey = ConfigurationManager.AppSettings["FirebaseServerKey"];

        public static async Task<bool> SendPushNotification(string jsonMessage)
        {
                bool sent = false;
                //Create request to Firebase API
                var request = new HttpRequestMessage(HttpMethod.Post, FireBasePushNotificationsURL);

                request.Headers.TryAddWithoutValidation("Authorization", "key=" + ServerKey);
                request.Content = new StringContent(jsonMessage, Encoding.UTF8, "application/json");

                HttpResponseMessage result;
                using (var client = new HttpClient())
                {
                    result = await client.SendAsync(request);
                    sent = sent && result.IsSuccessStatusCode;
                }
           

            return sent;
        }

        public static object getMessageJson(String[] tokens, String title, String body, String data)
        {
            var messageInformation = new
            {
                notification = new
                {
                    title = title,
                    text = body
                },
                data=data,
                registration_ids = tokens
            };

            return messageInformation;
        }
    }
}
