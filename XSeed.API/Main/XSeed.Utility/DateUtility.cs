﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XSeed.Utility
{
    public static class DateUtility
    {
        public static DateTime GetUTCDate()
        {
            return DateTime.UtcNow; 
        }
    }
}
