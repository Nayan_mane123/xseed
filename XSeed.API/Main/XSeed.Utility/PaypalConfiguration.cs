﻿using PayPal.Api;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
 

namespace XSeed.Utility
{
    public class SubscriptionPlanModel
    {

        public Guid Id { get; set; }
        public string PlanName { get; set; }
        public int PlanPrice { get; set; }
        public string Description { get; set; }
        
        //.............Child Organization
        public string OrganizationName { get; set; }
        
         
        //............for save payment
       
        public string guid { get; set; }

        public Guid organizationId { get; set; }
        public Guid? SubscriptionPlanId { get; set; }
        
        public double PaymentAmount { get; set; }
        public string PaymentStatus { get; set; }
        public DateTime PaymentDate { get; set; }
        public string PaypalReferenceId { get; set; }
        public string PaypalPaymentStatus { get; set; }
        public string PaypalPayerId { get; set; }
        public string PaypalInvoiceNumber { get; set; }

        public string PaypalPaymentId { get; set; }
        public string PaypalToken { get; set; }

        public Guid FeatureId { get; set; }
        public string Feature { get; set; }
        public short? IsActive { get; set; }
        public int? Limit { get; set; }

        public int? PlanLimit { get; set; }
        
    }

     
    

    public class PaypalConfiguration
    {
        //Variables for storing the clientID and clientSecret key  
        public readonly static string ClientId;
        public readonly static string ClientSecret;
         
        //Constructor  
        static PaypalConfiguration()
        {
            var config = GetConfig();
            ClientId = config["clientId"];
            ClientSecret = config["clientSecret"];
             
        }
        // getting properties from the web.config  
        public static Dictionary<string, string> GetConfig()
        {
            return PayPal.Api.ConfigManager.Instance.GetProperties();
        }
        private static string GetAccessToken()
        {
            // getting accesstocken from paypal  
            string accessToken = new OAuthTokenCredential(ClientId, ClientSecret, GetConfig()).GetAccessToken();
            return accessToken;
        }
        public static APIContext GetAPIContext()
        {
            // return apicontext object by invoking it with the accesstoken  
            APIContext apiContext = new APIContext(GetAccessToken());
            apiContext.Config = GetConfig();
            return apiContext;
        }  
    }

    public class PayPalTransactions 
    {
        
        private PayPal.Api.Payment payment;
        public string PayWithPaypal(SubscriptionPlanModel model)
        {

            //getting the apiContext  
            var apiContext = PaypalConfiguration.GetAPIContext();
            try
            {
                //A resource representing a Payer that funds a payment Payment Method as paypal  
                //Payer Id will be returned when payment proceeds or click to pay  
                
                if (string.IsNullOrEmpty(model.PaypalPayerId))
                {
                    //this section will be executed first because PayerID doesn't exist  
                    //it is returned by the create function call of the payment class  
                    // Creating a payment  
                    // baseURL is the url on which paypal sendsback the data.  

                     
                    string baseURI = ConfigurationManager.AppSettings["PaypalRedirectURL"];
                 
                 
                  
                    //here we are generating guid for storing the paymentID received in session  
                    //which will be used in the payment execution  
                    var guid = Convert.ToString((new Random()).Next(100000));
                    //CreatePayment function gives us the payment approval url  
                    //on which payer is redirected for paypal account payment  
                    var createdPayment = this.CreatePayment(model, apiContext, baseURI + "guid=" + guid);
                    //get links returned from paypal in response to Create function call  
                    var links = createdPayment.links.GetEnumerator();
                    string paypalRedirectUrl = null;
                    while (links.MoveNext())
                    {
                        Links lnk = links.Current;
                        if (lnk.rel.ToLower().Trim().Equals("approval_url"))
                        {
                            //saving the payapalredirect URL to which user will be redirected for payment  
                            paypalRedirectUrl = lnk.href;
                        }
                    }

                   
                 

                    return paypalRedirectUrl;
                }
                else
                {
                     
                    // This function exectues after receving all parameters for the payment    
                    var executedPayment = ExecutePayment(apiContext, model.PaypalPayerId, model.PaypalPaymentId as string);
                    //If executed payment failed then we will show payment failure message to user  
                    if (executedPayment.state != null && executedPayment.state.ToLower() != "approved")
                    {
                        return "FailureView";
                    }
                }
            }
            catch (Exception ex)
            {
                Exception exception = new Exception(ex.Message);
                throw exception;
            }
            //on successful payment, show success page to user.  
            return "Completed";


        }

         

        private Payment ExecutePayment(APIContext apiContext, string payerId, string paymentId)
        {
            var paymentExecution = new PaymentExecution()
            {
                payer_id = payerId
            };
            this.payment = new Payment()
            {
                id = paymentId
            };
            try
            {
                this.payment.Execute(apiContext, paymentExecution);
            }
            catch (Exception e) {

                Exception exception = new Exception(e.Message);
                throw exception;
            }
             return this.payment;
            
        }
        private Payment CreatePayment(SubscriptionPlanModel model, APIContext apiContext, string redirectUrl)
        {
            //create itemlist and add item objects to it  
            var itemList = new ItemList()
            {
                items = new List<Item>()
            };
            //Adding Item Details like name, currency, price etc  
            itemList.items.Add(new Item()
            {
                name = model.PlanName,//"Item Name comes here",
                currency = "USD",
                price = (model.PlanPrice).ToString(),//"1",
                quantity = "1",
                sku =  "sku"
            });
            var payer = new Payer()
            {
                payment_method = "paypal"
            };
            // Configure Redirect Urls here with RedirectUrls object  
            var redirUrls = new RedirectUrls()
            {
                cancel_url = redirectUrl + "&Cancel=true",
                return_url = redirectUrl
            };
            // Adding Tax, shipping and Subtotal details  
            var details = new Details()
            {
                tax = "1",
                shipping = "1",
                subtotal = (model.PlanPrice).ToString(),// "1"
            };
            //Final amount with details  
            var amount = new Amount()
            {
                currency = "USD",
                total = (Convert.ToInt32(details.tax) + Convert.ToInt32(details.subtotal)+ Convert.ToInt32(details.shipping)).ToString(),// "4", // Total must be equal to sum of tax, shipping and subtotal.  
                details = details
            };
            var transactionList = new List<Transaction>();
            // Adding description about the transaction  
            transactionList.Add(new Transaction()
            {
                description = model.Description,// "Transaction description",
                invoice_number = model.PaypalInvoiceNumber,//"your generated invoice number", //Generate an Invoice No  
                amount = amount,
                item_list = itemList
            });
            this.payment = new Payment()
            {
                intent = "sale",
                payer = payer,
                transactions = transactionList,
                redirect_urls = redirUrls
            };
            // Create a payment using a APIContext  
            return this.payment.Create(apiContext);
        }  
    
    }
}
