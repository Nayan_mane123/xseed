﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace XSeed.Utility
{
    /// <summary>
    /// Password Utility
    /// </summary>
    public static class PasswordUtility
    {
        #region Encrypt Password and Decrypt Password Logic --

        /// <summary>
        /// Encrypt Password
        /// </summary>
        /// <param name="stringToEncrypt">String To Encrypt</param>
        /// <returns>Encrypted Password</returns>
        public static string EncryptPassword(string stringToEncrypt)
        {
            byte[] Results;
            string encryptedPassword = "";
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes("Passord"));
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            byte[] DataToEncrypt = UTF8.GetBytes(stringToEncrypt);
            try
            {
                ICryptoTransform Encryptor = TDESAlgorithm.CreateEncryptor();
                Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);
                encryptedPassword = Convert.ToBase64String(Results);
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }
            return encryptedPassword;
        }

        /// <summary>
        /// Decrypt Password
        /// </summary>
        /// <param name="stringToDecrypt">String To Decrypt</param>
        /// <returns>Decrypted Password</returns>
        public static string DecryptPassword(string stringToDecrypt)
        {
            byte[] Results;
            string decryptedPassword = "";
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes("Passord"));
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            byte[] DataToDecrypt = Convert.FromBase64String(stringToDecrypt);
            try
            {
                ICryptoTransform Decryptor = TDESAlgorithm.CreateDecryptor();
                Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
                decryptedPassword = UTF8.GetString(Results);
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }
            return decryptedPassword;
        }

        #endregion

        #region Random Password Generation Logic

        /// <summary>
        /// Generate Random Password
        /// </summary>
        /// <param name="length">Length</param>
        /// <returns>Random Password</returns>
        public static string RandomPassword(int length)
        {
            StringBuilder builder = new StringBuilder();
            string randomPassword = "";
            Random random = new Random();
            char ch;
            for (int i = 0; i < length; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            randomPassword = builder.ToString();
            return randomPassword + "@a1";
        }

        #endregion
    }
}
