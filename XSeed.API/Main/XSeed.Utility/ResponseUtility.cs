﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XSeed.Utility
{
   public static class ResponseUtility
    {
        public static dynamic SuccessEmptyResponseData(String message)
        {
            var result = new
            {
                Status = "200",
                Message = message
            };

            return result;
        }

        public static dynamic SuccessResponseData(Object data)
        {
            var result = new
            {
                Status = "200",
                Data = data,
                Message="Success"
            };

            return result;
        }

        public static dynamic SuccessGUIDResponseData(Guid data)
        {
            var result = new
            {
                Status = "200",
                Data = data,
                Message = "Success"
            };

            return result;
        }
    }
}
