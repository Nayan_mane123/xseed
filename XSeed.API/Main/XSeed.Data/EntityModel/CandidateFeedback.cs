﻿

namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using XSeed.Data.ViewModel.Submission;
    using System.Configuration;
    using MongoDB.Bson;
    using MongoDB.Bson.Serialization.Attributes;
    using MongoDB.Driver;
    using XSeed.Data.ViewModel.User.CandidateUser;
    using XSeed.Data.ViewModel.Notification;
    using XSeed.Utility;


    [MetadataType(typeof(CandidateFeedbackMD))]
    public partial class CandidateFeedback : IAuditable
    {
        static string url = ConfigurationManager.AppSettings["MongoURL"].ToString();
        static string dbName = ConfigurationManager.AppSettings["MongoDB"].ToString();
        public static MongoClient client = new MongoClient(url);
        static IMongoDatabase database = client.GetDatabase(dbName);

        public class CandidateFeedbackMD
        {
            public int Id { get; set; }
            public Nullable<int> SubmissionId { get; set; }
            public Nullable<int> StatusId { get; set; }
            public string Remark { get; set; }
            public Nullable<System.DateTime> Date { get; set; }

            public double CommunicationRating { get; set; }
            public double TechnicalRating { get; set; }
            public double ExperianceRating { get; set; }
            public double EducationalRating { get; set; }
    
        }


        public Guid? ModifiedBy
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public DateTime? ModifiedOn
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Get candidate feedback list for particular submission
        /// </summary>
        /// <param name="submissionId">Submission Id</param>
        /// <returns>List of Candidate feedback</returns>
        internal static List<CandidateFeedbackModel> GetCandidateFeedbackList(Guid submissionId)
        {
            List<CandidateFeedbackModel> list = new List<CandidateFeedbackModel>();

            if (submissionId != null)
            {
                using (var db = new XSeedEntities())
                {
                    var Candidatefeedbacks = db.CandidateFeedbacks.Where(f => f.SubmissionId == submissionId).ToList().OrderByDescending(d => d.CreatedOn);

                    foreach (var feedback in Candidatefeedbacks)
                    {
                        CandidateFeedbackModel model = new CandidateFeedbackModel();

                        /* Populate model */
                        model.Id = feedback.Id;
                        model.SubmissionId = feedback.SubmissionId;
                        model.FeedbackType = "Candidate Feedback";
                        model.StatusId = feedback.StatusId;
                        model.Status = feedback.CandidateFeedbackMaster != null ? feedback.CandidateFeedbackMaster.Status : string.Empty;
                        model.Remark = feedback.Remark;
                        model.Date = feedback.Date;
                        model.CreatedBy = feedback.CreatedBy.HasValue ? OrganizationUserDetail.GetCreatedByUserName(feedback.CreatedBy) : string.Empty;
                        list.Add(model);
                    }
                }
            }

            return list;
        }


        /// <summary>
        /// Add Candidate Feedback
        /// </summary>
        /// <param name="candidateFeedbackModel">Candidate Feedback Model </param>
        internal static void AddCandidateFeedback(CandidateFeedbackModel candidateFeedbackModel)
        {
            if (candidateFeedbackModel != null)
            {
                using (var db = new XSeedEntities())
                {
                    CandidateFeedback model = new CandidateFeedback();

                    /* Populate db model */
                    model.Id = candidateFeedbackModel.Id;
                    model.SubmissionId = candidateFeedbackModel.SubmissionId;
                    model.StatusId = candidateFeedbackModel.StatusId;
                    model.Remark = candidateFeedbackModel.Remark;
                    model.Date = candidateFeedbackModel.Date;

                    model.CommunicationRating = candidateFeedbackModel.CommunicationRating;
                    model.TechnicalRating = candidateFeedbackModel.TechnicalRating;
                    model.ExperianceRating = candidateFeedbackModel.ExperianceRating;
                    model.EducationalRating = candidateFeedbackModel.EducationalRating;

                    db.CandidateFeedbacks.Add(model);

                    /* Update Submission Status with latest record */
                    var submission = db.SubmissionDetails.Find(model.SubmissionId);
                    submission.StatusId = candidateFeedbackModel.StatusId;
                    submission.Status = candidateFeedbackModel.StatusId != null ? db.CandidateFeedbackMasters.Find(candidateFeedbackModel.StatusId).Status : string.Empty;
                    updateCandidateRating(candidateFeedbackModel);
                   
                    {
                        string companyEmail = submission.JobDetail.CompanyContact.PrimaryEmail;
                            EmailNotificationModel emailNotificationModel = new EmailNotificationModel();
                            List<string> toEmailIds = new List<string>();
                            toEmailIds.Add(companyEmail);
                            emailNotificationModel.ToEmailID = toEmailIds;
                            emailNotificationModel.TypeOfNotification = Constants.typeOfNotificationEmailSubmissionStatusAttachment;
                            emailNotificationModel.FromEmailID = ConfigurationManager.AppSettings["EmailFromUserName"];
                            emailNotificationModel.FromEmailPassword = ConfigurationManager.AppSettings["EmailFromPassword"];
                            emailNotificationModel.CallBackUrl = ConfigurationManager.AppSettings["LoginCallBackURL"];
                            emailNotificationModel.MailBody = submission.Status;
                            emailNotificationModel.ExtraData = submission.CandidateName;
                            EmailNotificationModel.SendEmailNotification(emailNotificationModel);
                    }

                    db.SaveChanges();
                }
            }
        }

        internal static void updateCandidateRating(CandidateFeedbackModel candidateFeedbackModel)
        {
            decimal avgRating=Convert.ToDecimal(((candidateFeedbackModel.CommunicationRating + candidateFeedbackModel.TechnicalRating + candidateFeedbackModel.ExperianceRating + candidateFeedbackModel.EducationalRating) / 4));
           
            var filter = Builders<CandidateUserModel>.Filter.Eq("_id", candidateFeedbackModel.CandidateId);
            var collection = database.GetCollection<CandidateUserModel>("CandidateDetail");


            CandidateUserModel model = collection.Find(filter).FirstOrDefault();
            if(null!=model.Rating && ""!=model.Rating)
            {
                avgRating = (Convert.ToDecimal(model.Rating) + avgRating) / 2;
                
            }

            avgRating = decimal.Round(avgRating, 1, MidpointRounding.AwayFromZero);
            var update = Builders<CandidateUserModel>.Update.Set(s => s.Rating, Convert.ToString(avgRating));
            collection.UpdateOne(filter, update);
        }
    }
}
