﻿namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [MetadataType(typeof(SubscriptionPaymentDetailMD))]
    public partial class SubscriptionPaymentDetail
    {
        public class SubscriptionPaymentDetailMD
        {
             
            public  Guid Id { get; set; }
            public  Guid  OrganizationId { get; set; }
            public  Guid  SubscriptionPlanId { get; set; }
            public  int  PlanPrice { get; set; }
            public  double PaymentAmount { get; set; }
            public string PaymentStatus { get; set; }
            public DateTime  PaymentDate { get; set; }
            public string PaypalReferenceId { get; set; }
            public string PaypalPaymentStatus { get; set; }
            public string PayerId { get; set; }
            public string InvoiceNumber { get; set; }
       }
    }
}
