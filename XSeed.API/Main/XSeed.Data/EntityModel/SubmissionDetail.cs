﻿
namespace XSeed.Data.Entity
{
    using Microsoft.Practices.EnterpriseLibrary.Logging;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Diagnostics;
    using System.Linq;
    using System.Linq.Dynamic;
    using System.Text;
    using System.Threading.Tasks;
    using XSeed.Data.ViewModel.Common;
    using XSeed.Data.ViewModel.Submission;
    using XSeed.Data.ViewModel.User.CandidateUser;

    [MetadataType(typeof(SubmissionDetailMD))]
    public partial class SubmissionDetail : IAuditable
    {
        #region Property Declaration

        public class SubmissionDetailMD
        {
            public System.Guid Id { get; set; }
            public System.Guid JobId { get; set; }
            public Nullable<System.Guid> OrganizationUserId { get; set; }
            public string CandidateId { get; set; }
            public string CandidateName { get; set; }
            public Nullable<System.DateTime> AssignedDate { get; set; }
            public Nullable<System.Guid> StatusId { get; set; }
            public string Status { get; set; }
            public Nullable<decimal> ClientRate { get; set; }
            public Nullable<decimal> Margin { get; set; }
        }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Submissions
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <returns>List of Submissions</returns>
        internal static List<SubmissionModel> GetSubmissions(Guid organizationId)
        {
            List<SubmissionModel> list = new List<SubmissionModel>();
            using (var db = new XSeedEntities())
            {
                var submissions = db.SubmissionDetails.Where(s => s.JobDetail.CompanyDetail.OrganizationId == organizationId).OrderByDescending(d => d.CreatedOn);

                foreach (var submission in submissions)
                {
                    SubmissionModel model = ListSubmissionModel(submission);
                    list.Add(model);
                }
            }

            return list;
        }

        /// <summary>
        /// Get list of submission using server side pagination
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>        
        /// <returns>submission List</returns>
        internal static List<SubmissionModel> GetSubmissions(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", bool isExport = false)
        {
            List<SubmissionModel> list = new List<SubmissionModel>();

            using (var db = new XSeedEntities())
            {
                //var totalCount = db.SubmissionDetails.Where(o => o.JobDetail.OrganizationId == organizationId).Count();
                //var totalPages = Math.Ceiling((double)totalCount / pageSize);

                var submissions = isExport ? db.SubmissionDetails.Where(o => o.JobDetail.OrganizationId == organizationId).OrderBy(sortBy + " " + sortOrder).ToList()
                                           : db.SubmissionDetails.Where(o => o.JobDetail.OrganizationId == organizationId).OrderBy(sortBy + " " + sortOrder).ToList()
                                           .Skip((pageNumber - 1) * pageSize).Take(pageSize);

                foreach (var submission in submissions)
                {
                    SubmissionModel model = ListSubmissionModel(submission);
                    list.Add(model);
                }
            }

            return list;
        }

        /// <summary>
        /// Populate Submission Model
        /// </summary>
        /// <param name="submission">Submission </param>
        /// <returns>Submission Model</returns>
        public static SubmissionModel ListSubmissionModel(SubmissionDetail submission)
        {
            SubmissionModel model = new SubmissionModel();

            /* Populate model */
            model.Id = submission.Id;

            /* Company Info */
            model.CompanyId = submission.JobDetail != null ? submission.JobDetail.CompanyId : Guid.Empty;
            model.CompanyName = submission.JobDetail != null ? (submission.JobDetail.CompanyDetail != null ? submission.JobDetail.CompanyDetail.Name : string.Empty) : string.Empty;
            model.ContactEmailList = CompanyContact.GetContactEmailByCompany(model.CompanyId);

            /* Job Info */
            model.JobId = submission.JobId;
            model.JobTitle = submission.JobDetail != null ? submission.JobDetail.JobTitle : string.Empty;
            model.JobType = submission.JobDetail != null ? (submission.JobDetail.JobTypeMaster != null ? submission.JobDetail.JobTypeMaster.Type : string.Empty) : string.Empty;
            model.JobCode = submission.JobDetail != null ? submission.JobDetail.RequisitionId : string.Empty;

            /* Recruiter Info */
            model.OrganizationUserId = submission.OrganizationUserId;
            model.OrganizationUserName = submission.OrganizationUserDetail != null ? (submission.OrganizationUserDetail.FirstName + " " + submission.OrganizationUserDetail.LastName) : string.Empty;

            /* Candidate Info */
            model.CandidateList = new List<MongoLookUpModel>();
            model.CandidateList.Add(new MongoLookUpModel() { Id = submission.CandidateId, Name = submission.CandidateName });

            /* Submission Info */
            model.AssignedDate = submission.AssignedDate;
            model.StatusId = submission.StatusId;
            model.Status = submission.Status;
            model.ClientRate = submission.ClientRate;
            model.Margin = submission.Margin;

            /* Audit Trail Info */
            model.CreatedBy = OrganizationUserDetail.GetCreatedByUserName(submission.CreatedBy);
            model.CreatedOn = submission.CreatedOn;
            model.ModifiedBy = OrganizationUserDetail.GetCreatedByUserName(submission.ModifiedBy);
            model.ModifiedOn = submission.ModifiedOn;

            return model;
        }

        /// <summary>
        /// Get paritcular Submission Detail
        /// </summary>
        /// <param name="Id">Submission Id</param>
        /// <returns>Submission Detail</returns>
        internal static SubmissionModel GetSubmissionDetail(Guid organizationId, Guid? Id)
        {
            SubmissionModel model = new SubmissionModel();

            using (var db = new XSeedEntities())
            {
                var submission = db.SubmissionDetails.Find(Id);

                if (submission != null)
                {
                    model = PopulateSubmissionModel(submission);
                }
            }

            return model;
        }

        /// <summary>
        /// Create New Submission
        /// </summary>
        /// <param name="SubmissionModel">Submission Detail Model </param>
        internal static void CreateSubmissionDetail(SubmissionModel submissionDetailModel, out List<string> successCandidateList, out string message)
        {
            int count = 0;
            StringBuilder candidateNames = new StringBuilder();
            successCandidateList = new List<string>();
            message = string.Empty;


            using (var db = new XSeedEntities())
            {
                foreach (var candidate in submissionDetailModel.CandidateList)
                {
                    /* check if candidate is already submitted for the job */
                    if (db.SubmissionDetails.Any(s => s.CandidateId == candidate.Id && s.JobId == submissionDetailModel.JobId))
                    {
                        var CandidateDetail = CandidateUserModel.GetCandidateUserDetail(candidate.Id);

                        if (CandidateDetail != null)
                        {
                            candidateNames.Append(" " + CandidateDetail.FirstName + " " + CandidateDetail.LastName + ",");
                            count++;
                        }
                    }
                    else
                    {
                        SubmissionDetail model = new SubmissionDetail();

                        var organizationUser = db.OrganizationUserDetails.FirstOrDefault(u => u.UserId == submissionDetailModel.OrganizationUserId);

                        /* Populate db model */
                        model.JobId = submissionDetailModel.JobId;
                        model.OrganizationUserId = organizationUser != null ? organizationUser.Id : Guid.Empty;
                        model.CandidateId = candidate.Id;
                        model.CandidateName = CandidateUserModel.GetCandidateName(candidate.Id);
                        successCandidateList.Add(candidate.Id);
                        model.AssignedDate = DateTime.UtcNow;
                        model.ClientRate = submissionDetailModel.ClientRate;
                        model.Margin = submissionDetailModel.Margin;
                        model.StatusId = db.SubmissionStatusMasters.FirstOrDefault(s => s.Status == "Submitted internally").Id; // set default status          
                        model.Status = "Submitted internally";
                        db.SubmissionDetails.Add(model);
                    }
                }

                db.SaveChanges();
            }



            if (count > 0)
            {
                /* send response exception */
                if (count == 1)
                {
                    message = "Candidate " + candidateNames.ToString().TrimEnd(',') + " is already submitted for this requirement, So no need to submit again.";
                }
                else
                {
                    message = "Candidates " + candidateNames.ToString().TrimEnd(',') + " are already submitted for this requirement, So no need to submit again.";
                }
            }
        }

        /// <summary>
        /// Populate Submission Model
        /// </summary>
        /// <param name="submission">Submission </param>
        /// <returns>Submission Model</returns>
        internal static SubmissionModel PopulateSubmissionModel(SubmissionDetail submission)
        {
            SubmissionModel model = new SubmissionModel();

            /* Populate model */
            model.Id = submission.Id;

            /* Company Info */
            model.CompanyId = submission.JobDetail != null ? submission.JobDetail.CompanyId : Guid.Empty;
            model.CompanyName = submission.JobDetail != null ? (submission.JobDetail.CompanyDetail != null ? submission.JobDetail.CompanyDetail.Name : string.Empty) : string.Empty;

            /* Job Info */
            model.JobId = submission.JobId;
            model.JobTitle = submission.JobDetail != null ? submission.JobDetail.JobTitle : string.Empty;
            model.JobType = submission.JobDetail != null ? (submission.JobDetail.JobTypeMaster != null ? submission.JobDetail.JobTypeMaster.Type : string.Empty) : string.Empty;
            model.JobCode = submission.JobDetail != null ? submission.JobDetail.ClientJobCode : string.Empty;

            /* Recruiter Info */
            model.OrganizationUserId = submission.OrganizationUserId;
            model.OrganizationUserName = submission.OrganizationUserDetail != null ? (submission.OrganizationUserDetail.FirstName + " " + submission.OrganizationUserDetail.LastName) : string.Empty;

            /* Candidate Info */
            model.CandidateList = new List<MongoLookUpModel>();
            model.CandidateList.Add(new MongoLookUpModel() { Id = submission.CandidateId, Name = submission.CandidateName });

            /* Submission Info */
            model.AssignedDate = submission.AssignedDate;
            model.StatusId = submission.StatusId;
            model.Status = submission.Status;
            model.ClientRate = submission.ClientRate;
            model.Margin = submission.Margin;

            /* Get Submission Feedback List  */
            model.SubmissionStatusList = SubmissionFeedback.GetSubmissionFeedbackList(model.Id);

            /* Get Client Feedback List  */
            model.ClientFeedbackList = ClientFeedback.GetClientFeedbackList(model.Id);

            /* Get Candidate Feedback List  */
            model.CandidateFeedbackList = CandidateFeedback.GetCandidateFeedbackList(model.Id);

            return model;
        }

        /// <summary>
        /// Get Associate Candidates
        /// </summary>
        /// <param name="jobId">JobId</param>
        /// <returns>List of Associate Candidates</returns>
        internal static List<MongoLookUpModel> GetAssociateCandidates(Guid jobId)
        {
            List<MongoLookUpModel> associateCandidates = new List<MongoLookUpModel>();
            using (var db = new XSeedEntities())
            {
                var submissions = db.SubmissionDetails.Where(s => s.JobId == jobId);
                if (submissions != null)
                {
                    foreach (var submission in submissions)
                    {
                        MongoLookUpModel submissionModel = new MongoLookUpModel();
                        var candidate = CandidateUserModel.GetCandidateUserDetail(submission.CandidateId);
                        submissionModel.Id = submission.CandidateId;
                        submissionModel.Name = candidate != null ? ((candidate.FirstName ?? "") + " " + (candidate.LastName ?? "")) : "";
                        associateCandidates.Add(submissionModel);
                    }
                }
            }
            return associateCandidates.ToList();
        }


        /// <summary>
        /// Get Associate Candidates
        /// </summary>
        /// <param name="jobId">JobId</param>
        /// <returns>List of Associate Candidates</returns>
        internal static List<MongoLookUpModel> GetAppliedCandidates(Guid jobId)
        {
            List<MongoLookUpModel> associateCandidates = new List<MongoLookUpModel>();
            using (var db = new XSeedEntities())
            {
                var submissions = db.SubmissionDetails.Where(s => s.JobId == jobId);
                if (submissions != null)
                {
                    foreach (var submission in submissions)
                    {
                        MongoLookUpModel submissionModel = new MongoLookUpModel();
                        var candidate = CandidateUserModel.GetCandidateUserDetail(submission.CandidateId);
                        submissionModel.Id = submission.CandidateId;
                        submissionModel.Name = candidate != null ? ((candidate.FirstName ?? "") + " " + (candidate.LastName ?? "")) : "";
                        associateCandidates.Add(submissionModel);
                    }
                }
            }
            return associateCandidates.ToList();
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        internal static PaginationModel GetPaginationInfo(Guid organizationId, int pageSize)
        {
            PaginationModel model = new PaginationModel();

            using (var db = new XSeedEntities())
            {
                model.TotalCount = db.SubmissionDetails.Where(o => o.JobDetail.OrganizationId == organizationId).Count();
                model.TotalPages = Math.Ceiling((double)model.TotalCount / pageSize);
            }

            return model;
        }

        #endregion
    }
}
