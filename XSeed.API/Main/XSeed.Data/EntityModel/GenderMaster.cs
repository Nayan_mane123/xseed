﻿
namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [MetadataType(typeof(GenderMasterMD))]
    public partial class GenderMaster:IAuditable
    {
        public class GenderMasterMD
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public Nullable<bool> IsActive { get; set; }
        }
    }
}
