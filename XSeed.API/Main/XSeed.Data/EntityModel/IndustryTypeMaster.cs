﻿namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using XSeed.Data.ViewModel.Common;

    [MetadataType(typeof(IndustryTypeMasterMD))]
    public partial class IndustryTypeMaster : IAuditable
    {
        #region Property Declaration

        public class IndustryTypeMasterMD
        {
            public int Id { get; set; }
            public Nullable<int> ParentIndustryId { get; set; }
            public string Type { get; set; }
            public Nullable<bool> IsActive { get; set; }
        }

        #endregion

        #region User Defined Functions

        #region Organization Detail

        /// <summary>
        /// Get Industry Types belongs to organization
        /// </summary>
        /// <param name="organizationDetailId">Organization Detail Id</param>
        /// <returns>Industry Types</returns>
        internal static List<ChildLookUpModel> GetOrganizationIndustryTypes(Guid organizationDetailId)
        {
            List<ChildLookUpModel> IndustryTypes = new List<ChildLookUpModel>();

            using (var db = new XSeedEntities())
            {
                OrganizationDetail model = db.OrganizationDetails.Find(organizationDetailId);

                if (model != null)
                {
                    foreach (var item in model.IndustryTypeMasters)
                    {
                        ChildLookUpModel industryTypeModel = new ChildLookUpModel();
                        industryTypeModel.Id = item.Id;
                        industryTypeModel.Name = item.Type;
                        industryTypeModel.ParentId = item.ParentIndustryId;
                        industryTypeModel.ParentName = industryTypeModel.ParentId != null ? db.IndustryTypeMasters.Find(industryTypeModel.ParentId).Type : string.Empty;

                        IndustryTypes.Add(industryTypeModel);
                    }
                }
            }
            return IndustryTypes;
        }

        /// <summary>
        /// Save Indusrty Types related to organization
        /// </summary>
        /// <param name="organization">Organization Detail</param>
        /// <param name="industryTypes">Industry Types</param>
        internal static void AddOrganizationIndustries(Guid organizationId, List<ChildLookUpModel> industryTypes)
        {
            if (industryTypes != null)
            {
                using (var db = new XSeedEntities())
                {
                    var organization = db.OrganizationDetails.Find(organizationId);

                    if (organization != null)
                    {
                        /* Remove existing industries */
                        organization.IndustryTypeMasters.Clear();

                        foreach (var industry in industryTypes)
                        {
                            IndustryTypeMaster industryType = db.IndustryTypeMasters.Find(industry.Id);
                            organization.IndustryTypeMasters.Add(industryType); // Add Industries
                        }

                        db.SaveChanges();
                    }
                }
            }
        }

        #endregion

        #region Company Detail

        /// <summary>
        /// Get Industry Types belongs to company
        /// </summary>
        /// <param name="companyDetailId">Company Detail Id</param>
        /// <returns>Industry Types</returns>
        internal static List<ChildLookUpModel> GetCompanyIndustryTypes(Guid companyDetailId)
        {
            List<ChildLookUpModel> IndustryTypes = new List<ChildLookUpModel>();

            using (var db = new XSeedEntities())
            {
                CompanyDetail model = db.CompanyDetails.Find(companyDetailId);

                if (model != null)
                {
                    foreach (var item in model.IndustryTypeMasters)
                    {
                        ChildLookUpModel industryTypeModel = new ChildLookUpModel();
                        industryTypeModel.Id = item.Id;
                        industryTypeModel.Name = item.Type;
                        industryTypeModel.ParentId = item.ParentIndustryId;
                        industryTypeModel.ParentName = industryTypeModel.ParentId != null ? db.IndustryTypeMasters.Find(industryTypeModel.ParentId).Type : string.Empty;

                        IndustryTypes.Add(industryTypeModel);
                    }
                }
            }
            return IndustryTypes;
        }

        /// <summary>
        /// Save Indusrty Types related to Company Detail
        /// </summary>
        /// <param name="organization">Company Detail</param>
        /// <param name="industryTypes">Industry Types</param>
        internal static void SaveCompanyIndustryTypes(Guid companyDetailId, List<ChildLookUpModel> industryTypes)
        {
            if (industryTypes != null)
            {
                using (var db = new XSeedEntities())
                {
                    var companyDetail = db.CompanyDetails.Find(companyDetailId);

                    if (companyDetail != null)
                    {
                        /* Remove existing industries */
                        companyDetail.IndustryTypeMasters.Clear();

                        foreach (var industry in industryTypes)
                        {
                            IndustryTypeMaster industryType = db.IndustryTypeMasters.Find(industry.Id);
                            companyDetail.IndustryTypeMasters.Add(industryType); // Add Industries
                        }

                        db.SaveChanges();
                    }
                }
            }
        }

        #endregion

        #endregion
    }
}
