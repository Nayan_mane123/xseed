﻿namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [MetadataType(typeof(SubscriptionPlanMasterMD))]
    public partial class SubscriptionPlanMaster
    {
        public class SubscriptionPlanMasterMD
        {
            public int Id { get; set; }
            public string PlanName { get; set; }
            public int PlanPrice { get; set; }
            public string Description { get; set; }
        }
    }
}
