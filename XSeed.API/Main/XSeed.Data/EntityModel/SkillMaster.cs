﻿
namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using XSeed.Data.ViewModel.Common;

    [MetadataType(typeof(SkillMasterMD))]
    public partial class SkillMaster : IAuditable
    {
        #region Property Declaration

        public class SkillMasterMD
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public Nullable<bool> IsActive { get; set; }
        }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Save Job Skills
        /// </summary>
        /// <param name="list">list of skills</param>
        //internal static void SaveJobSkillInfo(Guid jobId, List<LookUpModel> skillList)
        //{
        //    using (var db = new XSeedEntities())
        //    {
        //        if (skillList != null)
        //        {
        //            JobDetail jobDetail = db.JobDetails.Find(jobId);

        //            //Remove Existing Skills
        //            jobDetail.SkillMasters.Clear();

        //            if (jobDetail != null)
        //            {
        //                foreach (var skill in skillList)
        //                {
        //                    SkillMaster model = db.SkillMasters.Find(skill.Id);
        //                    jobDetail.SkillMasters.Add(model);
        //                }

        //                db.SaveChanges();
        //            }
        //        }
        //    }
        //}

        /// <summary>
        /// Get Skills Associated with Perticular Job
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <returns>List Of Skills Associated with Perticular Job</returns>
        //internal static List<LookUpModel> GetJobSkillInfo(Guid jobId)
        //{
        //    List<LookUpModel> skillList = new List<LookUpModel>();

        //    using (var db = new XSeedEntities())
        //    {
        //        JobDetail model = db.JobDetails.Where(j => j.Id == jobId).FirstOrDefault();

        //        if (model != null)
        //        {
        //            foreach (var skill in model.SkillMasters)
        //            {
        //                LookUpModel skillModel = new LookUpModel();
        //                skillModel.Id = skill.Id;
        //                skillModel.Name = skill.Name;

        //                skillList.Add(skillModel);
        //            }
        //        }
        //    }
        //    return skillList;
        //}

        /// <summary>
        /// Get list of Skills of a Candidate 
        /// </summary>
        /// <param name="candidateId">Candidate Id</param>
        /// <returns>LookUp Model List</returns>
        internal static List<LookUpModel> GetCandidateSkills(Guid candidateId)
        {
            List<LookUpModel> list = new List<LookUpModel>();

            if (candidateId != null)
            {
                using (var db = new XSeedEntities())
                {
                    CandidateDetail candidate = db.CandidateDetails.Find(candidateId);

                    if (candidate.SkillMasters != null)
                    {
                        foreach (var skill in candidate.SkillMasters)
                        {
                            LookUpModel model = new LookUpModel();
                            model.Id = skill.Id;
                            model.Name = skill.Name;

                            list.Add(model);
                        }
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Save Candidate Skills
        /// </summary>
        /// <param name="list">list of skills</param>
        internal static void SaveCandidateSkills(Guid candidateId, List<LookUpModel> list)
        {
            if (list != null)
            {
                using (var db = new XSeedEntities())
                {
                    CandidateDetail model = db.CandidateDetails.Find(candidateId);

                    if (model != null)
                    {
                        /* Remove existing skills */
                        model.SkillMasters.Clear();

                        foreach (var skill in list)
                        {
                            SkillMaster skillmaster = db.SkillMasters.Find(skill.Id);
                            model.SkillMasters.Add(skillmaster);
                        }

                        db.SaveChanges();
                    }
                }
            }
        }

        #endregion

    }
}


