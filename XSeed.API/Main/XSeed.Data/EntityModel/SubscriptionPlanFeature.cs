﻿namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [MetadataType(typeof(SubscriptionPlanFeatureMD))]
    public partial class SubscriptionPlanFeature
    {
        public class SubscriptionPlanFeatureMD
        {
            public System.Guid Id { get; set; }
            public System.Guid SubscriptionPlanId { get; set; }
            public System.Guid FeatureId { get; set; }
            public Nullable<short> IsActive { get; set; }
            public Nullable<int> Limit { get; set; }
        }
    }
}
