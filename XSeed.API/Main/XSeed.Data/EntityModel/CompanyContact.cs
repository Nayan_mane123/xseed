﻿
namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using XSeed.Data.ViewModel.Company;
    using XSeed.Utility;

    [MetadataType(typeof(CompanyContactMD))]
    public partial class CompanyContact : IAuditable
    {
        #region Property Declaration

        public class CompanyContactMD
        {
            public int Id { get; set; }
            public int CompanyId { get; set; }
            public Nullable<int> TitleId { get; set; }
            public string FirstName { get; set; }
            public string MiddleName { get; set; }
            public string LastName { get; set; }
            public Nullable<System.DateTime> BirthDate { get; set; }
            public Nullable<System.DateTime> AnniversaryDate { get; set; }
            public string ProfileImagePath { get; set; }
            public Nullable<int> ReportingTo { get; set; }
            public string Designation { get; set; }
            public string PracticeLine { get; set; }
            public string PrimaryEmail { get; set; }
            public string SecondaryEmail { get; set; }
            public string Mobile { get; set; }
            public string Phone { get; set; }
            public string Fax { get; set; }
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string Address3 { get; set; }
            public Nullable<int> CountryId { get; set; }
            public Nullable<int> StateId { get; set; }
            public Nullable<int> CityId { get; set; }
            public string Zip { get; set; }
            public Nullable<bool> IsActive { get; set; }
        }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get list of contacts belongs to company
        /// </summary>
        /// <param name="companyId">Company Id</param>
        /// <returns>Company Contact Model List</returns>
        internal static List<CompanyContactModel> ListCompanyContacts(Guid companyId)
        {
            List<CompanyContactModel> list = new List<CompanyContactModel>();

            using (var db = new XSeedEntities())
            {
                var contacts = db.CompanyContacts.Where(c => c.CompanyId == companyId && c.IsActive == Constants.IsActiveTrue).OrderByDescending(d => d.CreatedOn);

                foreach (var contact in contacts)
                {
                    CompanyContactModel model = populateContactModel(contact);
                    list.Add(model);
                }

                return list;
            }
        }

        /// <summary>
        /// Get particular contact
        /// </summary>
        /// <param name="companyId">Company Id</param>
        /// <param name="contactId">Contact Id</param>
        /// <returns>Company Contact Model</returns>
        internal static CompanyContactModel GetCompanyContact(Guid? Id, Guid companyId)
        {
            CompanyContactModel model = new CompanyContactModel();

            using (var db = new XSeedEntities())
            {
                var contact = db.CompanyContacts.FirstOrDefault(c => c.Id == Id && c.CompanyId == companyId);

                if (contact != null)
                {
                    model = populateContactModel(contact);
                }
            }

            return model;
        }

        /// <summary>
        /// Populate Contact detail model from contact
        /// </summary>
        /// <param name="contact">Company Contact</param>
        /// <returns>Company Contact Model</returns>
        private static CompanyContactModel populateContactModel(CompanyContact contact)
        {
            CompanyContactModel model = new CompanyContactModel();

            /* Reference */
            model.Id = contact.Id;
            model.CompanyId = contact.CompanyId;
            model.CompanyName = contact.CompanyDetail != null ? contact.CompanyDetail.Name : string.Empty;

            /* Personal Info */
            model.TitleId = contact.TitleId;
            model.Title = contact.TitleMaster != null ? contact.TitleMaster.Name : string.Empty;
            model.FirstName = contact.FirstName;
            model.MiddleName = contact.MiddleName;
            model.LastName = contact.LastName;
            model.BirthDate = contact.BirthDate;
            model.AnniversaryDate = contact.AnniversaryDate;
            model.ProfileImage = contact.ProfileImagePath;

            /* Professional Info */
            model.ReportingTo = contact.ReportingTo;
            model.Designation = contact.Designation;
            model.PracticeLine = contact.PracticeLine;

            /* Contact Info */
            model.PrimaryEmail = contact.PrimaryEmail;
            model.SecondaryEmail = contact.SecondaryEmail;
            model.Mobile = contact.Mobile;
            model.Phone = contact.Phone;
            model.Fax = contact.Fax;

            /* Address Info */
            model.Address1 = contact.Address1;
            model.Address2 = contact.Address2;
            model.Address3 = contact.Address3;
            model.CountryId = contact.CountryId;
            model.Country = contact.CountryMaster != null ? contact.CountryMaster.Name : string.Empty;
            model.StateId = contact.StateId;
            model.State = contact.StateMaster != null ? contact.StateMaster.Name : string.Empty;
            model.CityId = contact.CityId;
            model.City = contact.CityMaster != null ? contact.CityMaster.Name : string.Empty;
            model.Zip = contact.Zip;

            return model;
        }

        /// <summary>
        /// Create New Company Contact
        /// </summary>
        /// <param name="companyContactModel">Company Contact Model </param>
        internal static Guid CreateCompanyContact(CompanyContactModel companyContactModel)
        {
            return SaveCompanyContact(companyContactModel);
        }

        /// <summary>
        /// Update Company Contact
        /// </summary>
        /// <param name="companyContactModel">Company Contact Model </param>
        internal static void UpdateCompanyContact(CompanyContactModel companyContactModel)
        {
            SaveCompanyContact(companyContactModel);
        }

        /// <summary>
        /// Save Company Contact
        /// </summary>
        /// <param name="model">Company Contact Model</param>
        private static Guid SaveCompanyContact(CompanyContactModel model)
        {
            Guid companyContactId = Guid.Empty;

            using (var db = new XSeedEntities())
            {
                CompanyContact companyContact = model.Id != Guid.Empty ? db.CompanyContacts.Find(model.Id) : new CompanyContact();

                /* Reference */
                companyContact.Id = model.Id;
                companyContact.CompanyId = (Guid)(model.CompanyId);

                /* Personal Info */
                companyContact.TitleId = model.TitleId;
                companyContact.FirstName = model.FirstName;
                companyContact.MiddleName = model.MiddleName;
                companyContact.LastName = model.LastName;
                companyContact.BirthDate = model.BirthDate;
                companyContact.AnniversaryDate = model.AnniversaryDate;
                companyContact.ProfileImagePath = string.IsNullOrEmpty(model.ProfileImage) ? companyContact.ProfileImagePath : model.ProfileImage;

                /* Professional Info */
                companyContact.ReportingTo = model.ReportingTo;
                companyContact.Designation = model.Designation;
                companyContact.PracticeLine = model.PracticeLine;

                /* model Info */
                companyContact.PrimaryEmail = model.PrimaryEmail;
                companyContact.SecondaryEmail = model.SecondaryEmail;
                companyContact.Mobile = model.Mobile;
                companyContact.Phone = model.Phone;
                companyContact.Fax = model.Fax;

                /* Address Info */
                companyContact.Address1 = model.Address1;
                companyContact.Address2 = model.Address2;
                companyContact.Address3 = model.Address3;
                companyContact.CountryId = model.CountryId;
                companyContact.StateId = model.StateId;
                companyContact.CityId = model.CityId;
                companyContact.Zip = model.Zip;

                /* Is Active Flag */
                companyContact.IsActive = Constants.IsActiveTrue;

                if (model.Id == Guid.Empty)
                {
                    db.CompanyContacts.Add(companyContact);
                }

                db.SaveChanges();

                companyContactId = companyContact.Id;
            }

            return companyContactId;
        }

        /// <summary>
        /// Get company contact name by id
        /// </summary>
        /// <param name="guid">Id</param>
        /// <returns>Company Contact Name</returns>
        internal static string GetCompanyContactName(Guid Id)
        {
            string name = string.Empty;

            if (Id != null && Id != Guid.Empty)
            {
                using (var db = new XSeedEntities())
                {
                    var contact = db.CompanyContacts.Find(Id);
                    name = contact != null ? (contact.FirstName + " " + contact.LastName) : string.Empty;
                }
            }

            return name;
        }

        /// <summary>
        /// Get Company contact emails by company
        /// </summary>
        /// <param name="companyId">Company Id</param>
        /// <returns>Email Addresses</returns>
        internal static List<string> GetContactEmailByCompany(Guid? companyId)
        {
            List<string> list = null;

            if (companyId != null && companyId != Guid.Empty)
            {
                list = new List<string>();

                using (var db = new XSeedEntities())
                {
                    var companyDetail = db.CompanyDetails.Find(companyId);

                    if (companyDetail != null)
                    {
                        var contacts = companyDetail.CompanyContacts;

                        if (contacts != null)
                        {
                            list = contacts.Select(c => c.PrimaryEmail).ToList();
                        }
                    }
                }
            }

            return list;
        }

        #endregion
    }
}
