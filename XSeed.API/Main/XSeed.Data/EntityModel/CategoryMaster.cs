﻿namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [MetadataType(typeof(CategoryMasterMD))]
    public partial class CategoryMaster : IAuditable
    {
        public class CategoryMasterMD
        {
            public System.Guid Id { get; set; }
            public string Category { get; set; }
            public string Description { get; set; }
            public Nullable<bool> IsActive { get; set; }            
        }
    }
}
