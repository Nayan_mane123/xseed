﻿
namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using XSeed.Data.ViewModel.Common;

    [MetadataType(typeof(VisaTypeMasterMD))]
    public partial class VisaTypeMaster : IAuditable
    {
        #region Property Declaration

        public class VisaTypeMasterMD
        {
            public int Id { get; set; }
            public string Type { get; set; }
            public Nullable<bool> IsActive { get; set; }
        }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="visaTypeList"></param>
        /// <param name="isCreate"></param>
        internal static void SaveJobVisaTypeInfo(Guid jobId, List<LookUpModel> visaTypeList, bool isCreate = false)
        {
            string oldVisaTypes = string.Empty, newVisaTypes = string.Empty;
            StringBuilder visaTypes = new StringBuilder();

            ICollection<VisaTypeMaster> visaTypeMasters = null;

            using (var db = new XSeedEntities())
            {
                if (visaTypeList != null)
                {
                    JobDetail jobDetail = db.JobDetails.Find(jobId);

                    if (jobDetail != null)
                    {

                        if (!isCreate)
                        {
                            visaTypeMasters = jobDetail.VisaTypeMasters;

                            /* set old recruiters */
                            oldVisaTypes = string.Join(",", visaTypeMasters.Select(u => u.Type)).TrimEnd(',');

                            /* clear old values */
                            jobDetail.VisaTypeMasters.Clear();
                        }

                        foreach (var visaType in visaTypeList)
                        {
                            VisaTypeMaster model = db.VisaTypeMasters.Find(visaType.Id);
                            jobDetail.VisaTypeMasters.Add(model);

                            visaTypes.Append("," + (model.Type));
                        }

                        db.SaveChanges();

                        /* process string */
                        newVisaTypes = visaTypes.ToString().TrimStart(',');

                        if (string.Compare(oldVisaTypes, newVisaTypes) != 0)
                        {
                            if (!isCreate || !BaseModel.IsActivityUpdate)
                            {
                                /* Update Audit Trail */
                                JobDetail.UpdateAuditTrail(jobId);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jobId"></param>
        /// <returns></returns>
        internal static List<LookUpModel> GetVisaTypeInfo(Guid jobId)
        {
            List<LookUpModel> visaTypeList = new List<LookUpModel>();

            using (var db = new XSeedEntities())
            {
                JobDetail model = db.JobDetails.Find(jobId);

                if (model != null)
                {
                    foreach (var VisaType in model.VisaTypeMasters)
                    {
                        LookUpModel visaTypeModel = new LookUpModel();
                        visaTypeModel.Id = VisaType.Id;
                        visaTypeModel.Name = VisaType.Type;
                        visaTypeList.Add(visaTypeModel);
                    }
                }
            }
            return visaTypeList;
        }

        //internal static List<LookUpModel> GetCandidateDegrees(Guid candidateId)
        //{
        //    List<LookUpModel> list = new List<LookUpModel>();

        //    if (candidateId != null)
        //    {
        //        using (var db = new XSeedEntities())
        //        {
        //            CandidateDetail candidate = db.CandidateDetails.Find(candidateId);

        //            if (candidate.DegreeMasters != null)
        //            {
        //                foreach (var degree in candidate.DegreeMasters)
        //                {
        //                    LookUpModel model = new LookUpModel();
        //                    model.Id = degree.Id;
        //                    model.Name = degree.Name;
        //                    list.Add(model);
        //                }
        //            }
        //        }
        //    }

        //    return list;
        //}

        //internal static void SaveCandidateDegress(Guid candidateId, List<LookUpModel> list)
        //{
        //    if (candidateId != null && list != null)
        //    {
        //        using (var db = new XSeedEntities())
        //        {
        //            CandidateDetail model = db.CandidateDetails.Find(candidateId);

        //            if (model != null)
        //            {
        //                model.DegreeMasters.Clear();

        //                foreach (var degree in list)
        //                {
        //                    DegreeMaster degreeMaster = db.DegreeMasters.Find(degree.Id);
        //                    model.DegreeMasters.Add(degreeMaster);
        //                }

        //                db.SaveChanges();
        //            }
        //        }
        //    }
        //}

        #endregion

    }
}
