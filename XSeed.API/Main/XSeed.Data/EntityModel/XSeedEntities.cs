﻿namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using XSeed.Data.ViewModel.Activity;
    using XSeed.Data.ViewModel.Common;

    public interface IAuditable
    {
        Nullable<Guid> CreatedBy { get; set; }
        Nullable<System.DateTime> CreatedOn { get; set; }
        Nullable<Guid> ModifiedBy { get; set; }
        Nullable<System.DateTime> ModifiedOn { get; set; }
    }

    public partial class XSeedEntities : DbContext
    {
        public override int SaveChanges()
        {
            var changeSet = ChangeTracker.Entries<IAuditable>();

            if (changeSet != null)
            {
                // Get User ID from session.
                Guid UserID = BaseModel.InitiatedBy;
                string entityName = string.Empty;
                //  DateTime? CreatedOn = BaseModel.InitiatedOn;


                foreach (var entry in changeSet.Where(c => c.State != EntityState.Unchanged))
                {
                    switch (entry.State)
                    {
                        case EntityState.Added:
                            entry.Entity.CreatedBy = UserID;
                            //  entry.Entity.CreatedOn = CreatedOn.HasValue ? CreatedOn : DateTime.UtcNow;
                            entry.Entity.CreatedOn = DateTime.UtcNow;
                            entityName = entry.Entity.GetType().Name;
                            break;

                        case EntityState.Modified:
                            entry.Entity.ModifiedBy = UserID;
                            //  entry.Entity.ModifiedOn = CreatedOn.HasValue ? CreatedOn : DateTime.UtcNow;
                            entry.Entity.ModifiedOn = DateTime.UtcNow;
                            entityName = entry.Entity.GetType().BaseType.Name;
                            break;

                        case EntityState.Deleted:
                            entry.Entity.ModifiedBy = UserID;
                            //  entry.Entity.ModifiedOn = CreatedOn.HasValue ? CreatedOn : DateTime.UtcNow;
                            entry.Entity.ModifiedOn = DateTime.UtcNow;
                            entityName = entry.Entity.GetType().BaseType.Name;
                            break;

                        default:
                            // Dont' update anything
                            break;
                    }

                    if (entityName == "JobDetail")
                    {
                        BaseModel.ActivityId = string.Empty;
                        BaseModel.IsActivityUpdate = false;
                        BaseModel.ActivityId = JobActivityLogModel.TrackActivityLog(entry, entityName);
                        BaseModel.IsActivityUpdate = !string.IsNullOrEmpty(BaseModel.ActivityId) ? true : false;
                    }
                }
            }
            return base.SaveChanges();
        }

    }
}
