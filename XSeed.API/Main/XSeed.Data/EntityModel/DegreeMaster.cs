﻿
namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using XSeed.Data.ViewModel.Common;

    [MetadataType(typeof(DegreeMasterMD))]
    public partial class DegreeMaster : IAuditable
    {
        #region Property Declaration

        public class DegreeMasterMD
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public Nullable<bool> IsActive { get; set; }
            public Guid OrganizationId { get; set; }
        }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Save list of Degrees of a Job 
        /// </summary>
        /// <param name="candidateId">Job Id</param>
        internal static void SaveJobDegreeInfo(Guid jobId, List<LookUpModel> degreeList, bool isCreate = false)
        {
            string oldDegrees = string.Empty, newDegrees = string.Empty;
            StringBuilder degrees = new StringBuilder();

            ICollection<DegreeMaster> degreeMasters = null;

            using (var db = new XSeedEntities())
            {
                if (degreeList != null)
                {
                    JobDetail jobDetail = db.JobDetails.Find(jobId);

                    if (jobDetail != null)
                    {

                        if (!isCreate)
                        {
                            degreeMasters = jobDetail.DegreeMasters;

                            /* set old recruiters */
                            oldDegrees = string.Join(",", degreeMasters.Select(u => u.Name)).TrimEnd(',');

                            /* clear old values */
                            jobDetail.DegreeMasters.Clear();
                        }

                        foreach (var degree in degreeList)
                        {
                            DegreeMaster model = db.DegreeMasters.Find(degree.Id);
                            jobDetail.DegreeMasters.Add(model);

                            degrees.Append("," + (model.Name));
                        }

                        db.SaveChanges();

                        /* process string */
                        newDegrees = degrees.ToString().TrimStart(',');

                        if (string.Compare(oldDegrees, newDegrees) != 0)
                        {
                            if (!isCreate || !BaseModel.IsActivityUpdate)
                            {
                                /* Update Audit Trail */
                                JobDetail.UpdateAuditTrail(jobId);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Get list of Degrees of a Job 
        /// </summary>
        /// <param name="candidateId">Job Id</param>
        /// <returns>LookUp Model List</returns>
        internal static List<LookUpModel> GetJobDegreeInfo(Guid jobId)
        {
            List<LookUpModel> degreeList = new List<LookUpModel>();

            using (var db = new XSeedEntities())
            {
                JobDetail model = db.JobDetails.Find(jobId);

                if (model != null)
                {
                    foreach (var degree in model.DegreeMasters)
                    {
                        LookUpModel degreeModel = new LookUpModel();
                        degreeModel.Id = degree.Id;
                        degreeModel.Name = degree.Name;
                        degreeList.Add(degreeModel);
                    }
                }
            }
            return degreeList;
        }

        /// <summary>
        /// Get list of Degrees of a Candidate 
        /// </summary>
        /// <param name="candidateId">Candidate Id</param>
        /// <returns>LookUp Model List</returns>
        internal static List<LookUpModel> GetCandidateDegrees(Guid candidateId)
        {
            List<LookUpModel> list = new List<LookUpModel>();

            if (candidateId != null)
            {
                using (var db = new XSeedEntities())
                {
                    CandidateDetail candidate = db.CandidateDetails.Find(candidateId);

                    if (candidate.DegreeMasters != null)
                    {
                        foreach (var degree in candidate.DegreeMasters)
                        {
                            LookUpModel model = new LookUpModel();
                            model.Id = degree.Id;
                            model.Name = degree.Name;
                            list.Add(model);
                        }
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Save Candidate degrees
        /// </summary>
        /// <param name="list">list of degrees</param>
        internal static void SaveCandidateDegress(Guid candidateId, List<LookUpModel> list)
        {
            if (candidateId != null && list != null)
            {
                using (var db = new XSeedEntities())
                {
                    CandidateDetail model = db.CandidateDetails.Find(candidateId);

                    if (model != null)
                    {
                        model.DegreeMasters.Clear();

                        foreach (var degree in list)
                        {
                            DegreeMaster degreeMaster = db.DegreeMasters.Find(degree.Id);
                            model.DegreeMasters.Add(degreeMaster);
                        }

                        db.SaveChanges();
                    }
                }
            }
        }

        #endregion

    }
}
