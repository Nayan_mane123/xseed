﻿
namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [MetadataType(typeof(MaritalStatusMasterMD))]
    public partial class MaritalStatusMaster:IAuditable
    {
        public class MaritalStatusMasterMD
        {
            public int Id { get; set; }
            public string Status { get; set; }
            public Nullable<bool> IsActive { get; set; }
        }
    }
}
