﻿
namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using XSeed.Data.ViewModel.Common;
    using XSeed.Data.ViewModel.User.UserCommon;
    using XSeed.Utility;

    [MetadataType(typeof(UserRoleMasterMD))]
    public partial class UserRoleMaster : IAuditable
    {
        #region Property Declaration

        public class UserRoleMasterMD
        {
            public int Id { get; set; }
            public string Role { get; set; }
            public string Description { get; set; }
            public Nullable<bool> IsActive { get; set; }
        }

        #endregion

        #region User Defined Functions

       

       


        /// <summary>
        /// Get all roles list
        /// </summary>        
        /// <returns>User role list</returns>
        internal static List<UserRoleModel> GetAllUserRoles(Guid organizationId)
        {
            List<UserRoleModel> list = new List<UserRoleModel>();

            using (var db = new XSeedEntities())
            {
                var userRolemasters = db.UserRoleMasters.Where(x => x.OrganizationId == organizationId).OrderBy(x => x.CreatedOn).ToList();

                foreach (var userRole in userRolemasters)
                {
                    UserRoleModel model = new UserRoleModel();

                    model.Id = userRole.Id;
                    model.Role = userRole.Role;
                    model.Precedence = userRole.Precedence;
                    model.Description = userRole.Description;

                    model.AreaMapping = new List<AreaPermissionModel>();

                    foreach (var userRoleArea in userRole.UserRoleAreaMappings.OrderBy(x => x.AreaMaster.CreatedOn))
                    {
                        AreaPermissionModel areaModel = new AreaPermissionModel();

                        areaModel.Id = userRoleArea.AreaMaster.Id;
                        areaModel.Name = userRoleArea.AreaMaster.Name;

                        var AreaPermissionMappings = db.AreaPermissionMappings.Where(x => x.UserRoleAreaMappingId == userRoleArea.Id).FirstOrDefault();
                        if (AreaPermissionMappings != null)
                        {
                            areaModel.Create = AreaPermissionMappings.CreatePermission;
                            areaModel.Update = AreaPermissionMappings.UpdatePermission;
                            areaModel.Read = AreaPermissionMappings.ReadPermission;
                            areaModel.Delete = AreaPermissionMappings.DeletePermission;
                        }

                        model.AreaMapping.Add(areaModel);
                    }

                    list.Add(model);
                }
            }

            return list;
        }

        /// <summary>
        /// Get user role detail by Id
        /// </summary>
        /// <param name="Id">Role Id</param>
        /// <returns>user role detail </returns>
        internal static UserRoleModel GetUserRoleById(Guid? Id)
        {
            UserRoleModel model = new UserRoleModel();

            if (Id != null)
            {
                using (var db = new XSeedEntities())
                {
                    var userRolemaster = db.UserRoleMasters.Find(Id);

                    if (userRolemaster != null)
                    {
                        model.Id = userRolemaster.Id;
                        model.Role = userRolemaster.Role;
                        model.Precedence = userRolemaster.Precedence;
                        model.Description = userRolemaster.Description;

                        model.AreaMapping = new List<AreaPermissionModel>();

                        foreach (var userRoleArea in userRolemaster.UserRoleAreaMappings)
                        {
                            AreaPermissionModel areaModel = new AreaPermissionModel();

                            areaModel.Id = userRoleArea.AreaMaster.Id;
                            areaModel.Name = userRoleArea.AreaMaster.Name;

                            var AreaPermissionMappings = db.AreaPermissionMappings.Where(x => x.UserRoleAreaMappingId == userRoleArea.Id).FirstOrDefault();
                            if (AreaPermissionMappings != null)
                            {
                                areaModel.Create = AreaPermissionMappings.CreatePermission;
                                areaModel.Update = AreaPermissionMappings.UpdatePermission;
                                areaModel.Read = AreaPermissionMappings.ReadPermission;
                                areaModel.Delete = AreaPermissionMappings.DeletePermission;
                            }

                            model.AreaMapping.Add(areaModel);
                        }
                    }
                }
            }

            return model;
        }

        /// <summary>
        /// List of Organiztion Areas
        /// </summary>        
        /// <returns>List of Organiztion Areas</returns>
        internal static List<LookUpModel> GetOrganizationAreas()
        {
            List<LookUpModel> modelList = new List<LookUpModel>();

            using (var db = new XSeedEntities())
            {
                var orgnizationAreaList = db.AreaMasters.ToList();

                foreach (var area in orgnizationAreaList)
                {
                    var model = new LookUpModel();

                    model.Id = area.Id;
                    model.Name = area.Name;
                    modelList.Add(model);
                }
            }

            return modelList;
        }

        /// <summary>
        /// List of User Types
        /// </summary>        
        /// <returns>List of User Types</returns>
        internal static List<LookUpModel> GetUserTypes()
        {
            List<LookUpModel> modelList = new List<LookUpModel>();

            using (var db = new XSeedEntities())
            {
                var userTypeList = db.UserTypeMasters.ToList();

                foreach (var userType in userTypeList)
                {
                    var model = new LookUpModel();

                    model.Id = userType.Id;
                    model.Name = userType.Type;
                    modelList.Add(model);
                }
            }

            return modelList;
        }

        /// <summary>
        /// CreateUserRole
        /// </summary>
        /// <param name="userRoleModel">UserRoleModel</param>
        internal static Guid CreateUserRole(UserRoleModel model)
        {
            try
            {
                Guid userRoleId = Guid.Empty;
                if (model != null)
                {
                    using (var db = new XSeedEntities())
                    {
                        UserRoleMaster userRole = new UserRoleMaster();

                        userRole.Id = Guid.NewGuid();
                        userRole.Role = model.Role;
                        userRole.Precedence = model.Precedence;
                        userRole.Description = model.Description;
                        userRole.IsActive = Constants.IsActiveTrue;
                        userRole.OrganizationId = model.OrganizationId;

                        var areas = GetOrganizationAreas();

                        foreach (var areaMapping in model.AreaMapping)
                        {
                            var userRoleAreaMappingObj = new UserRoleAreaMapping();
                            userRoleAreaMappingObj.Id = Guid.NewGuid();
                            userRoleAreaMappingObj.AreaId = areas.Where(x => x.Name.ToLower() == areaMapping.Name.ToLower()).Select(x => x.Id.Value).FirstOrDefault();
                            userRoleAreaMappingObj.UserRoleId = userRole.Id;


                            var areaPermissionMappingObj = new AreaPermissionMapping();
                            areaPermissionMappingObj.Id = Guid.NewGuid();
                            areaPermissionMappingObj.UserRoleAreaMappingId = userRoleAreaMappingObj.Id;
                            areaPermissionMappingObj.CreatePermission = areaMapping.Create;
                            areaPermissionMappingObj.UpdatePermission = areaMapping.Update;
                            areaPermissionMappingObj.ReadPermission = areaMapping.Read;
                            areaPermissionMappingObj.DeletePermission = areaMapping.Delete;

                            userRoleAreaMappingObj.AreaPermissionMappings.Add(areaPermissionMappingObj);
                            userRole.UserRoleAreaMappings.Add(userRoleAreaMappingObj);
                        }

                        db.UserRoleMasters.Add(userRole);
                        db.SaveChanges();

                        return userRole.Id;
                    }
                }

                return userRoleId;
            }
            catch (Exception ex)
            {
                Exception exception = new Exception(ex.Message);
                throw exception;
            }
        }

        /// <summary>
        /// Update user role
        /// </summary>
        /// <param name="userRoleModel">User role model</param>
        /// <returns>Status Ok - 200</returns>
        internal static void UpdateUserRole(UserRoleModel userRoleModel)
        {
            if (userRoleModel != null)
            {
                using (var db = new XSeedEntities())
                {
                    UserRoleMaster userRole = db.UserRoleMasters.Find(userRoleModel.Id);

                    if (userRole != null)
                    {
                        userRole.Role = userRoleModel.Role;
                        userRole.Precedence = userRoleModel.Precedence;
                        userRole.Description = userRoleModel.Description;
                        userRole.IsActive = Constants.IsActiveTrue;

                        foreach (var areaMapping in userRoleModel.AreaMapping)
                        {
                            UserRoleAreaMapping userRoleAreaMapping = db.UserRoleAreaMappings.Where(x => x.AreaId == areaMapping.Id && x.UserRoleId == userRoleModel.Id).FirstOrDefault();
                            if (userRoleAreaMapping != null)
                            {
                                AreaPermissionMapping areaPermissionMapping = db.AreaPermissionMappings.Where(x => x.UserRoleAreaMappingId == userRoleAreaMapping.Id).FirstOrDefault();
                                if (areaPermissionMapping != null)
                                {
                                    areaPermissionMapping.CreatePermission = areaMapping.Create;
                                    areaPermissionMapping.UpdatePermission = areaMapping.Update;
                                    areaPermissionMapping.ReadPermission = areaMapping.Read;
                                    areaPermissionMapping.DeletePermission = areaMapping.Delete;
                                }
                            }
                        }

                        db.SaveChanges();

                        /* Insert/ Update role permissions  */
                        


                    }
                }
            }
        }

        /// <summary>
        /// Delete user role
        /// </summary>
        /// <param name="userRoleId">User role id</param>
        /// <returns>Status Ok - 200</returns>
        internal static void DeleteUserRole(Guid userRoleId)
        {
            using (var db = new XSeedEntities())
            {
                UserRoleMaster userRole = db.UserRoleMasters.Find(userRoleId);

                if (userRole != null)
                {
                    foreach (var userRoleAreaMapping in userRole.UserRoleAreaMappings)
                    {
                        userRoleAreaMapping.AreaPermissionMappings.ToList().ForEach(r => db.AreaPermissionMappings.Remove(r));
                    }

                    userRole.UserRoleAreaMappings.ToList().ForEach(r => db.UserRoleAreaMappings.Remove(r));
                    db.UserRoleMasters.Remove(userRole);
                    db.SaveChanges();
                }
            }
        }

        internal static string GetUserTypeByEmail(string userName)
        {
            using (var db = new XSeedEntities())
            {
                var user = db.UserLogins.FirstOrDefault(r => r.UserName == userName && r.IsActive == true);

                return user!= null ? user.UserTypeMaster.Type : string.Empty;
            }
        }

        #endregion

        
    }
}