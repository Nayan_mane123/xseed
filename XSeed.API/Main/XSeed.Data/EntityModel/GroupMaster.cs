﻿
namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [MetadataType(typeof(GroupMasterMD))]
    public partial class GroupMaster : IAuditable
    {
        public class GroupMasterMD
        {
            public System.Guid Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public Nullable<bool> IsActive { get; set; }            
        }
    }
}
