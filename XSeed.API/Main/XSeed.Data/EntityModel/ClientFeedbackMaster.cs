﻿

namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [MetadataType(typeof(ClientFeedbackMasterMD))]
    public partial class ClientFeedbackMaster : IAuditable
    {
        public class ClientFeedbackMasterMD 
        {
            public int Id { get; set; }
            public string Status { get; set; }
            public Nullable<bool> IsActive { get; set; }
        }
    }
}
