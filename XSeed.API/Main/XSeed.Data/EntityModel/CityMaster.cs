﻿

namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [MetadataType(typeof(CityMasterMD))]
    public partial class CityMaster : IAuditable
    {
        public class CityMasterMD
        {
            public int Id { get; set; }
            public int StateId { get; set; }
            public string Name { get; set; }

        }
    }
}
