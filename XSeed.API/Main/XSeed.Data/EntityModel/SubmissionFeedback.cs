﻿namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Configuration;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using XSeed.Data.ViewModel.Notification;
    using XSeed.Data.ViewModel.Submission;
    using XSeed.Utility;

    [MetadataType(typeof(SubmissionFeedbackMD))]
    public partial class SubmissionFeedback : IAuditable
    {
        #region Property Declaration

        public class SubmissionFeedbackMD
        {
            public int Id { get; set; }
            public Nullable<int> SubmissionId { get; set; }
            public Nullable<int> StatusId { get; set; }
            public string Remark { get; set; }
            public string InterviewType { get; set; }
            public Nullable<System.DateTime> Date { get; set; }
        }

        public Guid? ModifiedBy
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public DateTime? ModifiedOn
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region user Defined Functions

        /// <summary>
        /// Get submission feedback list for particular submission
        /// </summary>
        /// <param name="submissionId">Submission Id</param>
        /// <returns>List of feedback</returns>
        internal static List<SubmissionFeedbackModel> GetSubmissionFeedbackList(Guid submissionId)
        {
            List<SubmissionFeedbackModel> list = new List<SubmissionFeedbackModel>();

            if (submissionId != null)
            {
                using (var db = new XSeedEntities())
                {
                    var Submissionfeedbacks = db.SubmissionFeedbacks.Where(f => f.SubmissionId == submissionId).ToList().OrderByDescending(d => d.CreatedOn);

                    foreach (var feedback in Submissionfeedbacks)
                    {
                        SubmissionFeedbackModel model = new SubmissionFeedbackModel();

                        /* Populate model */
                        model.Id = feedback.Id;
                        model.SubmissionId = feedback.SubmissionId;
                        model.FeedbackType = "Internal Feedback";
                        model.StatusId = feedback.StatusId;
                        model.Status = feedback.SubmissionStatusMaster != null ? feedback.SubmissionStatusMaster.Status : string.Empty;
                        model.InterviewType = feedback.InterviewType;
                        model.Remark = feedback.Remark;
                        model.Date = feedback.Date;
                        model.CreatedBy = feedback.CreatedBy.HasValue ? OrganizationUserDetail.GetCreatedByUserName(feedback.CreatedBy) : string.Empty;
                        list.Add(model);
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Add submission feedback
        /// </summary>
        /// <param name="submissionFeedbackModel">Submission Feedback Model </param>
        internal static void AddSubmissionFeedback(SubmissionFeedbackModel submissionFeedbackModel)
        {
            if (submissionFeedbackModel != null)
            {
                using (var db = new XSeedEntities())
                {
                    SubmissionFeedback model = new SubmissionFeedback();

                    /* Populate db model */
                    model.Id = submissionFeedbackModel.Id;
                    model.SubmissionId = submissionFeedbackModel.SubmissionId;
                    model.StatusId = submissionFeedbackModel.StatusId;
                    model.InterviewType = submissionFeedbackModel.InterviewType;
                    model.Remark = submissionFeedbackModel.Remark;
                    model.Date = submissionFeedbackModel.Date;

                    db.SubmissionFeedbacks.Add(model);

                    if (model.SubmissionId != null)
                    {
                        /* Update Submission Status with latest record */
                        var submission = db.SubmissionDetails.Find(model.SubmissionId);
                        submission.StatusId = submissionFeedbackModel.StatusId;
                        submission.Status = submissionFeedbackModel.StatusId != null ? db.SubmissionStatusMasters.Find(submissionFeedbackModel.StatusId).Status : string.Empty;

                        {
                            string companyEmail = submission.JobDetail.CompanyContact.PrimaryEmail;
                            EmailNotificationModel emailNotificationModel = new EmailNotificationModel();
                            List<string> toEmailIds = new List<string>();
                            toEmailIds.Add(companyEmail);
                            emailNotificationModel.ToEmailID = toEmailIds;
                            emailNotificationModel.TypeOfNotification = Constants.typeOfNotificationEmailSubmissionStatusAttachment;
                            emailNotificationModel.FromEmailID = ConfigurationManager.AppSettings["EmailFromUserName"];
                            emailNotificationModel.FromEmailPassword = ConfigurationManager.AppSettings["EmailFromPassword"];
                            emailNotificationModel.CallBackUrl = ConfigurationManager.AppSettings["LoginCallBackURL"];
                            emailNotificationModel.MailBody = submission.Status;
                            emailNotificationModel.ExtraData = submission.CandidateName;
                            EmailNotificationModel.SendEmailNotification(emailNotificationModel);
                        }
                    }
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        string message = ex.Message;
                        throw;
                    }
                    
                }
            }
        }

        #endregion
    }
}
