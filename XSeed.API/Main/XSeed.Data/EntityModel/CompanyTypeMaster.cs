﻿
namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [MetadataType(typeof(CompanyTypeMasterMD))]
    public partial class CompanyTypeMaster:IAuditable
    {
        public class CompanyTypeMasterMD
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public Nullable<bool> IsActive { get; set; }
        }
    }
}
