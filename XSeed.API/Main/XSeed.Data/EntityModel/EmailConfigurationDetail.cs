﻿namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using XSeed.Data.Entity;
    using XSeed.Data.ViewModel.User.UserCommon;
    using XSeed.Utility;


    [MetadataType(typeof(EmailConfigurationDetailMD))]
    public partial class EmailConfigurationDetail : IAuditable
    {
        #region Property Declaration

        public class EmailConfigurationDetailMD
        {
            public System.Guid Id { get; set; }
            public Nullable<System.Guid> UserId { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
        }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Save User Email Configuration
        /// </summary>
        /// <param name="model">Email Configuration Detail Model</param>
        internal static void SaveUserEmailConfiguration(EmailConfigurationDetailModel model)
        {
            if (model != null)
            {
                using (var db = new XSeedEntities())
                {
                    EmailConfigurationDetail emailConfigurationDetailModel = new EmailConfigurationDetail();
                    emailConfigurationDetailModel.UserId = model.UserId;
                    emailConfigurationDetailModel.Email = model.Email;
                    emailConfigurationDetailModel.Password = PasswordUtility.EncryptPassword(model.Password);

                    db.EmailConfigurationDetails.Add(emailConfigurationDetailModel);
                    db.SaveChanges();
                }
            }
        }

        internal static EmailConfigurationDetailModel GetUserEmailConfiguration(Guid UserId)
        {
            if (UserId != null)
            {
                using (var db = new XSeedEntities())
                {
                    var model = db.EmailConfigurationDetails.Where(u => u.UserId == UserId).FirstOrDefault();

                    if (model != null)
                    {
                        EmailConfigurationDetailModel emailConfigurationModel = new EmailConfigurationDetailModel();
                        emailConfigurationModel.Id = model.Id;
                        emailConfigurationModel.Email = model.Email;
                        emailConfigurationModel.Password = model.Password;
                        return emailConfigurationModel;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Update User Email Configuration
        /// </summary>
        /// <param name="model">EmailConfigurationDetailModel</param>
        internal static void UpdateUserEmailConfiguration(EmailConfigurationDetailModel model)
        {
            if (model != null)
            {
                using (var db = new XSeedEntities())
                {
                    var userEmailConfigurationDetails = db.EmailConfigurationDetails.Where(u => u.UserId == model.UserId).FirstOrDefault();

                    if (userEmailConfigurationDetails != null)
                    {
                        userEmailConfigurationDetails.Email = model.Email;
                        userEmailConfigurationDetails.Password = PasswordUtility.EncryptPassword(model.Password);
                    }

                    db.SaveChanges();
                }
            }
        }

        #endregion

    }
}
