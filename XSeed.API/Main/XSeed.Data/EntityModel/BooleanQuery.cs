﻿namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using XSeed.Data.Entity;
    using XSeed.Data.ViewModel.BooleanSearch;
    using XSeed.Utility;

    [MetadataType(typeof(BooleanQueryMD))]
    public partial class BooleanQuery : IAuditable
    {
        #region Property Declaration

        public class BooleanQueryMD
        {
            public int Id { get; set; }
            public string Tag { get; set; }
            public string Query { get; set; }
            public Nullable<int> OrganizationId { get; set; }
            public Nullable<bool> IsActive { get; set; }
        }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Save Boolean Query
        /// </summary>
        /// <param name="booleanQueryModel">BooleanQueryModel</param>
        /// <returns></returns>
        internal static void SaveBooleanQuery(BooleanQueryModel booleanQueryModel)
        {
            using (var db = new XSeedEntities())
            {
                BooleanQuery model = new BooleanQuery();
                model.Tag = booleanQueryModel.Tag;
                model.Query = booleanQueryModel.Query;
                model.OrganizationId = booleanQueryModel.OrganizationId;
                model.IsActive = Constants.IsActiveTrue;

                db.BooleanQueries.Add(model);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Get List of Boolean Queries
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <returns>List of Boolean Queries</returns>
        internal static List<BooleanQueryModel> ListAllBooleanQueries(Guid organizationId)
        {
            List<BooleanQueryModel> listBooleanQueries = new List<BooleanQueryModel>();

            if (organizationId != Guid.Empty)
            {
                using (var db = new XSeedEntities())
                {
                    var booleanQueries = db.BooleanQueries.Where(q => q.OrganizationId == organizationId).ToList().OrderByDescending(d => d.CreatedOn);

                    if (booleanQueries.Count() > 0)
                    {
                        foreach (var query in booleanQueries)
                        {
                            var user = db.OrganizationUserDetails.Where(u => u.UserId == query.CreatedBy).FirstOrDefault();
                            BooleanQueryModel model = new BooleanQueryModel();
                            model.Id = query.Id;
                            model.Tag = query.Tag;
                            model.Query = query.Query;
                            model.CreatedBy = (user == null ? "" : user.FirstName) + " " + (user == null ? "" : user.LastName);
                            listBooleanQueries.Add(model);
                        }
                    }
                }
            }
            return listBooleanQueries;
        }

        #endregion

    }
}
