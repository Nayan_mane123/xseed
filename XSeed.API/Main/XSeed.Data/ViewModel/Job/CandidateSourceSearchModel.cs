﻿using LinqKit;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Dice;
using XSeed.Data.ViewModel.User.CandidateUser;
using XSeed.MonsterJobPortalAPI;
using XSeed.MonsterJobPortalAPI.Model;

namespace XSeed.Data.ViewModel.Job
{
    public class CandidateSourceSearchModel
    {
        static string url = ConfigurationManager.AppSettings["MongoURL"].ToString();
        static string dbName = ConfigurationManager.AppSettings["MongoDB"].ToString();
        MongoClient client = new MongoClient(url);

        #region Property Declaration

        public Guid JobId { get; set; }
        public List<string> Sources { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public string Location { get; set; }
        public Nullable<int> ExperienceInYear { get; set; }
        public Nullable<int> ExperienceInMonth { get; set; }
        public Nullable<int> MinExperience { get; set; }
        public Nullable<int> MaxExperience { get; set; }
        public Nullable<decimal> MinSalary { get; set; }
        public Nullable<decimal> MaxSalary { get; set; }
        public string SalaryType { get; set; }
        public string CurrencyType { get; set; }
        public List<string> Skills { get; set; }
        public string MonsterBooleanQuery { get; set; }
        public string MonsterLocationMilesAway { get; set; }
        public string MonsterLocationZIPCode { get; set; }
        public List<string> MonsterUSLocationID { get; set; }
        public string LocationType { get; set; }
        public string MonsterTargetWorkLocation { get; set; }
        public string MonsterResumeMinDate { get; set; }
        public string MonsterResumeMaxDate { get; set; }
        public List<string> SearchLocations { get; set; }


        #endregion

        #region User Defined Functions

        /// <summary>
        /// Source Candidates against search parameters
        /// </summary>
        /// <param name="searchModel">Candidate Source Search Model</param>
        /// <param name="pageSize">page Size</param>
        /// <param name="pageNumber">page Number</param>
        /// <param name="sortBy">sort By</param>
        /// <param name="sortOrder">sort Order</param>
        /// <param name="isExport">Is Export</param>
        /// <returns>Candidates</returns>
        public List<CandidateSourceResultModel> SourceCandidates(CandidateSourceSearchModel model, int pageSize, int pageNumber, string sortBy, string sortOrder, bool isExport)
        {
            List<CandidateSourceResultModel> list = new List<CandidateSourceResultModel>();

            if (model.Sources != null)
            {
                if (model.Sources.Contains("Local"))
                {
                    IQueryable<CandidateUserModel> localCandidates = SourceCandidatesFromLocal(model);

                    if (localCandidates != null)
                    {
                        //var localCandidateList = localCandidates.OrderByDescending(d => d.CreatedOn).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
                        var localCandidateList = localCandidates.OrderByDescending(d => d.CreatedOn).ThenByDescending(d => d.ModifiedOn).Take(20); //ToDo: Set count after all sources configuration

                        var localcandidates = CandidateUserModel.PopulateCandidateList(localCandidateList).ToList();

                        foreach (var localCandidate in localcandidates)
                        {
                            CandidateSourceResultModel sourceResultModel = CandidateSourceResultModel.PopulateLocalCandidate(localCandidate, model);
                            list.Add(sourceResultModel);
                        }
                    }
                }

                if (model.Sources.Contains("Monster"))
                {
                    var monsterLicenseInfo = GetMonsterLincenseInfo();

                    if (monsterLicenseInfo.IsLincenseValid)
                    {
                        MonsterResumeAPI resumeAPI = new MonsterResumeAPI();
                        MonsterCandidateSearch monsterCandidateSearch = BuildMonsterSearchModel(model);
                        List<MonsterCandidate> monsterCandidates = resumeAPI.GetMonsterCandidates(monsterCandidateSearch);

                        if (monsterCandidates != null)
                        {
                            var monsterCandidatesResumeIdList = monsterCandidates.Select(x => x.ResumeId).ToList();
                            var database = client.GetDatabase(dbName);

                            var localCandidates = database.GetCollection<CandidateUserModel>("CandidateDetail")
                                .AsQueryable(new AggregateOptions { AllowDiskUse = true })
                                .Where(x => monsterCandidatesResumeIdList.Contains(x.ProviderCandidateId)).ToList();

                            foreach (var monsterCandidate in monsterCandidates)
                            {
                                CandidateUserModel candidateDetailsIfExists = localCandidates.Where(x => x.ProviderCandidateId == monsterCandidate.ResumeId).FirstOrDefault();

                                CandidateSourceResultModel sourceResultModel = CandidateSourceResultModel.PopulateMonsterCandidate(monsterCandidate, candidateDetailsIfExists);
                                list.Add(sourceResultModel);
                            }
                        }
                    }
                }
                if (model.Sources.Contains("Dice"))
                {
                    var diceTokenResponse = DiceCandidateSearchResultModel.GetDiceTokenInfo();

                    if (diceTokenResponse != null && !string.IsNullOrWhiteSpace(diceTokenResponse.access_token))
                    {
                        //MonsterResumeAPI resumeAPI = new MonsterResumeAPI();
                        DiceCandidateSearchModel diceCandidateSearchModel = BuildDiceCandidateSearchModel(model);
                        DiceCandidateSearchResultModel diceCandidateList = DiceCandidateSearchResultModel.GetDiceSearchCandidateResult(diceCandidateSearchModel, diceTokenResponse.access_token);

                        if (diceCandidateList != null && diceCandidateList.count > 0)
                        {
                            var diceCandidatesIdList = diceCandidateList.documents.Select(x => x.id).ToList();
                            var database = client.GetDatabase(dbName);

                            var localCandidates = database.GetCollection<CandidateUserModel>("CandidateDetail")
                                .AsQueryable(new AggregateOptions { AllowDiskUse = true })
                                .Where(x => diceCandidatesIdList.Contains(x.ProviderCandidateId)).ToList();

                            foreach (var diceCandidate in diceCandidateList.documents)
                            {
                                CandidateUserModel candidateDetailsIfExists = localCandidates.Where(x => x.ProviderCandidateId == diceCandidate.id).FirstOrDefault();

                                CandidateSourceResultModel sourceResultModel = CandidateSourceResultModel.PopulateDiceCandidate(diceCandidate, candidateDetailsIfExists, model);
                                list.Add(sourceResultModel);
                            }
                        }
                    }
                }
            }

            if (list.Count > 0)
            {
                list = list.OrderByDescending(x => !string.IsNullOrEmpty(x.ModifiedDate) ? Convert.ToDateTime(x.ModifiedDate) : Convert.ToDateTime(x.CreatedDate))
                    .ThenByDescending(x => Convert.ToDateTime(x.CreatedDate))
                    .ThenByDescending(x => Convert.ToDouble(x.Relevance))
                    .ThenByDescending(x => x.Source).ToList();
            }

            return list;
        }


        /// <summary>
        /// Get Monster Lincense Info
        /// </summary>
        /// <returns>Portal License Info</returns>
        public PortalLicenseInfo GetMonsterLincenseInfo()
        {
            PortalLicenseInfo model = new PortalLicenseInfo();

            /* Initialize monster api */
            MonsterAccountAPI accountAPI = new MonsterAccountAPI();

            /* call License info api */
            LicenseInfo monsterLicenseInfo = accountAPI.getLicenseInformation();
            model.IsLincenseValid = true;

            if (monsterLicenseInfo != null)
            {
                if (Convert.ToDateTime(monsterLicenseInfo.InventoryExpireDate) < DateTime.UtcNow)
                {
                    /* License has expired */
                    model.IsLincenseValid = false;
                }

                if (Convert.ToInt32(monsterLicenseInfo.TotalAvailableQuantity) == 0)
                {
                    /* Resume download limit exceeded */
                    model.IsLincenseValid = false;
                }

                model.LicenseExpiryDate = Convert.ToDateTime(monsterLicenseInfo.InventoryExpireDate);
                model.AvailableCount = Convert.ToInt32(monsterLicenseInfo.TotalAvailableQuantity);
            }

            return model;
        }

        /// <summary>
        /// Convert Candidate Source Search Model To Monster Candidate Search Model
        /// </summary>
        /// <param name="model">Candidate Source Search Model</param>
        /// <returns>Monster Candidate Search</returns>
        private MonsterCandidateSearch BuildMonsterSearchModel(CandidateSourceSearchModel model)
        {
            return new MonsterCandidateSearch()
            {
                Skills = model.Skills,
                ExperienceLevel = model.ExperienceInMonth != null ? model.ExperienceInYear.ToString() + "." + model.ExperienceInMonth.ToString() : model.ExperienceInYear.ToString(),
                Location = model.Location,
                // SalaryRange = model.SalaryRange,
                Title = model.Title,
                Type = model.Type,
                MonsterBooleanQuery = model.MonsterBooleanQuery,
                MonsterLocationMilesAway = model.MonsterLocationMilesAway,
                MonsterLocationZIPCode = model.MonsterLocationZIPCode,
                MonsterUSLocationID = model.MonsterUSLocationID,
                LocationType = model.LocationType,
                MonsterTargetWorkLocation = model.MonsterTargetWorkLocation,
                MonsterResumeMinDate = model.MonsterResumeMinDate,
                MonsterResumeMaxDate = model.MonsterResumeMaxDate
            };
        }

        /// <summary>
        /// Source Candidates From Local database with search parameters
        /// </summary>
        /// <param name="model">Candidate Source Model</param>
        /// <returns>IQueryable<CandidateUserModel></returns>
        public IQueryable<CandidateUserModel> SourceCandidatesFromLocal(CandidateSourceSearchModel model)
        {
            /* Build Predicate */
            var predicate = BuildPredicate(model);

            /* Get Database collection */
            var database = client.GetDatabase(dbName);
            var collection = database.GetCollection<CandidateUserModel>("CandidateDetail");

            /* Apply Predicate */
            IQueryable<CandidateUserModel> candidates = collection.AsQueryable(new AggregateOptions { AllowDiskUse = true }).AsExpandable().Where(predicate);

            return candidates;
        }

        /// <summary>
        /// Build predicate to search loacal candidate with various parameters
        /// </summary>
        /// <param name="model">Candidate Source Model</param>
        /// <returns>ExpressionStarter<CandidateUserModel></returns>
        private ExpressionStarter<CandidateUserModel> BuildPredicate(CandidateSourceSearchModel model)
        {
            var predicate = PredicateBuilder.New<CandidateUserModel>();

            /* Search with Title */
            if (!string.IsNullOrEmpty(model.Title))
            {
                string word = model.Title.ToLower();
                predicate = predicate.Or(c => c.ProfileTitle.ToLower().Contains(word));
            }

            /* Search with Skills */
            if (model.Skills != null)
            {
                foreach (var skill in model.Skills)
                {
                    string word = skill.ToLower();
                    predicate = predicate.Or(c => c.Skills.Any(s => s.ToLower().Contains(word)));
                }
            }

            /* Search with Job Type */
            if (!string.IsNullOrEmpty(model.Type))
            {
                string word = model.Type.ToLower();
                predicate = predicate.Or(c => c.EmployementType.ToLower().Contains(word));
            }

            /* Search with Location */
            if (!string.IsNullOrEmpty(model.Location))
            {
                string word = model.Location.ToLower();
                predicate = predicate.Or(c => c.Country.ToLower().Contains(word) || c.State.ToLower().Contains(word) || c.City.ToLower().Contains(word) || c.CurrentLocation.ToLower().Contains(word) || c.PreferredLocation.ToLower().Contains(word));
            }

            /* Search with Experience Level */
            if (model.ExperienceInYear.HasValue)
            {
                predicate = predicate.Or(c => c.ExperienceInYear == model.ExperienceInYear || c.USExperienceInYear == model.ExperienceInYear);
            }

            /* Search with Experience Level */
            if (model.ExperienceInMonth.HasValue)
            {
                predicate = predicate.Or(c => c.ExperienceInMonth == model.ExperienceInMonth || c.USExperienceInMonth == model.ExperienceInMonth);
            }

            /* Search with Min Salary */
            if (model.MinSalary.HasValue)
            {
                predicate = predicate.Or(c => c.CurrentSalary >= model.MinSalary || c.ExpectedSalary >= model.MinSalary);
            }

            /* Search with max Salary */
            if (model.MaxSalary.HasValue)
            {
                predicate = predicate.Or(c => c.CurrentSalary <= model.MaxSalary || c.ExpectedSalary <= model.MaxSalary);
            }

            return predicate;
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>
        /// <param name="searchModel">Candidate Source Search Model</param>
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        internal Common.PaginationModel GetSourceSearchPaginationInfo(CandidateSourceSearchModel searchModel, int pageSize)
        {
            PaginationModel model = new PaginationModel();

            if (searchModel.Sources != null && searchModel.Sources.Contains("Local"))
            {
                IQueryable<CandidateUserModel> localCandidates = SourceCandidatesFromLocal(searchModel);

                if (localCandidates != null)
                {
                    model.TotalCount = localCandidates.Count();
                    model.TotalPages = Math.Ceiling((double)model.TotalCount / pageSize);
                }
            }

            return model;
        }

        /// <summary>
        /// Build Dice Candidate Search Model
        /// </summary>
        /// <param name="model">Candidate Source Search Model</param>
        /// <returns>Dice Candidate Search Model</returns>
        private DiceCandidateSearchModel BuildDiceCandidateSearchModel(CandidateSourceSearchModel model)
        {
            DiceCandidateSearchModel diceCandidateSearchModel = new DiceCandidateSearchModel();

            diceCandidateSearchModel.Title = model.Title;
            diceCandidateSearchModel.Type = model.Type;
            diceCandidateSearchModel.ExperienceLevel = model.ExperienceInMonth != null ? model.ExperienceInYear.ToString() + "." + model.ExperienceInMonth.ToString() : model.ExperienceInYear.ToString();
            diceCandidateSearchModel.MinSalary = model.MinSalary.ToString();
            diceCandidateSearchModel.MaxSalary = model.MaxSalary.ToString();
            diceCandidateSearchModel.Skills = model.Skills;
            diceCandidateSearchModel.SalaryType = model.SalaryType;
            diceCandidateSearchModel.Location = model.Location;
            diceCandidateSearchModel.MinExperience = model.MinExperience.ToString();
            diceCandidateSearchModel.MaxExperience = model.MaxExperience.ToString();

            //for boolean search
            diceCandidateSearchModel.BooleanSearchQuery = model.MonsterBooleanQuery;
            if (!string.IsNullOrWhiteSpace(model.MonsterResumeMinDate) && !string.IsNullOrWhiteSpace(model.MonsterResumeMaxDate))
            {
                var resumePostedTo = TimeSpan.FromMinutes(Convert.ToDouble(model.MonsterResumeMaxDate));
                diceCandidateSearchModel.ResumePostedTo = resumePostedTo.Days.ToString();
            }
            //diceCandidateSearchModel.ResumePostedFrom = model.MonsterResumeMinDate;
            //diceCandidateSearchModel.ResumePostedTo = model.MonsterResumeMaxDate;
            diceCandidateSearchModel.LocationMilesAway = model.MonsterLocationMilesAway;
            diceCandidateSearchModel.LocationZIPCode = model.MonsterLocationZIPCode;
            diceCandidateSearchModel.SearchLocations = model.SearchLocations;


            return diceCandidateSearchModel;
        }

        #endregion
    }
}