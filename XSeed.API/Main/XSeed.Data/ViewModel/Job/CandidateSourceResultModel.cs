﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Dice;
using XSeed.Data.ViewModel.User.CandidateUser;

namespace XSeed.Data.ViewModel.Job
{
    public class CandidateSourceResultModel
    {
        static string url = ConfigurationManager.AppSettings["MongoURL"].ToString();
        static string dbName = ConfigurationManager.AppSettings["MongoDB"].ToString();
        static MongoClient client = new MongoClient(url);

        #region Property Declaration

        public string Source { get; set; }
        public string ProviderCandidateId { get; set; }
        public string Candidate_Id { get; set; }
        public string CandidateId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string HomePhone { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public string Location { get; set; }
        public string ExperienceLevel { get; set; }
        public string SalaryRange { get; set; }
        public List<string> Skills { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Relevance { get; set; }
        public bool HasLocalCopy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string ProviderModifiedDate { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Populate Local Candidate model
        /// </summary>
        /// <param name="localCandidate">Candidate User Model</param>
        /// <returns>Candidate Source Result Model</returns>
        internal static CandidateSourceResultModel PopulateLocalCandidate(CandidateUserModel localCandidate, CandidateSourceSearchModel searchModel)
        {
            CandidateSourceResultModel model = new CandidateSourceResultModel();

            model.Source = "Local";
            model.Candidate_Id = localCandidate._id;
            model.CandidateId = localCandidate.CandidateId;
            model.Name = localCandidate.FirstName + " " + localCandidate.LastName;
            model.Email = localCandidate.PrimaryEmail;
            model.Mobile = localCandidate.Mobile;
            model.HomePhone = localCandidate.Phone;
            model.Title = localCandidate.ProfileTitle;
            model.Type = localCandidate.EmployementType;
            model.Skills = localCandidate.Skills;
            model.Location = localCandidate.CurrentLocation;
            model.ExperienceLevel = localCandidate.ExperienceInMonth.HasValue ? (localCandidate.ExperienceInYear.ToString() + "." + localCandidate.ExperienceInMonth.ToString()) : localCandidate.ExperienceInYear.ToString();
            model.SalaryRange = localCandidate.CurrentSalary.ToString();
            model.Country = localCandidate.Country;
            model.State = localCandidate.State;
            model.City = localCandidate.City;
            model.Zip = localCandidate.Zip;
            model.ProviderModifiedDate = localCandidate.ProviderModifiedDate;
            model.ProviderCandidateId = localCandidate.ProviderCandidateId;
            model.CreatedDate = !string.IsNullOrEmpty(localCandidate.CreatedOn.ToString()) ? localCandidate.CreatedOn.ToString() : null;
            model.ModifiedDate = !string.IsNullOrEmpty(localCandidate.ModifiedOn.ToString()) ? localCandidate.ModifiedOn.ToString() : null;

            ApplyRelevanceLogic(ref model, searchModel);

            return model;
        }

        private static void ApplyRelevanceLogic(ref CandidateSourceResultModel candidateModel, CandidateSourceSearchModel searchModel)
        {
            int totalSearchParamCount = 0;
            int totalMatchingSearchParamCount = 0;

            /* Search with Title */
            if (!string.IsNullOrEmpty(searchModel.Title))
            {
                ++totalSearchParamCount;

                string word = searchModel.Title.ToLower();
                if (!string.IsNullOrEmpty(candidateModel.Title) && candidateModel.Title.ToLower().Contains(word))
                {
                    ++totalMatchingSearchParamCount;
                }
            }

            /* Search with Skills */
            if (searchModel.Skills != null)
            {
                ++totalSearchParamCount;
                foreach (var skill in searchModel.Skills)
                {
                    string word = skill.ToLower();
                    if (candidateModel.Skills != null && candidateModel.Skills.Any(s => s.ToLower().Contains(word)))
                    {
                        ++totalMatchingSearchParamCount;
                        break;
                    }
                }
            }

            /* Search with Job Type */
            if (!string.IsNullOrEmpty(searchModel.Type))
            {
                totalSearchParamCount++;
                string word = searchModel.Type.ToLower();
                if (!string.IsNullOrEmpty(candidateModel.Type) && candidateModel.Type.ToLower().Contains(word))
                {
                    ++totalMatchingSearchParamCount;
                }
            }

            /* Search with Location */
            if (!string.IsNullOrEmpty(searchModel.Location))
            {
                totalSearchParamCount++;
                string word = searchModel.Location.ToLower();
                if ((!string.IsNullOrEmpty(candidateModel.Country) && candidateModel.Country.ToLower().Contains(word))
                    || (!string.IsNullOrEmpty(candidateModel.State) && candidateModel.State.ToLower().Contains(word))
                    || (!string.IsNullOrEmpty(candidateModel.City) && candidateModel.City.ToLower().Contains(word))
                    || (!string.IsNullOrEmpty(candidateModel.Location) && candidateModel.Location.ToLower().Contains(word)))
                {
                    ++totalMatchingSearchParamCount;
                }
            }

            /* Search with Experience Level */
            if (searchModel.ExperienceInYear.HasValue)
            {
                totalSearchParamCount++;
                if (candidateModel.ExperienceLevel.ToLower() == searchModel.ExperienceInYear.ToString().ToLower() + "." + searchModel.ExperienceInMonth.ToString().ToLower())
                {
                    ++totalMatchingSearchParamCount;
                }
            }

            /* Search with Min Salary */
            if (searchModel.MinSalary.HasValue)
            {
                totalSearchParamCount++;
                if (!string.IsNullOrEmpty(candidateModel.SalaryRange) && Convert.ToDecimal(candidateModel.SalaryRange) >= searchModel.MinSalary)
                {
                    ++totalMatchingSearchParamCount;
                }
            }

            /* Search with max Salary */
            if (searchModel.MaxSalary.HasValue)
            {
                totalSearchParamCount++;
                if (!string.IsNullOrEmpty(candidateModel.SalaryRange) && Convert.ToDecimal(candidateModel.SalaryRange) <= searchModel.MaxSalary)
                {
                    ++totalMatchingSearchParamCount;
                }
            }

            candidateModel.Relevance = (Math.Round(((double)totalMatchingSearchParamCount / (double)totalSearchParamCount), 2) * 100).ToString();
        }

        /// <summary>
        /// Populate Monster Candidate model
        /// </summary>
        /// <param name="monsterCandidate">Monster Candidate Model</param>
        /// <returns>Candidate Source Result Model</returns>
        internal static CandidateSourceResultModel PopulateMonsterCandidate(MonsterJobPortalAPI.Model.MonsterCandidate monsterCandidate, CandidateUserModel candidateDetailsIfExists)
        {
            CandidateSourceResultModel model = new CandidateSourceResultModel();

            /* Get Database collection */
            var database = client.GetDatabase(dbName);
            var collection = database.GetCollection<CandidateUserModel>("CandidateDetail");

            model.Source = "Monster";
            model.Candidate_Id = null;
            model.ProviderCandidateId = monsterCandidate.ResumeId;
            model.Name = monsterCandidate.FirstName + " " + monsterCandidate.LastName;
            model.Email = string.Empty; //monsterCandidate.Email;
            model.Mobile = string.Empty; //monsterCandidate.Mobile;
            model.HomePhone = string.Empty; //monsterCandidate.Phone;
            model.Title = monsterCandidate.ResumeTitle;
            model.Type = !string.IsNullOrEmpty(monsterCandidate.TargetJobType) ? monsterCandidate.TargetJobType : string.Empty;
            model.Skills = null;  //monsterCandidate.Skills;
            model.Location = monsterCandidate.Location;
            model.ExperienceLevel = string.Empty; //monsterCandidate.Experience;
            model.SalaryRange = !string.IsNullOrEmpty(monsterCandidate.TargetMinSalary) ? monsterCandidate.TargetMinSalary + "-" + monsterCandidate.TargetMaxSalary : monsterCandidate.TargetMaxSalary;
            model.Relevance = !string.IsNullOrEmpty(monsterCandidate.Relevance) ? (Convert.ToDouble(monsterCandidate.Relevance) * 100).ToString() : "0";
            model.Country = monsterCandidate.Country;
            model.State = monsterCandidate.State;
            model.City = monsterCandidate.City;
            model.Zip = monsterCandidate.PostalCode;
            model.CreatedDate = !string.IsNullOrEmpty(monsterCandidate.CreatedDate) ? monsterCandidate.CreatedDate : null;
            model.ModifiedDate = !string.IsNullOrEmpty(monsterCandidate.ModifiedDate) ? monsterCandidate.ModifiedDate : null;
            model.ProviderModifiedDate = !string.IsNullOrEmpty(monsterCandidate.ModifiedDate) ? monsterCandidate.ModifiedDate : null;

            if (candidateDetailsIfExists != null)
            {
                model.HasLocalCopy = true;
                model.Candidate_Id = candidateDetailsIfExists._id;
                model.ProviderModifiedDate = candidateDetailsIfExists.ProviderModifiedDate;
                model.ModifiedDate = candidateDetailsIfExists.ProviderModifiedDate;
                model.CandidateId = candidateDetailsIfExists.CandidateId;
            }

            return model;
        }

        /// <summary>
        /// Populate Dice Candidate
        /// </summary>
        /// <param name="diceCandidate">Documents(Dice Candidates)</param>
        /// <param name="candidateDetailsIfExists"></param>
        /// <returns></returns>
        internal static CandidateSourceResultModel PopulateDiceCandidate(Documents diceCandidate, CandidateUserModel candidateDetailsIfExists, CandidateSourceSearchModel searchModel)
        {
            CandidateSourceResultModel model = new CandidateSourceResultModel();

            /* Get Database collection */
            var database = client.GetDatabase(dbName);
            var collection = database.GetCollection<CandidateUserModel>("CandidateDetail");

            model.Source = "Dice";
            model.Candidate_Id = null;
            model.ProviderCandidateId = diceCandidate.id;
            model.Name = diceCandidate.contact != null ? diceCandidate.contact.formattedName : (diceCandidate.contact.firstName + " " + diceCandidate.contact.lastName);
            model.Email = diceCandidate.contact != null ? diceCandidate.contact.email : string.Empty;
            model.Mobile = diceCandidate.contact != null ? diceCandidate.contact.phoneNumber : string.Empty;
            model.HomePhone = diceCandidate.contact != null ? diceCandidate.contact.altPhoneNumber : string.Empty;
            model.Title = diceCandidate.profileName != null ? diceCandidate.profileName : diceCandidate.desiredPosition;
            model.Type = diceCandidate.desiredEmployment != null ? diceCandidate.desiredEmployment.type : string.Empty;
            if (diceCandidate.skillList != null)
            {
                model.Skills = new List<string>();
                foreach (var skill in diceCandidate.skillList)
                {
                    model.Skills.Add(skill.name);
                }
            }
            model.Location = (diceCandidate.contact != null && diceCandidate.contact.location != null) ? diceCandidate.contact.location.formattedLocation : diceCandidate.contact.location.country;
            model.ExperienceLevel = diceCandidate.yearsOfExperience;
            model.SalaryRange = diceCandidate.desiredEmployment != null ? diceCandidate.desiredEmployment.annualSalary : diceCandidate.desiredEmployment.hourlyPayRate;
            //model.Relevance = "0";
            model.Country = (diceCandidate.contact != null && diceCandidate.contact.location != null) ? diceCandidate.contact.location.country : diceCandidate.contact.location.formattedLocation;
            model.State = (diceCandidate.contact != null && diceCandidate.contact.location != null) ? diceCandidate.contact.location.region : diceCandidate.contact.location.formattedLocation;
            model.City = (diceCandidate.contact != null && diceCandidate.contact.location != null) ? diceCandidate.contact.location.region : diceCandidate.contact.location.formattedLocation;
            model.Zip = (diceCandidate.contact != null && diceCandidate.contact.location != null) ? diceCandidate.contact.location.postalCode : string.Empty;
            model.CreatedDate = diceCandidate.datePosted != null ? diceCandidate.datePosted.ToString() : diceCandidate.announceDate.ToString();
            model.ModifiedDate = diceCandidate.lastModified != null ? diceCandidate.lastModified.ToString() : string.Empty;
            model.ProviderModifiedDate = string.Empty;

            if (candidateDetailsIfExists != null)
            {
                model.HasLocalCopy = true;
                model.Candidate_Id = candidateDetailsIfExists._id;
                model.ProviderModifiedDate = candidateDetailsIfExists.ProviderModifiedDate;
                model.ModifiedDate = candidateDetailsIfExists.ProviderModifiedDate;
                model.CandidateId = candidateDetailsIfExists.CandidateId;
            }

            return model;
        }

        #endregion
    }
}
