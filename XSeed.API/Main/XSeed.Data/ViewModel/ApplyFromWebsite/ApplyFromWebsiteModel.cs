﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Common;

namespace XSeed.Data.ViewModel.ApplyFromWebsite
{
    public class ApplyFromWebsiteModel
    {
        public string JobCode { get; set; }
        public Nullable<Guid> JobId { get; set; }
        public string JobTitle { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Location { get; set; }
        public string VisaStatus { get; set; }
        public XSeedFileEntity Resume { get; set; }
    }
}
