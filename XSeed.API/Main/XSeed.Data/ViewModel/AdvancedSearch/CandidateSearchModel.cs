﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.User.CandidateUser;

namespace XSeed.Data.ViewModel.AdvancedSearch
{
    public class CandidateSearchModel
    {
        static string url = ConfigurationManager.AppSettings["MongoURL"].ToString();
        static string dbName = ConfigurationManager.AppSettings["MongoDB"].ToString();

        public string Address { get; set; }
        public List<string> Skills { get; set; }
        public List<string> DegreeList { get; set; }
        public string TotalCount { get; set; }
        public string TotalPages { get; set; }

        /// <summary>
        /// Candidate Advanced Search
        /// </summary>
        /// <param name="candidateSearchModel">Candidate Search Model</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>
        /// <param name="TotalCount">TotalCount</param>
        /// <param name="TotalPages">TotalPages</param>
        /// <returns>Candidate List</returns>
        public List<CandidateUserModel> SearchCandidates(CandidateSearchModel candidateSearchModel, int pageSize, int pageNumber, out int TotalCount, out double TotalPages)
        {
            List<CandidateUserModel> list = new List<CandidateUserModel>();

            MongoClient client = new MongoClient(url);
            var database = client.GetDatabase(dbName);

            var collection = database.GetCollection<CandidateUserModel>("CandidateDetail");
            var documents = collection.Find(Builders<CandidateUserModel>.Filter.Empty).ToList();

            var builder = Builders<CandidateUserModel>.Filter;

            var filt = Builders<CandidateUserModel>.Filter.Empty;

            /* Apply Address filter */
            if (!string.IsNullOrEmpty(candidateSearchModel.Address))
            {
                List<string> searchQuery = new List<string>();

                searchQuery = candidateSearchModel.Address.Split(new Char[] { ' ', ',', ';', '|', '&' }, StringSplitOptions.RemoveEmptyEntries).ToArray().ToList();

                foreach (var query in searchQuery)
                {
                    filt = filt & (builder.Regex("Address1", new BsonRegularExpression(query, ".*" + query + ".*'i")) |
                                   builder.Regex("Address2", new BsonRegularExpression(query, ".*" + query + ".*'i")) |
                                   builder.Regex("Address3", new BsonRegularExpression(query, ".*" + query + ".*'i")) |
                                   builder.Regex("Country", new BsonRegularExpression(query, ".*" + query + ".*'i")) |
                                   builder.Regex("State", new BsonRegularExpression(query, ".*" + query + ".*'i")) |
                                   builder.Regex("City", new BsonRegularExpression(query, ".*" + query + ".*'i")) |
                                   builder.Regex("Zip", new BsonRegularExpression(query, ".*" + query + ".*'i")));
                }
            }

            /* Apply Degrees filter */
            if (candidateSearchModel.DegreeList != null && candidateSearchModel.DegreeList.Count > 0)
            {
                filt = filt & (Builders<CandidateUserModel>.Filter.In("DegreeList", candidateSearchModel.DegreeList));
            }

            /* Apply skills filter */
            if (candidateSearchModel.Skills != null && candidateSearchModel.Skills.Count > 0)
            {
                filt = filt & Builders<CandidateUserModel>.Filter.In("Skills", candidateSearchModel.Skills);
            }

            TotalCount = (int)collection.Find(filt).Count();
            TotalPages = Math.Ceiling((double)TotalCount / pageSize);

            var result = collection.Find(filt).Skip((pageNumber - 1) * pageSize).Limit(pageSize).ToList();

            return result;
        }
    }
}
