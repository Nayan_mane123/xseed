﻿using LinqKit;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.User.CandidateUser;

namespace XSeed.Data.ViewModel.AdvancedSearch
{
    public class CandidateAdvancedSearchModel
    {
        static string url = ConfigurationManager.AppSettings["MongoURL"].ToString();
        static string dbName = ConfigurationManager.AppSettings["MongoDB"].ToString();
        MongoClient client = new MongoClient(url);

        #region Property Declaration

        public List<string> SearchIn { get; set; }
        public string SearchAll { get; set; }
        public string SearchAny { get; set; }
        public string SearchNone { get; set; }
        public string SearchExact { get; set; }
        public string SearchZip { get; set; }

        public CandidateFilterModel filter { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Candidate Advanced Search API
        /// </summary>
        /// <param name="candidateAdvancedSearchModel">Candidate Advanced Search Model</param>        
        /// <param name="pageSize">Page Size</param>
        /// <param name="pageNumber">Page Number</param>
        /// <param name="sortBy">Sort By</param>
        /// <param name="sortOrder">Sort Order</param>
        /// <returns>Candidate List</returns>
        public List<CandidateUserModel> SearchCandidates(CandidateAdvancedSearchModel candidateAdvancedSearchModel, string sortBy, string sortOrder, int pageSize, int pageNumber, out int TotalCount, out double TotalPages, bool isExport = false)
        {
            List<CandidateUserModel> list = AdvancedSearchCandidates(candidateAdvancedSearchModel, pageSize, pageNumber, sortBy, sortOrder, out TotalCount, out TotalPages, isExport);

            return list;
        }

        #region Advanced Search

        /// <summary>
        /// Advanced Search for Candidate Module Mulitple Keywords with multiple columns
        /// </summary>
        /// <param name="candidateAdvancedSearchModel">Candidate Advanced Search Model</param>
        /// <param name="pageSize">Page Size</param>
        /// <param name="pageNumber">Page Number</param>
        /// <param name="TotalCount">Total Count</param>
        /// <param name="TotalPages">Total Pages</param>
        /// <returns>List<CandidatesDetailModel></returns>
        private List<CandidateUserModel> AdvancedSearchCandidates(CandidateAdvancedSearchModel searchModel, int pageSize, int pageNumber, string sortBy, string sortOrder, out int TotalCount, out double TotalPages, bool isExport = false)
        {
            List<CandidateUserModel> list = new List<CandidateUserModel>();
            IQueryable<CandidateUserModel> candidates = null;

            /* Get Database collection */
            var database = client.GetDatabase(dbName);
            var collection = database.GetCollection<CandidateUserModel>("CandidateDetail");


            /* Get All When not with AND/ OR/ EQUAL */
            if (string.IsNullOrEmpty(searchModel.SearchAll) && string.IsNullOrEmpty(searchModel.SearchAll) && string.IsNullOrEmpty(searchModel.SearchExact))
            {
                candidates = collection.AsQueryable(new AggregateOptions { AllowDiskUse = true });
            }
            else
            {
                //For exact matches
                var predicate = Builders<CandidateUserModel>.Filter.Where(
                      c => c.CandidateId.ToLower().Contains(searchModel.SearchAll.ToLower()) ||
                      c.PrimaryEmail.ToLower().Contains(searchModel.SearchAll.ToLower()) || c.SecondaryEmail.ToLower().Contains(searchModel.SearchAll.ToLower()) ||
                      c.Phone.Contains(searchModel.SearchAll) || c.WorkPhone.Contains(searchModel.SearchAll) || c.Fax.Contains(searchModel.SearchAll) ||
                      c.ProfileTitle.ToLower().Contains(searchModel.SearchAll.ToLower()) ||
                      c.Skills.Contains(searchModel.SearchAll.ToLower()) ||
                      c.Country.ToLower().Contains(searchModel.SearchAll) || c.State.ToLower().Contains(searchModel.SearchAll) || c.City.ToLower().Contains(searchModel.SearchAll) || c.CurrentLocation.ToLower().Contains(searchModel.SearchAll) || c.PreferredLocation.ToLower().Contains(searchModel.SearchAll) ||
                      c.CompanyName.ToLower().Contains(searchModel.SearchAll.ToLower())
                    );

                //For mutliple matches
                string[] searchText = null;
                if (searchModel.SearchAll != null)
                {
                    searchText = searchModel.SearchAll.Split();
                    foreach (string word in searchText)
                    {
                        //Name
                        predicate = predicate | Builders<CandidateUserModel>.Filter.Where(
                        c => c.FirstName.ToLower().Contains(word.ToLower()) || c.MiddleName.ToLower().Contains(word.ToLower()) || c.LastName.ToLower().Contains(word.ToLower()));
                    }
                }
                
                //Check date
                DateTime dtCurrentDate = DateTime.Now;
                DateTime.TryParse(searchModel.SearchAll, out dtCurrentDate);
                if (dtCurrentDate.Year > 1900)
                    predicate = predicate | Builders<CandidateUserModel>.Filter.Where(c => c.AvailabilityDate.Contains(dtCurrentDate.ToString("yyyy-mm-dd")));

                //Experience years
                float experience = 0;
                float.TryParse(searchModel.SearchAll, out experience);
                if (experience > 0 && experience < 500)
                    predicate = predicate | (Builders<CandidateUserModel>.Filter.Where(c => c.ExperienceInYear == experience || c.ExperienceInMonth == experience || c.USExperienceInYear == experience || c.USExperienceInMonth == experience));

                
                TotalCount = (int)collection.Count(predicate);
                TotalPages = Math.Ceiling((double)TotalCount / pageSize);
                int skip = (pageNumber - 1) * pageSize;
                list = collection.Find(predicate).Sort(new BsonDocument("CreatedOn", -1)).Skip(skip).Limit(pageSize).ToList();
                
                return list;
            }

            /* Search with Not */
            if (!string.IsNullOrEmpty(searchModel.SearchNone))
            {
                var keywords = searchModel.SearchNone.Split(' ').ToArray();
                candidates = SearchWithNotOperator(searchModel.SearchIn, candidates, keywords);
            }

            /* Apply Filters */
            if (searchModel.filter != null)
            {
                candidates = CandidateFilterModel.ApplySearchFilter(candidates, searchModel.filter);
            }

            /* Assign Total Count & Pages */
            TotalCount = candidates.Count();
            TotalPages = Math.Ceiling((double)TotalCount / pageSize);

            candidates = isExport ? candidates.OrderBy(sortBy + " " + sortOrder).Take(1000) : candidates.OrderBy(sortBy + " " + sortOrder).Skip((pageNumber - 1) * pageSize).Take(pageSize);

            var result = CandidateUserModel.PopulateCandidateList(candidates);
            list = result != null ? result.ToList() : list;

            return list;
        }

        /// <summary>
        /// Build predicate with AND/ OR/ Not Operators
        /// </summary>
        /// <param name="searchModel">Candidate Advanced Search Model</param>
        /// <returns>Predicate</returns>
        private ExpressionStarter<CandidateUserModel> BuildPredicate(CandidateAdvancedSearchModel searchModel)
        {
            var predicate = PredicateBuilder.New<CandidateUserModel>();

            /* Search with OR Predicate */
            if (!string.IsNullOrEmpty(searchModel.SearchAll))
            {
                var keywords = searchModel.SearchAll.Split(',').ToArray();
                keywords = (from e in keywords select e.Trim()).ToArray();
                predicate.Extend(SearchWithOperator(searchModel.SearchIn, keywords, "OR"));
            }

            /* Search with AND Predicate */
            if (!string.IsNullOrEmpty(searchModel.SearchAll))
            {
                var keywords = searchModel.SearchAll.Split(' ').ToArray();
                predicate.Extend(SearchWithOperator(searchModel.SearchIn, keywords.ToArray(), "AND"));
            }

            /* Search with Exact Predicate */
            if (!string.IsNullOrEmpty(searchModel.SearchExact))
            {
                var keywords = searchModel.SearchExact.Split(' ').ToArray();
                predicate.Extend(SearchWithOperator(searchModel.SearchIn, keywords.ToArray(), "EQUAL"));
            }

            return predicate;
        }

        /// <summary>
        /// Build predicate with AND/ OR/ Not Operators
        /// </summary>
        /// <param name="searchModel">Candidate Advanced Search Model</param>
        /// <returns>Predicate</returns>
        private FilterDefinition<CandidateUserModel> BuildCollectionPredicate_WithOR(CandidateAdvancedSearchModel searchModel)
        {
            var predicate = Builders<CandidateUserModel>.Filter.Where(c => c.FirstName.Contains(searchModel.SearchAll));
            var keywords = searchModel.SearchAll.Split(' ').ToArray();

            foreach (string keyword in keywords)
            {
                string word = keyword.ToLower();
                foreach (var column in searchModel.SearchIn)
                {
                    switch (column)
                    {
                        case "CandidateId":
                            predicate = predicate | (Builders<CandidateUserModel>.Filter.Where(c => c.CandidateId.ToLower().Contains(column)));
                            break;
                        case "Name":
                            // predicate = predicate.Or(c => c.FirstName.ToLower().Contains(word) || c.MiddleName.ToLower().Contains(word) || c.LastName.ToLower().Contains(word) || c.DisplayName.ToLower().Contains(word));
                            string[] values = word.Split();
                            foreach (string wordpart in values)
                            {
                                predicate = predicate | (Builders<CandidateUserModel>.Filter.Where(c => c.FirstName.ToLower().Contains(wordpart) || c.MiddleName.ToLower().Contains(wordpart) || c.LastName.ToLower().Contains(wordpart) || c.DisplayName.ToLower().Contains(wordpart)));
                            }
                            break;
                        case "Email":
                            predicate = predicate | (Builders<CandidateUserModel>.Filter.Where(c => c.PrimaryEmail.ToLower().Contains(column) || c.SecondaryEmail.ToLower().Contains(column)));
                            break;
                        case "Contact":
                            predicate = predicate | (Builders<CandidateUserModel>.Filter.Where(c => c.Phone.ToLower().Contains(column) || c.WorkPhone.ToLower().Contains(column) || c.Fax.ToLower().Contains(column)));
                            break;
                        case "JobTitle":
                            predicate = predicate | (Builders<CandidateUserModel>.Filter.Where(c => c.ProfileTitle.ToLower().Contains(word)));
                            break;
                        case "Skills":
                            predicate = predicate | (Builders<CandidateUserModel>.Filter.Where(c => c.Skills.Any(s => s.ToLower().Contains(word))));
                            break;
                        case "Location":
                            predicate = predicate | (Builders<CandidateUserModel>.Filter.Where(c => c.Country.ToLower().Contains(word) || c.State.ToLower().Contains(word) || c.City.ToLower().Contains(word) || c.CurrentLocation.ToLower().Contains(word) || c.PreferredLocation.ToLower().Contains(word)));
                            break;
                        case "Availability":
                            predicate = predicate | (Builders<CandidateUserModel>.Filter.Where(c => c.AvailabilityDate.ToLower().Contains(word)));
                            break;
                        case "Experience":
                            int experience = 0;
                            int.TryParse(word, out experience);
                            if(experience > 0)
                                predicate = predicate | (Builders<CandidateUserModel>.Filter.Where(c => c.ExperienceInYear == experience || c.ExperienceInMonth == experience || c.USExperienceInYear == experience || c.USExperienceInMonth == experience));
                            break;
                        case "Company":
                            predicate = predicate | (Builders<CandidateUserModel>.Filter.Where(c => c.CompanyName.ToLower().Contains(word)));
                            break;
                    }
                }
            }

            return predicate;
            //return yourFilter;
        }



        #region Search With 'AND', 'OR' and 'EQUAL' Operator

        /// <summary>
        /// Search with operator like AND/ OR/ Equal
        /// </summary>
        /// <param name="columns">Columns to Search In</param>
        /// <param name="keywords">Search Keyword</param>
        /// <param name="Operator">Operator</param>
        /// <returns>ExpressionStarter<CandidateUserModel></returns>
        private System.Linq.Expressions.Expression<Func<CandidateUserModel, bool>> SearchWithOperator(List<string> columns, string[] keywords, string Operator)
        {
            var predicate = PredicateBuilder.New<CandidateUserModel>();

            foreach (var column in columns)
            {
                switch (column)
                {
                    case "CandidateId":
                        predicate.Extend(SearchInCandidateId(keywords, Operator));
                        break;
                    case "Name":
                        predicate.Extend(SearchInName(keywords, Operator));
                        break;
                    case "Email":
                        predicate.Extend(SearchInEmail(keywords, Operator));
                        break;
                    case "Contact":
                        predicate.Extend(SearchInContactNumber(keywords, Operator));
                        break;
                    case "JobTitle":
                        predicate.Extend(SearchInTitle(keywords, Operator));
                        break;
                    case "Skills":
                        predicate.Extend(SearchInSkills(keywords, Operator));
                        break;
                    case "Location":
                        predicate.Extend(SearchInLocation(keywords, Operator));
                        break;
                    case "Availability":
                        predicate.Extend(SearchInAvailability(keywords, Operator));
                        break;
                    case "Experience":
                        predicate.Extend(SearchInExperience(keywords, Operator));
                        break;
                    case "Company":
                        predicate.Extend(SearchInCompany(keywords, Operator));
                        break;
                }
            }

            return predicate;
        }

        /// <summary>
        /// Search keyword in Candidate Column
        /// </summary>
        /// <param name="keywords">Search Keyword</param>
        /// <param name="Operator">Operator</param>
        /// <returns>ExpressionStarter<CandidateUserModel></returns>
        private System.Linq.Expressions.Expression<Func<CandidateUserModel, bool>> SearchInCandidateId(string[] keywords, string Operator)
        {
            var predicate = PredicateBuilder.New<CandidateUserModel>();

            if (Operator == "AND")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.And(c => c.CandidateId.ToLower().Contains(word));
                }
            }
            else if (Operator == "OR")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.Start(c => c.CandidateId.ToLower().Contains(word));
                }
            }
            else if (Operator == "EQUAL")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.Or(c => c.CandidateId.ToLower() == word);
                }
            }

            return predicate;
        }

        /// <summary>
        /// Search keyword in Status Column
        /// </summary>
        /// <param name="keywords">Search Keyword</param>
        /// <param name="Operator">Operator</param>
        /// <returns>ExpressionStarter<CandidateUserModel></returns>
        private System.Linq.Expressions.Expression<Func<CandidateUserModel, bool>> SearchInStatus(string[] keywords, string Operator)
        {
            var predicate = PredicateBuilder.New<CandidateUserModel>();

            if (Operator == "AND")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.And(c => c.Status.ToLower().Contains(word));
                }
            }
            else if (Operator == "OR")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.Or(c => c.Status.ToLower().Contains(word));
                }
            }
            else if (Operator == "EQUAL")
            {
                string phrase = string.Join(" ", keywords);
                predicate = predicate.Or(c => c.Status.ToLower() == phrase.ToLower());

            }

            return predicate;
        }

        /// <summary>
        /// Search keyword in Job Title Column
        /// </summary>
        /// <param name="keywords">Search Keyword</param>
        /// <param name="Operator">Operator</param>
        /// <returns>ExpressionStarter<CandidateUserModel></returns>
        private System.Linq.Expressions.Expression<Func<CandidateUserModel, bool>> SearchInTitle(string[] keywords, string Operator)
        {
            var predicate = PredicateBuilder.New<CandidateUserModel>();

            if (Operator == "AND")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.And(c => c.ProfileTitle.ToLower().Contains(word));
                }
            }
            else if (Operator == "OR")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.Or(c => c.ProfileTitle.ToLower().Contains(word));
                }
            }
            else if (Operator == "EQUAL")
            {
                string phrase = string.Join(" ", keywords);
                predicate = predicate.Or(c => c.ProfileTitle.ToLower() == phrase.ToLower());
            }

            return predicate;
        }

        /// <summary>
        /// Search keyword in Company Column
        /// </summary>
        /// <param name="keywords">Search Keyword</param>
        /// <param name="Operator">Operator</param>
        /// <returns>ExpressionStarter<CandidateUserModel></returns>
        private System.Linq.Expressions.Expression<Func<CandidateUserModel, bool>> SearchInCompany(string[] keywords, string Operator)
        {
            var predicate = PredicateBuilder.New<CandidateUserModel>();

            if (Operator == "AND")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.And(c => c.CompanyName.ToLower().Contains(word));
                }
            }
            else if (Operator == "OR")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.Or(c => c.CompanyName.ToLower().Contains(word));
                }
            }
            else if (Operator == "EQUAL")
            {
                string phrase = string.Join(" ", keywords);
                predicate = predicate.Or(c => c.CompanyName.ToLower() == phrase.ToLower());
            }

            return predicate;
        }

        /// <summary>
        /// Search keyword in Created By Column
        /// </summary>
        /// <param name="keywords">Search Keyword</param>
        /// <param name="Operator">Operator</param>
        /// <returns>ExpressionStarter<CandidateUserModel></returns>
        private System.Linq.Expressions.Expression<Func<CandidateUserModel, bool>> SearchInCreatedBy(string[] keywords, string Operator)
        {
            var predicate = PredicateBuilder.New<CandidateUserModel>();

            if (Operator == "AND")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.And(c => c.CreatedByName.ToLower().Contains(word) || c.ModifiedByName.ToLower().Contains(word));
                }
            }
            else if (Operator == "OR")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.Or(c => c.CreatedByName.ToLower().Contains(word) || c.ModifiedByName.ToLower().Contains(word));
                }
            }
            else if (Operator == "EQUAL")
            {
                string phrase = string.Join(" ", keywords);
                predicate = predicate.Or(c => c.CreatedByName.ToLower() == phrase.ToLower() || c.ModifiedByName.ToLower() == phrase.ToLower());
            }

            return predicate;
        }

        /// <summary>
        /// Search keyword in First, Middle, Last and Nick Name Column
        /// </summary>
        /// <param name="keywords">Search Keyword</param>
        /// <param name="Operator">Operator</param>
        /// <returns>ExpressionStarter<CandidateUserModel></returns>
        private System.Linq.Expressions.Expression<Func<CandidateUserModel, bool>> SearchInName(string[] keywords, string Operator)
        {
            var predicate = PredicateBuilder.New<CandidateUserModel>();

            if (Operator == "AND")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.And(c => c.FirstName.ToLower().Contains(word) || c.MiddleName.ToLower().Contains(word) || c.LastName.ToLower().Contains(word) || c.DisplayName.ToLower().Contains(word));
                }
            }
            else if (Operator == "OR")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                   // predicate = predicate.Or(c => c.FirstName.ToLower().Contains(word) || c.MiddleName.ToLower().Contains(word) || c.LastName.ToLower().Contains(word) || c.DisplayName.ToLower().Contains(word));
                    string[] values = word.Split();
                    foreach (string value1 in values)
                    {
                        predicate = predicate.Or(c => c.FirstName.ToLower().Contains(value1) || c.MiddleName.ToLower().Contains(value1) || c.LastName.ToLower().Contains(value1) || c.DisplayName.ToLower().Contains(value1));
                    }
                }
            }
            else if (Operator == "EQUAL")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.Or(c => c.FirstName.ToLower() == word || c.MiddleName.ToLower() == word || c.LastName.ToLower() == word || c.DisplayName.ToLower() == word);
                }
            }

            return predicate;
        }

        /// <summary>
        /// Search keyword in Email Column
        /// </summary>
        /// <param name="keywords">Search Keyword</param>
        /// <param name="Operator">Operator</param>
        /// <returns>ExpressionStarter<CandidateUserModel></returns>
        private System.Linq.Expressions.Expression<Func<CandidateUserModel, bool>> SearchInEmail(string[] keywords, string Operator)
        {
            var predicate = PredicateBuilder.New<CandidateUserModel>();

            if (Operator == "AND")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.And(c => c.PrimaryEmail.ToLower().Contains(word) || c.SecondaryEmail.ToLower().Contains(word));
                }
            }
            else if (Operator == "OR")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.Or(c => c.PrimaryEmail.ToLower().Contains(word) || c.SecondaryEmail.ToLower().Contains(word));
                }
            }
            else if (Operator == "EQUAL")
            {
                string phrase = string.Join(" ", keywords);
                predicate = predicate.Or(c => c.PrimaryEmail.ToLower() == phrase.ToLower() || c.SecondaryEmail.ToLower() == phrase.ToLower());
            }

            return predicate;
        }

        /// <summary>
        /// Search keyword in Contact Number Column
        /// </summary>
        /// <param name="keywords">Search Keyword</param>
        /// <param name="Operator">Operator</param>
        /// <returns>ExpressionStarter<CandidateUserModel></returns>
        private System.Linq.Expressions.Expression<Func<CandidateUserModel, bool>> SearchInContactNumber(string[] keywords, string Operator)
        {
            var predicate = PredicateBuilder.New<CandidateUserModel>();

            if (Operator == "AND")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.And(c => c.Mobile.ToLower().Contains(word) || c.Phone.ToLower().Contains(word) || c.WorkPhone.ToLower().Contains(word) || c.Fax.ToLower().Contains(word));
                }
            }
            else if (Operator == "OR")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.Or(c => c.Mobile.ToLower().Contains(word) || c.Phone.ToLower().Contains(word) || c.WorkPhone.ToLower().Contains(word) || c.Fax.ToLower().Contains(word));
                }
            }
            else if (Operator == "EQUAL")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.Or(c => c.Mobile.ToLower() == word || c.Phone.ToLower() == word || c.WorkPhone.ToLower() == word || c.Fax.ToLower() == word);
                }
            }

            return predicate;
        }

        /// <summary>
        /// Search keyword in Technical Skills Column
        /// </summary>
        /// <param name="keywords">Search Keyword</param>
        /// <param name="Operator">Operator</param>
        /// <returns>ExpressionStarter<CandidateUserModel></returns>
        private System.Linq.Expressions.Expression<Func<CandidateUserModel, bool>> SearchInSkills(string[] keywords, string Operator)
        {
            var predicate = PredicateBuilder.New<CandidateUserModel>();

            if (Operator == "AND")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.And(c => c.Skills.Any(s => s.ToLower().Contains(word)));
                }
            }
            else if (Operator == "OR")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.Or(c => c.Skills.Any(s => s.ToLower().Contains(word)));
                }
            }
            else if (Operator == "EQUAL")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.Or(c => c.Skills.Any(s => s.ToLower() == word));
                }
            }

            return predicate;
        }

        /// <summary>
        /// Search keyword in Country, State, City, Current and Prefferred Location Column
        /// </summary>
        /// <param name="keywords">Search Keyword</param>
        /// <param name="Operator">Operator</param>
        /// <returns>ExpressionStarter<CandidateUserModel></returns>
        private System.Linq.Expressions.Expression<Func<CandidateUserModel, bool>> SearchInLocation(string[] keywords, string Operator)
        {
            var predicate = PredicateBuilder.New<CandidateUserModel>();

            if (Operator == "AND")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.And(c => c.Country.ToLower().Contains(word) || c.State.ToLower().Contains(word) || c.City.ToLower().Contains(word) || c.CurrentLocation.ToLower().Contains(word) || c.PreferredLocation.ToLower().Contains(word));
                }
            }
            else if (Operator == "OR")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.Or(c => c.Country.ToLower().Contains(word) || c.State.ToLower().Contains(word) || c.City.ToLower().Contains(word) || c.CurrentLocation.ToLower().Contains(word) || c.PreferredLocation.ToLower().Contains(word));
                }
            }
            else if (Operator == "EQUAL")
            {
                string phrase = string.Join(" ", keywords);
                predicate = predicate.Or(c => c.Country.ToLower() == phrase.ToLower() || c.State.ToLower() == phrase.ToLower() || c.City.ToLower() == phrase.ToLower() || c.CurrentLocation.ToLower() == phrase.ToLower() || c.PreferredLocation.ToLower() == phrase.ToLower());
            }

            return predicate;
        }

        /// <summary>
        /// Search keyword in Availability Column
        /// </summary>
        /// <param name="keywords">Search Keyword</param>
        /// <param name="Operator">Operator</param>
        /// <returns>ExpressionStarter<CandidateUserModel></returns>
        private System.Linq.Expressions.Expression<Func<CandidateUserModel, bool>> SearchInAvailability(string[] keywords, string Operator)
        {
            var predicate = PredicateBuilder.New<CandidateUserModel>();

            if (Operator == "AND")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.And(c => c.AvailabilityDate.ToLower().Contains(word));
                }
            }
            else if (Operator == "OR")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.Or(c => c.AvailabilityDate.ToLower().Contains(word));
                }
            }
            else if (Operator == "EQUAL")
            {
                string phrase = string.Join(" ", keywords);
                predicate = predicate.Or(c => c.AvailabilityDate.ToLower() == phrase.ToLower());
            }

            return predicate;
        }

        /// <summary>
        /// Search keyword in Total and Relevant Experience Column
        /// </summary>
        /// <param name="keywords">Search Keyword</param>
        /// <param name="Operator">Operator</param>
        /// <returns>ExpressionStarter<CandidateUserModel></returns>
        private System.Linq.Expressions.Expression<Func<CandidateUserModel, bool>> SearchInExperience(string[] keywords, string Operator)
        {
            var predicate = PredicateBuilder.New<CandidateUserModel>();

            if (Operator == "AND")
            {
                foreach (string keyword in keywords)
                {
                    int word;
                    if (Int32.TryParse(keyword, out word))
                    {                        
                        predicate = predicate.And(c => c.ExperienceInYear == word || c.ExperienceInMonth == word || c.USExperienceInYear == word || c.USExperienceInMonth == word);                        
                    }
                }
            }
            else if (Operator == "OR")
            {
                foreach (string keyword in keywords)
                {
                    int word;
                    if (Int32.TryParse(keyword, out word))
                    {
                        predicate = predicate.Or(c => c.ExperienceInYear == word || c.ExperienceInMonth == word || c.USExperienceInYear == word || c.USExperienceInMonth == word);
                    }
                }
            }
            else if (Operator == "EQUAL")
            {
                string phrase = string.Join(" ", keywords);
                int word;
                if (Int32.TryParse(phrase, out word))
                {
                    predicate = predicate.Or(c => c.ExperienceInYear == word || c.ExperienceInMonth == word || c.USExperienceInYear == word || c.USExperienceInMonth == word);
                }
            }

            return predicate;
        }

        #endregion

        #region Search With 'Not' Operator

        /// <summary>
        /// Search With Not Operator
        /// </summary>
        /// <param name="searchIn">Columns to Search In</param>
        /// <param name="candidates">IQueryable</param>
        /// <param name="keywords">Search Keyword</param>
        /// <returns>IQueryable</returns>
        private IQueryable<CandidateUserModel> SearchWithNotOperator(List<string> searchIn, IQueryable<CandidateUserModel> candidates, string[] keywords)
        {
            foreach (var column in searchIn)
            {
                switch (column)
                {
                    case "CandidateId":
                        candidates = SearchNotWithCandidateId(candidates, keywords);
                        break;
                    case "Status":
                        candidates = SearchNotWithStatus(candidates, keywords);
                        break;
                    case "Name":
                        candidates = SearchNotWithName(candidates, keywords);
                        break;
                    case "Email":
                        candidates = SearchNotWithEmail(candidates, keywords);
                        break;
                    case "Contact":
                        candidates = SearchNotWithContactNumber(candidates, keywords);
                        break;
                    case "JobTitle":
                        candidates = SearchNotWithTitle(candidates, keywords);
                        break;
                    case "Skills":
                        candidates = SearchNotWithSkills(candidates, keywords);
                        break;
                    case "Location":
                        candidates = SearchNotWithLocation(candidates, keywords);
                        break;
                    case "Availability":
                        candidates = SearchNotWithAvailability(candidates, keywords);
                        break;
                    case "Experience":
                        candidates = SearchNotWithExperience(candidates, keywords);
                        break;
                    case "Company":
                        candidates = SearchNotWithCompany(candidates, keywords);
                        break;
                    case "CreatedBy":
                        candidates = SearchNotWithCreatedBy(candidates, keywords);
                        break;
                }
            }

            return candidates;
        }

        /// <summary>
        /// Search With Not Operator in Candidate Id column
        /// </summary>
        /// <param name="jobs">IQueryable<JobDetail></param>
        /// <param name="keywords">Search Keyword</param>
        /// <returns>IQueryable<JobDetail></returns>
        private IQueryable<CandidateUserModel> SearchNotWithCandidateId(IQueryable<CandidateUserModel> candidates, string[] keywords)
        {
            foreach (string keyword in keywords)
            {
                string word = keyword.ToLower();
                candidates = candidates.Where(c => !c.CandidateId.ToLower().Contains(word));
            }

            return candidates;
        }

        /// <summary>
        /// Search With Not Operator in Status column
        /// </summary>
        /// <param name="jobs">IQueryable<JobDetail></param>
        /// <param name="keywords">Search Keyword</param>
        /// <returns>IQueryable<JobDetail></returns>
        private IQueryable<CandidateUserModel> SearchNotWithStatus(IQueryable<CandidateUserModel> candidates, string[] keywords)
        {
            foreach (string keyword in keywords)
            {
                string word = keyword.ToLower();
                candidates = candidates.Where(c => !c.Status.ToLower().Contains(word));
            }

            return candidates;
        }

        /// <summary>
        /// Search With Not Operator in Title column
        /// </summary>
        /// <param name="jobs">IQueryable<JobDetail></param>
        /// <param name="keywords">Search Keyword</param>
        /// <returns>IQueryable<JobDetail></returns>
        private IQueryable<CandidateUserModel> SearchNotWithTitle(IQueryable<CandidateUserModel> candidates, string[] keywords)
        {
            foreach (string keyword in keywords)
            {
                string word = keyword.ToLower();
                candidates = candidates.Where(c => !c.ProfileTitle.ToLower().Contains(word));
            }

            return candidates;
        }

        /// <summary>
        /// Search With Not Operator in Company column
        /// </summary>
        /// <param name="jobs">IQueryable<JobDetail></param>
        /// <param name="keywords">Search Keyword</param>
        /// <returns>IQueryable<JobDetail></returns>
        private IQueryable<CandidateUserModel> SearchNotWithCompany(IQueryable<CandidateUserModel> candidates, string[] keywords)
        {
            foreach (string keyword in keywords)
            {
                string word = keyword.ToLower();
                candidates = candidates.Where(c => !c.CompanyName.ToLower().Contains(word));
            }

            return candidates;
        }

        /// <summary>
        /// Search With Not Operator in Created By column
        /// </summary>
        /// <param name="jobs">IQueryable<JobDetail></param>
        /// <param name="keywords">Search Keyword</param>
        /// <returns>IQueryable<JobDetail></returns>
        private IQueryable<CandidateUserModel> SearchNotWithCreatedBy(IQueryable<CandidateUserModel> candidates, string[] keywords)
        {
            foreach (string keyword in keywords)
            {
                string word = keyword.ToLower();
                candidates = candidates.Where(c => !c.CreatedByName.ToLower().Contains(word) && !c.ModifiedByName.ToLower().Contains(word));
            }

            return candidates;
        }

        /// <summary>
        /// Search With Not Operator in First, Middle, Last and Display Name column
        /// </summary>
        /// <param name="jobs">IQueryable<JobDetail></param>
        /// <param name="keywords">Search Keyword</param>
        /// <returns>IQueryable<JobDetail></returns>
        private IQueryable<CandidateUserModel> SearchNotWithName(IQueryable<CandidateUserModel> candidates, string[] keywords)
        {
            foreach (string keyword in keywords)
            {
                string word = keyword.ToLower();
                candidates = candidates.Where(c => !c.FirstName.ToLower().Contains(word) && !c.MiddleName.ToLower().Contains(word) && !c.LastName.ToLower().Contains(word) && !c.DisplayName.ToLower().Contains(word));
            }

            return candidates;
        }

        /// <summary>
        /// Search With Not Operator in Primary and Secondary Email column
        /// </summary>
        /// <param name="jobs">IQueryable<JobDetail></param>
        /// <param name="keywords">Search Keyword</param>
        /// <returns>IQueryable<JobDetail></returns>
        private IQueryable<CandidateUserModel> SearchNotWithEmail(IQueryable<CandidateUserModel> candidates, string[] keywords)
        {
            foreach (string keyword in keywords)
            {
                string word = keyword.ToLower();
                candidates = candidates.Where(c => !c.PrimaryEmail.ToLower().Contains(word) && !c.SecondaryEmail.ToLower().Contains(word));
            }

            return candidates;
        }

        /// <summary>
        /// Search With Not Operator in Primary and Secondary Email column
        /// </summary>
        /// <param name="jobs">IQueryable<JobDetail></param>
        /// <param name="keywords">Search Keyword</param>
        /// <returns>IQueryable<JobDetail></returns>
        private IQueryable<CandidateUserModel> SearchNotWithContactNumber(IQueryable<CandidateUserModel> candidates, string[] keywords)
        {
            foreach (string keyword in keywords)
            {
                string word = keyword.ToLower();
                candidates = candidates.Where(c => !c.Mobile.ToLower().Contains(word) && !c.Phone.ToLower().Contains(word) && !c.WorkPhone.ToLower().Contains(word) && !c.Fax.ToLower().Contains(word));
            }

            return candidates;
        }

        /// <summary>
        /// Search With Not Operator in Technical Skills column
        /// </summary>
        /// <param name="jobs">IQueryable<JobDetail></param>
        /// <param name="keywords">Search Keyword</param>
        /// <returns>IQueryable<JobDetail></returns>
        private IQueryable<CandidateUserModel> SearchNotWithSkills(IQueryable<CandidateUserModel> candidates, string[] keywords)
        {
            foreach (string keyword in keywords)
            {
                string word = keyword.ToLower();
                candidates = candidates.Where(c => !c.Skills.Any(s => s.ToLower().Contains(word)));
            }

            return candidates;
        }

        /// <summary>
        /// Search With Not Operator in Country, State, City, Current and Preffered Location column
        /// </summary>
        /// <param name="jobs">IQueryable<JobDetail></param>
        /// <param name="keywords">Search Keyword</param>
        /// <returns>IQueryable<JobDetail></returns>
        private IQueryable<CandidateUserModel> SearchNotWithLocation(IQueryable<CandidateUserModel> candidates, string[] keywords)
        {
            foreach (string keyword in keywords)
            {
                string word = keyword.ToLower();
                candidates = candidates.Where(c => !c.Country.ToLower().Contains(word) && !c.State.ToLower().Contains(word) && !c.City.ToLower().Contains(word) && !c.CurrentLocation.ToLower().Contains(word) && !c.PreferredLocation.ToLower().Contains(word));
            }

            return candidates;
        }

        /// <summary>
        /// Search With Not Operator in Availability column
        /// </summary>
        /// <param name="jobs">IQueryable<JobDetail></param>
        /// <param name="keywords">Search Keyword</param>
        /// <returns>IQueryable<JobDetail></returns>
        private IQueryable<CandidateUserModel> SearchNotWithAvailability(IQueryable<CandidateUserModel> candidates, string[] keywords)
        {
            foreach (string keyword in keywords)
            {
                string word = keyword.ToLower();
                candidates = candidates.Where(c => !c.AvailabilityDate.ToLower().Contains(word));
            }

            return candidates;
        }

        /// <summary>
        /// Search With Not Operator in Total and Relevant Experience column
        /// </summary>
        /// <param name="jobs">IQueryable<JobDetail></param>
        /// <param name="keywords">Search Keyword</param>
        /// <returns>IQueryable<JobDetail></returns>
        private IQueryable<CandidateUserModel> SearchNotWithExperience(IQueryable<CandidateUserModel> candidates, string[] keywords)
        {
            foreach (string keyword in keywords)
            {
                int word;
                if (Int32.TryParse(keyword, out word))
                {
                    candidates = candidates.Where(c => c.ExperienceInYear != word && c.ExperienceInMonth != word && c.USExperienceInYear != word && c.USExperienceInMonth != word);
                }
            }

            return candidates;
        }

        #endregion

        #endregion

        #endregion
    }
}
