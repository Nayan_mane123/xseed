﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Submission;

namespace XSeed.Data.ViewModel.AdvancedSearch
{
    public class SubmissionFilterModel
    {
        public List<Nullable<Guid>> OrganizationUser { get; set; }
        public List<Nullable<Guid>> Company { get; set; }
        public List<string> JobTitle { get; set; }

        #region User Defined Functions

        /// <summary>
        /// Apply Search Filter
        /// </summary>
        /// <param name="submissions">IQueryable</param>
        /// <param name="filterModel">Submission Filter Model</param>
        /// <returns>IQueryable</returns>
        internal static IQueryable<SubmissionDetail> ApplySearchFilter(IQueryable<SubmissionDetail> submissions, SubmissionFilterModel filterModel)
        {
            /* Apply Created By filter */
            if (filterModel.OrganizationUser != null && filterModel.OrganizationUser.Count > 0)
            {
                using (var db = new XSeedEntities())
                {
                    var createdByList = db.OrganizationUserDetails.Where(u => filterModel.OrganizationUser.Contains(u.Id)).Select(u => u.UserId.ToString()).ToList();
                    submissions = submissions.Where(s => createdByList.Contains(s.CreatedBy.ToString()));
                }
            }

            /* Apply company filter */
            if (filterModel.Company != null && filterModel.Company.Count > 0)
            {
                using (var db = new XSeedEntities())
                {
                    submissions = submissions.Where(s => filterModel.Company.Contains(s.JobDetail.CompanyId));
                }
            }

            /* Apply job title filter */
            if (filterModel.JobTitle != null && filterModel.JobTitle.Count > 0)
            {
                using (var db = new XSeedEntities())
                {
                    submissions = submissions.Where(s => filterModel.JobTitle.Contains(s.JobDetail.JobTitle));
                }
            }

            return submissions;
        }

        #endregion
    }
}
