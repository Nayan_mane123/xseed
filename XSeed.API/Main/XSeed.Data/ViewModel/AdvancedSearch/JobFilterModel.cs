﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;

namespace XSeed.Data.ViewModel.AdvancedSearch
{
    public class JobFilterModel
    {
        #region Property Declaration

        public Nullable<Guid> OrganizationId { get; set; }
        public Nullable<Guid> OrganizationUserId { get; set; }
        public string RequirementType { get; set; }
        public List<Nullable<Guid>> Status { get; set; }
        public List<Nullable<Guid>> Type { get; set; }
        public List<Nullable<Guid>> Company { get; set; }
        public List<Nullable<Guid>> OrganizationUser { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Apply Search Filter
        /// </summary>
        /// <param name="jobs">IQueryable<JobDetail></param>
        /// <param name="jobFilterModel">Job Filter Model</param>
        /// <returns>IQueryable<JobDetail></returns>
        internal static IQueryable<JobDetail> ApplySearchFilter(IQueryable<JobDetail> jobs, JobFilterModel jobFilterModel)
        {
            /* Apply Req Type filter like "My Open/ Closed/ Hold" */
            if (!string.IsNullOrEmpty(jobFilterModel.RequirementType))
            {
                if (jobFilterModel.RequirementType == "All")
                {
                    jobs = jobs.Where(j => j.CompanyDetail.OrganizationId == jobFilterModel.OrganizationId && j.OrganizationUserDetails.Any(u => u.UserId == jobFilterModel.OrganizationUserId));
                }
                else
                {
                    jobs = jobs.Where(j => j.CompanyDetail.OrganizationId == jobFilterModel.OrganizationId && j.OrganizationUserDetails.Any(u => u.UserId == jobFilterModel.OrganizationUserId) && j.JobStatusMaster.Status == jobFilterModel.RequirementType);
                }
            }

            /* Apply Req status filter like "Open/ Closed/ Hold" */
            if (jobFilterModel.Status != null && jobFilterModel.Status.Count > 0)
            {
                jobs = jobs.Where(j => jobFilterModel.Status.Contains(j.JobStatusId));
            }

            /* Apply Req type filter like "Contract/ Permanent" */
            if (jobFilterModel.Type != null && jobFilterModel.Type.Count > 0)
            {
                jobs = jobs.Where(j => jobFilterModel.Type.Contains(j.JobTypeId));
            }

            /* Apply company name filter */
            if (jobFilterModel.Company != null && jobFilterModel.Company.Count > 0)
            {
                jobs = jobs.Where(j => jobFilterModel.Company.Contains(j.CompanyId));
            }

            /* Apply assigned recruiter filter */            
            if (jobFilterModel.OrganizationUser != null && jobFilterModel.OrganizationUser.Count > 0)
            {                
                 jobs = jobs.Where(j => j.OrganizationUserDetails.Any(u => jobFilterModel.OrganizationUser.Contains(u.Id)));
            }

            return jobs;
        }

        #endregion
    }
}
