﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Common;

namespace XSeed.Data.ViewModel.Company
{
    public class CompanyDataModel
    {
        static string url = ConfigurationManager.AppSettings["MongoURL"].ToString();
        static string dbName = ConfigurationManager.AppSettings["MongoDB"].ToString();

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string website { get; set; }
        public string wikidataLink { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string logoLink { get; set; }
        public string employeeCount { get; set; }
        public string dataSource { get; set; }
        public string foundedDate { get; set; }
        public string headquarter { get; set; }
        public string streetAddress { get; set; }
        public string postalCode { get; set; }
        public string country { get; set; }
        public string googleDescription { get; set; }
        public string googleDetailedDescription { get; set; }
        public string googleWebsite { get; set; }
        public string googleWikipediaPage { get; set; }
        public string googleImageLink { get; set; }
        public string googleResultScore { get; set; }
        public string linkedInUrl { get; set; }
        public string industry { get; set; }
        public string linkedInFollowerCount { get; set; }
        public string speciality { get; set; }
        public string type { get; set; }
        public string locality { get; set; }
        public string region { get; set; }
        public string LogoImagePath { get; set; }


        /// <summary>
        /// Search Company Information.
        /// </summary>
        /// <param name="searchText">searchText</param>
        /// <returns>List<CompanyDataModel></returns>
        public static List<CompanyDataModel> SearchCompanyData(string searchText)
        {
            MongoClient client = new MongoClient(url);
            var database = client.GetDatabase(dbName);

            var builder = Builders<CompanyDataModel>.Filter;
            var filt = Builders<CompanyDataModel>.Filter.Empty;

            filt = builder.Regex("name", new BsonRegularExpression(searchText, ".*" + searchText + ".*'i"));

            var collection = database.GetCollection<CompanyDataModel>("CompanyData");
            var result = collection.Find(filt).Limit(10).ToList();

            return result;
        }

        /// <summary>
        /// Save Company Logo
        /// </summary>
        public void SaveCompanyLogo()
        {
            MongoClient client = new MongoClient(url);
            var database = client.GetDatabase(dbName);
            var collection = database.GetCollection<CompanyDataModel>("CompanyData");

            var LogoFilter = Builders<CompanyDataModel>.Filter.Where(c => string.IsNullOrEmpty(c.LogoImagePath));
            var companies = collection.Find(LogoFilter).ToList();

            foreach (var company in companies)
            {
                string logoURL = company.logoLink;

                if (!string.IsNullOrEmpty(logoURL))
                {
                    string linkedInId = company.linkedInUrl.Split('/').Last();
                    XSeedFileEntity.SaveImageFromURL(linkedInId, logoURL);

                    if (!string.IsNullOrEmpty(linkedInId))
                    {
                        var filter = Builders<CompanyDataModel>.Filter.Eq("_id", ObjectId.Parse(company._id));
                        company.LogoImagePath = linkedInId + ".jpg";
                        collection.ReplaceOne(filter, company);
                    }
                }
            }
        }
    }
}
