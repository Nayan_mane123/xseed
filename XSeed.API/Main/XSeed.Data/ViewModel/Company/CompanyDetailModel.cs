﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using XSeed.Utility;

namespace XSeed.Data.ViewModel.Company
{
    public class CompanyDetailModel
    {
        #region Property Declaration

        public Guid Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Organization), ErrorMessageResourceName = "OrgnizationIdRequired")]
        public Nullable<Guid> OrganizationId { get; set; }
        public string OrganizationName { get; set; }

        public Nullable<Guid> CompanyTypeId { get; set; }
        public string CompanyType { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Company), ErrorMessageResourceName = "CompanyNameRequired")]
        [Display(Name = "Company Name")]
        public string Name { get; set; }

        public string Description { get; set; }
        public string Website { get; set; }
        public string CareerPageURL { get; set; }
        public string LinkedInURL { get; set; }
        public string TwitterURL { get; set; }
        public string GooglePlusURL { get; set; }
        public string FacebookURL { get; set; }
        public string ProfileImage { get; set; }
        public XSeedFileEntity ProfileImageFile { get; set; }
        public string Size { get; set; }

        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public Nullable<Guid> CountryId { get; set; }
        public string Country { get; set; }
        public Nullable<Guid> StateId { get; set; }
        public string State { get; set; }
        public Nullable<Guid> CityId { get; set; }
        public string City { get; set; }

        [System.ComponentModel.DefaultValue(""), RegularExpression(Constants.ZipExpression, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "ZipLength")]
        public string Zip { get; set; }
        public Nullable<Guid> CompanySourceId { get; set; }
        public string CompanySourceName { get; set; }
        public string Via { get; set; }
        public string ViaWebsite { get; set; }
        public string SubmissionPageURL { get; set; }
        public List<ChildLookUpModel> IndustryTypeList { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get list of companies belongs to an organization
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <returns>Company Detail Model List</returns>
        public List<CompanyDetailModel> ListAllCompanies(Guid organizationId, int pageSize, int pageNumber, string sortBy = "ModifiedOn", string sortOrder = "desc")
        {
            return CompanyDetail.ListOrganizationCompanies(organizationId, pageSize, pageNumber, sortBy, sortOrder);
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        public static PaginationModel GetPaginationInfo(Guid organizationId, int pageSize)
        {
            return CompanyDetail.GetPaginationInfo(organizationId, pageSize);
        }

        /// <summary>
        /// Get Company Detail
        /// </summary>        
        /// <param name="companyId">Company Id</param>
        /// <returns>Company Detail Model</returns>
        public CompanyDetailModel GetCompanyDetail(Guid organizationId, Guid? companyId)
        {
            return CompanyDetail.GetCompanyDetail(organizationId, companyId);
        }

        /// <summary>
        /// Create New Company Detail
        /// </summary>
        /// <param name="companyDetailModel">Company Detail Model </param>
        public Guid CreateCompanyDetail(CompanyDetailModel companyDetailModel)
        {
            return CompanyDetail.SaveCompanyDetail(companyDetailModel);
        }

        /// <summary>
        /// Update New Company Detail
        /// </summary>
        /// <param name="companyDetailModel">Company Detail Model </param>
        public void UpdateCompanyDetail(CompanyDetailModel companyDetailModel)
        {
            CompanyDetail.SaveCompanyDetail(companyDetailModel);
        }

        /// <summary>
        /// Create New Company Source
        /// </summary>
        /// <param name="lookUpModel">LookUpModel </param>
        public static Guid CreateCompanySource(LookUpModel lookUpModel)
        {
            return CompanySource.CreateCompanySource(lookUpModel);
        }

        /// <summary>
        /// Update Company Source
        /// </summary>
        /// <param name="lookUpModel"></param>
        /// <param name="organizationId"></param>
        public static void UpdateCompanySource(LookUpModel lookUpModel)
        {
            CompanySource.UpdateCompanySource(lookUpModel);
        }

        /// <summary>
        /// Delete Company Source
        /// </summary>
        /// <param name="genericLookupId"></param>
        public static void DeleteCompanySource(Guid genericLookupId)
        {
            CompanySource.DeleteCompanySource(genericLookupId);
        }

        #endregion





    }
}
