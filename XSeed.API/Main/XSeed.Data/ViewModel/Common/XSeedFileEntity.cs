﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Drawing;
using XSeed.Utility;

namespace XSeed.Data.ViewModel.Common
{
    public class XSeedFileEntity
    {
        #region Property Declaration

        public string lastModified { get; set; }
        public string lastModifiedDate { get; set; }
        public string name { get; set; }
        public string size { get; set; }
        public string type { get; set; }
        public string data { get; set; }
        public string filePath { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Save Profile Image
        /// </summary>
        /// <param name="fileEntity">XSeed File Entity</param>
        /// <param name="request">Request Type</param>
        /// <returns>File Name</returns>
        public static string SaveProfileImage(XSeedFileEntity fileEntity, string request)
        {
            string fileName = string.Empty;

            if (fileEntity != null)
            {
                /* Rename file & Update Model */
                XSeedFileEntity file = fileEntity;
                fileName = FileUtility.RenameFile(file.name);

                /* Save profile image */
                FileUtility.SaveImageFile(fileName, file.type, file.data, request, file.name);
            }
            return fileName;
        }

        /// <summary>
        /// Save candidate resume file
        /// </summary>
        /// <param name="resume">XSeed File Entity</param>
        /// <param name="request">Request Type</param>
        /// <returns>File Name</returns>
        public static string SaveResumeFile(XSeedFileEntity resume, string request)
        {
            string fileName = string.Empty;

            if (resume != null)
            {
                /* Rename file & Update Model */
                XSeedFileEntity file = resume;
                fileName = FileUtility.RenameFile(file.name);

                /* Save Resume */
                fileName = FileUtility.SaveResumeFile(fileName, file.type, file.data, request);

                if (request == "ParseResume" && file.type == "application/msword")
                {
                    /* Save Document (.doc) as PDF */
                    string pdfName = FileUtility.SaveDocumentAsPDF(fileName);

                    /* Delete Resume File */
                    XSeedFileEntity.DeleteResumeFile(fileName);

                    return pdfName;
                }
            }

            return fileName;
        }


        /// <summary>
        /// Delete candidate resume file after parsing 
        /// </summary>
        /// <param name="fileName">File Name</param>
        public static void DeleteResumeFile(string fileName)
        {
            /* Delete profile image */
            FileUtility.DeleteResumeFile(fileName);
        }

        /// <summary>
        /// Save Image File From URL
        /// </summary>
        /// <param name="companyName">companyName</param>
        /// <param name="logoURL">logoURL</param>
        /// <returns>File Name</returns>
        internal static void SaveImageFromURL(string companyName, string logoURL)
        {
            FileUtility.SaveImageFileFromURL(companyName, logoURL, "CompanyData");
        }

        /// <summary>
        /// Convert resume file to PDF format
        /// </summary>
        /// <param name="path">file path</param>
        /// <returns>PDF file path</returns>
        internal static string ConvertToPDF(string path)
        {
            return FileUtility.ConvertToPDF(path);
        }

        /// <summary>
        /// Resume Bulk Upload
        /// </summary>
        /// <param name="Resumes">List of Resumes</param>
        /// <param name="request">request-entity name</param>
        /// <returns>Count of uploaded files</returns>
        public static int SaveResumeUploadFiles(List<XSeedFileEntity> Resumes, string request)
        {
            string fileName = string.Empty;
            int uploadCount = 0;

            if (Resumes != null)
            {
                foreach (var resumeFile in Resumes)
                {
                    /* Rename file & Update Model */
                    XSeedFileEntity file = resumeFile;
                    fileName = FileUtility.RenameFile(file.name);

                    /* Save Resume */
                    fileName = FileUtility.SaveResumeFile(fileName, file.type, file.data, request);

                    if (fileName != null)
                    {
                        uploadCount++;
                    }
                }
            }
            return uploadCount;
        }

        #endregion
    }
}
