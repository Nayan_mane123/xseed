﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Utility;

namespace XSeed.Data.ViewModel.Common
{
    public class LookUpModel
    {
        #region Property Declaration

        public Nullable<Guid> Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<Guid> OrganizationId { get; set; }
        public string RoleName { get; set; } 
        public string Email {get; set;}

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Country List
        /// </summary>
        /// <returns>Country List</returns>
        public static List<LookUpModel> GetCountries()
        {
            using (var db = new XSeedEntities())
            {
                return db.CountryMasters.Select(x => new LookUpModel { Id = x.Id, Name = x.Name }).ToList();
            }
        }

        /// <summary>
        /// Get State List by Country Id
        /// </summary>
        /// <param name="countryId">Country Id</param>
        /// <returns>State List</returns>
        public static List<LookUpModel> GetStates(Guid countryId)
        {
            using (var db = new XSeedEntities())
            {
                return db.StateMasters.Where(c => c.CountryId == countryId).Select(x => new LookUpModel { Id = x.Id, Name = x.Name }).ToList();
            }
        }

        /// <summary>
        /// Get City List by State Id
        /// </summary>
        /// <param name="stateId">State Id</param>
        /// <returns>State List</returns>
        public static List<LookUpModel> GetCities(Guid stateId)
        {
            using (var db = new XSeedEntities())
            {
                return db.CityMasters.Where(c => c.StateId == stateId).Select(x => new LookUpModel { Id = x.Id, Name = x.Name }).ToList();
            }
        }

        /// <summary>
        /// Get User Roles List
        /// </summary>        
        /// <returns>User Roles List</returns>
        public static List<LookUpModel> GetUserRoles(Guid organizationId)
        {
            using (var db = new XSeedEntities())
            {
                return db.UserRoleMasters.Where(c => c.OrganizationId == organizationId).Select(x => new LookUpModel { Id = x.Id, Name = x.Role }).ToList();
            }
        }

        /// <summary>
        /// Get Company Types
        /// </summary>        
        /// <returns>Company Types</returns>
        public static List<LookUpModel> GetCompanyTypes()
        {
            using (var db = new XSeedEntities())
            {
                return db.CompanyTypeMasters.Select(x => new LookUpModel { Id = x.Id, Name = x.Name }).ToList();
            }
        }

        /// <summary>
        /// Get Company Sources
        /// </summary>        
        /// <returns>Company Sources</returns>
        public static List<LookUpModel> GetCompanySources(Guid organizationId)
        {
            using (var db = new XSeedEntities())
            {
                return db.CompanySources.Where(c => c.OrganizationId == organizationId).OrderByDescending(b => b.CreatedOn).Select(x => new LookUpModel { Id = x.Id, Name = x.Name, Description = x.Description, OrganizationId = x.OrganizationId }).ToList();
            }
        }

        /// <summary>
        /// Get Title/ Suffix List
        /// </summary>
        /// <returns>Title/ Suffix List</returns>
        public static List<LookUpModel> GetTitles()
        {
            using (var db = new XSeedEntities())
            {
                return db.TitleMasters.Select(x => new LookUpModel { Id = x.Id, Name = x.Name }).ToList();
            }
        }

        /// <summary>
        /// Get Reporting Authorities
        /// </summary>
        /// <returns>Reporting Authorities</returns>
        public static List<LookUpModel> GetReportingAuthorities(Guid organizationId, Guid? roleId)
        {
            using (var db = new XSeedEntities())
            {
                if (roleId != null)
                {
                    int precedenceLevel = Convert.ToInt32(db.UserRoleMasters
                        .Where(x => x.Id == roleId)
                        .Select(x => x.Precedence)
                        .FirstOrDefault());

                    return (from o in db.OrganizationUserDetails
                            join r in db.UserRoleMasters on o.RoleId equals r.Id
                            where o.OrganizationId == organizationId && r.Precedence < precedenceLevel
                            select (new LookUpModel { Id = o.Id, Name = o.FirstName + " " + o.LastName })
                                    ).ToList();
                }

                return db.OrganizationUserDetails
                         .Where(o => o.OrganizationId == organizationId)
                         .Select(x => new LookUpModel { Id = x.Id, Name = x.FirstName + " " + x.LastName }).ToList();
            }
        }

        /// <summary>
        /// Get Job Status
        /// </summary>
        /// <returns>List of Job Status</returns>
        public static List<LookUpModel> GetJobStatus()
        {
            using (var db = new XSeedEntities())
            {
                return db.JobStatusMasters.Where(s => s.IsActive == Constants.IsActiveTrue).Select(x => new LookUpModel { Id = x.Id, Name = x.Status }).ToList();
            }
        }


        /// <summary>
        /// Get Job Priority
        /// </summary>
        /// <returns>List of Job Priority</returns>
        public static List<LookUpModel> GetJobPriority(Boolean isMobileRequest = false)
        {
            using (var db = new XSeedEntities())
            {
                return db.JobPriorities.Select(x => new LookUpModel { Id = x.Id, Name = x.Priority }).ToList();
            }
        }

        /// <summary>
        /// Get Job Title
        /// </summary>
        /// <returns>List of Job Titles</returns>
        public static List<LookUpModel> GetJobTitles(Guid organizationId)
        {
            using (var db = new XSeedEntities())
            {
                return db.JobTitleMasters.Where(s => s.OrganizationId == organizationId && s.IsActive == Constants.IsActiveTrue).OrderByDescending(b => b.CreatedOn).Select(x => new LookUpModel { Id = x.Id, Name = x.Title, Description = x.Description, OrganizationId = x.OrganizationId }).ToList();
            }
        }

        /// <summary>
        /// Get Job Type
        /// </summary>
        /// <returns>List of Job Types</returns>
        public static List<LookUpModel> GetJobTypes(Guid organizationId)
        {
            using (var db = new XSeedEntities())
            {
                return db.JobTypeMasters.Where(s => s.OrganizationId == organizationId && s.IsActive == Constants.IsActiveTrue).OrderByDescending(b => b.CreatedOn).Select(x => new LookUpModel { Id = x.Id, Name = x.Type, Description = x.Description, OrganizationId = x.OrganizationId }).ToList();
            }
        }

        /// <summary>
        /// Get Degrees
        /// </summary>
        /// <returns>List of Degrees</returns>
        public static List<LookUpModel> GetDegrees(Guid organizationId)
        {
            using (var db = new XSeedEntities())
            {
                return db.DegreeMasters.Where(s => s.OrganizationId == organizationId && s.IsActive == true).OrderByDescending(b => b.CreatedOn).Select(x => new LookUpModel { Id = x.Id, Name = x.Name, Description = x.Description, OrganizationId = x.OrganizationId }).ToList();
            }
        }

        /// <summary>
        /// Get Genders
        /// </summary>
        /// <returns>List of Genders</returns>
        public static List<LookUpModel> GetGenders()
        {
            using (var db = new XSeedEntities())
            {
                return db.GenderMasters.Where(s => s.IsActive == Constants.IsActiveTrue).Select(x => new LookUpModel { Id = x.Id, Name = x.Name }).ToList();
            }
        }

        /// <summary>
        /// Get Marital Status
        /// </summary>
        /// <returns>List of Marital Status</returns>
        public static List<LookUpModel> GetMaritalStatus()
        {
            using (var db = new XSeedEntities())
            {
                return db.MaritalStatusMasters.Where(s => s.IsActive == Constants.IsActiveTrue).Select(x => new LookUpModel { Id = x.Id, Name = x.Status }).ToList();
            }
        }

        /// <summary>
        /// Get Visa Types
        /// </summary>
        /// <returns>List of Visa Types</returns>
        public static List<LookUpModel> GetVisaTypes()
        {
            using (var db = new XSeedEntities())
            {
                return db.VisaTypeMasters.Where(s => s.IsActive == Constants.IsActiveTrue).Select(x => new LookUpModel { Id = x.Id, Name = x.Type }).ToList();
            }
        }

        /// <summary>
        /// Get submission feedback status
        /// </summary>
        /// <returns>Submission feedback status list</returns>
        public static List<LookUpModel> GetSubmissionFeedbackStatus()
        {
            using (var db = new XSeedEntities())
            {
                return db.SubmissionStatusMasters.Where(s => s.IsActive == Constants.IsActiveTrue).Select(x => new LookUpModel { Id = x.Id, Name = x.Status }).ToList();
            }
        }

        /// <summary>
        /// Get client feedback Status
        /// </summary>
        /// <returns>List of client feedback Status</returns>
        public static List<LookUpModel> GetClientFeedbackStatus()
        {
            using (var db = new XSeedEntities())
            {
                return db.ClientFeedbackMasters.Where(s => s.IsActive == Constants.IsActiveTrue).Select(x => new LookUpModel { Id = x.Id, Name = x.Status }).ToList();
            }
        }

        /// <summary>
        /// Get candidate feedback Status
        /// </summary>
        /// <returns>List of candidate feedback Status</returns>
        public static List<LookUpModel> GetCandidateFeedbackStatus()
        {
            using (var db = new XSeedEntities())
            {
                return db.CandidateFeedbackMasters.Where(s => s.IsActive == Constants.IsActiveTrue).Select(x => new LookUpModel { Id = x.Id, Name = x.Status }).ToList();
            }
        }

        /// <summary>
        /// Get company name for lookup
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <returns>Company names</returns>
        public static List<LookUpModel> GetCompanies(Guid organizationId)
        {
            using (var db = new XSeedEntities())
            {
                return db.CompanyDetails.Where(s => s.IsActive == Constants.IsActiveTrue && s.OrganizationId == organizationId).Select(x => new LookUpModel { Id = x.Id, Name = x.Name }).ToList();
            }
        }

        /// <summary>
        /// Get Organization Users
        /// </summary>        
        /// <param name="organizationId">Organization Id</param>
        /// <returns>Organization user list</returns>
        public static List<LookUpModel> GetOrganizationUsers(Guid organizationId)
        {
            using (var db = new XSeedEntities())
            {
                return db.OrganizationUserDetails.Where(s => s.OrganizationId == organizationId && s.UserLogin.IsActive == Constants.IsActiveTrue).Select(x => new LookUpModel { Id = x.Id, Name = (x.FirstName + " " + x.LastName), RoleName = x.UserRoleMaster.Role }).ToList();
            }
        }

        /// <summary>
        /// Get Company Contacts
        /// </summary>
        /// <param name="companyId">CompanyId</param>
        /// <returns>Company contact list</returns>
        public static List<LookUpModel> ListCompanyContacts(Guid companyId)
        {
            using (var db = new XSeedEntities())
            {
                return db.CompanyContacts.Where(c => c.CompanyId == companyId && c.IsActive == Constants.IsActiveTrue).Select(x => new LookUpModel { Id = x.Id, Name = (x.FirstName + " " + x.LastName) }).ToList();
            }
        }

        /// <summary>
        /// Get Company Contact emails
        /// </summary>
        /// <param name="companyId">CompanyId</param>
        /// <returns>Company contact email list</returns>
        public static List<LookUpModel> ListSubmissionContactEmail(Guid companyId)
        {
            using (var db = new XSeedEntities())
            {
                return db.CompanyContacts.Where(c => c.CompanyId == companyId && c.IsActive == Constants.IsActiveTrue).Select(x => new LookUpModel { Id = x.Id, Name = x.PrimaryEmail }).ToList();
            }
        }

        /// <summary>
        /// Get Skills
        /// </summary>
        /// <returns>List of Skills</returns>
        public static List<LookUpModel> GetSkills()
        {
            using (var db = new XSeedEntities())
            {
                return db.SkillMasters.Where(s => s.IsActive == Constants.IsActiveTrue).Select(x => new LookUpModel { Id = x.Id, Name = x.Name }).OrderBy(s => s.Name).ToList();
            }
        }

        /// <summary>
        /// Get State code based on country 
        /// </summary>
        /// <param name="country">country</param>
        /// <param name="city">city</param>
        /// <returns>State Code</returns>
        public static Guid? GetStateCode(string country, string city)
        {
            Guid? stateId = null;

            using (var db = new XSeedEntities())
            {
                var countryMaster = db.CountryMasters.FirstOrDefault(c => c.Name.ToLower() == country.ToLower());

                if (countryMaster != null)
                {
                    var cities = db.CityMasters.Where(c => c.Name.ToLower() == city.ToLower());

                    if (cities != null && cities.Count() > 0)
                    {
                        if (cities.Any(s => s.StateMaster.CountryId == countryMaster.Id))
                        {
                            stateId = cities.FirstOrDefault(s => s.StateMaster.CountryId == countryMaster.Id).StateId;
                        }
                    }
                }
            }

            return stateId;
        }

        /// <summary>
        /// Get City code based on country 
        /// </summary>
        /// <param name="country">country</param>
        /// <param name="city">city</param>
        /// <returns>City Code</returns>
        public static Guid? GetCityCode(string country, string city)
        {
            Guid? cityId = null;

            using (var db = new XSeedEntities())
            {
                var countryMaster = db.CountryMasters.FirstOrDefault(c => c.Name.ToLower() == country.ToLower());

                if (countryMaster != null)
                {
                    var cities = db.CityMasters.Where(c => c.Name.ToLower() == city.ToLower());

                    if (cities != null && cities.Count() > 0)
                    {
                        if (cities.Any(s => s.StateMaster.CountryId == countryMaster.Id))
                        {
                            cityId = cities.FirstOrDefault(s => s.StateMaster.CountryId == countryMaster.Id).Id;
                        }
                    }
                }
            }

            return cityId;
        }

        /// <summary>
        /// Get Business Units
        /// </summary>        
        /// <returns>Business Units</returns>
        public static List<LookUpModel> GetBusinessUnits(Guid organizationId)
        {
            using (var db = new XSeedEntities())
            {
                return db.BusinessUnitMasters.Where(c => c.OrganizationId == organizationId).OrderByDescending(b => b.CreatedOn).Select(x => new LookUpModel { Id = x.Id, Name = x.Name, Description = x.Description, OrganizationId = x.OrganizationId }).ToList();
            }
        }

        #endregion
        /// <summary>
        /// Update Job Titles
        /// </summary>
        /// <param name="lookUpModel"></param>
        public static void UpdateJobTitles(LookUpModel lookUpModel)
        {
            using (var db = new XSeedEntities())
            {
                JobTitleMaster jobtitleMaster = db.JobTitleMasters.Find(lookUpModel.Id);

                if (jobtitleMaster != null)
                {
                    jobtitleMaster.Title = lookUpModel.Name;
                    jobtitleMaster.Description = lookUpModel.Description;
                }
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Create Job Titles
        /// </summary>
        /// <param name="lookUpModel"></param>
        public static void CreateJobTitles(LookUpModel lookUpModel)
        {
            using (var db = new XSeedEntities())
            {
                JobTitleMaster jobtitleMaster = (lookUpModel.Id != null && lookUpModel.Id != Guid.Empty
                                                 && lookUpModel.OrganizationId != null && lookUpModel.OrganizationId != Guid.Empty)
                                                ? db.JobTitleMasters.Find(lookUpModel.Id) : new JobTitleMaster();

                //jobtitleMaster.Id = (Guid)lookUpModel.Id;

                jobtitleMaster.OrganizationId = lookUpModel.OrganizationId;
                jobtitleMaster.Title = lookUpModel.Name;
                jobtitleMaster.Description = lookUpModel.Description;
                jobtitleMaster.IsActive = true;
                db.JobTitleMasters.Add(jobtitleMaster);

                db.SaveChanges();
            }
        }

        /// <summary>
        /// Delete Job Titles
        /// </summary>
        /// <param name="genericLookupId"></param>
        public static void DeleteJobTitles(Guid genericLookupId)
        {
            using (var db = new XSeedEntities())
            {
                JobTitleMaster jobtitleMaster = db.JobTitleMasters.Find(genericLookupId);
                db.JobTitleMasters.Remove(jobtitleMaster);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Create Job Types
        /// </summary>
        /// <param name="lookUpModel"></param>
        public static void CreateJobTypes(LookUpModel lookUpModel)
        {
            using (var db = new XSeedEntities())
            {
                JobTypeMaster jobTypeMaster = (lookUpModel.Id != null && lookUpModel.Id != Guid.Empty
                                               && lookUpModel.OrganizationId != null && lookUpModel.OrganizationId != Guid.Empty)
                                              ? db.JobTypeMasters.Find(lookUpModel.Id) : new JobTypeMaster();

                //jobtitleMaster.Id = (Guid)lookUpModel.Id;

                jobTypeMaster.OrganizationId = lookUpModel.OrganizationId;
                jobTypeMaster.Type = lookUpModel.Name;
                jobTypeMaster.Description = lookUpModel.Description;
                jobTypeMaster.IsActive = true;
                db.JobTypeMasters.Add(jobTypeMaster);

                db.SaveChanges();
            }
        }

        /// <summary>
        /// Update Job Types
        /// </summary>
        /// <param name="lookUpModel"></param>
        public static void UpdateJobTypes(LookUpModel lookUpModel)
        {
            using (var db = new XSeedEntities())
            {
                JobTypeMaster jobTypeMaster = db.JobTypeMasters.Find(lookUpModel.Id);

                if (jobTypeMaster != null)
                {
                    jobTypeMaster.Type = lookUpModel.Name;
                    jobTypeMaster.Description = lookUpModel.Description;
                }
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Delete Job Types
        /// </summary>
        /// <param name="genericLookupId"></param>
        public static void DeleteJobTypes(Guid genericLookupId)
        {
            using (var db = new XSeedEntities())
            {
                JobTypeMaster jobTypeMaster = db.JobTypeMasters.Find(genericLookupId);
                db.JobTypeMasters.Remove(jobTypeMaster);
                db.SaveChanges();
            }
        }


        /// <summary>
        /// Create Job Degrees
        /// </summary>
        /// <param name="lookUpModel"></param>
        public static void CreateJobDegrees(LookUpModel lookUpModel)
        {
            using (var db = new XSeedEntities())
            {
                DegreeMaster degreeMaster = (lookUpModel.Id != null && lookUpModel.Id != Guid.Empty
                                              && lookUpModel.OrganizationId != null && lookUpModel.OrganizationId != Guid.Empty)
                                            ? db.DegreeMasters.Find(lookUpModel.Id) : new DegreeMaster();

                //jobtitleMaster.Id = (Guid)lookUpModel.Id;

                degreeMaster.OrganizationId = lookUpModel.OrganizationId;
                degreeMaster.Name = lookUpModel.Name;
                degreeMaster.Description = lookUpModel.Description;
                degreeMaster.IsActive = true;
                db.DegreeMasters.Add(degreeMaster);

                db.SaveChanges();
            }
        }
        #region Visa Type
        
        /// <summary>
        /// Create Job Visa Type
        /// </summary>
        /// <param name="lookUpModel"> lookUpModel</param>
        public static void CreateJobVisaType(LookUpModel lookUpModel)
        {
            using (var db = new XSeedEntities())
            {
                VisaTypeMaster visaTypeMaster = (lookUpModel.Id != null && lookUpModel.Id != Guid.Empty)
                                            ? db.VisaTypeMasters.Find(lookUpModel.Id) : new VisaTypeMaster();

                visaTypeMaster.Type = lookUpModel.Name;
                visaTypeMaster.IsActive = true;
                db.VisaTypeMasters.Add(visaTypeMaster);

                db.SaveChanges();
            }
        }

        /// <summary>
        /// Delete Job Visa Type
        /// </summary>
        /// <param name="lookUpModel"> lookUpModel</param>
        public static void DeleteJobVisaType(Guid genericLookupId)
        {

            using (var db = new XSeedEntities())
            {
                VisaTypeMaster visaTypeMaster = db.VisaTypeMasters.Find(genericLookupId);
                db.VisaTypeMasters.Remove(visaTypeMaster);
                db.SaveChanges();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="lookUpModel"></param>
        public static void UpdateJobVisaType(LookUpModel lookUpModel)
        {
            using (var db = new XSeedEntities())
            {
                VisaTypeMaster visaTypeMaster = db.VisaTypeMasters.Find(lookUpModel.Id);

                if (visaTypeMaster != null)
                {
                    visaTypeMaster.Type = lookUpModel.Name;
                }
                db.SaveChanges();
            }
        }
        #endregion
        

        /// <summary>
        /// Update Job Degrees
        /// </summary>
        /// <param name="lookUpModel"></param>
        public static void UpdateJobDegrees(LookUpModel lookUpModel)
        {
            using (var db = new XSeedEntities())
            {
                DegreeMaster degreeMaster = db.DegreeMasters.Find(lookUpModel.Id);

                if (degreeMaster != null)
                {
                    degreeMaster.Name = lookUpModel.Name;
                    degreeMaster.Description = lookUpModel.Description;
                }
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Delete Job Degrees
        /// </summary>
        /// <param name="genericLookupId"></param>
        public static void DeleteJobDegrees(Guid genericLookupId)
        {
            using (var db = new XSeedEntities())
            {
                DegreeMaster degreeMaster = db.DegreeMasters.Find(genericLookupId);
                db.DegreeMasters.Remove(degreeMaster);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Get candidate status
        /// </summary>        
        /// <remarks> Get candidate status 
        ///</remarks>
        /// <returns>List of candidate status</returns>
        public static List<LookUpModel> GetCandidateStatus()
        {
            using (var db = new XSeedEntities())
            {
                return db.CandidateStatusMasters.Where(s => s.IsActive == Constants.IsActiveTrue).Select(x => new LookUpModel { Id = x.Id, Name = x.Status }).ToList();
            }
        }

        /// <summary>
        /// Create/ Update Candidate Status
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        public static void SaveCandidateStatus(LookUpModel lookUpModel)
        {
            using (var db = new XSeedEntities())
            {
                CandidateStatusMaster statusMaster = (lookUpModel.Id != null && lookUpModel.Id != Guid.Empty) ? db.CandidateStatusMasters.Find(lookUpModel.Id) : new CandidateStatusMaster();

                statusMaster.Status = lookUpModel.Name;
                statusMaster.Description = lookUpModel.Description;
                statusMaster.IsActive = true;

                if (statusMaster.Id == Guid.Empty)
                    db.CandidateStatusMasters.Add(statusMaster);

                db.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes Candidate status
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        public static void DeleteCandidateStatus(Guid genericLookupId)
        {
            using (var db = new XSeedEntities())
            {
                CandidateStatusMaster statusMaster = db.CandidateStatusMasters.Find(genericLookupId);
                db.CandidateStatusMasters.Remove(statusMaster);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Get candidate categories
        /// </summary>        
        /// <remarks>Get candidate categories
        ///</remarks>        
        /// <returns>Categories</returns>
        public static List<LookUpModel> GetCandidateCategories(Guid organizationId)
        {
            using (var db = new XSeedEntities())
            {
                return db.CategoryMasters.Where(s => s.OrganizationId == organizationId && s.IsActive == Constants.IsActiveTrue).OrderByDescending(b => b.CreatedOn).Select(x => new LookUpModel { Id = x.Id, Name = x.Category, Description = x.Description, OrganizationId = x.OrganizationId }).ToList();
            }
        }

        /// <summary>
        /// Create/ Update Candidate Category
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        public static void SaveCandidateCategory(LookUpModel lookUpModel)
        {
            using (var db = new XSeedEntities())
            {
                CategoryMaster categoryMaster = (lookUpModel.Id != null && lookUpModel.Id != Guid.Empty
                                                && lookUpModel.OrganizationId != null && lookUpModel.OrganizationId != Guid.Empty)
                                                ? db.CategoryMasters.Find(lookUpModel.Id) : new CategoryMaster();

                categoryMaster.OrganizationId = lookUpModel.OrganizationId;
                categoryMaster.Category = lookUpModel.Name;
                categoryMaster.Description = lookUpModel.Description;
                categoryMaster.IsActive = true;

                if (categoryMaster.Id == Guid.Empty)
                    db.CategoryMasters.Add(categoryMaster);

                db.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes Candidate Category
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        public static void DeleteCandidateCategory(Guid genericLookupId)
        {
            using (var db = new XSeedEntities())
            {
                CategoryMaster categoryMaster = db.CategoryMasters.Find(genericLookupId);
                db.CategoryMasters.Remove(categoryMaster);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Get candidate groups
        /// </summary>        
        /// <remarks>Get candidate groups
        ///</remarks>        
        /// <returns>Groups</returns>
        public static List<LookUpModel> GetCandidateGroups(Guid organizationId)
        {
            using (var db = new XSeedEntities())
            {
                return db.GroupMasters.Where(s => s.OrganizationId == organizationId && s.IsActive == Constants.IsActiveTrue).OrderByDescending(b => b.CreatedOn).Select(x => new LookUpModel { Id = x.Id, Name = x.Name, Description = x.Description, OrganizationId = x.OrganizationId }).ToList();
            }
        }

        /// <summary>
        /// Create/ Update Candidate Group
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        public static void SaveCandidateGroup(LookUpModel lookUpModel)
        {
            using (var db = new XSeedEntities())
            {
                GroupMaster groupMaster = (lookUpModel.Id != null && lookUpModel.Id != Guid.Empty
                                          && lookUpModel.OrganizationId != null && lookUpModel.OrganizationId != Guid.Empty)
                                          ? db.GroupMasters.Find(lookUpModel.Id) : new GroupMaster();

                groupMaster.OrganizationId = lookUpModel.OrganizationId;
                groupMaster.Name = lookUpModel.Name;
                groupMaster.Description = lookUpModel.Description;
                groupMaster.IsActive = true;

                if (groupMaster.Id == Guid.Empty)
                    db.GroupMasters.Add(groupMaster);

                db.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes Candidate Group
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        public static void DeleteCandidateGroup(Guid genericLookupId)
        {
            using (var db = new XSeedEntities())
            {
                GroupMaster groupMaster = db.GroupMasters.Find(genericLookupId);
                db.GroupMasters.Remove(groupMaster);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Get candidate sources
        /// </summary>        
        /// <remarks>Get candidate sources
        ///</remarks>        
        /// <returns>Sources</returns>
        public static List<LookUpModel> GetCandidateSources(Guid organizationId)
        {
            using (var db = new XSeedEntities())
            {
                return db.SourceMasters.Where(s => s.OrganizationId == organizationId && s.IsActive == Constants.IsActiveTrue).OrderByDescending(b => b.CreatedOn).Select(x => new LookUpModel { Id = x.Id, Name = x.Source, Description = x.Description, OrganizationId = x.OrganizationId }).ToList();
            }
        }

        /// <summary>
        /// Create/ Update Candidate Source
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        public static void SaveCandidateSource(LookUpModel lookUpModel)
        {
            using (var db = new XSeedEntities())
            {
                SourceMaster sourceMaster = (lookUpModel.Id != null && lookUpModel.Id != Guid.Empty
                                            && lookUpModel.OrganizationId != null && lookUpModel.OrganizationId != Guid.Empty)
                                            ? db.SourceMasters.Find(lookUpModel.Id) : new SourceMaster();

                sourceMaster.OrganizationId = lookUpModel.OrganizationId;
                sourceMaster.Source = lookUpModel.Name;
                sourceMaster.Description = lookUpModel.Description;
                sourceMaster.IsActive = true;

                if (sourceMaster.Id == Guid.Empty)
                    db.SourceMasters.Add(sourceMaster);

                db.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes Candidate Source
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        public static void DeleteCandidateSource(Guid genericLookupId)
        {
            using (var db = new XSeedEntities())
            {
                SourceMaster sourceMaster = db.SourceMasters.Find(genericLookupId);
                db.SourceMasters.Remove(sourceMaster);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Get Languages
        /// </summary>        
        /// <remarks>Get Languages
        ///</remarks>        
        /// <returns>Languages</returns>
        public static List<LookUpModel> GetLanguages()
        {
            using (var db = new XSeedEntities())
            {
                return db.LanguageMasters.Where(s => s.IsActive == Constants.IsActiveTrue).Select(x => new LookUpModel { Id = x.Id, Name = x.Language }).ToList();
            }
        }

        /// <summary>
        /// Create/ Update Language
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        public static void SaveLanguage(LookUpModel lookUpModel)
        {
            using (var db = new XSeedEntities())
            {
                LanguageMaster languageMaster = (lookUpModel.Id != null && lookUpModel.Id != Guid.Empty) ? db.LanguageMasters.Find(lookUpModel.Id) : new LanguageMaster();

                languageMaster.Language = lookUpModel.Name;
                languageMaster.Description = lookUpModel.Description;
                languageMaster.IsActive = true;

                if (languageMaster.Id == Guid.Empty)
                    db.LanguageMasters.Add(languageMaster);

                db.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes Language
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        public static void DeleteLanguage(Guid genericLookupId)
        {
            using (var db = new XSeedEntities())
            {
                LanguageMaster languageMaster = db.LanguageMasters.Find(genericLookupId);
                db.LanguageMasters.Remove(languageMaster);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Get Salary Types
        /// </summary>         
        /// <returns>List of Salary Type</returns>
        public static List<LookUpModel> GetSalaryType()
        {
            using (var db = new XSeedEntities())
            {
                return db.SalaryTypeMasters.Where(s => s.IsActive == Constants.IsActiveTrue).OrderByDescending(c => c.CreatedOn).Select(x => new LookUpModel { Id = x.Id, Name = x.Type }).ToList();
            }
        }

        /// <summary>
        /// Get Currency Types
        /// </summary>         
        /// <returns>List of Currency Type</returns>
        public static List<LookUpModel> GetCurrencyType()
        {
            using (var db = new XSeedEntities())
            {
                return db.CurrencyTypeMasters.Where(s => s.IsActive == Constants.IsActiveTrue).OrderByDescending(c => c.CreatedOn).Select(x => new LookUpModel { Id = x.Id, Name = x.Type }).ToList();
            }
        }

        public static Guid CreateCity(string city, Guid stateId)
        {
            using (var db = new XSeedEntities())
            {

                CityMaster cityMaster = new CityMaster();
                String cityvar = db.CityMasters.Where(c => c.Name == city).Select(x => x.Name).FirstOrDefault();
                if (cityvar != null)
                {
                    if (cityvar.ToLower() == city.ToLower())
                    {
                        throw new Exception("City already exists.");
                    }
                }
                else
                {
                    cityMaster.StateId = stateId;
                    cityMaster.Name = city;

                    db.CityMasters.Add(cityMaster);
                    db.SaveChanges();
                }
                return cityMaster.Id;
            }
        }

        /// <summary>
        /// Check for Duplicate Lookup Name
        /// </summary>
        /// <param name="lookupName"> lookup Name</param>
        /// <param name="lookupType"> lookup Type</param>
        /// <param name="organizationId"> organization Id</param>
        /// <returns>True/ False</returns>
        public bool IsDuplicateLookupName(string lookupName, string lookupType, Guid organizationId, bool editedValue, string existingValue = "")
        {
            using (var db = new XSeedEntities())
            {
                #region RequirementTitle
                if (lookupType == Constants.RequirementTitle)
                {
                    if (editedValue)
                    {
                        if (db.JobTitleMasters.Where(o => o.Title.ToLower() != existingValue.ToLower() && o.Title.ToLower() == lookupName.ToLower() && o.OrganizationId == organizationId).Any())
                        {
                            return true;
                        }
                    }
                    else
                    {

                        if (db.JobTitleMasters.Where(o => o.Title.ToLower() == lookupName.ToLower() && o.OrganizationId == organizationId).Any())
                        {
                            return true;
                        }
                    }
                }
                #endregion

                #region RequirementType
                if (lookupType == Constants.RequirementType)
                {
                    if (editedValue)
                    {
                        if (db.JobTypeMasters.Where(o => o.Type.ToLower() != existingValue.ToLower() && o.Type.ToLower() == lookupName.ToLower() && o.OrganizationId == organizationId).Any())
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if (db.JobTypeMasters.Where(o => o.Type.ToLower() == lookupName.ToLower() && o.OrganizationId == organizationId).Any())
                        {
                            return true;
                        }
                    }
                }
                #endregion

                #region Degree
                if (lookupType == Constants.Degree)
                {
                    if (editedValue)
                    {
                        if (db.DegreeMasters.Where(o => o.Name.ToLower() != existingValue.ToLower() && o.Name.ToLower() == lookupName.ToLower() && o.OrganizationId == organizationId).Any())
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if (db.DegreeMasters.Where(o => o.Name.ToLower() == lookupName.ToLower() && o.OrganizationId == organizationId).Any())
                        {
                            return true;
                        }
                    }
                }
                #endregion

                #region Visa Type
                if (lookupType == Constants.VisaType)
                {
                    if (editedValue)
                    {
                        if (db.VisaTypeMasters.Where(o => o.Type.ToLower() != existingValue.ToLower() && o.Type.ToLower() == lookupName.ToLower()).Any())
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if (db.VisaTypeMasters.Where(o => o.Type.ToLower() == lookupName.ToLower()).Any())
                        {
                            return true;
                        }
                    }
                }
                #endregion

                #region BusinessUnit
                if (lookupType == Constants.BusinessUnit)
                {
                    if (editedValue)
                    {
                        if (db.BusinessUnitMasters.Where(o => o.Name.ToLower() != existingValue.ToLower() && o.Name.ToLower() == lookupName.ToLower() && o.OrganizationId == organizationId).Any())
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if (db.BusinessUnitMasters.Where(o => o.Name.ToLower() == lookupName.ToLower() && o.OrganizationId == organizationId).Any())
                        {
                            return true;
                        }
                    }
                }
                #endregion

                #region CompanySource
                if (lookupType == Constants.CompanySource)
                {
                    if (editedValue)
                    {
                        if (db.CompanySources.Where(o => o.Name.ToLower() != existingValue.ToLower() && o.Name.ToLower() == lookupName.ToLower() && o.OrganizationId == organizationId).Any())
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if (db.CompanySources.Where(o => o.Name.ToLower() == lookupName.ToLower() && o.OrganizationId == organizationId).Any())
                        {
                            return true;
                        }
                    }
                }
                #endregion

                #region CandidateSource
                if (lookupType == Constants.CandidateSource)
                {
                    if (editedValue)
                    {
                        if (db.SourceMasters.Where(o => o.Source.ToLower() != existingValue.ToLower() && o.Source.ToLower() == lookupName.ToLower() && o.OrganizationId == organizationId).Any())
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if (db.SourceMasters.Where(o => o.Source.ToLower() == lookupName.ToLower() && o.OrganizationId == organizationId).Any())
                        {
                            return true;
                        }
                    }
                }
                #endregion

                #region CandidateCategories
                if (lookupType == Constants.CandidateCategories)
                {
                    if (editedValue)
                    {
                        if (db.CategoryMasters.Where(o => o.Category.ToLower() != existingValue.ToLower() && o.Category.ToLower() == lookupName.ToLower() && o.OrganizationId == organizationId).Any())
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if (db.CategoryMasters.Where(o => o.Category.ToLower() == lookupName.ToLower() && o.OrganizationId == organizationId).Any())
                        {
                            return true;
                        }
                    }
                }
                #endregion

                #region CandidateGroups
                if (lookupType == Constants.CandidateGroups)
                {
                    if (editedValue)
                    {
                        if (db.GroupMasters.Where(o => o.Name.ToLower() != existingValue.ToLower() && o.Name.ToLower() == lookupName.ToLower() && o.OrganizationId == organizationId).Any())
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if (db.GroupMasters.Where(o => o.Name.ToLower() == lookupName.ToLower() && o.OrganizationId == organizationId).Any())
                        {
                            return true;
                        }
                    }
                }
                #endregion
            }
            return false;
        }

    }

    public class ChildLookUpModel : LookUpModel
    {
        #region Property Declaration

        public Nullable<Guid> ParentId { get; set; }
        public string ParentName { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Industry Type List
        /// </summary>        
        /// <returns>Industry Type List</returns>
        public static List<ChildLookUpModel> GetIndustryTypes()
        {
            using (var db = new XSeedEntities())
            {
                return db.IndustryTypeMasters.Select(x => new ChildLookUpModel { Id = x.Id, Name = x.Type, ParentId = x.ParentIndustryId }).ToList();
            }
        }

        #endregion
    }

    public class MongoLookUpModel
    {
        #region Property Declaration

        public string Id { get; set; }
        public string Name { get; set; }

        #endregion
    }
}
