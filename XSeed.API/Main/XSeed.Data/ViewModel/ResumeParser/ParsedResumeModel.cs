﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using XSeed.ResumeParser.Model;
using System.Linq.Dynamic;

namespace XSeed.Data.ViewModel.ResumeParser
{


    public class ParsedResumeModel
    {
        static string url = ConfigurationManager.AppSettings["MongoURL"].ToString();
        static string dbName = ConfigurationManager.AppSettings["MongoDB"].ToString();
        public static MongoClient client = new MongoClient(url);
        static IMongoDatabase database = client.GetDatabase(dbName);

        #region Property Declaration

        /* Id References */
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string EmailAddress { get; set; }
        //public string PhoneNumbers { get; set; }
        public string Languages { get; set; }
        public string SummaryDescription { get; set; }
        public List<string> Skills { get; set; }
        public string Location { get; set; }
        public List<Position> Positions { get; set; }
        public List<Project> Projects { get; set; }
        public List<string> SocialProfiles { get; set; }
        public List<Education> Educations { get; set; }
        public List<string> Courses { get; set; }
        public List<string> Awards { get; set; }

        //Kaustubh
        public string MiddleName { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string StreetLine { get; set; }
        public string Mobile { get; set; }
        public string Phone { get; set; }

        //Manish
        public XSeedFileEntity ResumeData { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<DateTime> ModifiedOn { get; set; }
        public Nullable<bool> IsValidResume { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Parsed Resumes
        /// </summary>
        /// <returns>List of Parsed Resumes</returns>
        public static List<ParsedResumeViewModel> GetParsedResumes(int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc")
        {
            IQueryable<ParsedResumeModel> collection = null;

            IEnumerable<ParsedResumeViewModel> listParsedResume = new List<ParsedResumeViewModel>();

            collection = database.GetCollection<ParsedResumeModel>("ParsedResume").AsQueryable(new AggregateOptions { AllowDiskUse = true }).Where(p => p.IsValidResume == null).OrderBy(sortBy + " " + sortOrder).Skip((pageNumber - 1) * pageSize).Take(pageSize);

            if (collection != null)
            {
                listParsedResume = PopulateParsedResumeViewModel(collection.ToList());
            }

            return listParsedResume.ToList();
        }

        /// <summary>
        /// Populate Parsed Resume View Model
        /// </summary>
        /// <param name="parsedResume">ParsedResumeModel</param>
        /// <returns>ParsedResumeViewModel</returns>
        internal static List<ParsedResumeViewModel> PopulateParsedResumeViewModel(List<ParsedResumeModel> parsedResumesData)
        {
            List<ParsedResumeViewModel> listParsedResumeModel = new List<ParsedResumeViewModel>();

            foreach (ParsedResumeModel parsedResume in parsedResumesData)
            {
                ParsedResumeViewModel model = new ParsedResumeViewModel();

                /* Personal Info */
                model._id = parsedResume._id;
                model.FirstName = parsedResume.FirstName;
                model.MiddleName = parsedResume.MiddleName;
                model.LastName = parsedResume.LastName;
                model.Gender = parsedResume.Gender;

                /* Contact Info */
                model.EmailAddress = parsedResume.EmailAddress;
                model.Phone = parsedResume.Phone;
                model.Mobile = parsedResume.Mobile;
                model.Location = parsedResume.Location;

                /* Employement Info */

                model.Positions = new List<Position>();
                foreach (var position in parsedResume.Positions)
                {
                    Position positionModel = new Position();
                    positionModel.Company = position.Company;
                    positionModel.Summary = position.Summary;
                    positionModel.StartDate = position.StartDate;
                    positionModel.EndDate = position.EndDate;
                    model.Positions.Add(positionModel);
                }

                /* Educational Info */
                model.Educations = new List<Education>();
                foreach (var education in parsedResume.Educations)
                {
                    Education educationModel = new Education();
                    educationModel.School = education.School;
                    educationModel.Course = education.Course;
                    educationModel.StartDate = education.StartDate;
                    educationModel.EndDate = education.EndDate;
                    model.Educations.Add(educationModel);
                }

                model.Courses = parsedResume.Courses;

                /* Profiessional Info */
                model.Awards = parsedResume.Awards;
                model.Languages = parsedResume.Languages;
                model.SummaryDescription = parsedResume.SummaryDescription;
                model.Skills = parsedResume.Skills;

                /* ToDo : Need to parse project info with additional fields from resume in parser module */
                model.Projects = new List<Project>();
                foreach (var project in parsedResume.Projects)
                {
                    Project projectModel = new Project();
                    projectModel.StartDate = project.StartDate;
                    projectModel.EndDate = project.EndDate;
                    projectModel.Title = project.Title;
                    projectModel.Summary = project.Summary;

                    model.Projects.Add(projectModel);
                }

                model.Country = parsedResume.Country;
                model.State = parsedResume.State;
                model.City = parsedResume.City;
                model.StreetLine = parsedResume.StreetLine;
                model.Zip = parsedResume.Zip;
                model.CreatedOn = parsedResume.CreatedOn;
                model.ResumeData = parsedResume.ResumeData;
                model.IsValidResume = parsedResume.IsValidResume;

                /* Get adddress from look up masters */
                using (var db = new XSeedEntities())
                {
                    CountryMaster country = db.CountryMasters.FirstOrDefault(c => c.Name == parsedResume.Country);
                    model.CountryId = country != null ? country.Id : (Guid?)null;
                    model.Country = country != null ? country.Name : parsedResume.Country;

                    StateMaster state = db.StateMasters.FirstOrDefault(c => c.Name == parsedResume.State);
                    model.StateId = state != null ? state.Id : (Guid?)null;
                    model.State = state != null ? state.Name : parsedResume.State;

                    CityMaster city = db.CityMasters.FirstOrDefault(c => c.Name == parsedResume.City);
                    model.CityId = city != null ? city.Id : (Guid?)null;
                    model.City = city != null ? city.Name : parsedResume.City;
                }
                listParsedResumeModel.Add(model);
            }

            return listParsedResumeModel;
        }

        /// <summary>
        /// Reject Parsed Resume
        /// </summary>
        /// <param name="Id">Parsed Resume Id</param>
        /// <returns></returns>
        public static void AcceptRejectParsedResume(string parsedResumeId, bool IsValidResume = false)
        {
            var filter = Builders<ParsedResumeModel>.Filter.Eq("_id", ObjectId.Parse(parsedResumeId));

            var collection = database.GetCollection<ParsedResumeModel>("ParsedResume");
            ParsedResumeModel model = collection.Find(filter).FirstOrDefault();

            //update flag
            model.ModifiedBy = BaseModel.CandidateInitiatedBy != null ? BaseModel.CandidateInitiatedBy : string.Empty;
            model.ModifiedOn = DateTime.UtcNow;
            if (IsValidResume)
            {
                model.IsValidResume = true;
            }
            else
            {
                model.IsValidResume = false;
            }

            /* Update document */
            collection.ReplaceOne(filter, model);
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>        
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        internal static PaginationModel GetPaginationInfo(int pageSize)
        {
            PaginationModel model = new PaginationModel();

            var collection = database.GetCollection<ParsedResumeModel>("ParsedResume");
            model.TotalCount = Convert.ToInt32(collection.AsQueryable(new AggregateOptions { AllowDiskUse = true }).Where(p => p.IsValidResume == null).Count());
            model.TotalPages = Math.Ceiling((double)model.TotalCount / pageSize);

            return model;
        }

        #endregion
    }
}
