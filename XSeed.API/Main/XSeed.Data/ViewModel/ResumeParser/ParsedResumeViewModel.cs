﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using XSeed.ResumeParser.Model;

namespace XSeed.Data.ViewModel.ResumeParser
{


    public class ParsedResumeViewModel
    {
        #region Property Declaration

        /* Id References */
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string EmailAddress { get; set; }
        public string Languages { get; set; }
        public string SummaryDescription { get; set; }
        public List<string> Skills { get; set; }
        public string Location { get; set; }
        public List<Position> Positions { get; set; }
        public List<Project> Projects { get; set; }
        public List<string> SocialProfiles { get; set; }
        public List<Education> Educations { get; set; }
        public List<string> Courses { get; set; }
        public List<string> Awards { get; set; }
        public string MiddleName { get; set; }
        public string Country { get; set; }
        public Guid? CountryId { get; set; }
        public string State { get; set; }
        public Guid? StateId { get; set; }
        public string City { get; set; }
        public Guid? CityId { get; set; }
        public string Zip { get; set; }
        public string StreetLine { get; set; }
        public string Mobile { get; set; }
        public string Phone { get; set; }
        public XSeedFileEntity ResumeData { get; set; }
        public Nullable<DateTime> CreatedOn { get; set; }
        public Nullable<bool> IsValidResume { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Parsed Resumes
        /// </summary>
        /// <returns>List of Parsed Resumes</returns>
        public static List<ParsedResumeViewModel> GetParsedResumes(int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc")
        {
            return (ParsedResumeModel.GetParsedResumes(pageSize, pageNumber, sortBy, sortOrder));
        }

        /// <summary>
        /// Reject Parsed Resume
        /// </summary>
        /// <param name="Id">Parsed Resume Id</param>
        /// <returns></returns>
        public static void AcceptRejectParsedResume(string parsedResumeId, bool IsValidResume = false)
        {
            ParsedResumeModel.AcceptRejectParsedResume(parsedResumeId, IsValidResume);
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>        
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        public static PaginationModel GetPaginationInfo(int pageSize)
        {
            return (ParsedResumeModel.GetPaginationInfo(pageSize));
        }

        #endregion
        
    }
}
