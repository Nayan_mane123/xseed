﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using System.Linq.Dynamic;
using XSeed.Utility;

namespace XSeed.Data.ViewModel.Reports
{
    public class CompanyLeadReportModel
    {
        #region Property Declaration

        public string SourceName { get; set; }
        public string CompanytName { get; set; }
        public string JobTitle { get; set; }
        public int? Positions { get; set; }
        public Nullable<DateTime> RequirementDate { get; set; }
        public int Submissions { get; set; }
        public string JobStatus { get; set; }
        public string CompanyType { get; set; }
        public string Via { get; set; }
        public string ViaWebsite { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Company Lead Report
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Company Lead Report</returns>
        public List<CompanyLeadReportModel> GetClientLeadReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", string type = "W", bool isExport = false)
        {
            List<CompanyLeadReportModel> list = new List<CompanyLeadReportModel>();
            /* Calculate date */

            DateTime beforeDate = GraphReportDataUtility.GetBeforeDate(type);

            if (sortBy != "Count")
            {
                sortBy = "Key." + sortBy;
            }

            using (var db = new XSeedEntities())
            {
                var jobs = isExport ? db.JobDetails
                    .Join(db.CompanySources, c => c.CompanyDetail.CompanySourceId, cs => cs.Id, (c, cs) => new { c, cs })
                    .Join(db.SubmissionDetails, j => j.c.Id, sd => sd.JobId, (j, sd) => new { j, sd })
                    .Where(j => j.sd.JobDetail.CompanyDetail.OrganizationId == organizationId && j.sd.JobDetail.CompanyDetail.CreatedOn >= beforeDate)
                    .GroupBy(x => new { LeadName = x.j.cs.Name, x.j.c.CreatedOn, x.sd.JobDetail.CompanyDetail.Name, x.j.c.TotalPositions, x.j.c.JobTitle, x.j.c.JobStatusMaster.Status, CompanyType = x.sd.JobDetail.CompanyDetail.CompanyTypeMaster.Name, x.sd.JobDetail.CompanyDetail.Via, x.sd.JobDetail.CompanyDetail.ViaWebsite })
                    .Select(x => new { x.Key, Count = x.Count() })
                    .OrderBy(sortBy + " " + sortOrder)
                    .ToList() :
                    db.JobDetails
                    .Join(db.CompanySources, c => c.CompanyDetail.CompanySourceId, cs => cs.Id, (c, cs) => new { c, cs })
                    .Join(db.SubmissionDetails, j => j.c.Id, sd => sd.JobId, (j, sd) => new { j, sd })
                    .Where(j => j.sd.JobDetail.CompanyDetail.OrganizationId == organizationId && j.sd.JobDetail.CompanyDetail.CreatedOn >= beforeDate)
                    .GroupBy(x => new { LeadName = x.j.cs.Name, x.j.c.CreatedOn, x.sd.JobDetail.CompanyDetail.Name, x.j.c.TotalPositions, x.j.c.JobTitle, x.j.c.JobStatusMaster.Status, CompanyType = x.sd.JobDetail.CompanyDetail.CompanyTypeMaster.Name, x.sd.JobDetail.CompanyDetail.Via, x.sd.JobDetail.CompanyDetail.ViaWebsite })
                    .Select(x => new { x.Key, Count = x.Count() })
                    .OrderBy(sortBy + " " + sortOrder).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

                if (jobs != null && jobs.Count > 0)
                {
                    foreach (var job in jobs)
                    {
                        CompanyLeadReportModel model = new CompanyLeadReportModel();

                        model.SourceName = job.Key.LeadName;
                        model.CompanytName = job.Key.Name;
                        model.JobTitle = job.Key.JobTitle;
                        model.Positions = job.Key.TotalPositions;
                        model.RequirementDate = job.Key.CreatedOn;
                        model.JobStatus = job.Key.Status;
                        model.CompanyType = job.Key.CompanyType;
                        model.Via = job.Key.Via;
                        model.ViaWebsite = job.Key.ViaWebsite;
                        model.Submissions = job.Count;

                        list.Add(model);
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Get Pagination Info
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid organizationId, int pageSize, string type = "W")
        {
            PaginationModel model = new PaginationModel();
            /* Calculate date */
            DateTime beforeDate = GraphReportDataUtility.GetBeforeDate(type);

            //.AddDays(daysBefore).Date;

            using (var db = new XSeedEntities())
            {
                model.TotalCount = db.JobDetails
                    .Join(db.CompanySources, c => c.CompanyDetail.CompanySourceId, cs => cs.Id, (c, cs) => new { c, cs })
                    .Join(db.SubmissionDetails, j => j.c.Id, sd => sd.JobId, (j, sd) => new { j, sd })
                    .Where(j => j.sd.JobDetail.CompanyDetail.OrganizationId == organizationId && j.sd.JobDetail.CompanyDetail.CreatedOn >= beforeDate)
                    .GroupBy(x => new { LeadName = x.j.cs.Name, x.j.c.CreatedOn, x.sd.JobDetail.CompanyDetail.Name, x.j.c.TotalPositions, x.j.c.JobTitle, x.j.c.JobStatusMaster.Status, CompanyType = x.sd.JobDetail.CompanyDetail.CompanyTypeMaster.Name, x.sd.JobDetail.CompanyDetail.Via, x.sd.JobDetail.CompanyDetail.ViaWebsite }).Count();
                model.TotalPages = Math.Ceiling((double)model.TotalCount / pageSize);
            }

            return model;
        }



        /// <summary>
        /// Get Client Graph Count
        /// </summary>
        /// <returns>Client graph</returns>
        public List<GraphModel> getClientTrackerGraphReport(Guid organizationId, string type)
        {
            using (var db = new XSeedEntities())
            {
                /* Calculate date */
                //DateTime beforeDate = DateTime.Now.AddDays(daysBefore).Date;

                var param = new System.Data.SqlClient.SqlParameter("@OrganizationId", organizationId);
                var dateParam = new System.Data.SqlClient.SqlParameter("@Type", type);
                var graphData = db.Database.SqlQuery<GraphModel>("SP_GetClientTrackerGraphCount @OrganizationId, @Type", param, dateParam).ToList();

                var returnData = GraphReportDataUtility.getreportdatabytype(graphData, type, true);

                return returnData;
            }
        }

        /// <summary>
        /// Get Client Report
        /// </summary>
        /// <returns>Client report</returns>
        public GraphRightCountModel getClientAllCounts(Guid organizationId)
        {
            GraphRightCountModel result = new GraphRightCountModel();


            using (var db = new XSeedEntities())
            {
                var param = new System.Data.SqlClient.SqlParameter("@OrganizationId", organizationId);
                //db.Database.ExecuteSqlCommand("SP_GetDashboardRequirementCount", param);
                var list = db.Database.SqlQuery<GraphRightCountModel>("SP_GetClientGraphCount @OrganizationId", param).ToList();
                if (list.Count > 0)
                {
                    result = list[0];
                }

            }


            return result;
        }

        #endregion

    }
}
