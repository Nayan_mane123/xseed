﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.User.CandidateUser;
using System.Linq.Dynamic;
using System.Globalization;
using XSeed.Utility;

namespace XSeed.Data.ViewModel.Reports
{
    public class CandidateAddedReportModel
    {
        static string url = ConfigurationManager.AppSettings["MongoURL"].ToString();
        static string dbName = ConfigurationManager.AppSettings["MongoDB"].ToString();
        /* Mongo DB Connection */
        static MongoClient client = new MongoClient(url);
        IMongoDatabase database = client.GetDatabase(dbName);

        #region Property Declaration

        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedById { get; set; }
        public string Recruiter { get; set; }
        public int MonsterCount { get; set; }
        public int DiceCount { get; set; }
        public int ManuallyCreatedCount { get; set; }
        public int Total { get; set; }
        public string PrimaryEmail { get; set; }

        public string label { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Candidate Added by recruiters Report
        /// </summary>
        /// <returns>Candidate added report</returns>
        public List<CandidateAddedReportModel> GetCandidateAddedReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", string type = "W", bool isExport = false)
        {
            /* Initialize list */
            List<CandidateAddedReportModel> list = new List<CandidateAddedReportModel>();
            /* Calculate date */
            DateTime beforeDate = GraphReportDataUtility.GetBeforeDate(type);

            using (var db = new XSeedEntities())
            {
                /* Get all users */
                var orgUserIdList = db.OrganizationUserDetails.Where(x => x.OrganizationId == organizationId)
                                                              .Select(x => x.UserId.ToString()).ToList();

                var candidateList = database.GetCollection<CandidateUserModel>("CandidateDetail")
                                            .AsQueryable(new AggregateOptions { AllowDiskUse = true })
                                            .OrderByDescending(x => x.CreatedOn)
                                            .Where(x => x.CreatedOn >= beforeDate && orgUserIdList.Contains(x.CreatedBy))
                                            .Select(c => new CandidateUserModel()
                                            {
                                                _id = c._id,
                                                Source = c.Source,
                                                CreatedBy = c.CreatedBy,
                                                CreatedByName = c.CreatedByName,
                                                CreatedOn = c.CreatedOn,
                                                PrimaryEmail = c.PrimaryEmail
                                            });
                int j = candidateList.Count();
                if (candidateList != null)
                {
                    var candidates = candidateList.ToList()
                                        .GroupBy(c => new
                                        {
                                            c.CreatedBy
                                        })
                                        .Select(c => new
                                        {
                                            c.Key,
                                            LocalCount = c.Where(x => x.Source != "Monster" || x.Source != "Dice").Count(),
                                            MonsterCount = c.Where(x => x.Source == "Monster").Count(),
                                            DiceCount = c.Where(x => x.Source == "Dice").Count(),
                                            CreatedBy = c.Key.CreatedBy,
                                            CreatedOn = c.Select(x => x.CreatedOn).First(),
                                            CreatedByName = c.Select(x => x.CreatedByName).First(),
                                        });
                    int k = candidates.Count();
                    DateTime dtCreatedOn = new DateTime();
                    foreach (var candidate in candidates)
                    {
                        /* Initialize model */
                        CandidateAddedReportModel model = new CandidateAddedReportModel();

                        /* Populate model */
                        DateTime.TryParse(candidate.CreatedOn.ToString(), out dtCreatedOn);
                        model.CreatedOn = dtCreatedOn;

                        model.CreatedById = candidate.CreatedBy;
                        model.Recruiter = candidate.CreatedByName.ToString();
                        model.ManuallyCreatedCount = candidate.LocalCount;
                        model.MonsterCount = candidate.MonsterCount;
                        model.DiceCount = candidate.DiceCount;
                        model.Total = candidate.LocalCount + candidate.MonsterCount + candidate.DiceCount;
                        model.PrimaryEmail = db.OrganizationUserDetails.FirstOrDefault(u => u.UserId.ToString() == model.CreatedById).PrimaryEmail;
                        list.Add(model);
                    }

                    list = isExport ? list.OrderByDescending(c => c.CreatedOn).ToList() : list.OrderBy(sortBy + " " + sortOrder).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
                }
            }

            return list;
        }

        /// <summary>
        /// Get Pagination Info
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid organizationId, int pageSize, string type = "W")
        {
            PaginationModel model = new PaginationModel();
            /* Calculate date */
            DateTime beforeDate = GraphReportDataUtility.GetBeforeDate(type);

            using (var db = new XSeedEntities())
            {
                /* Get all users */
                var orgUserIdList = db.OrganizationUserDetails.Where(x => x.OrganizationId == organizationId)
                                                              .Select(x => x.UserId.ToString()).ToList();

                //model.TotalCount = database.GetCollection<CandidateUserModel>("CandidateDetail")
                //                            .AsQueryable(new AggregateOptions { AllowDiskUse = true })
                //                            .OrderByDescending(x => x.CreatedOn)
                //                            .Where(x => x.CreatedOn >= beforeDate && orgUserIdList.Contains(x.CreatedBy)).GroupBy(c => new
                //                            {
                //                                c.CreatedByName,
                //                                c.CreatedBy,
                //                                c.PrimaryEmail,
                //                                y = c.CreatedOn.Value.Year,
                //                                m = c.CreatedOn.Value.Month,
                //                                d = c.CreatedOn.Value.Day
                //                            }).Count();


                var candidateList = database.GetCollection<CandidateUserModel>("CandidateDetail")
                                            .AsQueryable(new AggregateOptions { AllowDiskUse = true })
                                            .OrderByDescending(x => x.CreatedOn)
                                            .Where(x => x.CreatedOn >= beforeDate && orgUserIdList.Contains(x.CreatedBy))
                                            .Select(c => new CandidateUserModel()
                                            {
                                                _id = c._id,
                                                Source = c.Source,
                                                CreatedBy = c.CreatedBy,
                                                CreatedByName = c.CreatedByName,
                                                CreatedOn = c.CreatedOn,
                                                PrimaryEmail = c.PrimaryEmail
                                            });
                
                if (candidateList != null)
                {
                    model.TotalCount = candidateList.ToList()
                                        .GroupBy(c => new
                                        {
                                            c.CreatedBy
                                        }).Count();
                }

                model.TotalPages = Math.Ceiling((double)model.TotalCount / pageSize);
            }

            return model;
        }

        #endregion

        public List<GraphModel> GetCandidateGraphReport(Guid organizationId, string type = "W")
        {

            DateTime Today = DateTime.UtcNow;
            /* Mongo DB Connection */
            MongoClient client = new MongoClient(url);
            var database = client.GetDatabase(dbName);
            var collection = database.GetCollection<CandidateUserModel>("CandidateDetail");
            List<GraphModel> list = new List<GraphModel>();
            if (type == "Y")
            {
                DateTime startToday = new DateTime(DateTime.Now.Date.Year, 1, 1);
                //DateTime endToday = new DateTime(DateTime.Now.Date.Year, 12, 31, 23, 59, 59);
                DateTime endToday = startToday.AddYears(1).AddSeconds(-1);
                using (var db = new XSeedEntities())
                {
                    var orgUserIdList = db.OrganizationUserDetails.Where(x => x.OrganizationId == organizationId)
                                                                                  .Select(x => x.UserId.ToString()).ToList();
               
                var candidateList = database.GetCollection<CandidateUserModel>("CandidateDetail")
                                           .AsQueryable(new AggregateOptions { AllowDiskUse = true })
                                           .OrderByDescending(x => x.CreatedOn)
                                           .Where(x => x.CreatedOn >= startToday && orgUserIdList.Contains(x.CreatedBy))
                                           .Select(c => new 
                                           {
                                                   _id = c._id,
                                                CreatedOn = c.CreatedOn
                                                
                                           });

                var candidates = candidateList.ToList()
                      .GroupBy(c => new
                      {  
                          monthPosition = c.CreatedOn.Value.Month    
                      })
                      .Select(c => new
                      {
                          c.Key,
                          LocalCount = c.Count(),
                          MonthPosition = c.Key.monthPosition 
                      });


                
                foreach (var candidate in candidates)
                {
                    int a = candidate.LocalCount;
                    GraphModel model = new GraphModel();
                    model.Count = candidate.LocalCount;
                    string monthName = new DateTime(2010, candidate.MonthPosition, 1).ToString("MMM", CultureInfo.InvariantCulture);
                    model.Label = monthName;
                    list.Add(model);
                }
                }
            }
            else if (type == "M")
            {
                DateTime startToday = new DateTime(DateTime.Now.Date.Year, DateTime.Now.Date.Month, 1);
                //DateTime endToday = new DateTime(DateTime.Now.Date.Year, DateTime.Now.Date.Month, 30, 23, 59, 59);
                DateTime endToday = startToday.AddMonths(1).AddSeconds(-1);
                using (var db = new XSeedEntities())
                {
                    var orgUserIdList = db.OrganizationUserDetails.Where(x => x.OrganizationId == organizationId)
                                                                                      .Select(x => x.UserId.ToString()).ToList();

                    var MonthlyCount = collection.AsQueryable().Where(t => t.CreatedOn >= startToday && orgUserIdList.Contains(t.CreatedBy))
                                      .Count()
                                      .ToString();
                    GraphModel model = new GraphModel();
                    model.Count = Convert.ToInt32(MonthlyCount);
                    string monthName = new DateTime(2010, DateTime.Now.Date.Month, 1).ToString("MMM", CultureInfo.InvariantCulture);
                    model.Label = monthName;
                    list.Add(model);
                }
            }
            else if (type == "W")
            {
                DateTime startToday = Today.AddDays(-(int)Today.DayOfWeek);
                DateTime endToday = startToday.AddDays(7).AddSeconds(-1);
                using (var db = new XSeedEntities())
                {
                    var orgUserIdList = db.OrganizationUserDetails.Where(x => x.OrganizationId == organizationId)
                                                                                      .Select(x => x.UserId.ToString()).ToList();

                    var candidateList = database.GetCollection<CandidateUserModel>("CandidateDetail")
                                                .AsQueryable(new AggregateOptions { AllowDiskUse = true })
                                                .OrderByDescending(x => x.CreatedOn)
                                                .Where(x => x.CreatedOn >= startToday && orgUserIdList.Contains(x.CreatedBy))
                                                .Select(c => new
                                                {
                                                    _id = c._id,
                                                    CreatedOn = c.CreatedOn

                                                });

                    var candidates = candidateList.ToList()
                          .GroupBy(c => new
                          {
                              dayPosition = c.CreatedOn.Value.DayOfWeek
                          })
                          .Select(c => new
                          {
                              c.Key,
                              LocalCount = c.Count(),
                              DayPosition = c.Key.dayPosition
                          });

                    foreach (var candidate in candidates)
                    {
                        int a = candidate.LocalCount;
                        GraphModel model = new GraphModel();
                        model.Count = candidate.LocalCount;
                        string[] labelNames = new string[] { };
                        labelNames = new string[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
                        string monthName = labelNames[Convert.ToInt32(candidate.DayPosition)];
                        model.Label = monthName;
                        list.Add(model);
                    }
                }
                
            }

           
            return list;
        }

    }
}
