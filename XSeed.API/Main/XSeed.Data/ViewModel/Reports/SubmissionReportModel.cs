﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.User.CandidateUser;
using System.Linq.Dynamic;
using XSeed.Utility;

namespace XSeed.Data.ViewModel.Reports
{
    public class SubmissionReportModel
    {
        #region Property Declaration

        public Nullable<System.DateTime> SubmissionDate { get; set; }
        public Nullable<System.DateTime> RequirementDate { get; set; }
        public string Recruiter { get; set; }
        public string Company { get; set; }
        public string Candidate { get; set; }
        public string JobTitle { get; set; }
        public string Remark { get; set; }
        public string Status { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Submission Report
        /// </summary>
        /// <returns>Submission report</returns>
        public List<SubmissionReportModel> GetSubmissionTrackerReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", string type = "W", bool isExport = false)
        {
            /* Initialize list */
            List<SubmissionReportModel> list = new List<SubmissionReportModel>();

            using (var db = new XSeedEntities())
            {
                /* Calculate date */
                DateTime beforeDate = GraphReportDataUtility.GetBeforeDate(type);

                /* Get all submissions */
                var submissions = isExport ? db.SubmissionDetails
                            .Where(s => s.OrganizationUserDetail.OrganizationId == organizationId && s.CreatedOn >= beforeDate)
                            .OrderBy(sortBy + " " + sortOrder) :
                            db.SubmissionDetails
                            .Where(s => s.OrganizationUserDetail.OrganizationId == organizationId && s.CreatedOn >= beforeDate)
                            .OrderBy(sortBy + " " + sortOrder)
                            .Skip((pageNumber - 1) * pageSize).Take(pageSize);

                foreach (var submission in submissions)
                {
                    /* Initialize model */
                    SubmissionReportModel model = new SubmissionReportModel();

                    /* Get organization user, job and candidate */
                    OrganizationUserDetail recruiter = submission.OrganizationUserDetail;
                    JobDetail job = submission.JobDetail;

                    /* Populate model */
                    model.SubmissionDate = submission.CreatedOn;
                    model.RequirementDate = job != null ? job.CreatedOn : null;
                    model.Recruiter = recruiter != null ? (recruiter.FirstName + " " + recruiter.LastName) : string.Empty;
                    model.Company = job.CompanyDetail != null ? job.CompanyDetail.Name : string.Empty;
                    model.Candidate = submission.CandidateName;
                    model.JobTitle = job.JobTitle;
                    model.Status = job.JobStatusMaster != null ? job.JobStatusMaster.Status : string.Empty;

                    model.Remark = (submission.SubmissionFeedbacks != null && submission.SubmissionFeedbacks.Count > 0) ? submission.SubmissionFeedbacks.OrderByDescending(s => s.Date).FirstOrDefault().Remark : null;

                    list.Add(model);
                }
            }

            return list;
        }


        /// <summary>
        /// Get Submission Graph Count
        /// </summary>
        /// <returns>Submission graph</returns>
        public List<GraphModel> getSubmissionTrackerGraphReport(Guid organizationId, string type)
        {
            using (var db = new XSeedEntities())
            {
                /* Calculate date */
                //DateTime beforeDate = DateTime.Now.AddDays(daysBefore).Date;

                var param = new System.Data.SqlClient.SqlParameter("@OrganizationId", organizationId);
                var dateParam = new System.Data.SqlClient.SqlParameter("@Type", type);
                var graphData = db.Database.SqlQuery<GraphModel>("SP_GetSubmissionTrackerGraphCount @OrganizationId, @Type", param, dateParam).ToList();

                var returnData = GraphReportDataUtility.getreportdatabytype(graphData, type);

                return returnData;
            }
        }



        /// <summary>
        /// Get Submission Report
        /// </summary>
        /// <returns>Submission report</returns>
        public GraphRightCountModel GetSubmissionAllCounts(Guid organizationId)
        {
            GraphRightCountModel result = new GraphRightCountModel();
          

            using (var db = new XSeedEntities())
            {
                var param = new System.Data.SqlClient.SqlParameter("@OrganizationId", organizationId);
                //db.Database.ExecuteSqlCommand("SP_GetDashboardRequirementCount", param);
                var list = db.Database.SqlQuery<GraphRightCountModel>("SP_GetSubmissionGraphCount @OrganizationId", param).ToList();
                if (list.Count > 0)
                {
                    result = list[0];
                }

            }


            return result;
        }

        /// <summary>
        /// Get Pagination Info
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid organizationId, int pageSize, string type = "W")
        {
            PaginationModel model = new PaginationModel();
            /* Calculate date */
            DateTime beforeDate = GraphReportDataUtility.GetBeforeDate(type);

            using (var db = new XSeedEntities())
            {
                model.TotalCount = db.SubmissionDetails.OrderByDescending(c => c.CreatedOn).Where(s => s.OrganizationUserDetail.OrganizationId == organizationId && s.CreatedOn >= beforeDate).Count();
                model.TotalPages = Math.Ceiling((double)model.TotalCount / pageSize);
            }

            return model;
        }

        #endregion
    }
}
