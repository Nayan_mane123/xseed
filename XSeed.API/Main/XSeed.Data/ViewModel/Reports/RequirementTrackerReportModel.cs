﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using System.Linq.Dynamic;
using XSeed.Utility;

namespace XSeed.Data.ViewModel.Reports
{
    public class MonthlyModel
    {
        //public int Month { get; set; }
        //public string MonthName { get; set; }
        //public int Count { get; set; }
        //public string Label { get; set; }
    }
    public class RequirementTrackerReportModel
    {
        #region Property Declaration

        public Nullable<System.DateTime> RequirementDate { get; set; }
        public string CompanyContact { get; set; }
        public string Company { get; set; }
        public string JobTitle { get; set; }
        public string Status { get; set; }
        public int InternalSubmissions { get; set; }
        public string RequisitionId { get; set; }
        public string CompanyContactEmail { get; set; }
        public string CompanyWebsite { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Requirement Tracker Report
        /// </summary>
        /// <returns>Requirement tracker report</returns>
        public List<RequirementTrackerReportModel> GetRequirementTrackerReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", string type = "W", bool isExport = false)
        {
            /* Initialize list */
            List<RequirementTrackerReportModel> list = new List<RequirementTrackerReportModel>();

            using (var db = new XSeedEntities())
            {
                /* Calculate date */
                DateTime beforeDate = GraphReportDataUtility.GetBeforeDate(type);

                /* Get all jobs  */
                var jobs = isExport ? db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == organizationId && j.CreatedOn >= beforeDate).OrderBy(sortBy + " " + sortOrder)
                                           : db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == organizationId && j.CreatedOn >= beforeDate)
                                           .OrderBy(sortBy + " " + sortOrder)
                                           .Skip((pageNumber - 1) * pageSize).Take(pageSize);

                foreach (var job in jobs)
                {
                    /* Initialize model */
                    RequirementTrackerReportModel model = new RequirementTrackerReportModel();

                    /* Get contact */
                    CompanyContact contact = job.CompanyContact;

                    /* Populate model */
                    model.RequirementDate = job.CreatedOn;
                    model.CompanyContact = contact != null ? (contact.FirstName + " " + contact.LastName) : string.Empty;
                    model.Company = job.CompanyDetail != null ? job.CompanyDetail.Name : string.Empty;
                    model.JobTitle = job.JobTitle;
                    model.Status = job.JobStatusMaster != null ? job.JobStatusMaster.Status : string.Empty;
                    model.InternalSubmissions = job.SubmissionDetails != null ? job.SubmissionDetails.Count() : 0;
                    model.RequisitionId = job.RequisitionId;
                    model.CompanyWebsite = job.CompanyDetail!=null?job.CompanyDetail.Website:string.Empty;
                    model.CompanyContactEmail = job.CompanyContact!=null?job.CompanyContact.PrimaryEmail:string.Empty;
                    list.Add(model);
                }
            }

            return list;
        }


        /// <summary>
        /// Get Requirement Tracker Report
        /// </summary>
        /// <returns>Requirement tracker report</returns>
        public GraphRightCountModel GetRequirementAllCount(Guid organizationId)
        {
           GraphRightCountModel result = new GraphRightCountModel();


            using (var db = new XSeedEntities())
            {
                var param = new System.Data.SqlClient.SqlParameter("@OrganizationId", organizationId);
                //db.Database.ExecuteSqlCommand("SP_GetDashboardRequirementCount", param);
                var list = db.Database.SqlQuery<GraphRightCountModel>("SP_GetRequirementGraphCount @OrganizationId", param).ToList();
                if(list.Count>0)
                {
                    result = list[0];
                }

            }


            return result;
        }

        /// <summary>
        /// Get Pagination Info
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid organizationId, int pageSize, string type = "W")
        {
            PaginationModel model = new PaginationModel();
            DateTime dayBeforeDate = GraphReportDataUtility.GetBeforeDate(type);


            using (var db = new XSeedEntities())
            {
                model.TotalCount = db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == organizationId && j.CreatedOn >= dayBeforeDate).Count();
                model.TotalPages = Math.Ceiling((double)model.TotalCount / pageSize);
            }

            return model;
        }

        #endregion


        public List<GraphModel> GetRequirementTrackerGraphReport(Guid organizationId, string type)
        {
            using (var db = new XSeedEntities())
            {
                /* Calculate date */
                //DateTime beforeDate = DateTime.Now.AddDays(daysBefore).Date;

                var param = new System.Data.SqlClient.SqlParameter("@OrganizationId", organizationId);
                var dateParam = new System.Data.SqlClient.SqlParameter("@Type", type);
                var graphData = db.Database.SqlQuery<GraphModel>("SP_GetRequirementTrackerGraphCount @OrganizationId, @Type", param, dateParam).ToList();

                var returnData = GraphReportDataUtility.getreportdatabytype(graphData, type);

                return returnData;
            }
        }



        /// <summary>
        /// Get Requirement Graph Count
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>Get Requirement Graph Count</returns>
        public List<RequirementTrackerReportModel> GetRequirementGraphCount(Guid organizationId)
        {


            using (var db = new XSeedEntities())
            {
                var param = new System.Data.SqlClient.SqlParameter("@OrganizationId", organizationId);
                var graphDataCount = db.Database.SqlQuery<RequirementTrackerReportModel>("SP_GetRequirementGraphCount @OrganizationId", param).ToList();
                return graphDataCount;
            }
        }
    }
}
