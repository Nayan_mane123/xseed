﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;

namespace XSeed.Data.ViewModel.Organization
{
    public class OrganizationDetailModel
    {
        #region Property Declaration

        /// <summary>
        /// Primary Key
        /// </summary>
        public Guid Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Organization), ErrorMessageResourceName = "OrgnizationNameRequired")]
        [Display(Name = "Organization Name")]
        public string OrganizationName { get; set; }

        [Display(Name = "Country")]
        public Nullable<Guid> CountryId { get; set; }
        public string Country { get; set; }

        public string Website { get; set; }
        public string ProfileImage { get; set; }
        public XSeedFileEntity ProfileImageFile { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public Nullable<Guid> CityId { get; set; }
        public Nullable<Guid> StateId { get; set; }
        public string City { get; set; }
        public string State{ get; set; }
        public string PhoneNumber { get; set; }
        public string LinkedInURL { get; set; }
        public string TwitterURL { get; set; }
        public string GooglePlusURL { get; set; }
        public string FacebookURL { get; set; }
        public List<ChildLookUpModel> IndustryTypeList { get; set; }
        public string YouTubeURL { get; set; }
        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Organization Detail
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <returns>Organization Detail Model</returns>
        public OrganizationDetailModel GetOrganizationDetail(Guid organizationId)
        {
            return OrganizationDetail.GetOrganizationDetail(organizationId);
        }

        /// <summary>
        /// Update Organization Detail
        /// </summary>
        /// <param name="organizationId">Organization Detail Model</param>
        public void UpdateOrganizationDetail(OrganizationDetailModel organizationDetailModel)
        {
            OrganizationDetail.UpdateOrganizationDetail(organizationDetailModel);
        }

        #endregion
    }
}
