﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Common;

namespace XSeed.Data.ViewModel.ResumeBlaster
{
    public class SendResumeMail
    {
        public List<string> ResumeBlasterList { get; set; }
        public XSeedFileEntity Resume { get; set; }
    }
}
