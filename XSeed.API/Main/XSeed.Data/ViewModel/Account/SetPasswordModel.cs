﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;

namespace XSeed.Data.ViewModel.Account
{
    public class SetPasswordModel
    {
        #region Property Declaration

        [Required(ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "PasswordRequired")]
        [StringLength(100, MinimumLength = 6, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "PasswordLength")]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("NewPassword", ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "PasswordCompare")]
        public string ConfirmPassword { get; set; }

        [Required]
        public string UserId { get; set; }
        [Required]
        public string Token { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Reset User Password
        /// </summary>
        /// <param name="userName">UserName</param>
        /// <param name="newPassword">NewPassword</param>
        public static void ResetUserPassword(string userName, string newPassword)
        {
            UserLogin.ResetUserPassword(userName, newPassword);
        }

        #endregion
    }
}
