﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace XSeed.Data.ViewModel.Account
{
    public class ForgotPasswordModel
    {
        [Required]
        [EmailAddress]
        public string Username{ get; set; }
    }
}
