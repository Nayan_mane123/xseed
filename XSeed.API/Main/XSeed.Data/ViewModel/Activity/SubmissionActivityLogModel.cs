﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Notification;

namespace XSeed.Data.ViewModel.Activity
{
    public class SubmissionActivityLogModel
    {
        static string url = ConfigurationManager.AppSettings["MongoURL"].ToString();
        static string dbName = ConfigurationManager.AppSettings["MongoDB"].ToString();
        static MongoClient client = new MongoClient(url);

        #region Property Declaration

        /* Id References */
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public Nullable<Guid> SubmissionId { get; set; }
        public string ActivityType { get; set; }
        public string Description { get; set; }

        /* Log */
        public List<ChangeLog> Activity { get; set; }
        public EmailLog EmailActivity { get; set; }

        /* Audit Trail Info */
        public Guid CreatedBy { get; set; }
        public Guid CreatedById { get; set; }
        public string CreatedByName { get; set; }
        public string CreatedByEmail { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Save quick notes related to submission
        /// </summary>
        /// <param name="note">Note</param>
        /// <param name="submissionId">Job Id</param>
        /// <returns>Notes</returns>
        internal static List<Note> SaveQuickNote(string note, Guid submissionId)
        {
            /* log note */
            Log(submissionId, "Note", note);

            return GetQuickNotes(submissionId);
        }

        /// <summary>
        /// Get Quick Notes based on submission
        /// </summary>
        /// <param name="submissionId">submission Id</param>
        /// <returns>Notes</returns>
        public static List<Note> GetQuickNotes(Guid submissionId)
        {
            List<Note> notes = new List<Note>();

            var database = client.GetDatabase(dbName);

            var collection = database.GetCollection<SubmissionActivityLogModel>("SubmissionActivityLog");

            /* declare filter */
            var filter = Builders<SubmissionActivityLogModel>.Filter.Where(s => s.ActivityType == "Note" && s.SubmissionId == submissionId);

            /* apply filter */
            var documents = collection.Find(filter).Limit(10).SortByDescending(c => c.CreatedOn).ToList();

            /* format notes */
            foreach (var document in documents)
            {
                Note note = new Note();
                note.Id = document._id;
                note.CreatedBy = document.CreatedByName;
                note.ProfileImageName = OrganizationUserDetail.GetCreatedByUserProfileImage(document.CreatedBy);
                note.CreatedByEmail =  document.CreatedByEmail;
                note.CreatedById =  document.CreatedBy;
                note.CreatedOn = document.CreatedOn;
                note.Description = document.Description;
                notes.Add(note);
            }

            return notes;
        }

        public static void DeleteQuickNotes(string Id)
        {
            var database = client.GetDatabase(dbName);

            var collection = database.GetCollection<SubmissionActivityLogModel>("SubmissionActivityLog");

            /* declare filter */
            var filter = Builders<SubmissionActivityLogModel>.Filter.Where(s => s.ActivityType == "Note" && s._id == Id);

            var activityNote = collection.DeleteOne(filter);


        }

        public static void UpdateQNote(QNoteModel request, string editid)
        {
            var database = client.GetDatabase(dbName);

            var collection = database.GetCollection<SubmissionActivityLogModel>("SubmissionActivityLog");

            /* declare filter */
            var filter = Builders<SubmissionActivityLogModel>.Filter.Where(s => s.ActivityType == "Note" && s._id == editid);


            var update = Builders<SubmissionActivityLogModel>.Update.Set(s => s.Description, request.Description);
            collection.UpdateOne(filter, update);


        }

        /// <summary>
        /// Create activity log
        /// </summary>
        /// <param name="submissionId">Submission Id</param>
        /// <param name="activityType">Activity Type</param>
        /// <param name="description">Description</param>
        /// <param name="activityLog">Activity Log</param>
        /// <param name="emailLog">Email Log</param>
        /// <returns>Activity Log Id</returns>
        internal static string Log(Nullable<Guid> submissionId, string activityType, string description, List<ChangeLog> activityLog = null, EmailLog emailLog = null)
        {
            if ((activityLog != null && activityLog.Count > 0) || emailLog != null || activityType == "Note")
            {
                var database = client.GetDatabase(dbName);
                var collection = database.GetCollection<SubmissionActivityLogModel>("SubmissionActivityLog");

                /* Populate model */
                SubmissionActivityLogModel model = new SubmissionActivityLogModel();
                model.SubmissionId = submissionId;
                model.ActivityType = activityType;
                model.Description = description;
                model.Activity = activityLog;
                model.EmailActivity = emailLog;
                model.CreatedBy = BaseModel.InitiatedBy;
                model.CreatedByName = OrganizationUserDetail.GetCreatedByUserName(model.CreatedBy);
                model.CreatedByEmail = OrganizationUserDetail.GetCreatedByUserEmail(model.CreatedBy);
                model.CreatedOn = DateTime.Now;

                /* Save to db */
                collection.InsertOne(model);

                return model._id;
            }

            return string.Empty;
        }

        #endregion
    }
}
