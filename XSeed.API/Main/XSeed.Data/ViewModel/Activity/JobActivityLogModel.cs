﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Job;
using XSeed.Data.ViewModel.Activity;
using XSeed.Data.ViewModel.Notification;

namespace XSeed.Data.ViewModel.Activity
{
    public class JobActivityLogModel
    {
        static string url = ConfigurationManager.AppSettings["MongoURL"].ToString();
        static string dbName = ConfigurationManager.AppSettings["MongoDB"].ToString();
        static MongoClient client = new MongoClient(url);

        #region Property Declaration

        /* Id References */
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public Nullable<Guid> JobId { get; set; }
        public string ActivityType { get; set; }
        public string Description { get; set; }
        public Nullable<Guid> OrganizationId { get; set; }


        /* Log */
        public List<ChangeLog> Activity { get; set; }
        public EmailLog EmailActivity { get; set; }

        /* Audit Trail Info */
        public Guid CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public string CreatedByEmail { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Save quick notes related to Job
        /// </summary>
        /// <param name="note">Note</param>
        /// <param name="jobId">Job Id</param>
        /// <returns>Notes</returns>
        internal static List<Note> SaveQuickNote(string note, Guid jobId)
        {
            /* log note */
            Log(jobId, "Note", note);

            return GetQuickNotes(jobId);
        }

        public static void DeleteQuickNotes(string Id)
        {
            var database = client.GetDatabase(dbName);

           

            /* declare filter */
            var filter = Builders<JobActivityLogModel>.Filter.Where(s => s.ActivityType == "Note" && s._id == Id);


        }

        public static void UpdateQNote(QNoteModel request, string editid)
        {
            var database = client.GetDatabase(dbName);

            var collection = database.GetCollection<JobActivityLogModel>("JobActivityLog");

            /* declare filter */
            var filter = Builders<JobActivityLogModel>.Filter.Where(s => s.ActivityType == "Note" && s._id == editid);


            var update = Builders<JobActivityLogModel>.Update.Set(s => s.Description, request.Description);
            collection.UpdateOne(filter, update);


        }

        /// <summary>
        /// Get Quick Notes based on job
        /// </summary>
        /// <param name="jobId">Job Id</param>
        /// <returns>Notes</returns>
        public static List<Note> GetQuickNotes(Guid jobId)
        {
            List<Note> notes = new List<Note>();

            var database = client.GetDatabase(dbName);

            var collection = database.GetCollection<JobActivityLogModel>("JobActivityLog");

            /* declare filter */
            var filter = Builders<JobActivityLogModel>.Filter.Where(s => s.ActivityType == "Note" && s.JobId == jobId);

            /* apply filter */
            var documents = collection.Find(filter).Limit(10).SortByDescending(c => c.CreatedOn).ToList();

            /* format notes */
            foreach (var document in documents)
            {
                Note note = new Note();
                
                note.CreatedBy = document.CreatedByName;
                note.ProfileImageName = OrganizationUserDetail.GetCreatedByUserProfileImage(document.CreatedBy);
      
                note.CreatedByEmail = document.CreatedByEmail;
                note.CreatedOn = document.CreatedOn;
                note.Description = document.Description;
                note.Id = document._id;
                note.CreatedById = document.CreatedBy;
                notes.Add(note);
            }

            return notes;
        }

  
/// <summary>
        /// Create activity log
        /// </summary>
        /// <param name="jobId">Job Id</param>
        /// <param name="activityType">Activity Type</param>
        /// <param name="description">Description</param>
        /// <param name="activityLog">Activity Log</param>
        /// <param name="emailLog">Email Log</param>
        /// <returns>Activity Log Id</returns>
        internal static string Log(Nullable<Guid> jobId, string activityType, string description, List<ChangeLog> activityLog = null, EmailLog emailLog = null, Nullable<Guid> orgId = null)
        {
            if ((activityLog != null && activityLog.Count > 0) || emailLog != null || activityType == "Note")
            {
                var database = client.GetDatabase(dbName);
                var collection = database.GetCollection<JobActivityLogModel>("JobActivityLog");

                /* Populate model */
                JobActivityLogModel model = new JobActivityLogModel();
                model.JobId = jobId;
                model.ActivityType = activityType;
                model.Description = description;
                model.Activity = activityLog;
                model.EmailActivity = emailLog;
                model.CreatedBy = BaseModel.InitiatedBy;
                model.CreatedByName = OrganizationUserDetail.GetCreatedByUserName(model.CreatedBy);
                model.CreatedByEmail = OrganizationUserDetail.GetCreatedByUserEmail(model.CreatedBy);
                model.CreatedOn = DateTime.Now;
                model.OrganizationId = orgId;

                /* Save to db */
                collection.InsertOne(model);

                return model._id;
            }

            return string.Empty;
        }

 /// <summary>
        /// Track Activity Log
        /// </summary>
        /// <param name="entry">entry</param>
        /// <returns>Activity Log Id</returns>
        internal static string TrackActivityLog(System.Data.Entity.Infrastructure.DbEntityEntry<IAuditable> entry, string entityName)
        {
            Nullable<Guid> jobId = null;
            string description = string.Empty;
            List<ChangeLog> list = new List<ChangeLog>();

            /* get activity fields to log */
            List<string> activityFields = GetActivityFields();

            if (entry.State == System.Data.Entity.EntityState.Added)
            {
                foreach (var prop in entry.CurrentValues.PropertyNames)
                {
                    if (activityFields.Any(s => s == prop) && entry.CurrentValues[prop] != null)
                    {
                        /* set current value */
                        var currentValue = GetMasterValue(prop, entry.CurrentValues[prop].ToString());
                        list.Add(PopulateChangeLog(entityName, SetPropertyName(prop), "", currentValue));
                    }
                }

                /* set description */
                description = "Create New Requirement";
            }
            else if (entry.State == System.Data.Entity.EntityState.Modified)
            {
                /* set jobId */
                if (entry.CurrentValues["Id"] != null)
                {
                    jobId = Guid.Parse((entry.CurrentValues["Id"].ToString()));
                }

                foreach (var prop in entry.OriginalValues.PropertyNames)
                {
                    if (activityFields.Any(s => s == prop))
                    {
                        /* set old and new value */
                        var originalValue = entry.OriginalValues[prop] != null ? GetMasterValue(prop, entry.OriginalValues[prop].ToString()) : string.Empty;
                        var currentValue = entry.CurrentValues[prop] != null ? GetMasterValue(prop, entry.CurrentValues[prop].ToString()) : string.Empty;

                        if (originalValue != currentValue)
                        {
                            list.Add(PopulateChangeLog(entityName, SetPropertyName(prop), originalValue, currentValue));
                        }
                    }

                    /* set description */
                    description = "Updated Requirement";
                }
            }
             
            return Log(jobId, "Action", description, list);
        }
        /// <summary>
        /// Set valid property name 
        /// </summary>
        /// <param name="prop">property</param>
        /// <returns>Name</returns>
        private static string SetPropertyName(string prop)
        {
            string name = string.Empty;

            switch (prop)
            {
                case "JobTitle":
                    name = "Requirement Title";
                    break;
                case "JobTypeId":
                    name = "Requirement Type";
                    break;
                case "JobStatusId":
                    name = "Requirement Status";
                    break;
                case "CompanyId":
                    name = "Company";
                    break;
                case "TechnicalSkills":
                    name = "Technical Skills";
                    break;
                case "AssignedRecruiter":
                    name = "Assigned Recruiter";
                    break;
                default:
                    name = prop;
                    break;
            }

            return name;

        }

        /// <summary>
        /// Get Text value from masters
        /// </summary>
        /// <param name="type">Type</param>
        /// <param name="Id">Id</param>
        /// <returns>Master Value</returns>
        private static string GetMasterValue(string type, string Id)
        {
            string value = string.Empty;

            using (var db = new XSeedEntities())
            {
                switch (type)
                {
                    case "JobTypeId":
                        value = db.JobTypeMasters.Find(Guid.Parse(Id)).Type;
                        break;
                    case "JobStatusId":
                        value = db.JobStatusMasters.Find(Guid.Parse(Id)).Status;
                        break;
                    case "CompanyId":
                        value = db.CompanyDetails.Find(Guid.Parse(Id)).Name;
                        break;
                    default:
                        value = Id;
                        break;
                }
            }

            return value;
        }

        /// <summary>
        /// Populate Chage Log model
        /// </summary>
        /// <param name="entityName">Entity Name</param>
        /// <param name="prop">property</param>
        /// <param name="oldValue">Old Value</param>
        /// <param name="currentValue">Current Value</param>
        /// <returns>Change Log Model</returns>
        private static ChangeLog PopulateChangeLog(string entityName, string prop, string oldValue, string currentValue)
        {
            entityName = entityName == "JobDetail" ? "Requirement Detail" : entityName;

            /* fill log model */
            ChangeLog log = new ChangeLog()
            {
                EntityName = entityName,
                PropertyName = GetPropertyName(prop),
                OldValue = oldValue,
                NewValue = currentValue,
                DateChanged = DateTime.UtcNow
            };

            return log;
        }

        /// <summary>
        /// Get Property Name for activity Log
        /// </summary>
        /// <param name="prop">prop</param>
        /// <returns>Property Name</returns>
        private static string GetPropertyName(string prop)
        {
            string propertyName = string.Empty;

            using (var db = new XSeedEntities())
            {
                switch (prop)
                {
                    case "CompanyId":
                        propertyName = "Company";
                        break;
                    case "JobTitleId":
                        propertyName = "Title";
                        break;
                    case "JobTypeId":
                        propertyName = "Type";
                        break;
                    case "JobStatusId":
                        propertyName = "Status";
                        break;
                    default:
                        propertyName = prop;
                        break;
                }
            }
            return propertyName;
        }

        /// <summary>
        /// Update activity log with id after creating job.
        /// </summary>
        /// <param name="jobId">jobId</param>
        internal static void UpdateLog(Nullable<Guid> jobId, Nullable<Guid> OrgId)
        {
            var database = client.GetDatabase(dbName);

            var filter = Builders<JobActivityLogModel>.Filter.Eq("_id", ObjectId.Parse(BaseModel.ActivityId));

            var collection = database.GetCollection<JobActivityLogModel>("JobActivityLog");

            var jobActivityLogModel = collection.Find(filter).FirstOrDefault();

            if (jobActivityLogModel != null)
            {
                jobActivityLogModel.JobId = jobId; // update job id
                jobActivityLogModel.OrganizationId = OrgId;
                collection.ReplaceOne(filter, jobActivityLogModel);
            }
            
            
        }

        /// <summary>
        /// Update Assigned Recruiter to activity Log
        /// </summary>
        /// <param name="jobId">Job Id</param>
        /// <param name="oldRecruiters">Old Recruiters</param>
        /// <param name="newRecruiters">New Recruiters</param>
        internal static void UpdateAssignedRecruiterLog(Guid jobId, string oldRecruiters, string newRecruiters)
        {
            if (!string.IsNullOrEmpty(BaseModel.ActivityId))
            {
                var database = client.GetDatabase(dbName);
                var filter = Builders<JobActivityLogModel>.Filter.Eq("_id", ObjectId.Parse(BaseModel.ActivityId));

                var collection = database.GetCollection<JobActivityLogModel>("JobActivityLog");
                var jobActivityLogModel = collection.Find(filter).FirstOrDefault();
                jobActivityLogModel.JobId = jobId;

                /* add change log */
                jobActivityLogModel.Activity.Add(new ChangeLog() { EntityName = "Requirement Detail", PropertyName = "Assigned Recruiter", OldValue = oldRecruiters, NewValue = newRecruiters, DateChanged = DateTime.UtcNow });

                collection.ReplaceOne(filter, jobActivityLogModel);

                BaseModel.IsActivityUpdate = false;
            }
        }

        /// <summary>
        /// Get Activity Log related to Job
        /// </summary>
        /// <param name="jobId">Job Id</param>
        /// <returns>Activity Logs</returns>
        internal static List<JobActivityLogModel> GetJobActivityLog(Guid jobId)
        {
            

            var database = client.GetDatabase(dbName);

            var collection = database.GetCollection<JobActivityLogModel>("JobActivityLog");

            /* declare filter */
            var filter = Builders<JobActivityLogModel>.Filter.Where(s => s.ActivityType != "Note" && s.JobId == jobId);

            /* apply filter */
            var documents = collection.Find(filter).SortByDescending(c => c.CreatedOn).ToList();

            return documents;
        }

        /// <summary>
        /// Get Activity Log for Dashboard
        /// </summary>
        /// <param name="jobId">Job Id</param>
        /// <returns>Activity Logs</returns>
        internal static List<JobActivityLogModel> GetDashboardJobActivityLog(Guid organizationId)
        {
            

            var database = client.GetDatabase(dbName);

            var collection = database.GetCollection<JobActivityLogModel>("JobActivityLog");

            /* declare filter */                                                                       
            var filter = Builders<JobActivityLogModel>.Filter.Where(s => s.ActivityType != "Note" && s.OrganizationId == organizationId);  

            /* apply filter */
            var documents = collection.Find(filter).SortByDescending(c => c.CreatedOn).ToList().Take(8).ToList();

            return documents;
        }
        public static List<Note> GetDashboardQuickNotes(Guid organizationId)
        {
            List<Note> notes = new List<Note>();

            var database = client.GetDatabase(dbName);

            var collection = database.GetCollection<JobActivityLogModel>("JobActivityLog");

            /* declare filter */
            var filter = Builders<JobActivityLogModel>.Filter.Where(s => s.ActivityType == "Note");

            /* apply filter */
            var documents = collection.Find(filter).SortByDescending(c => c.CreatedOn).ToList().Take(8).ToList();

            /* format notes */
            foreach (var document in documents)
            {
                Note note = new Note();
                
                note.CreatedBy = document.CreatedByName;
                note.ProfileImageName = OrganizationUserDetail.GetCreatedByUserProfileImage(document.CreatedBy);
               
                note.CreatedByEmail = document.CreatedByEmail;
                note.CreatedOn = document.CreatedOn;
                note.Description = document.Description;
                note.Id = document._id;
                note.CreatedById = document.CreatedBy;
                notes.Add(note);
            }

            return notes;
        }
        /// <summary>
        /// Log Email Notification related to job
        /// </summary>
        /// <param name="emailNotificationModel">EmailNotificationModel</param>
        /// <param name="jobId">Job Id</param>        
        public static void LogEmailActivity(EmailNotificationModel emailNotificationModel, Nullable<Guid> jobId)
        {
            if (jobId != null)
            {
                /* populate log model */
                EmailLog log = new EmailLog();
                log.EmailSubject = emailNotificationModel.MailSubject;
                log.EmailContent = emailNotificationModel.MailBody;
                log.To = emailNotificationModel.ToEmailID != null ? string.Join(",", emailNotificationModel.ToEmailID.Select(s => s)).TrimEnd(',') : string.Empty;
                log.Cc = emailNotificationModel.ToCC != null ? string.Join(",", emailNotificationModel.ToCC.Select(s => s)).TrimEnd(',') : string.Empty;
                log.Bcc = emailNotificationModel.ToBCC != null ? string.Join(",", emailNotificationModel.ToBCC.Select(s => s)).TrimEnd(',') : string.Empty;
                log.Attachments = emailNotificationModel.MailAttachment;

                /* Log Email */
                Log(jobId, "Email", "Requirement Ëmail Detail", null, log);
            }
        }

        #endregion

        #region Utility Functions

        /// <summary>
        /// Get Activity field list to track changes of job detail.
        /// </summary>
        /// <returns>Activity Fields</returns>
        private static List<string> GetActivityFields()
        {
            List<string> list = new List<string>();
            list.Add("JobTitle");
            list.Add("JobTypeId");
            list.Add("JobStatusId");
            list.Add("CompanyId");
            list.Add("TechnicalSkills");
            list.Add("Duration");

            return list;
        }

        #endregion
    }
}