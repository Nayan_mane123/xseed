﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using XSeed.Utility;

namespace XSeed.Data.ViewModel.Candidate
{
    public class CandidateRatingDetailModel
    {
        #region Property Declaration

        public double CommunicationAvgRating { get; set; }
        public double EducationAvgRating { get; set; }
        public double TechnicalAvgRating { get; set; }
        public double ExperienceAvgRating { get; set; }

        public int totalFiveStarRatingCount { get; set; }
        public int totalFourStarRatingCount { get; set; }
        public int totalThreeStarRatingCount { get; set; }
        public int totalTwoStarRatingCount { get; set; }
        public int totalOneStarRatingCount { get; set; }

        

#endregion

        #region USER DEFINED FUNCTIONS

        /// <summary>
        /// Get candidate ratings detail
        /// </summary>
        /// <param name="Id">Candidate Id</param>        
        /// <returns>Candidate Contact Model</returns>
        public CandidateRatingDetailModel GetCandidateRatingDetail(string Id)
        {
            return CandidateDetail.GetCandidateRatingDetail(Id);
        }

        #endregion
    }
}
