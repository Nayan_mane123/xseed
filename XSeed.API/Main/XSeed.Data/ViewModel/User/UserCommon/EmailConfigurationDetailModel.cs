﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;

namespace XSeed.Data.ViewModel.User.UserCommon
{
    public class EmailConfigurationDetailModel
    {
        #region Property Declaration

        public Guid Id { get; set; }

        [Required]
        public Guid UserId { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Save User Email Configuration
        /// </summary>
        /// <param name="model">Email Configuration Detail Model</param>
        public static void SaveUserEmailConfiguration(EmailConfigurationDetailModel model)
        {
            EmailConfigurationDetail.SaveUserEmailConfiguration(model);
        }

        public static EmailConfigurationDetailModel GetUserEmailConfiguration(Guid UserId)
        {
            return (EmailConfigurationDetail.GetUserEmailConfiguration(UserId));
        }

        /// <summary>
        /// Update User Email Configuration
        /// </summary>
        /// <param name="model">EmailConfigurationDetailModel</param>
        public static void UpdateUserEmailConfiguration(EmailConfigurationDetailModel model)
        {
            EmailConfigurationDetail.UpdateUserEmailConfiguration(model);
        }

        #endregion
        
    }
}
