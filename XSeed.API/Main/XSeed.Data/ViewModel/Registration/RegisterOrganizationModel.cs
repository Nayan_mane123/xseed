﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.User.OrganizationUser;
using XSeed.Data.ViewModel.User.UserCommon;
using XSeed.Utility;

namespace XSeed.Data.ViewModel.Registration
{
    


    public class RegisterOrganizationModel : BaseModel
    {
        #region Property Declaration

        /// <summary>
        /// Primary Key of Login User
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// User Registration Type 
        /// </summary>
        public Guid UserType { get; set; }

        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "EmailRequired")]
        [RegularExpression(Constants.EmailExpression, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "EmailInvalid")]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "PasswordRequired")]
        [StringLength(100, MinimumLength = 6, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "PasswordLength")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "PasswordCompare")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Organization), ErrorMessageResourceName = "OrgnizationNameRequired")]
        [Display(Name = "Organization name")]
        public string OrganizationName { get; set; }

        [Display(Name = "Website")]
        public string Website { get; set; }

        [Display(Name = "Logo")]
        public string ProfileImage { get; set; }

        [Display(Name = "Country")]
        public Nullable<int> CountryId { get; set; }

        [Display(Name = "CityId")]
        public Nullable<int> CityId { get; set; }

        [Display(Name = "StateId")]
        public Nullable<int> StateId { get; set; }

        [Display(Name = "Address Line 1")]
        public string Address1 { get; set; }

        [Display(Name = "Address Line 2")]
        public string Address2 { get; set; }

        [Display(Name = "Address Line 3")]
        public string Address3 { get; set; }

        [Display(Name = "Contact Number")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Linked In URL")]
        public string LinkedInURL { get; set; }

        [Display(Name = "Twitter URL")]
        public string TwitterURL { get; set; }

        [Display(Name = "Google Plus URL")]
        public string GooglePlusURL { get; set; }

        [Display(Name = "Facebook URL")]
        public string FacebookURL { get; set; }
        public List<ChildLookUpModel> IndustryTypeList { get; set; }

        #endregion

        #region User Defined Functions

        public List<SubscriptionPlanModel> GetChildOrganizations(Guid OrganizationId)
        {

            List<SubscriptionPlanModel> OrganizationList = new List<SubscriptionPlanModel>();
            try
            {
                using (var db = new XSeedEntities())
                {

                    var param = new System.Data.SqlClient.SqlParameter("@OrganizationId", OrganizationId);

                    var childorgs = db.Database.SqlQuery<SubscriptionPlanModel>("SP_GetChildOrganizations @OrganizationId", param).ToList();

                    foreach (var childorg in childorgs)
                    {
                        SubscriptionPlanModel OrganizationData = new SubscriptionPlanModel();
                        OrganizationData.organizationId = childorg.organizationId;
                        OrganizationData.OrganizationName = childorg.OrganizationName;
                        OrganizationList.Add(OrganizationData);
                    }

                    return OrganizationList;
                }
            }
            catch (Exception ex)
            {

                Exception exception = new Exception(ex.Message);
                throw exception;
            }

           
        }

        public List<SubscriptionPlanModel> GetActivePlanFeatures(Guid ActivePlanId, Guid OrganizationId)
        {

            List<SubscriptionPlanModel> FeatureList = new List<SubscriptionPlanModel>();
            try
            {
                using (var db = new XSeedEntities())
                {
                    var featureData = db.SubscriptionPlanFeatures 
                        .Where(j => j.SubscriptionPlanId == ActivePlanId).ToList();
                     
                    foreach (var Feature in featureData)
                    {
                        SubscriptionPlanModel model = new SubscriptionPlanModel();
                        model.Id = Feature.Id;
                        model.SubscriptionPlanId = Feature.SubscriptionPlanId;
                        model.FeatureId = Feature.FeatureId;
                        model.Limit = Feature.Limit;
                        model.IsActive = Feature.IsActive;
                        FeatureList.Add(model);
                    }

                    
                    return FeatureList;
                }
            }
            catch (Exception ex)
            {

                Exception exception = new Exception(ex.Message);
                throw exception;
            }



        }
          
        public List<SubscriptionPlanModel> GetSubscriptionPlans()
        {

            List<SubscriptionPlanModel> PlanList = new List<SubscriptionPlanModel>();
            try
            {
                  
                using (var db = new XSeedEntities())
                {


                    var modelData = db.SubscriptionPlanMasters
                        .Join(db.SubscriptionPlanFeatures, j => j.Id, js => js.SubscriptionPlanId, (j, js) => new { j, js })
                        .Join(db.SubscriptionPlanFeatureMasters, f => f.js.FeatureId, fm => fm.Id, (f, fm) => new { f, fm }).OrderBy(d => d.fm.SortOrder).ToList();


                 
                    foreach(var PlansData in modelData)
                    {
                        SubscriptionPlanModel model = new SubscriptionPlanModel();
                        model.Id = PlansData.f.j.Id;
                        model.PlanName = PlansData.f.j.PlanName;
                        model.PlanPrice = PlansData.f.j.PlanPrice;
                        model.Description = PlansData.f.j.Description;

                        model.FeatureId = PlansData.f.js.FeatureId;
                        model.Feature = PlansData.fm.Feature;
                        model.IsActive = PlansData.f.js.IsActive;
                        model.PlanLimit = PlansData.f.j.PlanLimit;

                        PlanList.Add(model);
                    }
                    
                    return PlanList;
                }
            }
            catch (Exception ex)
            {
                Exception exception = new Exception(ex.Message);
                throw exception;
            }
        }


        public List<SubscriptionPlanModel> UpdateOrganizationSubscriptionPlans(SubscriptionPlanModel model)
        {
            List<SubscriptionPlanModel> list = new List<SubscriptionPlanModel>();

            /* Update subscription plan */

            try
            {
                

                using (var db = new XSeedEntities())
                {
                   // organizationDetails = db.OrganizationDetails.Find(model.organizationId); // Update Organization User info
                    db.Database.ExecuteSqlCommand("UPDATE Organization.OrganizationDetail SET ActivePlanId='" + model.Id + "', PaymentStatus='1' WHERE id = '" + model.organizationId + "'");
                }
            }
            catch (Exception ex)
            {
                Exception exception = new Exception(ex.Message);
                throw exception;
            }

            if (model != null)
            {
                list.Add(model);
            }
            return list;
        }


       

        public List<SubscriptionPlanModel> SavePaypalPaymentDetails(SubscriptionPlanModel model)
        {
            List<SubscriptionPlanModel> list = new List<SubscriptionPlanModel>();

            /* Save Payment details */

            try
            {
               
                using (var db = new XSeedEntities())
                {
                    // find user from userLogin 
                    SubscriptionPaymentDetail Pdetail = new SubscriptionPaymentDetail();


                    Pdetail.Id = Guid.NewGuid();
                    Pdetail.PaypalToken = model.PaypalToken;
                    Pdetail.PaypalPaymentId = model.PaypalPaymentId;
                    Pdetail.PaypalInvoiceNumber = model.PaypalInvoiceNumber;
                    Pdetail.PaypalPayerId = model.PaypalPayerId;
                    Pdetail.PaymentDate = DateTime.Now;                    
                    Pdetail.PaymentAmount = model.PlanPrice;
                    Pdetail.PlanPrice = model.PlanPrice;
                    Pdetail.SubscriptionPlanId = model.SubscriptionPlanId;

                    Pdetail.OrganizationId = model.organizationId;

                    Pdetail.PlanExpiryDate = DateTime.Now.AddDays(Convert.ToInt32(model.PlanLimit));

                   

                    db.SubscriptionPaymentDetails.Add(Pdetail);
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception e){
                        Exception exception = new Exception(e.Message);
                        throw exception;
                    }
                   
                     
                     
                }
 
            }
            catch (Exception ex)
            {

                Exception exception = new Exception(ex.Message);
                throw exception;
            }

            if (model != null)
            {
                list.Add(model);
            }
            return null;
        }

        

        /// <summary>
        /// function to Save Registration Info
        /// </summary>
        public List<OrganizationUserModel> SaveRegistrationInfo(RegisterOrganizationModel registerOrgnization, List<UserRoleModel> userRoleModelList)
        {
            List<OrganizationUserModel> list = new List<OrganizationUserModel>();
            Guid organizationId, loginUserId;
            /* Save Login Info */
            loginUserId = UserLogin.SaveUserLoginInfo(registerOrgnization);

            /* Save Organization Info */
            organizationId = OrganizationDetail.CreateOrganizationDetail(registerOrgnization);

            /* Create Default Roles for an organization */
            Guid adminRoleId = Guid.Empty;
            foreach (var userRoleModel in userRoleModelList)
            {
                userRoleModel.OrganizationId = organizationId;
                Guid roleId = UserRoleModel.CreateUserRole(userRoleModel);

                if (userRoleModelList.First() == userRoleModel)
                {
                    adminRoleId = roleId;
                }
            }

            /* Populate Organization User Detail */
            OrganizationUserModel organizationUserModel = new OrganizationUserModel();
            organizationUserModel.UserId = loginUserId;
            organizationUserModel.RoleId = adminRoleId;
            organizationUserModel.OrganizationId = organizationId;
            organizationUserModel.PrimaryEmail = registerOrgnization.UserName;
            organizationUserModel.FirstName = registerOrgnization.FirstName;
            organizationUserModel.LastName = registerOrgnization.LastName;

            /* Save Organization User Detail Info */
            OrganizationUserDetail.CreateOrganizationUser(organizationUserModel);

            if (organizationUserModel !=null)
            {
            list.Add(organizationUserModel);
            }

            return list;
        }


      


        /// <summary>
        /// Check if organization is already registered
        /// </summary>
        /// <param name="userName">User Name</param>
        /// <returns>Is exists flag</returns>
        public static bool IsOrganizationDomainExists(string userName)
        {
            string domain = string.Empty;

            if (userName != null)
            {
                /* get domain from email */
                domain = userName.Split('@').Last();

                if (!string.IsNullOrEmpty(domain))
                {
                    /* get public domains from config */
                    string[] publicDomains = ConfigurationManager.AppSettings["PublicDomains"].Split(',');

                    if (!publicDomains.Contains(domain))
                    {
                        using (var db = new XSeedEntities())
                        {
                            /* get registered domains from db */
                            var registeredDomains = db.OrganizationUserDetails.Select(e => e.PrimaryEmail).ToList();

                            if (registeredDomains.Any(e => e.Split('@').Last().ToLower() == domain.ToLower()))
                            {
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        public bool IsOrganizationAlreadyExist(RegisterOrganizationModel registerOrgnization)
        {
            return OrganizationDetail.IsOrganizationAlreadyExist(registerOrgnization);
        }

        #endregion
    }
}
