﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;

namespace XSeed.Data.ViewModel.BooleanSearch
{
    public class BooleanQueryModel
    {
        #region Property Declaration

        public Guid Id { get; set; }
        public string Tag { get; set; }

        [Required]
        public string Query { get; set; }
        public Nullable<Guid> OrganizationId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string CreatedBy { get; set; } 

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Save Boolean Query
        /// </summary>
        /// <param name="booleanQueryModel">BooleanQueryModel</param>
        /// <returns></returns>
        public static void SaveBooleanQuery(BooleanQueryModel booleanQueryModel)
        {
            BooleanQuery.SaveBooleanQuery(booleanQueryModel);
        }

        /// <summary>
        /// Get List of Boolean Queries
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <returns>List of Boolean Queries</returns>
        public static List<BooleanQueryModel> ListAllBooleanQueries(Guid organizationId)
        {
            return (BooleanQuery.ListAllBooleanQueries(organizationId));
        }

        #endregion

    }
}
