﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Job;

namespace XSeed.Data.ViewModel.Parser
{
    public class ParserModel
    {

        #region Property Declaration

        public System.Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string SkillList { get; set; }
        public string Company { get; set; }
        public string CompanyContact { get; set; }
        public string Html { get; set; }
        public Nullable<bool> IsRejected { get; set; }
        public Nullable<bool> IsAccepted { get; set; }
        public string EmailUniqueId { get; set; }
        public string UserEmailAddress { get; set; }
        public Nullable<System.Guid> AcceptedBy { get; set; }
        public Nullable<System.Guid> JobId { get; set; }
        public Nullable<System.DateTime> EmailDate { get; set; }
        public string Subject { get; set; }
        public string RequisitionId { get; set; }

        #endregion

        /// <summary>
        /// Get list of unparsed email
        /// </summary>        
        /// <returns>Unparsed Email list</returns>
        public static List<EmailParser> GetParsedJob(Guid OrganizationUserId)
        {
            return EmailParser.GetParsedJob(OrganizationUserId);
        }

        /// <summary>
        /// Get list of parsed job
        /// </summary>
        /// <param name="OrganizationUserId"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <returns>Parsed job List</returns>
        public static List<ParserModel> ListAllParsedJobs(Guid OrganizationUserId, int pageSize, int pageNumber)
        {
            return EmailParser.ListAllParsedJobs(OrganizationUserId, pageSize, pageNumber);
        }

        /// <summary>
        /// Get list of accepted parsed job
        /// </summary>        
        /// <returns>Accepted parsed job list</returns>
        public static List<ParserModel> GetAcceptedParsedJob(Guid OrganizationUserId)
        {
            return EmailParser.GetAcceptedParsedJob(OrganizationUserId);
        }

        /// <summary>
        /// Accept Parsed Job and add to Job Detail
        /// </summary>
        /// <param name="Id">Id</param>
        /// <param name="OrganizationUserId">Organization User Id</param>
        /// <param name="isReviewed">IsReviewed flag</param>
        public static void AcceptParsedJob(Guid Id, Guid JobId, Guid OrganizationUserId, bool isReviewed)
        {
            EmailParser.AcceptParsedJob(Id, JobId, OrganizationUserId, isReviewed);
        }

        /// <summary>
        /// Mark Parsed Job as rejected
        /// </summary>
        /// <param name="Id">Id</param>
        public static void RejectParsedJob(Guid Id)
        {
            EmailParser.RejectParsedJob(Id);
        }

        /// <summary>
        /// Get Parsed Job Detail information for review.
        /// </summary>
        /// <param name="Id">Id</param>
        /// <param name="organizationUserId">Organization User Id</param>
        /// <returns>Job Detail Model</returns>
        public static JobDetailModel ReviewParsedJob(Guid Id, Guid organizationUserId)
        {
            return EmailParser.ReviewParsedJob(Id, organizationUserId);
        }
        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>
        /// <param name="organizationId">organization user Id</param>
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        public static PaginationModel GetPaginationInfo(Guid OrganizationUserId, int pageSize)
        {
            return EmailParser.GetPaginationInfo(OrganizationUserId, pageSize);
        }

        /// <summary>
        /// Get Parsed Job List Count
        /// </summary>
        /// <param name="OrganizationUserId">Organization UserId</param>
        /// <returns>Parsed Job List Count</returns>
        public static int GetParsedJobListCount(Guid OrganizationUserId)
        {
            return EmailParser.GetParsedJobListCount(OrganizationUserId);
        }
    }
}
