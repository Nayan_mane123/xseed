﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;

namespace XSeed.Data.ViewModel.Submission
{
    public class CandidateFeedbackModel
    {
        #region Property Declaration

        public Guid Id { get; set; }
        public Nullable<Guid> SubmissionId { get; set; }
        public string FeedbackType { get; set; }
        public Nullable<Guid> StatusId { get; set; }
        public string Status { get; set; }
        public string Remark { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string CreatedBy { get; set; }

        public double CommunicationRating { get; set; }
        public double TechnicalRating { get; set; }
        public double ExperianceRating { get; set; }
        public double EducationalRating { get; set; }

        public double Rating { get; set; }
        public string CandidateId { get; set; }
        #endregion

        /// <summary>
        /// Get candidate feedback list for particular submission
        /// </summary>
        /// <param name="submissionId">Submission Id</param>
        /// <returns>List of Candidate feedback</returns>
        public List<CandidateFeedbackModel> GetCandidateFeedbackList(Guid submissionId)
        {
            return CandidateFeedback.GetCandidateFeedbackList(submissionId);
        }

        /// <summary>
        /// Add Candidate Feedback
        /// </summary>
        /// <param name="candidateFeedbackModel">Candidate Feedback Model </param>
        public static void AddCandidateFeedback(CandidateFeedbackModel candidateFeedbackModel)
        {
            CandidateFeedback.AddCandidateFeedback(candidateFeedbackModel);
        }
    }
}
