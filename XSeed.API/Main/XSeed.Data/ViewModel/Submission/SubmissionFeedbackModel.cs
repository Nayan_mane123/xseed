﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;

namespace XSeed.Data.ViewModel.Submission
{
    public class SubmissionFeedbackModel
    {
        #region  Property Declaration

        public Guid Id { get; set; }
        public Nullable<Guid> SubmissionId { get; set; }
        public string FeedbackType { get; set; }
        public Nullable<Guid> StatusId { get; set; }
        public string Status { get; set; }
        public string Remark { get; set; }
        public string InterviewType { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string CreatedBy { get; set; }

        #endregion

        #region  User Defined Functions

        /// <summary>
        /// Get submission feedback list for particular submission
        /// </summary>
        /// <param name="submissionId">Submission Id</param>
        /// <returns>List of feedback</returns>
        public List<SubmissionFeedbackModel> GetSubmissionFeedbackList(Guid submissionId)
        {
            return SubmissionFeedback.GetSubmissionFeedbackList(submissionId);
        }

        /// <summary>
        /// Add submission feedback
        /// </summary>
        /// <param name="submissionFeedbackModel">Submission Feedback Model </param>
        public void AddSubmissionFeedback(SubmissionFeedbackModel submissionFeedbackModel)
        {
            SubmissionFeedback.AddSubmissionFeedback(submissionFeedbackModel);
        }

        #endregion
    }
}