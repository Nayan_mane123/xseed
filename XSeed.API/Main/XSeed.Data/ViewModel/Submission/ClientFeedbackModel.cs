﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;

namespace XSeed.Data.ViewModel.Submission
{
    public class ClientFeedbackModel
    {
        #region Property Declaration

        public Nullable<Guid> Id { get; set; }
        public Nullable<Guid> SubmissionId { get; set; }
        public string FeedbackType { get; set; }
        public Nullable<Guid> StatusId { get; set; }
        public string Status { get; set; }
        public string Remark { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string CreatedBy { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get client feedback list for particular submission
        /// </summary>
        /// <param name="submissionId">Submission Id</param>
        /// <returns>List of client feedback</returns>
        public List<ClientFeedbackModel> GetClientFeedbackList(Guid submissionId)
        {
            return ClientFeedback.GetClientFeedbackList(submissionId);
        }

        /// <summary>
        /// Add Client Feedback
        /// </summary>
        /// <param name="clientFeedbackModel">Client Feedback Model </param>
        public void AddClientFeedback(ClientFeedbackModel clientFeedbackModel)
        {
            ClientFeedback.AddClientFeedback(clientFeedbackModel);
        }

        #endregion
    }
}
