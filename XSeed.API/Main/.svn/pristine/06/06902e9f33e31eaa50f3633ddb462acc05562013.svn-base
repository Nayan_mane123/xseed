﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using XSeed.API.Models;
using XSeed.Business.Submission;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Job;
using XSeed.Data.ViewModel.Submission;

using XSeed.Data.ViewModel.Activity;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Submission API
    /// </summary>
    public class SubmissionController : BaseController
    {

        /// <summary>
        /// Get submission(s)
        /// </summary>        
        /// <param name="organizationId">Organization Id</param>
        /// <param name="Id">Id</param>
        /// <remarks>Get list of submission(s)
        /// - Organization Id is required parameter
        /// - Id is an optional parameter
        /// - Id with value as zero will return list submissions
        /// - Id with value as positive integer will return single submission details
        ///</remarks>
        /// <returns>Submission(s)</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Submission_Read", ClaimValue = "True")]
        public IHttpActionResult Get(Guid organizationId, Guid? Id = null)
        {
            SubmissionInfo model = new SubmissionInfo();

            if (Id == null)
            {
                return Ok(model.GetSubmissions(organizationId)); // return submission list
            }
            else
            {
                return Ok(model.GetSubmissionDetail(organizationId, Id)); // return single submission
            }

        }

        /// <summary>
        /// Get list of submission using server side pagination
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>        
        /// <returns>submission List</returns>
        // GET: api/submission/pageSize/pageNumber/
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Submission_Read", ClaimValue = "True")]
        public IHttpActionResult Get(Guid organizationId, int pageSize, int pageNumber, string sortBy = "ModifiedOn", string sortOrder = "desc", bool isExport = false, bool loadPageData = false)
        {
            SubmissionInfo model = new SubmissionInfo();

             if (loadPageData)
            {
                /* Set Pagination Info */
                var paginationInfo = model.GetPaginationInfo(organizationId, pageSize);
                HttpContext.Current.Response.AppendHeader("TotalCount", paginationInfo.TotalCount.ToString());
                HttpContext.Current.Response.AppendHeader("TotalPages", paginationInfo.TotalPages.ToString());


                var result = new
                {
                    TotalCount = paginationInfo.TotalCount,
                    totalPages = paginationInfo.TotalPages,
                    Submissions = model.GetSubmissions(organizationId, pageSize, pageNumber, sortBy, sortOrder, isExport)
                };

                return Ok(result); // return candidate list
            }
            else
            {
                 var result = new
                 {
                     Submissions = model.GetSubmissions(organizationId, pageSize, pageNumber, sortBy, sortOrder, isExport)
                 };
                 return Ok(result); // return candidate list
            }
        }

        /// <summary>
        /// Create new submission
        /// </summary>
        /// <param name="submissionDetailModel">Submission detail model </param>
        /// <remarks>Create new submission</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Submission_Create", ClaimValue = "True")]
        public IHttpActionResult Post(SubmissionModel submissionDetailModel)
        {
            List<string> successCandidateList = new List<string>();
            string message = string.Empty;

            SubmissionInfo model = new SubmissionInfo();
            model.CreateSubmissionDetail(submissionDetailModel, out successCandidateList, out message);

            var result = new
            {
                successCandidates = successCandidateList,
                message = message
            };

            return Ok(result);
        }

        /// <summary>
        /// Send Submission E-Mails
        /// </summary>
        /// <param name="submissionMailModel">Submission Mail Model</param>
        /// <returns></returns>
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Submission_Create", ClaimValue = "True")]
        public IHttpActionResult SendSubmissionMail(SubmissionMailModel submissionMailModel)
        {
            SubmissionInfo model = new SubmissionInfo();
            model.SendSubmissionMail(submissionMailModel);
            return Ok();
        }

        /// <summary>
        /// Save Quick Note related to submission
        /// </summary>
        /// <param name="note">Note</param>
        /// <param name="submissionId">Submission Id</param>
        /// <returns>Notes</returns>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Submission_Create", ClaimValue = "True")]
        public IHttpActionResult SaveQuickNote(string note, Guid submissionId)
        {
            SubmissionInfo model = new SubmissionInfo();
            return Ok(model.SaveQuickNote(note, submissionId));
        }

        /// <summary>
        /// Get Quick Notes related to submission
        /// </summary>
        /// <param name="submissionId">Submission Id</param>
        /// <returns>Quick Notes</returns>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Submission_Read", ClaimValue = "True")]
        public IHttpActionResult GetQuickNotes(Guid submissionId)
        {
            SubmissionInfo model = new SubmissionInfo();
            return Ok(model.GetQuickNotes(submissionId));
        }

        [HttpDelete]
        public async Task<IHttpActionResult> DeleteQuickNotes(string qnoteId)
        {
            if (qnoteId == null)
            {
                throw new ArgumentNullException("QNotesId"); ;
            }

            SubmissionInfo model = new SubmissionInfo();
            model.DeleteQuickNotes(qnoteId);
            return Ok();
        }

        [HttpPut]
        public IHttpActionResult UpdateQNoteSubmission(QNoteModel request, string editid = null)
        {
            SubmissionInfo model = new SubmissionInfo();

            model.UpdateQNote(request, editid);
            return Ok();
        }

        /// <summary>
        /// Submit sourced candidates against selected Job.
        /// </summary>
        /// <param name="candidateSourceResultModelList">Candidate Source Result Model</param>
        /// <remarks>Create new submission</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Submission_Create", ClaimValue = "True")]
        public IHttpActionResult SubmitSourcedCandidates(List<CandidateSourceResultModel> candidateSourceResultModelList)
        {
            SubmissionInfo model = new SubmissionInfo();
            List<MongoLookUpModel> candidates = model.SubmitSourcedCandidates(candidateSourceResultModelList);

            return Ok(candidates);
        }

    }
}
