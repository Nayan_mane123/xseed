﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.User.CandidateUser;
using System.Linq.Dynamic;
using XSeed.Utility;
using MongoDB.Bson;
namespace XSeed.Data.ViewModel.Dashboard
{
    public class DashboardModel
    {
        static string url = ConfigurationManager.AppSettings["MongoURL"].ToString();
        static string dbName = ConfigurationManager.AppSettings["MongoDB"].ToString();

        #region Property Declaration

        public int TotalCandidates { get; set; }
        public int CandidatesAddedToday { get; set; }
        public int TotalJobs { get; set; }
        public int JobsAddedToday { get; set; }
        public int TotalSubmissions { get; set; }
        public int TodaysSubmission { get; set; }
        public int TotalClients { get; set; }
        public int ClientsAddedToday { get; set; }
        //Updated get more counts
        //Aug 15 2019
        public int JobEmailCount { get; set; }
        public int ResumeEmailCount { get; set; }
        public int ClientEmailCount { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Dashboard Detail
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="userId">user Id</param>
        /// <returns>Dashboard Model</returns>
        public static DashboardModel GetDashboardDetail(Guid organizationId, Guid? userId)
        {
            DashboardModel model = new DashboardModel();

            /* Mongo DB Connection */
            MongoClient client = new MongoClient(url);
            var database = client.GetDatabase(dbName);
            var collection = database.GetCollection<CandidateUserModel>("CandidateDetail").AsQueryable(new AggregateOptions { AllowDiskUse = true });

            /* candidate info */
            model.TotalCandidates = (int)collection.Count();
            model.CandidatesAddedToday = (int)collection.Count(c => c.CreatedOn.Value.CompareTo(DateTime.Today) >= 0);

            using (var db = new XSeedEntities())
            {
                /* Job Info */
                model.TotalJobs = db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == organizationId && j.IsActive != false).Count();
                model.JobsAddedToday = db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == organizationId && j.IsActive != false && j.JobStatusMaster.Status != "Closed" && j.JobStatusMaster.Status != "Archive" && j.CreatedOn.Value.CompareTo(DateTime.Today) >= 0).Count();

                /* Submission Info */
                model.TotalSubmissions = db.SubmissionDetails.Where(s => s.JobDetail.CompanyDetail.OrganizationId == organizationId).Count();
                model.TodaysSubmission = db.SubmissionDetails.Where(s => s.CreatedOn.Value.CompareTo(DateTime.Today) >= 0 && s.JobDetail.CompanyDetail.OrganizationId == organizationId).Count();

                /* Client Info */
                model.TotalClients = db.CompanyDetails.Where(c => c.OrganizationId == organizationId && c.IsActive != false).Count();
                model.ClientsAddedToday = db.CompanyDetails.Where(c => c.OrganizationId == organizationId && c.IsActive != false && c.CreatedOn.Value.CompareTo(DateTime.Today) >= 0).Count();
            }

            return model;
        }


        /// <summary>
        /// Get Candidate Added By Recruiters Chart
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <param name="userId">User Id</param>
        /// <returns>List<CandidateChartModel></returns>
        public static List<CandidateChartModel> GetCandidateChartDetail(Guid organizationId, Guid? userId)
        {
            List<CandidateChartModel> list = new List<CandidateChartModel>();
            List<OrganizationUserDetail> recruiters = null;

            using (var db = new XSeedEntities())
            {
                recruiters = db.OrganizationUserDetails.Where(u => u.OrganizationId == organizationId && u.UserLogin.IsActive != false).Take(6).ToList(); // Get Recruiters
            }

            if (recruiters != null)
            {
                /* Mongo DB Connection */
                MongoClient client = new MongoClient(url);
                var database = client.GetDatabase(dbName);
                var collection = database.GetCollection<CandidateUserModel>("CandidateDetail");

                foreach (var recruiter in recruiters)
                {
                    /* Filters */
                    var createdByFilter = Builders<CandidateUserModel>.Filter.Eq("CreatedBy", recruiter.UserId.ToString());

                    /* Populate Model */
                    CandidateChartModel model = new CandidateChartModel();
                    model.Count = (int)collection.Find(createdByFilter).Count();
                    model.UserId = recruiter.Id;
                    model.Name = recruiter.FirstName + " " + recruiter.LastName;

                    if (model.Count > 0)
                    {
                        list.Add(model);
                    }

                }
            }

            return list;
        }

        /// <summary>
        /// Get Recruiter wise Submission Chart Detail
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <param name="userId">userId</param>
        /// <returns>List<SubmissionChartModel></returns>
        public static List<SubmissionChartModel> GetSubmissionChartDetail(Guid organizationId, Guid? userId)
        {
            List<SubmissionChartModel> list = new List<SubmissionChartModel>();

            using (var db = new XSeedEntities())
            {
                /* Get Recruiter wise submissions */
                var submissions = db.SubmissionDetails.Join(db.OrganizationUserDetails, s => s.CreatedBy, r => r.UserId, (s, r) => new { s, r })
                                                      .Where(w => w.r.OrganizationId == organizationId && w.r.UserLogin.IsActive != false)
                                                      .GroupBy(x => new { x.r.UserId })
                                                      .Select(x => new { x.Key, Count = x.Count() }).ToList().Take(6);

                foreach (var submission in submissions)
                {
                    SubmissionChartModel model = new SubmissionChartModel();
                    var recruiter = db.OrganizationUserDetails.FirstOrDefault(u => u.UserId == submission.Key.UserId);

                    if (recruiter != null)
                    {
                        model.Name = recruiter.FirstName + " " + recruiter.LastName;
                        model.UserId = recruiter.Id;
                        model.Count = submission.Count;

                        if (submission.Count > 0)
                        {
                            list.Add(model);
                        }
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Get Job Status Chart Detail
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <param name="userId">userId</param>
        /// <returns>List<JobStatusChartModel></returns>
        public static List<JobStatusChartModel> GetJobStatusChartDetail(Guid organizationId, Guid? userId)
        {
            List<JobStatusChartModel> list = new List<JobStatusChartModel>();

            using (var db = new XSeedEntities())
            {
                var param = new System.Data.SqlClient.SqlParameter("@OrganizationId", organizationId);
                //db.Database.ExecuteSqlCommand("SP_GetDashboardRequirementCount", param);
                list = db.Database.SqlQuery<JobStatusChartModel>("SP_GetDashboardRequirementCount @OrganizationId", param).ToList();
            }

            return list;
        }

        /// <summary>
        /// Get Job and Submissions done in last year chart detail
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <param name="userId">userId</param>
        /// <returns>List<JobSubmissionChartModel></returns>
        public static List<JobSubmissionChartModel> GetjobSubmissionChartDetail(Guid organizationId, Guid? userId, string type = "Y" )
        {
            List<JobSubmissionChartModel> list = new List<JobSubmissionChartModel>();
            DateTime beforeDate = DateTime.UtcNow;
            if (type != "D")
            {
                
                 beforeDate = Utility.GraphReportDataUtility.GetBeforeDate(type);
            }
           
            using (var db = new XSeedEntities())
            {
                DateTime lastYear = DateTime.Today.AddYears(-1).AddMonths(1);
                /* Get Jobs with last year filter */

                var jobs = db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == organizationId && j.CreatedOn >= beforeDate)
                                        .GroupBy(j => new { j.CreatedOn.Value.Month, y = j.CreatedOn.Value.Year, m = j.CreatedOn.Value.Month })
                                        .Select(j => new { j.Key, Count = j.Count() });


                foreach (var job in jobs)
                {
                    JobSubmissionChartModel model = new JobSubmissionChartModel();
                    model.JobCount = job.Count;
                    model.SubmissionCount = 0;
                    model.Year = job.Key.y;
                    model.Month = job.Key.m;
                    model.MonthName = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(job.Key.m);//job.Key.CreatedOn.Value.ToString("MM", CultureInfo.InvariantCulture);

                    list.Add(model);
                }


                /* Get submissions with last year filter */
                var submissions = db.SubmissionDetails.Where(s => s.JobDetail.CompanyDetail.OrganizationId == organizationId && s.CreatedOn >= beforeDate)
                                        .GroupBy(s => new { s.CreatedOn.Value.Month, y = s.CreatedOn.Value.Year, m = s.CreatedOn.Value.Month })
                                        .Select(s => new { s.Key, Count = s.Count() });

                foreach (var submission in submissions)
                {
                    var Year = submission.Key.y;
                    var Month = submission.Key.m;

                    foreach (var item in list)
                    {
                        if (item.Month == Month && item.Year == Year)
                        {
                            item.SubmissionCount = submission.Count;
                        }
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Get dashboard charts periodic data
        /// </summary>
        /// <param name="organizationId"></param>
        /// <param name="userId"></param>
        /// <returns>dashboard charts periodic data</returns>
        public static DashboardGraphModel GetDashboardPeriodicData(Guid organizationId, Guid? userId)
        {
            using (var db = new XSeedEntities())
            {
                //var Jobs = db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == organizationId);

                DateTime Today = DateTime.UtcNow;
                DateTime beforeYear = Utility.GraphReportDataUtility.GetBeforeDate("Y");
                DateTime beforeMonth = Utility.GraphReportDataUtility.GetBeforeDate("M");
                DateTime beforeWeek = Utility.GraphReportDataUtility.GetBeforeDate("W");

                DashboardGraphModel dashboardGraphModel = new DashboardGraphModel();
                var param = new System.Data.SqlClient.SqlParameter("@OrganizationId", organizationId);

                List<PeriodicAllCounts> periodicAllCountsList = db.Database.SqlQuery<PeriodicAllCounts>("SP_GetDashboardPeriodicCount @OrganizationId", param).ToList();

                dashboardGraphModel.RequirementData = new PeriodicData();
                if (periodicAllCountsList.Count > 0)
                {
                    ////Requirement status periodic data
                    dashboardGraphModel.RequirementData.YearlyCount = periodicAllCountsList[0].RequirementDataYearlyCount.ToString();
                    dashboardGraphModel.RequirementData.MonthlyCount = periodicAllCountsList[0].RequirementDataMonthlyCount.ToString();
                    dashboardGraphModel.RequirementData.WeeklyCount = periodicAllCountsList[0].RequirementDataWeeklyCount.ToString();
                    dashboardGraphModel.RequirementData.DailyCount = periodicAllCountsList[0].RequirementDataDailyCount.ToString();


                    dashboardGraphModel.SubmissionData = new PeriodicData();
                    dashboardGraphModel.SubmissionData.YearlyCount = periodicAllCountsList[0].SubmissionDataYearlyCount.ToString();
                    dashboardGraphModel.SubmissionData.MonthlyCount = periodicAllCountsList[0].SubmissionDataMonthlyCount.ToString();
                    dashboardGraphModel.SubmissionData.WeeklyCount = periodicAllCountsList[0].SubmissionDataWeeklyCount.ToString();
                    dashboardGraphModel.SubmissionData.DailyCount = periodicAllCountsList[0].SubmissionDataDailyCount.ToString();

                    //RequiremtnVSubmission periodic data
                    dashboardGraphModel.RequirementVsSubmission = new PeriodicData();
                    dashboardGraphModel.RequirementVsSubmission.YearlyCount = dashboardGraphModel.RequirementData.YearlyCount + "/" + dashboardGraphModel.SubmissionData.YearlyCount;
                    dashboardGraphModel.RequirementVsSubmission.MonthlyCount = dashboardGraphModel.RequirementData.MonthlyCount + "/" + dashboardGraphModel.SubmissionData.MonthlyCount;
                    dashboardGraphModel.RequirementVsSubmission.WeeklyCount = dashboardGraphModel.RequirementData.WeeklyCount + "/" + dashboardGraphModel.SubmissionData.WeeklyCount;
                    dashboardGraphModel.RequirementVsSubmission.DailyCount = dashboardGraphModel.RequirementData.DailyCount + "/" + dashboardGraphModel.SubmissionData.DailyCount;
                }
                //Candidates added periodic data 

                //-------------------------------------------------------------------
                /* Mongo DB Connection */
                //-------------------------------------------------------------------
                MongoClient client = new MongoClient(url);
                var database = client.GetDatabase(dbName);
                var collection = database.GetCollection<CandidateUserModel>("CandidateDetail");


                dashboardGraphModel.CandidateData = new PeriodicData();

                DateTime startToday = new DateTime(DateTime.Now.Date.Year, DateTime.Now.Date.Month, DateTime.Now.Date.Day);
                DateTime endToday = new DateTime(DateTime.Now.Date.Year, DateTime.Now.Date.Month, DateTime.Now.Date.Day,23,59,59);

                dashboardGraphModel.CandidateData.DailyCount = collection.AsQueryable().Where(t => t.CreatedOn >= startToday && t.CreatedOn <= endToday).Count().ToString();
                dashboardGraphModel.CandidateData.YearlyCount = collection.AsQueryable().Where(t => t.CreatedOn >= beforeYear).Count().ToString();
                dashboardGraphModel.CandidateData.MonthlyCount = collection.AsQueryable().Where(t => t.CreatedOn >= beforeMonth).Count().ToString();
                dashboardGraphModel.CandidateData.WeeklyCount = collection.AsQueryable().Where(t => t.CreatedOn >= beforeWeek).Count().ToString();
                //-------------------------------------------------------------------
                return dashboardGraphModel;
            }
        }

        /// <summary>
        /// Get week from the given date
        /// </summary>
        /// <param name="time"></param>
        /// <returns>week number</returns>
        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            var week = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        /// <summary>
        /// Get year from given date
        /// </summary>
        /// <param name="date"></param>
        /// <returns>year</returns>
        private static int GetYear(DateTime? date)
        {
            return Convert.ToDateTime(date).Year;
        }

        /// <summary>
        /// Get month from given date
        /// </summary>
        /// <param name="date"></param>
        /// <returns>return month</returns>
        private static int GetMonth(DateTime? date)
        {
            return Convert.ToDateTime(date).Month;
        }

        #endregion

        /// <summary>
        /// Get Dashboard Detail
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="userId">user Id</param>
        public static DashboardModel GetAllCounts(Guid organizationId)
        {
            DashboardModel model = new DashboardModel();
            using (var db = new XSeedEntities())
            {
                var param = new System.Data.SqlClient.SqlParameter("@OrganizationId", organizationId);
                //db.Database.ExecuteSqlCommand("SP_GetDashboardCount", param);
                List<DashboardModel> listCounts = db.Database.SqlQuery<DashboardModel>("SP_GetDashboardCount @OrganizationId", param).ToList();

                if (listCounts.Count > 0)
                    model = listCounts[0];
            }

            ///* Mongo DB Connection */
            //MongoClient client = new MongoClient(url);
            //var database = client.GetDatabase(dbName);
            //var collection = database.GetCollection<CandidateUserModel>("CandidateDetail").AsQueryable(new AggregateOptions { AllowDiskUse = true });

            ///* candidate info */
            //model.TotalCandidates = (int)collection.Count();
            //model.CandidatesAddedToday = (int)collection.Count(c => c.CreatedOn.Value.CompareTo(DateTime.Today) >= 0);

            return model;
        }

        /// <summary>
        /// Get Dashboard Detail
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="userId">user Id</param>
        public static DashboardModel GetCandidateCounts(Guid organizationId)
        {
            DashboardModel countModel = new DashboardModel();

            ///* Mongo DB Connection */
            MongoClient client = new MongoClient(url);
            var database = client.GetDatabase(dbName);
            var collectiondb = database.GetCollection<CandidateUserModel>("CandidateDetail");
            //Total candidates
            countModel.TotalCandidates = (int)collectiondb.Count(new BsonDocument());
            //Total candiates Today
            countModel.CandidatesAddedToday = (int)collectiondb.Find(Builders<CandidateUserModel>.Filter.Where(c => c.CreatedOn.Value.CompareTo(DateTime.Today) >= 0)).Count();

            return countModel;
        }
    }




    public class CandidateChartModel
    {
        public string Name { get; set; }
        public int Count { get; set; }
        public Guid UserId { get; set; }
    }

    public class SubmissionChartModel
    {
        public string Name { get; set; }
        public Guid UserId { get; set; }
        public int Count { get; set; }
    }

    public class JobStatusChartModel
    {
        public int Count { get; set; }
        public string Status { get; set; }
    }

    public class JobSubmissionChartModel
    {
        public int JobCount { get; set; }
        public int SubmissionCount { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public string MonthName { get; set; }
    }

    public class PeriodicData
    {
        public string DailyCount { get; set; }
        public string WeeklyCount { get; set; }
        public string MonthlyCount { get; set; }
        public string YearlyCount { get; set; }
    }

    public class DashboardGraphModel
    {
        public PeriodicData RequirementData { get; set; }
        public PeriodicData SubmissionData { get; set; }
        public PeriodicData CandidateData { get; set; }
        public PeriodicData RequirementVsSubmission { get; set; }
    }

    public class PeriodicAllCounts
    {
        public int RequirementDataYearlyCount { get; set; }
        public int RequirementDataMonthlyCount { get; set; }
        public int RequirementDataWeeklyCount { get; set; }
        public int RequirementDataDailyCount { get; set; }

        public int SubmissionDataYearlyCount { get; set; }
        public int SubmissionDataMonthlyCount { get; set; }
        public int SubmissionDataWeeklyCount { get; set; }
        public int SubmissionDataDailyCount { get; set; }
    }
}
