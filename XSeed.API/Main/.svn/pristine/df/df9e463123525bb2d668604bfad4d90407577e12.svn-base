﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using XSeed.API.Models;
using XSeed.Business.Vendor;
using XSeed.Data.ViewModel.Vendors;
using XSeed.Utility;

namespace XSeed.API.Controllers
{
    public class VendorController : BaseController
    {
        /// <summary>
        /// Get List of Vendors
        /// Get Vendor Detail
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="Id">Id</param>
        /// <returns></returns>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Vendor_Read", ClaimValue = "True")]
        public IHttpActionResult Get(Guid organizationId, Guid? Id = null)
        {
            VendorInfo model = new VendorInfo();

            if (Id == null)
            {
                return Ok(model.ListAllVendors(organizationId)); // return vendors list
            }
            else
            {
                return Ok(model.GetVendorDetail(organizationId, Id)); // return single vendor details
            }
        }

        /// <summary>
        /// Get Vendor Parsed Email
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>
        /// <returns>List of All Parsed Vendor Email</returns>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Vendor_Read", ClaimValue = "True")]
        public IHttpActionResult GetVendorParsedEmail(Guid organizationId, int pageSize, int pageNumber, string sortBy = "Date", string sortOrder = "desc",Boolean isMobileRequest=false)
        {
            VendorInfo model = new VendorInfo();

            /* Set Pagination Info */
            var paginationInfo = model.GetPaginationInfo(organizationId, pageSize);

            /* return job list */
            if (isMobileRequest)
            {
                var result = new
                {
                    TotalCount = paginationInfo.TotalCount,
                    totalPages = paginationInfo.TotalPages,
                    VendorEmail = model.ListAllParsedVendorEmail(organizationId, pageSize, pageNumber, sortBy, sortOrder)
                };

                return Ok(ResponseUtility.SuccessResponseData(result));
            }


            return Ok(model.ListAllParsedVendorEmail(organizationId, pageSize, pageNumber, sortBy, sortOrder));

            // return Ok(model.ListAllParsedVendorEmail(organizationId)); // return vendors parsed email list
        }

        /// <summary>
        /// Get Unread Mail Count 
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <returns>Unread Mail Count</returns>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Vendor_Read", ClaimValue = "True")]
        public IHttpActionResult GetUnreadMailCount(Guid organizationId,Boolean isMobileRequest=false)
        {
            VendorInfo model = new VendorInfo();
            if (isMobileRequest)
            {
                return Ok(ResponseUtility.SuccessResponseData(model.GetUnreadMailCount(organizationId)));
            }
            return Ok(model.GetUnreadMailCount(organizationId));
        }

        /// <summary>
        /// Update Vendor Parsed Email
        /// </summary>
        /// <param name="vendorEmailParserModel">Vendor Email Parser Model</param>
        /// <returns></returns>
        [HttpPut]
        [ClaimsAuthorization(ClaimType = "Vendor_Read", ClaimValue = "True")]
        public IHttpActionResult UpdateVendorParsedEmail(VendorEmailParserModel vendorEmailParserModel,Boolean isMobileRequest=false)
        {
            if (!ModelState.IsValid)
            {
                throw new ArgumentException();
            }

            // Add Vendor
            VendorInfo model = new VendorInfo();
            model.UpdateVendorParsedEmail(vendorEmailParserModel);
            if (isMobileRequest)
            {
                ResponseUtility.SuccessEmptyResponseData("Updated Successfully");
                return Ok();
            }
            return Ok();
        }

        /// <summary>
        /// Save Vendor Detail
        /// </summary>
        /// <param name="vendorDetailModel">Vendor Detail Model</param>
        /// <returns>VendorId</returns>
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Vendor_Create", ClaimValue = "True")]
        public async Task<IHttpActionResult> Post(VendorDetailModel vendorDetailModel)
        {
            if (!ModelState.IsValid)
            {
                throw new ArgumentException();
            }

            // Add Vendor
            VendorInfo model = new VendorInfo();
            return Ok(model.CreateVendor(vendorDetailModel));

        }

        /// <summary>
        /// Update Vendor Details
        /// </summary>
        /// <param name="vendorDetailModel">Vendor Detail Model</param>
        /// <returns></returns>
        [HttpPut]
        [ClaimsAuthorization(ClaimType = "Vendor_Update", ClaimValue = "True")]
        public IHttpActionResult Put(VendorDetailModel vendorDetailModel)
        {
            if (!ModelState.IsValid)
            {
                throw new ArgumentException();
            }

            // Add Vendor
            VendorInfo model = new VendorInfo();
            model.UpdateVendor(vendorDetailModel);
            return Ok();
        }
    }
}
