﻿using MongoDB.Bson;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using XSeed.Business.Common;
using XSeed.Data.ViewModel.Common;

namespace XSeed.API.Models
{
    /// <summary>
    /// Custom exception filter
    /// </summary>
    public class CustomExceptionFilter : ExceptionFilterAttribute
    {
        /// <summary>
        /// Override exception method
        /// </summary>
        /// <param name="actionExecutedContext">actionExecutedContext</param>
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception != null)
            {
                APIExceptionLoggerInfo.LogException(actionExecutedContext.Exception);
            }

            HttpStatusCode status = HttpStatusCode.InternalServerError;
            string message = string.Empty;
            var errorMessage = new ErrorMessage();

            var exceptionType = actionExecutedContext.Exception.GetType();

            if (exceptionType == typeof(AccessViolationException))
            {
                errorMessage = GetCustomExceptionMessage("EC40301");
                status = HttpStatusCode.Forbidden;

            }
            else if (exceptionType == typeof(AggregateException))
            {
                errorMessage = GetCustomExceptionMessage("EC50001");
                status = HttpStatusCode.InternalServerError;
            }
            else if (exceptionType == typeof(ApplicationException))
            {
                errorMessage = GetCustomExceptionMessage("EC50002");
                status = HttpStatusCode.InternalServerError;
            }
            else if (exceptionType == typeof(ArgumentException))
            {
                errorMessage = GetCustomExceptionMessage("EC40001");
                status = HttpStatusCode.BadRequest;
            }
            else if (exceptionType == typeof(ArgumentNullException))
            {
                errorMessage = GetCustomExceptionMessage("EC40001");
                status = HttpStatusCode.BadRequest;
            }
            else if (exceptionType == typeof(ArgumentOutOfRangeException))
            {
                errorMessage = GetCustomExceptionMessage("EC40001");
                status = HttpStatusCode.BadRequest;
            }
            else if (exceptionType == typeof(ArithmeticException))
            {
                errorMessage = GetCustomExceptionMessage("EC50001");
                status = HttpStatusCode.InternalServerError;
            }
            else if (exceptionType == typeof(ArrayTypeMismatchException))
            {
                errorMessage = GetCustomExceptionMessage("EC50001");
                status = HttpStatusCode.InternalServerError;
            }
            else if (exceptionType == typeof(BadImageFormatException))
            {
                errorMessage = GetCustomExceptionMessage("EC41501");
                status = HttpStatusCode.UnsupportedMediaType;
            }
            else if (exceptionType == typeof(CookieException))
            {
                errorMessage = GetCustomExceptionMessage("EC40004");
                status = HttpStatusCode.BadRequest;
            }
            else if (exceptionType == typeof(DivideByZeroException))
            {
                errorMessage = GetCustomExceptionMessage("EC50001");
                status = HttpStatusCode.InternalServerError;
            }
            else if (exceptionType == typeof(DllNotFoundException))
            {
                errorMessage = GetCustomExceptionMessage("EC50002");
                status = HttpStatusCode.InternalServerError;
            }
            else if (exceptionType == typeof(EntryPointNotFoundException))
            {
                errorMessage = GetCustomExceptionMessage("EC40401");
                status = HttpStatusCode.NotFound;
            }
            else if (exceptionType == typeof(FieldAccessException))
            {
                errorMessage = GetCustomExceptionMessage("EC50001");
                status = HttpStatusCode.InternalServerError;
            }
            else if (exceptionType == typeof(FormatException))
            {
                errorMessage = GetCustomExceptionMessage("EC40001");
                status = HttpStatusCode.BadRequest;
            }
            else if (exceptionType == typeof(HttpException))
            {
                errorMessage = GetCustomExceptionMessage("EC50002");
                status = HttpStatusCode.InternalServerError;
            }
            else if (exceptionType == typeof(HttpRequestValidationException))
            {
                errorMessage = GetCustomExceptionMessage("EC40004");
                status = HttpStatusCode.BadRequest;
            }
            else if (exceptionType == typeof(HttpUnhandledException))
            {
                errorMessage = GetCustomExceptionMessage("EC50001");
                status = HttpStatusCode.InternalServerError;
            }
            else if (exceptionType == typeof(IndexOutOfRangeException))
            {
                errorMessage = GetCustomExceptionMessage("EC50001");
                status = HttpStatusCode.InternalServerError;
            }
            else if (exceptionType == typeof(InsufficientMemoryException))
            {
                errorMessage = GetCustomExceptionMessage("EC50001");
                status = HttpStatusCode.InternalServerError;
            }
            else if (exceptionType == typeof(InvalidCastException))
            {
                errorMessage = GetCustomExceptionMessage("EC50001");
                status = HttpStatusCode.InternalServerError;
            }
            else if (exceptionType == typeof(KeyNotFoundException))
            {
                errorMessage = GetCustomExceptionMessage("EC50001");
                status = HttpStatusCode.InternalServerError;
            }
            else if (exceptionType == typeof(MemberAccessException))
            {
                errorMessage = GetCustomExceptionMessage("EC50001");
                status = HttpStatusCode.InternalServerError;
            }
            else if (exceptionType == typeof(MissingMethodException))
            {
                errorMessage = GetCustomExceptionMessage("EC40401");
                status = HttpStatusCode.NotFound;
            }
            else if (exceptionType == typeof(NotImplementedException))
            {
                errorMessage = GetCustomExceptionMessage("EC50001");
                status = HttpStatusCode.InternalServerError;
            }
            else if (exceptionType == typeof(NotSupportedException))
            {
                errorMessage = GetCustomExceptionMessage("EC40301");
                status = HttpStatusCode.Forbidden;
            }
            else if (exceptionType == typeof(NullReferenceException))
            {
                errorMessage = GetCustomExceptionMessage("EC50001");
                status = HttpStatusCode.InternalServerError;
            }
            else if (exceptionType == typeof(OutOfMemoryException))
            {
                errorMessage = GetCustomExceptionMessage("EC50001");
                status = HttpStatusCode.InternalServerError;
            }
            else if (exceptionType == typeof(SystemException))
            {
                errorMessage = GetCustomExceptionMessage("EC50002");
                status = HttpStatusCode.InternalServerError;
            }
            else if (exceptionType == typeof(TimeoutException))
            {
                errorMessage = GetCustomExceptionMessage("EC50002");
                status = HttpStatusCode.InternalServerError;
            }
            else if (exceptionType == typeof(UnauthorizedAccessException))
            {
                errorMessage = GetCustomExceptionMessage("EC40006");
                status = HttpStatusCode.Unauthorized;
            }
            else if (exceptionType == typeof(UnsupportedMediaTypeException))
            {
                errorMessage = GetCustomExceptionMessage("EC41501");
                status = HttpStatusCode.UnsupportedMediaType;
            }
            else if (exceptionType == typeof(OperationCanceledException))
            {
                errorMessage = GetCustomExceptionMessage("EC50301");
                status = HttpStatusCode.ServiceUnavailable;
            }
            else
            {
                if (exceptionType.Name == "EntityException")
                {
                    errorMessage = GetCustomExceptionMessage("EC50301");
                    status = HttpStatusCode.ServiceUnavailable;
                }
                else if (actionExecutedContext.Exception.Message.Contains("already exists."))
                {
                    errorMessage = GetCustomExceptionMessage("EC40901");
                    errorMessage.Message = actionExecutedContext.Exception.Message;
                    status = HttpStatusCode.Conflict;
                }
                else if (actionExecutedContext.Exception.Message.Contains("Candidate is already registered"))
                {
                    errorMessage = GetCustomExceptionMessage("EC40901");
                    errorMessage.Message = actionExecutedContext.Exception.Message;
                    status = HttpStatusCode.Conflict;
                }
                else if (actionExecutedContext.Exception.Message.Contains("Resume not parsed successfully!"))
                {
                    errorMessage = GetCustomExceptionMessage("EC40004");
                    errorMessage.Message = actionExecutedContext.Exception.Message;
                    status = HttpStatusCode.BadRequest;
                }
                else if (actionExecutedContext.Exception.Message.Contains("verify your email"))
                {
                    errorMessage = GetCustomExceptionMessage("EC40901");
                    errorMessage.Message = actionExecutedContext.Exception.Message;
                    status = HttpStatusCode.Conflict;
                }
                else if (actionExecutedContext.Exception.Message.Contains("already submitted"))
                {
                    errorMessage = GetCustomExceptionMessage("EC40901");
                    errorMessage.Message = actionExecutedContext.Exception.Message;
                    status = HttpStatusCode.Conflict;
                }
                else if (actionExecutedContext.Exception.Message.Contains("Resume not found"))
                {
                    errorMessage = GetCustomExceptionMessage("EC40401");
                    errorMessage.Message = actionExecutedContext.Exception.Message;
                    status = HttpStatusCode.Conflict;
                }
                else if (exceptionType.Name == "DbUpdateException")
                {
                    errorMessage = GetCustomExceptionMessage("EC50002");
                    status = HttpStatusCode.InternalServerError;
                }
                else if (actionExecutedContext.Exception.Message.Contains("The email or password is incorrect"))
                {
                    errorMessage = GetCustomExceptionMessage("EC40006");
                    errorMessage.Message = actionExecutedContext.Exception.Message;
                    status = HttpStatusCode.Conflict;
                }
                else if (actionExecutedContext.Exception.Message.Contains("Incorrect current password"))
                {
                    errorMessage = GetCustomExceptionMessage("EC40006");
                    errorMessage.Message = actionExecutedContext.Exception.Message;
                    status = HttpStatusCode.Conflict;
                }

                else if (actionExecutedContext.Exception.Message == "MonsterLicenceException")
                {
                    errorMessage = GetCustomExceptionMessage("MNSTR01");
                    status = HttpStatusCode.Conflict;
                }
                else if (actionExecutedContext.Exception.Message == "DownloadMonsterCandidateFailed")
                {
                    errorMessage = GetCustomExceptionMessage("MNSTR02");
                    status = HttpStatusCode.Conflict;
                }
                else if (actionExecutedContext.Exception.Message == "GetMonsterCandidateException")
                {
                    errorMessage = GetCustomExceptionMessage("MNSTR03");
                    status = HttpStatusCode.Conflict;
                }
                else if (actionExecutedContext.Exception.Message.Contains("Username not exists"))
                {
                    errorMessage = GetCustomExceptionMessage("EC40066");
                    errorMessage.Message = actionExecutedContext.Exception.Message;
                    status = HttpStatusCode.Conflict;
                }
                else
                {
                    errorMessage = GetCustomExceptionMessage("EC50002");
                    status = HttpStatusCode.InternalServerError;
                }
            }

            message = JsonConvert.SerializeObject(errorMessage);
            actionExecutedContext.Response = new HttpResponseMessage()
            {
                Content = new StringContent(message, System.Text.Encoding.UTF8, "text/json"),
                StatusCode = status,
                //Headers
                //ReasonPhrase
                //RequestMessage
                //Version
            };

            base.OnException(actionExecutedContext);
        }

        /// <summary>
        /// Get custom error message.
        /// </summary>
        /// <param name="errorCode">error Code</param>
        /// <returns>error message</returns>
        private ErrorMessage GetCustomExceptionMessage(string errorCode)
        {
            ErrorMessage message = new ErrorMessage();
            message.Language = "en_US";
            switch (errorCode)
            {
                case "EC40001":
                    message.Code = "EC40001";
                    message.Status = "400";
                    message.Message = "Invalid query or body parameters.";
                    break;

                case "EC40066":
                    message.Code = "EC40066";
                    message.Status = "400";
                    message.Message = "Username not exists.";
                    break;
                //case "EC40002":
                //    message.Code = "EC40002";
                //    message.Status = "400";
                //    message.Message = "Invalid Account ID, subscription did not match with Client Account ID.";
                //    break;
                //case "EC40003":
                //    message.Code = "EC40003";
                //    message.Status = "400";
                //    message.Message = "Request in Progress. Previously submitted request in progress.";
                //    break;
                case "EC40004":
                    message.Code = "EC40004";
                    message.Status = "400";
                    message.Message = "Invalid user request.";
                    break;
                //case "EC40005":
                //    message.Code = "EC40005";
                //    message.Status = "400";
                //    message.Message = "Id in the url does not match the id in the body.";
                //    break;
                case "EC40006":
                    message.Code = "EC40006";
                    message.Status = "401";
                    message.Message = "Invalid Authentication or not Authenticated.";
                    break;
                case "EC40301":
                    message.Code = "EC40301";
                    message.Status = "403";
                    message.Message = "Forbidden, Not Authorized.";
                    break;
                //case "EC40302":
                //    message.Code = "EC40302";
                //    message.Status = "403";
                //    message.Message = "Not enough privileges to execute this service request.";
                //    break;
                //case "EC40303":
                //    message.Code = "EC40303";
                //    message.Status = "400";
                //    message.Message = "Subscription not active.";
                //    break;
                case "EC40401":
                    message.Code = "EC40401";
                    message.Status = "404";
                    message.Message = "Not Found.";
                    break;
                case "EC40501":
                    message.Code = "EC40501";
                    message.Status = "405";
                    message.Message = "Method Not Allowed.";
                    break;
                //case "EC40601":
                //    message.Code = "EC40601";
                //    message.Status = "406";
                //    message.Message = "Not Acceptable.";
                //    break;
                case "EC40901":
                    message.Code = "EC40901";
                    message.Status = "409";
                    message.Message = "Resource already exists.";
                    break;
                case "EC41201":
                    message.Code = "EC41201";
                    message.Status = "412";
                    message.Message = "Missing required Header Field.";
                    break;
                case "EC41501":
                    message.Code = "EC41501";
                    message.Status = "415";
                    message.Message = "Unsupported Media Type.";
                    break;
                //case "EC42901":
                //    message.Code = "EC42901";
                //    message.Status = "429";
                //    message.Message = "Too Many Requests.";
                //    break;
                case "EC50001":
                    message.Code = "EC50001";
                    message.Status = "500";
                    message.Message = "Internal Server processing error, Recoverable.";
                    break;
                case "EC50002":
                    message.Code = "EC50002";
                    message.Status = "500";
                    message.Message = "Internal Server processing error, Not Recoverable.";
                    break;
                case "EC50301":
                    message.Code = "EC50301";
                    message.Status = "503";
                    message.Message = "Backend Systems not available.";
                    break;
                case "MNSTR01":
                    message.Code = "MNSTR01";
                    message.Status = "503";
                    message.Message = "Monster service not available.";
                    break;
                case "MNSTR02":
                    message.Code = "MNSTR02";
                    message.Status = "503";
                    message.Message = "Monster server processing error.";
                    break;
                case "MNSTR03":
                    message.Code = "MNSTR03";
                    message.Status = "503";
                    message.Message = "Monster Backend Systems not available.";
                    break;
                default:
                    message.Code = "EC40004";
                    message.Status = "400";
                    message.Message = "Bad request.";
                    break;
            }

            return message;
        }
    }

    /// <summary>
    /// Validate Model State filter
    /// </summary>
    public class ValidateModelStateAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// Override On Action Executing Method
        /// </summary>
        /// <param name="actionContext">Http Action Context</param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {

            if (!actionContext.ModelState.IsValid)
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, actionContext.ModelState);
            }
        }
    }

    /// <summary>
    /// Custom Authorize Class 
    /// </summary>
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// Override On Authorization Executing Method
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {

            if (AuthorizeRequest(actionContext))
            {
                return;
            }

            HandleUnauthorizedRequest(actionContext);

        }

        /// <summary>
        /// Handle Unauthorized Request
        /// </summary>
        /// <param name="actionContext"></param>
        protected override void HandleUnauthorizedRequest(System.Web.Http.Controllers.HttpActionContext actionContext)
        {

            //Code to handle unauthorized request

        }

        /// <summary>
        /// Authorize Request
        /// </summary>
        /// <param name="actionContext">Http Action Context</param>
        /// <returns>Is authorized flag</returns>
        private bool AuthorizeRequest(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
                /* Get Header */
                System.Net.Http.Headers.HttpRequestHeaders headers = actionContext.Request.Headers;

                /* Set Default Value */
                BaseModel.InitiatedBy = Guid.Empty;
                BaseModel.CandidateInitiatedBy = string.Empty;
                BaseModel.UserType = string.Empty;

                /* Get User Id */
                if (headers != null && headers.Contains("UserId"))
                {
                    var headerInfo = headers.GetValues("UserId").First();
                    //  var createdOnInfo = headers.GetValues("CreatedOn").First();

                    if (headers.Contains("Token"))
                    {
                        BaseModel.CandidateInitiatedBy = headerInfo != null && headerInfo != "" ? headerInfo : string.Empty;
                        BaseModel.UserType = "Candidate";
                    }
                    else
                    {
                        BaseModel.InitiatedBy = headerInfo != null && headerInfo != "" ? Guid.Parse(headerInfo) : Guid.Empty;
                        BaseModel.CandidateInitiatedBy = headerInfo != null && headerInfo != "" ? headerInfo : string.Empty;
                        BaseModel.UserType = "Recruiter";
                    }

                    //  BaseModel.InitiatedOn = createdOnInfo != null && createdOnInfo != "" ? DateTime.Parse(createdOnInfo) : DateTime.Now;
            }

            return true;

        }

    }

    public class ErrorMessage
    {
        public string Status { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }
        public string Language { get; set; }
    }
}