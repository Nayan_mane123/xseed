﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XSeed.Business.Dashboard;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Dashboard API
    /// </summary>    
    public class DashboardController : BaseController
    {
        /// <summary>
        /// Get Dashboard Detail
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="userId">user Id</param>
        /// <returns>Dashboard Model</returns>
        [HttpGet]
        public IHttpActionResult Get(Guid organizationId, Guid? userId = null)
        {
            DashboardInfo model = new DashboardInfo();
            return Ok(model.GetDashboardDetail(organizationId, userId)); // return dashboard detail           
        }

        /// <summary>
        /// Get Candidate Chart Detail
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="userId">user Id</param>
        /// <returns>Candidate Chart Detail</returns>
        [HttpGet]
        public IHttpActionResult GetCandidateChartDetail(Guid organizationId, Guid? userId = null)
        {
            DashboardInfo model = new DashboardInfo();
            return Ok(model.GetCandidateChartDetail(organizationId, userId)); // return Candidate Chart Detail
        }

        /// <summary>
        /// Get Submission Chart Detail
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="userId">user Id</param>
        /// <returns>Submission Chart Detail</returns>
        [HttpGet]
        public IHttpActionResult GetSubmissionChartDetail(Guid organizationId, Guid? userId = null)
        {
            DashboardInfo model = new DashboardInfo();
            return Ok(model.GetSubmissionChartDetail(organizationId, userId)); // return Submission Chart Detail
        }

        /// <summary>
        /// Get Job Status Chart Detail
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="userId">user Id</param>
        /// <returns>Job Status Chart Detail</returns>
        [HttpGet]
        public IHttpActionResult GetJobStatusChartDetail(Guid organizationId, Guid? userId = null)
        {
            DashboardInfo model = new DashboardInfo();
            return Ok(model.GetJobStatusChartDetail(organizationId, userId)); // return Submission Chart Detail
        }


        /// <summary>
        /// Get job Submission Chart Chart Detail
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="userId">user Id</param>
        /// <returns>job Submission Chart Detail</returns>
        [HttpGet]
        public IHttpActionResult GetjobSubmissionChartDetail(Guid organizationId, Guid? userId = null,string period=null)
        {
            DashboardInfo model = new DashboardInfo();
            return Ok(model.GetjobSubmissionChartDetail(organizationId, userId,period)); // return Job Submission Chart Detail
        }

        /// <summary>
        /// Get dashboard chart periodic data
        /// </summary>
        /// <param name="organizationId"></param>
        /// <param name="userId"></param>
        /// <returns>dashboard chart periodic data</returns>
        [HttpGet]
        public IHttpActionResult GetDashboardPeriodicData(Guid organizationId, Guid? userId = null)
        {
            DashboardInfo model = new DashboardInfo();
            return Ok(model.GetDashboardPeriodicData(organizationId, userId));
        }
    }
}
