﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using XSeed.API.Models;
using XSeed.Business.Job;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Job;
using XSeed.Data.ViewModel.Activity;
using XSeed.MonsterJobPortalAPI;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Job API
    /// </summary>
    public class JobController : BaseController
    {

        /// <summary>
        /// Get detail of job(s) 
        /// </summary>        
        /// <param name="organizationId">Organization Id</param>
        /// <param name="Id">Id</param>
        /// <remarks>Get all jobs belongs to an organization  
        /// - Organization Id is required parameter
        /// - Id is an optional parameter
        /// - Id with value as zero will return list of jobs belongs to an organization
        /// - Id with value as positive integer will return single job details
        ///</remarks>
        /// <returns>job(s</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")]
        public IHttpActionResult Get(Guid organizationId, Guid? Id = null)
        {
            JobInfo model = new JobInfo();

            if (Id == null)
            {
                HttpContext.Current.Response.AppendHeader("TotalCount", "20");
                return Ok(model.ListAllJobs(organizationId)); // return job list
            }
            else
            {
                return Ok(model.GetJobDetail(organizationId, Id)); // return single job
            }
        }

        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")]
        public IHttpActionResult GetCollpasingJob(Guid organizationId, Guid? Id = null)
        {
            JobInfo model = new JobInfo();
            return Ok(model.GetCollapsingJobDetail(organizationId, Id)); // return single job
            
        }
        /// <summary>
        /// Get list of jobs using server side pagination
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>        
        /// <param name="sortBy">Sort by</param>
        /// <param name="sortOrder">Sort Order</param>
        /// <returns>Job List</returns>
        // GET: api/Jobs/pageSize/pageNumber/
        //[Route("{pageSize:int}/{pageNumber:int}")]

        [HttpDelete]
        public async Task<IHttpActionResult> DeleteQuickNotes(string qnoteId)
        {
            if (qnoteId == null)
            {
                throw new ArgumentNullException("QNotesId"); ;
            }

            JobInfo model = new JobInfo();
            model.DeleteQuickNotes(qnoteId);
            return Ok();
        }

        [HttpPut]
        public IHttpActionResult UpdateQNote(QNoteModel request, string editid = null)
        {
            JobInfo model = new JobInfo();
            
            model.UpdateQNote(request, editid);
            return Ok();
        }

        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")]
        public IHttpActionResult Get(Guid organizationId, int pageSize, int pageNumber, string sortBy = "ModifiedOn", string sortOrder = "desc", bool isExport = false, bool loadPageData = true)
        {
            JobInfo model = new JobInfo();

            /* Set Pagination Info */
            if (loadPageData)
            {
                var paginationInfo = model.GetPaginationInfo(organizationId, pageSize);

                var result = new
                {
                    TotalCount = paginationInfo.TotalCount,
                    totalPages = paginationInfo.TotalPages,
                    Jobs = model.ListAllJobs(organizationId, pageSize, pageNumber, sortBy, sortOrder, isExport)
                };

                return Ok(result);
            }
            else
            {
                var result = new
                {
                    Jobs = model.ListAllJobs(organizationId, pageSize, pageNumber, sortBy, sortOrder, isExport)
                };

                return Ok(result);
            }
        }

        /// <summary>
        /// Get list of jobs by user and status using server side pagination
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <param name="userId">user Id</param>
        /// <param name="status">status</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param> 
        /// <param name="sortBy">Sort by</param>
        /// <param name="sortOrder">Sort Order</param>
        /// <returns>Job List</returns>
        public IHttpActionResult GetJobByUser(Guid organizationId, Guid userId, string status = null, int pageSize = 10, int pageNumber = 1, string sortBy = "ModifiedOn", string sortOrder = "desc",Boolean isPriority=false)
        {
            JobInfo model = new JobInfo();

            /* Set Pagination Info */
            var paginationInfo = model.GetPaginationInfo(organizationId, userId, status, pageSize, isPriority);

            /* return job list */
            var result = new
            {
                TotalCount = paginationInfo.TotalCount,
                totalPages = paginationInfo.TotalPages,
                Jobs = model.ListAllJobs(organizationId, userId, status, pageSize, pageNumber, sortBy, sortOrder, isPriority)
            };

            return Ok(result);
        }

        /// <summary>
        /// Create a new job for on organization
        /// </summary>
        /// <param name="jobDetailModel">Job detail model </param>
        /// <remarks>Create a new job for organization</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Requirement_Create", ClaimValue = "True")]
        public IHttpActionResult Post(JobDetailModel jobDetailModel)
        {
            JobInfo model = new JobInfo();
            return Ok(model.CreateJob(jobDetailModel));
        }

        /// <summary>
        /// Update the details for a job
        /// </summary>
        /// <param name="jobDetailModel">Job detail model </param>
        /// <remarks>Update the details about the job</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPut]
        [ClaimsAuthorization(ClaimType = "Requirement_Update", ClaimValue = "True")]
        public IHttpActionResult Put(JobDetailModel jobDetailModel)
        {
            JobInfo model = new JobInfo();
            model.UpdateJob(jobDetailModel);
            return Ok();
        }

        /// <summary>
        /// Update the status for a job
        /// </summary>
        /// <param name="id">id </param>
        /// <param name="jobStatusId">jobStatusId </param>
        /// <remarks>Update the details about the job</remarks>        
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPut]
        [ClaimsAuthorization(ClaimType = "Requirement_Update", ClaimValue = "True")]
        public IHttpActionResult UpdateJobStatus(Guid id, Guid jobStatusId)
        {
            JobInfo model = new JobInfo();
            model.UpdateJobStatus(id, jobStatusId);
            return Ok();
        }

        /// <summary>
        /// Get job list by company
        /// </summary>        
        /// <param name="CompanyId">CompanyId</param>
        /// <returns>Job list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")]
        public IHttpActionResult GetJobsByCompany(Guid CompanyId)
        {
            JobInfo model = new JobInfo();
            return Ok(model.GetJobsByCompany(CompanyId));
        }

        /// <summary>
        /// Get list of candidates associated with the job
        /// </summary>        
        /// <param name="jobId">Job Id</param>
        /// <returns>List of associated candidates and their status</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")]
        public IHttpActionResult GetAssociatedCandidateList(Guid jobId)
        {
            JobInfo model = new JobInfo();
            return Ok(model.GetAssociatedCandidateList(jobId));
        }

        /// <summary>
        /// Get Requisition Id based on Organization
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <returns>Requisition Id</returns>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Create", ClaimValue = "True")]
        public IHttpActionResult GetRequisitionId(Guid organizationId)
        {
            JobInfo model = new JobInfo();
            return Ok(model.GetRequisitionId(organizationId));
        }

        /// <summary>
        /// Check for Duplicate Requisition Id
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <param name="requisitionId">Requisition Id</param>
        /// <returns>True/ False</returns>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Create", ClaimValue = "True")]
        public IHttpActionResult IsDuplicateRequisitionId(Guid organizationId, string requisitionId)
        {
            JobInfo model = new JobInfo();
            return Ok(model.IsDuplicateRequisitionId(organizationId, requisitionId));
        }

        /// <summary>
        /// Add new business unit
        /// </summary>
        /// <param name="lookUpModel">LookUpModel</param>
        /// <param name="organizationId">organization Id</param>
        /// <returns>Business Unit Id</returns>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Requirement_Create", ClaimValue = "True")]
        public IHttpActionResult AddBusinessUnit(LookUpModel lookUpModel)
        {
            JobInfo model = new JobInfo();
            return Ok(model.AddBusinessUnit(lookUpModel));
        }

        /// <summary>
        /// Update business unit information
        /// </summary>
        /// <param name="lookUpModel">Look Up Model</param>
        /// <returns>Http Ok</returns>
        [HttpPut]
        public async Task<IHttpActionResult> PutBusinessUnit(LookUpModel lookUpModel)
        {
            JobInfo.PutBusinessUnit(lookUpModel);
            return Ok();
        }

        /// <summary>
        /// Delete Business Unit
        /// </summary>
        /// <param name="genericLookupId">Generic Lookup Id</param>
        /// <returns>Http Ok</returns>
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteBusinessUnit(Guid genericLookupId)
        {
            if (genericLookupId == null || genericLookupId == Guid.Empty)
            {
                throw new ArgumentNullException("BusinessUnitId"); ;
            }

            JobInfo.DeleteBusinessUnit(genericLookupId);
            return Ok();
        }

        /// <summary>
        /// Save Quick Note related to Job
        /// </summary>
        /// <param name="note">Note</param>
        /// <param name="jobId">Job Id</param>
        /// <returns>Notes</returns>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")]
        public IHttpActionResult SaveQuickNote(string note, Guid jobId)
        {
            JobInfo model = new JobInfo();
            return Ok(model.SaveQuickNote(note, jobId));
        }

        /// <summary>
        /// Get Quick Notes related to Job
        /// </summary>
        /// <param name="jobId">Job Id</param>
        /// <returns>Quick Notes</returns>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")]
        public IHttpActionResult GetQuickNotes(Guid jobId)
        {
            JobInfo model = new JobInfo();
            return Ok(model.GetQuickNotes(jobId));
        }

        /// <summary>
        /// Get Activity Log related to Job
        /// </summary>
        /// <param name="jobId">Job Id</param>
        /// <returns>Activity Logs</returns>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")]
        public IHttpActionResult GetJobActivityLog(Guid jobId)
        {
            JobInfo model = new JobInfo();
            return Ok(model.GetJobActivityLog(jobId));
        }

        /// <summary>
        /// Source Candidates against search parameters
        /// </summary>
        /// <param name="searchModel">Candidate Source Search Model</param>
        /// <param name="pageSize">page Size</param>
        /// <param name="pageNumber">page Number</param>
        /// <param name="sortBy">sort By</param>
        /// <param name="sortOrder">sort Order</param>
        /// <param name="isExport">Is Export</param>
        /// <returns>Candidates</returns>
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")]
        public IHttpActionResult SourceCandidates(CandidateSourceSearchModel searchModel, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", bool isExport = false)
        {
            JobInfo model = new JobInfo();

            /* Set Pagination Info */
            var paginationInfo = model.GetSourceSearchPaginationInfo(searchModel, pageSize);

            /* return job list */
            var result = new
            {
                TotalCount = paginationInfo.TotalCount,
                totalPages = paginationInfo.TotalPages,
                candidates = model.SourceCandidates(searchModel, pageSize, pageNumber, sortBy, sortOrder, isExport)
            };

            return Ok(result);
        }

        /// <summary>
        /// Get Monster portal candidates based on skillset provided
        /// </summary>
        /// <param name="skillSet">skillset</param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult GetMonsterCandidates(string skillSet)
        {
            MonsterResumeAPI resumeAPI = new MonsterResumeAPI();
            var Candidates = resumeAPI.GetMonsterCandidates(skillSet);
            return Ok(Candidates);
        }

        /// <summary>
        /// Download candidates resume from Monster
        /// </summary>
        /// <param name="resumeId">monster resume Id</param>
        /// <param name="resumeModifiedDate"> monster resume modified date</param>
        /// <param name="firstName">candidates first name</param>
        /// <param name="lastName">candidates last name</param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult DownloadMonsterCandidate(string resumeId, string resumeModifiedDate, string firstName, string lastName)
        {
            MonsterResumeAPI resumeAPI = new MonsterResumeAPI();
            string filePath = resumeAPI.GetCandidateResume(resumeId, resumeModifiedDate, firstName, lastName);
            return Ok(filePath);
        }

        /// <summary>
        /// Get Monster Lincense Info
        /// </summary>
        /// <returns>Portal License Info</returns>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")]
        public IHttpActionResult GetMonsterLicenseInfo()
        {
            JobInfo model = new JobInfo();
            return Ok(model.GetMonsterLicenseInfo()); 
        }

        /// <summary>
        /// List Active Requirements to display on tCognition Website
        /// </summary>
        /// <returns>Active Jobs</returns>
        // [EnableCors(origins: "http://www.tcognition.com", headers: "*", methods: "*")]
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult ListActiveRequirements(Nullable<Guid> Id = null, string countryId = "")
        {
            JobInfo model = new JobInfo();
            return Ok(model.ListActiveRequirements(Id, countryId)); // return job list
        }
    }
}
