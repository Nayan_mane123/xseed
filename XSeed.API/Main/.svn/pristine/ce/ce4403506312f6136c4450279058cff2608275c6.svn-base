﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Activity;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.User.CandidateUser;

namespace XSeed.Data.ViewModel.Job
{
    public class JobDetailModel
    {
        #region Property Declaration

        public Guid Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.CompanyContact), ErrorMessageResourceName = "CompanyIdRequired")]
        public Nullable<Guid> CompanyId { get; set; }
        public string CompanyName { get; set; }
        public Nullable<Guid> CompanyContactId { get; set; }
        public string CompanyContactName { get; set; }
        public string CompanyContactEmail { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Job), ErrorMessageResourceName = "JobTitleIdRequired")]
        public string JobTitle { get; set; }
        public string InternalJobTitle { get; set; }
        public Nullable<Guid> JobTypeId { get; set; }
        public string JobType { get; set; }
        public Nullable<Guid> JobStatusId { get; set; }
        public string JobStatus { get; set; }
        public string InterviewType { get; set; }
        public string JobDescription { get; set; }
        public string RequisitionId { get; set; }
        public string ClientJobCode { get; set; }
        public Nullable<int> TotalPositions { get; set; }
        public Nullable<System.DateTime> DriveFromDate { get; set; }
        public Nullable<System.DateTime> DriveToDate { get; set; }
        public string Priority { get; set; }
        public Nullable<Guid> CountryId { get; set; }
        public string Country { get; set; }
        public Nullable<Guid> StateId { get; set; }
        public string State { get; set; }
        public Nullable<Guid> CityId { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Location { get; set; }
        public string TechnicalSkills { get; set; }
        public string Qualification { get; set; }
        public Nullable<int> ExperienceInYear { get; set; }
        public Nullable<int> ExperienceInMonth { get; set; }
        public Nullable<int> USExperienceInYear { get; set; }
        public Nullable<int> USExperienceInMonth { get; set; }
        public Nullable<System.DateTime> JobPostedDate { get; set; }
        public Nullable<System.DateTime> JobClosedDate { get; set; }
        public Nullable<System.DateTime> JobExpiryDate { get; set; }
        public string HtmlText { get; set; }
        public int AppliedCandidateCount { get; set; }
        public string Duration { get; set; }
        public Nullable<System.Guid> VisaTypeId { get; set; }
        public string VisaType { get; set; }
        public Nullable<decimal> Margin { get; set; }
        public Nullable<decimal> MinSalary { get; set; }
        public Nullable<decimal> MaxSalary { get; set; }
        public Nullable<System.Guid> SalaryTypeId { get; set; }
        public Nullable<System.Guid> CurrencyTypeId { get; set; }
        public string FeePercent { get; set; }
        public Nullable<decimal> ClientRate { get; set; }
        public Nullable<decimal> PayRate { get; set; }
        public string ClientRateType { get; set; }
        public string PayRateType { get; set; }
        public Nullable<decimal> PerDiem { get; set; }
        public string SecurityClearance { get; set; }
        public Nullable<System.Guid> BusinessUnit { get; set; }
        public string BusinessUnitName { get; set; }
        public string CommunicationSkills { get; set; }
        public Nullable<decimal> WeeklyHours { get; set; }
        public Nullable<decimal> TotalHours { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string ParsedEmailHTMLContent { get; set; }
        public Nullable<DateTime> ParsedEmailDate { get; set; }
        public string ParsedEmailSubject { get; set; }
        public string ParsedEmailSourceAddress { get; set; }

        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }

        //[Required(ErrorMessageResourceType = typeof(Resources.OrganizationUser), ErrorMessageResourceName = "OrgnizationUserIDRequired")]
        public List<LookUpModel> OrganizationUser { get; set; }
        public List<LookUpModel> DegreeList { get; set; }
        public List<LookUpModel> VisaTypeList { get; set; }
        public List<MongoLookUpModel> AssociateCandidate { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get List of All Jobs
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <returns>List of All Jobs</returns>
        public static List<JobDetailModel> ListAllJobs(Guid organizationId)
        {
            return (JobDetail.ListAllJobs(organizationId));
        }

        /// <summary>
        /// Get list of jobs using server side pagination
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>        
        /// <returns>Jobs List</returns>
        public static List<JobDetailModel> ListAllJobs(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", bool isExport = false)
        {
            return (JobDetail.ListAllJobs(organizationId, pageSize, pageNumber, sortBy, sortOrder, isExport));
        }

        /// <summary>
        /// Get list of jobs using server side pagination
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <param name="userId">user Id</param>
        /// <param name="status">status</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>        
        /// <returns>Jobs List</returns>
        public static List<JobDetailModel> ListUserJobsByStatus(Guid organizationId, Guid userId, string status, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc")
        {
            return JobDetail.ListUserJobsByStatus(organizationId, userId, status, pageSize, pageNumber, sortBy, sortOrder);
        }

        /// <summary>
        /// Get Job Details
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns>JobDetailModel</returns>
        public static JobDetailModel GetJobDetail(Guid organizationId, Guid? jobId)
        {
            return (JobDetail.GetJobDetail(organizationId, jobId));
        }

        /// <summary>
        /// Create Job
        /// </summary>
        /// <param name="jobDetailModel">Job Detail Model</param>
        /// <returns>Id of Newly Added Job</returns>
        public static Guid CreateJob(JobDetailModel jobDetailModel)
        {
            return (JobDetail.SaveJobInfo(jobDetailModel, true));
        }

        /// <summary>
        /// Update Job
        /// </summary>
        /// <param name="jobDetailModel">Job Detail Model</param>
        /// <returns></returns>
        public static void UpdateJob(JobDetailModel jobDetailModel)
        {
            JobDetail.SaveJobInfo(jobDetailModel, false);
        }

        /// <summary>
        /// Update Job Status
        /// </summary>
        /// <param name="id">JobId</param>
        /// <param name="jobStatusId">JobStatusId</param>
        public static void UpdateJobStatus(Guid jobId, Guid jobStatusId)
        {
            JobDetail.UpdateJobStatus(jobId, jobStatusId);
        }

        /// <summary>
        /// Get job list by company
        /// </summary>
        /// <param name="CompanyId">CompanyId</param>
        /// <returns>Job list</returns>
        public static List<JobDetailModel> GetJobsByCompany(Guid CompanyId)
        {
            return (JobDetail.GetJobsByCompany(CompanyId));
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        public static PaginationModel GetPaginationInfo(Guid organizationId, int pageSize)
        {
            return JobDetail.GetPaginationInfo(organizationId, pageSize);
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        public static PaginationModel GetPaginationInfo(Guid organizationId, Guid userId, string status, int pageSize)
        {
            return JobDetail.GetPaginationInfo(organizationId, userId, status, pageSize);
        }

        /// <summary>
        /// Get Job openings related to candidate skills
        /// </summary>
        /// <param name="skills">Candidate Skills</param>
        /// <returns>Job Openings</returns>
        internal static List<JobDetailModel> ListJobOpenings(List<JobDetail> matchedJobs)
        {
            return (JobDetail.ListJobOpenings(matchedJobs));
        }

        /// <summary>
        /// Add new business unit
        /// </summary>
        /// <param name="lookUpModel">LookUpModel</param>
        /// <param name="organizationId">organization Id</param>
        /// <returns>Business Unit Id</returns>
        public static Guid AddBusinessUnit(LookUpModel lookUpModel)
        {
            return BusinessUnitMaster.AddBusinessUnit(lookUpModel);
        }

        /// <summary>
        /// Save quick notes related to Job
        /// </summary>
        /// <param name="note">Note</param>
        /// <param name="jobId">Job Id</param>
        /// <returns>Notes</returns>
        public static List<Note> SaveQuickNote(string note, Guid jobId)
        {
            return JobActivityLogModel.SaveQuickNote(note, jobId);
        }

        /// <summary>
        /// Get Quick Notes related to Job
        /// </summary>
        /// <param name="jobId">Job Id</param>
        /// <returns>Quick Notes</returns>
        public static List<Note> GetQuickNotes(Guid jobId)
        {
            return JobActivityLogModel.GetQuickNotes(jobId);
        }

        /// <summary>
        /// Get Activity Log related to Job
        /// </summary>
        /// <param name="jobId">Job Id</param>
        /// <returns>Activity Logs</returns>
        public static List<JobActivityLogModel> GetJobActivityLog(Guid jobId)
        {
            return JobActivityLogModel.GetJobActivityLog(jobId);
        }

        /// <summary>
        /// Update Business Unit
        /// </summary>
        /// <param name="lookUpModel"></param>
        /// <param name="organizationId"></param>
        public static void PutBusinessUnit(LookUpModel lookUpModel)
        {
            BusinessUnitMaster.PutBusinessUnit(lookUpModel);
        }

        /// <summary>
        /// Delete Business Unit
        /// </summary>
        /// <param name="genericLookupId"></param>
        public static void DeleteBusinessUnit(Guid genericLookupId)
        {
            BusinessUnitMaster.DeleteBusinessUnit(genericLookupId);
        }

        /// <summary>
        /// Get Requisition Id based on Organization
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <returns>Requisition Id</returns>
        public string GetRequisitionId(Guid organizationId)
        {
            string ReqId = "Req-10001";

            using (var db = new XSeedEntities())
            {
                if (db.JobDetails.Any(o => o.CompanyDetail.OrganizationId == organizationId))
                {
                    var jobDetail = db.JobDetails.OrderByDescending(c => c.CreatedOn).FirstOrDefault(o => o.CompanyDetail.OrganizationId == organizationId && o.RequisitionId.Contains("Req-"));

                    if (jobDetail != null)
                    {
                        ReqId = GetNextSequence(jobDetail.RequisitionId);

                        while (IsDuplicateRequisitionId(organizationId, ReqId))
                        {
                            ReqId = GetNextSequence(ReqId); // check for next sequence
                        }
                    }
                }
            }

            return ReqId;
        }

        /// <summary>
        /// Check for Duplicate Requisition Id
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <param name="requisitionId">Requisition Id</param>
        /// <returns>True/ False</returns>
        public bool IsDuplicateRequisitionId(Guid organizationId, string requisitionId)
        {
            using (var db = new XSeedEntities())
            {
                if (db.JobDetails.Any(o => o.CompanyDetail.OrganizationId == organizationId && o.RequisitionId == requisitionId))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Get Next Sequence for Requisition Id
        /// </summary>
        /// <param name="RequisitionId">Requisition Id</param>
        /// <returns>Requisition Id</returns>
        private string GetNextSequence(string RequisitionId)
        {
            // Get number from last RequisitionId
            var number = Regex.Match(RequisitionId, @"\d+").Value;

            // increment by 1
            return "Req-" + (Convert.ToInt32(number) + 1).ToString();
        }

        /// <summary>
        /// Source Candidates against search parameters
        /// </summary>
        /// <param name="searchModel">Candidate Source Search Model</param>
        /// <param name="pageSize">page Size</param>
        /// <param name="pageNumber">page Number</param>
        /// <param name="sortBy">sort By</param>
        /// <param name="sortOrder">sort Order</param>
        /// <param name="isExport">Is Export</param>
        /// <returns>Candidates</returns>
        public static List<CandidateSourceResultModel> SourceCandidates(CandidateSourceSearchModel searchModel, int pageSize, int pageNumber, string sortBy, string sortOrder, bool isExport)
        {
            CandidateSourceSearchModel model = new CandidateSourceSearchModel();
            return model.SourceCandidates(searchModel, pageSize, pageNumber, sortBy, sortOrder, isExport);
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>
        /// <param name="jobId">Job Id</param>
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        public static PaginationModel GetSourceSearchPaginationInfo(CandidateSourceSearchModel searchModel, int pageSize)
        {
            return searchModel.GetSourceSearchPaginationInfo(searchModel, pageSize);
        }

        /// <summary>
        /// Get Monster Lincense Info
        /// </summary>
        /// <returns>Portal License Info</returns>
        public static PortalLicenseInfo GetMonsterLicenseInfo()
        {
            CandidateSourceSearchModel searchModel = new CandidateSourceSearchModel();
            return searchModel.GetMonsterLincenseInfo();
        }

        /// <summary>
        /// List Active Requirements to display on tCognition Website
        /// </summary>
        /// <returns>Active Jobs</returns>
        public static object ListActiveRequirements(Nullable<Guid> Id = null)
        {
            using (var db = new XSeedEntities())
            {
                if (Id != null)
                {
                    var job = db.JobDetails.Find(Id);

                    if (job != null)
                    {
                        return (new
                        {
                            Id = job.Id,
                            JobCode = job.RequisitionId,
                            Title = job.JobTitle,
                            Skills = job.TechnicalSkills,
                            Location = job.Location,
                            Position = job.TotalPositions,
                            Description = job.JobDescription,
                            ExperienceInYear = job.ExperienceInYear,
                            ExperienceInMonth = job.ExperienceInMonth,
                            Country = job.CountryMaster != null ? job.CountryMaster.Name : "",
                            State = job.StateMaster != null ? job.StateMaster.Name : "",
                            City = job.CityMaster != null ? job.CityMaster.Name : "",
                            PostedDate = job.CreatedOn,
                        });
                    }


                }
                else
                {
                    var jobs = db.JobDetails.Where(j => j.CompanyDetail.OrganizationDetail.Name == "tCognition USA" && j.IsActive != false && j.JobStatusMaster.Status != "Closed" && j.JobStatusMaster.Status != "Archive").OrderByDescending(c => c.CreatedOn).Take(100).ToList();

                    if (jobs != null)
                    {
                        return jobs.Select(j => new
                        {
                            Id = j.Id,
                            JobCode = j.RequisitionId,
                            Title = j.JobTitle,
                            Skills = j.TechnicalSkills,
                            Location = j.Location,
                            Position = j.TotalPositions,
                            Description = j.JobDescription,
                            ExperienceInYear = j.ExperienceInYear,
                            ExperienceInMonth = j.ExperienceInMonth,
                            Country = j.CountryMaster != null ? j.CountryMaster.Name : "",
                            State = j.StateMaster != null ? j.StateMaster.Name : "",
                            City = j.CityMaster != null ? j.CityMaster.Name : "",
                            PostedDate = j.CreatedOn,
                        }).ToList();
                    }
                }
            }

            return null;
        }

        #endregion
    }

    public class JobCandidateMapModel
    {
        #region Property Declaration

        public string CandidateId { get; set; }
        public string CandidateName { get; set; }
        public Guid? SubmittedBy { get; set; }
        public string SubmittedByName { get; set; }
        public string Status { get; set; }
        public string Remark { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get list of candidates associated with the job
        /// </summary>
        /// <param name="jobId">Job Id</param>
        /// <returns>List of associated candidates and their status</returns>
        public static List<JobCandidateMapModel> GetAssociatedCandidateList(Guid jobId)
        {
            List<JobCandidateMapModel> list = new List<JobCandidateMapModel>();

            using (var db = new XSeedEntities())
            {
                /* Get submission info */
                var submissions = db.SubmissionDetails.Where(s => s.JobId == jobId).ToList();

                foreach (var submission in submissions)
                {
                    JobCandidateMapModel model = new JobCandidateMapModel();

                    /* candidate info */
                    var candidate = CandidateUserModel.GetCandidateUserDetail(submission.CandidateId);
                    model.CandidateId = candidate != null ? candidate._id : "";
                    model.CandidateName = candidate != null ? (candidate.FirstName + " " + candidate.LastName) : string.Empty;

                    /* user info */
                    OrganizationUserDetail user = db.OrganizationUserDetails.FirstOrDefault(u => u.UserId == submission.CreatedBy);
                    model.SubmittedBy = user != null ? user.UserId : Guid.Empty;
                    model.SubmittedByName = user != null ? (user.FirstName + " " + user.LastName) : string.Empty;

                    /* status info */
                    model.Status = submission.SubmissionStatusMaster != null ? submission.SubmissionStatusMaster.Status : string.Empty;

                    /* submission feedback info */
                    SubmissionFeedback feedback = submission.SubmissionFeedbacks.OrderByDescending(s => s.CreatedOn).FirstOrDefault(s => s.StatusId == submission.StatusId);
                    model.Remark = feedback != null ? feedback.Remark : string.Empty;

                    list.Add(model);
                }
            }

            return list;
        }

        #endregion
    }
}
