﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Activity;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.User.CandidateUser;
using System.Linq.Dynamic;
using XSeed.Utility;

namespace XSeed.Data.ViewModel.Reports
{

    //public class ActivityModel
    //{
    //    public Guid OrganizationUserId { get; set; }
    //    public string Mobile { get; set; }
    //    public string OrganizationUser { get; set; }
    //    public int RequirementCount { get; set; }
    //    public int CompanyCount { get; set; }
    //    public int CompanyContactCount { get; set; }
    //    public int SubmissionCount { get; set; }
    //    public int SubmissionFeedbackCount { get; set; }
    //    public int SubmissionFeedbackClosedCount { get; set; }

    //}

    public class ActivityReportModel
    {
        static string url = ConfigurationManager.AppSettings["MongoURL"].ToString();
        static string dbName = ConfigurationManager.AppSettings["MongoDB"].ToString();

        #region Property Declaration

        public string OrganizationUser { get; set; }
        public Guid OrganizationUserId { get; set; }
        public string OrganizationUserContact { get; set; }
        public int CompanyCount { get; set; }
        public int CompanyContactCount { get; set; }
        public int RequirementCount { get; set; }
        public int CandidateCount { get; set; }
        public int SubmissionCount { get; set; }
        public int SubmissionFeedbackCount { get; set; }
        public int SubmissionFeedbackClosedCount { get; set; }
        public int QNoteActivityCount { get; set; }
        public Guid UserId { get; set; }
        

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Activity Report
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Activity Report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        public List<ActivityReportModel> GetActivityReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", string type="W", bool isExport = false)
        {
            MongoClient client = new MongoClient(url);
            var database = client.GetDatabase(dbName);
            var collection = database.GetCollection<CandidateUserModel>("CandidateDetail");
            var collectionJobQNoteActivity = database.GetCollection<JobActivityLogModel>("JobActivityLog");
            var collectionCandidateQNoteActivity = database.GetCollection<CandidateActivityLogModel>("CandidateActivityLog");

            /* Calculate date */
            DateTime beforeDate = GraphReportDataUtility.GetBeforeDate(type);
            /* Initialize list */
            List<ActivityReportModel> list = new List<ActivityReportModel>();

            using (var db = new XSeedEntities())
            {

                var param = new System.Data.SqlClient.SqlParameter("@OrganizationId", organizationId);
                var dateParam = new System.Data.SqlClient.SqlParameter("@Type", type);
                var pagenumber = new System.Data.SqlClient.SqlParameter("@PageNo", pageNumber);
                var pagesize = new System.Data.SqlClient.SqlParameter("@PageSize", pageSize);
                var reportData = db.Database.SqlQuery<ActivityReportModel>("SP_GetActivityReportDetails @OrganizationId, @Type, @PageNo, @PageSize", param, dateParam, pagenumber, pagesize).ToList();

                //Get Mongo DB Counts
                foreach (var user in reportData)
                {
                    user.CandidateCount = collection.AsQueryable().Where(c => c.CreatedBy == user.UserId.ToString() && c.CreatedOn >= beforeDate).Count();

                    int jobQNoteActivityCount = collectionJobQNoteActivity.AsQueryable().Where(c => c.CreatedBy == user.UserId && c.CreatedOn >= beforeDate).Count();
                    int candidateQNoteActivityCount = collectionCandidateQNoteActivity.AsQueryable().Where(c => c.CreatedBy == user.UserId && c.CreatedOn >= beforeDate).Count();
                    user.QNoteActivityCount = jobQNoteActivityCount + candidateQNoteActivityCount;
                    
                    //Add to list
                    list.Add(user);
                }
            }

            list = isExport ? list.OrderBy(sortBy + " " + sortOrder).ToList() : list.OrderBy(sortBy + " " + sortOrder).ToList();
            
            return list;
        }

        public List<JobActivityLogModel> OrganizationUserActivityNote(Guid OrganizationUserId, string type = "W")
        {
            List<JobActivityLogModel> notes = new List<JobActivityLogModel>();
            MongoClient client = new MongoClient(url);
            var database = client.GetDatabase(dbName);
            DateTime beforeDate = GraphReportDataUtility.GetBeforeDate(type);
            var collection = database.GetCollection<JobActivityLogModel>("JobActivityLog");

            /* declare filter */
            var filter = Builders<JobActivityLogModel>.Filter.Where(s => s.CreatedBy == OrganizationUserId && s.CreatedOn >= beforeDate);

            /* apply filter */
            var documents = collection.Find(filter).SortByDescending(c => c.CreatedOn).ToList();//.ToList().Take(8)

            return documents;
        }

        /// <summary>
        /// Get Activity Graph Count
        /// </summary>
        /// <returns>Activity graph</returns>
        public List<ActivityReportModel> getActivityTrackerGraphReport(Guid organizationId, string type)
        {
            using (var db = new XSeedEntities())
            {
                /* Calculate date */
                //DateTime beforeDate = DateTime.Now.AddDays(daysBefore).Date;

                var param = new System.Data.SqlClient.SqlParameter("@OrganizationId", organizationId);
                var dateParam = new System.Data.SqlClient.SqlParameter("@Type", type);
                var graphData = db.Database.SqlQuery<ActivityReportModel>("SP_GetActivityReportDetails @OrganizationId, @Type", param, dateParam).ToList();


                return graphData;
            }
        }


        /// <summary>
        /// Get Pagination Info
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid organizationId, int pageSize, string type = "W")
        {
            PaginationModel model = new PaginationModel();
            /* Calculate date */
            DateTime beforeDate = GraphReportDataUtility.GetBeforeDate(type);

            using (var db = new XSeedEntities())
            {
                model.TotalCount = db.OrganizationUserDetails.Where(u => u.OrganizationId == organizationId).Count();
                model.TotalPages = Math.Ceiling((double)model.TotalCount / pageSize);
            }

            return model;
        }

        #endregion


    }
}
