﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;

namespace XSeed.Data.ViewModel.Vendors
{
    public class VendorEmailParserModel
    {
        #region Property Declaration

        public System.Guid Id { get; set; }
        public string Vendor { get; set; }
        public string SourceEmail { get; set; }
        public string Html { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string EmailUniqueId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsRead { get; set; }
        public List<VendorEmailParserAttachment> EmailAttachment { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Vendor Parsed Email
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>
        /// <returns>List of All Parsed Vendor Email</returns>
        public static List<VendorEmailParserModel> ListAllParsedVendorEmail(Guid organizationId, int pageSize, int pageNumber, string sortBy = "Date", string sortOrder = "desc")
        {
            return (VendorEmailParser.ListAllParsedVendorEmail(organizationId, pageSize, pageNumber, sortBy, sortOrder));
        }

        /// <summary>
        /// Get Vendor Parsed Email
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="Id">Id</param>
        /// <returns>Single Parsed Vendor Email</returns>
        public static VendorEmailParserModel GetVendorParsedEmail(Guid organizationId, Guid? vendorId)
        {
            return (VendorEmailParser.GetVendorParsedEmail(organizationId, vendorId));
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        public static PaginationModel GetPaginationInfo(Guid organizationId, int pageSize)
        {
            return VendorEmailParser.GetPaginationInfo(organizationId, pageSize);
        }

        /// <summary>
        /// Update Vendor Parsed Email
        /// </summary>
        /// <param name="vendorEmailParserModel">Vendor Email Parser Model</param>
        /// <returns></returns>
        public static void UpdateVendorParsedEmail(VendorEmailParserModel vendorEmailParserModel)
        {
            VendorEmailParser.UpdateVendorParsedEmail(vendorEmailParserModel);
        }

        /// <summary>
        /// Get Unread Mail Count 
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <returns>Unread Mail Count</returns>
        public static int GetUnreadMailCount(Guid organizationId)
        {
            return VendorEmailParser.GetUnreadMailCount(organizationId);
        }

        #endregion
    }
}
