﻿
namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using XSeed.Data.ViewModel.Submission;

    [MetadataType(typeof(ClientFeedbackMD))]
    public partial class ClientFeedback : IAuditable
    {
        #region Property Declaration

        public class ClientFeedbackMD
        {
            public int Id { get; set; }
            public Nullable<int> SubmissionId { get; set; }
            public Nullable<int> StatusId { get; set; }
            public string Remark { get; set; }
            public Nullable<System.DateTime> Date { get; set; }
        }

        public Guid? ModifiedBy
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public DateTime? ModifiedOn
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get client feedback list for particular submission
        /// </summary>
        /// <param name="submissionId">Submission Id</param>
        /// <returns>List of client feedback</returns>
        internal static List<ClientFeedbackModel> GetClientFeedbackList(Guid submissionId)
        {
            List<ClientFeedbackModel> list = new List<ClientFeedbackModel>();

            if (submissionId != null)
            {
                using (var db = new XSeedEntities())
                {
                    var Clientfeedbacks = db.ClientFeedbacks.Where(f => f.SubmissionId == submissionId).ToList().OrderByDescending(d => d.CreatedOn);

                    foreach (var feedback in Clientfeedbacks)
                    {
                        ClientFeedbackModel model = new ClientFeedbackModel();

                        /* Populate model */
                        model.Id = feedback.Id;
                        model.SubmissionId = feedback.SubmissionId;
                        model.FeedbackType = "Client Feedback";
                        model.StatusId = feedback.StatusId;
                        model.Status = feedback.ClientFeedbackMaster != null ? feedback.ClientFeedbackMaster.Status : string.Empty;
                        model.Remark = feedback.Remark;
                        model.Date = feedback.Date;
                        model.CreatedBy = feedback.CreatedBy.HasValue ? OrganizationUserDetail.GetCreatedByUserName(feedback.CreatedBy) : string.Empty;
                        list.Add(model);
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Add Client Feedback
        /// </summary>
        /// <param name="clientFeedbackModel">Client Feedback Model </param>
        internal static void AddClientFeedback(ClientFeedbackModel clientFeedbackModel)
        {
            if (clientFeedbackModel != null)
            {
                using (var db = new XSeedEntities())
                {
                    ClientFeedback model = new ClientFeedback();

                    /* Populate db model */
                    model.SubmissionId = clientFeedbackModel.SubmissionId;
                    model.StatusId = clientFeedbackModel.StatusId;
                    model.Remark = clientFeedbackModel.Remark;
                    model.Date = clientFeedbackModel.Date;

                    db.ClientFeedbacks.Add(model);

                    /* Update Submission Status with latest record */
                    var submission = db.SubmissionDetails.Find(model.SubmissionId);
                    submission.StatusId = clientFeedbackModel.StatusId;
                    submission.Status = clientFeedbackModel.StatusId != null ? db.ClientFeedbackMasters.Find(clientFeedbackModel.StatusId).Status : string.Empty;

                    db.SaveChanges();
                }
            }
        }

        #endregion
    }
}
