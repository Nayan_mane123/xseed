﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XSeed.API.Models;
using XSeed.Business.Reports;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Report API
    /// </summary>
    public class ReportController : BaseController
    {
        /// <summary>
        /// Get Candidate Added by recruiters Report
        /// </summary>        
        /// <remarks>Get Candidate Added by recruiters Report
        ///</remarks>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Candidate added report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult GetCandidateAddedReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", string type="W", bool isExport = false)
        {
            CandidateAddedReportInfo model = new CandidateAddedReportInfo();

            /* Set Pagination Info */
            var paginationInfo = model.GetPaginationInfo(organizationId, pageSize, type);

            /* return job list */
            var result = new
            {
                TotalCount = paginationInfo.TotalCount,
                TotalPages = paginationInfo.TotalPages,
                ReportData = model.GetCandidateAddedReport(organizationId, pageSize, pageNumber, sortBy, sortOrder,type, isExport)
            };

            return Ok(result);
        }


        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult getCandidateGraphReport(Guid organizationId, string type = "W")
        {
            CandidateAddedReportInfo model = new CandidateAddedReportInfo();
            return Ok(model.GetCandidateGraphReport(organizationId, type));
        }

        /// <summary>
        /// Get Requirement Tracker Report
        /// </summary>        
        /// <remarks>Get Requirement Tracker Report
        ///</remarks>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Requirement tracker report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult GetRequirementTrackerReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", string type = "W", bool isExport = false)
        {
            RequirementTrackerReportInfo model = new RequirementTrackerReportInfo();

            /* Set Pagination Info */
            var paginationInfo = model.GetPaginationInfo(organizationId, pageSize, type);            /* return job list */
            var result = new
            {
                TotalCount = paginationInfo.TotalCount,
                TotalPages = paginationInfo.TotalPages,
                ReportData = model.GetRequirementTrackerReport(organizationId, pageSize, pageNumber, sortBy, sortOrder, type, isExport)
            };

            return Ok(result);
        }

        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult getRequirementGraphTrackerReport(Guid organizationId, string type)
        {
            RequirementTrackerReportInfo model = new RequirementTrackerReportInfo();
            return Ok(model.GetRequirementTrackerGraphReport(organizationId, type));
        }


        /// <summary>
        /// Get Submission Graph Report
        /// </summary>        
        /// <remarks>Get Submission Graph Report
        ///</remarks>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Get Submission Graph Report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult getSubmissionTrackerGraphReport(Guid organizationId, string type)
        {
            SubmissionReportInfo model = new SubmissionReportInfo();
            return Ok(model.getSubmissionTrackerGraphReport(organizationId, type));
        }

        /// <summary>
        /// Get Client Graph Report
        /// </summary>        
        /// <remarks>Get Clien Graph Report
        ///</remarks>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Get Client Graph Report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult getClientTrackerGraphReport(Guid organizationId, string type)
        {
            CompanyLeadReportInfo model = new CompanyLeadReportInfo();
            return Ok(model.getClientTrackerGraphReport(organizationId, type));
        }


        /// <summary>
        /// Get Submission Report
        /// </summary>        
        /// <remarks>Get Submission Report
        ///</remarks>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Submission report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult GetSubmissionTrackerReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", string type="W", bool isExport = false)
        {
            SubmissionReportInfo model = new SubmissionReportInfo();

            /* Set Pagination Info */
            var paginationInfo = model.GetPaginationInfo(organizationId, pageSize, type);

            /* return job list */
            var result = new
            {
                TotalCount = paginationInfo.TotalCount,
                TotalPages = paginationInfo.TotalPages,
                ReportData = model.GetSubmissionTrackerReport(organizationId, pageSize, pageNumber, sortBy, sortOrder, type, isExport)
            };

            return Ok(result);
        }

        /// <summary>
        /// Get Submission All Counts
        /// </summary>        
        /// <remarks>Get Submission All Counts
        ///</remarks>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Submission report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult getSubmissionAllCounts(Guid organizationId)
        {
            SubmissionReportInfo model = new SubmissionReportInfo();

            /* Set Pagination Info */

            /* return job list */
            var result = new
            {
                SubmissionAllCount = model.GetSubmissionAllCount(organizationId)
            };

            return Ok(result);
        }

        /// <summary>
        /// Get Closure All Counts
        /// </summary>        
        /// <remarks>Get Closure All Counts
        ///</remarks>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Closure report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult getClosureAllCounts(Guid organizationId)
        {
            ClosureReportInfo model = new ClosureReportInfo();

            /* Set Pagination Info */

            /* return job list */
            var result = new
            {
                ClosureAllCount = model.getClosureAllCounts(organizationId)
            };

            return Ok(result);
        }

        /// <summary>
        /// Get Client All Counts
        /// </summary>        
        /// <remarks>Get Client All Counts
        ///</remarks>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Client report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult getClientAllCounts(Guid organizationId)
        {
            CompanyLeadReportInfo model = new CompanyLeadReportInfo();

            /* Set Pagination Info */

            /* return job list */
            var result = new
            {
                ClientAllCount = model.getClientAllCounts(organizationId)
            };

            return Ok(result);
        }


        /// <summary>
        /// Get Requirement All Counts
        /// </summary>        
        /// <remarks>Get Requirement All Counts
        ///</remarks>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Requirement report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult getRequirementAllCounts(Guid organizationId)
        {
            RequirementTrackerReportInfo model = new RequirementTrackerReportInfo();

            
            var result = new
            {
                RequirementAllCount = model.GetRequirementTrackerReport(organizationId)
            };

            return Ok(result);
            
        }

        /// <summary>
        /// Get Company Lead Report
        /// </summary>        
        /// <remarks>Get Company Lead Report </remarks>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Company Lead report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult GetCompanyLeadReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", string type = "W", bool isExport = false)
        {
            CompanyLeadReportInfo model = new CompanyLeadReportInfo();

            /* Set Pagination Info */
            var paginationInfo = model.GetPaginationInfo(organizationId, pageSize, type);

            /* return job list */
            var result = new
            {
                TotalCount = paginationInfo.TotalCount,
                TotalPages = paginationInfo.TotalPages,
                ReportData = model.GetCompanyLeadReport(organizationId, pageSize, pageNumber, sortBy, sortOrder, type, isExport)
            };

            return Ok(result);
        }

        /// <summary>
        /// Get Activity Report
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Activity Report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult GetActivityReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", string type = "W", bool isExport = false)
        {
            ActivityReportInfo model = new ActivityReportInfo();

            /* Set Pagination Info */
            var paginationInfo = model.GetPaginationInfo(organizationId, pageSize, type);

            /* return job list */
            var result = new
            {
                TotalCount = paginationInfo.TotalCount,
                TotalPages = paginationInfo.TotalPages,
                ReportData = model.GetActivityReport(organizationId, pageSize, pageNumber, sortBy, sortOrder, type, isExport)
            };

            return Ok(result);
        }

        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult getActivityReportGraphData(Guid organizationId, string type = "W")
        {
            ActivityReportInfo model = new ActivityReportInfo(); 
            /* return job list */
            var result = new
            {
                ReportData = model.getActivityReportGraphData(organizationId, type)
            };

           return Ok(result);
        }

        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult OrganizationUserActivityNote(Guid OrganizationUserId, string type = "W", int pageNo=0, int pageLength=10)
        {
            ActivityReportInfo model = new ActivityReportInfo();
            /* return job list */
            var result = new
            {
                ActivityData = model.OrganizationUserJobsActivityNote(OrganizationUserId, type, pageNo, pageLength),
                ActivityDataCanididate = model.OrganizationUserCanidatesActivityNote(OrganizationUserId, type, pageNo, pageLength)
            };

            return Ok(result);
        }


        /// <summary>
        /// Get Closure Report
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Closure Report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult GetClosureReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", string type = "W", bool isExport = false)
        {
            ClosureReportInfo model = new ClosureReportInfo();

            /* Set Pagination Info */
            var paginationInfo = model.GetPaginationInfo(organizationId, pageSize, type);

            /* return job list */
            var result = new
            {
                TotalCount = paginationInfo.TotalCount,
                TotalPages = paginationInfo.TotalPages,
                ReportData = model.GetClosureReport(organizationId, pageSize, pageNumber, sortBy, sortOrder, type, isExport)
            };

            return Ok(result);
        }

        /// <summary>
        /// Get Closure Graph Report
        /// </summary>        
        /// <remarks>Get Closure Graph Report
        ///</remarks>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Get Closure Graph Report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult getClosureTrackerGraphReport(Guid organizationId, string type)
        {
            ClosureReportInfo model = new ClosureReportInfo();
            return Ok(model.getClosureTrackerGraphReport(organizationId, type));
        }

        /// <summary>
        /// Get Activity Graph Report
        /// </summary>        
        /// <remarks>Get Activity Graph Report
        ///</remarks>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Get Activity Graph Report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult getActivityTrackerGraphReport(Guid organizationId, string type)
        {
            ActivityReportInfo model = new ActivityReportInfo();
            return Ok(model.getActivityTrackerGraphReport(organizationId, type));
        }
    }
}
