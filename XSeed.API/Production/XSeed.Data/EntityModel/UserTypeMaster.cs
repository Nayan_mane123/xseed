﻿
namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [MetadataType(typeof(UserTypeMasterMD))]
    public partial class UserTypeMaster:IAuditable
    {
        public class UserTypeMasterMD
        {
            public int Id { get; set; }
            public string Type { get; set; }
            public string Description { get; set; }
            public Nullable<bool> IsActive { get; set; }
 
        }
    }
}
