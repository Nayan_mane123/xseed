﻿namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using XSeed.Data.Entity;

    [MetadataType(typeof(LanguageMasterMD))]
    public partial class LanguageMaster : IAuditable
    {
        public class LanguageMasterMD
        {
            public System.Guid Id { get; set; }
            public string Language { get; set; }
            public string Description { get; set; }
            public Nullable<bool> IsActive { get; set; }            
        }
    }
}
