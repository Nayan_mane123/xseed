﻿namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using XSeed.Data.ViewModel.Organization;
    using XSeed.Data.ViewModel.Registration;
    using XSeed.Utility;

    [MetadataType(typeof(OrganizationDetailMD))]
    public partial class OrganizationDetail : IAuditable
    {
        #region Property Declaration

        /// <summary>
        /// Partial Class OrganizationDetail
        /// </summary>
        public class OrganizationDetailMD
        {
            /// <summary>
            /// Primary Key
            /// </summary>
            public int Id { get; set; }

            [Required]
            [Display(Name = "Organization Name")]
            public string Name { get; set; }
            public string Website { get; set; }
            public string Logo { get; set; }

            [Display(Name = "Country")]
            public Nullable<int> CountryId { get; set; }

            public Nullable<System.Guid> CityId { get; set; }
            public Nullable<System.Guid> StateId { get; set; }
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string Address3 { get; set; }
            public string PhoneNumber { get; set; }
            public string LinkedInURL { get; set; }
            public string TwitterURL { get; set; }
            public string GooglePlusURL { get; set; }
            public string FacebookURL { get; set; }
            public Nullable<bool> IsActive { get; set; }
        }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Organization Detail
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <returns>Organization Detail</returns>
        internal static OrganizationDetailModel GetOrganizationDetail(Guid organizationId)
        {

            /* Initialise Model */
            OrganizationDetailModel model = new OrganizationDetailModel();

            using (var db = new XSeedEntities())
            {

                /* Get Organization Detail */
                OrganizationDetail organizationDetail = db.OrganizationDetails.Find(organizationId);

                if (organizationDetail != null)
                {
                    /* Populate Model */
                    model.Id = organizationDetail.Id;
                    model.OrganizationName = organizationDetail.Name;
                    model.Website = organizationDetail.Website;
                    model.ProfileImage = organizationDetail.Logo;
                    model.CountryId = organizationDetail.CountryId;
                    model.Country = organizationDetail.CountryMaster != null ? organizationDetail.CountryMaster.Name : string.Empty;
                    model.Address1 = organizationDetail.Address1;
                    model.Address2 = organizationDetail.Address2;
                    model.Address3 = organizationDetail.Address3;
                    model.CityId = organizationDetail.CityId;
                    model.StateId = organizationDetail.StateId;
                    model.PhoneNumber = organizationDetail.PhoneNumber;
                    model.LinkedInURL = organizationDetail.LinkedInURL;
                    model.TwitterURL = organizationDetail.TwitterURL;
                    model.GooglePlusURL = organizationDetail.GooglePlusURL;
                    model.FacebookURL = organizationDetail.FacebookURL;
                    model.IndustryTypeList = IndustryTypeMaster.GetOrganizationIndustryTypes(model.Id);
                }
            }

            return model;
        }

        /// <summary>
        /// Insert Organization Information
        /// </summary>
        /// <param name="registerUser">register User Model</param>
        internal static Guid CreateOrganizationDetail(RegisterOrganizationModel registerOrgnization)
        {
            Guid organizationId = Guid.Empty;

            /* Create organization detail */
            using (var db = new XSeedEntities())
            {
                if (db.OrganizationDetails.Any(u => u.Name == registerOrgnization.OrganizationName && u.Website == registerOrgnization.Website))
                {
                    //throw new InvalidOperationException("EC40901");
                    throw new Exception("Organization already exists.");

                }

                /* Initialze & populate model */
                OrganizationDetail organization = new OrganizationDetail();
                organization = PopulateOrganizationDetail(organization, registerOrgnization);
                db.OrganizationDetails.Add(organization);
                db.SaveChanges();
                organizationId = organization.Id;
            }

            if (organizationId != Guid.Empty)
            {
                /* Add Organization Industries */
                IndustryTypeMaster.AddOrganizationIndustries(organizationId, registerOrgnization.IndustryTypeList);
            }

            return organizationId;
        }

        /// <summary>
        /// Update Organization Detail
        /// </summary>
        /// <param name="organizationId">Organization Detail Model</param>
        internal static void UpdateOrganizationDetail(OrganizationDetailModel organizationDetailModel)
        {
            Guid organizationId = Guid.Empty;

            /* Update organization detail */
            using (var db = new XSeedEntities())
            {
                /* Initialze & populate model */
                OrganizationDetail organization = db.OrganizationDetails.Find(organizationDetailModel.Id);

                if (organization != null)
                {
                    organization = PopulateOrganizationDetail(organization, organizationDetailModel);
                    db.SaveChanges();
                    organizationId = organization.Id;
                }
            }

            if (organizationId != Guid.Empty)
            {
                /* Add Organization Industries */
                IndustryTypeMaster.AddOrganizationIndustries(organizationId, organizationDetailModel.IndustryTypeList);
            }
        }

        /// <summary>
        /// Populate Organization Detail
        /// </summary>
        /// <param name="model">Dynamic Model</param>
        internal static OrganizationDetail PopulateOrganizationDetail(OrganizationDetail organization, dynamic model)
        {
            /* populate model */
            organization.Name = model.OrganizationName;
            organization.Website = model.Website;
            organization.Logo = string.IsNullOrEmpty(model.ProfileImage) ? organization.Logo : model.ProfileImage;
            organization.CountryId = model.CountryId;
            organization.Address1 = model.Address1;
            organization.Address2 = model.Address2;
            organization.Address3 = model.Address3;
            organization.CityId = model.CityId;
            organization.StateId = model.StateId;
            organization.PhoneNumber = model.PhoneNumber;
            organization.LinkedInURL = model.LinkedInURL;
            organization.TwitterURL = model.TwitterURL;
            organization.GooglePlusURL = model.GooglePlusURL;
            organization.FacebookURL = model.FacebookURL;
            organization.IsActive = Constants.IsActiveTrue;

            return organization;
        }

        /// <summary>
        /// Get All Organizations Detail
        /// </summary>        
        /// <returns>All Organizations Detail</returns>
        internal static bool IsOrganizationAlreadyExist(RegisterOrganizationModel registerOrgnization)
        {
            using (var db = new XSeedEntities())
            {
                /* Get Organization Detail */
                return db.OrganizationDetails.Where(x => x.Name.ToLower() == registerOrgnization.OrganizationName.ToLower() && x.Website.ToLower() == registerOrgnization.Website.ToLower()).Any();
            }
        }

        #endregion
    }
}
