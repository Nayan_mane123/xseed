﻿

namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using XSeed.Data.ViewModel.Submission;

    [MetadataType(typeof(CandidateFeedbackMD))]
    public partial class CandidateFeedback : IAuditable
    {
        public class CandidateFeedbackMD
        {
            public int Id { get; set; }
            public Nullable<int> SubmissionId { get; set; }
            public Nullable<int> StatusId { get; set; }
            public string Remark { get; set; }
            public Nullable<System.DateTime> Date { get; set; }
        }


        public Guid? ModifiedBy
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public DateTime? ModifiedOn
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Get candidate feedback list for particular submission
        /// </summary>
        /// <param name="submissionId">Submission Id</param>
        /// <returns>List of Candidate feedback</returns>
        internal static List<CandidateFeedbackModel> GetCandidateFeedbackList(Guid submissionId)
        {
            List<CandidateFeedbackModel> list = new List<CandidateFeedbackModel>();

            if (submissionId != null)
            {
                using (var db = new XSeedEntities())
                {
                    var Candidatefeedbacks = db.CandidateFeedbacks.Where(f => f.SubmissionId == submissionId).ToList().OrderByDescending(d => d.CreatedOn);

                    foreach (var feedback in Candidatefeedbacks)
                    {
                        CandidateFeedbackModel model = new CandidateFeedbackModel();

                        /* Populate model */
                        model.Id = feedback.Id;
                        model.SubmissionId = feedback.SubmissionId;
                        model.FeedbackType = "Candidate Feedback";
                        model.StatusId = feedback.StatusId;
                        model.Status = feedback.CandidateFeedbackMaster != null ? feedback.CandidateFeedbackMaster.Status : string.Empty;
                        model.Remark = feedback.Remark;
                        model.Date = feedback.Date;
                        model.CreatedBy = feedback.CreatedBy.HasValue ? OrganizationUserDetail.GetCreatedByUserName(feedback.CreatedBy) : string.Empty;
                        list.Add(model);
                    }
                }
            }

            return list;
        }


        /// <summary>
        /// Add Candidate Feedback
        /// </summary>
        /// <param name="candidateFeedbackModel">Candidate Feedback Model </param>
        internal static void AddCandidateFeedback(CandidateFeedbackModel candidateFeedbackModel)
        {
            if (candidateFeedbackModel != null)
            {
                using (var db = new XSeedEntities())
                {
                    CandidateFeedback model = new CandidateFeedback();

                    /* Populate db model */
                    model.Id = candidateFeedbackModel.Id;
                    model.SubmissionId = candidateFeedbackModel.SubmissionId;
                    model.StatusId = candidateFeedbackModel.StatusId;
                    model.Remark = candidateFeedbackModel.Remark;
                    model.Date = candidateFeedbackModel.Date;

                    db.CandidateFeedbacks.Add(model);

                    /* Update Submission Status with latest record */
                    var submission = db.SubmissionDetails.Find(model.SubmissionId);
                    submission.StatusId = candidateFeedbackModel.StatusId;
                    submission.Status = candidateFeedbackModel.StatusId != null ? db.CandidateFeedbackMasters.Find(candidateFeedbackModel.StatusId).Status : string.Empty;

                    db.SaveChanges();
                }
            }
        }
    }
}
