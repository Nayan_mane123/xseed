﻿
namespace XSeed.Data.Entity
{
    using Microsoft.Practices.EnterpriseLibrary.Logging;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Diagnostics;
    using System.Linq;
    using System.Linq.Dynamic;
    using System.Text;
    using System.Threading.Tasks;
    using XSeed.Data.ViewModel.Activity;
    using XSeed.Data.ViewModel.AdvancedSearch;
    using XSeed.Data.ViewModel.Common;
    using XSeed.Data.ViewModel.Job;
    using XSeed.Data.ViewModel.User.CandidateUser;
    using XSeed.Utility;

    [MetadataType(typeof(JobDetailMD))]
    public partial class JobDetail : IAuditable
    {
        #region Property Declaration

        public class JobDetailMD
        {
            public int Id { get; set; }
            public int CompanyId { get; set; }
            public Nullable<int> CompanyContactId { get; set; }
            public string JobTitle { get; set; }
            public string InternalJobTitle { get; set; }
            public Nullable<int> JobTypeId { get; set; }
            public Nullable<int> JobStatusId { get; set; }
            public string InterviewType { get; set; }
            public string JobDescription { get; set; }
            public string RequisitionId { get; set; }
            public string ClientJobCode { get; set; }
            public Nullable<int> TotalPositions { get; set; }
            public Nullable<System.DateTime> DriveFromDate { get; set; }
            public Nullable<System.DateTime> DriveToDate { get; set; }
            public string Priority { get; set; }
            public Nullable<int> CountryId { get; set; }
            public Nullable<int> StateId { get; set; }
            public Nullable<int> CityId { get; set; }
            public string Zip { get; set; }
            public string Location { get; set; }
            public string TechnicalSkills { get; set; }
            public string Qualification { get; set; }
            public Nullable<int> ExperienceInYear { get; set; }
            public Nullable<int> ExperienceInMonth { get; set; }
            public Nullable<System.DateTime> JobPostedDate { get; set; }
            public Nullable<System.DateTime> JobClosedDate { get; set; }
            public Nullable<System.DateTime> JobExpiryDate { get; set; }
            public string Duration { get; set; }
            public Nullable<System.Guid> VisaTypeId { get; set; }
            public Nullable<decimal> Margin { get; set; }
            public Nullable<decimal> MinSalary { get; set; }
            public Nullable<decimal> MaxSalary { get; set; }
            public Nullable<System.Guid> SalaryTypeId { get; set; }
            public Nullable<System.Guid> CurrencyTypeId { get; set; }
            public string FeePercent { get; set; }
            public Nullable<decimal> ClientRate { get; set; }
            public Nullable<decimal> PayRate { get; set; }
            public string ClientRateType { get; set; }
            public string PayRateType { get; set; }
            public Nullable<decimal> PerDiem { get; set; }
            public Nullable<int> USExperienceInYear { get; set; }
            public Nullable<int> USExperienceInMonth { get; set; }
            public string SecurityClearance { get; set; }
            public Nullable<System.Guid> BusinessUnit { get; set; }
            public string CommunicationSkills { get; set; }
            public Nullable<decimal> WeeklyHours { get; set; }
            public Nullable<decimal> TotalHours { get; set; }
            public Nullable<bool> IsActive { get; set; }
        }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get List of All Jobs
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <returns>List of All Jobs</returns>
        internal static List<JobDetailModel> ListAllJobs(Guid organizationId)
        {
            List<JobDetailModel> listJobDetail = new List<JobDetailModel>();

            using (var db = new XSeedEntities())
            {

                var jobList = db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == organizationId).ToList().OrderByDescending(d => d.CreatedOn);

                foreach (var job in jobList)
                {
                    JobDetailModel model = PopulateJobModel(organizationId, job.Id);
                    listJobDetail.Add(model);
                }

            }
            return listJobDetail;
        }

        /// <summary>
        /// Get list of jobs using server side pagination
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>        
        /// <returns>Jobs List</returns>
        internal static List<JobDetailModel> ListAllJobs(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", bool isExport = false)
        {
            List<JobDetailModel> listJobDetail = new List<JobDetailModel>();

            using (var db = new XSeedEntities())
            {
                var totalCount = db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == organizationId).Count();
                var totalPages = Math.Ceiling((double)totalCount / pageSize);

                var jobList = isExport ? db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == organizationId).OrderBy(sortBy + " " + sortOrder)
                                       : db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == organizationId).OrderBy(sortBy + " " + sortOrder)
                                       .Skip((pageNumber - 1) * pageSize).Take(pageSize);

                foreach (var job in jobList)
                {
                    JobDetailModel model = PopulateJobModel(organizationId, job.Id);
                    listJobDetail.Add(model);
                }
            }

            return listJobDetail;
        }

        /// <summary>
        /// List User Jobs By Status
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="userId">user Id</param>
        /// <param name="status">status</param>
        /// <param name="pageSize">page Size</param>
        /// <param name="pageNumber">page Number</param>
        /// <returns>Job List</returns>
        internal static List<JobDetailModel> ListUserJobsByStatus(Guid organizationId, Guid userId, string status, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc")
        {
            List<JobDetailModel> listJobDetail = new List<JobDetailModel>();
            IQueryable<JobDetail> jobs = null;

            using (var db = new XSeedEntities())
            {
                if (!string.IsNullOrEmpty(status))
                {
                    jobs = db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == organizationId && j.OrganizationUserDetails.Any(u => u.UserId == userId) && j.JobStatusMaster.Status == status)
                                                        .OrderBy(sortBy + " " + sortOrder)
                                                        .Skip((pageNumber - 1) * pageSize)
                                                        .Take(pageSize);
                }
                else
                {
                    jobs = db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == organizationId && j.OrganizationUserDetails.Any(u => u.UserId == userId))
                                                        .OrderBy(sortBy + " " + sortOrder)
                                                        .Skip((pageNumber - 1) * pageSize)
                                                        .Take(pageSize);
                }


                if (jobs != null)
                {
                    foreach (var job in jobs.ToList())
                    {
                        JobDetailModel model = PopulateJobModel(organizationId, job.Id);
                        listJobDetail.Add(model);
                    }
                }
            }

            return listJobDetail;
        }

        /// <summary>
        /// Get Job Details
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns>JobDetailModel</returns>
        internal static JobDetailModel GetJobDetail(Guid? organizationId, Guid? jobId)
        {
            var model = PopulateJobModel(organizationId, jobId);

            /* Associate Candidates Mapping */
            model.AssociateCandidate = SubmissionDetail.GetAssociateCandidates(model.Id);

            /* Degree List */
            model.DegreeList = DegreeMaster.GetJobDegreeInfo(model.Id);

            /* Applied Candidates Count */
            model.AppliedCandidateCount = CandidateUserModel.GetCandidateCountByJob(model.Id);

            return model;
        }

        /// <summary>
        /// Populate Job Model
        /// </summary>
        /// <param name="jobId">JobId</param>
        /// <returns>Job Model</returns>
        public static JobDetailModel PopulateJobModel(Guid? organizationId, Guid? jobId)
        {
            JobDetailModel model = new JobDetailModel();

            using (var db = new XSeedEntities())
            {
                JobDetail job = db.JobDetails.Find(jobId);

                if (job != null)
                {
                    model.Id = job.Id;

                    /* Company Info */
                    model.CompanyId = job.CompanyId;
                    model.CompanyName = job.CompanyDetail != null ? job.CompanyDetail.Name : string.Empty;
                    model.CompanyContactId = job.CompanyContactId;
                    model.CompanyContactName = job.CompanyContact != null ? job.CompanyContact.FirstName + " " + job.CompanyContact.LastName : string.Empty;
                    model.CompanyContactName = job.CompanyContact != null ? job.CompanyContact.PrimaryEmail : string.Empty;

                    /* Job Info */
                    model.JobTitle = job.JobTitle;
                    model.InternalJobTitle = job.InternalJobTitle;
                    model.JobTypeId = job.JobTypeId;
                    model.JobType = job.JobTypeMaster != null ? job.JobTypeMaster.Type : string.Empty;
                    model.JobStatusId = job.JobStatusId;
                    model.JobStatus = job.JobStatusMaster != null ? job.JobStatusMaster.Status : string.Empty;
                    model.InterviewType = job.InterviewType;
                    model.JobDescription = job.JobDescription;
                    model.RequisitionId = job.RequisitionId;
                    model.ClientJobCode = job.ClientJobCode;
                    model.TotalPositions = job.TotalPositions;
                    model.DriveFromDate = job.DriveFromDate;
                    model.DriveToDate = job.DriveToDate;
                    model.Priority = job.Priority;

                    /* Location Info */
                    model.CountryId = job.CountryId;
                    model.Country = job.CountryMaster != null ? job.CountryMaster.Name : string.Empty;
                    model.StateId = job.StateId;
                    model.State = job.StateMaster != null ? job.StateMaster.Name : string.Empty;
                    model.CityId = job.CityId;
                    model.City = job.CityMaster != null ? job.CityMaster.Name : string.Empty;
                    model.Zip = job.Zip;
                    model.Location = job.Location;

                    /* Skill Info */
                    model.TechnicalSkills = job.TechnicalSkills;
                    model.Qualification = job.Qualification;
                    model.ExperienceInYear = job.ExperienceInYear;
                    model.ExperienceInMonth = job.ExperienceInMonth;
                    model.JobPostedDate = job.JobPostedDate;
                    model.JobClosedDate = job.JobClosedDate;
                    model.JobExpiryDate = job.JobExpiryDate;

                    /* Other Info */
                    model.Duration = job.Duration;
                    model.VisaTypeId = job.VisaTypeId;
                    model.VisaType = job.VisaTypeMaster != null ? job.VisaTypeMaster.Type : string.Empty;
                    model.Margin = job.Margin;
                    model.MinSalary = job.MinSalary;
                    model.MaxSalary = job.MaxSalary;
                    model.SalaryTypeId = job.SalaryTypeId;
                    model.CurrencyTypeId = job.CurrencyTypeId;
                    model.FeePercent = job.FeePercent;
                    model.ClientRate = job.ClientRate;
                    model.PayRate = job.PayRate;
                    model.ClientRateType = job.ClientRateType;
                    model.PayRateType = job.PayRateType;
                    model.PerDiem = job.PerDiem;
                    model.USExperienceInYear = job.USExperienceInYear;
                    model.USExperienceInMonth = job.USExperienceInMonth;
                    model.SecurityClearance = job.SecurityClearance;
                    model.BusinessUnit = job.BusinessUnit;
                    model.BusinessUnitName = job.BusinessUnitMaster != null ? job.BusinessUnitMaster.Name : string.Empty;
                    model.CommunicationSkills = job.CommunicationSkills;
                    model.WeeklyHours = job.WeeklyHours;
                    model.TotalHours = job.TotalHours;

                    /* Audit Trail Info */
                    model.CreatedBy = OrganizationUserDetail.GetCreatedByUserName(job.CreatedBy);
                    model.CreatedOn = job.CreatedOn;
                    model.ModifiedBy = OrganizationUserDetail.GetCreatedByUserName(job.ModifiedBy);
                    model.ModifiedOn = job.ModifiedOn;

                    /* Organization User Mapping */
                    model.OrganizationUser = OrganizationUserDetail.GetJobUserMapping(job.Id);

                    /* Applied Candidates Count */
                    model.AppliedCandidateCount = 0; // CandidateUserModel.GetCandidateCountByJob(job.Id);

                    model.ParsedEmailHTMLContent = job.EmailParser != null ? job.EmailParser.Html : string.Empty;
                    model.ParsedEmailSubject = job.EmailParser != null ? job.EmailParser.Subject : string.Empty;
                    model.ParsedEmailSourceAddress = job.EmailParser != null ? job.EmailParser.CompanyContact : string.Empty;
                    model.ParsedEmailDate = job.EmailParser != null ? job.EmailParser.Date : (DateTime?)null;
                }
            }

            return model;
        }

        /// <summary>
        /// Common Function for Add and Update the Job
        /// </summary>
        /// <param name="model">JobDetailModel</param>
        /// <returns>Id of Newly Added Job</returns>
        internal static Guid SaveJobInfo(JobDetailModel model, bool isCreate = false)
        {
            JobDetail jobDetail = new JobDetail(); ;

            using (var db = new XSeedEntities())
            {
                if (!isCreate)
                    jobDetail = db.JobDetails.Find(model.Id);

                PopulateJobModel(jobDetail, model);

                if (isCreate)
                    db.JobDetails.Add(jobDetail);

                db.SaveChanges();

                if (isCreate)
                    JobActivityLogModel.UpdateLog(jobDetail.Id);

                /* Save Job Degree Info */
                DegreeMaster.SaveJobDegreeInfo(jobDetail.Id, model.DegreeList, isCreate);

                /* Add Organization User Mapping */
                OrganizationUserDetail.SaveJobUserMapping(jobDetail.Id, model.OrganizationUser, isCreate);


                return jobDetail.Id;
            }
        }

        /// <summary>
        /// Populate Job Model for create/ Update
        /// </summary>
        /// <param name="jobDetail">Job Detail</param>
        /// <param name="jobDetailModel">Job Detail Model</param>
        /// <returns>Job Detail</returns>
        private static JobDetail PopulateJobModel(JobDetail jobDetail, JobDetailModel model)
        {
            using (var db = new XSeedEntities())
            {
                jobDetail.Id = model.Id;

                //Company Info
                jobDetail.CompanyId = (Guid)(model.CompanyId);
                jobDetail.CompanyContactId = model.CompanyContactId;

                //Job Info
                jobDetail.JobTitle = model.JobTitle;
                jobDetail.InternalJobTitle = model.InternalJobTitle;
                jobDetail.JobTypeId = model.JobTypeId;
                jobDetail.JobStatusId = model.JobStatusId == null ? db.JobStatusMasters.FirstOrDefault(s => s.Status == "Open").Id : model.JobStatusId;
                jobDetail.InterviewType = model.InterviewType;
                jobDetail.JobDescription = model.JobDescription;
                jobDetail.RequisitionId = model.RequisitionId;
                jobDetail.ClientJobCode = model.ClientJobCode;
                jobDetail.TotalPositions = model.TotalPositions;
                jobDetail.DriveFromDate = model.DriveFromDate;
                jobDetail.DriveToDate = model.DriveToDate;
                jobDetail.Priority = model.Priority;
                jobDetail.CountryId = model.CountryId;
                jobDetail.StateId = model.StateId;
                jobDetail.CityId = model.CityId;
                jobDetail.Zip = model.Zip;
                jobDetail.Location = model.Location;
                jobDetail.TechnicalSkills = model.TechnicalSkills;
                jobDetail.Qualification = model.Qualification;
                jobDetail.ExperienceInYear = model.ExperienceInYear;
                jobDetail.ExperienceInMonth = model.ExperienceInMonth;
                jobDetail.JobPostedDate = model.JobPostedDate;
                jobDetail.JobClosedDate = model.JobClosedDate;
                jobDetail.JobExpiryDate = model.JobExpiryDate;

                /* Other Info */
                jobDetail.Duration = model.Duration;
                jobDetail.VisaTypeId = model.VisaTypeId;
                jobDetail.Margin = model.Margin;
                jobDetail.MinSalary = model.MinSalary;
                jobDetail.MaxSalary = model.MaxSalary;
                jobDetail.SalaryTypeId = model.SalaryTypeId;
                jobDetail.CurrencyTypeId = model.CurrencyTypeId;
                jobDetail.FeePercent = model.FeePercent;
                jobDetail.ClientRate = model.ClientRate;
                jobDetail.PayRate = model.PayRate;
                jobDetail.ClientRateType = model.ClientRateType;
                jobDetail.PayRateType = model.PayRateType;
                jobDetail.PerDiem = model.PerDiem;
                jobDetail.USExperienceInYear = model.USExperienceInYear;
                jobDetail.USExperienceInMonth = model.USExperienceInMonth;
                jobDetail.SecurityClearance = model.SecurityClearance;
                jobDetail.BusinessUnit = model.BusinessUnit;
                jobDetail.CommunicationSkills = model.CommunicationSkills;
                jobDetail.WeeklyHours = model.WeeklyHours;
                jobDetail.TotalHours = model.TotalHours;
                jobDetail.IsActive = Constants.IsActiveTrue;
            }

            return jobDetail;
        }

        /// <summary>
        /// Update Job Status
        /// </summary>
        /// <param name="id">JobId</param>
        /// <param name="jobStatusId">JobStatusId</param>
        internal static void UpdateJobStatus(Guid jobId, Guid jobStatusId)
        {
            using (var db = new XSeedEntities())
            {
                JobDetail jobDetail = db.JobDetails.Find(jobId);

                if (jobDetail != null)
                {
                    jobDetail.JobStatusId = jobStatusId;
                }
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Get job list by company
        /// </summary>
        /// <param name="CompanyId">CompanyId</param>
        /// <returns>Job list</returns>
        internal static List<JobDetailModel> GetJobsByCompany(Guid CompanyId)
        {
            List<JobDetailModel> listJobDetail = new List<JobDetailModel>();

            using (var db = new XSeedEntities())
            {
                /* Get Company Detail */
                CompanyDetail company = db.CompanyDetails.Find(CompanyId);

                if (company != null)
                {
                    /* Get Jobs listed by company */
                    var jobList = company.JobDetails
                        .Join(db.JobStatusMasters, j => j.JobStatusId, js => js.Id, (j, js) => new { j, js })
                        .Where(j => j.js.Status != "Closed" && j.js.Status != "Archive").OrderByDescending(c => c.j.CreatedOn).ToList();

                    foreach (var job in jobList)
                    {
                        /* Populate model */
                        JobDetailModel model = PopulateJobModel(company.OrganizationId, job.j.Id);
                        listJobDetail.Add(model);
                    }
                }
            }

            return listJobDetail;
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        internal static PaginationModel GetPaginationInfo(Guid organizationId, int pageSize)
        {
            PaginationModel model = new PaginationModel();

            using (var db = new XSeedEntities())
            {
                model.TotalCount = db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == organizationId).Count();
                model.TotalPages = Math.Ceiling((double)model.TotalCount / pageSize);
            }

            return model;
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        internal static PaginationModel GetPaginationInfo(Guid organizationId, Guid userId, string status, int pageSize)
        {
            PaginationModel model = new PaginationModel();

            using (var db = new XSeedEntities())
            {
                if (!string.IsNullOrEmpty(status))
                {
                    model.TotalCount = db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == organizationId && j.OrganizationUserDetails.Any(u => u.UserId == userId) && j.JobStatusMaster.Status == status).Count();
                }
                else
                {
                    model.TotalCount = db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == organizationId && j.OrganizationUserDetails.Any(u => u.UserId == userId)).Count();
                }

                model.TotalPages = Math.Ceiling((double)model.TotalCount / pageSize);
            }

            return model;
        }

        /// <summary>
        /// Get Job openings related to candidate skills
        /// </summary>
        /// <param name="skills">Candidate Skills</param>
        /// <returns>Job Openings</returns>
        internal static List<JobDetailModel> ListJobOpenings(List<JobDetail> matchedJobs)
        {
            List<JobDetailModel> list = new List<JobDetailModel>();

            foreach (var job in matchedJobs)
            {
                JobDetailModel model = PopulateJobModel(null, job.Id);
                list.Add(model);
            }

            return list;
        }

        /// <summary>
        /// Job Advanced Search API
        /// </summary>
        /// <param name="jobSearchModel">Job Search Model</param>
        /// <param name="pageSize">page Size</param>
        /// <param name="pageNumber">page Number</param>
        /// <param name="TotalCount">Total Count</param>
        /// <param name="TotalPages">Total Pages</param>
        /// <returns>List<JobDetailModel></returns>
        internal static List<JobDetailModel> SearchJobs(JobSearchModel jobSearchModel, int pageSize, int pageNumber, out int TotalCount, out double TotalPages)
        {
            List<JobDetailModel> list = new List<JobDetailModel>();

            using (var db = new XSeedEntities())
            {
                var jobList = db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == jobSearchModel.OrganizationId);

                /* Apply Job Title filter */
                if (!string.IsNullOrEmpty(jobSearchModel.JobTitle))
                {
                    jobList = jobList.Where(j => j.JobTitle == jobSearchModel.JobTitle);
                }

                /* Apply Company filter */
                if (!string.IsNullOrEmpty(jobSearchModel.CompanyName))
                {
                    jobList = jobList.Where(j => j.CompanyDetail.Name == jobSearchModel.CompanyName);
                }

                /* Apply Status filter */
                if (!string.IsNullOrEmpty(jobSearchModel.JobStatus))
                {
                    jobList = jobList.Where(j => j.JobStatusMaster.Status == jobSearchModel.JobStatus);
                }

                /* Assign Total Count & Pages */
                TotalCount = (int)jobList.Count();
                TotalPages = Math.Ceiling((double)TotalCount / pageSize);

                /* Apply Pagination */
                jobList = jobList.OrderByDescending(d => d.CreatedOn);

                var jobs = jobList.ToList().Skip((pageNumber - 1) * pageSize).Take(pageSize);

                /* Populate model list */
                foreach (var job in jobs)
                {
                    JobDetailModel model = PopulateJobModel(jobSearchModel.OrganizationId, job.Id);
                    list.Add(model);
                }
            }

            return list;
        }

        /// <summary>
        /// Update Audit Trail Info forjob on assoiciated entity update
        /// </summary>
        /// <param name="jobId">Job Id</param>
        public static void UpdateAuditTrail(Guid jobId)
        {
            using (var db = new XSeedEntities())
            {
                var jobDetail = db.JobDetails.Find(jobId);

                if (jobDetail != null)
                {
                    jobDetail.ModifiedBy = BaseModel.InitiatedBy;
                    jobDetail.ModifiedOn = DateTime.UtcNow;

                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Get Job Title by Job Id
        /// </summary>
        /// <param name="jobId">Job Id</param>
        /// <returns>Job Title</returns>
        internal static string GetJobTitle(Guid jobId)
        {
            string title = string.Empty;

            if (jobId != null && jobId != Guid.Empty)
            {
                using (var db = new XSeedEntities())
                {
                    var jobDetail = db.JobDetails.Find(jobId);
                    title = jobDetail != null ? jobDetail.JobTitle : string.Empty;
                }
            }

            return title;
        }

        /// <summary>
        /// Get Job Status by Job Id
        /// </summary>
        /// <param name="jobId">Job Id</param>
        /// <returns>Status</returns>
        internal static string GetJobStatus(Guid? jobId)
        {
            string status = string.Empty;

            if (jobId != null && jobId != Guid.Empty)
            {
                using (var db = new XSeedEntities())
                {
                    var jobDetail = db.JobDetails.Find(jobId);
                    status = jobDetail != null && jobDetail.JobStatusMaster != null ? jobDetail.JobStatusMaster.Status : string.Empty;
                }
            }

            return status;
        }

        #endregion
    }
}
