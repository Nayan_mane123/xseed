﻿namespace XSeed.Data.Entity
{
    using MongoDB.Bson;
    using MongoDB.Driver;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Configuration;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;
    using XSeed.Data.ViewModel.Account;
    using XSeed.Data.ViewModel.Common;
    using XSeed.Data.ViewModel.Notification;
    using XSeed.Data.ViewModel.Registration;
    using XSeed.Data.ViewModel.User.CandidateUser;
    using XSeed.Utility;


    [MetadataType(typeof(UserLoginMD))]
    public partial class UserLogin : IAuditable
    {
        static string url = ConfigurationManager.AppSettings["MongoURL"].ToString();
        static string dbName = ConfigurationManager.AppSettings["MongoDB"].ToString();
        public static MongoClient client = new MongoClient(url);
        static IMongoDatabase database = client.GetDatabase(dbName);

        #region Property Declaration

        public class UserLoginMD
        {
            public int Id { get; set; }
            public int UserTypeId { get; set; }
            public string UserName { get; set; }
            public string Password { get; set; }
            public Nullable<int> SocialNetworkTypeId { get; set; }
            public Nullable<bool> IsActive { get; set; }
        }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Save User Login Info
        /// </summary>
        /// <param name="registerUser">register User Model</param>
        internal static Guid SaveUserLoginInfo(RegisterOrganizationModel registerOrgnization)
        {
            UserLogin UL = new UserLogin();

            using (var db = new XSeedEntities())
            {
                if (db.UserLogins.Any(u => u.UserName == registerOrgnization.UserName))
                {
                    //throw new InvalidOperationException("EC40901");
                    throw new Exception("User already exists.");

                }

                /* Save Login Info */
                UL.Id = Guid.NewGuid();
                UL.UserTypeId = registerOrgnization.UserType;
                UL.UserName = registerOrgnization.UserName;
                UL.Password = PasswordUtility.EncryptPassword(registerOrgnization.Password);
                UL.IsActive = Constants.IsActiveTrue;
                db.UserLogins.Add(UL);

                db.SaveChanges();
            }

            return UL.Id;
        }

        //internal static Guid SaveCandidateUserLoginInfo(RegisterCandidateModel model)
        //{
        //    UserLogin UL = new UserLogin();

        //    using (var db = new XSeedEntities())
        //    {
        //        if (db.UserLogins.Any(u => u.UserName == model.UserName))
        //        {
        //            //throw new InvalidOperationException("EC40901");
        //            throw new Exception("User already exists.");

        //        }

        //        /* Save Login Info */
        //        UL.Id = Guid.NewGuid();
        //        UL.UserTypeId = model.UserType;
        //        UL.UserName = model.UserName;
        //        UL.Password = PasswordUtility.EncryptPassword(model.Password);
        //        UL.IsActive = Constants.IsActiveTrue;
        //        db.UserLogins.Add(UL);

        //        db.SaveChanges();
        //    }

        //    return UL.Id;
        //}

        //internal static Guid SaveCandidateUserLoginInfo(RegisterCandidateModel model)
        //{
        //    UserLogin UL = new UserLogin();

        //    using (var db = new XSeedEntities())
        //    {
        //        if (db.UserLogins.Any(u => u.UserName == model.UserName))
        //        {
        //            //throw new InvalidOperationException("EC40901");
        //            throw new Exception("User already exists.");

        //        }

        //        /* Save Login Info */
        //        UL.Id = Guid.NewGuid();
        //        //UL.UserTypeId = model.UserType;
        //        UL.UserName = model.UserName;
        //        UL.Password = PasswordUtility.EncryptPassword(model.Password);
        //        UL.IsActive = Constants.IsActiveTrue;
        //        db.UserLogins.Add(UL);

        //        db.SaveChanges();
        //    }

        //    return UL.Id;
        //}

        internal static string SaveCandidateUserLoginInfo(RegisterCandidateModel model, bool IsCandidateCall = false)
        {
            var database = client.GetDatabase(dbName);
            var collection = database.GetCollection<RegisterCandidateModel>("CandidateUser");

            model.UserName = model.UserName.ToLower();
            model.Password = model.Password != null ? PasswordUtility.EncryptPassword(model.Password) : model.Password;
            model.ConfirmPassword = model.Password;


            /* Generate Token */
            var newSecurityToken = Guid.NewGuid().ToString();
            model.SecurityToken = newSecurityToken;


            /* Update Audit Trail */
            model.CreatedBy = BaseModel.CandidateInitiatedBy;
            model.CreatedOn = BaseModel.InitiatedOn.HasValue ? BaseModel.InitiatedOn : DateTime.UtcNow;

            //Insert Candidate Detail
            collection.InsertOne(model);

            if (IsCandidateCall && !model.IsEmailConfirmed)
            {
                //Send verification email to candidate
                SendCandidateEmailVerificationEmail(model.UserName, model._id, newSecurityToken);
            }

            return model._id;
        }


        /// <summary>
        /// Change User Password
        /// </summary>
        /// <param name="model">ChangePasswordModel</param>
        internal static void ChangeUserPassword(ChangePasswordModel model)
        {
            UserLogin UL = new UserLogin();

            using (var db = new XSeedEntities())
            {
                string encryptedOldPassword = PasswordUtility.EncryptPassword(model.OldPassword);
                UL = db.UserLogins.Where(u => u.Id == model.Id && u.Password == encryptedOldPassword).FirstOrDefault();

                if (UL != null)
                {
                    /* Update Login Info */
                    UL.Password = PasswordUtility.EncryptPassword(model.NewPassword);
                }
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Reset User Password
        /// </summary>
        /// <param name="userName">UserName</param>
        /// <param name="newPassword">NewPassword</param>
        internal static void ResetUserPassword(string username, string newPassword)
        {
            UserLogin UL = new UserLogin();

            using (var db = new XSeedEntities())
            {
                UL = db.UserLogins.Where(u => u.UserName == username).FirstOrDefault();

                if (UL != null)
                {
                    /* Update Login Info */
                    UL.Password = PasswordUtility.EncryptPassword(newPassword);
                }
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Update Registration Info
        /// </summary>
        /// <param name="model">RegisterCandidateModel</param>
        internal static void UpdateRegistrationInfo(RegisterCandidateModel model)
        {
            //For Candidate User Collection
            var filterCandidateUser = (Builders<RegisterCandidateModel>.Filter.Eq("UserName", model.UserName.ToLower()));
            var collectionCandidateUser = database.GetCollection<RegisterCandidateModel>("CandidateUser");
            var candidateUser = collectionCandidateUser.Find(filterCandidateUser).FirstOrDefault();

            if (candidateUser != null)
            {
                //Update required fields
                model._id = candidateUser._id;
                model.UserName = candidateUser.UserName.ToLower();
                if (candidateUser.Password == null)
                {
                    if (model.Password != null)
                    {
                        model.Password = PasswordUtility.EncryptPassword(model.Password);
                        model.ConfirmPassword = model.Password;
                    }
                }
                candidateUser = model;

                /* Generate Token */
                var newSecurityToken = Guid.NewGuid().ToString();
                candidateUser.SecurityToken = newSecurityToken;

                /* Update IsRegistered */
                candidateUser.IsRegistered = Constants.IsActiveTrue;

                /* Update Audit Trail */
                candidateUser.CreatedBy = string.Empty;
                candidateUser.CreatedOn = BaseModel.InitiatedOn.HasValue ? BaseModel.InitiatedOn : DateTime.UtcNow;

                collectionCandidateUser.ReplaceOne(filterCandidateUser, candidateUser);

                /* Update Candidate Info */
                //For Candidate Detail Collection
                var filterCandidateDetail = (Builders<CandidateUserModel>.Filter.Eq("UserId", candidateUser._id));
                var collectionCandidateDetail = database.GetCollection<CandidateUserModel>("CandidateDetail");
                var candidateDetail = collectionCandidateDetail.Find(filterCandidateDetail).FirstOrDefault();
                if (candidateDetail != null)
                {
                    CandidateUserModel candidateUserModel = new CandidateUserModel();
                    candidateUserModel._id = candidateDetail._id;
                    candidateUserModel.CandidateId = candidateDetail.CandidateId;
                    candidateUserModel.UserId = candidateUser._id;
                    candidateUserModel.FirstName = model.FirstName;
                    candidateUserModel.LastName = model.LastName;
                    candidateUserModel.PrimaryEmail = model.UserName.ToLower();
                    candidateUserModel.Mobile = model.Mobile;
                    candidateUserModel.Status = "New";


                    /* Merge previous data with null model - Rohit */
                    candidateUserModel.State = string.IsNullOrEmpty(candidateDetail.State) ? candidateUserModel.State : candidateDetail.State;
                    candidateUserModel.StateId = candidateDetail.StateId == null ? candidateUserModel.StateId : candidateDetail.StateId;
                    candidateUserModel.City = string.IsNullOrEmpty(candidateDetail.City) ? candidateUserModel.City : candidateDetail.City;
                    candidateUserModel.CityId = candidateDetail.CityId == null ? candidateUserModel.CityId : candidateDetail.CityId;
                    candidateUserModel.Country = string.IsNullOrEmpty(candidateDetail.Country) ? candidateUserModel.Country : candidateDetail.Country;
                    candidateUserModel.CountryId = candidateDetail.CountryId == null ? candidateUserModel.CountryId : candidateDetail.CountryId;
                    candidateUserModel.Gender = string.IsNullOrEmpty(candidateDetail.Gender) ? candidateUserModel.Gender : candidateDetail.Gender;
                    candidateUserModel.MaritalStatus = string.IsNullOrEmpty(candidateDetail.MaritalStatus) ? candidateUserModel.MaritalStatus : candidateDetail.MaritalStatus;
                    candidateUserModel.SecondaryEmail = string.IsNullOrEmpty(candidateDetail.SecondaryEmail) ? candidateUserModel.SecondaryEmail : candidateDetail.SecondaryEmail;
                    candidateUserModel.Phone = candidateDetail.Phone == null ? candidateUserModel.Phone : candidateDetail.Phone;
                    candidateUserModel.Fax = candidateDetail.Fax == null ? candidateUserModel.Fax : candidateDetail.Fax;
                    candidateUserModel.Address1 = string.IsNullOrEmpty(candidateDetail.Address1) ? candidateUserModel.Address1 : candidateDetail.Address1;
                    candidateUserModel.Address2 = string.IsNullOrEmpty(candidateDetail.Address2) ? candidateUserModel.Address2 : candidateDetail.Address2;
                    candidateUserModel.Address3 = string.IsNullOrEmpty(candidateDetail.Address3) ? candidateUserModel.Address3 : candidateDetail.Address3;
                    candidateUserModel.MiddleName = string.IsNullOrEmpty(candidateDetail.MiddleName) ? candidateUserModel.MiddleName : candidateDetail.MiddleName;
                    candidateUserModel.Title = string.IsNullOrEmpty(candidateDetail.Title) ? candidateUserModel.Title : candidateDetail.Title;
                    candidateUserModel.BirthDate = candidateDetail.BirthDate == null ? candidateUserModel.BirthDate : candidateDetail.BirthDate;
                    candidateUserModel.ProfileImageFile = candidateDetail.ProfileImageFile == null ? candidateUserModel.ProfileImageFile : candidateDetail.ProfileImageFile;
                    candidateUserModel.ProfileImagePath = string.IsNullOrEmpty(candidateDetail.ProfileImagePath) ? candidateUserModel.ProfileImagePath : candidateDetail.ProfileImagePath;
                    candidateUserModel.WorkPhone = string.IsNullOrEmpty(candidateDetail.WorkPhone) ? candidateUserModel.WorkPhone : candidateDetail.WorkPhone;
                    candidateUserModel.Zip = string.IsNullOrEmpty(candidateDetail.Zip) ? candidateUserModel.Zip : candidateDetail.Zip;
                    candidateUserModel.HavePassport = candidateDetail.HavePassport == null ? candidateUserModel.HavePassport : candidateDetail.HavePassport;
                    candidateUserModel.PassportValidUpto = candidateDetail.PassportValidUpto == null ? candidateUserModel.PassportValidUpto : candidateDetail.PassportValidUpto;
                    candidateUserModel.VisaType = string.IsNullOrEmpty(candidateDetail.VisaType) ? candidateUserModel.VisaType : candidateDetail.VisaType;
                    candidateUserModel.VisaValidUpto = candidateDetail.VisaValidUpto == null ? candidateUserModel.VisaValidUpto : candidateDetail.VisaValidUpto;
                    candidateUserModel.ProfileTitle = string.IsNullOrEmpty(candidateDetail.ProfileTitle) ? candidateUserModel.ProfileTitle : candidateDetail.ProfileTitle;
                    candidateUserModel.ProfileSummary = string.IsNullOrEmpty(candidateDetail.ProfileSummary) ? candidateUserModel.ProfileSummary : candidateDetail.ProfileSummary;
                    candidateUserModel.Designation = string.IsNullOrEmpty(candidateDetail.Designation) ? candidateUserModel.Designation : candidateDetail.Designation;
                    candidateUserModel.ExperienceInYear = candidateDetail.ExperienceInYear == null ? candidateUserModel.ExperienceInYear : candidateDetail.ExperienceInYear;
                    candidateUserModel.ExperienceInMonth = candidateDetail.ExperienceInMonth == null ? candidateUserModel.ExperienceInMonth : candidateDetail.ExperienceInMonth;
                    candidateUserModel.CurrentSalary = candidateDetail.CurrentSalary == null ? candidateUserModel.CurrentSalary : candidateDetail.CurrentSalary;
                    candidateUserModel.CurrentSalaryType = candidateDetail.CurrentSalaryType == null ? candidateUserModel.CurrentSalaryType : candidateDetail.CurrentSalaryType;
                    candidateUserModel.CurrentSalaryCurrencyType = candidateDetail.CurrentSalaryCurrencyType == null ? candidateUserModel.CurrentSalaryCurrencyType : candidateDetail.CurrentSalaryCurrencyType;
                    candidateUserModel.ExpectedSalary = candidateDetail.ExpectedSalary == null ? candidateUserModel.ExpectedSalary : candidateDetail.ExpectedSalary;
                    candidateUserModel.ExpectedSalaryType = candidateDetail.ExpectedSalaryType == null ? candidateUserModel.ExpectedSalaryType : candidateDetail.ExpectedSalaryType;
                    candidateUserModel.ExpectedSalaryCurrencyType = candidateDetail.ExpectedSalaryCurrencyType == null ? candidateUserModel.ExpectedSalaryCurrencyType : candidateDetail.ExpectedSalaryCurrencyType;
                    candidateUserModel.CurrentLocation = string.IsNullOrEmpty(candidateDetail.CurrentLocation) ? candidateUserModel.CurrentLocation : candidateDetail.CurrentLocation;
                    candidateUserModel.PreferredLocation = string.IsNullOrEmpty(candidateDetail.PreferredLocation) ? candidateUserModel.PreferredLocation : candidateDetail.PreferredLocation;
                    candidateUserModel.SendJobAlert = candidateDetail.SendJobAlert == null ? candidateUserModel.SendJobAlert : candidateDetail.SendJobAlert;
                    candidateUserModel.Educations = candidateDetail.Educations == null ? candidateUserModel.Educations : candidateDetail.Educations;
                    candidateUserModel.Courses = candidateDetail.Courses == null ? candidateUserModel.Courses : candidateDetail.Courses;
                    candidateUserModel.Certifications = candidateDetail.Certifications == null ? candidateUserModel.Certifications : candidateDetail.Certifications;
                    candidateUserModel.Skills = candidateDetail.Skills == null ? candidateUserModel.Skills : candidateDetail.Skills;
                    candidateUserModel.DegreeList = candidateDetail.DegreeList == null ? candidateUserModel.DegreeList : candidateDetail.DegreeList;
                    candidateUserModel.ResumeList = candidateDetail.ResumeList == null ? candidateUserModel.ResumeList : candidateDetail.ResumeList;
                    candidateUserModel.VideoResume = candidateDetail.VideoResume == null ? candidateUserModel.VideoResume : candidateDetail.VideoResume;
                    candidateUserModel.isResumeCreated = candidateDetail.isResumeCreated == null ? candidateUserModel.isResumeCreated : candidateDetail.isResumeCreated;
                    candidateUserModel.Interests = candidateDetail.Interests == null ? candidateUserModel.Interests : candidateDetail.Interests;
                    candidateUserModel.ApplicationList = candidateDetail.ApplicationList == null ? candidateUserModel.ApplicationList : candidateDetail.ApplicationList;
                    candidateUserModel.LinkedInURL = candidateDetail.LinkedInURL == null ? candidateUserModel.LinkedInURL : candidateDetail.LinkedInURL;
                    candidateUserModel.TwitterURL = candidateDetail.TwitterURL == null ? candidateUserModel.TwitterURL : candidateDetail.TwitterURL;
                    candidateUserModel.GooglePlusURL = candidateDetail.GooglePlusURL == null ? candidateUserModel.GooglePlusURL : candidateDetail.GooglePlusURL;
                    candidateUserModel.FacebookURL = string.IsNullOrEmpty(candidateDetail.FacebookURL) ? candidateUserModel.FacebookURL : candidateDetail.FacebookURL;
                    candidateUserModel.ReferredBy = string.IsNullOrEmpty(candidateDetail.ReferredBy) ? candidateUserModel.ReferredBy : candidateDetail.ReferredBy;
                    candidateUserModel.Source = string.IsNullOrEmpty(candidateDetail.Source) ? candidateUserModel.Source : candidateDetail.Source;
                    candidateUserModel.CompanyName = string.IsNullOrEmpty(candidateDetail.CompanyName) ? candidateUserModel.CompanyName : candidateDetail.CompanyName;
                    candidateUserModel.Status = string.IsNullOrEmpty(candidateDetail.Status) ? candidateUserModel.Status : candidateDetail.Status;
                    candidateUserModel.Rating = string.IsNullOrEmpty(candidateDetail.Rating) ? candidateUserModel.Rating : candidateDetail.Rating;
                    candidateUserModel.EmployementType = string.IsNullOrEmpty(candidateDetail.EmployementType) ? candidateUserModel.EmployementType : candidateDetail.EmployementType;
                    candidateUserModel.PayRate = candidateDetail.PayRate == null ? candidateUserModel.PayRate : candidateDetail.PayRate;
                    candidateUserModel.BillRate = candidateDetail.BillRate == null ? candidateUserModel.BillRate : candidateDetail.BillRate;
                    candidateUserModel.Compensation = candidateDetail.Compensation == null ? candidateUserModel.Compensation : candidateDetail.Compensation;
                    candidateUserModel.Languages = candidateDetail.Languages == null ? candidateUserModel.Languages : candidateDetail.Languages;
                    candidateUserModel.AvailabilityDate = string.IsNullOrEmpty(candidateDetail.AvailabilityDate) ? candidateUserModel.AvailabilityDate : candidateDetail.AvailabilityDate;
                    candidateUserModel.USExperienceInYear = candidateDetail.USExperienceInYear == null ? candidateUserModel.USExperienceInYear : candidateDetail.USExperienceInYear;
                    candidateUserModel.USExperienceInMonth = candidateDetail.USExperienceInMonth == null ? candidateUserModel.USExperienceInMonth : candidateDetail.USExperienceInMonth;
                    candidateUserModel.SSN = candidateDetail.SSN == null ? candidateUserModel.SSN : candidateDetail.SSN;
                    candidateUserModel.Categories = candidateDetail.Categories == null ? candidateUserModel.Categories : candidateDetail.Categories;
                    candidateUserModel.Groups = string.IsNullOrEmpty(candidateDetail.Groups) ? candidateUserModel.Groups : candidateDetail.Groups;
                    candidateUserModel.DisplayName = candidateDetail.DisplayName == null ? candidateUserModel.DisplayName : candidateDetail.DisplayName;
                    candidateUserModel.SourceQueue = candidateDetail.SourceQueue == null ? candidateUserModel.SourceQueue : candidateDetail.SourceQueue;
                    candidateUserModel.EmployementHistory = candidateDetail.EmployementHistory == null ? candidateUserModel.EmployementHistory : candidateDetail.EmployementHistory;
                    candidateUserModel.Projects = candidateDetail.Projects == null ? candidateUserModel.Projects : candidateDetail.Projects;
                    candidateUserModel.IsCandidate = candidateDetail.IsCandidate == null ? candidateUserModel.IsCandidate : candidateDetail.IsCandidate;
                    /* Merge previous data with null model - Rohit */


                    /* Update Audit Trail */
                    candidateUserModel.CreatedBy = string.IsNullOrEmpty(BaseModel.CandidateInitiatedBy) ? candidateUser._id : string.Empty;
                    candidateUserModel.CreatedOn = BaseModel.InitiatedOn.HasValue ? BaseModel.InitiatedOn : DateTime.UtcNow;

                    candidateUserModel.UpdateCandidateUserDetail(candidateUserModel);
                }

                //Send verification email to candidate
                SendCandidateEmailVerificationEmail(candidateUser.UserName, candidateUser._id, candidateUser.SecurityToken);
            }
        }

        internal static void SendCandidateEmailVerificationEmail(string email, string userId, string code)
        {
            //Create token and send email with callback url
            var callBackUrl = ConfigurationManager.AppSettings["EmailVerificationCallBackURL"] + "?userId=" + userId + "&code=" + HttpUtility.UrlEncode(code);

            EmailNotificationModel emailNotificationModel = new EmailNotificationModel();
            emailNotificationModel.ToEmailID = new List<string>();
            emailNotificationModel.ToEmailID.Add(email);
            emailNotificationModel.FromEmailID = ConfigurationManager.AppSettings["EmailFromUserName"];
            emailNotificationModel.FromEmailPassword = ConfigurationManager.AppSettings["EmailFromPassword"];
            emailNotificationModel.TypeOfNotification = XSeed.Utility.Constants.typeOfNotificationVerifyEmail;
            emailNotificationModel.CallBackUrl = callBackUrl;
            EmailNotificationModel.SendEmailNotification(emailNotificationModel);
        }

        /// <summary>
        /// Change Candidate Password
        /// </summary>
        /// <param name="model">Change Password Model</param>
        internal static void ChangeCandidatePassword(ChangePasswordModel model)
        {
            var database = client.GetDatabase(dbName);
            var collection = database.GetCollection<RegisterCandidateModel>("CandidateUser");

            var filter = Builders<RegisterCandidateModel>.Filter.Eq("_id", ObjectId.Parse(model.CandidateUserId));
            var candidateUser = collection.Find(filter).FirstOrDefault();

            string encryptedOldPassword = PasswordUtility.EncryptPassword(model.OldPassword);

            if (candidateUser != null && candidateUser.Password == encryptedOldPassword)
            {
                /* Update Login Info */
                candidateUser.Password = PasswordUtility.EncryptPassword(model.NewPassword);
                candidateUser.ConfirmPassword = candidateUser.Password;

                /* Update document */
                collection.ReplaceOne(filter, candidateUser);
            }
            else
            {
                throw new Exception("Incorrect current password");
            }
        }

        #endregion
    }
}
