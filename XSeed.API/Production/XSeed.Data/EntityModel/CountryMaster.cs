﻿
namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [MetadataType(typeof(CountryMasterMD))]
    public partial class CountryMaster
    {
        public class CountryMasterMD
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Code { get; set; }
 
        }
    }
}
