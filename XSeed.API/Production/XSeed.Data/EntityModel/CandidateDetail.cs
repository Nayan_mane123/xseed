﻿namespace XSeed.Data.Entity
{
    using Microsoft.Practices.EnterpriseLibrary.Logging;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using XSeed.Data.Entity;
    using XSeed.Data.ViewModel.Candidate;
    using XSeed.Data.ViewModel.Common;

    [MetadataType(typeof(CandidateDetailMD))]
    public partial class CandidateDetail : IAuditable
    {
        #region Property Declaration

        public class CandidateDetailMD
        {
            public int Id { get; set; }
            public Nullable<int> UserId { get; set; }
            public Nullable<int> TitleId { get; set; }
            public string FirstName { get; set; }
            public string MiddleName { get; set; }
            public string LastName { get; set; }
            public Nullable<System.DateTime> BirthDate { get; set; }
            public string ProfileImagePath { get; set; }
            public Nullable<int> GenderId { get; set; }
            public Nullable<int> MaritalStatusId { get; set; }
            public string PrimaryEmail { get; set; }
            public string SecondaryEmail { get; set; }
            public string Mobile { get; set; }
            public string Phone { get; set; }
            public string Fax { get; set; }
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string Address3 { get; set; }
            public Nullable<int> CountryId { get; set; }
            public Nullable<int> StateId { get; set; }
            public Nullable<int> CityId { get; set; }
            public string Zip { get; set; }
            public Nullable<bool> HavePassport { get; set; }
            public Nullable<System.DateTime> PassportValidUpto { get; set; }
            public Nullable<int> VisaTypeId { get; set; }
            public Nullable<System.DateTime> VisaValidUpto { get; set; }
            public Nullable<bool> SendJobAlert { get; set; }
            public Nullable<bool> IsExperienced { get; set; }
        }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get list of candidates
        /// </summary>        
        /// <returns>Candidate Detail Model List</returns>
        internal static List<CandidateDetailModel> ListCandidates()
        {
            Stopwatch timer = new Stopwatch();
            List<CandidateDetailModel> list = new List<CandidateDetailModel>();

            using (var db = new XSeedEntities())
            {
                timer.Start();
                var candidates = db.CandidateDetails.ToList().OrderByDescending(d => d.CreatedOn);
                timer.Stop();
                Logger.Write("Candidates: Database- Entity - ListCandidates, time taken:" + timer.ElapsedMilliseconds / 1000 + " seconds");

                timer.Reset();

                timer.Start();
                foreach (var candidate in candidates)
                {
                    CandidateDetailModel model = PopulateCandidateDetailModel(candidate);
                    list.Add(model);
                }
                timer.Stop();
                Logger.Write("Candidates: Data Population - ListCandidates, time taken:" + timer.ElapsedMilliseconds / 1000 + " seconds");
            }

            return list;
        }

        /// <summary>
        /// Get list of candidates using server side pagination
        /// </summary>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>
        /// <param name="orderBy">orderBy</param>
        /// <returns>Candidate List</returns>
        internal static List<CandidateDetailModel> ListCandidates(int pageSize, int pageNumber)
        {
            Stopwatch timer = new Stopwatch();
            List<CandidateDetailModel> list = new List<CandidateDetailModel>();

            using (var db = new XSeedEntities())
            {
                timer.Start();

                var totalCount = db.CandidateDetails.Count();
                var totalPages = Math.Ceiling((double)totalCount / pageSize);

                var candidates = db.CandidateDetails.OrderByDescending(d => d.CreatedOn).ToList().Skip((pageNumber - 1) * pageSize)
                                    .Take(pageSize);
                                    

                timer.Stop();

                Logger.Write("Candidates: Database- Entity - ListCandidates, time taken:" + timer.ElapsedMilliseconds / 1000 + " seconds");

                timer.Reset();

                timer.Start();
                foreach (var candidate in candidates)
                {
                    CandidateDetailModel model = PopulateCandidateDetailModel(candidate);
                    list.Add(model);
                }
                timer.Stop();
                Logger.Write("Candidates: Data Population - ListCandidates, time taken:" + timer.ElapsedMilliseconds / 1000 + " seconds");
            }

            return list;
        }

        /// <summary>
        /// Get particular candidate
        /// </summary>
        /// <param name="Id">Candidate Id</param>        
        /// <returns>Candidate Contact Model</returns>
        internal static CandidateDetailModel GetCandidateDetail(Guid? Id)
        {
            CandidateDetailModel model = new CandidateDetailModel();

            using (var db = new XSeedEntities())
            {
                var candidate = db.CandidateDetails.Find(Id);

                if (candidate != null)
                {
                    model = PopulateCandidateDetailModel(candidate);
                }
            }

            return model;
        }

        /// <summary>
        /// Populate Candidate Detail Model
        /// </summary>
        /// <param name="candidate">Candidate Detail</param>
        /// <returns>Candidate Detail Model</returns>
        private static CandidateDetailModel PopulateCandidateDetailModel(CandidateDetail candidate)
        {
            CandidateDetailModel model = new CandidateDetailModel();

            /* References */
            model.Id = candidate.Id;
            model.UserId = candidate.UserId;

            /* Personal Detail */
            model.TitleId = candidate.TitleId;
            model.Title = candidate.TitleMaster != null ? candidate.TitleMaster.Name : string.Empty;
            model.FirstName = candidate.FirstName;
            model.MiddleName = candidate.MiddleName;
            model.LastName = candidate.LastName;
            model.BirthDate = candidate.BirthDate;
            model.ProfileImage = candidate.ProfileImagePath;
            model.GenderId = candidate.GenderId;
            model.Gender = candidate.GenderMaster != null ? candidate.GenderMaster.Name : string.Empty;
            model.MaritalStatusId = candidate.MaritalStatusId;
            model.MaritalStatus = candidate.MaritalStatusMaster != null ? candidate.MaritalStatusMaster.Status : string.Empty;

            /* Contact Detail */
            model.PrimaryEmail = candidate.PrimaryEmail;
            model.SecondaryEmail = candidate.SecondaryEmail;
            model.Mobile = candidate.Mobile;
            model.Phone = candidate.Phone;
            model.Fax = candidate.Fax;

            /* Address Detail */
            model.Address1 = candidate.Address1;
            model.Address2 = candidate.Address2;
            model.Address3 = candidate.Address3;
            model.CountryId = candidate.CountryId;
            model.Country = candidate.CountryMaster != null ? candidate.CountryMaster.Name : string.Empty;
            model.StateId = candidate.StateId;
            model.State = candidate.StateMaster != null ? candidate.StateMaster.Name : string.Empty;
            model.CityId = candidate.CityId;
            model.City = candidate.CityMaster != null ? candidate.CityMaster.Name : string.Empty;
            model.Zip = candidate.Zip;

            /* Other Detail */
            model.HavePassport = candidate.HavePassport;
            model.PassportValidUpto = candidate.PassportValidUpto;
            model.VisaTypeId = candidate.VisaTypeId;
            model.VisaType = candidate.VisaTypeMaster != null ? candidate.VisaTypeMaster.Type : string.Empty;
            model.VisaValidUpto = candidate.VisaValidUpto;
            model.SendJobAlert = candidate.SendJobAlert;
            model.IsExperienced = candidate.IsExperienced;

            /* List Detail */
            model.DegreeList = DegreeMaster.GetCandidateDegrees(model.Id);
            model.SkillList = SkillMaster.GetCandidateSkills(model.Id);

            return model;
        }

        /// <summary>
        /// Create New Candidate
        /// </summary>
        /// <param name="candidateDetailModel">Candidate Detail Model </param>
        internal static Guid CreateCandidateDetail(CandidateDetailModel candidateDetailModel)
        {
            return SaveCandidateDetail(candidateDetailModel);
        }

        /// <summary>
        /// Update Candidate Detail
        /// </summary>
        /// <param name="candidateDetailModel">Candidate Detail Model </param>
        internal static void UpdateCandidateDetail(CandidateDetailModel candidateDetailModel)
        {
            SaveCandidateDetail(candidateDetailModel);
        }

        /// <summary>
        /// Save Candidate Detail
        /// </summary>
        /// <param name="candidateDetailModel">Candidate Detail Model </param>
        private static Guid SaveCandidateDetail(CandidateDetailModel candidateDetailModel)
        {
            Guid candidateId = Guid.Empty;

            using (var db = new XSeedEntities())
            {
                CandidateDetail model = candidateDetailModel.Id != Guid.Empty ? db.CandidateDetails.Find(candidateDetailModel.Id) : new CandidateDetail();

                /* References */
                model.Id = candidateDetailModel.Id;
                model.UserId = candidateDetailModel.UserId;

                /* Personal Detail */
                model.TitleId = candidateDetailModel.TitleId;
                model.FirstName = candidateDetailModel.FirstName;
                model.MiddleName = candidateDetailModel.MiddleName;
                model.LastName = candidateDetailModel.LastName;
                model.BirthDate = candidateDetailModel.BirthDate;
                model.ProfileImagePath = string.IsNullOrEmpty(candidateDetailModel.ProfileImage) ? model.ProfileImagePath : candidateDetailModel.ProfileImage;
                model.GenderId = candidateDetailModel.GenderId;
                model.MaritalStatusId = candidateDetailModel.MaritalStatusId;

                /* Contact Detail */
                model.PrimaryEmail = candidateDetailModel.PrimaryEmail;
                model.SecondaryEmail = candidateDetailModel.SecondaryEmail;
                model.Mobile = candidateDetailModel.Mobile;
                model.Phone = candidateDetailModel.Phone;
                model.Fax = candidateDetailModel.Fax;

                /* Address Detail */
                model.Address1 = candidateDetailModel.Address1;
                model.Address2 = candidateDetailModel.Address2;
                model.Address3 = candidateDetailModel.Address3;
                model.CountryId = candidateDetailModel.CountryId;
                model.StateId = candidateDetailModel.StateId;
                model.CityId = candidateDetailModel.CityId;
                model.Zip = candidateDetailModel.Zip;

                /* Other Detail */
                model.HavePassport = candidateDetailModel.HavePassport;
                model.PassportValidUpto = candidateDetailModel.PassportValidUpto;
                model.VisaTypeId = candidateDetailModel.VisaTypeId;
                model.VisaValidUpto = candidateDetailModel.VisaValidUpto;
                model.SendJobAlert = candidateDetailModel.SendJobAlert;
                model.IsExperienced = candidateDetailModel.IsExperienced;

                if (candidateDetailModel.Id == Guid.Empty)
                {
                    db.CandidateDetails.Add(model);
                }

                db.SaveChanges();

                candidateId = model.Id;

                /* Save Candidate Skills */
                SkillMaster.SaveCandidateSkills(model.Id, candidateDetailModel.SkillList);

                /* Save Candidate Degrees */
                DegreeMaster.SaveCandidateDegress(model.Id, candidateDetailModel.DegreeList);
            }

            return candidateId;
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>        
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        internal static PaginationModel GetPaginationInfo(int pageSize)
        {
            PaginationModel model = new PaginationModel();

            using (var db = new XSeedEntities())
            {
                model.TotalCount = db.CandidateDetails.Count();
                model.TotalPages = Math.Ceiling((double)model.TotalCount / pageSize);
            }

            return model;
        }

        #endregion
    }
}