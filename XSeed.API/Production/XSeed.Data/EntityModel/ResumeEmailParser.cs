﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.ResumeEmail;
using XSeed.Utility;

namespace XSeed.Data.Entity
{
    [MetadataType(typeof(ResumeEmailParserMD))]
    public partial class ResumeEmailParser
    {
        #region property declaration
        public class ResumeEmailParserMD
        {
            public System.Guid Id { get; set; }
            public string Name { get; set; }
            public string SourceEmail { get; set; }
            public string Html { get; set; }
            public Nullable<System.DateTime> Date { get; set; }
            public string EmailUniqueId { get; set; }
            public Nullable<bool> IsRead { get; set; }
            public string Subject { get; set; }
        }
        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Resume Parsed Email
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>
        /// <returns>List of All Parsed Resume Email</returns>
        internal static List<ResumeEmailParserModel> ListAllResumeEmails(Guid organizationId, int pageSize, int pageNumber, string sortBy = "Date", string sortOrder = "desc")
        {
            List<ResumeEmailParserModel> listResumeParsedEmail = new List<ResumeEmailParserModel>();

            using (var db = new XSeedEntities())
            {
                var resumeParsedEmailList = db.ResumeEmailParsers.OrderBy(sortBy + " " + sortOrder).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

                if (resumeParsedEmailList != null)
                {
                    foreach (var parsedEmail in resumeParsedEmailList)
                    {
                        listResumeParsedEmail.Add(PopulateResumeEmailListModel(parsedEmail));
                    }
                }
            }
            return listResumeParsedEmail;
        }

        /// <summary>
        /// Get Resume Parsed Email
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="Id">Id</param>
        /// <returns>Single Parsed Resume Email</returns>
        internal static ResumeEmailParserModel GetResumeEmailDetail(Guid? organizationId, Guid? Id)
        {
            using (var db = new XSeedEntities())
            {
                ResumeEmailParser reusmeEmail = db.ResumeEmailParsers.Find(Id);

                if (reusmeEmail != null)
                {
                    List<ResumeEmailParserAttachment> attachments = db.ResumeEmailParserAttachments.Where(c => c.EmailUniqueId == reusmeEmail.EmailUniqueId).ToList();
                    return PopulateResumeEmailModel(reusmeEmail, attachments);
                }
            }
            return null;
        }

        /// <summary>
        /// Populate Resume Email Model
        /// </summary>
        /// <param name="model">ResumeEmailParser</param>
        /// <returns></returns>
        private static ResumeEmailParserModel PopulateResumeEmailModel(ResumeEmailParser model, List<ResumeEmailParserAttachment> resumeEmailParserAttachment)
        {
            ResumeEmailParserModel resumeEmailParserModel = new ResumeEmailParserModel();

            resumeEmailParserModel.Id = model.Id;
            resumeEmailParserModel.Name = model.Name;
            resumeEmailParserModel.SourceEmail = model.SourceEmail;
            resumeEmailParserModel.Subject = model.Subject;
            resumeEmailParserModel.Html = model.Html.Replace("\"", "'");
            resumeEmailParserModel.Date = model.Date;
            resumeEmailParserModel.EmailUniqueId = model.EmailUniqueId;
            resumeEmailParserModel.IsRead = model.IsRead;
            resumeEmailParserModel.EmailAttachment = resumeEmailParserAttachment != null ? resumeEmailParserAttachment : null;

            return resumeEmailParserModel;
        }

        /// <summary>
        /// Populate Resume Email List Model
        /// </summary>
        /// <param name="model">ResumeEmailParser</param>
        /// <returns></returns>
        private static ResumeEmailParserModel PopulateResumeEmailListModel(ResumeEmailParser model)
        {
            ResumeEmailParserModel resumeEmailParserModel = new ResumeEmailParserModel();

            resumeEmailParserModel.Id = model.Id;
            resumeEmailParserModel.Name = model.Name;
            resumeEmailParserModel.SourceEmail = model.SourceEmail;
            resumeEmailParserModel.Subject = model.Subject;
            resumeEmailParserModel.Date = model.Date;
            resumeEmailParserModel.IsRead = model.IsRead;

            return resumeEmailParserModel;
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        internal static PaginationModel GetPaginationInfo(Guid organizationId, int pageSize)
        {
            PaginationModel model = new PaginationModel();

            using (var db = new XSeedEntities())
            {
                model.TotalCount = db.ResumeEmailParsers.Count();
                model.TotalPages = Math.Ceiling((double)model.TotalCount / pageSize);
            }

            return model;
        }

        /// <summary>
        /// Update Resume Parsed Email
        /// </summary>
        /// <param name="resumeEmailParserModel">Resume Email Parser Model</param>
        /// <returns></returns>
        internal static void UpdateResumeParsedEmail(Guid Id)
        {
            using (var db = new XSeedEntities())
            {
                ResumeEmailParser email = db.ResumeEmailParsers.Find(Id);
                if (email != null)
                {
                    email.IsRead = true;

                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Get Unread Mail Count 
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <returns>Unread Mail Count</returns>
        internal static int GetUnreadMailCount(Guid organizationId)
        {
            using (var db = new XSeedEntities())
            {
                return (db.ResumeEmailParsers.Count(v => v.IsRead == false));
            }
        }

        #endregion
    }
}
