﻿
namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [MetadataType(typeof(SourceMasterMD))]
    public partial class SourceMaster : IAuditable
    {
        public class SourceMasterMD
        {
            public System.Guid Id { get; set; }
            public string Source { get; set; }
            public string Description { get; set; }
            public Nullable<bool> IsActive { get; set; }
        }
    }
}
