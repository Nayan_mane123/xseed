﻿
namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [MetadataType(typeof(JobTitleMasterMD))]
    public partial class JobTitleMaster:IAuditable
    {
        public class JobTitleMasterMD
        {
            public int Id { get; set; }
            public string Title { get; set; }
            public Nullable<bool> IsActive { get; set; }
            public Guid OrganizationId { get; set; }
        }
    }
}
