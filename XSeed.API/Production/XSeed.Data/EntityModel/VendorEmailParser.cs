﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Vendors;
using XSeed.Utility;

namespace XSeed.Data.Entity
{
    [MetadataType(typeof(VendorEmailParserMD))]
    public partial class VendorEmailParser
    {
        #region property declaration
        public class VendorEmailParserMD
        {
            public System.Guid Id { get; set; }
            public string Vendor { get; set; }
            public string SourceEmail { get; set; }
            public string Html { get; set; }
            public Nullable<System.DateTime> Date { get; set; }
            public string EmailUniqueId { get; set; }
            public Nullable<bool> IsActive { get; set; }
            public Nullable<bool> IsRead { get; set; }
        }
        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Vendor Parsed Email
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>
        /// <returns>List of All Parsed Vendor Email</returns>
        internal static List<VendorEmailParserModel> ListAllParsedVendorEmail(Guid organizationId, int pageSize, int pageNumber, string sortBy = "Date", string sortOrder = "desc")
        {
            List<VendorEmailParserModel> listVendorParsedEmail = new List<VendorEmailParserModel>();

            using (var db = new XSeedEntities())
            {
                var vendorParsedEmailList = db.VendorEmailParsers
                 .Join(db.Vendors, vep => vep.SourceEmail, v => v.SourceEmailId, (vep, v) => new { vep, v })
                 .Where(j => j.v.CompanyDetail.OrganizationId == organizationId && j.vep.IsActive == true)
                 .ToList();

                if (vendorParsedEmailList != null && vendorParsedEmailList.Count > 0)
                {
                    vendorParsedEmailList = vendorParsedEmailList.OrderBy("vep." + sortBy + " " + sortOrder).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

                    foreach (var parsedEmail in vendorParsedEmailList)
                    {
                        //get attachments
                        List<VendorEmailParserAttachment> attachments = db.VendorEmailParserAttachments.Where(c => c.EmailUniqueId == parsedEmail.vep.EmailUniqueId).ToList();

                        listVendorParsedEmail.Add(PopulateVendorEmailModel(parsedEmail.vep, attachments));
                    }
                }
            }
            return listVendorParsedEmail;
        }

        /// <summary>
        /// Get Vendor Parsed Email
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="Id">Id</param>
        /// <returns>Single Parsed Vendor Email</returns>
        internal static VendorEmailParserModel GetVendorParsedEmail(Guid? organizationId, Guid? Id)
        {
            VendorEmailParserModel model = new VendorEmailParserModel();

            using (var db = new XSeedEntities())
            {
                VendorEmailParser vendor = db.VendorEmailParsers.Find(Id);

                if (vendor != null)
                {
                    List<VendorEmailParserAttachment> attachments = db.VendorEmailParserAttachments.Where(c => c.EmailUniqueId == vendor.EmailUniqueId).ToList();
                    PopulateVendorEmailModel(vendor, attachments);
                }
            }

            return model;
        }

        /// <summary>
        /// Populate Vendor Email Model
        /// </summary>
        /// <param name="model">VendorEmailParser</param>
        /// <returns></returns>
        private static VendorEmailParserModel PopulateVendorEmailModel(VendorEmailParser model, List<VendorEmailParserAttachment> vendorEmailParserAttachments)
        {
            VendorEmailParserModel vendorEmailParserModel = new VendorEmailParserModel();

            vendorEmailParserModel.Id = model.Id;
            vendorEmailParserModel.Vendor = model.Vendor;
            vendorEmailParserModel.SourceEmail = model.SourceEmail;
            vendorEmailParserModel.Html = model.Html;
            vendorEmailParserModel.Date = model.Date;
            vendorEmailParserModel.EmailUniqueId = model.EmailUniqueId;
            vendorEmailParserModel.IsActive = model.IsActive;
            vendorEmailParserModel.IsRead = model.IsRead;
            vendorEmailParserModel.EmailAttachment = vendorEmailParserAttachments;

            return vendorEmailParserModel;
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        internal static PaginationModel GetPaginationInfo(Guid organizationId, int pageSize)
        {
            PaginationModel model = new PaginationModel();

            using (var db = new XSeedEntities())
            {
                model.TotalCount = db.VendorEmailParsers.Join(db.Vendors, vep => vep.SourceEmail, v => v.SourceEmailId, (vep, v) => new { vep, v })
                 .Where(j => j.v.CompanyDetail.OrganizationId == organizationId && j.vep.IsActive == true).Count();
                model.TotalPages = Math.Ceiling((double)model.TotalCount / pageSize);
            }

            return model;
        }

        /// <summary>
        /// Update Vendor Parsed Email
        /// </summary>
        /// <param name="vendorEmailParserModel">Vendor Email Parser Model</param>
        /// <returns></returns>
        internal static void UpdateVendorParsedEmail(VendorEmailParserModel model)
        {
            using (var db = new XSeedEntities())
            {
                VendorEmailParser email = db.VendorEmailParsers.Where(c => c.EmailUniqueId == model.EmailUniqueId).FirstOrDefault();
                if (email != null)
                {
                    //Update Vendor Email
                    email.Id = model.Id;
                    email.Vendor = model.Vendor;
                    email.SourceEmail = model.SourceEmail;
                    email.Html = model.Html;
                    email.Date = model.Date;
                    email.EmailUniqueId = model.EmailUniqueId;
                    email.IsActive = model.IsActive;
                    email.IsRead = model.IsRead;

                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Get Unread Mail Count 
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <returns>Unread Mail Count</returns>
        internal static int GetUnreadMailCount(Guid organizationId)
        {
            using (var db = new XSeedEntities())
            {
                var count = db.VendorEmailParsers.Join(db.Vendors, vep => vep.SourceEmail, v => v.SourceEmailId, (vep, v) => new { vep, v })
                .Where(j => j.v.CompanyDetail.OrganizationId == organizationId && j.vep.IsActive == true && j.vep.IsRead == false).Count();

                return count;
            }
        }

        #endregion
    }
}
