﻿
namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public partial class JobStatusMaster : IAuditable
    {
        public class JobStatusMasterMD
        {
            public int Id { get; set; }
            public string Status { get; set; }
            public Nullable<bool> IsActive { get; set; }
        }
    }
}
