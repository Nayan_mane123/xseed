﻿
namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [MetadataType(typeof(SocialNetworkTypeMasterMD))]
    public partial class SocialNetworkTypeMaster:IAuditable
    {
        public class SocialNetworkTypeMasterMD
        {
            public int Id { get; set; }
            public string Type { get; set; }
            public Nullable<bool> IsActive { get; set; }
        }
    }
}
