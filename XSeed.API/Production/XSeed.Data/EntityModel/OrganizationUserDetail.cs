﻿namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;
    using XSeed.Data.Entity;
    using XSeed.Data.ViewModel.Activity;
    using XSeed.Data.ViewModel.Common;
    using XSeed.Data.ViewModel.User.OrganizationUser;

    [MetadataType(typeof(OrganizationUserDetailMD))]
    public partial class OrganizationUserDetail : IAuditable
    {
        #region Property Declaration

        public class OrganizationUserDetailMD
        {
            public int Id { get; set; }

            [Required]
            public int UserId { get; set; }

            [Required]
            public int OrganizationId { get; set; }

            public Nullable<int> TitleId { get; set; }
            public string FirstName { get; set; }
            public string MiddleName { get; set; }
            public string LastName { get; set; }
            public Nullable<System.DateTime> BirthDate { get; set; }
            public Nullable<System.DateTime> AnniversaryDate { get; set; }
            public string ProfileImagePath { get; set; }
            public Nullable<int> ReportingTo { get; set; }

            [Required]
            public string PrimaryEmail { get; set; }

            public string SecondaryEmail { get; set; }
            public string Mobile { get; set; }
            public string Phone { get; set; }
            public string Fax { get; set; }
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string Address3 { get; set; }
            public Nullable<int> CountryId { get; set; }
            public Nullable<int> StateId { get; set; }
            public Nullable<int> CityId { get; set; }
            public string Zip { get; set; }
        }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get List of Organization Users
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <returns>List of Organization Users</returns>
        public static List<OrganizationUserModel> ListOrganizationUsers(Guid organizationId)
        {
            using (var db = new XSeedEntities())
            {
                List<OrganizationUserModel> organizationUserList = new List<OrganizationUserModel>();
                var listOrganizationUsers = db.OrganizationUserDetails.Where(u => u.OrganizationId == organizationId).ToList().OrderByDescending(d => d.CreatedOn);

                foreach (var organizationUser in listOrganizationUsers)
                {
                    OrganizationUserModel model = populateOrganizationUserModel(organizationUser);
                    organizationUserList.Add(model);
                }

                return organizationUserList;
            }
        }

        /// <summary>
        /// Enable Disable User
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="organizationId">Organization Id</param>
        /// <param name="isActive">Activate/Deactivate</param>
        public static bool EnableDisableUser(Guid userId, Guid organizationId, bool isActive)
        {
            bool isShowPopUp = false;
            using (var db = new XSeedEntities())
            {
                // find user from userLogin 
                var user = db.UserLogins.Find(userId);
                var Requirements = db.JobDetails.Where(s => s.OrganizationUserDetails.Any(o => o.UserId == userId)).ToList();
                if (user != null)
                {
                    if (!Requirements.Any())
                    {
                        user.IsActive = isActive;
                        db.SaveChanges();
                    }
                    else
                    {
                        isShowPopUp = true;
                    }
                }
            }
            return isShowPopUp;
        }

        public static void TransferRequirements(Guid userId, Guid transferedUserId)
        {
            using (var db = new XSeedEntities())
            {
                if (userId != null && transferedUserId != null)
                {
                    OrganizationUserDetail transferedToUser = db.OrganizationUserDetails.Find(transferedUserId);

                    var Requirements = db.JobDetails.Where(s => s.OrganizationUserDetails.Any(o => o.UserId == userId)).ToList();
                    foreach (var item in Requirements)
                    {
                        /*Find job by Id*/
                        JobDetail job = db.JobDetails.Find(item.Id);

                        /* Get specific user that is assigned with requirement */
                        var user = job.OrganizationUserDetails.FirstOrDefault(o => o.UserId == userId);

                        /* Remove assignment for that Requirement from user */
                        job.OrganizationUserDetails.Remove(user);

                        /* Transfer Requirements to other user*/
                        job.OrganizationUserDetails.Add(transferedToUser);

                    }
                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Get List of Organization Users by Role
        /// </summary>
        /// <param name="organizationId">Role Id</param>
        /// <returns>List of Organization Users</returns>
        public static List<OrganizationUserModel> ListOrganizationUsersByRole(Guid roleId)
        {
            using (var db = new XSeedEntities())
            {
                List<OrganizationUserModel> organizationUserList = new List<OrganizationUserModel>();
                var listOrganizationUsers = db.OrganizationUserDetails.Where(u => u.RoleId == roleId).ToList().OrderByDescending(d => d.CreatedOn);

                foreach (var organizationUser in listOrganizationUsers)
                {
                    OrganizationUserModel model = populateOrganizationUserModel(organizationUser);
                    organizationUserList.Add(model);
                }

                return organizationUserList;
            }
        }

        /// <summary>
        /// Get Specific User Details From Organization 
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="organizationId">Organization Id</param>
        /// <returns>List of Organization Users</returns>
        public static OrganizationUserModel GetUser(Guid? userId, Guid organizationId)
        {
            OrganizationUserModel model = new OrganizationUserModel();

            using (var db = new XSeedEntities())
            {
                var organizationUser = db.OrganizationUserDetails.FirstOrDefault(u => u.OrganizationId == organizationId && u.UserId == userId);

                if (organizationUser != null)
                {
                    model = populateOrganizationUserModel(organizationUser);
                }

                return model;
            }
        }

        /// <summary>
        /// Get Specific User Credentials
        /// </summary>
        /// <param name="userName">User Name</param>
        internal static OrganizationUserModel GetUser(string userName)
        {
            OrganizationUserModel model = new OrganizationUserModel();

            using (var db = new XSeedEntities())
            {
                var organizationUser = db.OrganizationUserDetails.FirstOrDefault(u => u.PrimaryEmail == userName);

                if (organizationUser != null)
                {
                    model = populateOrganizationUserModel(organizationUser);
                }
            }

            return model;
        }

        /// <summary>
        /// Poopulate Organzation User Model
        /// </summary>
        /// <param name="organizationUser">Organization User Database Detail</param>
        /// <returns>Organization User Model</returns>
        private static OrganizationUserModel populateOrganizationUserModel(OrganizationUserDetail organizationUser)
        {
            OrganizationUserModel model = new OrganizationUserModel();

            /* References */
            model.Id = organizationUser.Id;
            model.UserId = organizationUser.UserId;
            model.UserName = organizationUser.UserLogin != null ? organizationUser.UserLogin.UserName : string.Empty;

            /* Organization Info */
            model.OrganizationId = organizationUser.OrganizationId;
            model.OrganizationName = organizationUser.OrganizationDetail != null ? organizationUser.OrganizationDetail.Name : string.Empty;

            model.RoleId = organizationUser.RoleId;
            model.RoleName = organizationUser.UserRoleMaster.Role;

            /* Personal Info */
            model.TitleId = organizationUser.TitleId;
            model.Title = organizationUser.TitleMaster != null ? organizationUser.TitleMaster.Name : string.Empty;
            model.FirstName = organizationUser.FirstName;
            model.MiddleName = organizationUser.MiddleName;
            model.LastName = organizationUser.LastName;
            model.BirthDate = organizationUser.BirthDate;
            model.AnniversaryDate = organizationUser.AnniversaryDate;
            model.ProfileImage = organizationUser.ProfileImagePath;
            model.ReportingTo = organizationUser.ReportingTo;

            /* Contact Info */
            model.PrimaryEmail = organizationUser.PrimaryEmail;
            model.SecondaryEmail = organizationUser.SecondaryEmail;
            model.Mobile = organizationUser.Mobile;
            model.Phone = organizationUser.Phone;
            model.Fax = organizationUser.Fax;

            /* Address Info */
            model.Address1 = organizationUser.Address1;
            model.Address2 = organizationUser.Address2;
            model.Address3 = organizationUser.Address3;
            model.CountryId = organizationUser.CountryId;
            model.Country = organizationUser.CountryMaster != null ? organizationUser.CountryMaster.Name : string.Empty;
            model.StateId = organizationUser.StateId;
            model.State = organizationUser.StateMaster != null ? organizationUser.StateMaster.Name : string.Empty;
            model.CityId = organizationUser.CityId;
            model.City = organizationUser.CityMaster != null ? organizationUser.CityMaster.Name : string.Empty;
            model.Zip = organizationUser.Zip;
            model.IsActive = organizationUser.UserLogin.IsActive;
            /* Get audit trail info */
            model.AuditTrailInfo = AuditTrail.GetAuditTrailInfo(organizationUser.CreatedBy, organizationUser.CreatedOn, organizationUser.ModifiedBy, organizationUser.ModifiedOn);

            return model;
        }

        /// <summary>
        /// Create Organization User
        /// </summary>
        /// <param name="OrganizationUserModel">Organization User Model</param>
        public static Guid CreateOrganizationUser(OrganizationUserModel organizationUserModel)
        {
            /* Save Organization User Detail */
            return SaveOrganizationUserDetail(organizationUserModel);
        }

        /// <summary>
        /// Update Organization User
        /// </summary>
        /// <param name="OrganizationUserModel">Organization User Model</param>
        public static void UpdateOrganizationUser(OrganizationUserModel organizationUserModel)
        {
            /* Save Organization User Detail */
            SaveOrganizationUserDetail(organizationUserModel);
        }

        /// <summary>
        /// function to Save Organization User Detail
        /// </summary>
        /// <param name="OrganizationUserModel">Organization User Model</param>
        internal static Guid SaveOrganizationUserDetail(OrganizationUserModel model)
        {
            try
            {
                OrganizationUserDetail organizationUser = new OrganizationUserDetail();

                using (var db = new XSeedEntities())
                {
                    if (model.Id != Guid.Empty)
                    {
                        organizationUser = db.OrganizationUserDetails.Find(model.Id); // Update Organization User info
                    }
                    else
                    {
                        organizationUser = new OrganizationUserDetail(); // Insert Organization User info
                        organizationUser.UserId = model.UserId;
                        organizationUser.OrganizationId = model.OrganizationId;
                    }

                    /* populate model */
                    organizationUser.TitleId = model.TitleId;
                    organizationUser.FirstName = model.FirstName;
                    organizationUser.MiddleName = model.MiddleName;
                    organizationUser.LastName = model.LastName;
                    organizationUser.BirthDate = model.BirthDate;
                    organizationUser.AnniversaryDate = model.AnniversaryDate;
                    organizationUser.ProfileImagePath = string.IsNullOrEmpty(model.ProfileImage) ? organizationUser.ProfileImagePath : model.ProfileImage;
                    organizationUser.ReportingTo = model.ReportingTo;
                    organizationUser.PrimaryEmail = model.PrimaryEmail;
                    organizationUser.SecondaryEmail = model.SecondaryEmail;
                    organizationUser.Mobile = model.Mobile;
                    organizationUser.Phone = model.Phone;
                    organizationUser.Fax = model.Fax;
                    organizationUser.Address1 = model.Address1;
                    organizationUser.Address2 = model.Address2;
                    organizationUser.Address3 = model.Address3;
                    organizationUser.CountryId = model.CountryId;
                    organizationUser.StateId = model.StateId;
                    organizationUser.CityId = model.CityId;
                    organizationUser.Zip = model.Zip;
                    organizationUser.RoleId = model.RoleId;

                    if (organizationUser.Id == Guid.Empty)
                    {
                        db.OrganizationUserDetails.Add(organizationUser);
                    }

                    db.SaveChanges();

                    return organizationUser.Id;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        #endregion

        #region Save Job User Mapping

        /// <summary>
        /// Save Job Organization User Mapping
        /// </summary>
        /// <param name="jobId">JobId</param>
        /// <param name="organizationUserId">OrganizationUserId</param>
        internal static void SaveJobUserMapping(Guid jobId, List<LookUpModel> OrganizationUsers, bool isCreate = false)
        {
            if (OrganizationUsers != null)
            {
                string oldRecruiters = string.Empty, newRecruiters = string.Empty;
                StringBuilder recruiter = new StringBuilder();

                ICollection<OrganizationUserDetail> assignedUsers = null;

                using (var db = new XSeedEntities())
                {
                    JobDetail jobDetail = db.JobDetails.Find(jobId);

                    if (jobDetail != null)
                    {
                        if (!isCreate)
                        {
                            assignedUsers = jobDetail.OrganizationUserDetails;

                            /* set old recruiters */
                            oldRecruiters = string.Join(",", assignedUsers.Select(u => u.FirstName + " " + u.LastName)).TrimEnd(',');

                            /* clear old values */
                            jobDetail.OrganizationUserDetails.Clear();
                        }

                        foreach (var user in OrganizationUsers)
                        {
                            OrganizationUserDetail model = db.OrganizationUserDetails.Find(user.Id);
                            jobDetail.OrganizationUserDetails.Add(model);

                            /* set new recruiters */
                            recruiter.Append("," + (model.FirstName + " " + model.LastName));
                        }

                        db.SaveChanges();

                        /* process string */
                        newRecruiters = recruiter.ToString().TrimStart(',');

                        if (string.Compare(oldRecruiters, newRecruiters) != 0)
                        {
                            if (isCreate || BaseModel.IsActivityUpdate)
                            {
                                /* update activity log */
                                JobActivityLogModel.UpdateAssignedRecruiterLog(jobId, oldRecruiters, newRecruiters);
                            }
                            else
                            {
                                JobActivityLogModel jobActivityLogModel = new JobActivityLogModel();
                                jobActivityLogModel.JobId = jobId;

                                /* add change log */
                                jobActivityLogModel.Activity = new List<ChangeLog>();
                                jobActivityLogModel.Activity.Add(new ChangeLog() { EntityName = "Requirement Detail", PropertyName = "Assigned Recruiter", OldValue = oldRecruiters, NewValue = newRecruiters, DateChanged = DateTime.UtcNow });

                                JobActivityLogModel.Log(jobId, "Action", "Updated Requirement", jobActivityLogModel.Activity, null);

                                /* Update Audit Trail */
                                JobDetail.UpdateAuditTrail(jobId);
                            }
                        }
                        else
                        {
                            BaseModel.IsActivityUpdate = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Get Job Organization User Mapping
        /// </summary>
        /// <param name="jobId">JobId</param>
        /// <returns>OrganizationUserId</returns>
        internal static List<LookUpModel> GetJobUserMapping(Guid jobId)
        {
            List<LookUpModel> organizationUser = new List<LookUpModel>();
            using (var db = new XSeedEntities())
            {
                JobDetail model = db.JobDetails.Find(jobId);

                if (model != null)
                {
                    foreach (var user in model.OrganizationUserDetails)
                    {
                        LookUpModel userModel = new LookUpModel();
                        userModel.Id = user.Id;
                        userModel.Name = user.FirstName + " " + user.LastName;
                        organizationUser.Add(userModel);
                    }
                }
            }
            return organizationUser.ToList();
        }

        #endregion

        #region Common

        /// <summary>
        /// Get Organization User Name - (Created By) 
        /// </summary>
        /// <param name="Id">User Id</param>
        /// <returns>Created By User Name</returns>
        public static string GetCreatedByUserName(Guid? Id)
        {
            string userName = string.Empty;

            if (Id != null)
            {
                using (var db = new XSeedEntities())
                {
                    var recruiter = db.OrganizationUserDetails.FirstOrDefault(u => u.UserId == Id);

                    if (recruiter != null)
                    {
                        userName = recruiter.FirstName + " " + recruiter.LastName;
                    }
                }
            }

            return userName;
        }


        /// <summary>
        /// Get Organization User Email - (Created By) 
        /// </summary>
        /// <param name="Id">User Id</param>
        /// <returns>Created By User Email</returns>
        public static string GetCreatedByUserEmail(Guid? Id)
        {
            string email = string.Empty;

            if (Id != null)
            {
                using (var db = new XSeedEntities())
                {
                    var recruiter = db.OrganizationUserDetails.FirstOrDefault(u => u.UserId == Id);

                    if (recruiter != null)
                    {
                        email = recruiter.PrimaryEmail;
                    }
                }
            }

            return email;
        }

        /// <summary>
        /// Get Organization User profile image file name - (Created By) 
        /// </summary>
        /// <param name="Id">User Id</param>
        /// <returns>Created By User image</returns>
        internal static string GetCreatedByUserProfileImage(Guid Id)
        {
            string name = string.Empty;

            if (Id != null)
            {
                using (var db = new XSeedEntities())
                {
                    var recruiter = db.OrganizationUserDetails.FirstOrDefault(u => u.UserId == Id);

                    if (recruiter != null)
                    {
                        name = recruiter.ProfileImagePath;
                    }
                }
            }

            return name;
        }

        #endregion

        /// <summary>
        /// Check Valid User
        /// </summary>
        /// <param name="userId">UserId</param>
        /// <returns>Bool</returns>
        public static bool? CheckValidUser(Guid userId)
        {
            using (var db = new XSeedEntities())
            {
                if (userId != null)
                {
                    return db.UserLogins.Find(userId).IsActive;
                }
                return false;
            }
        }
    }
}