﻿
namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using XSeed.Data.ViewModel.Company;
    using XSeed.Utility;

    [MetadataType(typeof(CompanyDetailMD))]
    public partial class CompanyDetail : IAuditable
    {
        #region Property Declaration

        public class CompanyDetailMD
        {
            public int Id { get; set; }
            public int OrganizationId { get; set; }
            public Nullable<int> CompanyTypeId { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public string Website { get; set; }
            public string CareerPageURL { get; set; }
            public string LinkedInURL { get; set; }
            public string GooglePlusURL { get; set; }
            public string FacebookURL { get; set; }
            public string TwitterURL { get; set; }
            public string LogoImage { get; set; }
            public string Size { get; set; }
            public string Phone { get; set; }
            public string Fax { get; set; }
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string Address3 { get; set; }
            public Nullable<int> CountryId { get; set; }
            public Nullable<int> StateId { get; set; }
            public Nullable<int> CityId { get; set; }
            public string Zip { get; set; }
            public Nullable<int> CompanySourceId { get; set; }
            public string Via { get; set; }
            public string ViaWebsite { get; set; }
            public Nullable<bool> IsActive { get; set; }
        }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get list of companies belongs to an organization
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <returns>Company Detail List</returns>
        internal static List<CompanyDetailModel> ListOrganizationCompanies(Guid organizationId)
        {
            List<CompanyDetailModel> list = new List<CompanyDetailModel>();

            using (var db = new XSeedEntities())
            {
                var companies = db.CompanyDetails.Where(o => o.OrganizationId == organizationId && o.IsActive == Constants.IsActiveTrue).ToList().OrderByDescending(d => d.CreatedOn); // return active company list

                foreach (var company in companies)
                {
                    CompanyDetailModel model = populateCompanyDetailModel(company);
                    list.Add(model);
                }
            }

            return list;
        }

        /// <summary>
        /// Get Company Detail
        /// </summary>        
        /// <param name="companyId">Company Id</param>
        /// <returns>Company Detail</returns>
        internal static CompanyDetailModel GetCompanyDetail(Guid organizationId, Guid? companyId)
        {
            CompanyDetailModel model = new CompanyDetailModel();

            using (var db = new XSeedEntities())
            {
                var company = db.CompanyDetails.FirstOrDefault(c => c.OrganizationId == organizationId && c.Id == companyId);

                if (company != null)
                {
                    model = populateCompanyDetailModel(company);
                }
            }

            return model;
        }

        /// <summary>
        /// Populate Company Detail Model
        /// </summary>
        /// <param name="company">Company Detail</param>
        /// <returns>Company Detail Model</returns>
        private static CompanyDetailModel populateCompanyDetailModel(CompanyDetail company)
        {
            CompanyDetailModel model = new CompanyDetailModel();

            /* References */
            model.Id = company.Id;
            model.OrganizationId = company.OrganizationId;
            model.OrganizationName = company.OrganizationDetail != null ? company.OrganizationDetail.Name : string.Empty;
            model.CompanyTypeId = company.CompanyTypeId;
            model.CompanyType = company.CompanyTypeMaster != null ? company.CompanyTypeMaster.Name : string.Empty;

            /* Company Detail */
            model.Name = company.Name;
            model.Description = company.Description;
            model.Website = company.Website;
            model.CareerPageURL = company.CareerPageURL;
            model.LinkedInURL = company.LinkedInURL;
            model.TwitterURL = company.TwitterURL;
            model.GooglePlusURL = company.GooglePlusURL;
            model.FacebookURL = company.FacebookURL;
            model.ProfileImage = company.LogoImage;
            model.Size = company.Size;

            /* Contact Info */
            model.Phone = company.Phone;
            model.Fax = company.Fax;

            /* Address Info */
            model.Address1 = company.Address1;
            model.Address2 = company.Address2;
            model.Address3 = company.Address3;
            model.CountryId = company.CountryId;
            model.Country = company.CountryMaster != null ? company.CountryMaster.Name : string.Empty;
            model.StateId = company.StateId;
            model.State = company.StateMaster != null ? company.StateMaster.Name : string.Empty;
            model.CityId = company.CityId;
            model.City = company.CityMaster != null ? company.CityMaster.Name : string.Empty;
            model.Zip = company.Zip;

            /* Company Source Info */
            model.CompanySourceId = company.CompanySourceId;
            model.CompanySourceName = company.CompanySource != null ? company.CompanySource.Name : string.Empty;
            model.Via = company.Via;
            model.ViaWebsite = company.ViaWebsite;
            model.SubmissionPageURL = company.SubmissionPageURL;

            model.IndustryTypeList = IndustryTypeMaster.GetCompanyIndustryTypes(model.Id);

            return model;
        }

        /// <summary>
        /// Save Company Detail
        /// </summary>
        /// <param name="model">Company Detail Model</param>
        internal static Guid SaveCompanyDetail(CompanyDetailModel model)
        {
            Guid companyId = Guid.Empty;

            using (var db = new XSeedEntities())
            {
                CompanyDetail companyDetail = model.Id != Guid.Empty ? db.CompanyDetails.Find(model.Id) : new CompanyDetail();

                companyDetail.Id = model.Id;
                companyDetail.OrganizationId = (Guid)model.OrganizationId;
                companyDetail.CompanyTypeId = model.CompanyTypeId;

                /* Company Detail */
                companyDetail.Name = model.Name;
                companyDetail.Description = model.Description;
                companyDetail.Website = model.Website;
                companyDetail.CareerPageURL = model.CareerPageURL;
                companyDetail.LinkedInURL = model.LinkedInURL;
                companyDetail.TwitterURL = model.TwitterURL;
                companyDetail.GooglePlusURL = model.GooglePlusURL;
                companyDetail.FacebookURL = model.FacebookURL;
                companyDetail.LogoImage = string.IsNullOrEmpty(model.ProfileImage) ? companyDetail.LogoImage : model.ProfileImage;
                companyDetail.Size = model.Size;

                /* Contact Info */
                companyDetail.Phone = model.Phone;
                companyDetail.Fax = model.Fax;

                /* Address Info */
                companyDetail.Address1 = model.Address1;
                companyDetail.Address2 = model.Address2;
                companyDetail.Address3 = model.Address3;
                companyDetail.CountryId = model.CountryId;
                companyDetail.StateId = model.StateId;
                companyDetail.CityId = model.CityId;
                companyDetail.Zip = model.Zip;

                /* Company Source Info */
                companyDetail.CompanySourceId = model.CompanySourceId;
                companyDetail.Via = model.Via;
                companyDetail.ViaWebsite = model.ViaWebsite;
                companyDetail.SubmissionPageURL = model.SubmissionPageURL;
                companyDetail.IsActive = Constants.IsActiveTrue;

                if (companyDetail.Id == Guid.Empty)
                    db.CompanyDetails.Add(companyDetail);

                db.SaveChanges();

                companyId = companyDetail.Id;

                /* Insert/ Update industry types  */
                IndustryTypeMaster.SaveCompanyIndustryTypes(companyDetail.Id, model.IndustryTypeList);
            }

            return companyId;
        }

        /// <summary>
        /// Get company name by id
        /// </summary>
        /// <param name="guid">Id</param>
        /// <returns>Company Name</returns>
        internal static string GetCompanyName(Guid Id)
        {
            string name = string.Empty;

            if (Id != null && Id != Guid.Empty)
            {
                using (var db = new XSeedEntities())
                {
                    var company = db.CompanyDetails.Find(Id);
                    name = company != null ? company.Name : string.Empty;
                }
            }

            return name;
        }

        #endregion
    }
}