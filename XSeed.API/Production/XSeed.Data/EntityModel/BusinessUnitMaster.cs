﻿namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using XSeed.Data.ViewModel.Common;
    using XSeed.Utility;

    [MetadataType(typeof(BusinessUnitMasterMD))]
    public partial class BusinessUnitMaster : IAuditable
    {
        #region Property Declaration

        public class BusinessUnitMasterMD
        {
            public System.Guid Id { get; set; }
            public string Name { get; set; }
            public Nullable<System.Guid> OrganizationId { get; set; }
            public Nullable<bool> IsActive { get; set; }
        }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Add new business unit
        /// </summary>
        /// <param name="lookUpModel">LookUpModel</param>
        /// <param name="organizationId">organization Id</param>
        /// <returns>Business Unit Id</returns>
        internal static Guid AddBusinessUnit(LookUpModel lookUpModel)
        {

            Guid businessUnitId = Guid.Empty;

            if (lookUpModel != null)
            {
                using (var db = new XSeedEntities())
                {
                    if (db.BusinessUnitMasters.Any(s => s.Name == lookUpModel.Name && s.OrganizationId == lookUpModel.OrganizationId))
                    {
                        businessUnitId = db.BusinessUnitMasters.FirstOrDefault(s => s.Name == lookUpModel.Name && s.OrganizationId == lookUpModel.OrganizationId).Id;
                    }
                    else
                    {
                        /* Initialise & populate model */
                        BusinessUnitMaster model = new BusinessUnitMaster();
                        model.Name = lookUpModel.Name;
                        model.Description = lookUpModel.Description;
                        model.OrganizationId = lookUpModel.OrganizationId;
                        model.IsActive = Constants.IsActiveTrue;

                        /* Save to db */
                        db.BusinessUnitMasters.Add(model);
                        db.SaveChanges();

                        businessUnitId = model.Id;
                    }
                }
            }

            return businessUnitId;
        }

        #endregion

        /// <summary>
        /// Update BusinessUnit
        /// </summary>
        /// <param name="lookUpModel"></param>
        /// <param name="organizationId"></param>
        internal static void PutBusinessUnit(LookUpModel lookUpModel)
        {
            using (var db = new XSeedEntities())
            {

                BusinessUnitMaster model = db.BusinessUnitMasters.Find(lookUpModel.Id);
                if (model != null)
                {
                    model.Name = lookUpModel.Name;
                    model.Description = lookUpModel.Description;
                }
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Delete Business Unit
        /// </summary>
        /// <param name="genericLookupId"></param>
        internal static void DeleteBusinessUnit(Guid genericLookupId)
        {
            using (var db = new XSeedEntities())
            {
                BusinessUnitMaster model = db.BusinessUnitMasters.Find(genericLookupId);
                db.BusinessUnitMasters.Remove(model);
                db.SaveChanges();
            }
        }
    }
}
