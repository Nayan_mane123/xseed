﻿namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [MetadataType(typeof(CandidateStatusMasterMD))]
    public partial class CandidateStatusMaster
    {
        public class CandidateStatusMasterMD
        {
            public System.Guid Id { get; set; }
            public string Status { get; set; }
            public string Description { get; set; }
            public Nullable<bool> IsActive { get; set; }
        }
    }
}
