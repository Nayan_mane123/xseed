﻿
namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [MetadataType(typeof(JobTypeMasterMD))]
    public partial class JobTypeMaster : IAuditable
    {
        public class JobTypeMasterMD
        {
            public int Id { get; set; }
            public string Type { get; set; }
            public Nullable<bool> IsActive { get; set; }
            public Guid OrganizationId { get; set; }
        }
    }
}
