﻿
namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using XSeed.Data.ViewModel.Common;
    using XSeed.Utility;

    [MetadataType(typeof(CompanySourceMD))]
    public partial class CompanySource : IAuditable
    {
        #region Property Declaration
        public class CompanySourceMD
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public Nullable<bool> IsActive { get; set; }
        }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Create New Company Source
        /// </summary>
        /// <param name="lookUpModel">LookUpModel </param>        
        internal static Guid CreateCompanySource(LookUpModel lookUpModel)
        {
            Guid companySourceId = Guid.Empty;

            if (lookUpModel != null)
            {
                using (var db = new XSeedEntities())
                {
                    CompanySource companySourceMaster = (lookUpModel.Id != null && lookUpModel.Id != Guid.Empty
                         && lookUpModel.OrganizationId != null && lookUpModel.OrganizationId != Guid.Empty)
                        ? db.CompanySources.Find(lookUpModel.Id) : new CompanySource();

                    //jobtitleMaster.Id = (Guid)lookUpModel.Id;

                    companySourceMaster.Name = lookUpModel.Name;
                    companySourceMaster.Description = lookUpModel.Description;
                    companySourceMaster.OrganizationId = lookUpModel.OrganizationId;
                    companySourceMaster.IsActive = Constants.IsActiveTrue;
                    db.CompanySources.Add(companySourceMaster);

                    db.SaveChanges();

                    companySourceId = companySourceMaster.Id;
                }

            }

            return companySourceId;
        }

        /// <summary>
        /// Update Company Source
        /// </summary>
        /// <param name="lookUpModel">LookUpModel</param>
        /// <param name="organizationId"></param>
        internal static void UpdateCompanySource(LookUpModel lookUpModel)
        {
            using (var db = new XSeedEntities())
            {
                CompanySource companySourceMaster = (lookUpModel.Id != null && lookUpModel.Id != Guid.Empty) ? db.CompanySources.Find(lookUpModel.Id) : new CompanySource();
                if (companySourceMaster != null)
                {
                    companySourceMaster.Name = lookUpModel.Name;
                    companySourceMaster.Description = lookUpModel.Description;
                }
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Delete company source
        /// </summary>
        /// <param name="genericLookupId"></param>
        internal static void DeleteCompanySource(Guid genericLookupId)
        {
            using (var db = new XSeedEntities())
            {
                CompanySource companySourceMaster = db.CompanySources.Find(genericLookupId);
                db.CompanySources.Remove(companySourceMaster);
                db.SaveChanges();
            }
        }
        #endregion

    }
}
