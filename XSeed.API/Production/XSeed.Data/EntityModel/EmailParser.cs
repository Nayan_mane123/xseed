﻿namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using XSeed.Data.ViewModel.Common;
    using XSeed.Data.ViewModel.Job;
    using XSeed.Data.ViewModel.Parser;
    using XSeed.Utility;

    [MetadataType(typeof(EmailParserMD))]
    public partial class EmailParser
    {
        #region Property Declaration

        public class EmailParserMD
        {
            public System.Guid Id { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public string Type { get; set; }
            public string State { get; set; }
            public string City { get; set; }
            public string SkillList { get; set; }
            public string CompanyContact { get; set; }
            public string Html { get; set; }
            public Nullable<bool> IsRejected { get; set; }
            public Nullable<bool> IsAccepted { get; set; }
            public string EmailUniqueId { get; set; }
            public string UserEmailAddress { get; set; }
            public Nullable<System.Guid> AcceptedBy { get; set; }
            public Nullable<System.Guid> JobId { get; set; }
            public string RequisitionId { get; set; }
        }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get list of Parsed job
        /// </summary>        
        /// <returns>Parsed Job list</returns>
        internal static List<EmailParser> GetParsedJob(Guid OrganizationUserId)
        {
            using (var db = new XSeedEntities())
            {
                string email = db.OrganizationUserDetails.FirstOrDefault(u => u.UserId == OrganizationUserId).PrimaryEmail;
                return db.EmailParsers.Where(p => p.UserEmailAddress == email && p.IsRejected == false && p.IsAccepted == false).ToList();
            }
        }

        /// <summary>
        /// Get list of Parsed jobs using server side pagination
        /// </summary>
        /// <param name="organizationUserId"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <returns>Parsed job List</returns>
        internal static List<ParserModel> ListAllParsedJobs(Guid organizationUserId, int pageSize, int pageNumber)
        {
            List<ParserModel> list = new List<ParserModel>();

            using (var db = new XSeedEntities())
            {
                string email = db.OrganizationUserDetails.FirstOrDefault(u => u.UserId == organizationUserId).PrimaryEmail;

                // var totalCount = db.EmailParsers.Where(j => j.UserEmailAddress == email && j.IsRejected == false && j.IsAccepted == false).Count();
                var totalCount = db.EmailParsers.Where(j => j.IsRejected == false && j.IsAccepted == false).Count();
                var totalPages = Math.Ceiling((double)totalCount / pageSize);

                //var parsedJobs = db.EmailParsers.Where(p => p.UserEmailAddress == email && p.IsRejected == false && p.IsAccepted == false)
                //                     .OrderByDescending(x => x.Date)
                //                     .Skip((pageNumber - 1) * pageSize)
                //                     .Take(pageSize).ToList();

                var parsedJobs = db.EmailParsers.Where(p => p.IsRejected == false && p.IsAccepted == false)
                                     .OrderByDescending(x => x.Date)
                                     .Skip((pageNumber - 1) * pageSize)
                                     .Take(pageSize).ToList();


                foreach (var parsedJob in parsedJobs)
                {
                    ParserModel model = populateParserModel(parsedJob);
                    list.Add(model);
                }

            }
            return list;
        }

        private static ParserModel populateParserModel(EmailParser parsedJob)
        {
            CompanyDetail company = null;
            ParserModel model = new ParserModel();

            using (var db = new XSeedEntities())
            {
                /* Company Contact Info */
                CompanyContact companyContact = db.CompanyContacts.FirstOrDefault(c => c.PrimaryEmail == parsedJob.CompanyContact);
                company = companyContact != null ? companyContact.CompanyDetail : null;
            }

            model.Id = parsedJob.Id;
            model.Title = parsedJob.Title;
            model.Description = parsedJob.Description;
            model.Type = parsedJob.Type;
            model.State = parsedJob.State;
            model.City = parsedJob.City;
            model.SkillList = parsedJob.SkillList;
            model.Company = company != null ? company.Name : "";
            model.CompanyContact = parsedJob.CompanyContact;
            model.Html = parsedJob.Html;
            model.IsRejected = parsedJob.IsRejected;
            model.IsAccepted = parsedJob.IsAccepted;
            model.EmailUniqueId = parsedJob.EmailUniqueId;
            model.UserEmailAddress = parsedJob.UserEmailAddress;
            model.AcceptedBy = parsedJob.AcceptedBy;
            model.JobId = parsedJob.JobId;
            model.EmailDate = parsedJob.Date;
            model.Subject = parsedJob.Subject;
            model.RequisitionId = parsedJob.RequisitionId;

            return model;
        }


        /// <summary>
        /// Get list of accepted parsed job
        /// </summary>        
        /// <returns>Accepted parsed job list</returns>
        internal static List<ParserModel> GetAcceptedParsedJob(Guid OrganizationUserId)
        {
            List<ParserModel> list = new List<ParserModel>();
            using (var db = new XSeedEntities())
            {
                string email = db.OrganizationUserDetails.FirstOrDefault(u => u.UserId == OrganizationUserId).PrimaryEmail;
                var parsedJobs = db.EmailParsers.Where(p => p.UserEmailAddress == email && p.IsAccepted == true).ToList();

                foreach (var parsedJob in parsedJobs)
                {
                    ParserModel model = populateParserModel(parsedJob);
                    list.Add(model);
                }
            }

            return list;
        }

        /// <summary>
        /// Accept Parsed Job and add to Job Detail
        /// </summary>
        /// <param name="Id">Id</param>
        /// <param name="OrganizationUserId">Organization User Id</param>
        /// <param name="isReviewed">IsReviewed flag</param>
        internal static void AcceptParsedJob(Guid Id, Guid JobId, Guid OrganizationUserId, bool isReviewed)
        {
            using (var db = new XSeedEntities())
            {
                EmailParser emailParser = db.EmailParsers.Find(Id);

                emailParser.JobId = JobId; // PostJobDetail(emailParser, OrganizationUserId);
                emailParser.AcceptedBy = BaseModel.InitiatedBy;
                emailParser.IsAccepted = true; // set flag isAccepted.

                db.SaveChanges();
            }
        }

        /// <summary>
        /// Post parsed job to db. 
        /// </summary>
        /// <param name="emailParser">Email Parser</param>
        /// <param name="organizationUserId">Organization UserId</param>
        /// <returns>Job Id</returns>
        private static Nullable<Guid> PostJobDetail(EmailParser emailParser, Guid organizationUserId)
        {
            Nullable<Guid> jobId = null;

            using (var db = new XSeedEntities())
            {
                JobDetail model = new JobDetail();

                model.Id = emailParser.Id;

                if (!string.IsNullOrEmpty(emailParser.CompanyContact) && !string.IsNullOrEmpty(emailParser.Title))
                {
                    OrganizationUserDetail organizationUserDetail = db.OrganizationUserDetails.FirstOrDefault(u => u.UserId == organizationUserId);

                    /* Company Contact Info */
                    CompanyContact companyContact = db.CompanyContacts.FirstOrDefault(c => c.PrimaryEmail == emailParser.CompanyContact && c.CompanyDetail.OrganizationId == organizationUserDetail.OrganizationId);

                    CompanyDetail company = companyContact != null ? companyContact.CompanyDetail : null;

                    if (company != null)
                    {
                        /* Company Info */
                        model.CompanyId = (Guid)(company.Id);

                        model.JobTitle = emailParser.Title;

                        model.CompanyContactId = companyContact != null ? companyContact.Id : Guid.Empty;

                        model.JobTypeId = !string.IsNullOrEmpty(emailParser.Type) ? db.JobTypeMasters.FirstOrDefault(t => t.Type == emailParser.Type).Id : db.JobTypeMasters.FirstOrDefault(t => t.Type == "Permanent/Full time").Id;
                        model.JobStatusId = db.JobStatusMasters.FirstOrDefault(s => s.Status == "Open").Id;
                        model.JobDescription = emailParser.Description;

                        model.CountryId = db.CountryMasters.Any(c => c.Name == emailParser.Country) ? db.CountryMasters.FirstOrDefault(c => c.Name == emailParser.Country).Id : Guid.Empty;
                        model.StateId = db.StateMasters.Any(c => c.Name == emailParser.State) ? db.StateMasters.FirstOrDefault(c => c.Name == emailParser.State).Id : Guid.Empty;
                        model.CityId = db.CityMasters.Any(c => c.Name == emailParser.City) ? db.CityMasters.FirstOrDefault(c => c.Name == emailParser.City).Id : Guid.Empty;

                        model.JobPostedDate = emailParser.Date;
                        model.TechnicalSkills = emailParser.SkillList;
                        model.ClientJobCode = emailParser.RequisitionId;
                        model.IsActive = Constants.IsActiveTrue;

                        db.JobDetails.Add(model);

                        db.SaveChanges();

                        jobId = model.Id;

                        /* Add Organization User Mapping */
                        if (organizationUserId != null)
                        {
                            List<LookUpModel> orgUsers = new List<LookUpModel>();
                            orgUsers.Add(new LookUpModel() { Id = organizationUserId });
                            OrganizationUserDetail.SaveJobUserMapping(model.Id, orgUsers);
                        }
                    }
                }
            }

            return jobId;
        }

        /// <summary>
        /// Mark Parsed Job as rejected
        /// </summary>
        /// <param name="Id">Id</param>
        internal static void RejectParsedJob(Guid Id)
        {
            using (var db = new XSeedEntities())
            {
                var emailParser = db.EmailParsers.Find(Id);
                emailParser.IsRejected = true;

                db.SaveChanges();
            }
        }

        /// <summary>
        /// Get Parsed Job Detail information for review.
        /// </summary>
        /// <param name="Id">Id</param>
        /// <param name="organizationUserId">Organization User Id</param>
        /// <returns>Job Detail Model</returns>
        internal static JobDetailModel ReviewParsedJob(Guid Id, Guid organizationUserId)
        {
            JobDetailModel model = new JobDetailModel();

            using (var db = new XSeedEntities())
            {
                var emailParser = db.EmailParsers.Find(Id);

                if (emailParser != null)
                {
                    OrganizationUserDetail organizationUserDetail = db.OrganizationUserDetails.FirstOrDefault(u => u.UserId == organizationUserId);

                    /* Company Contact Info */
                    CompanyContact companyContact = !string.IsNullOrEmpty(emailParser.CompanyContact) ? db.CompanyContacts.FirstOrDefault(c => c.PrimaryEmail == emailParser.CompanyContact && c.CompanyDetail.OrganizationId == organizationUserDetail.OrganizationId) : null;

                    model.CompanyContactId = companyContact != null ? companyContact.Id : Guid.Empty;
                    model.CompanyContactName = companyContact != null ? (companyContact.FirstName + " " + companyContact.LastName) : string.Empty;

                    CompanyDetail company = companyContact != null ? companyContact.CompanyDetail : null;

                    if (company != null)
                    {
                        /* Company Info */
                        model.CompanyId = (Guid)(company.Id);
                        model.CompanyName = company.Name;
                    }

                    /* Job Title Info */

                    model.JobTitle = emailParser.Title;

                    JobTypeMaster jobType = db.JobTypeMasters.FirstOrDefault(t => t.Type == emailParser.Type);
                    model.JobTypeId = jobType != null ? jobType.Id : Guid.Empty;
                    model.JobType = jobType != null ? jobType.Type : string.Empty;

                    JobStatusMaster jobStatus = db.JobStatusMasters.FirstOrDefault(s => s.Status == "Open");
                    model.JobStatusId = jobStatus.Id;
                    model.JobStatus = jobStatus.Status;

                    model.JobDescription = emailParser.Description;
                    model.JobPostedDate = emailParser.Date;
                    model.ClientJobCode = emailParser.RequisitionId;

                    /* Location Info */
                    CountryMaster countryMaster = db.CountryMasters.FirstOrDefault(c => c.Name == emailParser.Country);
                    if (countryMaster != null)
                    {
                        model.CountryId = countryMaster.Id;
                        model.Country = countryMaster.Name;
                    }

                    StateMaster stateMaster = db.StateMasters.FirstOrDefault(c => c.Name == emailParser.State);
                    if (stateMaster != null)
                    {
                        model.StateId = stateMaster.Id;
                        model.State = stateMaster.Name;
                    }

                    CityMaster cityMaster = db.CityMasters.FirstOrDefault(c => c.Name == emailParser.City);
                    if (cityMaster != null)
                    {
                        model.CityId = cityMaster.Id;
                        model.City = cityMaster.Name;
                    }

                    /* Skill Info */
                    model.TechnicalSkills = emailParser.SkillList;

                    if (organizationUserId != null && organizationUserId != Guid.Empty)
                    {
                        string userName = OrganizationUserDetail.GetCreatedByUserName(organizationUserId);
                        Guid userId = db.OrganizationUserDetails.FirstOrDefault(u => u.UserId == organizationUserId).Id; // get organization user Id.

                        /* Organization User Mapping */
                        model.OrganizationUser = new List<LookUpModel>();
                        model.OrganizationUser.Add(new LookUpModel() { Id = userId, Name = userName });
                    }

                    model.HtmlText = emailParser.Html;

                    /* Degree List */
                    model.DegreeList = new List<LookUpModel>();


                }
            }

            return model;
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>
        /// <param name="organizationId">organization user Id</param>
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        internal static PaginationModel GetPaginationInfo(Guid organizationUserId, int pageSize)
        {
            PaginationModel model = new PaginationModel();

            using (var db = new XSeedEntities())
            {
                string email = db.OrganizationUserDetails.FirstOrDefault(u => u.UserId == organizationUserId).PrimaryEmail;
                if (email != null)
                {
                    model.TotalCount = db.EmailParsers.Where(j => j.IsRejected == false && j.IsAccepted == false).Count();
                    model.TotalPages = Math.Ceiling((double)model.TotalCount / pageSize);
                }
            }

            return model;
        }

        /// <summary>
        /// Get Parsed Job List Count
        /// </summary>
        /// <param name="OrganizationUserId">Organization UserId</param>
        /// <returns>Parsed Job List Count</returns>
        internal static int GetParsedJobListCount(Guid OrganizationUserId)
        {
            int ParsedResumeCount = 0;
            using (var db = new XSeedEntities())
            {
                string email = db.OrganizationUserDetails.FirstOrDefault(u => u.UserId == OrganizationUserId).PrimaryEmail;
                if (email != null)
                {
                    ParsedResumeCount = db.EmailParsers.Where(j => j.IsRejected == false && j.IsAccepted == false).Count();
                }
            }
            return ParsedResumeCount;
        }

        #endregion

    }
}
