﻿namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using XSeed.Data.Entity;
    using XSeed.Data.ViewModel.ResumeBlaster;

    [MetadataType(typeof(BlasterMD))]
    public partial class Blaster : IAuditable
    {
        #region Property Declaration
        public class BlasterMD
        {

            public int Id { get; set; }
            public string Company { get; set; }
            public string RecruiterName { get; set; }
            public string PrimaryEmail { get; set; }
            public string Mobile { get; set; }
            public string Phone { get; set; }
            public string Address { get; set; }
            public string Website { get; set; }
            public Nullable<bool> IsMailSent { get; set; }

        }
        #endregion


        internal static List<ResumeBlasterDetailModel> ListResumeBlasters()
        {
            List<ResumeBlasterDetailModel> list = new List<ResumeBlasterDetailModel>();
            using (var db = new XSeedEntities())
            {
                var resumeBlasters = db.Blasters.ToList().OrderByDescending(d => d.CreatedOn);

                foreach (var recruiter in resumeBlasters)
                {
                    ResumeBlasterDetailModel model = PopulateResumeBlasterDetailModel(recruiter);
                    list.Add(model);
                }
            }
            return list;
        }

        internal static ResumeBlasterDetailModel GetResumeBlasterDetail(Guid? Id)
        {
            ResumeBlasterDetailModel model = new ResumeBlasterDetailModel();

            using (var db = new XSeedEntities())
            {
                var recruiter = db.Blasters.Find(Id);

                if (recruiter != null)
                {
                    model = PopulateResumeBlasterDetailModel(recruiter);
                }
            }

            return model;
        }


        private static ResumeBlasterDetailModel PopulateResumeBlasterDetailModel(Blaster recruiter)
        {
            ResumeBlasterDetailModel model = new ResumeBlasterDetailModel();

            /* References */
            model.Id = recruiter.Id;

            /* Personal Detail */

            model.Company = recruiter.Company;
            model.RecruiterName = recruiter.RecruiterName;


            /* Contact Detail */
            model.PrimaryEmail = recruiter.PrimaryEmail;

            model.Mobile = recruiter.Mobile;
            model.Phone = recruiter.Phone;


            /* Address Detail */
            model.Address = recruiter.Address;


            /* Other Detail */

            model.Website = recruiter.Website;
            model.IsMailSent = recruiter.IsMailSent;

            return model;
        }

        internal static Guid CreateResumeBlasterDetail(ResumeBlasterDetailModel resumeBlasterDetailModel)
        {
            return SaveResumeBlasterDetail(resumeBlasterDetailModel);
        }

        internal static object UpdateResumeBlasterDetail(ResumeBlasterDetailModel resumeBlasterDetailModel)
        {
            return SaveResumeBlasterDetail(resumeBlasterDetailModel);
        }
        private static Guid SaveResumeBlasterDetail(ResumeBlasterDetailModel resumeBlasterDetailModel)
        {
            Guid recruiterId = Guid.Empty;

            using (var db = new XSeedEntities())
            {
                Blaster model = resumeBlasterDetailModel.Id != Guid.Empty ? db.Blasters.Find(resumeBlasterDetailModel.Id) : new Blaster();

                /* References */
                model.Id = resumeBlasterDetailModel.Id;

                /* Personal Detail */
                model.Company = resumeBlasterDetailModel.Company;
                model.RecruiterName = resumeBlasterDetailModel.RecruiterName;

                /* Contact Detail */
                model.PrimaryEmail = resumeBlasterDetailModel.PrimaryEmail;
                model.Mobile = resumeBlasterDetailModel.Mobile;
                model.Phone = resumeBlasterDetailModel.Phone;

                /* Address Detail */
                model.Address = resumeBlasterDetailModel.Address;

                /* Other Detail */
                model.Website = resumeBlasterDetailModel.Website;
                model.IsMailSent = resumeBlasterDetailModel.IsMailSent;

                if (resumeBlasterDetailModel.Id == Guid.Empty)
                {
                    db.Blasters.Add(model);
                }

                db.SaveChanges();

                recruiterId = model.Id;
            }

            return recruiterId;
        }


    }
}
