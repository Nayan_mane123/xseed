﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Vendors;
using XSeed.Utility;

namespace XSeed.Data.Entity
{
    [MetadataType(typeof(VendorDetailMD))]
    public partial class Vendor : IAuditable
    {
        #region property declaration
        public class VendorDetailMD
        {
            public Guid Id { get; set; }
            public Guid CompanyId { get; set; }
            public bool? IsActive { get; set; }
            public string Name { get; set; }
            public string Details { get; set; }
            public string SourceEmailId { get; set; }
            public string DestinationEmailId { get; set; }
        }
        #endregion

        #region User Defined Functions
        /// <summary>
        /// Get List of All Vendors
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <returns>List of All Vendors</returns>
        internal static List<VendorDetailModel> ListAllVendors(Guid organizationId)
        {
            List<VendorDetailModel> listVendorDetail = new List<VendorDetailModel>();

            using (var db = new XSeedEntities())
            {

                var vendorsList = db.Vendors.Where(x => x.CompanyDetail.OrganizationId == organizationId).ToList().OrderByDescending(d => d.CreatedOn);

                foreach (var vendor in vendorsList)
                {
                    VendorDetailModel model = new VendorDetailModel();
                    model.Id = vendor.Id;
                    model.Name = vendor.Name;
                    model.CompanyId = vendor.CompanyId;
                    model.CompanyName = vendor.CompanyDetail.Name;
                    model.Details = vendor.Details;
                    model.SourceEmailId = vendor.SourceEmailId;
                    model.DestinationEmailId = vendor.DestinationEmailId;
                    model.IsActive = vendor.IsActive;

                    model.CreatedBy = OrganizationUserDetail.GetCreatedByUserName(vendor.CreatedBy);
                    model.CreatedOn = vendor.CreatedOn;
                    model.ModifiedBy = OrganizationUserDetail.GetCreatedByUserName(vendor.ModifiedBy);
                    model.ModifiedOn = vendor.ModifiedOn;

                    listVendorDetail.Add(model);
                }

            }
            return listVendorDetail;
        }


        /// <summary>
        /// Get Job Details
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns>JobDetailModel</returns>
        internal static VendorDetailModel GetVendorDetail(Guid? organizationId, Guid? vendorId)
        {
            VendorDetailModel model = new VendorDetailModel();

            using (var db = new XSeedEntities())
            {
                Vendor vendor = db.Vendors.Find(vendorId);

                if (vendor != null)
                {
                    model.Id = vendor.Id;
                    model.Name = vendor.Name;
                    model.CompanyId = vendor.CompanyId;
                    model.CompanyName = vendor.CompanyDetail.Name;
                    model.Details = vendor.Details;
                    model.SourceEmailId = vendor.SourceEmailId;
                    model.DestinationEmailId = vendor.DestinationEmailId;
                    model.IsActive = vendor.IsActive;

                    model.CreatedBy = OrganizationUserDetail.GetCreatedByUserName(vendor.CreatedBy);
                    model.CreatedOn = vendor.CreatedOn;
                    model.ModifiedBy = OrganizationUserDetail.GetCreatedByUserName(vendor.ModifiedBy);
                    model.ModifiedOn = vendor.ModifiedOn;
                }
            }

            return model;
        }

        /// <summary>
        /// Save Vendor Detail
        /// </summary>
        /// <param name="vendorDetailModel">Vendor Detail Model</param>
        /// <returns>VendorId</returns>
        internal static Guid CreateVendor(VendorDetailModel vendorDetailModel)
        {
            return SaveVendorInfo(vendorDetailModel);
        }

        /// <summary>
        /// Update Vendor Details
        /// </summary>
        /// <param name="vendorDetailModel">Vendor Detail Model</param>
        /// <returns></returns>
        internal static void UpdateVendor(VendorDetailModel vendorDetailModel)
        {
            SaveVendorInfo(vendorDetailModel);
        }

        /// <summary>
        /// Save / Update Vendor Detail
        /// </summary>
        /// <param name="vendorDetailModel">Vendor Detail Model</param>
        /// <returns>VendorId</returns>
        private static Guid SaveVendorInfo(VendorDetailModel model)
        {
            Guid vendorId = Guid.Empty;

            using (var db = new XSeedEntities())
            {
                Vendor vendor = model.Id != Guid.Empty ? db.Vendors.Find(model.Id) : new Vendor();

                /* Reference */
                vendor.Id = model.Id;
                vendor.CompanyId = (Guid)(model.CompanyId);

                /* Personal Info */
                vendor.Name = model.Name;
                vendor.SourceEmailId = model.SourceEmailId;
                vendor.DestinationEmailId = model.DestinationEmailId;
                vendor.Details = model.Details;

                /* Is Active Flag */
                vendor.IsActive = model.IsActive == null ? Constants.IsActiveTrue : model.IsActive;

                if (model.Id == Guid.Empty)
                {
                    db.Vendors.Add(vendor);
                }

                db.SaveChanges();

                vendorId = vendor.Id;
            }

            return vendorId;
        }

        #endregion
    }
}
