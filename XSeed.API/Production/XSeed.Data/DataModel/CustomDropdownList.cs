﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XSeed.Data.ViewModel
{
    public class CustomDropdownList
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class CustomDropdownListState : CustomDropdownList
    {
        public int CountryId { get; set; }
    }
    public class CustomDropdownListCity : CustomDropdownList
    {
        public int StateId { get; set; }
    }
}
