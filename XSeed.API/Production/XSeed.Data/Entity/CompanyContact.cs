//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class CompanyContact
    {
        public CompanyContact()
        {
            this.JobDetails = new HashSet<JobDetail>();
            this.OrganizationUserDetails = new HashSet<OrganizationUserDetail>();
        }
    
        public System.Guid Id { get; set; }
        public System.Guid CompanyId { get; set; }
        public Nullable<System.Guid> TitleId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public Nullable<System.DateTime> BirthDate { get; set; }
        public Nullable<System.DateTime> AnniversaryDate { get; set; }
        public string ProfileImagePath { get; set; }
        public string ReportingTo { get; set; }
        public string Designation { get; set; }
        public string PracticeLine { get; set; }
        public string PrimaryEmail { get; set; }
        public string SecondaryEmail { get; set; }
        public string Mobile { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public Nullable<System.Guid> CountryId { get; set; }
        public Nullable<System.Guid> StateId { get; set; }
        public Nullable<System.Guid> CityId { get; set; }
        public string Zip { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.Guid> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.Guid> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    
        public virtual CityMaster CityMaster { get; set; }
        public virtual CountryMaster CountryMaster { get; set; }
        public virtual StateMaster StateMaster { get; set; }
        public virtual TitleMaster TitleMaster { get; set; }
        public virtual CompanyDetail CompanyDetail { get; set; }
        public virtual ICollection<JobDetail> JobDetails { get; set; }
        public virtual ICollection<OrganizationUserDetail> OrganizationUserDetails { get; set; }
    }
}
