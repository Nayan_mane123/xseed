//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace XSeed.Data.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class CandidateFeedback
    {
        public System.Guid Id { get; set; }
        public Nullable<System.Guid> SubmissionId { get; set; }
        public Nullable<System.Guid> StatusId { get; set; }
        public string Remark { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<System.Guid> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
    
        public virtual CandidateFeedbackMaster CandidateFeedbackMaster { get; set; }
        public virtual SubmissionDetail SubmissionDetail { get; set; }
    }
}
