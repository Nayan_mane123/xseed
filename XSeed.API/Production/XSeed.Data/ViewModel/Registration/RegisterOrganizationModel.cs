﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.User.OrganizationUser;
using XSeed.Data.ViewModel.User.UserCommon;
using XSeed.Utility;

namespace XSeed.Data.ViewModel.Registration
{
    public class RegisterOrganizationModel : BaseModel
    {
        #region Property Declaration

        /// <summary>
        /// Primary Key of Login User
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// User Registration Type 
        /// </summary>
        public Guid UserType { get; set; }

        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "EmailRequired")]
        [RegularExpression(Constants.EmailExpression, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "EmailInvalid")]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "PasswordRequired")]
        [StringLength(100, MinimumLength = 6, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "PasswordLength")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "PasswordCompare")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Organization), ErrorMessageResourceName = "OrgnizationNameRequired")]
        [Display(Name = "Organization name")]
        public string OrganizationName { get; set; }

        [Display(Name = "Website")]
        public string Website { get; set; }

        [Display(Name = "Logo")]
        public string ProfileImage { get; set; }

        [Display(Name = "Country")]
        public Nullable<int> CountryId { get; set; }

        [Display(Name = "Address Line 1")]
        public string Address1 { get; set; }

        [Display(Name = "Address Line 2")]
        public string Address2 { get; set; }

        [Display(Name = "Address Line 3")]
        public string Address3 { get; set; }

        [Display(Name = "Contact Number")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Linked In URL")]
        public string LinkedInURL { get; set; }

        [Display(Name = "Twitter URL")]
        public string TwitterURL { get; set; }

        [Display(Name = "Google Plus URL")]
        public string GooglePlusURL { get; set; }

        [Display(Name = "Facebook URL")]
        public string FacebookURL { get; set; }
        public List<ChildLookUpModel> IndustryTypeList { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// function to Save Registration Info
        /// </summary>
        public void SaveRegistrationInfo(RegisterOrganizationModel registerOrgnization, List<UserRoleModel> userRoleModelList)
        {
            Guid organizationId = Guid.Empty, loginUserId = Guid.Empty;

            /* Save Login Info */
            loginUserId = UserLogin.SaveUserLoginInfo(registerOrgnization);

            /* Save Organization Info */
            organizationId = OrganizationDetail.CreateOrganizationDetail(registerOrgnization);

            /* Create Default Roles for an organization */
            Guid adminRoleId = Guid.Empty;
            foreach (var userRoleModel in userRoleModelList)
            {
                userRoleModel.OrganizationId = organizationId;
                Guid roleId = UserRoleModel.CreateUserRole(userRoleModel);

                if (userRoleModelList.First() == userRoleModel)
                {
                    adminRoleId = roleId;
                }
            }

            /* Populate Organization User Detail */
            OrganizationUserModel organizationUserModel = new OrganizationUserModel();
            organizationUserModel.UserId = loginUserId;
            organizationUserModel.RoleId = adminRoleId;
            organizationUserModel.OrganizationId = organizationId;
            organizationUserModel.PrimaryEmail = registerOrgnization.UserName;
            organizationUserModel.FirstName = registerOrgnization.FirstName;
            organizationUserModel.LastName = registerOrgnization.LastName;

            /* Save Organization User Detail Info */
            OrganizationUserDetail.CreateOrganizationUser(organizationUserModel);
        }

        /// <summary>
        /// Check if organization is already registered
        /// </summary>
        /// <param name="userName">User Name</param>
        /// <returns>Is exists flag</returns>
        public static bool IsOrganizationDomainExists(string userName)
        {
            string domain = string.Empty;

            if (userName != null)
            {
                /* get domain from email */
                domain = userName.Split('@').Last();

                if (!string.IsNullOrEmpty(domain))
                {
                    /* get public domains from config */
                    string[] publicDomains = ConfigurationManager.AppSettings["PublicDomains"].Split(',');

                    if (!publicDomains.Contains(domain))
                    {
                        using (var db = new XSeedEntities())
                        {
                            /* get registered domains from db */
                            var registeredDomains = db.OrganizationUserDetails.Select(e => e.PrimaryEmail).ToList();

                            if (registeredDomains.Any(e => e.Split('@').Last().ToLower() == domain.ToLower()))
                            {
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        public bool IsOrganizationAlreadyExist(RegisterOrganizationModel registerOrgnization)
        {
            return OrganizationDetail.IsOrganizationAlreadyExist(registerOrgnization);
        }

        #endregion
    }
}
