﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Candidate;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.User.CandidateUser;
using XSeed.Utility;

namespace XSeed.Data.ViewModel.Registration
{
    public class RegisterCandidateModel : BaseModel
    {
        #region Property Declaration


        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }

        [Display(Name = "Candidate User UniqueId")]
        public string UniqueId { get; set; }

        [Display(Name = "Candidate User Name")]
        public string CandidateUserName { get; set; }

        [Display(Name = "Provider")]
        public string Provider { get; set; }

        [Display(Name = "Providers")]
        public List<string> Providers { get; set; }

        [Required]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [StringLength(100, MinimumLength = 6, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "PasswordLength")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "PasswordCompare")]
        public string ConfirmPassword { get; set; }

        [System.ComponentModel.DefaultValue(""), RegularExpression(Constants.MobileExpression, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "MobileInvalid")]
        [Required]
        public string Mobile { get; set; }

        [Display(Name = "Security Token")]
        [System.ComponentModel.DefaultValue("")]
        public string SecurityToken { get; set; }

        public bool IsEmailConfirmed { get; set; }

        public bool IsRegistered { get; set; }

        /* Audit Trail Info */
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }

        #endregion

        #region User Defined Functions

        public void SaveRegistrationInfo(RegisterCandidateModel model)
        {
            var candidateUserId = UserLogin.SaveCandidateUserLoginInfo(model, true);

            /* Save Candidate Info */
            CandidateUserModel candidateUserModel = new CandidateUserModel();
            candidateUserModel.CandidateId = CandidateUserModel.GetCandidateId();
            /* Set Default Status */
            candidateUserModel.Status = "New";
            candidateUserModel.UserId = candidateUserId;
            candidateUserModel.FirstName = model.FirstName;
            candidateUserModel.LastName = model.LastName;
            candidateUserModel.PrimaryEmail = model.UserName.ToLower();
            candidateUserModel.Mobile = model.Mobile;

            candidateUserModel.CreateCandidateUserDetail(candidateUserModel, true);
        }

        #endregion

        public RegisterCandidateModel CandidateAlreadyExists(RegisterCandidateModel model)
        {
            CandidateUserModel candidateUserModel = new CandidateUserModel();
            return candidateUserModel.CandidateAlreadyExists(model);
        }

        //public bool SocialCandidateAlreadyExists(RegisterCandidateModel model)
        //{
        //    CandidateUserModel candidateUserModel = new CandidateUserModel();
        //    return candidateUserModel.SocialCandidateAlreadyExists(model);
        //}

        public bool HasExternalUserRegistered(string loginProvider, string providerKey)
        {
            CandidateUserModel candidateUserModel = new CandidateUserModel();
            return candidateUserModel.HasExternalUserRegistered(loginProvider, providerKey);
        }

        /// <summary>
        /// Candidate Login
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="password">password</param>
        /// <returns>User Detail</returns>
        public object CandidateLogin(string username, string password)
        {
            CandidateUserModel candidateUserModel = new CandidateUserModel();
            return candidateUserModel.CandidateLogin(username, password);
        }

        public bool IsAuthenticated(string userId, string token)
        {
            CandidateUserModel candidateUserModel = new CandidateUserModel();
            return candidateUserModel.IsAuthenticated(userId, token);
        }

        /// <summary>
        /// Verify Candidate Email
        /// </summary>
        /// <param name="userId">userId</param>
        /// <param name="code">code</param>
        /// <returns>Email Verification Status - true/false</returns>
        public bool VerifyCandidateEmail(string userId, string code)
        {
            CandidateUserModel candidateUserModel = new CandidateUserModel();
            return candidateUserModel.VerifyCandidateEmail(userId, code);
        }

        /// <summary>
        /// Update Registration Info
        /// </summary>
        /// <param name="model">RegisterCandidateModel</param>
        public void UpdateRegistrationInfo(RegisterCandidateModel model)
        {
            UserLogin.UpdateRegistrationInfo(model);
        }

        /// <summary>
        /// Resend Candidate Verification Email
        /// </summary>
        /// <param name="UserName">UserName</param>
        /// <returns>Status Ok - 200</returns>
        public void ResendCandidateVerificationEmail(string UserName)
        {
            CandidateUserModel candidateUserModel = new CandidateUserModel();
            candidateUserModel.ResendCandidateVerificationEmail(UserName);
        }

        /// <summary>
        /// Is Candidate Valid
        /// </summary>
        /// <param name="userId">userId</param>
        /// <param name="code">code</param>
        /// <returns>RegisterCandidateModel</returns>
        public RegisterCandidateModel IsCandidateValid(string userId, string code)
        {
            CandidateUserModel candidateUserModel = new CandidateUserModel();
            return candidateUserModel.IsCandidateValid(userId, code);
        }
    }

}
