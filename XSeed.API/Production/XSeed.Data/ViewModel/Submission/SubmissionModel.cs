﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Activity;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Job;
using XSeed.Data.ViewModel.User.CandidateUser;
using XSeed.MonsterJobPortalAPI;

namespace XSeed.Data.ViewModel.Submission
{
    public class SubmissionModel
    {
        #region Property Declaration

        public Guid Id { get; set; }
        public Nullable<Guid> CompanyId { get; set; }
        public string CompanyName { get; set; }
        public Guid JobId { get; set; }
        public string JobTitle { get; set; }
        public string JobType { get; set; }
        public string JobCode { get; set; }
        public Nullable<Guid> OrganizationUserId { get; set; }
        public string OrganizationUserName { get; set; }
        public Nullable<Guid> StatusId { get; set; }
        public string Status { get; set; }
        public Nullable<System.DateTime> AssignedDate { get; set; }
        public Nullable<decimal> ClientRate { get; set; }
        public Nullable<decimal> Margin { get; set; }

        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }

        public List<MongoLookUpModel> CandidateList { get; set; }
        public List<SubmissionFeedbackModel> SubmissionStatusList { get; set; }
        public List<ClientFeedbackModel> ClientFeedbackList { get; set; }
        public List<CandidateFeedbackModel> CandidateFeedbackList { get; set; }
        public List<string> ContactEmailList { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Submissions
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <returns>List of Submissions</returns>
        public List<SubmissionModel> GetSubmissions(Guid organizationId)
        {
            return SubmissionDetail.GetSubmissions(organizationId);
        }

        /// <summary>
        /// Get list of submission using server side pagination
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>        
        /// <returns>submission List</returns>
        public List<SubmissionModel> GetSubmissions(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", bool isExport = false)
        {
            return SubmissionDetail.GetSubmissions(organizationId, pageSize, pageNumber, sortBy, sortOrder, isExport);
        }

        /// <summary>
        /// Get paritcular Submission Detail
        /// </summary>
        /// <param name="Id">Submission Id</param>
        /// <returns>Submission Detail</returns>
        public SubmissionModel GetSubmissionDetail(Guid organizationId, Guid? Id)
        {
            return SubmissionDetail.GetSubmissionDetail(organizationId, Id);
        }

        /// <summary>
        /// Create New Submission
        /// </summary>
        /// <param name="SubmissionModel">Submission Detail Model </param>
        public void CreateSubmissionDetail(SubmissionModel submissionDetailModel, out List<string> successCandidateList, out string message)
        {
            SubmissionDetail.CreateSubmissionDetail(submissionDetailModel, out successCandidateList, out message);
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        public static PaginationModel GetPaginationInfo(Guid organizationId, int pageSize)
        {
            return SubmissionDetail.GetPaginationInfo(organizationId, pageSize);
        }

        /// <summary>
        /// Save Quick Note related to Job
        /// </summary>
        /// <param name="note">Note</param>
        /// <param name="submissionId">Submission Id</param>
        /// <returns>Notes</returns>
        public static List<Note> SaveQuickNote(string note, Guid submissionId)
        {
            return SubmissionActivityLogModel.SaveQuickNote(note, submissionId);
        }

        /// <summary>
        /// Get Quick Notes related to submission
        /// </summary>
        /// <param name="submissionId">Submission Id</param>
        /// <returns>Quick Notes</returns>
        public static List<Note> GetQuickNotes(Guid submissionId)
        {
            return SubmissionActivityLogModel.GetQuickNotes(submissionId);
        }

        /// <summary>
        /// Submit sourced candidates against selected Job.
        /// </summary>
        /// <param name="candidateSourceResultModel">Candidate Source Result Model</param>
        /// <returns>Candidates</returns>
        public static List<MongoLookUpModel> SubmitSourcedCandidates(List<CandidateSourceResultModel> candidateSourceResultModelList)
        {
            List<MongoLookUpModel> list = new List<MongoLookUpModel>();

            if (candidateSourceResultModelList != null)
            {
                /* Filter Local candidates */
                var localCandidates = candidateSourceResultModelList.Where(s => s != null && s.Source != null && (s.Source == "Local" || !string.IsNullOrEmpty(s.Candidate_Id)));

                if (localCandidates != null && localCandidates.Count() > 0)
                {
                    foreach (var localCandidate in localCandidates)
                    {
                        MongoLookUpModel model = new MongoLookUpModel();
                        model.Id = localCandidate.Candidate_Id;
                        model.Name = localCandidate.Name;

                        list.Add(model);
                    }
                }

                /* Filter Monster candidates */
                var monsterCandidates = candidateSourceResultModelList.Where(s => s != null && s.Source != null && s.Source == "Monster" && string.IsNullOrEmpty(s.Candidate_Id));

                if (monsterCandidates != null && monsterCandidates.Count() > 0)
                {
                    foreach (var monsterCandidate in monsterCandidates)
                    {
                        /* Initialize Monster API */
                        MonsterResumeAPI resumeAPI = new MonsterResumeAPI();

                        /* Download Monster Resume */
                        Stream monsterCandidateResumeXmlResponse = resumeAPI.GetCandidateResume(monsterCandidate.ProviderCandidateId);

                        if (monsterCandidateResumeXmlResponse != null)
                        {
                            /* Download Monster Candidate */
                            string candidateId = CandidateUserModel.DownloadMonsterCandidate(monsterCandidateResumeXmlResponse);

                            if (!string.IsNullOrEmpty(candidateId))
                            {
                                MongoLookUpModel model = new MongoLookUpModel();
                                model.Id = candidateId;
                                model.Name = monsterCandidate.Name;

                                list.Add(model);
                            }
                        }
                    }
                }

                /* Filter Dice candidates */
                var diceCandidates = candidateSourceResultModelList.Where(s => s != null && s.Source != null && s.Source == "Dice" && string.IsNullOrEmpty(s.Candidate_Id));

                if (diceCandidates != null && diceCandidates.Count() > 0)
                {
                    foreach (var diceCandidate in diceCandidates)
                    {
                        /* Download Dice Candidate */
                        string candidateId = CandidateUserModel.DownloadDiceCandidate(diceCandidate.ProviderCandidateId, diceCandidate.Candidate_Id);

                        if (!string.IsNullOrEmpty(candidateId))
                        {
                            MongoLookUpModel model = new MongoLookUpModel();
                            model.Id = candidateId;
                            model.Name = diceCandidate.Name;

                            list.Add(model);
                        }
                    }
                }
            }

            return list;
        }

        #endregion
    }

    public class SubmissionMailModel
    {
        public string FromEmailID { get; set; }
        public List<string> ToEmailID { get; set; }
        public List<string> ToCC { get; set; }
        public List<string> ToBCC { get; set; }
        public string MailSubject { get; set; }
        public string MailBody { get; set; }
        public string FromDisplayName { get; set; }
        public List<string> CandidateList { get; set; }
        public List<XSeedFileEntity> MailAttachment { get; set; }
    }
}