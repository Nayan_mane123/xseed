﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;

namespace XSeed.Data.ViewModel.Vendors
{
    public class VendorDetailModel
    {
        #region Property Declaration

        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public string SourceEmailId { get; set; }
        public string DestinationEmailId { get; set; }
        public bool? IsActive { get; set; }

        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get List of All Vendors
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <returns>List of All Vendors</returns>
        public static List<VendorDetailModel> ListAllVendors(Guid organizationId)
        {
            return (Vendor.ListAllVendors(organizationId));
        }

        /// <summary>
        /// Get Vendor Details
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns>VendorDetailModel</returns>
        public static VendorDetailModel GetVendorDetail(Guid organizationId, Guid? vendorId)
        {
            return (Vendor.GetVendorDetail(organizationId, vendorId));
        }

        /// <summary>
        /// Save Vendor Detail
        /// </summary>
        /// <param name="vendorDetailModel">Vendor Detail Model</param>
        /// <returns>VendorId</returns>
        public static Guid CreateVendor(VendorDetailModel vendorDetailModel)
        {
            return (Vendor.CreateVendor(vendorDetailModel));
        }

        /// <summary>
        /// Update Vendor Details
        /// </summary>
        /// <param name="vendorDetailModel">Vendor Detail Model</param>
        /// <returns></returns>
        public void UpdateVendor(VendorDetailModel vendorDetailModel)
        {
            Vendor.UpdateVendor(vendorDetailModel);
        }

        #endregion
    }
}
