﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.User.CandidateUser;

namespace XSeed.Data.ViewModel.AdvancedSearch
{
    public class CandidateFilterModel
    {
        #region Property Declaration

        public List<string> Status { get; set; }
        public List<string> JobTitle { get; set; }
        public List<string> Company { get; set; }
        public List<string> CreatedBy { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Apply Search Filter
        /// </summary>
        /// <param name="candidates">IQueryable</param>
        /// <param name="candidateFilterModel">Candidate Filter Model</param>
        /// <returns>IQueryable</returns>
        internal static IQueryable<CandidateUserModel> ApplySearchFilter(IQueryable<CandidateUserModel> candidates, CandidateFilterModel candidateFilterModel)
        {
            /* Apply Req status filter like "Open/ Closed/ Hold" */
            if (candidateFilterModel.Status != null && candidateFilterModel.Status.Count > 0)
            {
                candidates = candidates.Where(c => candidateFilterModel.Status.Contains(c.Status));
            }

            /* Apply Job Title/ Profile Title filter */
            if (candidateFilterModel.JobTitle != null && candidateFilterModel.JobTitle.Count > 0)
            {
                candidates = candidates.Where(c => candidateFilterModel.JobTitle.Contains(c.ProfileTitle));
            }

            /* Apply Company filter */
            if (candidateFilterModel.Company != null && candidateFilterModel.Company.Count > 0)
            {
                candidates = candidates.Where(c => candidateFilterModel.Company.Contains(c.CompanyName));
            }


            /* Apply Created By filter */
            if (candidateFilterModel.CreatedBy != null && candidateFilterModel.CreatedBy.Count > 0)
            {
                using (var db = new XSeedEntities())
                {
                    var createdByList = db.OrganizationUserDetails.Where(u => candidateFilterModel.CreatedBy.Contains(u.Id.ToString())).Select(u => u.UserId.ToString()).ToList();
                    candidates = candidates.Where(c => createdByList.Contains(c.CreatedBy));
                }
            }

            return candidates;
        }

        #endregion
    }
}
