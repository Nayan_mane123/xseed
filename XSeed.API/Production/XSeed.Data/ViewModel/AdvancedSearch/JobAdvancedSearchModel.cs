﻿using LinqKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Job;

namespace XSeed.Data.ViewModel.AdvancedSearch
{
    public class JobAdvancedSearchModel
    {
        #region Property Declaration

        public Guid OrganizationId { get; set; }
        public List<string> SearchIn { get; set; }
        public string SearchAll { get; set; }
        public string SearchAny { get; set; }
        public string SearchNone { get; set; }
        public string SearchExact { get; set; }
        public string SearchZip { get; set; }

        public JobFilterModel filter { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Job Advanced Search API
        /// </summary>
        /// <param name="jobAdvancedSearchModel">job Search Model</param>        
        /// <param name="pageSize">Page Size</param>
        /// <param name="pageNumber">Page Number</param>
        /// <param name="sortBy">Sort By</param>
        /// <param name="sortOrder">Sort Order</param>
        /// <returns>Job List</returns>
        public List<JobDetailModel> SearchJobs(JobAdvancedSearchModel jobAdvancedSearchModel, string sortBy, string sortOrder, int pageSize, int pageNumber, out int TotalCount, out double TotalPages, bool isExport = false)
        {
            List<JobDetailModel> list = AdvancedSearchJobs(jobAdvancedSearchModel, pageSize, pageNumber, sortBy, sortOrder, out TotalCount, out TotalPages, isExport);

            return list;
        }

        #region Advanced Search

        /// <summary>
        /// Advanced Search for Job Module Mulitple Keywords with multiple columns
        /// </summary>
        /// <param name="searchModel">Job Advanced Search Model</param>
        /// <param name="pageSize">Page Size</param>
        /// <param name="pageNumber">Page Number</param>
        /// <param name="TotalCount">Total Count</param>
        /// <param name="TotalPages">Total Pages</param>
        /// <returns>List<JobDetailModel></returns>
        private List<JobDetailModel> AdvancedSearchJobs(JobAdvancedSearchModel searchModel, int pageSize, int pageNumber, string sortBy, string sortOrder, out int TotalCount, out double TotalPages, bool isExport = false)
        {
            IQueryable<JobDetail> jobs = null;
            List<JobDetailModel> list = new List<JobDetailModel>();

            /* Build Predicate */
            var predicate = BuildPredicate(searchModel);

            /* Search with Predicate */
            using (var db = new XSeedEntities())
            {
                /* Apply Predicate */
                jobs = db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == searchModel.OrganizationId).AsExpandable().Where(predicate);

                /* Get All When not with AND/ OR/ EQUAL */
                if (string.IsNullOrEmpty(searchModel.SearchAny) && string.IsNullOrEmpty(searchModel.SearchAll) && string.IsNullOrEmpty(searchModel.SearchExact))
                {
                    jobs = db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == searchModel.OrganizationId);
                }

                /* Search with Not */
                if (!string.IsNullOrEmpty(searchModel.SearchNone))
                {
                    var keywords = searchModel.SearchNone.Split(' ').ToArray();
                    jobs = SearchWithNotOperator(searchModel.SearchIn, jobs, keywords);
                }

                /* Apply Filters */
                if (searchModel.filter != null)
                {
                    jobs = JobFilterModel.ApplySearchFilter(jobs, searchModel.filter);
                }

                /* Search with Zip Code */
                if (!string.IsNullOrEmpty(searchModel.SearchZip))
                {
                    var keywords = searchModel.SearchZip.Split(' ').ToArray();
                    jobs = SearchInZip(jobs, keywords);
                }

                /* Apply Pagination */
                jobs = jobs.OrderByDescending(d => d.CreatedOn);

                /* Assign Total Count & Pages */
                TotalCount = (int)jobs.ToList().Count();
                TotalPages = Math.Ceiling((double)TotalCount / pageSize);

                var jobList = isExport ? jobs.OrderBy(sortBy + " " + sortOrder) : jobs.OrderBy(sortBy + " " + sortOrder).Skip((pageNumber - 1) * pageSize).Take(pageSize);

                /* Populate model list */
                foreach (var job in jobList)
                {
                    JobDetailModel model = JobDetail.PopulateJobModel(OrganizationId, job.Id);
                    list.Add(model);
                }
            }

            return list;
        }

        /// <summary>
        /// Build predicate with AND/ OR/ Not Operators
        /// </summary>
        /// <param name="searchModel">Job Advanced Search Model</param>
        /// <returns>Predicate</returns>
        private ExpressionStarter<JobDetail> BuildPredicate(JobAdvancedSearchModel searchModel)
        {
            var predicate = PredicateBuilder.New<JobDetail>();

            /* Search with OR Predicate */
            if (!string.IsNullOrEmpty(searchModel.SearchAny))
            {
                var keywords = searchModel.SearchAny.Split(' ').ToArray();
                predicate.Extend(SearchWithOperator(searchModel.SearchIn, keywords.ToArray(), "OR"));
            }

            /* Search with AND Predicate */
            if (!string.IsNullOrEmpty(searchModel.SearchAll))
            {
                var keywords = searchModel.SearchAll.Split(' ').ToArray();
                predicate.Extend(SearchWithOperator(searchModel.SearchIn, keywords.ToArray(), "AND"));
            }

            /* Search with Exact Predicate */
            if (!string.IsNullOrEmpty(searchModel.SearchExact))
            {
                var keywords = searchModel.SearchExact.Split(' ').ToArray();
                predicate.Extend(SearchWithOperator(searchModel.SearchIn, keywords.ToArray(), "EQUAL"));
            }

            return predicate;
        }

        #region Search With 'AND', 'OR' and 'EQUAL' Operator

        /// <summary>
        /// Search with operator like AND/ OR/ Equal
        /// </summary>
        /// <param name="columns">Columns to Search In</param>
        /// <param name="keywords">Search Keyword</param>
        /// <param name="Operator">Operator</param>
        /// <returns>ExpressionStarter<JobDetail></returns>
        private ExpressionStarter<JobDetail> SearchWithOperator(List<string> columns, string[] keywords, string Operator)
        {
            var predicate = PredicateBuilder.New<JobDetail>();

            foreach (var column in columns)
            {
                switch (column)
                {
                    case "Title":
                        predicate.Extend(SearchInTitle(keywords, Operator));
                        break;
                    case "Status":
                        predicate.Extend(SearchInStatus(keywords, Operator));
                        break;
                    case "Type":
                        predicate.Extend(SearchInType(keywords, Operator));
                        break;
                    case "Source":
                        predicate.Extend(SearchInSource(keywords, Operator));
                        break;
                    case "Company":
                        predicate.Extend(SearchInCompany(keywords, Operator));
                        break;
                    case "RequisitionId":
                        predicate.Extend(SearchInRequisitionId(keywords, Operator));
                        break;
                    case "AssignedTo":
                        predicate.Extend(SearchInAssignedTo(keywords, Operator));
                        break;
                }
            }

            return predicate;
        }


        /// <summary>
        /// Search keyword in Title Column
        /// </summary>
        /// <param name="keywords">Search Keyword</param>
        /// <param name="Operator">Operator</param>
        /// <returns>ExpressionStarter<JobDetail></returns>
        private System.Linq.Expressions.Expression<Func<JobDetail, bool>> SearchInTitle(string[] keywords, string Operator)
        {
            var predicate = PredicateBuilder.New<JobDetail>();

            if (Operator == "AND")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.And(j => j.JobTitle.ToLower().Contains(word));
                }
            }
            else if (Operator == "OR")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.Or(j => j.JobTitle.ToLower().Contains(word));
                }
            }
            else if (Operator == "EQUAL")
            {
                string phrase = string.Join(" ", keywords);
                predicate = predicate.Or(j => j.JobTitle.ToLower() == phrase.ToLower());
            }

            return predicate;
        }

        /// <summary>
        /// Search keyword in Status Column
        /// </summary>
        /// <param name="keywords">Search Keyword</param>
        /// <param name="Operator">Operator</param>
        /// <returns>ExpressionStarter<JobDetail></returns>
        private ExpressionStarter<JobDetail> SearchInStatus(string[] keywords, string Operator)
        {
            var predicate = PredicateBuilder.New<JobDetail>();

            if (Operator == "AND")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.And(j => j.JobStatusMaster.Status.ToLower().Contains(word));
                }
            }
            else if (Operator == "OR")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.Or(j => j.JobStatusMaster.Status.ToLower().Contains(word));
                }
            }
            else if (Operator == "EQUAL")
            {
                string phrase = string.Join(" ", keywords);
                predicate = predicate.Or(j => j.JobStatusMaster.Status.ToLower() == phrase.ToLower());
            }

            return predicate;
        }

        /// <summary>
        /// Search keyword in Type Column
        /// </summary>
        /// <param name="keywords">Search Keyword</param>
        /// <param name="Operator">Operator</param>
        /// <returns>ExpressionStarter<JobDetail></returns>
        private System.Linq.Expressions.Expression<Func<JobDetail, bool>> SearchInType(string[] keywords, string Operator)
        {
            var predicate = PredicateBuilder.New<JobDetail>();

            if (Operator == "AND")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.And(j => j.JobTypeMaster.Type.ToLower().Contains(word));
                }
            }
            else if (Operator == "OR")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.Or(j => j.JobTypeMaster.Type.ToLower().Contains(word));
                }
            }
            else if (Operator == "EQUAL")
            {
                string phrase = string.Join(" ", keywords);
                predicate = predicate.Or(j => j.JobTypeMaster.Type.ToLower() == phrase.ToLower());
            }


            return predicate;
        }

        /// <summary>
        /// Search keyword in Source Column
        /// </summary>
        /// <param name="keywords">Search Keyword</param>
        /// <param name="Operator">Operator</param>
        /// <returns>ExpressionStarter<JobDetail></returns>
        private System.Linq.Expressions.Expression<Func<JobDetail, bool>> SearchInSource(string[] keywords, string Operator)
        {
            var predicate = PredicateBuilder.New<JobDetail>();

            if (Operator == "AND")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.And(j => j.CompanyDetail.CompanySource.Name.ToLower().Contains(word));
                }
            }
            else if (Operator == "OR")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.Or(j => j.CompanyDetail.CompanySource.Name.ToLower().Contains(word));
                }
            }
            else if (Operator == "EQUAL")
            {
                string phrase = string.Join(" ", keywords);
                predicate = predicate.Or(j => j.CompanyDetail.CompanySource.Name.ToLower() == phrase.ToLower());
            }

            return predicate;
        }

        /// <summary>
        /// Search keyword in Company Column
        /// </summary>
        /// <param name="keywords">Search Keyword</param>
        /// <param name="Operator">Operator</param>
        /// <returns>ExpressionStarter<JobDetail></returns>
        private System.Linq.Expressions.Expression<Func<JobDetail, bool>> SearchInCompany(string[] keywords, string Operator)
        {
            var predicate = PredicateBuilder.New<JobDetail>();

            if (Operator == "AND")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.And(j => j.CompanyDetail.Name.ToLower().Contains(word));
                }
            }
            else if (Operator == "OR")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.Or(j => j.CompanyDetail.Name.ToLower().Contains(word));
                }
            }
            else if (Operator == "EQUAL")
            {
                string phrase = string.Join(" ", keywords);
                predicate = predicate.Or(j => j.CompanyDetail.Name.ToLower() == phrase.ToLower());
            }

            return predicate;
        }

        /// <summary>
        /// Search keyword in Requisition Id Column
        /// </summary>
        /// <param name="keywords">Search Keyword</param>
        /// <param name="Operator">Operator</param>
        /// <returns>ExpressionStarter<JobDetail></returns>
        private System.Linq.Expressions.Expression<Func<JobDetail, bool>> SearchInRequisitionId(string[] keywords, string Operator)
        {
            var predicate = PredicateBuilder.New<JobDetail>();

            if (Operator == "AND")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.And(j => j.RequisitionId.ToLower().Contains(word));
                }
            }
            else if (Operator == "OR")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.Or(j => j.RequisitionId.ToLower().Contains(word));
                }
            }
            else if (Operator == "EQUAL")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.Or(j => j.RequisitionId.ToLower() == word);
                }
            }

            return predicate;
        }

        /// <summary>
        /// Search keyword in Assigned To Column
        /// </summary>
        /// <param name="keywords">Search Keyword</param>
        /// <param name="Operator">Operator</param>
        /// <returns>ExpressionStarter<JobDetail></returns>
        private System.Linq.Expressions.Expression<Func<JobDetail, bool>> SearchInAssignedTo(string[] keywords, string Operator)
        {
            var predicate = PredicateBuilder.New<JobDetail>();

            if (Operator == "AND")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.And(j => j.OrganizationUserDetails.Any(u => u.FirstName.ToLower().Contains(word) || u.MiddleName.ToLower().Contains(word) || u.LastName.ToLower().Contains(word)));
                }
            }
            else if (Operator == "OR")
            {
                foreach (string keyword in keywords)
                {
                    string word = keyword.ToLower();
                    predicate = predicate.Or(j => j.OrganizationUserDetails.Any(u => u.FirstName.ToLower().Contains(word) || u.MiddleName.ToLower().Contains(word) || u.LastName.ToLower().Contains(word)));
                }
            }
            else if (Operator == "EQUAL")
            {
                string phrase = string.Join(" ", keywords);
                predicate = predicate.Or(j => j.OrganizationUserDetails
                                    .Any(u => u.FirstName.ToLower() == phrase.ToLower()
                                    || u.MiddleName.ToLower() == phrase.ToLower()
                                    || u.LastName.ToLower() == phrase.ToLower()
                                    || (u.FirstName + " " + u.LastName).ToLower() == phrase.ToLower()
                                    || (u.FirstName + " " + u.MiddleName + " " + u.LastName).ToLower() == phrase.ToLower()));
            }

            return predicate;
        }

        #endregion

        #region Search With 'Not' Operator

        /// <summary>
        /// Search With Not Operator
        /// </summary>
        /// <param name="SearchIn">Columns to Search In</param>
        /// <param name="jobs">IQueryable<JobDetail></param>
        /// <param name="keywords">Search Keyword</param>
        /// <returns> IQueryable<JobDetail></returns>
        private IQueryable<JobDetail> SearchWithNotOperator(List<string> SearchIn, IQueryable<JobDetail> jobs, string[] keywords)
        {
            foreach (var column in SearchIn)
            {
                switch (column)
                {
                    case "Title":
                        jobs = SearchNotWithTitle(jobs, keywords);
                        break;
                    case "Status":
                        jobs = SearchNotWithStatus(jobs, keywords);
                        break;
                    case "Type":
                        jobs = SearchNotWithType(jobs, keywords);
                        break;
                    case "Source":
                        jobs = SearchNotWithSource(jobs, keywords);
                        break;
                    case "Company":
                        jobs = SearchNotWithCompany(jobs, keywords);
                        break;
                    case "RequisitionId":
                        jobs = SearchNotWithRequisitionId(jobs, keywords);
                        break;
                    case "AssignedTo":
                        jobs = SearchNotWithAssignedTo(jobs, keywords);
                        break;
                }
            }

            return jobs;
        }

        /// <summary>
        /// Search With Not Operator in Title column
        /// </summary>
        /// <param name="jobs">IQueryable<JobDetail></param>
        /// <param name="keywords">Search Keyword</param>
        /// <returns>IQueryable<JobDetail></returns>
        private IQueryable<JobDetail> SearchNotWithTitle(IQueryable<JobDetail> jobs, string[] keywords)
        {
            foreach (string keyword in keywords)
            {
                string word = keyword.ToLower();
                jobs = jobs.Where(j => !j.JobTitle.ToLower().Contains(word));
            }
            return jobs;
        }

        /// <summary>
        /// Search With Not Operator in Status column
        /// </summary>
        /// <param name="jobs">IQueryable<JobDetail></param>
        /// <param name="keywords">Search Keyword</param>
        /// <returns>IQueryable<JobDetail></returns>
        private IQueryable<JobDetail> SearchNotWithStatus(IQueryable<JobDetail> jobs, string[] keywords)
        {
            foreach (string keyword in keywords)
            {
                string word = keyword.ToLower();
                jobs = jobs.Where(j => !j.JobStatusMaster.Status.ToLower().Contains(word));
            }
            return jobs;
        }

        /// <summary>
        /// Search With Not Operator in Type column
        /// </summary>
        /// <param name="jobs">IQueryable<JobDetail></param>
        /// <param name="keywords">Search Keyword</param>
        /// <returns>IQueryable<JobDetail></returns>
        private IQueryable<JobDetail> SearchNotWithType(IQueryable<JobDetail> jobs, string[] keywords)
        {
            foreach (string keyword in keywords)
            {
                string word = keyword.ToLower();
                jobs = jobs.Where(j => !j.JobTypeMaster.Type.ToLower().Contains(word));
            }

            return jobs;
        }

        /// <summary>
        /// Search With Not Operator in Source column
        /// </summary>
        /// <param name="jobs">IQueryable<JobDetail></param>
        /// <param name="keywords">Search Keyword</param>
        /// <returns>IQueryable<JobDetail></returns>
        private IQueryable<JobDetail> SearchNotWithSource(IQueryable<JobDetail> jobs, string[] keywords)
        {
            foreach (string keyword in keywords)
            {
                string word = keyword.ToLower();
                jobs = jobs.Where(j => !j.CompanyDetail.CompanySource.Name.ToLower().Contains(word));
            }

            return jobs;
        }

        /// <summary>
        /// Search With Not Operator in Company column
        /// </summary>
        /// <param name="jobs">IQueryable<JobDetail></param>
        /// <param name="keywords">Search Keyword</param>
        /// <returns>IQueryable<JobDetail></returns>
        private IQueryable<JobDetail> SearchNotWithCompany(IQueryable<JobDetail> jobs, string[] keywords)
        {
            foreach (string keyword in keywords)
            {
                string word = keyword.ToLower();
                jobs = jobs.Where(j => !j.CompanyDetail.Name.ToLower().Contains(word));
            }

            return jobs;
        }

        /// <summary>
        /// Search With Not Operator in Requisition Id column
        /// </summary>
        /// <param name="jobs">IQueryable<JobDetail></param>
        /// <param name="keywords">Search Keyword</param>
        /// <returns>IQueryable<JobDetail></returns>
        private IQueryable<JobDetail> SearchNotWithRequisitionId(IQueryable<JobDetail> jobs, string[] keywords)
        {
            foreach (string keyword in keywords)
            {
                string word = keyword.ToLower();
                jobs = jobs.Where(j => !j.RequisitionId.ToLower().Contains(word));
            }

            return jobs;
        }

        /// <summary>
        /// Search With Not Operator in Assigned To column
        /// </summary>
        /// <param name="jobs">IQueryable<JobDetail></param>
        /// <param name="keywords">Search Keyword</param>
        /// <returns>IQueryable<JobDetail></returns>
        private IQueryable<JobDetail> SearchNotWithAssignedTo(IQueryable<JobDetail> jobs, string[] keywords)
        {
            List<Guid> recruiters = new List<Guid>();

            var keywordsLower = keywords.Select(s => s.ToLowerInvariant()).ToArray();

            using (var db = new XSeedEntities())
            {
                /* Get Organization User Id */
                recruiters = db.OrganizationUserDetails.Where(u => keywordsLower.Contains(u.FirstName.ToLower()) || keywordsLower.Contains(u.LastName.ToLower())).Select(u => u.Id).ToList();
            }

            foreach (var item in recruiters)
            {
                /* Apply Not search */
                jobs = jobs.Where(j => !j.OrganizationUserDetails.Any(s => s.Id == item));
            }

            return jobs;
        }

        #endregion

        /// <summary>
        /// Search In Zip column
        /// </summary>
        /// <param name="jobs">IQueryable<JobDetail></param>
        /// <param name="keywords">Search Keyword</param>
        /// <returns>IQueryable<JobDetail></returns>
        private IQueryable<JobDetail> SearchInZip(IQueryable<JobDetail> jobs, string[] keywords)
        {
            foreach (string keyword in keywords)
            {
                string word = keyword.ToLower();
                jobs = jobs.Where(j => j.Zip.ToLower().Contains(word));
            }
            return jobs;
        }

        #endregion

        #endregion
    }
}
