﻿using System;
using System.Collections.Generic;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Job;

namespace XSeed.Data.ViewModel.AdvancedSearch
{
    public class JobSearchModel
    {
        #region Property Declaration

        public Guid OrganizationId { get; set; }
        public string JobTitle { get; set; }
        public string CompanyName { get; set; }
        public string JobStatus { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Job Advanced Search API
        /// </summary>
        /// <param name="jobSearchModel">Job Search Model</param>
        /// <param name="pageSize">page Size</param>
        /// <param name="pageNumber">page Number</param>
        /// <param name="TotalCount">Total Count</param>
        /// <param name="TotalPages">Total Pages</param>
        /// <returns>List<JobDetailModel></returns>
        public List<JobDetailModel> SearchJobs(JobSearchModel jobSearchModel, int pageSize, int pageNumber, out int TotalCount, out double TotalPages)
        {
            return JobDetail.SearchJobs(jobSearchModel, pageSize, pageNumber, out TotalCount, out TotalPages);
        }

        #endregion
    }
}
