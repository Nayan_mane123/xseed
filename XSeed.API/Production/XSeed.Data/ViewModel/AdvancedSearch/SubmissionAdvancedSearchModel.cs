﻿using LinqKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Submission;

namespace XSeed.Data.ViewModel.AdvancedSearch
{
    public class SubmissionAdvancedSearchModel
    {
        #region Property Declaration

        public Guid OrganizationId { get; set; }
        public string SearchIn { get; set; }

        public SubmissionFilterModel filter { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Advance Search Submissions
        /// </summary>
        /// <param name="searchString">searchString</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>
        /// <param name="sortBy">sortBy</param>
        /// <param name="sortOrder">sortOrder</param>
        /// <param name="isExport">isExport</param>
        /// <returns>Submissions List</returns>
        public static List<SubmissionModel> SearchSubmissions(SubmissionAdvancedSearchModel submissionAdvancedSearchModel, int pageSize, int pageNumber, string sortBy, string sortOrder, out int TotalCount, out double TotalPages, bool isExport = false)
        {
            List<SubmissionModel> list = AdvancedSearchSubmissions(submissionAdvancedSearchModel, pageSize, pageNumber, sortBy, sortOrder, out TotalCount, out TotalPages, isExport);

            return list;
        }

        #region Advanced Search

        /// <summary>
        /// Advanced Search for Submission Module Mulitple Keywords with multiple columns
        /// </summary>
        /// <param name="searchString">searchString</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>
        /// <param name="sortBy">sortBy</param>
        /// <param name="sortOrder">sortOrder</param>
        /// <param name="isExport">isExport</param>
        /// <returns>Submissions List</returns>
        private static List<SubmissionModel> AdvancedSearchSubmissions(SubmissionAdvancedSearchModel searchModel, int pageSize, int pageNumber, string sortBy, string sortOrder, out int TotalCount, out double TotalPages, bool isExport = false)
        {
            List<SubmissionModel> list = new List<SubmissionModel>();
            List<SubmissionDetail> submissionList = new List<SubmissionDetail>();
            IQueryable<SubmissionDetail> submissions = null;

            string[] splitNameSearch = null; 
            
            /* Search with Predicate */
            using (var db = new XSeedEntities())
            {
                if (!String.IsNullOrEmpty(searchModel.SearchIn))
                {
                    string firstName = searchModel.SearchIn.ToLower();
                    string lastName = searchModel.SearchIn.ToLower();
                    splitNameSearch = searchModel.SearchIn.Split(' ');
                    if (splitNameSearch != null && splitNameSearch.Length > 1)
                    {
                        firstName = splitNameSearch[0].ToLower();
                        lastName = splitNameSearch[1].ToLower();
                    }
                    /* Apply Predicate */
                    submissions = db.SubmissionDetails.Where(s =>
                        (s.OrganizationUserDetail.OrganizationId == searchModel.OrganizationId)
                        && ((s.CandidateName.ToLower().Contains(searchModel.SearchIn.ToLower()))
                        || (s.JobDetail.CompanyDetail.Name.ToLower().Contains(searchModel.SearchIn.ToLower()))
                        || (s.JobDetail.JobTitle.ToLower().Contains(searchModel.SearchIn.ToLower()))
                        || (s.JobDetail.JobTypeMaster.Type.ToLower().Contains(searchModel.SearchIn.ToLower()))
                        || (s.JobDetail.RequisitionId.ToLower().Contains(searchModel.SearchIn.ToLower()))
                        || (s.OrganizationUserDetail.FirstName.ToLower().Contains(firstName)
                        || s.OrganizationUserDetail.LastName.ToLower().Contains(lastName))))
                        .OrderByDescending(d => d.CreatedOn);
                    
                }
                else
                {
                    submissions = db.SubmissionDetails.Where(s => s.OrganizationUserDetail.OrganizationId == searchModel.OrganizationId);
                }

                /* Apply Filters */
                if (searchModel.filter != null)
                {
                    submissions = SubmissionFilterModel.ApplySearchFilter(submissions, searchModel.filter);
                }

                /* Assign Total Count & Pages */
                TotalCount = (int)submissions.Count();
                TotalPages = Math.Ceiling((double)TotalCount / pageSize);

                submissions = isExport ? submissions.OrderBy(sortBy + " " + sortOrder).Take(1000) : submissions.OrderBy(sortBy + " " + sortOrder).Skip((pageNumber - 1) * pageSize).Take(pageSize);

                /* Populate model list */
                foreach (var submission in submissions)
                {
                    SubmissionModel model = SubmissionDetail.ListSubmissionModel(submission);
                    list.Add(model);
                }
            }

            return list;
        }

        #endregion

        #endregion
    }
}
