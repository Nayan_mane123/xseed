﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.User.CandidateUser;

namespace XSeed.Data.ViewModel.Dashboard
{
    public class DashboardModel
    {
        static string url = ConfigurationManager.AppSettings["MongoURL"].ToString();
        static string dbName = ConfigurationManager.AppSettings["MongoDB"].ToString();

        #region Property Declaration

        public int TotalCandidates { get; set; }
        public int CandidatesAddedToday { get; set; }
        public int TotalJobs { get; set; }
        public int JobsAddedToday { get; set; }
        public int TotalSubmissions { get; set; }
        public int TodaysSubmission { get; set; }
        public int TotalClients { get; set; }
        public int ClientsAddedToday { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Dashboard Detail
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="userId">user Id</param>
        /// <returns>Dashboard Model</returns>
        public static DashboardModel GetDashboardDetail(Guid organizationId, Guid? userId)
        {
            DashboardModel model = new DashboardModel();

            /* Mongo DB Connection */
            MongoClient client = new MongoClient(url);
            var database = client.GetDatabase(dbName);
            var collection = database.GetCollection<CandidateUserModel>("CandidateDetail").AsQueryable(new AggregateOptions { AllowDiskUse = true });

            /* candidate info */
            model.TotalCandidates = (int)collection.Count();
            model.CandidatesAddedToday = (int)collection.Count(c => c.CreatedOn.Value.CompareTo(DateTime.Today) >= 0);

            using (var db = new XSeedEntities())
            {
                /* Job Info */
                model.TotalJobs = db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == organizationId && j.IsActive != false).Count();
                model.JobsAddedToday = db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == organizationId && j.IsActive != false && j.JobStatusMaster.Status != "Closed" && j.JobStatusMaster.Status != "Archive" && j.CreatedOn.Value.CompareTo(DateTime.Today) >= 0).Count();

                /* Submission Info */
                model.TotalSubmissions = db.SubmissionDetails.Where(s => s.JobDetail.CompanyDetail.OrganizationId == organizationId).Count();
                model.TodaysSubmission = db.SubmissionDetails.Where(s => s.CreatedOn.Value.CompareTo(DateTime.Today) >= 0 && s.JobDetail.CompanyDetail.OrganizationId == organizationId).Count();

                /* Client Info */
                model.TotalClients = db.CompanyDetails.Where(c => c.OrganizationId == organizationId && c.IsActive != false).Count();
                model.ClientsAddedToday = db.CompanyDetails.Where(c => c.OrganizationId == organizationId && c.IsActive != false && c.CreatedOn.Value.CompareTo(DateTime.Today) >= 0).Count();
            }

            return model;
        }

        /// <summary>
        /// Get Candidate Added By Recruiters Chart
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <param name="userId">User Id</param>
        /// <returns>List<CandidateChartModel></returns>
        public static List<CandidateChartModel> GetCandidateChartDetail(Guid organizationId, Guid? userId)
        {
            List<CandidateChartModel> list = new List<CandidateChartModel>();
            List<OrganizationUserDetail> recruiters = null;

            using (var db = new XSeedEntities())
            {
                recruiters = db.OrganizationUserDetails.Where(u => u.OrganizationId == organizationId && u.UserLogin.IsActive != false).Take(6).ToList(); // Get Recruiters
            }

            if (recruiters != null)
            {
                /* Mongo DB Connection */
                MongoClient client = new MongoClient(url);
                var database = client.GetDatabase(dbName);
                var collection = database.GetCollection<CandidateUserModel>("CandidateDetail");

                foreach (var recruiter in recruiters)
                {
                    /* Filters */
                    var createdByFilter = Builders<CandidateUserModel>.Filter.Eq("CreatedBy", recruiter.UserId.ToString());

                    /* Populate Model */
                    CandidateChartModel model = new CandidateChartModel();
                    model.Count = (int)collection.Find(createdByFilter).Count();
                    model.UserId = recruiter.Id;
                    model.Name = recruiter.FirstName + " " + recruiter.LastName;

                    if (model.Count > 0)
                    {
                        list.Add(model);
                    }

                }
            }

            return list;
        }

        /// <summary>
        /// Get Recruiter wise Submission Chart Detail
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <param name="userId">userId</param>
        /// <returns>List<SubmissionChartModel></returns>
        public static List<SubmissionChartModel> GetSubmissionChartDetail(Guid organizationId, Guid? userId)
        {
            List<SubmissionChartModel> list = new List<SubmissionChartModel>();

            using (var db = new XSeedEntities())
            {
                /* Get Recruiter wise submissions */
                var submissions = db.SubmissionDetails.Join(db.OrganizationUserDetails, s => s.CreatedBy, r => r.UserId, (s, r) => new { s, r })
                                                      .Where(w => w.r.OrganizationId == organizationId && w.r.UserLogin.IsActive != false)
                                                      .GroupBy(x => new { x.r.UserId })
                                                      .Select(x => new { x.Key, Count = x.Count() }).ToList().Take(6);

                foreach (var submission in submissions)
                {
                    SubmissionChartModel model = new SubmissionChartModel();
                    var recruiter = db.OrganizationUserDetails.FirstOrDefault(u => u.UserId == submission.Key.UserId);

                    if (recruiter != null)
                    {
                        model.Name = recruiter.FirstName + " " + recruiter.LastName;
                        model.UserId = recruiter.Id;
                        model.Count = submission.Count;

                        if (submission.Count > 0)
                        {
                            list.Add(model);
                        }
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Get Job Status Chart Detail
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <param name="userId">userId</param>
        /// <returns>List<JobStatusChartModel></returns>
        public static List<JobStatusChartModel> GetJobStatusChartDetail(Guid organizationId, Guid? userId)
        {
            List<JobStatusChartModel> list = new List<JobStatusChartModel>();

            using (var db = new XSeedEntities())
            {
                var jobs = db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == organizationId)
                                        .GroupBy(j => new { j.JobStatusId })
                                        .Select(j => new { j.Key, Count = j.Count() });

                foreach (var job in jobs)
                {
                    JobStatusChartModel model = new JobStatusChartModel();
                    model.Status = db.JobStatusMasters.Find(job.Key.JobStatusId).Status;
                    model.Count = job.Count;
                    list.Add(model);
                }
            }

            return list;
        }

        /// <summary>
        /// Get Job and Submissions done in last year chart detail
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <param name="userId">userId</param>
        /// <returns>List<JobSubmissionChartModel></returns>
        public static List<JobSubmissionChartModel> GetjobSubmissionChartDetail(Guid organizationId, Guid? userId)
        {
            List<JobSubmissionChartModel> list = new List<JobSubmissionChartModel>();

            using (var db = new XSeedEntities())
            {
                DateTime lastYear = DateTime.Today.AddYears(-1).AddMonths(1);
                /* Get Jobs with last year filter */
                var jobs = db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == organizationId && j.CreatedOn > lastYear)
                                        .GroupBy(j => new { j.CreatedOn.Value.Month, y = j.CreatedOn.Value.Year, m = j.CreatedOn.Value.Month })
                                        .Select(j => new { j.Key, Count = j.Count() });

                foreach (var job in jobs)
                {
                    JobSubmissionChartModel model = new JobSubmissionChartModel();
                    model.JobCount = job.Count;
                    model.SubmissionCount = 0;
                    model.Year = job.Key.y;
                    model.Month = job.Key.m;
                    model.MonthName = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(job.Key.m);//job.Key.CreatedOn.Value.ToString("MM", CultureInfo.InvariantCulture);

                    list.Add(model);
                }

                /* Get submissions with last year filter */
                var submissions = db.SubmissionDetails.Where(s => s.JobDetail.CompanyDetail.OrganizationId == organizationId && s.CreatedOn > lastYear)
                                        .GroupBy(s => new { s.CreatedOn.Value.Month, y = s.CreatedOn.Value.Year, m = s.CreatedOn.Value.Month })
                                        .Select(s => new { s.Key, Count = s.Count() });

                foreach (var submission in submissions)
                {
                    var Year = submission.Key.y;
                    var Month = submission.Key.m;

                    foreach (var item in list)
                    {
                        if (item.Month == Month && item.Year == Year)
                        {
                            item.SubmissionCount = submission.Count;
                        }
                    }
                }
            }

            return list;
        }

        #endregion
    }

    public class CandidateChartModel
    {
        public string Name { get; set; }
        public int Count { get; set; }
        public Guid UserId { get; set; }
    }

    public class SubmissionChartModel
    {
        public string Name { get; set; }
        public Guid UserId { get; set; }
        public int Count { get; set; }
    }

    public class JobStatusChartModel
    {
        public int Count { get; set; }
        public string Status { get; set; }
    }

    public class JobSubmissionChartModel
    {
        public int JobCount { get; set; }
        public int SubmissionCount { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public string MonthName { get; set; }
    }
}
