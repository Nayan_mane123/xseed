﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.User.CandidateUser;

namespace XSeed.Data.ViewModel.Common
{
    public class SchedulerModel
    {
        static string url = ConfigurationManager.AppSettings["MongoURL"].ToString();
        static string dbName = ConfigurationManager.AppSettings["MongoDB"].ToString();
        public static MongoClient client = new MongoClient(url);

        #region Property Declaration

        /* Id References */
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }

        public string Type { get; set; }
        public string SubType { get; set; }
        public string Priority { get; set; }

        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }

        public bool IsSetReminder { get; set; }
        public string ReminderTime { get; set; }
        public string FollowUp { get; set; }

        public string Subject { get; set; }
        public string Notes { get; set; }

        public Guid CompanyId { get; set; }
        public string CompanyName { get; set; }
        public Guid JobId { get; set; }
        public string JobTitle { get; set; }
        public Guid ContactId { get; set; }
        public string Contact { get; set; }
        public List<string> Candidates { get; set; }
        public List<string> CandidateNames { get; set; }

        public bool IsPrivate { get; set; }
        public bool IsCompleted { get; set; }

        /* Audit Trail Info */
        public string CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public string CreatedByEmail { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }

        public string ModifiedBy { get; set; }
        public string ModifiedByName { get; set; }
        public string ModifiedByEmail { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get list of Schedules
        /// </summary>        
        /// <param name="OrganizationUserId">Organization User Id</param>
        /// <returns>Schedules</returns>
        public static List<SchedulerModel> ListSchedules(Guid organizationUserId)
        {
            var database = client.GetDatabase(dbName);

            var filter = Builders<SchedulerModel>.Filter.Eq("CreatedBy", organizationUserId.ToString());
            var collection = database.GetCollection<SchedulerModel>("SchedulerLog");

            var documents = collection.Find(filter).SortBy(c => c.StartDate).ToList();

            return documents;
        }

        /// <summary>
        /// Get single Schedule
        /// </summary>
        /// <param name="Id"> Schedule Log Id</param>
        /// <param name="OrganizationUserId">Organization User Id</param>
        /// <returns>Schedule Log</returns>
        public static SchedulerModel GetScheduleDetail(Guid organizationUserId, string Id)
        {
            var database = client.GetDatabase(dbName);

            var filter = Builders<SchedulerModel>.Filter.Eq("_id", Id);
            var collection = database.GetCollection<SchedulerModel>("SchedulerLog");

            return collection.Find(filter).FirstOrDefault();
        }

        /// <summary>
        /// Update Schedule Log Detail
        /// </summary>
        /// <param name="scheduleModel">Schedule Log Model</param>
        public static void PutScheduleLogDetail(SchedulerModel scheduleModel)
        {
            var database = client.GetDatabase(dbName);
            var filter = Builders<SchedulerModel>.Filter.Eq("_id", scheduleModel._id);
            var collection = database.GetCollection<SchedulerModel>("SchedulerLog");

            /* Update Company, Contact 
             * and Candidate Names */
            UpdateScheduleModel(scheduleModel);

            /* Update Audit Trail */
            scheduleModel.ModifiedBy = BaseModel.CandidateInitiatedBy;
            scheduleModel.ModifiedByName = !string.IsNullOrEmpty(scheduleModel.ModifiedBy) ? OrganizationUserDetail.GetCreatedByUserName(Guid.Parse(scheduleModel.ModifiedBy)) : string.Empty;
            scheduleModel.ModifiedByEmail = !string.IsNullOrEmpty(scheduleModel.ModifiedBy) ? OrganizationUserDetail.GetCreatedByUserEmail(Guid.Parse(scheduleModel.ModifiedBy)) : string.Empty; ;
            scheduleModel.ModifiedOn = DateTime.UtcNow;

            collection.ReplaceOne(filter, scheduleModel);
        }

        /// <summary>
        /// Post schedule Log Detail
        /// </summary>
        /// <param name="scheduleModel">schedule Model</param>
        public static void PostScheduleLogDetail(SchedulerModel scheduleModel)
        {
            MongoClient client = new MongoClient(url);
            var database = client.GetDatabase(dbName);
            var collection = database.GetCollection<SchedulerModel>("SchedulerLog");

            /* Update Company, Contact 
             * and Candidate Names */
            UpdateScheduleModel(scheduleModel);

            /* Update Audit Trail */
            scheduleModel.CreatedBy = BaseModel.CandidateInitiatedBy;
            scheduleModel.CreatedByName = !string.IsNullOrEmpty(scheduleModel.CreatedBy) ? OrganizationUserDetail.GetCreatedByUserName(Guid.Parse(scheduleModel.CreatedBy)) : string.Empty;
            scheduleModel.CreatedByEmail = !string.IsNullOrEmpty(scheduleModel.CreatedBy) ? OrganizationUserDetail.GetCreatedByUserEmail(Guid.Parse(scheduleModel.CreatedBy)) : string.Empty; ;
            scheduleModel.CreatedOn = BaseModel.InitiatedOn.HasValue ? BaseModel.InitiatedOn : DateTime.UtcNow;

            collection.InsertOne(scheduleModel);
        }

        /// <summary>
        /// Update Company, Contact and Candidate Names
        /// </summary>
        /// <param name="scheduleModel"></param>
        private static void UpdateScheduleModel(SchedulerModel scheduleModel)
        {
            scheduleModel.CompanyName = CompanyDetail.GetCompanyName(scheduleModel.CompanyId);
            scheduleModel.Contact = CompanyContact.GetCompanyContactName(scheduleModel.ContactId);
            scheduleModel.JobTitle = JobDetail.GetJobTitle(scheduleModel.JobId);

            if (scheduleModel.Candidates != null)
            {
                scheduleModel.CandidateNames = new List<string>();

                foreach (var Id in scheduleModel.Candidates)
                {
                    string name = CandidateUserModel.GetCandidateName(Id);

                    if (!string.IsNullOrEmpty(name))
                    {
                        scheduleModel.CandidateNames.Add(name);
                    }
                }
            }
        }

        #endregion
    }
}
