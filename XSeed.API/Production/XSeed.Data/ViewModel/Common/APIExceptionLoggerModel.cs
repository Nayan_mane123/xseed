﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XSeed.Data.ViewModel.Common
{
    public class APIExceptionLoggerModel
    {
        static string url = ConfigurationManager.AppSettings["MongoURL"].ToString();
        static string dbName = ConfigurationManager.AppSettings["MongoDB"].ToString();
        public static MongoClient client = new MongoClient(url);
        static IMongoDatabase database = client.GetDatabase(dbName);

        #region Property Declaration

        /* Id References */
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string ExceptionMessage { get; set; }
        public string ExceptionStackTrace { get; set; }
        public string ExceptionInnerException { get; set; }
        public string UserId { get; set; }
        public string UserType { get; set; }
        public Nullable<DateTime> CreatedOn { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get list API Exception logs
        /// </summary>        
        /// <returns>API Exception Logs</returns>
        public static List<APIExceptionLoggerModel> ListAPIExceptionLogs()
        {
            var filter = Builders<APIExceptionLoggerModel>.Filter.Empty;
            var collection = database.GetCollection<APIExceptionLoggerModel>("APIExceptionLog");

            var documents = collection.Find(filter).SortByDescending(c => c.CreatedOn).ToList();

            return documents;
        }

        /// <summary>
        /// Get single API Exception log
        /// </summary>
        /// <param name="Id"> API Log Id</param>
        /// <returns>API Exception Log</returns>
        public static APIExceptionLoggerModel GetAPIExceptionLogDetail(string Id)
        {
            var filter = Builders<APIExceptionLoggerModel>.Filter.Eq("_id", Id);
            var collection = database.GetCollection<APIExceptionLoggerModel>("APIExceptionLog");

            return collection.Find(filter).FirstOrDefault();
        }

        /// <summary>
        /// Log API Exception
        /// </summary>
        /// <param name="exceptionModel">API Exception Logger Model</param>
        public void LogException(APIExceptionLoggerModel exceptionModel)
        {
            var collection = database.GetCollection<APIExceptionLoggerModel>("APIExceptionLog");

            collection.InsertOne(exceptionModel);
        }

        #endregion

    }
}
