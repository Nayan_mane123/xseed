﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;

namespace XSeed.Data.ViewModel.Common
{
    public class BaseModel
    {
        public static Guid InitiatedBy { get; set; }
        public static string CandidateInitiatedBy { get; set; }
        public static Nullable<DateTime> InitiatedOn { get; set; }
        public static string ActivityId { get; set; }
        public static bool IsActivityUpdate { get; set; }
        public static string UserType { get; set; }
    }

    public class AuditTrail
    {
        #region Property Declaration
        public Guid CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public Nullable<DateTime> CreatedOn { get; set; }

        public Guid ModifiedBy { get; set; }
        public string ModifiedByName { get; set; }
        public Nullable<DateTime> ModifiedOn { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get audit trail information for the record
        /// </summary>
        /// <param name="createdBy">Created By</param>
        /// <param name="createdOn">Created On</param>
        /// <param name="modifiedBy">Modified By</param>
        /// <param name="modifiedOn">Modified On</param>
        /// <returns>AuditTrail Info</returns>
        internal static AuditTrail GetAuditTrailInfo(Guid? createdBy, DateTime? createdOn, Guid? modifiedBy, DateTime? modifiedOn)
        {
            AuditTrail auditTrail = new AuditTrail();

            using (var db = new XSeedEntities())
            {

                var createdByUser = createdBy != Guid.Empty ? db.OrganizationUserDetails.FirstOrDefault(u => u.UserId == createdBy) : null;

                /* populate created info */
                auditTrail.CreatedBy = createdByUser != null ? createdByUser.UserId : Guid.Empty;
                auditTrail.CreatedByName = createdByUser != null ? createdByUser.FirstName + " " + createdByUser.LastName : string.Empty;
                auditTrail.CreatedOn = createdOn;

                /* populate modified info */
                var modifiedByUser = modifiedBy != Guid.Empty ? db.OrganizationUserDetails.FirstOrDefault(u => u.UserId == modifiedBy) : null;
                auditTrail.ModifiedBy = modifiedByUser != null ? modifiedByUser.UserId : Guid.Empty;
                auditTrail.ModifiedByName = modifiedByUser != null ? modifiedByUser.FirstName + " " + modifiedByUser.LastName : string.Empty;
                auditTrail.ModifiedOn = modifiedOn;
            }

            return auditTrail;
        }

        #endregion
    }

    public class PaginationModel
    {
        public int TotalCount { get; set; }
        public double TotalPages { get; set; }
    }

    public class PortalLicenseInfo
    {
        public bool IsLincenseValid { get; set; }
        public Nullable<DateTime> LicenseExpiryDate { get; set; }
        public Nullable<int> AvailableCount { get; set; }
    }

    public class PortalTokenInfo
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string refresh_token { get; set; }
        public int expires_in { get; set; }
        public string scope { get; set; }
        public string client_id { get; set; }
        public string username { get; set; }
        public string user_id { get; set; }
        public string user_type { get; set; }
        public bool dice_user { get; set; }
        public string company_id { get; set; }
        public int external_id { get; set; }
    }
}
