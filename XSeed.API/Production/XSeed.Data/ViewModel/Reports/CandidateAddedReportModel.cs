﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.User.CandidateUser;
using System.Linq.Dynamic;

namespace XSeed.Data.ViewModel.Reports
{
    public class CandidateAddedReportModel
    {
        static string url = ConfigurationManager.AppSettings["MongoURL"].ToString();
        static string dbName = ConfigurationManager.AppSettings["MongoDB"].ToString();
        /* Mongo DB Connection */
        static MongoClient client = new MongoClient(url);
        IMongoDatabase database = client.GetDatabase(dbName);

        #region Property Declaration

        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string Recruiter { get; set; }
        public int MonsterCount { get; set; }
        public int DiceCount { get; set; }
        public int ManuallyCreatedCount { get; set; }
        public int Total { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Candidate Added by recruiters Report
        /// </summary>
        /// <returns>Candidate added report</returns>
        public List<CandidateAddedReportModel> GetCandidateAddedReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", int daysBefore = -7, bool isExport = false)
        {
            /* Initialize list */
            List<CandidateAddedReportModel> list = new List<CandidateAddedReportModel>();
            /* Calculate date */
            DateTime beforeDate = DateTime.Now.AddDays(daysBefore).Date;

            using (var db = new XSeedEntities())
            {
                /* Get all users */
                var orgUserIdList = db.OrganizationUserDetails.Where(x => x.OrganizationId == organizationId)
                                                              .Select(x => x.UserId.ToString()).ToList();

                var candidateList = database.GetCollection<CandidateUserModel>("CandidateDetail")
                                            .AsQueryable(new AggregateOptions { AllowDiskUse = true })
                                            .OrderByDescending(x => x.CreatedOn)
                                            .Where(x => x.CreatedOn >= beforeDate && orgUserIdList.Contains(x.CreatedBy))
                                            .Select(c => new CandidateUserModel()
                                            {
                                                _id = c._id,
                                                Source = c.Source,
                                                CreatedBy = c.CreatedBy,
                                                CreatedByName = c.CreatedByName,
                                                CreatedOn = c.CreatedOn
                                            });

                if (candidateList != null)
                {
                    var candidates = candidateList.ToList()
                                        .GroupBy(c => new
                                        {
                                            c.CreatedByName,
                                            c.CreatedBy,
                                            y = c.CreatedOn.Value.Year,
                                            m = c.CreatedOn.Value.Month,
                                            d = c.CreatedOn.Value.Day
                                        })
                                        .Select(c => new
                                        {
                                            c.Key,
                                            LocalCount = c.Where(x => x.Source != "Monster" || x.Source != "Dice").Count(),
                                            MonsterCount = c.Where(x => x.Source == "Monster").Count(),
                                            DiceCount = c.Where(x => x.Source == "Dice").Count()
                                        });

                    foreach (var candidate in candidates)
                    {
                        /* Initialize model */
                        CandidateAddedReportModel model = new CandidateAddedReportModel();

                        /* Populate model */
                        model.CreatedOn = new DateTime(candidate.Key.y, candidate.Key.m, candidate.Key.d);
                        model.Recruiter = candidate.Key.CreatedByName;
                        model.ManuallyCreatedCount = candidate.LocalCount;
                        model.MonsterCount = candidate.MonsterCount;
                        model.DiceCount = candidate.DiceCount;
                        model.Total = candidate.LocalCount + candidate.MonsterCount + candidate.DiceCount;

                        list.Add(model);
                    }

                    list = isExport ? list.OrderByDescending(c => c.CreatedOn).ToList() : list.OrderBy(sortBy + " " + sortOrder).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
                }
            }

            return list;
        }

        /// <summary>
        /// Get Pagination Info
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid organizationId, int pageSize, int daysBefore = -7)
        {
            PaginationModel model = new PaginationModel();
            /* Calculate date */
            DateTime beforeDate = DateTime.Now.AddDays(daysBefore).Date;

            using (var db = new XSeedEntities())
            {
                /* Get all users */
                var orgUserIdList = db.OrganizationUserDetails.Where(x => x.OrganizationId == organizationId)
                                                              .Select(x => x.UserId.ToString()).ToList();

                model.TotalCount = database.GetCollection<CandidateUserModel>("CandidateDetail")
                                            .AsQueryable(new AggregateOptions { AllowDiskUse = true })
                                            .OrderByDescending(x => x.CreatedOn)
                                            .Where(x => x.CreatedOn >= beforeDate && orgUserIdList.Contains(x.CreatedBy)).GroupBy(c => new
                                            {
                                                c.CreatedByName,
                                                c.CreatedBy,
                                                y = c.CreatedOn.Value.Year,
                                                m = c.CreatedOn.Value.Month,
                                                d = c.CreatedOn.Value.Day
                                            }).Count();
                model.TotalPages = Math.Ceiling((double)model.TotalCount / pageSize);
            }

            return model;
        }

        #endregion

    }
}
