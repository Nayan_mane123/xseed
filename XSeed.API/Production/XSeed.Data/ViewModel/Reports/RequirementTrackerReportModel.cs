﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using System.Linq.Dynamic;

namespace XSeed.Data.ViewModel.Reports
{
    public class RequirementTrackerReportModel
    {
        #region Property Declaration

        public Nullable<System.DateTime> RequirementDate { get; set; }
        public string CompanyContact { get; set; }
        public string Company { get; set; }
        public string JobTitle { get; set; }
        public string Status { get; set; }
        public int InternalSubmissions { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Requirement Tracker Report
        /// </summary>
        /// <returns>Requirement tracker report</returns>
        public List<RequirementTrackerReportModel> GetRequirementTrackerReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", int daysBefore = -7, bool isExport = false)
        {
            /* Initialize list */
            List<RequirementTrackerReportModel> list = new List<RequirementTrackerReportModel>();

            using (var db = new XSeedEntities())
            {
                /* Calculate date */
                DateTime beforeDate = DateTime.Now.AddDays(daysBefore).Date;

                /* Get all jobs  */
                var jobs = isExport ? db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == organizationId && j.CreatedOn >= beforeDate).OrderBy(sortBy + " " + sortOrder)
                                           : db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == organizationId && j.CreatedOn >= beforeDate)
                                           .OrderBy(sortBy + " " + sortOrder)
                                           .Skip((pageNumber - 1) * pageSize).Take(pageSize);

                foreach (var job in jobs)
                {
                    /* Initialize model */
                    RequirementTrackerReportModel model = new RequirementTrackerReportModel();

                    /* Get contact */
                    CompanyContact contact = job.CompanyContact;

                    /* Populate model */
                    model.RequirementDate = job.CreatedOn;
                    model.CompanyContact = contact != null ? (contact.FirstName + " " + contact.LastName) : string.Empty;
                    model.Company = job.CompanyDetail != null ? job.CompanyDetail.Name : string.Empty;
                    model.JobTitle = job.JobTitle;
                    model.Status = job.JobStatusMaster != null ? job.JobStatusMaster.Status : string.Empty;
                    model.InternalSubmissions = job.SubmissionDetails != null ? job.SubmissionDetails.Count() : 0;

                    list.Add(model);
                }
            }

            return list;
        }

        /// <summary>
        /// Get Pagination Info
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid organizationId, int pageSize, int daysBefore = -7)
        {
            PaginationModel model = new PaginationModel();
            DateTime dayBeforeDate = DateTime.Now.AddDays(daysBefore).Date;
            using (var db = new XSeedEntities())
            {
                model.TotalCount = db.JobDetails.Where(j => j.CompanyDetail.OrganizationId == organizationId && j.CreatedOn >= dayBeforeDate).Count();
                model.TotalPages = Math.Ceiling((double)model.TotalCount / pageSize);
            }

            return model;
        }

        #endregion

    }
}
