﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Activity;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.User.CandidateUser;
using System.Linq.Dynamic;


namespace XSeed.Data.ViewModel.Reports
{
    public class ActivityReportModel
    {
        static string url = ConfigurationManager.AppSettings["MongoURL"].ToString();
        static string dbName = ConfigurationManager.AppSettings["MongoDB"].ToString();

        #region Property Declaration

        public string OrganizationUser { get; set; }
        public int CompanyCount { get; set; }
        public int CompanyContactCount { get; set; }
        public int RequirementCount { get; set; }
        public int CandidateCount { get; set; }
        public int SubmissionCount { get; set; }
        public int SubmissionFeedbackCount { get; set; }
        public int SubmissionFeedbackClosedCount { get; set; }
        public int QNoteActivityCount { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Activity Report
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Activity Report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        public List<ActivityReportModel> GetActivityReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", int daysBefore = -7, bool isExport = false)
        {
            MongoClient client = new MongoClient(url);
            var database = client.GetDatabase(dbName);
            var collection = database.GetCollection<CandidateUserModel>("CandidateDetail");
            var collectionJobQNoteActivity = database.GetCollection<JobActivityLogModel>("JobActivityLog");
            var collectionCandidateQNoteActivity = database.GetCollection<CandidateActivityLogModel>("CandidateActivityLog");
            /* Calculate date */
            DateTime beforeDate = DateTime.Now.AddDays(daysBefore).Date;

            /* Initialize list */
            List<ActivityReportModel> list = new List<ActivityReportModel>();

            using (var db = new XSeedEntities())
            {
                /* Get all users */
                var organizationUsers = db.OrganizationUserDetails.Where(u => u.OrganizationId == organizationId);
                
                foreach (var user in organizationUsers)
                {
                    int jobQNoteActivityCount = collectionJobQNoteActivity.AsQueryable().Where(c => c.CreatedBy == user.UserId && c.CreatedOn >= beforeDate).Count();
                    int candidateQNoteActivityCount = collectionCandidateQNoteActivity.AsQueryable().Where(c => c.CreatedBy == user.UserId && c.CreatedOn >= beforeDate).Count();

                    ActivityReportModel model = new ActivityReportModel();

                    model.OrganizationUser = user.FirstName + " " + user.LastName;
                    model.CompanyCount = db.CompanyDetails.Where(c => c.CreatedBy == user.UserId && c.CreatedOn >= beforeDate).Count();
                    model.CompanyContactCount = db.CompanyContacts.Where(c => c.CreatedBy == user.UserId && c.CreatedOn >= beforeDate).Count();
                    model.RequirementCount = db.JobDetails.Where(c => c.CreatedBy == user.UserId && c.CreatedOn >= beforeDate).Count();
                    model.CandidateCount = collection.AsQueryable().Where(c => c.CreatedBy == user.UserId.ToString() && c.CreatedOn >= beforeDate).Count();
                    model.SubmissionCount = db.SubmissionDetails.Where(c => c.CreatedBy == user.UserId && c.CreatedOn >= beforeDate).Count();
                    model.SubmissionFeedbackCount = db.SubmissionFeedbacks.Where(c => c.CreatedBy == user.UserId && c.CreatedOn >= beforeDate).Count();
                    model.SubmissionFeedbackClosedCount = db.SubmissionFeedbacks.Where(c => (c.CreatedBy == user.UserId) && (c.SubmissionStatusMaster.Status == "Selected" || c.SubmissionStatusMaster.Status == "Hired") && c.CreatedOn >= beforeDate).Count();
                    model.QNoteActivityCount = jobQNoteActivityCount + candidateQNoteActivityCount;

                    list.Add(model);
                }
            }

            list = isExport ? list.OrderBy(sortBy + " " + sortOrder).ToList() : list.OrderBy(sortBy + " " + sortOrder).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

            return list;
        }

        /// <summary>
        /// Get Pagination Info
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid organizationId, int pageSize, int daysBefore = -7)
        {
            PaginationModel model = new PaginationModel();
            /* Calculate date */
            DateTime beforeDate = DateTime.Now.AddDays(daysBefore).Date;

            using (var db = new XSeedEntities())
            {
                model.TotalCount = db.OrganizationUserDetails.Where(u => u.OrganizationId == organizationId).Count();
                model.TotalPages = Math.Ceiling((double)model.TotalCount / pageSize);
            }

            return model;
        }

        #endregion


    }
}
