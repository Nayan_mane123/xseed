﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using System.Configuration;
using XSeed.Data.ViewModel.Common;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace XSeed.Data.ViewModel.APILogger
{
    public class ApiLogEntry
    {
        static string mongoUrl = Convert.ToString(ConfigurationManager.AppSettings["MongoURL"]);
        static string dbName = Convert.ToString(ConfigurationManager.AppSettings["MongoDB"]);
        static string collectionName = Convert.ToString(ConfigurationManager.AppSettings["MongoApiLogCollection"]);

        #region Property Declaration

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }                     // The (database) ID for the API log entry.
        public string Application { get; set; }             // The application that made the request.
        public string User { get; set; }                    // The user that made the request.
        public string Machine { get; set; }                 // The machine that made the request.
        public string RequestIpAddress { get; set; }        // The IP address that made the request.
        public string RequestContentType { get; set; }      // The request content type.
        public string RequestContentBody { get; set; }      // The request content body.
        public string RequestUri { get; set; }              // The request URI.
        public string RequestMethod { get; set; }           // The request method (GET, POST, etc).
        public string RequestRouteTemplate { get; set; }    // The request route template.
        public string RequestRouteData { get; set; }        // The request route data.
        public string RequestHeaders { get; set; }          // The request headers.
        public DateTime? RequestTimestamp { get; set; }     // The request timestamp.
        public string ResponseContentType { get; set; }     // The response content type.
        public string ResponseContentBody { get; set; }     // The response content body.
        public int? ResponseStatusCode { get; set; }        // The response status code.
        public string ResponseHeaders { get; set; }         // The response headers.
        public DateTime? ResponseTimestamp { get; set; }    // The response timestamp.

        #endregion

        #region User Defined Functions

        public static void SaveApiLogEntry(ApiLogEntry apiLogEntry)
        {
            MongoClient client = new MongoClient(mongoUrl);
            var database = client.GetDatabase(dbName);
            var collection = database.GetCollection<ApiLogEntry>(collectionName);
            collection.InsertOne(apiLogEntry);
        }

        #endregion

        public static List<ApiLogEntry> ListApiLog(int pageSize, int pageNumber)
        {
            MongoClient client = new MongoClient(mongoUrl);
            var database = client.GetDatabase(dbName);

            var collection = database.GetCollection<ApiLogEntry>(collectionName);
            var documents = collection.Find(Builders<ApiLogEntry>.Filter.Empty).SortByDescending(c => c._id).Skip((pageNumber - 1) * pageSize).Limit(pageSize).ToList();

            return documents;
        }

        /// <summary>
        /// Get Pagination Info
        /// </summary>
        /// <param name="pageSize">pageSize</param>
        /// <returns>Pagination Model</returns>
        public static PaginationModel GetPaginationInfo(int pageSize)
        {
            PaginationModel model = new PaginationModel();

            MongoClient client = new MongoClient(mongoUrl);
            var database = client.GetDatabase(dbName);

            var collection = database.GetCollection<ApiLogEntry>(collectionName);
            model.TotalCount = (int)collection.Find(Builders<ApiLogEntry>.Filter.Empty).Count();
            model.TotalPages = Math.Ceiling((double)model.TotalCount / pageSize);

            return model;
        }

        /// <summary>
        /// Get ApiLog Detail
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns>ApiLog Detail</returns>
        public static ApiLogEntry GetApiLogDetail(string Id)
        {
            try
            {
                MongoClient client = new MongoClient(mongoUrl);
                var database = client.GetDatabase(dbName);

                var filter = Builders<ApiLogEntry>.Filter.Eq("_id", ObjectId.Parse(Id));
                var collection = database.GetCollection<ApiLogEntry>(collectionName);

                return collection.Find(filter).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
