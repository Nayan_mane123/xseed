﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using XSeed.Data.ViewModel.Common;
using XSeed.Utility;


namespace XSeed.Data.ViewModel.Notification
{

    public class EmailNotificationModel
    {
        #region Property Declaration

        [Required]
        public string TypeOfNotification { get; set; }

        [Required]
        public string FromEmailID { get; set; }

        public string FromEmailPassword { get; set; }
        public string FromDisplayName { get; set; }

        [Required]
        public List<string> ToEmailID { get; set; }
        public List<string> ToCC { get; set; }
        public List<string> ToBCC { get; set; }
        public string MailSubject { get; set; }

        [Required]
        public string MailBody { get; set; }
        public string CallBackUrl { get; set; }
        public List<XSeedFileEntity> MailAttachment { get; set; }

        public string ExtraData { get; set; }
        public int Priority { get; set; }
        public bool IsSent { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Sends Email Notification
        /// </summary>
        /// <param name="emailNotificationModel">EmailNotificationModel</param>
        public static bool SendEmailNotification(EmailNotificationModel model)
        {
            bool result = true;

            try
            {
                if (model.ToEmailID != null)
                {
                    string mailTemplate = "";

                    switch (model.TypeOfNotification)
                    {
                        case "ResetPassword":
                            model.MailSubject = Constants.ResetPasswordSubject;
                            mailTemplate = File.ReadAllText(HttpContext.Current.Server.MapPath(Constants.ResetPasswordEmailTemplatePath));
                            mailTemplate = mailTemplate.Replace("action_url", model.CallBackUrl.ToString());
                            mailTemplate = mailTemplate.Replace("xseed_logo", ConfigurationManager.AppSettings["XSeedLogoFilePath"]);
                            model.MailBody = mailTemplate;
                            model.Priority = Convert.ToInt32(EmailPriority.Urgent);
                            break;
                        case "VerifyEmail":
                            model.MailSubject = Constants.VerifyEmailSubject;
                            mailTemplate = File.ReadAllText(HttpContext.Current.Server.MapPath(Constants.VerifyEmailEmailTemplatePath));
                            mailTemplate = mailTemplate.Replace("action_url", model.CallBackUrl.ToString());
                            mailTemplate = mailTemplate.Replace("xseed_logo", ConfigurationManager.AppSettings["XSeedLogoFilePath"]);
                            model.MailBody = mailTemplate;
                            model.Priority = Convert.ToInt32(EmailPriority.Urgent);
                            break;
                        case "PasswordMail":
                            model.MailSubject = Constants.PasswordMailSubject;
                            mailTemplate = File.ReadAllText(HttpContext.Current.Server.MapPath(Constants.PasswordMailEmailTemplatePath));
                            mailTemplate = mailTemplate.Replace("userName", model.ToEmailID.FirstOrDefault());
                            mailTemplate = mailTemplate.Replace("userPassword", model.ExtraData.ToString());
                            mailTemplate = mailTemplate.Replace("action_url", model.CallBackUrl.ToString());
                            mailTemplate = mailTemplate.Replace("xseed_logo", ConfigurationManager.AppSettings["XSeedLogoFilePath"]);
                            model.MailBody = mailTemplate;
                            model.Priority = Convert.ToInt32(EmailPriority.Urgent);
                            break;

                        case "ResumeAttachment":
                            model.MailSubject = Constants.resumeAttachmentSubject;
                            mailTemplate = File.ReadAllText(HttpContext.Current.Server.MapPath(Constants.ResumeAttachmentTemplatePath));
                            model.MailBody = mailTemplate;
                            model.Priority = Convert.ToInt32(EmailPriority.High);
                            break;

                        case "EmailSubmission":
                            model.MailSubject = Constants.EmailSubmissionAttachmentSubject;
                            mailTemplate = File.ReadAllText(HttpContext.Current.Server.MapPath(Constants.EmailSubmissionAttachmentTemplatePath));
                            //model.MailBody = mailTemplate;
                            model.Priority = Convert.ToInt32(EmailPriority.High);
                            break;

                        default:
                            model.Priority = Convert.ToInt32(EmailPriority.Low);
                            break;
                    }

                    model.FromDisplayName = model.FromDisplayName == null ? ConfigurationManager.AppSettings["EmailFromDisplayName"] : model.FromDisplayName;
                    model.IsSent = false;
                    model.CreatedDate = DateTime.UtcNow;

                    return EnqueEmail(model);
                }
            }
            catch (Exception)
            {
                result = false;
                //throw;
            }

            return result;
        }

        private static bool EnqueEmail(EmailNotificationModel model)
        {
            bool result = true;
            MongoClient client = new MongoClient(ConfigurationManager.AppSettings["MongoURL"]);

            try
            {
                var database = client.GetDatabase(ConfigurationManager.AppSettings["MongoDB"]);
                var collection = database.GetCollection<EmailNotificationModel>(ConfigurationManager.AppSettings["MongoEmailCollection"]);

                collection.InsertOne(model);
            }
            catch (Exception)
            {
                result = false;
                //throw;
            }
            finally
            {
                client = null;
            }

            return result;
        }

        #endregion

    }
}
