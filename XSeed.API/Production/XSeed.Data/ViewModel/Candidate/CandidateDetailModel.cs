﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using XSeed.Utility;

namespace XSeed.Data.ViewModel.Candidate
{
    public class CandidateDetailModel
    {
        #region Property Declaration

        public Guid Id { get; set; }
        public Nullable<Guid> UserId { get; set; }
        public Nullable<Guid> TitleId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public Nullable<System.DateTime> BirthDate { get; set; }
        public string ProfileImage { get; set; }
        public XSeedFileEntity ProfileImageFile { get; set; }
        public Nullable<Guid> GenderId { get; set; }
        public string Gender { get; set; }
        public Nullable<Guid> MaritalStatusId { get; set; }
        public string MaritalStatus { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "EmailRequired")]
        [RegularExpression(Constants.EmailExpression, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "EmailInvalid")]
        public string PrimaryEmail { get; set; }

        [System.ComponentModel.DefaultValue(""), RegularExpression(Constants.EmailExpression, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "EmailInvalid")]
        [DataType(DataType.EmailAddress)]
        public string SecondaryEmail { get; set; }

        [System.ComponentModel.DefaultValue(""), RegularExpression(Constants.MobileExpression, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "MobileInvalid")]
        public string Mobile { get; set; }

        [System.ComponentModel.DefaultValue(""), RegularExpression(Constants.PhoneNoExpression, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "PhoneNoInvalid")]
        public string Phone { get; set; }

        public string Fax { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public Nullable<Guid> CountryId { get; set; }
        public string Country { get; set; }
        public Nullable<Guid> StateId { get; set; }
        public string State { get; set; }
        public Nullable<Guid> CityId { get; set; }
        public string City { get; set; }

        [System.ComponentModel.DefaultValue(""), RegularExpression(Constants.ZipExpression, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "ZipLength")]
        public string Zip { get; set; }
        public Nullable<bool> HavePassport { get; set; }
        public Nullable<System.DateTime> PassportValidUpto { get; set; }
        public Nullable<Guid> VisaTypeId { get; set; }
        public string VisaType { get; set; }
        public Nullable<System.DateTime> VisaValidUpto { get; set; }
        public Nullable<bool> SendJobAlert { get; set; }
        public Nullable<bool> IsExperienced { get; set; }
        public virtual List<LookUpModel> DegreeList { get; set; }
        public virtual List<LookUpModel> SkillList { get; set; }

        /* Social Info */
        public string LinkedInURL { get; set; }
        public string TwitterURL { get; set; }
        public string GooglePlusURL { get; set; }
        public string FacebookURL { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get list of candidates
        /// </summary>        
        /// <returns>Candidate Detail Model List</returns>
        public List<CandidateDetailModel> ListCandidates()
        {
            return CandidateDetail.ListCandidates();
        }

        /// <summary>
        /// Get list of candidates using server side pagination
        /// </summary>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>
        /// <param name="orderBy">orderBy</param>
        /// <returns>Candidate List</returns>
        public List<CandidateDetailModel> ListCandidates(int pageSize, int pageNumber)
        {
            return CandidateDetail.ListCandidates(pageSize, pageNumber);
        }

        /// <summary>
        /// Get particular candidate
        /// </summary>
        /// <param name="Id">Candidate Id</param>        
        /// <returns>Candidate Contact Model</returns>
        public CandidateDetailModel GetCandidateDetail(Guid? Id)
        {
            return CandidateDetail.GetCandidateDetail(Id);
        }

        /// <summary>
        /// Create New Candidate
        /// </summary>
        /// <param name="candidateDetailModel">Candidate Detail Model </param>
        public Guid CreateCandidateDetail(CandidateDetailModel candidateDetailModel)
        {
            return CandidateDetail.CreateCandidateDetail(candidateDetailModel);
        }

        /// <summary>
        /// Update Candidate Detail
        /// </summary>
        /// <param name="candidateDetailModel">Candidate Detail Model </param>
        public void UpdateCandidateDetail(CandidateDetailModel candidateDetailModel)
        {
            CandidateDetail.UpdateCandidateDetail(candidateDetailModel);
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>        
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        public static PaginationModel GetPaginationInfo(int pageSize)
        {
            return CandidateDetail.GetPaginationInfo(pageSize);
        }

        #endregion
    }
}
