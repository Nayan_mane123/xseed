﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Utility;

namespace XSeed.Data.ViewModel.Account
{
    public class ChangePasswordModel
    {
        #region Property Declaration

        [Display(Name = "User ID")]
        public Nullable<Guid> Id { get; set; }

        [Display(Name = "Candidate User ID")]
        public string CandidateUserId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "PasswordRequired")]
        [Compare("OldPassword", ErrorMessage = "Old password Compare Failed")]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "PasswordRequired")]
        [StringLength(100, MinimumLength = 6, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "PasswordLength")]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "PasswordCompare")]
        public string ConfirmPassword { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Change User Password
        /// </summary>
        /// <param name="model">ChangePasswordModel</param>
        public static void ChangeUserPassword(ChangePasswordModel model)
        {
            UserLogin.ChangeUserPassword(model);
        }        

        public static void ChangeCandidatePassword(ChangePasswordModel model)
        {
            UserLogin.ChangeCandidatePassword(model);
        }

        #endregion
    }
}
