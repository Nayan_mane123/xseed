﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using XSeed.Utility;

namespace XSeed.Data.ViewModel.Company
{
    public class CompanyContactModel
    {
        #region Property Declaration

        public Guid Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.CompanyContact), ErrorMessageResourceName = "CompanyIdRequired")]
        public Nullable<Guid> CompanyId { get; set; }
        public string CompanyName { get; set; }

        public Nullable<Guid> TitleId { get; set; }
        public string Title { get; set; }

        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public Nullable<System.DateTime> BirthDate { get; set; }
        public Nullable<System.DateTime> AnniversaryDate { get; set; }

        public string ProfileImage { get; set; }
        public XSeedFileEntity ProfileImageFile { get; set; }

        public string ReportingTo { get; set; }
        public string Designation { get; set; }
        public string PracticeLine { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "EmailRequired")]
        [RegularExpression(Constants.EmailExpression, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "EmailInvalid")]
        [DataType(DataType.EmailAddress)]
        public string PrimaryEmail { get; set; }


        [System.ComponentModel.DefaultValue(""), RegularExpression(Constants.EmailExpression, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "EmailInvalid")]
        [DataType(DataType.EmailAddress)]
        public string SecondaryEmail { get; set; }

        [System.ComponentModel.DefaultValue(""), RegularExpression(Constants.MobileExpression, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "MobileInvalid")]
        public string Mobile { get; set; }

        [System.ComponentModel.DefaultValue(""), RegularExpression(Constants.PhoneNoExpression, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "PhoneNoInvalid")]
        public string Phone { get; set; }

        public string Fax { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public Nullable<Guid> CountryId { get; set; }
        public string Country { get; set; }
        public Nullable<Guid> StateId { get; set; }
        public string State { get; set; }
        public Nullable<Guid> CityId { get; set; }
        public string City { get; set; }

        [System.ComponentModel.DefaultValue(""), RegularExpression(Constants.ZipExpression, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "ZipLength")]
        public string Zip { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get list of contacts belongs to company
        /// </summary>
        /// <param name="companyId">Company Id</param>
        /// <returns>Company Contact Model List</returns>
        public List<CompanyContactModel> ListCompanyContacts(Guid companyId)
        {
            return CompanyContact.ListCompanyContacts(companyId);
        }

        /// <summary>
        /// Get particular contact
        /// </summary>
        /// <param name="companyId">Company Id</param>
        /// <param name="contactId">Contact Id</param>
        /// <returns>Company Contact Model</returns>
        public CompanyContactModel GetCompanyContactModel(Guid? Id, Guid companyId)
        {
            return CompanyContact.GetCompanyContact(Id, companyId);
        }

        /// <summary>
        /// Create New Company Contact
        /// </summary>
        /// <param name="companyContactModel">Company Contact Model </param>
        public Guid CreateCompanyContact(CompanyContactModel companyContactModel)
        {
            return CompanyContact.CreateCompanyContact(companyContactModel);
        }

        public void UpdateCompanyContact(CompanyContactModel companyContactModel)
        {
            CompanyContact.UpdateCompanyContact(companyContactModel);
        }

        #endregion
    }
}
