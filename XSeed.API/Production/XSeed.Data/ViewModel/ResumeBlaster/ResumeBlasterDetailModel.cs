﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using XSeed.Utility;

namespace XSeed.Data.ViewModel.ResumeBlaster
{
    public class ResumeBlasterDetailModel
    {
        #region Property Declaration

        public Guid Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Company), ErrorMessageResourceName = "CompanyNameRequired")]
        public string Company { get; set; }

        public string RecruiterName { get; set; }

        [RegularExpression(Constants.EmailExpression, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "EmailInvalid")]
        public string PrimaryEmail { get; set; }

        [System.ComponentModel.DefaultValue(""), RegularExpression(Constants.MobileExpression, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "MobileInvalid")]
        public string Mobile { get; set; }

        [System.ComponentModel.DefaultValue(""), RegularExpression(Constants.PhoneNoExpression, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "PhoneNoInvalid")]
        public string Phone { get; set; }

        public string Address { get; set; }
        public string Website { get; set; }
        public Nullable<bool> IsMailSent { get; set; }


        #endregion

        /// <summary>
        /// Get list of recruiter
        /// </summary>
        /// <returns>Resume Blaster Detail Model List</returns>
        public List<ResumeBlasterDetailModel> ListResumeBlasters()
        {
            return Blaster.ListResumeBlasters();
        }

        public ResumeBlasterDetailModel GetResumeBlasterDetail(Guid? Id)
        {
            return Blaster.GetResumeBlasterDetail(Id);
        }

        public Guid ResumeBlasterDetail(ResumeBlasterDetailModel resumeBlasterDetailModel)
        {
            return Blaster.CreateResumeBlasterDetail(resumeBlasterDetailModel);
        }

        public void UpdateResumeBlasterDetail(ResumeBlasterDetailModel resumeBlasterDetailModel)
        {
            Blaster.UpdateResumeBlasterDetail(resumeBlasterDetailModel);
        }
    }
}
