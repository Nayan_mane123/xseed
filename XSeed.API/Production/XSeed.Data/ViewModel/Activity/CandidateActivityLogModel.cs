﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.User.CandidateUser;

namespace XSeed.Data.ViewModel.Activity
{
    public class CandidateActivityLogModel
    {
        static string url = ConfigurationManager.AppSettings["MongoURL"].ToString();
        static string dbName = ConfigurationManager.AppSettings["MongoDB"].ToString();
        static MongoClient client = new MongoClient(url);

        #region Property Declaration

        /* Id References */
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string CandidateId { get; set; }
        public string ActivityType { get; set; }
        public string Description { get; set; }

        /* Log */
        public List<ChangeLog> Activity { get; set; }
        public EmailLog EmailActivity { get; set; }

        /* Audit Trail Info */
        public Guid CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public string CreatedByEmail { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }

        #endregion

        #region Quick Notes

        /// <summary>
        /// Save quick notes related to Candidate
        /// </summary>
        /// <param name="note">Note</param>
        /// <param name="candidateId">Candidate Id</param>
        /// <returns>Notes</returns>
        internal static List<Note> SaveQuickNote(string note, string candidateId)
        {
            /* log note */
            Log(candidateId, "Note", note);

            return GetQuickNotes(candidateId);
        }

        /// <summary>
        /// Get Quick Notes based on candidate
        /// </summary>
        /// <param name="candidateId">Candidate Id</param>
        /// <returns>Notes</returns>
        public static List<Note> GetQuickNotes(string candidateId)
        {
            List<Note> notes = new List<Note>();

            var database = client.GetDatabase(dbName);

            var collection = database.GetCollection<CandidateActivityLogModel>("CandidateActivityLog");

            /* declare filter */
            var filter = Builders<CandidateActivityLogModel>.Filter.Where(s => s.ActivityType == "Note" && s.CandidateId == candidateId);

            /* apply filter */
            var documents = collection.Find(filter).Limit(10).SortByDescending(c => c.CreatedOn).ToList();

            /* format notes */
            foreach (var document in documents)
            {
                Note note = new Note();
                note.CreatedBy = OrganizationUserDetail.GetCreatedByUserName(document.CreatedBy);
                note.ProfileImageName = OrganizationUserDetail.GetCreatedByUserProfileImage(document.CreatedBy);
                note.CreatedByEmail = OrganizationUserDetail.GetCreatedByUserEmail(document.CreatedBy);
                note.CreatedOn = document.CreatedOn;
                note.Description = document.Description;
                notes.Add(note);
            }

            return notes;
        }

        #endregion

        #region  Activity & Quick Notes Log

        /// <summary>
        /// Get Activity Log related to Candidate
        /// </summary>
        /// <param name="candidateId">Candidate Id</param>
        /// <returns>Activity Logs</returns>
        internal static List<CandidateActivityLogModel> GetCandidateActivityLog(string candidateId)
        {
            List<CandidateActivityLogModel> notes = new List<CandidateActivityLogModel>();

            var database = client.GetDatabase(dbName);

            var collection = database.GetCollection<CandidateActivityLogModel>("CandidateActivityLog");

            /* declare filter */
            var filter = Builders<CandidateActivityLogModel>.Filter.Where(s => s.ActivityType != "Note" && s.CandidateId == candidateId);

            /* apply filter */
            var documents = collection.Find(filter).SortByDescending(c => c.CreatedOn).ToList();

            return documents;
        }

        /// <summary>
        /// Create activity log
        /// </summary>
        /// <param name="candidateId">Candidate Id</param>
        /// <param name="activityType">Activity Type</param>
        /// <param name="description">Description</param>
        /// <param name="activityLog">Activity Log</param>
        /// <param name="emailLog">Email Log</param>
        /// <returns>Activity Log Id</returns>
        internal static string Log(string candidateId, string activityType, string description, List<ChangeLog> activityLog = null, EmailLog emailLog = null)
        {
            if ((activityLog != null && activityLog.Count > 0) || emailLog != null || activityType == "Note")
            {
                var database = client.GetDatabase(dbName);
                var collection = database.GetCollection<CandidateActivityLogModel>("CandidateActivityLog");

                /* Populate model */
                CandidateActivityLogModel model = new CandidateActivityLogModel();
                model.CandidateId = candidateId;
                model.ActivityType = activityType;
                model.Description = description;
                model.Activity = activityLog;
                model.EmailActivity = emailLog;
                model.CreatedBy = BaseModel.InitiatedBy;
                model.CreatedByName = OrganizationUserDetail.GetCreatedByUserName(model.CreatedBy);
                model.CreatedByEmail = OrganizationUserDetail.GetCreatedByUserEmail(model.CreatedBy);
                model.CreatedOn = DateTime.Now;

                /* Save to db */
                collection.InsertOne(model);

                return model._id;
            }

            return string.Empty;
        }

        /// <summary>
        /// Track Activity Log
        /// </summary>
        /// <param name="candidateUserModel">Candidate User Model</param>
        /// <param name="EntityState">Entity State</param>
        /// <returns>Activity Log Id</returns>
        internal static string TrackActivityLog(CandidateUserModel candidateUserModel, string EntityState)
        {
            string candidateId = null;
            string description = string.Empty;

            List<ChangeLog> list = new List<ChangeLog>();

            /* get activity fields to log */
            List<string> activityFields = GetActivityFields();

            /* Get Properties */
            var PropertyNames = candidateUserModel.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public).ToDictionary(prop => prop.Name, prop => prop.GetValue(candidateUserModel, null) == null ? "" : prop.GetValue(candidateUserModel, null).ToString());

            /* set _Id */
            PropertyNames.TryGetValue("_id", out candidateId);

            if (EntityState == "Added")
            {
                foreach (var prop in PropertyNames)
                {
                    if (activityFields.Any(s => s == prop.Key) && !string.IsNullOrEmpty(prop.Value))
                    {
                        /* set current value */
                        list.Add(PopulateChangeLog("Candidate Detail", SetPropertyName(prop.Key), "", prop.Value));
                    }
                }

                /* set description */
                description = "Created New Candidate";
            }
            else if (EntityState == "Modified")
            {
                foreach (var prop in PropertyNames)
                {
                    if (activityFields.Any(s => s == prop.Key))
                    {
                        /* set old and new value */
                        string originalValue = GetPropertyOldValue(candidateId, prop.Key);

                        if (originalValue != prop.Value)
                        {
                            list.Add(PopulateChangeLog("Candidate Detail", SetPropertyName(prop.Key), originalValue, prop.Value));
                        }
                    }

                    /* set description */
                    description = "Updated Candidate";
                }
            }

            return Log(candidateId, "Action", description, list);
        }

        /// <summary>
        /// Get Property's Old Value
        /// </summary>
        /// <param name="candidateId">Candidate Id</param>
        /// <param name="property">Property</param>
        /// <returns>Old Value</returns>
        private static string GetPropertyOldValue(string candidateId, string property)
        {
            string propValue = string.Empty;

            CandidateUserModel model = CandidateUserModel.GetCandidateUserDetail(candidateId);

            /* Get Properties */
            var PropertyNames = model.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public).ToDictionary(prop => prop.Name, prop => prop.GetValue(model, null) == null ? "" : prop.GetValue(model, null).ToString());

            PropertyNames.TryGetValue(property, out propValue);

            return propValue;
        }

        /// <summary>
        /// Populate Chage Log model
        /// </summary>
        /// <param name="entityName">Entity Name</param>
        /// <param name="prop">property</param>
        /// <param name="oldValue">Old Value</param>
        /// <param name="currentValue">Current Value</param>
        /// <returns>Change Log Model</returns>
        private static ChangeLog PopulateChangeLog(string entityName, string prop, string oldValue, string currentValue)
        {
            /* fill log model */
            ChangeLog log = new ChangeLog()
            {
                EntityName = entityName,
                PropertyName = prop,
                OldValue = oldValue,
                NewValue = currentValue,
                DateChanged = DateTime.UtcNow
            };

            return log;
        }

        /// <summary>
        /// Log Email Notification related to candidate
        /// </summary>
        /// <param name="emailNotificationModel">Email Notification Model</param>
        /// <param name="candidateId">Candidate Id</param>
        public static void LogEmailActivity(Notification.EmailNotificationModel emailNotificationModel, string candidateId)
        {
            if (!string.IsNullOrEmpty(candidateId))
            {
                /* populate log model */
                EmailLog log = new EmailLog();
                log.EmailSubject = emailNotificationModel.MailSubject;
                log.EmailContent = emailNotificationModel.MailBody;
                log.To = emailNotificationModel.ToEmailID != null ? string.Join(",", emailNotificationModel.ToEmailID.Select(s => s)).TrimEnd(',') : string.Empty;
                log.Cc = emailNotificationModel.ToCC != null ? string.Join(",", emailNotificationModel.ToCC.Select(s => s)).TrimEnd(',') : string.Empty;
                log.Bcc = emailNotificationModel.ToBCC != null ? string.Join(",", emailNotificationModel.ToBCC.Select(s => s)).TrimEnd(',') : string.Empty;
                log.Attachments = emailNotificationModel.MailAttachment;

                /* Log Email */
                Log(candidateId, "Email", "Candidate Ëmail Detail", null, log);
            }
        }

        #endregion

        #region Utility Functions

        /// <summary>
        /// Get Activity field list to track changes of job detail.
        /// </summary>
        /// <returns>Activity Fields</returns>
        private static List<string> GetActivityFields()
        {
            List<string> list = new List<string>();
            list.Add("Status");
            list.Add("FirstName");
            list.Add("LastName");
            list.Add("PrimaryEmail");
            list.Add("SecondaryEmail");
            list.Add("Mobile");
            list.Add("Phone");
            list.Add("WorkPhone");
            list.Add("ProfileTitle");
            list.Add("TotalExperience");
            list.Add("USExperience");
            list.Add("CurrentSalary");
            list.Add("CurrentLocation");
            list.Add("PreferredLocation");
            list.Add("CompanyName");
            list.Add("Address1");
            list.Add("Address2");
            list.Add("Country");
            list.Add("State");
            list.Add("City");
            list.Add("Zip");

            return list;
        }

        /// <summary>
        /// Set valid property name 
        /// </summary>
        /// <param name="prop">property</param>
        /// <returns>Name</returns>
        private static string SetPropertyName(string prop)
        {
            string name = string.Empty;

            switch (prop)
            {
                case "FirstName":
                    name = "First Name";
                    break;
                case "LastName":
                    name = "Last Name";
                    break;
                case "PrimaryEmail":
                    name = "Primary Email";
                    break;
                case "SecondaryEmail":
                    name = "Secondary Email";
                    break;
                case "WorkPhone":
                    name = "Work Phone";
                    break;
                case "ProfileTitle":
                    name = "Job Title";
                    break;
                case "TotalExperience":
                    name = "Total Experience";
                    break;
                case "CurrentSalary":
                    name = "Current Salary";
                    break;
                case "CurrentLocation":
                    name = "Current Location";
                    break;
                case "PreferredLocation":
                    name = "Preferred Location";
                    break;
                case "CompanyName":
                    name = "Company Name";
                    break;
                case "USExperience":
                    name = "US Experience";
                    break;

                default:
                    name = prop;
                    break;
            }

            return name;
        }

        #endregion
    }
}
