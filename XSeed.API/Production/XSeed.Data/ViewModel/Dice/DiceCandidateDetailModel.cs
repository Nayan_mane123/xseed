﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Common;
using XSeed.MonsterJobPortalAPI.Model;

namespace XSeed.Data.ViewModel.Dice
{
    public class DiceCandidateSearchResultModel
    {
        #region Property Declaration

        public int page { get; set; }
        public int count { get; set; }
        public int total { get; set; }
        public int pageCount { get; set; }
        public List<Documents> documents { get; set; }
        public List<string> facets { get; set; }
        public List<Filters> filters { get; set; }
        public string trackingId { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Dice Token Info
        /// </summary>
        /// <returns>Portal Token Info</returns>
        public static PortalTokenInfo GetDiceTokenInfo()
        {
            string diceTokenUrl = ConfigurationManager.AppSettings["DiceTokenUrl"];
            string diceUserName = ConfigurationManager.AppSettings["DiceUserName"];
            string dicePassword = ConfigurationManager.AppSettings["DicePassword"];
            string DiceDeveloperIDandPasswordBase64 = ConfigurationManager.AppSettings["DiceDeveloperIDandPasswordBase64"];

            //https://secure.dice.com/oauth/token?grant_type=password&username=captain.marwah@tcognition.com&password=kemble70

            string diceTokenEndPointUrl = diceTokenUrl + "?grant_type=password&username=" + diceUserName + "&password=" + dicePassword;

            PortalTokenInfo diceTokenResponse = new PortalTokenInfo();

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(diceTokenEndPointUrl.ToString());
            request.ContentType = "application/x-www-form-urlencoded";
            request.Headers.Add("Authorization", "Basic " + DiceDeveloperIDandPasswordBase64);
            request.Method = "Get";
            var response = (HttpWebResponse)request.GetResponse();
            if (response != null && response.StatusCode == HttpStatusCode.OK)
            {
                var responseStream = response.GetResponseStream();
                string responseStr = new StreamReader(responseStream).ReadToEnd();
                diceTokenResponse = JsonConvert.DeserializeObject<PortalTokenInfo>(responseStr);
            }

            return diceTokenResponse;
        }

        /// <summary>
        /// Get Dice Search Candidate Result
        /// </summary>
        /// <param name="diceCandidateSearchModel">Monster Candidate Search</param>
        /// <param name="access_token">access_token</param>
        /// <returns>Dice Candidate Search Result Model</returns>
        internal static DiceCandidateSearchResultModel GetDiceSearchCandidateResult(DiceCandidateSearchModel diceCandidateSearchModel, string access_token)
        {
            string diceIntegratedProfilesSearchUrl = ConfigurationManager.AppSettings["DiceIntegratedProfilesSearchUrl"];
            string diceIntegratedProfilesUrl = diceIntegratedProfilesSearchUrl + BuildDiceSearchUrl(diceCandidateSearchModel);

            //diceIntegratedProfilesSearchUrl + "?searchType=0&q=java&country=US&location=US";
            DiceCandidateSearchResultModel diceCandidateList = new DiceCandidateSearchResultModel();
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(diceIntegratedProfilesUrl.ToString());
                //request.ContentType = "application/json";
                request.Headers.Add("Authorization", "bearer " + access_token);
                request.Method = "Get";
                var response = (HttpWebResponse)request.GetResponse();
                if (response != null && response.StatusCode == HttpStatusCode.OK)
                {
                    try
                    {
                        var responseStream = response.GetResponseStream();
                        string responseStr = new StreamReader(responseStream).ReadToEnd();
                        diceCandidateList = JsonConvert.DeserializeObject<DiceCandidateSearchResultModel>(responseStr);
                    }
                    catch (Exception ex)
                    {
                        var msg = ex.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }

            return diceCandidateList;
        }

        public static DiceProfile GetDiceCandidateProfile(string diceId, string access_token)
        {
            string diceProfileGetUrl = ConfigurationManager.AppSettings["DiceProfilesGetUrl"];
            string diceProfileGet = diceProfileGetUrl + diceId;

            DiceProfile diceCandidateDetails = new DiceProfile();
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(diceProfileGet.ToString());
                //request.ContentType = "application/json";
                request.Headers.Add("Authorization", "bearer " + access_token);
                request.Method = "Get";
                var response = (HttpWebResponse)request.GetResponse();
                if (response != null && response.StatusCode == HttpStatusCode.OK)
                {
                    try
                    {
                        var responseStream = response.GetResponseStream();
                        string responseStr = new StreamReader(responseStream).ReadToEnd();
                        diceCandidateDetails = JsonConvert.DeserializeObject<DiceProfile>(responseStr);
                    }
                    catch (Exception ex)
                    {
                        var msg = ex.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }

            return diceCandidateDetails;
        }

        /// <summary>
        /// Build Dice Search Url
        /// </summary>
        /// <param name="diceCandidateSearchModel">Monster Candidate Search</param>
        /// <returns>Dice Search Url</returns>
        internal static string BuildDiceSearchUrl(DiceCandidateSearchModel searchModel)
        {
            StringBuilder destUrl = new StringBuilder();

            #region Build Dice Search Query

            ////Country
            //if (!string.IsNullOrWhiteSpace(searchModel.Location))
            {
                destUrl.AppendFormat("&country={0}", "US");
            }
            ////----------------------

            ////Sort Order Defaults to relevancy sort.
            destUrl.AppendFormat("&sortBy={0}", "announceDate+desc");
            ////---------------------

            ////Page Size
            destUrl.AppendFormat("&count={0}", "100");
            ////---------------------

            ////Skills
            if (searchModel.Skills != null)
            {
                string query = string.Empty;

                foreach (var item in searchModel.Skills)
                {
                    if (searchModel.Skills.First() == item)
                    {
                        query = query + item;
                    }
                    else
                    {
                        query = query + "::" + item;
                    }
                }

                if (!string.IsNullOrWhiteSpace(query))
                {
                    destUrl.AppendFormat("&skills={0}", query);
                }
            }
            ////----------------------

            ////JobTitle
            if (!string.IsNullOrWhiteSpace(searchModel.Title))
            {
                searchModel.Title = searchModel.Title.Replace(" ", "+");
                destUrl.AppendFormat("&title={0}", searchModel.Title);
            }
            ////----------------------

            ////JobType
            if (!string.IsNullOrWhiteSpace(searchModel.Type))
            {
                if (searchModel.Type == "Permanent")
                {
                    destUrl.Append("&jobType=FULLTIME");
                }
                else if (searchModel.Type == "Temp/Contract")
                {
                    destUrl.Append("&jobType=CON::PARTTIME::CON_CORP::CON_IND::CON_W2::CON_HIRE_CORP::CON_HIRE_IND::CON_HIRE_W2");
                }
                else if (searchModel.Type == "Full Time")
                {
                    destUrl.Append("&jobType=FULLTIME");
                }
                else if (searchModel.Type == "Part Time")
                {
                    destUrl.Append("&jobType=PARTTIME");
                }
                else if (searchModel.Type == "Intern")
                {
                    destUrl.Append("&jobType=FULLTIME::PARTTIME");
                }
                else if (searchModel.Type == "Per Diem")
                {
                    destUrl.Append("&jobType=CON::PARTTIME::CON_CORP::CON_IND::CON_W2::CON_HIRE_CORP::CON_HIRE_IND::CON_HIRE_W2");
                }
                else if (searchModel.Type == "Seasonal")
                {
                    destUrl.Append("&jobType=CON::PARTTIME::CON_CORP::CON_IND::CON_W2::CON_HIRE_CORP::CON_HIRE_IND::CON_HIRE_W2");
                }
            }
            ////----------------------

            ////Experience Level
            if (!string.IsNullOrWhiteSpace(searchModel.MinExperience) && !string.IsNullOrWhiteSpace(searchModel.MaxExperience))
            {
                destUrl.AppendFormat("&yearsExp={0}", "[" + searchModel.MinExperience + " TO " + searchModel.MaxExperience + "]");
            }
            ////----------------------

            ////Location
            if (!string.IsNullOrWhiteSpace(searchModel.Location))
            {
                searchModel.Location = searchModel.Location.Replace(" ", "+");
                destUrl.AppendFormat("&location={0}", searchModel.Location.ToLower());
            }
            ////----------------------

            ////Zip
            if (!string.IsNullOrWhiteSpace(searchModel.LocationZIPCode))
            {
                destUrl.AppendFormat("&location={0}", searchModel.LocationZIPCode);
            }
            ////----------------------

            ////Multiple Locations
            if (searchModel.SearchLocations != null)
            {
                string locationList = string.Empty;
                foreach (string location in searchModel.SearchLocations)
                {
                    string formattedLocation = location.Replace(" ", "+");
                    if (searchModel.SearchLocations.First() == location)
                    {
                        locationList = locationList + formattedLocation;
                    }
                    else
                    {
                        locationList = locationList + "::" + formattedLocation;
                    }

                }
                if (!string.IsNullOrWhiteSpace(locationList))
                {
                    destUrl.AppendFormat("&location={0}", locationList);
                }
            }
            ////----------------------

            ////Salary Range with Supported Salary Type
            if (!string.IsNullOrWhiteSpace(searchModel.MinSalary) && !string.IsNullOrWhiteSpace(searchModel.MaxSalary))
            {
                destUrl.AppendFormat("&salary={0}", "-1");

                //for Hourly salary type
                if (!string.IsNullOrWhiteSpace(searchModel.MinSalary) && !string.IsNullOrWhiteSpace(searchModel.MaxSalary) && !string.IsNullOrWhiteSpace(searchModel.SalaryType) && searchModel.SalaryType.ToLower() == "hourly")
                {
                    destUrl.AppendFormat("&hourly={0}", "[" + searchModel.MinSalary + " TO " + searchModel.MaxSalary + "]");
                }
                //for Other salary types(it only consider as annually, but we added as default if salary type is not mentioned)
                else
                {
                    destUrl.AppendFormat("&salary={0}", "[" + searchModel.MinSalary + " TO " + searchModel.MaxSalary + "]");
                }
            }
            ////----------------------


            ////Days Ago
            if (!string.IsNullOrWhiteSpace(searchModel.ResumePostedTo))
            {
                destUrl.AppendFormat("&daysAgo={0}", searchModel.ResumePostedTo);
            }
            ////----------------------

            ////Radius for the location search (in miles)
            if (!string.IsNullOrWhiteSpace(searchModel.LocationMilesAway))
            {
                destUrl.AppendFormat("&distance={0}", searchModel.LocationMilesAway);
            }
            ////----------------------

            ////Boolean Search (keep it last)
            if (!string.IsNullOrWhiteSpace(searchModel.BooleanSearchQuery))
            {
                destUrl.AppendFormat("&q={0}", searchModel.BooleanSearchQuery);
            }
            ////----------------------


            #endregion Build Dice Search Query

            return destUrl.ToString();
        }

        #endregion
    }

    public class Documents
    {
        public string id { get; set; }
        public string openWebId { get; set; }
        public string peopleId { get; set; }
        public string profileName { get; set; }
        public string status { get; set; }
        public string desiredPosition { get; set; }
        public Nullable<DateTime> dateAvailable { get; set; }
        public Nullable<DateTime> announceDate { get; set; }
        public Nullable<DateTime> lastModified { get; set; }
        public Nullable<DateTime> datePosted { get; set; }
        public Nullable<DateTime> dateCreated { get; set; }
        public Nullable<bool> thirdParty { get; set; }
        public Resume resume { get; set; }
        public Contact contact { get; set; }
        public List<EmploymentHistoryList> employmentHistoryList { get; set; }
        public List<EducationList> educationList { get; set; }
        public List<CertificationList> certificationList { get; set; }
        public string jobClassificationList { get; set; }
        public List<SkillList> skillList { get; set; }
        public DesiredEmployment desiredEmployment { get; set; }
        public List<string> preferredLocationList { get; set; }
        public string yearsOfExperience { get; set; }
        public string willingToTravel { get; set; }
        public Nullable<bool> willingToRelocate { get; set; }
        public Nullable<bool> willingToTelecommute { get; set; }
        public List<LinkList> linkList { get; set; }
        public Nullable<bool> active { get; set; }
        public string personalSummary { get; set; }
        public string summaryText { get; set; }
        public List<string> tagList { get; set; }
        public Nullable<bool> parseResume { get; set; }
        public Nullable<bool> socialProfile { get; set; }
        public string completeness { get; set; }
        public string currency { get; set; }
        public string spokenLanguages { get; set; }
        public string blockedEmployers { get; set; }
        public Nullable<bool> createResume { get; set; }
        public string externalId { get; set; }
        public Nullable<bool> likelyToSwitch { get; set; }
        public string likelyToSwitchScore { get; set; }
        public string bio { get; set; }
        public string imageUrl { get; set; }
        public List<WebProfiles> webProfiles { get; set; }
        public string openWebScore { get; set; }
        public List<string> searchableCountries { get; set; }
        public List<Location> locations { get; set; }
        public string viewedTime { get; set; }
        public Nullable<bool> countOwsView { get; set; }
        public ProfileRating profileRating { get; set; }
        public string profileNote { get; set; }
        public Nullable<bool> openWebProfile { get; set; }
    }

    public class Contact
    {
        public string firstName { get; set; }
        public string middleInitial { get; set; }
        public string lastName { get; set; }
        public string formattedName { get; set; }
        public Location location { get; set; }
        public string email { get; set; }
        public string emails { get; set; }
        public string altAreaCode { get; set; }
        public string altPhoneNumber { get; set; }
        public string areaCode { get; set; }
        public string phoneNumber { get; set; }
        public string formattedPhoneNumber { get; set; }
        public string unformattedPhoneNumber { get; set; }
        public string faxAreaCode { get; set; }
        public string faxPhoneNumber { get; set; }
    }

    public class Location
    {
        public string addrOne { get; set; }
        public string addrTwo { get; set; }
        public string country { get; set; }
        public string countryCode { get; set; }
        public string municipality { get; set; }
        public string region { get; set; }
        public string postalCode { get; set; }
        public string formattedLocation { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string locationText { get; set; }
        public string origin { get; set; }
        public Methods methods { get; set; }
    }

    public class Methods
    {
        public Nullable<bool> phone { get; set; }
        public Nullable<bool> email { get; set; }
    }

    public class EmploymentHistoryList
    {
        public string jobTitle { get; set; }
        public string employerName { get; set; }
        public Nullable<bool> mostRecentEmployer { get; set; }
        public Location location { get; set; }
        public string startYear { get; set; }
        public string endYear { get; set; }
        public string description { get; set; }
        public string startMonth { get; set; }
        public string endMonth { get; set; }
    }

    public class EducationList
    {
        public string type { get; set; }
        public string description { get; set; }
        public string institution { get; set; }
        public Location location { get; set; }
    }

    public class CertificationList
    {
        public string description { get; set; }
        public string yearCertified { get; set; }
    }

    public class SkillList
    {
        public string name { get; set; }
        public string years { get; set; }
        public string lastUsed { get; set; }
        public string knowledgeLevel { get; set; }
        public string score { get; set; }
        public string type { get; set; }
        public string current { get; set; }
    }

    public class DesiredEmployment
    {
        public string type { get; set; }
        public string workAuthorization { get; set; }
        public Nullable<bool> securityClearance { get; set; }
        public Nullable<bool> veteranStatus { get; set; }
        public string minimumAnnualSalary { get; set; }
        public string annualSalary { get; set; }
        public string minimumHourlyPayRate { get; set; }
        public string hourlyPayRate { get; set; }
        public string yearsTechExperience { get; set; }
    }

    public class WebProfiles
    {
        public string network { get; set; }
        public string url { get; set; }
        public string icon { get; set; }
        public string iconSecure { get; set; }
        public string bigIcon { get; set; }
        public string bigIconSecure { get; set; }
    }

    public class LinkList
    {
        public string title { get; set; }
        public string url { get; set; }
    }

    public class ProfileRating
    {
        public string href { get; set; }
        public string id { get; set; }
        public string profileId { get; set; }
        public string rating { get; set; }
    }

    public class Filters
    {
        public string key { get; set; }
        public string value { get; set; }
    }

    public class Resume
    {
        public bool uploaded { get; set; }
        public string fileName { get; set; }
        public string resumeHref { get; set; }
        public string strippedResume { get; set; }
        public string resumeText { get; set; }
        public string resumeData { get; set; }
        public string resumeXml { get; set; }
        public bool generated { get; set; }
        public string modifiedDate { get; set; }
        public string createDate { get; set; }
    }

    public class DiceCandidateSearchModel
    {
        public string Title { get; set; }
        public string Type { get; set; }
        public string Location { get; set; }
        public string ExperienceLevel { get; set; }
        public string MinSalary { get; set; }
        public string MaxSalary { get; set; }
        public string SalaryType { get; set; }
        public string CurrencyType { get; set; }
        public List<string> Skills { get; set; }
        public string MinExperience { get; set; }
        public string MaxExperience { get; set; }
        public string BooleanSearchQuery { get; set; }
        public string ResumePostedFrom { get; set; }
        public string ResumePostedTo { get; set; }
        public string LocationMilesAway { get; set; }
        public string LocationZIPCode { get; set; }
        public List<string> SearchLocations { get; set; }
    }

    public class DiceProfile
    {
        public Documents integratedProfile { get; set; }
    }
}
