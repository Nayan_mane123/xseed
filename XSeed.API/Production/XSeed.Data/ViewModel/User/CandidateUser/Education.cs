﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XSeed.Data.ViewModel.User.CandidateUser
{
    public class Education
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string DegreeDate { get; set; }
        public string DegreeType { get; set; }
        public string School { get; set; }
        public string SchoolType { get; set; }
        public string Course { get; set; }
        public string Major { get; set; }
        public string City { get; set; }
        public Nullable<Guid> CityId { get; set; }
        public string State { get; set; }
        public Nullable<Guid> StateId { get; set; }
        public string Country { get; set; }
        public Nullable<Guid> CountryId { get; set; }
        public string UserId { get; set; }
    }

    public class Skills
    {
        public List<string> ProgrammingLanguages { get; set; }
        public List<string> SkillList { get; set; }
        public List<string> Languages { get; set; }
    }

    public class WorkExperience
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string CompanyName { get; set; }
        public string JobTitle { get; set; }
        public Nullable<decimal> Salary { get; set; }
        public string SalaryType { get; set; }
        public Nullable<decimal> AdditionalSalary { get; set; }
        public string AdditionalSalaryDescription { get; set; }
        public string EmployementType { get; set; }
        public string ChangeReason { get; set; }
        public Nullable<bool> IsCurrent { get; set; }
        public string City { get; set; }
        public Nullable<Guid> CityId { get; set; }
        public string State { get; set; }
        public Nullable<Guid> StateId { get; set; }
        public string Country { get; set; }
        public Nullable<Guid> CountryId { get; set; }
        public string UserId { get; set; }
    }

    public class ProjectDetail
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string ProjectTitle { get; set; }
        public string Role { get; set; }
        public string Description { get; set; }
        public string TeamSize { get; set; }
        public string Client { get; set; }
        public string SkillsUsed { get; set; }
    }

    public class Application
    {
        public string Company { get; set; }
        public Nullable<Guid> JobId { get; set; }
        public string JobStatus { get; set; }
        public string JobTitle { get; set; }
        public string Status { get; set; }
        public Nullable<DateTime> AppliedDate { get; set; }
    }

    public class CandidateJobSearchModel
    {
        public string JobTitle { get; set; }
        public List<string> Skills { get; set; }
        public Nullable<int> ExperienceInYear { get; set; }
        public Nullable<int> ExperienceInMonth { get; set; }
        public string Location { get; set; }
        public List<string> DegreeList { get; set; }
    }

}
