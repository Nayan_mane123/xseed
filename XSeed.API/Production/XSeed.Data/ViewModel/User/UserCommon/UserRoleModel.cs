﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using XSeed.Utility;

namespace XSeed.Data.ViewModel.User.UserCommon
{
    public class UserRoleModel
    {
        #region Property Declaration

        public Guid Id { get; set; }
        public Guid OrganizationId { get; set; }

        [Required]
        public string Role { get; set; }
        public Nullable<int> Precedence { get; set; }
        [Required]
        public string Description { get; set; }

        public List<AreaPermissionModel> AreaMapping { get; set; }
        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get all roles list
        /// </summary>        
        /// <returns>User role list</returns>
        public static List<UserRoleModel> GetAllUserRoles(Guid organizationId)
        {
            return UserRoleMaster.GetAllUserRoles(organizationId);
        }

        /// <summary>
        /// Get user role detail by Id
        /// </summary>
        /// <param name="Id">Role Id</param>
        /// <returns>user role detail </returns>
        public static UserRoleModel GetUserRoleById(Guid? Id)
        {
            return UserRoleMaster.GetUserRoleById(Id);
        }

        /// <summary>
        /// Get Organization Areas
        /// </summary>        
        /// <returns>List of Organization Areas</returns>
        public static List<LookUpModel> GetOrganizationAreas()
        {
            return UserRoleMaster.GetOrganizationAreas();
        }

        /// <summary>
        /// Get User types
        /// </summary>        
        /// <returns>List of Organization Areas</returns>
        public static List<LookUpModel> GetUserTypes()
        {
            return UserRoleMaster.GetUserTypes();
        }

        /// <summary>
        /// CreateUserRole
        /// </summary>
        /// <param name="userRoleModel">UserRoleModel</param>
        public static Guid CreateUserRole(UserRoleModel model)
        {
            return UserRoleMaster.CreateUserRole(model);
        }

        /// <summary>
        /// Update user role
        /// </summary>
        /// <param name="userRoleModel">User role model</param>
        /// <returns>Status Ok - 200</returns>
        public static void UpdateUserRole(UserRoleModel userRoleModel)
        {
            UserRoleMaster.UpdateUserRole(userRoleModel);
        }

        /// <summary>
        /// Delete user role
        /// </summary>
        /// <param name="userRoleId">User role Id</param>
        /// <returns>Status Ok - 200</returns>
        public static void DeleteUserRole(Guid userRoleId)
        {
            UserRoleMaster.DeleteUserRole(userRoleId);
        }

        public static string GetUserTypeByEmail(string userName)
        {
            return UserRoleMaster.GetUserTypeByEmail(userName);
        }

        #endregion

        
    }

    public class AreaPermissionModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Boolean? Create { get; set; }
        public Boolean? Update { get; set; }
        public Boolean? Read { get; set; }
        public Boolean? Delete { get; set; }
    }
}