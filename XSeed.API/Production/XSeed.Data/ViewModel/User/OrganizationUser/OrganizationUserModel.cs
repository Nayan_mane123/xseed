﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Registration;
using XSeed.Utility;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.User.UserCommon;
using XSeed.Data.ViewModel.Notification;

namespace XSeed.Data.ViewModel.User.OrganizationUser
{
    public class OrganizationUserModel
    {
        #region Property Declaration

        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Organization), ErrorMessageResourceName = "OrgnizationIdRequired")]
        public Guid OrganizationId { get; set; }
        public string OrganizationName { get; set; }

        public Guid RoleId { get; set; }
        public string RoleName { get; set; }

        public Nullable<Guid> TitleId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public Nullable<System.DateTime> BirthDate { get; set; }
        public Nullable<System.DateTime> AnniversaryDate { get; set; }
        public string ProfileImage { get; set; }
        public XSeedFileEntity ProfileImageFile { get; set; }
        public Nullable<Guid> ReportingTo { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "EmailRequired")]
        [RegularExpression(Constants.EmailExpression, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "EmailInvalid")]
        [DataType(DataType.EmailAddress)]
        public string PrimaryEmail { get; set; }

        [System.ComponentModel.DefaultValue(""), RegularExpression(Constants.EmailExpression, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "EmailInvalid")]
        [DataType(DataType.EmailAddress)]
        public string SecondaryEmail { get; set; }

        [System.ComponentModel.DefaultValue(""), RegularExpression(Constants.MobileExpression, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "MobileInvalid")]
        public string Mobile { get; set; }

        [System.ComponentModel.DefaultValue(""), RegularExpression(Constants.PhoneNoExpression, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "PhoneNoInvalid")]
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public Nullable<Guid> CountryId { get; set; }
        public string Country { get; set; }
        public Nullable<Guid> StateId { get; set; }
        public string State { get; set; }
        public Nullable<Guid> CityId { get; set; }
        public string City { get; set; }
        public bool? IsActive { get; set; }

        [System.ComponentModel.DefaultValue(""), RegularExpression(Constants.ZipExpression, ErrorMessageResourceType = typeof(Resources.Common), ErrorMessageResourceName = "ZipLength")]
        public string Zip { get; set; }

        public AuditTrail AuditTrailInfo { get; set; }

        public bool UpdateUserPermissions { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Organization User List
        /// </summary>        
        /// <param name="organizationId">Organization Id</param>
        /// <returns>Organization User List</returns>
        public static List<OrganizationUserModel> ListOrganizationUsers(Guid organizationId)
        {
            return (OrganizationUserDetail.ListOrganizationUsers(organizationId));
        }

        /// <summary>
        /// Get Organization User List by Role
        /// </summary>        
        /// <param name="organizationId">Role Id</param>
        /// <returns>Organization User List</returns>
        public static List<OrganizationUserModel> ListOrganizationUsersByRole(Guid roleId)
        {
            return (OrganizationUserDetail.ListOrganizationUsersByRole(roleId));
        }

        /// <summary>
        /// Get Organization User
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="organizationId">Organization Id</param>
        /// <returns>Organization User</returns>
        public static OrganizationUserModel GetUser(Guid? userId, Guid organizationId)
        {
            return (OrganizationUserDetail.GetUser(userId, organizationId));
        }

        /// <summary>
        /// Get Specific User Credentials
        /// </summary>
        /// <param name="userName">User Name</param>
        public static OrganizationUserModel GetUser(string userName)
        {
            return (OrganizationUserDetail.GetUser(userName));
        }

        /// <summary>
        /// Create Organization User
        /// </summary>
        /// <param name="organizationUserModel">Organization User Model</param>
        public static Guid CreateOrganizationUser(OrganizationUserModel organizationUserModel, Guid UserId, string password = "")
        {
            Guid organizationUserID = Guid.Empty;
            var userTypes = UserRoleModel.GetUserTypes();

            /* Populate Register Organization Model */
            RegisterOrganizationModel registerOrgnization = new RegisterOrganizationModel();
            registerOrgnization.UserType = userTypes.Where(x => x.Name.ToLower() == "Recruiter".ToLower()).Select(x => x.Id.Value).FirstOrDefault();
            registerOrgnization.UserName = organizationUserModel.PrimaryEmail;
            registerOrgnization.Password = password;

            /* Save User Login Info */
            organizationUserModel.UserId = UserLogin.SaveUserLoginInfo(registerOrgnization);

            /* Create Organization User */
            organizationUserID = OrganizationUserDetail.CreateOrganizationUser(organizationUserModel);

            /* Send welcome email to user */
            WelcomeMail(registerOrgnization.UserName, UserId, password);

            return organizationUserID;
        }

        /// <summary>
        /// Send Welcome email to User
        /// </summary>
        /// <param name="userName">User Name</param>
        /// <param name="password">Password</param>
        private static void WelcomeMail(string userName, Guid UserId, string password)
        {
            /* Send Password to Organization User */
            /* Mail functionality can be enabled or disabled by changing "IsSendMail" flag in web.config file*/
            bool isSendMail = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSendMail"]);

            if (isSendMail == true)
            {
                //To send email to created users with Admin email configuration
                EmailConfigurationDetail userEmailConfiguration = null;
                using (var db = new XSeedEntities())
                {
                    var orgnization = db.OrganizationUserDetails.FirstOrDefault(u => u.UserId == UserId);

                    var adminUser = db.OrganizationUserDetails.OrderBy(c => c.CreatedOn).FirstOrDefault(o => o.OrganizationId == orgnization.OrganizationId && o.UserRoleMaster.Role == "Administrator");
                    if (adminUser != null)
                    {
                        userEmailConfiguration = db.EmailConfigurationDetails.FirstOrDefault(u => u.UserId == adminUser.UserId);
                    }
                }

                EmailNotificationModel emailNotificationModel = new EmailNotificationModel();
                emailNotificationModel.ToEmailID = new List<string>();
                emailNotificationModel.ToEmailID.Add(userName);
                emailNotificationModel.FromEmailID = userEmailConfiguration == null ? ConfigurationManager.AppSettings["EmailFromUserName"] : userEmailConfiguration.Email;
                emailNotificationModel.FromEmailPassword = userEmailConfiguration == null ? ConfigurationManager.AppSettings["EmailFromPassword"] : PasswordUtility.DecryptPassword(userEmailConfiguration.Password);
                emailNotificationModel.TypeOfNotification = XSeed.Utility.Constants.typeOfNotificationUserPasswordMail;
                emailNotificationModel.ExtraData = password;
                emailNotificationModel.CallBackUrl = ConfigurationManager.AppSettings["LoginCallBackURL"];
                EmailNotificationModel.SendEmailNotification(emailNotificationModel);
            }
        }

        /// <summary>
        /// Update Organization User
        /// </summary>
        /// <param name="organizationUserModel">Organization User Model</param>
        public static void UpdateOrganizationUser(OrganizationUserModel organizationUserModel)
        {
            /* Update Organization User */
            OrganizationUserDetail.UpdateOrganizationUser(organizationUserModel);
        }

        /// <summary>
        /// Get User Email Configuration by Email address
        /// </summary>
        /// <param name="email">Email</param>
        /// <returns>Email Configuration</returns>
        public static string GetUserEmailConfiguration(string email)
        {
            string emailConfiguration = "";

            using (var db = new XSeedEntities())
            {
                var model = db.EmailConfigurationDetails.FirstOrDefault(u => u.Email == email);

                if (model != null)
                {
                    emailConfiguration = model.Password;
                }
            }

            return emailConfiguration;
        }

        #endregion
    }
}