﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;

namespace XSeed.Data.ViewModel.ResumeEmail
{
    public class ResumeEmailParserModel
    {
        #region Property Declaration

        public System.Guid Id { get; set; }
        public string Name { get; set; }
        public string SourceEmail { get; set; }
        public string Html { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string EmailUniqueId { get; set; }
        public Nullable<bool> IsRead { get; set; }
        public string Subject { get; set; }
        public List<ResumeEmailParserAttachment> EmailAttachment { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get Resume Parsed Email
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>
        /// <returns>List of All Parsed Resume Email</returns>
        public static List<ResumeEmailParserModel> ListAllResumeEmails(Guid organizationId, int pageSize, int pageNumber, string sortBy = "Date", string sortOrder = "desc")
        {
            return (ResumeEmailParser.ListAllResumeEmails(organizationId, pageSize, pageNumber, sortBy, sortOrder));
        }

        /// <summary>
        /// Get Resume Parsed Email
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="Id">Id</param>
        /// <returns>Single Parsed Resume Email</returns>
        public static ResumeEmailParserModel GetResumeEmailDetail(Guid organizationId, Guid? resumeId)
        {
            return (ResumeEmailParser.GetResumeEmailDetail(organizationId, resumeId));
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        public static PaginationModel GetPaginationInfo(Guid organizationId, int pageSize)
        {
            return ResumeEmailParser.GetPaginationInfo(organizationId, pageSize);
        }

        /// <summary>
        /// Update Resume Parsed Email
        /// </summary>
        /// <param name="resumeEmailParserModel">Resume Email Parser Model</param>
        /// <returns></returns>
        public static void UpdateResumeParsedEmail(Guid Id)
        {
            ResumeEmailParser.UpdateResumeParsedEmail(Id);
        }

        /// <summary>
        /// Get Unread Mail Count 
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <returns>Unread Mail Count</returns>
        public static int GetUnreadMailCount(Guid organizationId)
        {
            return ResumeEmailParser.GetUnreadMailCount(organizationId);
        }

        #endregion
    }
}
