﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.BooleanSearch;

namespace XSeed.Business.BooleanQuery
{
    public class BooleanQueryInfo
    {
        /// <summary>
        /// Save Boolean Query
        /// </summary>
        /// <param name="booleanQueryModel">BooleanQueryModel</param>
        /// <returns></returns>
        public static void SaveBooleanQuery(BooleanQueryModel booleanQueryModel)
        {
            BooleanQueryModel.SaveBooleanQuery(booleanQueryModel);
        }

        /// <summary>
        /// Get List of Boolean Queries
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <returns>List of Boolean Queries</returns>
        public static List<BooleanQueryModel> ListAllBooleanQueries(Guid organizationId)
        {
            return (BooleanQueryModel.ListAllBooleanQueries(organizationId));
        }
    }
}
