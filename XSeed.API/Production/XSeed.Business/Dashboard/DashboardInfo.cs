﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Dashboard;

namespace XSeed.Business.Dashboard
{
    public class DashboardInfo
    {
        /// <summary>
        /// Get Dashboard Detail
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="userId">user Id</param>
        /// <returns>Dashboard Model</returns>
        public DashboardModel GetDashboardDetail(Guid organizationId, Guid? userId)
        {
            return DashboardModel.GetDashboardDetail(organizationId, userId);
        }

        /// <summary>
        /// Get Candidate Chart Detail
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="userId">user Id</param>
        /// <returns>Candidate Chart Detail</returns>
        public List<CandidateChartModel> GetCandidateChartDetail(Guid organizationId, Guid? userId)
        {
            return DashboardModel.GetCandidateChartDetail(organizationId, userId);
        }

        /// <summary>
        /// Get Submission Chart Detail
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="userId">user Id</param>
        /// <returns>Submission Chart Detail</returns>
        public List<SubmissionChartModel> GetSubmissionChartDetail(Guid organizationId, Guid? userId)
        {
            return DashboardModel.GetSubmissionChartDetail(organizationId, userId);
        }

        /// <summary>
        /// Get Job Status Chart Detail
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="userId">user Id</param>
        /// <returns>Job Status Chart Detail</returns>
        public List<JobStatusChartModel> GetJobStatusChartDetail(Guid organizationId, Guid? userId)
        {
            return DashboardModel.GetJobStatusChartDetail(organizationId, userId);
        }

        /// <summary>
        /// Get job Submission Chart Chart Detail
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="userId">user Id</param>
        /// <returns>job Submission Chart Detail</returns>
        public List<JobSubmissionChartModel> GetjobSubmissionChartDetail(Guid organizationId, Guid? userId)
        {
            return DashboardModel.GetjobSubmissionChartDetail(organizationId, userId);
        }
    }
}
