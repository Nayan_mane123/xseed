﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Common;

namespace XSeed.Business.Common
{
    public class LookUp
    {
        #region User Defined Functions

        /// <summary>
        /// Get Country List
        /// </summary>
        /// <returns>Country List</returns>
        public static List<LookUpModel> GetCountries()
        {
            return LookUpModel.GetCountries();
        }

        /// <summary>
        /// Get State List by Country Id
        /// </summary>
        /// <param name="countryId">Country Id</param>
        /// <returns>State List</returns>
        public static List<LookUpModel> GetStates(Guid countryId)
        {
            return LookUpModel.GetStates(countryId);
        }

        /// <summary>
        /// Get City List by State Id
        /// </summary>
        /// <param name="stateId">State Id</param>
        /// <returns>State List</returns>
        public static List<LookUpModel> GetCities(Guid stateId)
        {
            return LookUpModel.GetCities(stateId);
        }

        /// <summary>
        /// Get Industry Type List
        /// </summary>        
        /// <returns>Industry Type List</returns>
        public static List<ChildLookUpModel> GetIndustryTypes()
        {
            return ChildLookUpModel.GetIndustryTypes();
        }

        /// <summary>
        /// Get User Roles List
        /// </summary>        
        /// <returns>User Roles List</returns>
        public static List<LookUpModel> GetUserRoles(Guid organizationId)
        {
            return LookUpModel.GetUserRoles(organizationId);
        }

        /// <summary>
        /// Get Company Types
        /// </summary>        
        /// <returns>Company Types</returns>
        public static List<LookUpModel> GetCompanyTypes()
        {
            return LookUpModel.GetCompanyTypes();
        }

        /// <summary>
        /// Get Company Sources
        /// </summary>        
        /// <returns>Company Sources</returns>
        public static List<LookUpModel> GetCompanySources(Guid organizationId)
        {
            return LookUpModel.GetCompanySources(organizationId);
        }

        /// <summary>
        /// Get Title/ Suffix List
        /// </summary>
        /// <returns>Title/ Suffix List</returns>
        public static List<LookUpModel> GetTitles()
        {
            return LookUpModel.GetTitles();
        }

        /// <summary>
        /// Get Reporting Authorities
        /// </summary>
        /// <returns>Reporting Authorities</returns>
        public static List<LookUpModel> GetReportingAuthorities(Guid organizationId, Guid? roleId = null)
        {
            return LookUpModel.GetReportingAuthorities(organizationId, roleId);
        }

        /// <summary>
        /// Get Job Status
        /// </summary>
        /// <returns>List of Job Status</returns>
        public static List<LookUpModel> GetJobStatus()
        {
            return LookUpModel.GetJobStatus();
        }

        /// <summary>
        /// Get Job Title
        /// </summary>
        /// <returns>List of Job Titles</returns>
        public static List<LookUpModel> GetJobTitles(Guid organizationId)
        {
            return LookUpModel.GetJobTitles(organizationId);
        }

        /// <summary>
        /// Get Job Type
        /// </summary>
        /// <returns>List of Job Types</returns>
        public static List<LookUpModel> GetJobTypes(Guid organizationId)
        {
            return LookUpModel.GetJobTypes(organizationId);
        }

        /// <summary>
        /// Get Skills
        /// </summary>
        /// <returns>List of Skills</returns>
        public static List<LookUpModel> GetSkills()
        {
            return LookUpModel.GetSkills();
        }

        /// <summary>
        /// Get Degrees
        /// </summary>
        /// <returns>List of Degrees</returns>
        public static List<LookUpModel> GetDegrees(Guid organizationId)
        {
            return LookUpModel.GetDegrees(organizationId);
        }

        /// <summary>
        /// Get Genders
        /// </summary>
        /// <returns>List of Genders</returns>
        public static List<LookUpModel> GetGenders()
        {
            return LookUpModel.GetGenders();
        }

        /// <summary>
        /// Get Marital Status
        /// </summary>
        /// <returns>List of Marital Status</returns>
        public static List<LookUpModel> GetMaritalStatus()
        {
            return LookUpModel.GetMaritalStatus();
        }

        /// <summary>
        /// Get Visa Types
        /// </summary>
        /// <returns>List of Visa Types</returns>
        public static List<LookUpModel> GetVisaTypes()
        {
            return LookUpModel.GetVisaTypes();
        }

        /// <summary>
        /// Get submission feedback status
        /// </summary>
        /// <returns>Submission feedback status list</returns>
        public static List<LookUpModel> GetSubmissionFeedbackStatus()
        {
            return LookUpModel.GetSubmissionFeedbackStatus();
        }

        /// <summary>
        /// Get client feedback Status
        /// </summary>
        /// <returns>List of client feedback Status</returns>
        public static List<LookUpModel> GetClientFeedbackStatus()
        {
            return LookUpModel.GetClientFeedbackStatus();
        }

        /// <summary>
        /// Get candidate feedback Status
        /// </summary>
        /// <returns>List of candidate feedback Status</returns>
        public static List<LookUpModel> GetCandidateFeedbackStatus()
        {
            return LookUpModel.GetCandidateFeedbackStatus();
        }

        /// <summary>
        /// Get company name for lookup
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <returns>Company names</returns>
        public static List<LookUpModel> GetCompanies(Guid organizationId)
        {
            return LookUpModel.GetCompanies(organizationId);
        }

        /// <summary>
        /// Get Organization Users
        /// </summary>        
        /// <param name="organizationId">Organization Id</param>
        /// <returns>Organization user list</returns>
        public static List<LookUpModel> GetOrganizationUsers(Guid organizationId)
        {
            return LookUpModel.GetOrganizationUsers(organizationId);
        }

        /// <summary>
        /// Get Company Contacts
        /// </summary>
        /// <param name="companyId">CompanyId</param>
        /// <returns>Company contact list</returns>
        public static List<LookUpModel> ListCompanyContacts(Guid companyId)
        {
            return LookUpModel.ListCompanyContacts(companyId);
        }

        /// <summary>
        /// Get Company Contact emails
        /// </summary>
        /// <param name="companyId">CompanyId</param>
        /// <returns>Company contact email list</returns>
        public static List<LookUpModel> ListSubmissionContactEmail(Guid companyId)
        {
            return LookUpModel.ListSubmissionContactEmail(companyId);
        }

        /// <summary>
        /// Get State code based on country 
        /// </summary>
        /// <param name="country">country</param>
        /// <param name="city">city</param>
        /// <returns>State Code</returns>
        public static Nullable<Guid> GetStateCode(string country, string city)
        {
            return LookUpModel.GetStateCode(country, city);
        }

        /// <summary>
        /// Get City code based on country 
        /// </summary>
        /// <param name="country">country</param>
        /// <param name="city">city</param>
        /// <returns>City Code</returns>
        public static Nullable<Guid> GetCityCode(string country, string city)
        {
            return LookUpModel.GetCityCode(country, city);
        }

        /// <summary>
        /// Get Business Units
        /// </summary>        
        /// <returns>Business Units</returns>
        public static List<LookUpModel> GetBusinessUnits(Guid organizationId)
        {
            return LookUpModel.GetBusinessUnits(organizationId);
        }

        /// <summary>
        /// Get candidate status
        /// </summary>        
        /// <remarks> Get candidate status 
        ///</remarks>
        /// <returns>List of candidate status</returns>
        public static List<LookUpModel> GetCandidateStatus()
        {
            return LookUpModel.GetCandidateStatus();
        }

        /// <summary>
        /// Get candidate categories
        /// </summary>        
        /// <remarks>Get candidate categories
        ///</remarks>        
        /// <returns>categories</returns>
        public static List<LookUpModel> GetCandidateCategories(Guid organizationId)
        {
            return LookUpModel.GetCandidateCategories(organizationId);
        }

        #endregion
        /// <summary>
        /// Update Job Titles
        /// </summary>
        /// <param name="lookUpModel"></param>
        /// <returns>Job Titles List</returns>
        public static void UpdateJobTitles(LookUpModel lookUpModel)
        {
            LookUpModel.UpdateJobTitles(lookUpModel);
        }

        /// <summary>
        /// Create Job Titles
        /// </summary>
        /// <param name="lookUpModel"></param>

        public static void CreateJobTitles(LookUpModel lookUpModel)
        {
            LookUpModel.CreateJobTitles(lookUpModel);
        }

        /// <summary>
        /// Delete Job Titles
        /// </summary>
        /// <param name="genericLookupId"></param>
        public static void DeleteJobTitles(Guid genericLookupId)
        {
            LookUpModel.DeleteJobTitles(genericLookupId);
        }

        /// <summary>
        /// Create Job Types
        /// </summary>
        /// <param name="lookUpModel"></param>
        public static void CreateJobTypes(LookUpModel lookUpModel)
        {
            LookUpModel.CreateJobTypes(lookUpModel);
        }

        /// <summary>
        /// Delete Job Types
        /// </summary>
        /// <param name="genericLookupId"></param>
        public static void DeleteJobTypes(Guid genericLookupId)
        {
            LookUpModel.DeleteJobTypes(genericLookupId);
        }

        /// <summary>
        /// Update Job Types
        /// </summary>
        /// <param name="lookUpModel"></param>
        public static void UpdateJobTypes(LookUpModel lookUpModel)
        {
            LookUpModel.UpdateJobTypes(lookUpModel);
        }

        /// <summary>
        /// Create Job Degrees
        /// </summary>
        /// <param name="lookUpModel"></param>
        public static void CreateJobDegrees(LookUpModel lookUpModel)
        {
            LookUpModel.CreateJobDegrees(lookUpModel);
        }

        /// <summary>
        /// Update Job Degrees
        /// </summary>
        /// <param name="lookUpModel"></param>
        public static void UpdateJobDegrees(LookUpModel lookUpModel)
        {
            LookUpModel.UpdateJobDegrees(lookUpModel);
        }

        /// <summary>
        /// Delete Job Degrees
        /// </summary>
        /// <param name="genericLookupId"></param>
        public static void DeleteJobDegrees(Guid genericLookupId)
        {
            LookUpModel.DeleteJobDegrees(genericLookupId);
        }

        /// <summary>
        /// Create New Candidate Status
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>        
        public static void CreateCandidateStatus(LookUpModel lookUpModel)
        {
            LookUpModel.SaveCandidateStatus(lookUpModel);
        }

        /// <summary>
        /// Update Candidate Status
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        public static void UpdateCandidateStatus(LookUpModel lookUpModel)
        {
            LookUpModel.SaveCandidateStatus(lookUpModel);
        }

        /// <summary>
        /// Deletes Candidate Status
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        public static void DeleteCandidateStatus(Guid genericLookupId)
        {
            LookUpModel.DeleteCandidateStatus(genericLookupId);
        }

        /// <summary>
        /// Create New Candidate Category
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>        
        public static void CreateCandidateCategory(LookUpModel lookUpModel)
        {
            LookUpModel.SaveCandidateCategory(lookUpModel);
        }

        /// <summary>
        /// Update Candidate Category
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        public static void UpdateCandidateCategory(LookUpModel lookUpModel)
        {
            LookUpModel.SaveCandidateCategory(lookUpModel);
        }

        /// <summary>
        /// Deletes Candidate Category
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        public static void DeleteCandidateCategory(Guid genericLookupId)
        {
            LookUpModel.DeleteCandidateCategory(genericLookupId);
        }

        /// <summary>
        /// Get candidate groups
        /// </summary>        
        /// <remarks>Get candidate groups
        ///</remarks>        
        /// <returns>Groups</returns>
        public static List<LookUpModel> GetCandidateGroups(Guid organizationId)
        {
            return LookUpModel.GetCandidateGroups(organizationId);
        }

        /// <summary>
        /// Create New Candidate Group
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>        
        public static void CreateCandidateGroup(LookUpModel lookUpModel)
        {
            LookUpModel.SaveCandidateGroup(lookUpModel);
        }

        /// <summary>
        /// Update Candidate Group
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        public static void UpdateCandidateGroup(LookUpModel lookUpModel)
        {
            LookUpModel.SaveCandidateGroup(lookUpModel);
        }

        /// <summary>
        /// Deletes Candidate Group
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        public static void DeleteCandidateGroup(Guid genericLookupId)
        {
            LookUpModel.DeleteCandidateGroup(genericLookupId);
        }


        /// <summary>
        /// Get candidate sources
        /// </summary>         
        /// <returns>Sources</returns>
        public static List<LookUpModel> GetCandidateSources(Guid organizationId)
        {
            return LookUpModel.GetCandidateSources(organizationId);
        }

        /// <summary>
        /// Create New Candidate Source
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>        
        public static void CreateCandidateSource(LookUpModel lookUpModel)
        {
            LookUpModel.SaveCandidateSource(lookUpModel);
        }

        /// <summary>
        /// Update Candidate Source
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        public static void UpdateCandidateSource(LookUpModel lookUpModel)
        {
            LookUpModel.SaveCandidateSource(lookUpModel);
        }

        /// <summary>
        /// Deletes Candidate Source
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        public static void DeleteCandidateSource(Guid genericLookupId)
        {
            LookUpModel.DeleteCandidateSource(genericLookupId);
        }

        /// <summary>
        /// Get candidate Languages
        /// </summary>         
        /// <returns>Languages</returns>
        public static List<LookUpModel> GetLanguages()
        {
            return LookUpModel.GetLanguages();
        }

        /// <summary>
        /// Create New Candidate Language
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>        
        public static void CreateLanguage(LookUpModel lookUpModel)
        {
            LookUpModel.SaveLanguage(lookUpModel);
        }

        /// <summary>
        /// Update Candidate Language
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        public static void UpdateLanguage(LookUpModel lookUpModel)
        {
            LookUpModel.SaveLanguage(lookUpModel);
        }

        /// <summary>
        /// Deletes Candidate Language
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        public static void DeleteLanguage(Guid genericLookupId)
        {
            LookUpModel.DeleteLanguage(genericLookupId);
        }

        /// <summary>
        /// Check for Duplicate Lookup Name
        /// </summary>
        /// <param name="lookupName"> lookup Name</param>
        /// <param name="lookupType"> lookup Type</param>
        /// <param name="organizationId"> organization Id</param>
        /// <returns>True/ False</returns>
        public bool IsDuplicateLookupName(string lookupName, string lookupType, Guid organizationId, bool editedValue, string existingValue = "")
        {
            LookUpModel model = new LookUpModel();
            return model.IsDuplicateLookupName(lookupName, lookupType, organizationId, editedValue, existingValue);
        }

        /// <summary>
        /// Get Salary Types
        /// </summary>         
        /// <returns>List of Salary Type</returns>
        public static List<LookUpModel> GetSalaryType()
        {
            return LookUpModel.GetSalaryType();
        }

        /// <summary>
        /// Get Currency Types
        /// </summary>         
        /// <returns>List of Currency Type</returns>
        public static List<LookUpModel> GetCurrencyType()
        {
            return LookUpModel.GetCurrencyType();
        }

        public static Guid CreateCity(string city, Guid stateId)
        {
            return LookUpModel.CreateCity(city, stateId);
        }
    }
}
