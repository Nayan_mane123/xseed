﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Business.Notification;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Notification;
using XSeed.Data.ViewModel.Submission;
using XSeed.Data.ViewModel.User.CandidateUser;
using System.Configuration;
using System.Net.Mail;
using System.IO;
using XSeed.Data.ViewModel.User.OrganizationUser;
using XSeed.Data.ViewModel.Activity;
using XSeed.Data.ViewModel.Job;

namespace XSeed.Business.Submission
{
    public class SubmissionInfo
    {

        /// <summary>
        /// Get Submissions
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <returns>List of Submissions</returns>
        public List<SubmissionModel> GetSubmissions(Guid organizationId)
        {
            SubmissionModel model = new SubmissionModel();
            return model.GetSubmissions(organizationId);
        }

        /// <summary>
        /// Get list of submission using server side pagination
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>        
        /// <returns>submission List</returns>
        public List<SubmissionModel> GetSubmissions(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", bool isExport = false)
        {
            SubmissionModel model = new SubmissionModel();
            return model.GetSubmissions(organizationId, pageSize, pageNumber, sortBy, sortOrder, isExport);
        }

        /// <summary>
        /// Get paritcular Submission Detail
        /// </summary>
        /// <param name="Id">Submission Id</param>
        /// <returns>Submission Detail</returns>
        public SubmissionModel GetSubmissionDetail(Guid organizationId, Guid? Id)
        {
            SubmissionModel model = new SubmissionModel();
            return model.GetSubmissionDetail(organizationId, Id);
        }

        /// <summary>
        /// Create New Submission
        /// </summary>
        /// <param name="SubmissionModel">Submission Detail Model </param>
        public void CreateSubmissionDetail(SubmissionModel submissionDetailModel, out List<string> successCandidateList, out string message)
        {
            submissionDetailModel.CreateSubmissionDetail(submissionDetailModel, out successCandidateList, out message);
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid organizationId, int pageSize)
        {
            return SubmissionModel.GetPaginationInfo(organizationId, pageSize);
        }

        /// <summary>
        /// Send Submission E-Mails
        /// </summary>
        /// <param name="submissionMailModel">Submission Mail Model</param>
        public void SendSubmissionMail(SubmissionMailModel submissionMailModel)
        {
            string fromEmail = string.Empty, fromPassword = string.Empty;
            List<XSeedFileEntity> mailAttachments = null;


            /* Get Candidate Resumes */
            var resumes = CandidateUserModel.GetCandidateResume(submissionMailModel.CandidateList);

            if (resumes.Count > 0)
            {
                /* attach resumes */
                mailAttachments = resumes;
            }

            /* Get from email & password */
            if (!string.IsNullOrEmpty(submissionMailModel.FromEmailID))
            {
                fromPassword = OrganizationUserModel.GetUserEmailConfiguration(submissionMailModel.FromEmailID);
                fromEmail = string.IsNullOrEmpty(fromPassword) ? ConfigurationManager.AppSettings["EmailFromUserName"] : submissionMailModel.FromEmailID;
                fromPassword = string.IsNullOrEmpty(fromPassword) ? ConfigurationManager.AppSettings["EmailFromPassword"] : fromPassword;
            }

            /* Populate model */
            EmailNotificationModel model = new EmailNotificationModel();

            model.ToEmailID = submissionMailModel.ToEmailID;
            model.ToCC = submissionMailModel.ToCC;
            model.ToBCC = submissionMailModel.ToBCC;
            model.FromEmailID = fromEmail;
            model.FromEmailPassword = fromPassword;
            model.FromDisplayName = submissionMailModel.FromDisplayName;
            model.MailBody = submissionMailModel.MailBody;
            model.TypeOfNotification = XSeed.Utility.Constants.typeOfNotificationEmailSubmissionAttachment;

            /* Add external attachments */
            if (submissionMailModel.MailAttachment != null && submissionMailModel.MailAttachment.Count() > 0)
            {
                mailAttachments = mailAttachments == null ? new List<XSeedFileEntity>() : mailAttachments;

                foreach (var attachment in submissionMailModel.MailAttachment)
                {
                    mailAttachments.Add(attachment);
                }
            }

            model.MailAttachment = mailAttachments;

            /* Send email */
            EmailNotificationModel.SendEmailNotification(model);
        }

        /// <summary>
        /// Save Quick Note related to Job
        /// </summary>
        /// <param name="note">Note</param>
        /// <param name="submissionId">Submission Id</param>
        /// <returns>Notes</returns>
        public List<Note> SaveQuickNote(string note, Guid submissionId)
        {
            return SubmissionModel.SaveQuickNote(note, submissionId);
        }

        /// <summary>
        /// Get Quick Notes related to submission
        /// </summary>
        /// <param name="submissionId">Submission Id</param>
        /// <returns>Quick Notes</returns>
        public List<Note> GetQuickNotes(Guid submissionId)
        {
            return SubmissionModel.GetQuickNotes(submissionId);
        }

        /// <summary>
        /// Submit sourced candidates against selected Job.
        /// </summary>
        /// <param name="candidateSourceResultModel">Candidate Source Result Model</param>
        /// <returns>Candidates</returns>
        public List<MongoLookUpModel> SubmitSourcedCandidates(List<CandidateSourceResultModel> candidateSourceResultModelList)
        {
            return SubmissionModel.SubmitSourcedCandidates(candidateSourceResultModelList);
        }
    }
}
