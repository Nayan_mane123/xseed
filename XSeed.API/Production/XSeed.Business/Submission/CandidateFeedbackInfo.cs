﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Submission;

namespace XSeed.Data.Submission
{
    public class CandidateFeedbackInfo
    {
        /// <summary>
        /// Get candidate feedback list for particular submission
        /// </summary>
        /// <param name="submissionId">Submission Id</param>
        /// <returns>List of Candidate feedback</returns>
        public List<CandidateFeedbackModel> GetCandidateFeedbackList(Guid submissionId)
        {
            CandidateFeedbackModel model = new CandidateFeedbackModel();
            return model.GetCandidateFeedbackList(submissionId);
        }

        /// <summary>
        /// Add Candidate Feedback
        /// </summary>
        /// <param name="candidateFeedbackModel">Candidate Feedback Model </param>
        public void AddCandidateFeedback(CandidateFeedbackModel candidateFeedbackModel)
        {
            CandidateFeedbackModel.AddCandidateFeedback(candidateFeedbackModel);
        }
    }
}
