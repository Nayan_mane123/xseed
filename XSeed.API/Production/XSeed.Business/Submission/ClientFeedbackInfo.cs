﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Submission;

namespace XSeed.Business.Submission
{
    public class ClientFeedbackInfo
    {
        /// <summary>
        /// Get client feedback list for particular submission
        /// </summary>
        /// <param name="submissionId">Submission Id</param>
        /// <returns>List of client feedback</returns>
        public List<ClientFeedbackModel> GetClientFeedbackList(Guid submissionId)
        {
            ClientFeedbackModel model = new ClientFeedbackModel();
            return model.GetClientFeedbackList(submissionId);
        }

        /// <summary>
        /// Add Client Feedback
        /// </summary>
        /// <param name="clientFeedbackModel">Client Feedback Model </param>
        public void AddClientFeedback(ClientFeedbackModel clientFeedbackModel)
        {
            clientFeedbackModel.AddClientFeedback(clientFeedbackModel);
        }
    }
}
