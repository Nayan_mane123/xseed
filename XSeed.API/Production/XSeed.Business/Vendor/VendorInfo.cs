﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Vendors;

namespace XSeed.Business.Vendor
{
    public class VendorInfo
    {
        /// <summary>
        /// Get List of All Vendors
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <returns>List of All Vendors</returns>
        public List<VendorDetailModel> ListAllVendors(Guid organizationId)
        {
            return (VendorDetailModel.ListAllVendors(organizationId));
        }

        /// <summary>
        /// Get Job Details
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns>JobDetailModel</returns>
        public VendorDetailModel GetVendorDetail(Guid organizationId, Guid? vendorId)
        {
            return (VendorDetailModel.GetVendorDetail(organizationId, vendorId));
        }

        /// <summary>
        /// Save Vendor Detail
        /// </summary>
        /// <param name="vendorDetailModel">Vendor Detail Model</param>
        /// <returns>VendorId</returns>
        public Guid CreateVendor(VendorDetailModel vendorDetailModel)
        {
            return (VendorDetailModel.CreateVendor(vendorDetailModel));
        }

        /// <summary>
        /// Update Vendor Details
        /// </summary>
        /// <param name="vendorDetailModel">Vendor Detail Model</param>
        /// <returns></returns>
        public void UpdateVendor(VendorDetailModel vendorDetailModel)
        {
            vendorDetailModel.UpdateVendor(vendorDetailModel);
        }

        /// <summary>
        /// Get Vendor Parsed Email
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>
        /// <returns>List of All Parsed Vendor Email</returns>
        public List<VendorEmailParserModel> ListAllParsedVendorEmail(Guid organizationId, int pageSize, int pageNumber, string sortBy = "Date", string sortOrder = "desc")
        {
            return (VendorEmailParserModel.ListAllParsedVendorEmail(organizationId, pageSize, pageNumber, sortBy, sortOrder));
        }

        /// <summary>
        /// Get Vendor Parsed Email
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="Id">Id</param>
        /// <returns>Single Parsed Vendor Email</returns>
        public object GetVendorParsedEmail(Guid organizationId, Guid? Id)
        {
            return (VendorEmailParserModel.GetVendorParsedEmail(organizationId, Id));
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>
        /// <param name="organizationId">organization Id</param>
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid organizationId, int pageSize)
        {
            return VendorEmailParserModel.GetPaginationInfo(organizationId, pageSize);
        }

        /// <summary>
        /// Update Vendor Parsed Email
        /// </summary>
        /// <param name="vendorEmailParserModel">Vendor Email Parser Model</param>
        /// <returns></returns>
        public void UpdateVendorParsedEmail(VendorEmailParserModel vendorEmailParserModel)
        {
            VendorEmailParserModel.UpdateVendorParsedEmail(vendorEmailParserModel);
        }

        /// <summary>
        /// Get Unread Mail Count 
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <returns>Unread Mail Count</returns>
        public int GetUnreadMailCount(Guid organizationId)
        {
            return VendorEmailParserModel.GetUnreadMailCount(organizationId);
        }
    }
}
