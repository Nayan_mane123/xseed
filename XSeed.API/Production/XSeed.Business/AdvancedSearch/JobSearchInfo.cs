﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.AdvancedSearch;
using XSeed.Data.ViewModel.Job;

namespace XSeed.Business.AdvancedSearch
{
    public class JobSearchInfo
    {
        /// <summary>
        /// Job Advanced Search API
        /// </summary>
        /// <param name="jobSearchModel">Job Search Model</param>
        /// <param name="pageSize">page Size</param>
        /// <param name="pageNumber">page Number</param>
        /// <param name="TotalCount">Total Count</param>
        /// <param name="TotalPages">Total Pages</param>
        /// <returns>List<JobDetailModel></returns>
        public List<JobDetailModel> SearchJobs(JobSearchModel jobSearchModel, int pageSize, int pageNumber, out int TotalCount, out double TotalPages)
        {
            return jobSearchModel.SearchJobs(jobSearchModel, pageSize, pageNumber, out TotalCount, out TotalPages);
        }

        /// <summary>
        /// Job Advanced Search API
        /// </summary>
        /// <param name="jobAdvancedSearchModel">job Search Model</param>        
        /// <param name="pageSize">Page Size</param>
        /// <param name="pageNumber">Page Number</param>
        /// <param name="sortBy">Sort By</param>
        /// <param name="sortOrder">Sort Order</param>
        /// <returns>Job List</returns>
        public List<JobDetailModel> SearchJobs(JobAdvancedSearchModel jobAdvancedSearchModel, int pageSize, int pageNumber, string sortBy, string sortOrder, out int TotalCount, out double TotalPages, bool isExport = false)
        {
            return jobAdvancedSearchModel.SearchJobs(jobAdvancedSearchModel, sortBy, sortOrder, pageSize, pageNumber, out TotalCount, out TotalPages, isExport);
        }
    }
}
