﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Activity;
using XSeed.Data.ViewModel.Notification;

namespace XSeed.Business.Notification
{
    public class NotificationInfo
    {
        #region User Defined Functions

        /// <summary>
        /// Sends Email Notification
        /// </summary>
        /// <param name="emailNotificationModel">EmailNotificationModel</param>
        public static void SendEmailNotification(EmailNotificationModel model)
        {
            EmailNotificationModel.SendEmailNotification(model);
        }

        /// <summary>
        /// Sends Email Notification related to job
        /// </summary>
        /// <param name="emailNotificationModel">EmailNotificationModel</param>
        /// /// <param name="jobId">Job Id</param>
        /// <returns>Status Ok - 200</returns>
        public static void SendJobEmailNotification(EmailNotificationModel emailNotificationModel, Nullable<Guid> jobId)
        {
            if (EmailNotificationModel.SendEmailNotification(emailNotificationModel))
            {
                /*Log email detail */
                JobActivityLogModel.LogEmailActivity(emailNotificationModel, jobId);
            }
        }

        /// <summary>
        /// Sends Email Notification related to candidate
        /// </summary>
        /// <param name="emailNotificationModel">EmailNotificationModel</param>
        /// /// <param name="candidateId">Candidate Id</param>
        /// <returns>Status Ok - 200</returns>
        public static void SendCandidateEmailNotification(EmailNotificationModel emailNotificationModel, string candidateId)
        {
            if (EmailNotificationModel.SendEmailNotification(emailNotificationModel))
            {
                /*Log email detail */
                CandidateActivityLogModel.LogEmailActivity(emailNotificationModel, candidateId);
            }
        }

        #endregion

    }
}
