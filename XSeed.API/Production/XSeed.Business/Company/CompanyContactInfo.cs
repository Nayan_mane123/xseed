﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Company;
using XSeed.Utility;

namespace XSeed.Business.Company
{
    public class CompanyContactInfo
    {
        /// <summary>
        /// Get list of contacts belongs to company
        /// </summary>
        /// <param name="companyId">Company Id</param>
        /// <returns>Company Contact Model List</returns>
        public List<CompanyContactModel> ListCompanyContacts(Guid companyId)
        {
            CompanyContactModel model = new CompanyContactModel();
            return model.ListCompanyContacts(companyId);
        }

        /// <summary>
        /// Get particular contact
        /// </summary>
        /// <param name="companyId">Company Id</param>
        /// <param name="contactId">Contact Id</param>
        /// <returns>Company Contact Model</returns>
        public object GetCompanyContactModel(Guid? Id, Guid companyId)
        {
            CompanyContactModel model = new CompanyContactModel();
            return model.GetCompanyContactModel(Id, companyId);
        }

        /// <summary>
        /// Create New Company Contact
        /// </summary>
        /// <param name="companyContactModel">Company Contact Model </param>
        public Guid CreateCompanyContact(CompanyContactModel companyContactModel)
        {
            if (companyContactModel.ProfileImageFile != null)
            {
                /* Save Profile Image */
                companyContactModel.ProfileImage = XSeedFileEntity.SaveProfileImage(companyContactModel.ProfileImageFile, "CompanyContact");
            }

            /* Create Contact */
           return companyContactModel.CreateCompanyContact(companyContactModel);
        }

        /// <summary>
        /// Update Company Contact
        /// </summary>
        /// <param name="companyContactModel">Company Contact Model </param>
        public void UpdateCompanyContact(CompanyContactModel companyContactModel)
        {
            if (companyContactModel.ProfileImageFile != null)
            {
                /* Save Profile Image */
                companyContactModel.ProfileImage = XSeedFileEntity.SaveProfileImage(companyContactModel.ProfileImageFile, "CompanyContact");
            }

            /* Update Contact */
            companyContactModel.UpdateCompanyContact(companyContactModel);
        }
    }
}
