﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Company;
using XSeed.Utility;

namespace XSeed.Business.Company
{
    public class CompanyInfo
    {
        #region User Defined Functions

        /// <summary>
        /// Get list of companies belongs to an organization
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <returns>Company Detail Model List</returns>
        public List<CompanyDetailModel> ListAllCompanies(Guid OrganizationId)
        {
            CompanyDetailModel model = new CompanyDetailModel();
            return model.ListAllCompanies(OrganizationId);
        }

        /// <summary>
        /// Get Company Detail
        /// </summary>        
        /// <param name="companyId">Company Id</param>
        /// <returns>Company Detail Model</returns>
        public CompanyDetailModel GetCompanyDetail(Guid organizationId, Guid? companyId)
        {
            CompanyDetailModel model = new CompanyDetailModel();
            return model.GetCompanyDetail(organizationId, companyId);
        }

        /// <summary>
        /// Create New Company Detail
        /// </summary>
        /// <param name="companyDetailModel">Company Detail Model </param>
        public Guid CreateCompanyDetail(CompanyDetailModel companyDetailModel)
        {
            if (companyDetailModel.ProfileImageFile != null)
            {
                /* Save Profile Image */
                companyDetailModel.ProfileImage = XSeedFileEntity.SaveProfileImage(companyDetailModel.ProfileImageFile, "Company");
            }

            /* Create Company */
            return companyDetailModel.CreateCompanyDetail(companyDetailModel);
        }

        /// <summary>
        /// Update New Company Detail
        /// </summary>
        /// <param name="companyDetailModel">Company Detail Model </param>
        public void UpdateCompanyDetail(CompanyDetailModel companyDetailModel)
        {
            if (companyDetailModel.ProfileImageFile != null)
            {
                /* Save Profile Image */
                companyDetailModel.ProfileImage = XSeedFileEntity.SaveProfileImage(companyDetailModel.ProfileImageFile, "Company");
            }

            /* Update Company */
            companyDetailModel.UpdateCompanyDetail(companyDetailModel);
        }

        /// <summary>
        /// Create New Company Source
        /// </summary>
        /// <param name="lookUpModel">LookUpModel </param>
        public Guid CreateCompanySource(LookUpModel lookUpModel)
        {
            return CompanyDetailModel.CreateCompanySource(lookUpModel);
        }

        /// <summary>
        /// Update Company Source
        /// </summary>
        /// <param name="lookUpModel">LookUpModel</param>
        /// <param name="organizationId"></param>
        public static void UpdateCompanySource(LookUpModel lookUpModel)
        {
            CompanyDetailModel.UpdateCompanySource(lookUpModel);
        }

        /// <summary>
        /// Delete company source
        /// </summary>
        /// <param name="genericLookupId"></param>
        public static void DeleteCompanySource(Guid genericLookupId)
        {
            CompanyDetailModel.DeleteCompanySource(genericLookupId);
        }

        /// <summary>
        /// Search Company Information.
        /// </summary>
        /// <param name="searchText">searchText</param>
        /// <returns>List<CompanyDataModel></returns>
        public List<CompanyDataModel> SearchCompanyData(string searchText)
        {
            return CompanyDataModel.SearchCompanyData(searchText);
        }

        #endregion



    }
}
