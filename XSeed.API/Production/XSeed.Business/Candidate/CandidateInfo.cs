﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Candidate;
using XSeed.Data.ViewModel.Common;

namespace XSeed.Business.Candidate
{
    public class CandidateInfo
    {
        #region User Defined Functions

        /// <summary>
        /// Get list of candidates
        /// </summary>        
        /// <returns>Candidate Detail Model List</returns>
        public List<CandidateDetailModel> ListCandidates()
        {
            CandidateDetailModel model = new CandidateDetailModel();
            return model.ListCandidates();
        }

        /// <summary>
        /// Get list of candidates using server side pagination
        /// </summary>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>
        /// <param name="orderBy">orderBy</param>
        /// <returns>Candidate List</returns>
        public List<CandidateDetailModel> ListCandidates(int pageSize, int pageNumber)
        {
            CandidateDetailModel model = new CandidateDetailModel();
            return model.ListCandidates(pageSize, pageNumber);
        }

        /// <summary>
        /// Get particular candidate
        /// </summary>
        /// <param name="Id">Candidate Id</param>        
        /// <returns>Candidate Contact Model</returns>
        public CandidateDetailModel GetCandidateDetail(Guid? Id)
        {
            CandidateDetailModel model = new CandidateDetailModel();
            return model.GetCandidateDetail(Id);
        }

        /// <summary>
        /// Create New Candidate
        /// </summary>
        /// <param name="candidateDetailModel">Candidate Detail Model </param>
        public Guid CreateCandidateDetail(CandidateDetailModel candidateDetailModel)
        {
            if (candidateDetailModel.ProfileImageFile != null)
            {
                /* Save Profile Image */
                candidateDetailModel.ProfileImage = XSeedFileEntity.SaveProfileImage(candidateDetailModel.ProfileImageFile, "Candidate");
            }

            return candidateDetailModel.CreateCandidateDetail(candidateDetailModel);
        }

        /// <summary>
        /// Update Candidate Detail
        /// </summary>
        /// <param name="candidateDetailModel">Candidate Detail Model </param>
        public void UpdateCandidateDetail(CandidateDetailModel candidateDetailModel)
        {
            if (candidateDetailModel.ProfileImageFile != null)
            {
                /* Save Profile Image */
                candidateDetailModel.ProfileImage = XSeedFileEntity.SaveProfileImage(candidateDetailModel.ProfileImageFile, "Candidate");
            }

            candidateDetailModel.UpdateCandidateDetail(candidateDetailModel);
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>        
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(int pageSize)
        {
            return CandidateDetailModel.GetPaginationInfo(pageSize);
        }

        #endregion
    }
}
