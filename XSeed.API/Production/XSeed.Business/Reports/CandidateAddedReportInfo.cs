﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Reports;

namespace XSeed.Business.Reports
{
    public class CandidateAddedReportInfo
    {
        /// <summary>
        /// Get Candidate Added by recruiters Report
        /// </summary>
        /// <returns>Candidate added report</returns>
        public List<CandidateAddedReportModel> GetCandidateAddedReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", int daysBefore = -7, bool isExport = false)
        {
            CandidateAddedReportModel model = new CandidateAddedReportModel();
            return model.GetCandidateAddedReport(organizationId, pageSize, pageNumber, sortBy, sortOrder, daysBefore, isExport);
        }

        /// <summary>
        /// Get Pagination Info
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid organizationId, int pageSize,int daysBefore = -7)
        {
            CandidateAddedReportModel model = new CandidateAddedReportModel();
            return model.GetPaginationInfo(organizationId, pageSize, daysBefore);
        }
    }
}
