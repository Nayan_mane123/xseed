﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Reports;

namespace XSeed.Business.Reports
{
    public class ActivityReportInfo
    {
        /// <summary>
        /// Get Activity Report
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Activity Report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        public List<ActivityReportModel> GetActivityReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", int daysBefore = -7, bool isExport = false)
        {
            ActivityReportModel model = new ActivityReportModel();
            return model.GetActivityReport(organizationId, pageSize, pageNumber, sortBy, sortOrder, daysBefore, isExport);
        }

        /// <summary>
        /// Get Pagination Info
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid organizationId, int pageSize,int daysBefore = -7)
        {
            ActivityReportModel model = new ActivityReportModel();
            return model.GetPaginationInfo(organizationId, pageSize, daysBefore);
        }
    }
}
