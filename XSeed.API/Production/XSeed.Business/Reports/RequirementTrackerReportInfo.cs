﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Reports;

namespace XSeed.Business.Reports
{
    public class RequirementTrackerReportInfo
    {
        /// <summary>
        /// Get Requirement Tracker Report
        /// </summary>
        /// <returns>Requirement tracker report</returns>
        public List<RequirementTrackerReportModel> GetRequirementTrackerReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", int daysBefore = -7, bool isExport = false)
        {
            RequirementTrackerReportModel model = new RequirementTrackerReportModel();
            return model.GetRequirementTrackerReport(organizationId, pageSize, pageNumber, sortBy, sortOrder, daysBefore, isExport);
        }

        /// <summary>
        /// Get Pagination Info
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid organizationId, int pageSize, int daysBefore = -7)
        {
            RequirementTrackerReportModel model = new RequirementTrackerReportModel();
            return model.GetPaginationInfo(organizationId, pageSize, daysBefore);
        }
    }
}
