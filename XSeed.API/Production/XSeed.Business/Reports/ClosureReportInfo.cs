﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Reports;

namespace XSeed.Business.Reports
{
    public class ClosureReportInfo
    {
        /// <summary>
        /// Get Closure Report
        /// </summary>
        /// <returns>Closure report</returns>
        public List<ClosureReportModel> GetClosureReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", int daysBefore = -7, bool isExport = false)
        {
            ClosureReportModel model = new ClosureReportModel();
            return model.GetClosureReport(organizationId, pageSize, pageNumber, sortBy, sortOrder, daysBefore, isExport);
        }

        /// <summary>
        /// Get Pagination Info
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid organizationId, int pageSize, int daysBefore = -7)
        {
            ClosureReportModel model = new ClosureReportModel();
            return model.GetPaginationInfo(organizationId, pageSize, daysBefore);
        }
    }
}