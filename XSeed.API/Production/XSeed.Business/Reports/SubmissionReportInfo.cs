﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Reports;

namespace XSeed.Business.Reports
{
    public class SubmissionReportInfo
    {
        /// <summary>
        /// Get Submission Report
        /// </summary>
        /// <returns>Submission report</returns>
        public List<SubmissionReportModel> GetSubmissionTrackerReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", int daysBefore = -7, bool isExport = false)
        {
            SubmissionReportModel model = new SubmissionReportModel();
            return model.GetSubmissionTrackerReport(organizationId, pageSize, pageNumber, sortBy, sortOrder, daysBefore, isExport);
        }

        /// <summary>
        /// Get Pagination Info
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid organizationId, int pageSize, int daysBefore = -7)
        {
            SubmissionReportModel model = new SubmissionReportModel();
            return model.GetPaginationInfo(organizationId, pageSize, daysBefore);
        }
    }
}