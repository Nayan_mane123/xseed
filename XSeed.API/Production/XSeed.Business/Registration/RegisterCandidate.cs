﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Account;
using XSeed.Data.ViewModel.Registration;
using XSeed.Data.ViewModel.User.CandidateUser;
using XSeed.Data.ViewModel.User.UserCommon;

namespace XSeed.Business.Registration
{
    public class RegisterCandidate
    {
        #region User Defined Functions

        /// <summary>
        /// function to Save Registration Info
        /// </summary>
        //public void SaveRegistrationInfo(RegisterCandidateModel model)
        //{

        //    var userTypes = UserRoleModel.GetUserTypes();

        //    /* Update Model with default values */
        //    model.UserType = userTypes.Where(x => x.Name.ToLower() == "Candidate".ToLower()).Select(x => x.Id.Value).FirstOrDefault();

        //    /* Save Registration Info */
        //    model.SaveRegistrationInfo(model);
        //}

        public void SaveRegistrationInfo(RegisterCandidateModel model)
        {
            /* Save Registration Info */
            model.SaveRegistrationInfo(model);
        }

        #endregion

        public RegisterCandidateModel CandidateAlreadyExists(RegisterCandidateModel model)
        {
            return model.CandidateAlreadyExists(model);
        }

        //public bool SocialCandidateAlreadyExists(RegisterCandidateModel model)
        //{
        //    return model.SocialCandidateAlreadyExists(model);
        //}

        public bool HasExternalUserRegistered(string loginProvider, string providerKey)
        {
            RegisterCandidateModel model = new RegisterCandidateModel();
            return model.HasExternalUserRegistered(loginProvider, providerKey);
        }

        /// <summary>
        /// Candidate Login
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="password">password</param>
        /// <returns>User Detail</returns>
        public object CandidateLogin(string username, string password)
        {
            RegisterCandidateModel model = new RegisterCandidateModel();
            return model.CandidateLogin(username, password);
        }

        public bool IsAuthenticated(string userId, string token)
        {
            RegisterCandidateModel model = new RegisterCandidateModel();
            return model.IsAuthenticated(userId, token);
        }

        /// <summary>
        /// Verify Candidate Email
        /// </summary>
        /// <param name="userId">userId</param>
        /// <param name="code">code</param>
        /// <returns>Email Verification Status - true/false</returns>
        public bool VerifyCandidateEmail(string userId, string code)
        {
            RegisterCandidateModel model = new RegisterCandidateModel();
            return model.VerifyCandidateEmail(userId, code);
        }

        /// <summary>
        /// Update Registration Info
        /// </summary>
        /// <param name="model">RegisterCandidateModel</param>
        public void UpdateRegistrationInfo(RegisterCandidateModel model)
        {
            /* Save Registration Info */
            model.UpdateRegistrationInfo(model);
        }

        /// <summary>
        /// Resend Candidate Verification Email
        /// </summary>
        /// <param name="UserName">UserName</param>
        /// <returns>Status Ok - 200</returns>
        public void ResendCandidateVerificationEmail(string UserName)
        {
            RegisterCandidateModel model = new RegisterCandidateModel();
            model.ResendCandidateVerificationEmail(UserName);
        }

        /// <summary>
        /// Is Candidate Valid
        /// </summary>
        /// <param name="userId">userId</param>
        /// <param name="code">code</param>
        /// <returns>RegisterCandidateModel</returns>
        public RegisterCandidateModel IsCandidateValid(string userId, string code)
        {
            RegisterCandidateModel model = new RegisterCandidateModel();
            return model.IsCandidateValid(userId, code);
        }
    }
}
