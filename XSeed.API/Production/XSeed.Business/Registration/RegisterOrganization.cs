﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Registration;
using XSeed.Data.ViewModel.User.UserCommon;
using XSeed.Utility;

namespace XSeed.Business.Registration
{
    public class RegisterOrganization
    {
        #region User Defined Functions

        /// <summary>
        /// function to Save Registration Info
        /// </summary>
        public void SaveRegistrationInfo(RegisterOrganizationModel model, List<UserRoleModel> userRoleModelList)
        {
            var userTypes = UserRoleModel.GetUserTypes();

            /* Update Model with default values */
            model.UserType = userTypes.Where(x => x.Name.ToLower() == "Recruiter".ToLower()).Select(x => x.Id.Value).FirstOrDefault();

            /* Save Registration Info */
            model.SaveRegistrationInfo(model, userRoleModelList);
        }

        /// <summary>
        /// function to check if organization already exists
        /// </summary>
        public bool IsOrganizationAlreadyExist(RegisterOrganizationModel model)
        {            
            /* Save Registration Info */
            return model.IsOrganizationAlreadyExist(model);
        }

        #endregion
    }
}