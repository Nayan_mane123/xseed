﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.Activity;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Job;
using XSeed.Data.ViewModel.User.CandidateUser;
using XSeed.Utility;

namespace XSeed.Business.User.CandidateUserInfo
{
    public class CandidateUserInfo
    {
        #region Candidate User

        /// <summary>
        /// Get list of candidate users
        /// </summary>
        /// <remarks>Get candidate user list</remarks>
        public List<CandidateUserModel> ListCandidateUsers()
        {
            return CandidateUserModel.ListCandidateUsers();
        }

        /// <summary>
        /// Get single candidate user
        /// </summary>        
        /// <param name="Id">Id</param>
        /// <remarks>Get candidate user</remarks>
        public CandidateUserModel GetCandidateUserDetail(string Id)
        {
            return CandidateUserModel.GetCandidateUserDetail(Id);
        }

        /// <summary>
        /// Get single candidate user by email
        /// </summary>        
        /// <param name="email">email</param>
        /// <remarks>Get candidate user</remarks>
        public IDictionary<string, string> GetCandidateUser(string userName)
        {
            return CandidateUserModel.GetLoginCandidateUserDetail(userName);
        }

        /// <summary>
        /// Create new candidate user
        /// </summary>        
        /// <param name="candidateUserModel">candidate User Model</param>
        /// <remarks>Create candidate user</remarks>
        public string CreateCandidateUserDetail(CandidateUserModel candidateUserModel)
        {
            if (candidateUserModel.ProfileImageFile != null)
            {
                candidateUserModel.ProfileImagePath = FileUtility.RenameFile(candidateUserModel.ProfileImageFile.name);
            }

            if (candidateUserModel.ResumeList != null && candidateUserModel.ResumeList.Count > 0)
            {
                /* Get Resume File */
                var resume = candidateUserModel.ResumeList.FirstOrDefault();

                /* Save Resume File */
                resume.filePath = XSeedFileEntity.SaveResumeFile(resume, "Candidate");
            }

            if (candidateUserModel.VideoResume != null)
            {
                if (candidateUserModel.isResumeCreated)
                {
                    candidateUserModel.VideoResume.name = candidateUserModel.FirstName + ".mp4";
                    candidateUserModel.VideoResume.type = "video/mp4";
                }
                candidateUserModel.VideoResume.filePath = XSeedFileEntity.SaveResumeFile(candidateUserModel.VideoResume, "CandidateVideo");
            }

            return candidateUserModel.CreateCandidateUserDetail(candidateUserModel);
        }

        /// <summary>
        /// Update single candidate user
        /// </summary>        
        /// <param name="candidateUserModel">Candidate User Model</param>
        /// <remarks> UpdateCandidateUserDetail</remarks>
        public void UpdateCandidateUserDetail(CandidateUserModel candidateUserModel)
        {
            /* check access permission for update */
            bool hasUpdatePermission = candidateUserModel.checkUserUpdatePermission(candidateUserModel);

            if (!hasUpdatePermission)
            {
                throw new Exception("Candidate is already registered, can not update information!!!");
            }

            if (candidateUserModel.ProfileImageFile != null)
            {
                candidateUserModel.ProfileImagePath = FileUtility.RenameFile(candidateUserModel.ProfileImageFile.name);
            }           

            if (candidateUserModel.VideoResume != null)
            {                
                if (candidateUserModel.VideoResume.data != null)
                {
                    
                    //if (candidateUserModel.VideoResume.data != null && (candidateUserModel.VideoResume.name.Contains("mp4") || candidateUserModel.VideoResume.name.Contains("webm")))
                    if(candidateUserModel.isResumeCreated)
                    {
                        candidateUserModel.VideoResume.name = candidateUserModel.FirstName + ".mp4";
                        candidateUserModel.VideoResume.type = "video/mp4";
                    }
                    candidateUserModel.VideoResume.filePath = XSeedFileEntity.SaveResumeFile(candidateUserModel.VideoResume, "CandidateVideo");

                    if (!string.IsNullOrEmpty(candidateUserModel.VideoResume.filePath) && candidateUserModel.VideoResume.data != null)
                    {
                        candidateUserModel.VideoResume.data = null;
                    }
                }
            }

            /* Save Candidate Resume */
            if (candidateUserModel.ResumeList != null && candidateUserModel.ResumeList.Count > 0)
            {
                /* Get Resume File */
                var resume = candidateUserModel.ResumeList.OrderByDescending(r => r.lastModifiedDate).FirstOrDefault();

                if (!candidateUserModel.IsResumeExists(candidateUserModel._id, resume))
                {
                    /* Save Resume File */
                    resume.filePath = XSeedFileEntity.SaveResumeFile(resume, "Candidate");
                }

            }

            candidateUserModel.UpdateCandidateUserDetail(candidateUserModel);
        }

        /// <summary>
        /// Get Job openings related to candidate skills
        /// </summary>
        /// <param name="candidateId">Candidate Id</param>
        /// <returns>Job Openings</returns>
        public List<JobDetailModel> ListJobOpenings(string candidateId, int recordCount = 10)
        {
            return CandidateUserModel.ListJobOpenings(candidateId, recordCount);
        }

        /// <summary>
        /// Get Job Detail for candidate
        /// </summary>
        /// <param name="Id">Job Id</param>
        /// <returns>Job Detail</returns>
        public JobDetailModel GetJobDetail(Guid? Id)
        {
            return CandidateUserModel.GetJobDetail(Id);
        }

        /// <summary>
        /// Add Candidate job application
        /// </summary>
        /// <param name="applicationModel">Application Model</param>
        /// <param name="Id">Candidate UserId</param>
        public void Apply(Application applicationModel, string Id)
        {
            CandidateUserModel.Apply(applicationModel, Id);
        }

        /// <summary>
        /// Get candidate job applications 
        /// </summary>
        /// <param name="Id">user Id</param>
        /// <returns>List of job applications</returns>
        public List<Application> GetCandidateJobApplications(string Id)
        {
            return CandidateUserModel.GetCandidateJobApplications(Id);
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>        
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(int pageSize)
        {
            return CandidateUserModel.GetPaginationInfo(pageSize);
        }

        /// <summary>
        /// Get list of candidates using server side pagination
        /// </summary>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>
        /// <param name="orderBy">orderBy</param>
        /// <returns>Candidate List</returns>
        public List<CandidateUserModel> ListCandidateUsers(int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", bool isExport = false)
        {
            return CandidateUserModel.ListCandidateUsers(pageSize, pageNumber, sortBy, sortOrder, isExport);
        }

        /// <summary>
        /// Parse candidate resume
        /// </summary>
        /// <param name="resume">XSeedFileEntity model</param>
        /// <param name="Id">Candidate Id</param>
        /// <returns>Candidate User model</returns>
        public CandidateUserModel ParseResume(XSeedFileEntity resume, string Id)
        {
            if (resume != null)
            {
                /* Save Resume File */
                string fileName = XSeedFileEntity.SaveResumeFile(resume, "ParseResume");

                /* Get Parsed Data */
                CandidateUserModel model = CandidateUserModel.ParseResume(fileName);

                /* Delete Resume File */
                XSeedFileEntity.DeleteResumeFile(fileName);

                return model;
            }

            return new CandidateUserModel();
        }

        /// <summary>
        /// Get Candidates application by Job Id
        /// </summary>
        /// <param name="JobId">JobId</param>
        /// <returns>List<CandidateUserModel></returns>
        public List<CandidateUserModel> GetCandidateByJob(Guid? jobId)
        {
            return CandidateUserModel.GetCandidateByJob(jobId);
        }

        /// <summary>
        /// Update Application Status
        /// </summary>        
        /// <param name="applicationModel">Application Model</param>        
        /// <param name="Id">Candidate UserId</param>
        public void UpdateApplicationStatus(Application applicationModel, string Id)
        {
            CandidateUserModel.UpdateApplicationStatus(applicationModel, Id);
        }

        /// <summary>
        /// Social Login
        /// </summary>
        /// <param name="uniqueId">uniqueId</param>
        /// <param name="provider">provider</param>
        /// <returns>object</returns>
        public object SocialLogin(string uniqueId, string provider)
        {
            return CandidateUserModel.SocialLogin(uniqueId, provider);
        }

        /// <summary>
        /// Save quick notes related to candidate
        /// </summary>
        /// <param name="note">Note</param>
        /// <param name="candidateId">Candidate Id</param>
        /// <returns>Notes</returns>
        public List<Note> SaveQuickNote(string note, string candidateId)
        {
            return CandidateUserModel.SaveQuickNote(note, candidateId);
        }

        /// <summary>
        /// Get Quick Notes related to candidate
        /// </summary>
        /// <param name="candidateId">Candidate Id</param>
        /// <returns>Quick Notes</returns>
        public List<Note> GetQuickNotes(string candidateId)
        {
            return CandidateUserModel.GetQuickNotes(candidateId);
        }

        /// <summary>
        /// Get Activity Log related to Candidate
        /// </summary>
        /// <param name="candidateId">Candidate Id</param>
        /// <returns>Activity Logs</returns>
        public List<CandidateActivityLogModel> GetCandidateActivityLog(string candidateId)
        {
            return CandidateUserModel.GetCandidateActivityLog(candidateId);
        }

        /// <summary>
        /// Get Candidate Id
        /// </summary>        
        /// <returns>Candidate Id</returns>
        public string GetCandidateId()
        {
            return CandidateUserModel.GetCandidateId();
        }

        /// <summary>
        /// Check for Duplicate Candidate Id
        /// </summary>        
        /// <param name="candidateId">Candidate Id</param>
        /// <returns>True/ False</returns>
        public bool IsDuplicateCandidateId(string requisitionId)
        {
            return CandidateUserModel.IsDuplicateCandidateId(requisitionId);
        }

        /// <summary>
        /// Get candidate resume file path
        /// </summary>        
        /// <returns>Candidate Id</returns>
        public string ViewCandidateResume(string candidateId)
        {
            return CandidateUserModel.ViewCandidateResume(candidateId);
        }

        /// <summary>
        /// View Monster candidate detail
        /// </summary>        
        /// <returns>Monster candidate detail</returns>
        public CandidateUserModel ViewMonsterCandidateDetail(string resumeId, string candidateId = null)
        {
            return CandidateUserModel.ViewMonsterCandidateDetail(resumeId, candidateId);
        }

        /// <summary>
        /// View Dice Candidate Detail
        /// </summary>
        /// <param name="diceId">dice Id</param>
        /// <param name="candidateId">candidate Id</param>
        /// <returns>Dice Candidate Detail</returns>
        /// <returns></returns>
        public CandidateUserModel ViewDiceCandidateDetail(string diceId, string candidateId)
        {
            return CandidateUserModel.ViewDiceCandidateDetail(diceId, candidateId);
        }

        /// <summary>
        /// Search Job openings to candidate
        /// </summary>
        /// <param name="searchModel">Candidate Job Search Model</param>
        /// <param name="recordCount">Record Count</param>
        /// <returns>Job List</returns>
        public List<JobDetailModel> SearchJobOpenings(CandidateJobSearchModel searchModel, int recordCount)
        {
            return CandidateUserModel.SearchJobOpenings(searchModel, recordCount);
        }

        #endregion

        #region Candidate Log [Google Tag]

        /// <summary>
        /// Get list candidate logs
        /// </summary>        
        /// <returns>Candidate Log</returns>
        public List<CandidateLog> ListCandidateLogs()
        {
            return CandidateLog.ListCandidateLogs();
        }

        /// <summary>
        /// Get UnProcessed Passive Candidates
        /// </summary>
        /// <returns>Passive Candidates</returns>
        public List<PassiveSearchCandidateModel> GetUnprocessedPassiveCandidates()
        {
            return PassiveSearchCandidateModel.GetUnprocessedPassiveCandidates();
        }

        /// <summary>
        /// Get single candidate log
        /// </summary>
        /// <param name="Id"> Candidate Log Id</param>
        /// <returns>Candidate Log</returns>
        public CandidateLog GetCandidateLogDetail(string Id)
        {
            return CandidateLog.GetCandidateLogDetail(Id);
        }

        /// <summary>
        /// Update Candidate Log Detail
        /// </summary>
        /// <param name="candidateLogModel">Candidate Log Model</param>
        public void PutCandidateLogDetail(CandidateLog candidateLogModel)
        {
            CandidateLog.PutCandidateLogDetail(candidateLogModel);
        }

        /// <summary>
        /// Post Candidate Log Detail
        /// </summary>
        /// <param name="candidateLogModel">Candidate Log Model</param>
        public void PostCandidateLogDetail(CandidateLog candidateLogModel)
        {
            CandidateLog.PostCandidateLogDetail(candidateLogModel);
        }

        /// <summary>
        /// Update Passive Searched Candidate Status
        /// </summary>
        /// <param name="Id">Id</param> 
        public void UpdatePassiveSearchedCandidateStatus(PassiveSearchCandidateModel model, bool status)
        {
            PassiveSearchCandidateModel.UpdatePassiveSearchedCandidateStatus(model, status);
        }

        #endregion


    }
}
