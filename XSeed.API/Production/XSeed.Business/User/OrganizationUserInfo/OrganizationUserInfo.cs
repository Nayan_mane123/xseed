﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.User.OrganizationUser;
using XSeed.Utility;

namespace XSeed.Business.User.OrganizationUserInfo
{
    public class OrganizationUserInfo
    {
        #region User Defined Functions

        /// <summary>
        /// Get List of Organization Users
        /// </summary>
        /// <param name="organizationId">Organization Id</param>
        /// <returns>List of Organization Users</returns>
        public List<OrganizationUserModel> ListOrganizationUsers(Guid organizationId)
        {
            return (OrganizationUserModel.ListOrganizationUsers(organizationId));
        }

        /// <summary>
        /// Get Specific User Details From Organization 
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="organizationId">Organization Id</param>
        /// <returns>Organization User</returns>
        public OrganizationUserModel GetUser(Guid? userId, Guid organizationId)
        {
            return (OrganizationUserModel.GetUser(userId, organizationId));
        }

        /// <summary>
        /// Get Specific User Credentials
        /// </summary>
        /// <param name="userName">User Name</param>
        public OrganizationUserModel GetUser(string userName)
        {
            return (OrganizationUserModel.GetUser(userName));
        }

        /// <summary>
        /// Create Organization User
        /// </summary>
        /// <param name="OrganizationUserModel">Organization User Model</param>
        public Guid CreateOrganizationUser(OrganizationUserModel organizationUserModel, Guid UserId, string password = "")
        {
            if (organizationUserModel.ProfileImageFile != null)
            {
                /* Save Profile Image */
                organizationUserModel.ProfileImage = XSeedFileEntity.SaveProfileImage(organizationUserModel.ProfileImageFile, "User");
            }

            /* Create Organization User */
            return OrganizationUserModel.CreateOrganizationUser(organizationUserModel, UserId, password);
        }

        /// <summary>
        /// Update Organization User
        /// </summary>
        /// <param name="OrganizationUserModel">Organization User Model</param>
        public void UpdateOrganizationUser(OrganizationUserModel organizationUserModel)
        {
            if (organizationUserModel.ProfileImageFile != null)
            {
                /* Save Profile Image */
                organizationUserModel.ProfileImage = XSeedFileEntity.SaveProfileImage(organizationUserModel.ProfileImageFile, "User");
            }

            /* Update Organization User */
            OrganizationUserModel.UpdateOrganizationUser(organizationUserModel);
        }

        /// <summary>
        /// Enable Disable User
        /// </summary>
        /// <param name="userId">userId</param>
        /// <param name="oraganizationId">OrganizationId</param>
        /// <param name="isActive">Activate/Deactivate</param>
        public bool EnableDisableUser(Guid userId, Guid oraganizationId, bool isActive)
        {
            return OrganizationUserDetail.EnableDisableUser(userId, oraganizationId, isActive);
        }

        /// <summary>
        /// TransferRequirements to other user
        /// </summary>
        /// <param name="userId">userId</param>
        /// <param name="transeredUser">TransferToUserId</param>
        public void TransferRequirements(Guid userId, Guid transeredUser)
        {
            OrganizationUserDetail.TransferRequirements(userId, transeredUser);
        }

        /// <summary>
        /// Check Valid User
        /// </summary>
        /// <param name="userId">UserId</param>
        /// <returns>Bool</returns>
        public static bool? CheckValidUser(Guid userId)
        {
            return OrganizationUserDetail.CheckValidUser(userId);
        }

        #endregion
    }
}
