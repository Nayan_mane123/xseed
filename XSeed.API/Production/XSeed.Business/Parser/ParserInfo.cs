﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Job;
using XSeed.Data.ViewModel.Parser;

namespace XSeed.Business.Parser
{
    public class ParserInfo
    {
        /// <summary>
        /// Get list of unparsed email
        /// </summary>        
        /// <returns>Unparsed Email list</returns>
        public List<EmailParser> GetParsedJob(Guid OrganizationUserId)
        {
            return ParserModel.GetParsedJob(OrganizationUserId);
        }


        /// <summary>
        /// Get list of Parsed jobs using server side pagination
        /// </summary>
        /// <param name="OrganizationUserId"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <returns>Parsed job List</returns>

        public List<ParserModel> ListAllParsedJobs(Guid OrganizationUserId, int pageSize, int pageNumber)
        {
            return ParserModel.ListAllParsedJobs(OrganizationUserId, pageSize, pageNumber);
        }

        /// <summary>
        /// Get list of accepted parsed job
        /// </summary>        
        /// <returns>Accepted parsed job list</returns>
        public List<ParserModel> GetAcceptedParsedJob(Guid OrganizationUserId)
        {
            return ParserModel.GetAcceptedParsedJob(OrganizationUserId);
        }

        /// <summary>
        /// Accept Parsed Job and add to Job Detail
        /// </summary>
        /// <param name="Id">Id</param>
        /// <param name="OrganizationUserId">Organization User Id</param>
        /// <param name="isReviewed">IsReviewed flag</param>
        public void AcceptParsedJob(Guid Id, Guid JobId, Guid OrganizationUserId, bool isReviewed)
        {
            ParserModel.AcceptParsedJob(Id, JobId, OrganizationUserId, isReviewed);
        }

        /// <summary>
        /// Mark Parsed Job as rejected
        /// </summary>
        /// <param name="Id">Id</param> 
        public void RejectParsedJob(Guid Id)
        {
            ParserModel.RejectParsedJob(Id);
        }

        /// <summary>
        /// Get Parsed Job Detail information for review.
        /// </summary>
        /// <param name="Id">Id</param>
        /// <param name="organizationUserId">Organization User Id</param>
        /// <returns>Job Detail Model</returns>
        public JobDetailModel ReviewParsedJob(Guid Id, Guid organizationUserId)
        {
            return ParserModel.ReviewParsedJob(Id, organizationUserId);
        }

        /// <summary>
        /// Get Pagination Info as Total Record Count and Total Pages.
        /// </summary>
        /// <param name="organizationId">organization user Id</param>
        /// <param name="pageSize">page Size</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(Guid OrganizationUserId, int pageSize)
        {
            return ParserModel.GetPaginationInfo(OrganizationUserId, pageSize);
        }

        /// <summary>
        /// Get Parsed Job List Count
        /// </summary>
        /// <param name="OrganizationUserId">Organization UserId</param>
        /// <returns>Parsed Job List Count</returns>
        public int GetParsedJobListCount(Guid OrganizationUserId)
        {
            return ParserModel.GetParsedJobListCount(OrganizationUserId);
        }
    }
}
