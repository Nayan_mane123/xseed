﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSeed.Data.ViewModel.APILogger;
using XSeed.Data.ViewModel.Common;

namespace XSeed.Business.APILogger
{
    public class ApiLogInfo
    {
        /// <summary>
        /// Save ApiLogEntry
        /// </summary>
        /// <param name="apiLogEntry">ApiLogEntry</param>
        public static void SaveApiLogEntry(ApiLogEntry apiLogEntry)
        {
            ApiLogEntry.SaveApiLogEntry(apiLogEntry);
        }

        /// <summary>
        /// List of ApiLog
        /// </summary>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>
        /// <returns>List of ApiLog</returns>
        public List<ApiLogEntry> ListApiLog(int pageSize, int pageNumber)
        {
            return ApiLogEntry.ListApiLog(pageSize, pageNumber);
        }

        /// <summary>
        /// Get Pagination Info
        /// </summary>
        /// <param name="pageSize">pageSize</param>
        /// <returns>Pagination Model</returns>
        public PaginationModel GetPaginationInfo(int pageSize)
        {
            return ApiLogEntry.GetPaginationInfo(pageSize);
        }

        /// <summary>
        /// Get ApiLog Detail
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns>ApiLog Detail</returns>
        public ApiLogEntry GetApiLogDetail(string Id)
        {
            return ApiLogEntry.GetApiLogDetail(Id);
        }
    }
}
