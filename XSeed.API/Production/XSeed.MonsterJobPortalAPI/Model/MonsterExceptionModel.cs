﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace XSeed.MonsterJobPortalAPI.Model
{
    public class MonsterExceptionModel
    {
        static string url = ConfigurationManager.AppSettings["MongoURL"].ToString();
        static string dbName = ConfigurationManager.AppSettings["MongoDB"].ToString();
        public static MongoClient client = new MongoClient(url);
        static IMongoDatabase database = client.GetDatabase(dbName);

        #region Property Declaration

        /* Id References */
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string ResumeId { get; set; }
        public string RequestXML { get; set; }
        public string ResponseStatusCode { get; set; }
        public HttpWebResponse Response { get; set; }
        public string ExceptionMessage { get; set; }
        public string ExceptionStackTrace { get; set; }
        public string ExceptionInnerException { get; set; }
        public string InnerXML { get; set; }
        public Nullable<DateTime> CreatedDate { get; set; }

        #endregion

        #region User Defined Functions

        /// <summary>
        /// Get list Monster Exception logs
        /// </summary>        
        /// <returns>Monster Log</returns>
        public static List<MonsterExceptionModel> ListMonsterExceptionLogs()
        {
            var filter = Builders<MonsterExceptionModel>.Filter.Empty;
            var collection = database.GetCollection<MonsterExceptionModel>("MonsterExceptionLog");

            var documents = collection.Find(filter).SortByDescending(c => c.CreatedDate).ToList();

            return documents;
        }

        /// <summary>
        /// Get single Monster Exception log
        /// </summary>
        /// <param name="Id"> Monster Log Id</param>
        /// <returns>Monster Log</returns>
        public static MonsterExceptionModel GetMonsterExceptionLogDetail(string Id)
        {
            var filter = Builders<MonsterExceptionModel>.Filter.Eq("_id", Id);
            var collection = database.GetCollection<MonsterExceptionModel>("MonsterExceptionLog");

            return collection.Find(filter).FirstOrDefault();
        }

        /// <summary>
        /// Post Monster Log Detail
        /// </summary>
        /// <param name="monsterLogModel">Monster Log Model</param>
        public static void PostMonsterExceptionLogDetail(MonsterExceptionModel monsterExceptionModel)
        {
            var collection = database.GetCollection<MonsterExceptionModel>("MonsterExceptionLog");

            collection.InsertOne(monsterExceptionModel);
        }

        public static MonsterExceptionModel BuildExceptionModel(Exception ex, string requestXml = null, HttpWebResponse response = null, string innerXML = null)
        {
            MonsterExceptionModel exceptionModel = new MonsterExceptionModel();

            exceptionModel.ExceptionMessage = ex.Message;
            exceptionModel.ExceptionStackTrace = ex.StackTrace;
            exceptionModel.ExceptionInnerException = ex.InnerException != null ? ex.InnerException.Message : null;
            exceptionModel.RequestXML = requestXml;
            exceptionModel.ResponseStatusCode = response != null ? response.StatusCode.ToString() : null;
            exceptionModel.Response = response;
            exceptionModel.InnerXML = innerXML;
            exceptionModel.CreatedDate = DateTime.UtcNow;

            return exceptionModel;
        }

        #endregion


    }
}
