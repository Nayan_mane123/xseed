﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using XSeed.MonsterJobPortalAPI.Model;

namespace XSeed.MonsterJobPortalAPI
{
    public class MonsterAccountAPI
    {
        //Constants MonsterPortalConstants = Constants.GetInstance();

        public LicenseInfo getLicenseInformation()
        {
            string requestXml = string.Empty;
            HttpWebResponse response = null;

            try
            {
                LicenseInfo licenseInfo = new LicenseInfo();

                XmlDocument doc = new XmlDocument();
                var xmlPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Constants.monsterXmlPath, "LicenseRequest.xml");
                XmlTextReader reader = new XmlTextReader(xmlPath);
                reader.WhitespaceHandling = WhitespaceHandling.None;
                doc.Load(reader);
                requestXml = doc.InnerXml;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Constants.monsterBGWUrl);
                byte[] bytes;
                bytes = System.Text.Encoding.ASCII.GetBytes(requestXml);
                request.ContentType = "text/xml; encoding='utf-8'";
                request.ContentLength = bytes.Length;
                request.Method = "POST";
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();

                response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = response.GetResponseStream();
                    string responseStr = new StreamReader(responseStream).ReadToEnd();
                    var str = XElement.Parse(responseStr);

                    foreach (var element in str.Elements())
                    {
                        if (element.Name.LocalName == "Body")
                        {
                            foreach (var child in element.Elements())
                            {
                                licenseInfo.LicenseType = child.Elements().Where(x => x.Name.LocalName == "Inventories")
                                            .Elements().Where(x => x.Name.LocalName == "Inventory")
                                            .Elements().Where(x => x.Name.LocalName == "ResourceLicenseInfo")
                                            .Elements().Where(x => x.Name.LocalName == "ResourceLicenseTypeID").FirstOrDefault().Value;

                                licenseInfo.TotalPurchasedQuantity = child.Elements().Where(x => x.Name.LocalName == "Inventories")
                                            .Elements().Where(x => x.Name.LocalName == "Inventory")
                                            .Elements().Where(x => x.Name.LocalName == "Quantity")
                                            .Elements().Where(x => x.Name.LocalName == "TotalPurchased").FirstOrDefault().Value;

                                licenseInfo.TotalAvailableQuantity = child.Elements().Where(x => x.Name.LocalName == "Inventories")
                                            .Elements().Where(x => x.Name.LocalName == "Inventory")
                                            .Elements().Where(x => x.Name.LocalName == "Quantity")
                                            .Elements().Where(x => x.Name.LocalName == "TotalAvailable").FirstOrDefault().Value;

                                licenseInfo.InventoryActiveDate = child.Elements().Where(x => x.Name.LocalName == "Inventories")
                                            .Elements().Where(x => x.Name.LocalName == "Inventory")
                                            .Elements().Where(x => x.Name.LocalName == "InventoryDates")
                                            .Elements().Where(x => x.Name.LocalName == "InventoryActiveDate").FirstOrDefault().Value;

                                licenseInfo.InventoryExpireDate = child.Elements().Where(x => x.Name.LocalName == "Inventories")
                                            .Elements().Where(x => x.Name.LocalName == "Inventory")
                                            .Elements().Where(x => x.Name.LocalName == "InventoryDates")
                                            .Elements().Where(x => x.Name.LocalName == "InventoryExpireDate").FirstOrDefault().Value;


                                foreach (var attribute in child.Elements().Where(x => x.Name.LocalName == "Inventories")
                                                    .Elements().Where(x => x.Name.LocalName == "Inventory")
                                                    .Elements().Where(x => x.Name.LocalName == "InventoryAttrs")
                                                    .Elements())
                                {

                                    if (attribute.Elements().Where(x => x.Name.LocalName == "AttrTypeId")
                                                        .FirstOrDefault().Value == "ResumeBoardID")
                                    {
                                        licenseInfo.ResumeBoard = attribute.Elements().Where(x => x.Name.LocalName == "AttrValue")
                                                .Elements().Where(x => x.Name.LocalName == "Numeric1")
                                                .Elements().Where(x => x.Name.LocalName == "Name")
                                                .FirstOrDefault().Value;
                                    }

                                    if (attribute.Elements().Where(x => x.Name.LocalName == "AttrTypeId")
                                                        .FirstOrDefault().Value == "LocationGroupID")
                                    {
                                        licenseInfo.LocationGroup = attribute.Elements().Where(x => x.Name.LocalName == "AttrValue")
                                                .Elements().Where(x => x.Name.LocalName == "Numeric1")
                                                .Elements().Where(x => x.Name.LocalName == "Name")
                                                .FirstOrDefault().Value;
                                    }

                                    if (attribute.Elements().Where(x => x.Name.LocalName == "AttrTypeId")
                                                        .FirstOrDefault().Value == "OriginalQuantity")
                                    {
                                        licenseInfo.OriginalQuantity = attribute.Elements().Where(x => x.Name.LocalName == "AttrValue")
                                                .Elements().Where(x => x.Name.LocalName == "Numeric1")
                                                .Elements().Where(x => x.Name.LocalName == "Value")
                                                .FirstOrDefault().Value;
                                    }
                                }
                            }
                        }
                    }
                }

                return licenseInfo;
            }
            catch (Exception ex)
            {
                /* Log monster exception */
                var exceptionModel = MonsterExceptionModel.BuildExceptionModel(ex: ex, requestXml: requestXml, response: response);
                MonsterExceptionModel.PostMonsterExceptionLogDetail(exceptionModel);

                throw new Exception("MonsterLicenceException");
            }

        }
    }
}
