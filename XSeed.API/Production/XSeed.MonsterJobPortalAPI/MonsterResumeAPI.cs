﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using XSeed.MonsterJobPortalAPI.Model;

namespace XSeed.MonsterJobPortalAPI
{
    public class MonsterResumeAPI
    {
        //Constants MonsterPortalConstants = new Constants();

        #region Public Methods

        public List<MonsterCandidate> GetMonsterCandidates(string skills)
        {
            string[] skillArr = skills.Split(',');

            var query = string.Empty;
            foreach (var item in skillArr)
            {
                var skill = item.Contains(" ") ? "'" + item + "'" : item;
                if (skillArr.First() == item)
                {
                    query = query + skill;
                }
                else
                {
                    query = query + "+OR+" + skill;
                }
            }

            string sortOrder = "mdate,rank";
            string destinationUrl = string.Format("{0}?q={1}&ver={2}&sort={3}&cat={4}", Constants.monsterRSXUrl, query, Constants.monsterRSXVersion, sortOrder, Constants.monsterTCogCAT);

            List<MonsterCandidate> Candidates = new List<MonsterCandidate>();

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
            request.ContentType = "text/xml; encoding='utf-8'";
            request.Method = "Get";
            HttpWebResponse response;
            response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseStr = new StreamReader(responseStream).ReadToEnd();
                var str = XElement.Parse(responseStr);
                var availableResumeCount = Convert.ToInt32(str.Elements("Resumes").Select(x => x.Attribute("Found").Value).FirstOrDefault());

                if (availableResumeCount > 0)
                {
                    foreach (var child in str.Element("Resumes").Elements())
                    {
                        MonsterCandidate candidate = new MonsterCandidate();

                        candidate.ResumeId = child.Attribute("SID").Value;
                        candidate.FirstName = child.Element("PersonalData").Element("Name").Element("First").Value;
                        candidate.LastName = child.Element("PersonalData").Element("Name").Element("Last").Value;
                        candidate.Location = child.Element("PersonalData").Element("Address").Element("Location").Value;
                        candidate.Education = child.Element("Educations").Element("Education").Element("Level").Value;
                        //candidate.CreatedDate = Convert.ToDateTime(child.Element("DateCreated").Value).ToString("MM-dd-yyyy");
                        //candidate.ModifiedDate = Convert.ToDateTime(child.Element("DateModified").Value).ToString("MM-dd-yyyy");
                        candidate.CreatedDate = child.Element("DateCreated").Value;
                        candidate.ModifiedDate = child.Element("DateModified").Value;

                        candidate.LocalResumePath = GetMonsterResumeLocalPathIfAlreadyExists(candidate.ResumeId, candidate.ModifiedDate);
                        candidate.AlreadyDownloaded = string.IsNullOrEmpty(candidate.LocalResumePath) ? bool.FalseString : bool.TrueString + " (View Local Copy)";

                        if (child.Element("Experiences").HasElements)
                        {
                            candidate.CurrentCompany = child.Element("Experiences").Element("Experience").Element("Company").Element("Name").Value;
                            candidate.CurrentJobTitle = child.Element("Experiences").Element("Experience").Element("Job").Element("Title").Value;
                        }

                        candidate.Relevance = child.Element("Relevance").Value;
                        if (child.Element("Distance") != null)
                        {
                            candidate.Distance = child.Element("Distance").Value;
                        }
                        candidate.ResumeTitle = child.Element("ResumeTitle").Value;
                        if (child.Element("PersonalData").HasElements)
                        {
                            candidate.Confidential = child.Element("PersonalData").Element("Confidential").Value;
                            candidate.Country = child.Element("PersonalData").Element("Address").Element("Country").Value;
                            candidate.State = child.Element("PersonalData").Element("Address").Element("State").Value;
                            candidate.City = child.Element("PersonalData").Element("Address").Element("City").Value;
                            candidate.PostalCode = child.Element("PersonalData").Element("Address").Element("PostalCode").Value;

                            candidate.ServiceFlag = child.Element("PersonalData").Element("MilitaryExperience").Element("ServiceFlag").Value;
                            candidate.MilitaryInvolvement = child.Element("PersonalData").Element("MilitaryExperience").Element("MilitaryInvolvement").Value;
                        }

                        if (child.Element("Target").HasElements)
                        {
                            candidate.TargetJobTitle = child.Element("Target").Element("JobTitle").Value;
                            candidate.TargetRelocation = child.Element("Target").Element("Relocation").Value;
                            candidate.TargetMinSalary = child.Element("Target").Element("Salary").Element("Min").Value;
                            candidate.TargetMaxSalary = child.Element("Target").Element("Salary").Element("Max").Value;
                            candidate.TargetSalaryType = child.Element("Target").Element("Salary").Element("Type").Value;
                            candidate.TargetSalaryCurrency = child.Element("Target").Element("Salary").Element("Currency").Value;
                        }
                        if (child.Element("WorkAuths").HasElements)
                        {
                            candidate.WorkAuthType = child.Element("WorkAuths").Element("WorkAuth").Element("Country").Value;
                            candidate.WorkAuthCountry = child.Element("WorkAuths").Element("WorkAuth").Element("AuthType").Value;
                        }
                        candidate.Board = child.Element("Boards").Element("Board").Value;

                        Candidates.Add(candidate);
                    }
                }
            }
            return Candidates;
        }

        public List<MonsterCandidate> GetMonsterCandidates(MonsterCandidateSearch searchModel)
        {
            StringBuilder destUrl = new StringBuilder();
            HttpWebResponse response = null;
            Stream responseStream = null;

            try
            {
                #region Build Monster Search Query

                destUrl.AppendFormat("{0}?ver={1}&cat={2}", Constants.monsterRSXUrl, Constants.monsterRSXVersion, Constants.monsterTCogCAT);

                ////Skills
                var query = string.Empty;
                if (!string.IsNullOrEmpty(searchModel.MonsterBooleanQuery))
                {
                    query = HttpUtility.UrlEncode(searchModel.MonsterBooleanQuery);
                }
                else if (searchModel.Skills != null && string.IsNullOrEmpty(searchModel.MonsterBooleanQuery))
                {
                    foreach (var item in searchModel.Skills)
                    {
                        var skill = item.Contains(" ") ? "'" + item + "'" : item;
                        if (searchModel.Skills.First() == item)
                        {
                            query = query + skill;
                        }
                        else
                        {
                            query = query + "+OR+" + skill;
                        }
                    }
                }

                if (!string.IsNullOrWhiteSpace(query))
                {
                    destUrl.AppendFormat("&q={0}", query);
                }
                ////----------------------

                ////JobTitle
                if (!string.IsNullOrWhiteSpace(searchModel.Title) && string.IsNullOrEmpty(searchModel.MonsterBooleanQuery))
                {
                    destUrl.AppendFormat("&qtjt='{0}'", searchModel.Title);
                }
                ////----------------------

                ////JobType
                if (!string.IsNullOrWhiteSpace(searchModel.Type))
                {
                    if (searchModel.Type == "Permanent")
                    {
                        destUrl.Append("&tjtp=1");
                    }
                    else if (searchModel.Type == "Temp/Contract")
                    {
                        destUrl.Append("&tjttc=1");
                    }
                    else if (searchModel.Type == "Full Time")
                    {
                        destUrl.Append("&tjtft=1");
                    }
                    else if (searchModel.Type == "Part Time")
                    {
                        destUrl.Append("&tjtpt=1");
                    }
                    else if (searchModel.Type == "Intern")
                    {
                        destUrl.Append("&tjti=1");
                    }
                    else if (searchModel.Type == "Per Diem")
                    {
                        destUrl.Append("&tjtpd=1");
                    }
                    else if (searchModel.Type == "Seasonal")
                    {
                        destUrl.Append("&tjts=1");
                    }
                }
                ////----------------------

                ////Experience Level
                if (!string.IsNullOrWhiteSpace(searchModel.ExperienceLevel))
                {
                    decimal expLvl;
                    decimal.TryParse(searchModel.ExperienceLevel, out expLvl);

                    string monsterExperienceId = expLvl == 0 ? string.Empty : GetMonsterYearsExperienceId(expLvl);

                    if (!string.IsNullOrEmpty(monsterExperienceId))
                    {
                        destUrl.AppendFormat("&yrsexpid={0}", monsterExperienceId);
                    }
                }
                ////----------------------

                ////Location
                if (!string.IsNullOrWhiteSpace(searchModel.Location))
                {
                    var locationLookupPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Constants.monsterLookUpPath, "Location_All.csv");
                    var location = File.ReadLines(locationLookupPath).Skip(1)
                         .Select(ParseLocationFromLine).ToList()
                         .Where(p => p.CityName.Contains("\"") ? p.CityName.ToLower() == "\"" + searchModel.Location.ToLower() + "\"" : p.CityName.ToLower() == searchModel.Location.ToLower())
                         .FirstOrDefault();

                    if (location != null)
                    {
                        destUrl.AppendFormat("&twlid={0}&twlb={1}", location.LocationID.Contains("\"") ? location.LocationID.Replace("\"", string.Empty) : location.LocationID, 0);
                    }
                }
                ////----------------------

                ////Salary
                if (!string.IsNullOrWhiteSpace(searchModel.MinSalary)
                    && !string.IsNullOrWhiteSpace(searchModel.MaxSalary)
                    && !string.IsNullOrWhiteSpace(searchModel.SalaryType)
                    && !string.IsNullOrWhiteSpace(searchModel.CurrencyType))
                {
                    ////Salary Type
                    string monsterSalaryTypeId = GetMonsterSalaryTypeId(searchModel.SalaryType);
                    if (!string.IsNullOrEmpty(monsterSalaryTypeId))
                    {
                        ////Currency Type
                        var currencyLookupPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Constants.monsterLookUpPath, "Currency.csv");
                        var currency = File.ReadLines(currencyLookupPath).Skip(1)
                             .Select(ParseCurrencyFromLine).ToList()
                             .Where(p => p.Currency.Contains("\"") ? p.Currency.ToLower() == "\"" + searchModel.CurrencyType.ToLower() + "\"" : p.Currency.ToLower() == searchModel.CurrencyType.ToLower())
                             .FirstOrDefault();

                        if (currency != null)
                        {
                            destUrl.AppendFormat("&tsaltyp={0}", monsterSalaryTypeId);
                            destUrl.AppendFormat("&tsalcur={0}", currency.CurrencyID.Contains("\"") ? currency.CurrencyID.Replace("\"", string.Empty) : currency.CurrencyID);
                        }

                        ////Min Salary
                        destUrl.AppendFormat("&tsalmin={0}", searchModel.MinSalary);

                        ////Max Salary
                        destUrl.AppendFormat("&tsalmax={0}", searchModel.MaxSalary);
                    }
                }
                ////----------------------

                ////Sort Order
                destUrl.AppendFormat("&sort={0}", "mdate,rank");
                ////---------------------

                ////Page Size
                destUrl.AppendFormat("&pagesize={0}", "100");
                ////---------------------

                //string sortOrder = "mdate,rank";
                //string destinationUrl = string.Format("{0}?q={1}&ver={2}&sort={3}&cat={4}", Constants.monsterRSXUrl, query, Constants.monsterRSXVersion, sortOrder, Constants.monsterTCogCAT);

                //// Location Type viz
                var locationID = string.Empty;
                if (searchModel.LocationType == "byRadius")
                {
                    if (string.IsNullOrEmpty(searchModel.MonsterLocationMilesAway))
                    {
                        searchModel.MonsterLocationMilesAway = "0";
                    }

                    ////Postal Code Radius
                    if (!string.IsNullOrWhiteSpace(searchModel.MonsterLocationZIPCode) && !string.IsNullOrWhiteSpace(searchModel.MonsterLocationMilesAway))
                    {
                        destUrl.AppendFormat("&rpcr={0}-{1}", searchModel.MonsterLocationZIPCode, searchModel.MonsterLocationMilesAway);
                    }
                }
                else if (searchModel.LocationType == "byLocation")
                {
                    if (string.IsNullOrEmpty(searchModel.MonsterTargetWorkLocation))
                    {
                        searchModel.MonsterTargetWorkLocation = "false";
                    }
                    if (searchModel.MonsterUSLocationID != null && searchModel.MonsterUSLocationID.Any())
                    {
                        foreach (var item in searchModel.MonsterUSLocationID)
                        {
                            var USLocationID = item.Contains(" ") ? "'" + item + "'" : item;
                            if (searchModel.MonsterUSLocationID.First() == item)
                            {
                                locationID = locationID + USLocationID;
                            }
                            else
                            {
                                locationID = locationID + "+" + USLocationID;
                            }
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(locationID))
                    {
                        destUrl.AppendFormat("&rlid={0}", locationID);
                    }

                    if (searchModel.MonsterTargetWorkLocation == "true" && !string.IsNullOrWhiteSpace(locationID))
                    {
                        destUrl.AppendFormat("&twlid={0}", locationID);
                    }

                }

                ////Resume Posted

                if (!string.IsNullOrWhiteSpace(searchModel.MonsterResumeMinDate) && !string.IsNullOrWhiteSpace(searchModel.MonsterResumeMaxDate))
                {
                    destUrl.AppendFormat("&mdateminage={0}&mdatemaxage={1}", searchModel.MonsterResumeMinDate, searchModel.MonsterResumeMaxDate);
                }


                #endregion Build Monster Search Query

                List<MonsterCandidate> Candidates = new List<MonsterCandidate>();

                //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destUrl.ToString());
                request.ContentType = "text/xml; encoding='utf-8'";
                request.Method = "Get";
                response = (HttpWebResponse)request.GetResponse();
                if (response != null && response.StatusCode == HttpStatusCode.OK)
                {
                    responseStream = response.GetResponseStream();
                    string responseStr = new StreamReader(responseStream).ReadToEnd();
                    var str = XElement.Parse(responseStr);
                    var availableResumeCount = Convert.ToInt32(str.Elements("Resumes").Select(x => x.Attribute("Found").Value).FirstOrDefault());

                    if (availableResumeCount > 0)
                    {
                        foreach (var child in str.Element("Resumes").Elements())
                        {
                            MonsterCandidate candidate = new MonsterCandidate();

                            candidate.ResumeId = child.Attribute("SID") != null ? child.Attribute("SID").Value : string.Empty;
                            //candidate.CreatedDate = Convert.ToDateTime(child.Element("DateCreated").Value).ToString("MM-dd-yyyy");
                            //candidate.ModifiedDate = Convert.ToDateTime(child.Element("DateModified").Value).ToString("MM-dd-yyyy");
                            candidate.CreatedDate = child.Element("DateCreated") != null ? child.Element("DateCreated").Value : string.Empty;
                            candidate.ModifiedDate = child.Element("DateModified") != null ? child.Element("DateModified").Value : string.Empty;
                            candidate.Relevance = child.Element("Relevance") != null ? child.Element("Relevance").Value : string.Empty;
                            candidate.Distance = child.Element("Distance") != null ? child.Element("Distance").Value : string.Empty;
                            candidate.ResumeTitle = child.Element("ResumeTitle") != null ? child.Element("ResumeTitle").Value : string.Empty;
                            candidate.Board = child.Element("Boards") != null ? child.Element("Boards").Element("Board").Value : string.Empty;

                            ////Collect Personal data
                            if (child.Element("PersonalData") != null && child.Element("PersonalData").HasElements)
                            {
                                var element = child.Element("PersonalData");

                                if (element.Element("Name") != null && element.Element("Name").HasElements)
                                {
                                    candidate.FirstName = element.Element("Name").Element("First") != null ? element.Element("Name").Element("First").Value : string.Empty;
                                    candidate.LastName = element.Element("Name").Element("Last") != null ? element.Element("Name").Element("Last").Value : string.Empty;
                                }

                                if (element.Element("Address") != null && element.Element("Address").HasElements)
                                {
                                    candidate.Location = element.Element("Address").Element("Location") != null ? element.Element("Address").Element("Location").Value : string.Empty;

                                    candidate.Country = element.Element("Address").Element("Country") != null ? element.Element("Address").Element("Country").Value : string.Empty;
                                    candidate.State = element.Element("Address").Element("State") != null ? element.Element("Address").Element("State").Value : string.Empty;
                                    candidate.City = element.Element("Address").Element("City") != null ? element.Element("Address").Element("City").Value : string.Empty;
                                    candidate.PostalCode = element.Element("Address").Element("PostalCode") != null ? element.Element("Address").Element("PostalCode").Value : string.Empty;
                                }

                                candidate.Confidential = element.Element("Confidential") != null ? element.Element("Confidential").Value : string.Empty;

                                if (element.Element("MilitaryExperience") != null && element.Element("MilitaryExperience").HasElements)
                                {
                                    candidate.ServiceFlag = element.Element("MilitaryExperience").Element("ServiceFlag") != null ? element.Element("MilitaryExperience").Element("ServiceFlag").Value : string.Empty;
                                    candidate.MilitaryInvolvement = element.Element("MilitaryExperience").Element("MilitaryInvolvement") != null ? element.Element("MilitaryExperience").Element("MilitaryInvolvement").Value : string.Empty;
                                }
                            }

                            ////Collect Target data
                            if (child.Element("Target") != null && child.Element("Target").HasElements)
                            {
                                var element = child.Element("Target");

                                candidate.TargetJobTitle = element.Element("JobTitle") != null ? element.Element("JobTitle").Value : string.Empty;
                                candidate.TargetRelocation = element.Element("Relocation") != null ? element.Element("Relocation").Value : string.Empty;
                                candidate.TargetMinSalary = element.Element("Salary") != null ? element.Element("Salary").Element("Min").Value : string.Empty;
                                candidate.TargetMaxSalary = element.Element("Salary") != null ? element.Element("Salary").Element("Max").Value : string.Empty;
                                candidate.TargetSalaryType = element.Element("Salary") != null ? element.Element("Salary").Element("Type").Value : string.Empty;
                                candidate.TargetSalaryCurrency = element.Element("Salary") != null ? element.Element("Salary").Element("Currency").Value : string.Empty;

                                if (element.Element("JobTypes") != null && element.Element("JobTypes").HasElements)
                                {
                                    var jobTypes = element.Element("JobTypes").Elements();

                                    foreach (var jobType in jobTypes)
                                    {
                                        if (jobType != null && jobType.Value != "")
                                        {
                                            candidate.TargetJobType = string.Join(",", candidate.TargetJobType, jobType.Value);
                                        }
                                    }
                                    candidate.TargetJobType = candidate.TargetJobType.TrimStart(',');
                                }
                            }

                            ////Collect workauth data
                            if (child.Element("WorkAuths") != null && child.Element("WorkAuths").HasElements)
                            {
                                var element = child.Element("WorkAuths");

                                if (element.Element("WorkAuth") != null && element.Element("WorkAuth").HasElements)
                                {
                                    candidate.WorkAuthType = element.Element("WorkAuth").Element("Country") != null ? element.Element("WorkAuth").Element("Country").Value : string.Empty;
                                    candidate.WorkAuthCountry = element.Element("WorkAuth").Element("AuthType") != null ? element.Element("WorkAuth").Element("AuthType").Value : string.Empty;
                                }
                            }

                            ////collect experience data
                            if (child.Element("Experiences") != null && child.Element("Experiences").HasElements)
                            {
                                var element = child.Element("Experiences");

                                if (element.Element("Experience") != null && element.Element("Experience").HasElements)
                                {
                                    candidate.CurrentCompany = element.Element("Experience").Element("Company") != null ? element.Element("Experience").Element("Company").Element("Name").Value : string.Empty;
                                    candidate.CurrentJobTitle = element.Element("Experience").Element("Job") != null ? element.Element("Experience").Element("Job").Element("Title").Value : string.Empty;
                                }
                            }

                            ////Education
                            if (child.Element("Educations") != null && child.Element("Educations").HasElements)
                            {
                                var element = child.Element("Educations");
                                candidate.Education = element.Element("Education") != null ? element.Element("Education").Element("Level").Value : string.Empty;
                            }

                            //candidate.LocalResumePath = GetMonsterResumeLocalPathIfAlreadyExists(candidate.ResumeId, candidate.ModifiedDate);
                            //candidate.AlreadyDownloaded = string.IsNullOrEmpty(candidate.LocalResumePath) ? bool.FalseString : bool.TrueString + " (View Local Copy)";

                            Candidates.Add(candidate);
                        }
                    }
                }
                return Candidates;
            }
            catch (Exception ex)
            {
                /* Load stream */
                XmlDocument doc = new XmlDocument();
                doc.Load(responseStream);

                /* Log monster exception */
                var exceptionModel = MonsterExceptionModel.BuildExceptionModel(ex: ex, requestXml: destUrl.ToString(), response: response, innerXML: doc.InnerXml);
                MonsterExceptionModel.PostMonsterExceptionLogDetail(exceptionModel);

                throw new Exception("GetMonsterCandidateException");
            }
        }

        public string GetMonsterResumeLocalPathIfAlreadyExists(string monsterResumeId, string monsterResumeModifiedDate)
        {
            try
            {
                MongoClient client = new MongoClient(Constants.monsterMongoDbUrl);
                var database = client.GetDatabase(Constants.monsterMongoDbName);

                var filter = (Builders<MonsterCandidateDetail>.Filter.Eq("MonsterResumeId", monsterResumeId)
                              & Builders<MonsterCandidateDetail>.Filter.Eq("MonsterResumeModifiedDate", monsterResumeModifiedDate));
                var collection = database.GetCollection<MonsterCandidateDetail>(Constants.mongoDbCollectionName);

                //return collection.Find(filter).Count() > 0 ? bool.TrueString : bool.FalseString;
                //return collection.Find(filter).Count() > 0 ? bool.TrueString + " (View Local Copy)" : bool.FalseString;

                var monsterCandidateDetail = collection.Find(filter).FirstOrDefault();
                if (monsterCandidateDetail == null)
                {
                    return string.Empty;
                }
                else
                {
                    return monsterCandidateDetail.ResumeLocalFilePath;
                }
            }
            catch (Exception ex)
            {
                return bool.FalseString;
            }
        }

        public string GetCandidateResume(string resumeId, string resumeModifiedDate, string firstName, string lastName)
        {
            string filePath = string.Empty;
            var xmlPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Constants.monsterXmlPath, "ResumeViewRequest.xml");

            XmlDocument doc = new XmlDocument();
            XmlTextReader reader = new XmlTextReader(xmlPath);
            reader.WhitespaceHandling = WhitespaceHandling.None;
            doc.Load(reader);
            string myXMLDoc = doc.InnerXml;
            string destinationUrl = "https://gateway.monster.com:8443/bgwBroker";

            myXMLDoc = myXMLDoc.Replace("<Value></Value>", "<Value>" + resumeId + "</Value>");

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
            byte[] bytes;
            bytes = System.Text.Encoding.ASCII.GetBytes(myXMLDoc);
            request.ContentType = "text/xml; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response;
            response = (HttpWebResponse)request.GetResponse();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(response.GetResponseStream());

                //XmlNode textResume = xmlDoc.GetElementsByTagName("TextResume")[0];
                //var textResumeContents = textResume.InnerXml;
                XmlNode fileResumeNode = xmlDoc.GetElementsByTagName("File")[0];
                var fileResumeContents = fileResumeNode != null ? fileResumeNode.InnerXml : string.Empty;
                XmlNode fileResumeName = xmlDoc.GetElementsByTagName("FileName")[0];
                var resumeName = fileResumeName.InnerXml;

                if (!string.IsNullOrEmpty(resumeName) && !string.IsNullOrEmpty(fileResumeContents))
                {
                    var candidateResumeFilePath = CreateCandidateResume(fileResumeContents, resumeName);
                    var mongoDbCandidateId = InsertMonsterCandidateDetail(firstName, lastName, resumeId, resumeName, resumeModifiedDate, candidateResumeFilePath);

                    return candidateResumeFilePath;
                }

                //XmlDocument doc1 = new XmlDocument();
                //doc1.Load(responseStream);
                //XmlNode idNode = doc1.GetElementsByTagName("TextResume")[0];
                //var result = idNode.InnerXml;

                //XmlDocument doc1 = new XmlDocument();
                //doc1.Load("D:\\PROJECTS\\XSeed\\MonsterIntegrationDemoSample\\MonsterSingleResumeResponse2.xml");
                //XmlNode idNode = doc1.GetElementsByTagName("TextResume")[0];
                //var result = idNode.InnerXml;
            }

            return filePath;
        }

        public Stream GetCandidateResume(string resumeId)
        {
            string myXMLDoc = string.Empty;
            HttpWebResponse response = null;

            try
            {
                string filePath = string.Empty;
                var xmlPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Constants.monsterXmlPath, "ResumeViewRequest.xml");

                XmlDocument doc = new XmlDocument();
                XmlTextReader reader = new XmlTextReader(xmlPath);
                reader.WhitespaceHandling = WhitespaceHandling.None;
                doc.Load(reader);
                myXMLDoc = doc.InnerXml;
                myXMLDoc = myXMLDoc.Replace("<Value></Value>", "<Value>" + resumeId + "</Value>");

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Constants.monsterBGWUrl);
                byte[] bytes;
                bytes = System.Text.Encoding.ASCII.GetBytes(myXMLDoc);
                request.ContentType = "text/xml; encoding='utf-8'";
                request.ContentLength = bytes.Length;
                request.Method = "POST";
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();

                response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return response.GetResponseStream();
                }

                return Stream.Null;
            }
            catch (Exception ex)
            {
                /* Log monster exception */
                var exceptionModel = MonsterExceptionModel.BuildExceptionModel(ex: ex, response: response, innerXML: myXMLDoc);
                MonsterExceptionModel.PostMonsterExceptionLogDetail(exceptionModel);

                throw new Exception("DownloadMonsterCandidateFailed");
            }
        }

        public string InsertMonsterCandidateDetail(string firstName, string lastName, string resumeId, string resumeFileName, string resumeModifiedDate, string candidateResumeFilePath)
        {
            string result = string.Empty;

            try
            {
                MongoClient client = new MongoClient(Constants.monsterMongoDbUrl);
                var database = client.GetDatabase(Constants.monsterMongoDbName);
                var collection = database.GetCollection<MonsterCandidateDetail>(Constants.mongoDbCollectionName);

                MonsterCandidateDetail monsterCandidateDetail = new MonsterCandidateDetail();
                monsterCandidateDetail.FirstName = firstName;
                monsterCandidateDetail.LastName = lastName;
                monsterCandidateDetail.MonsterResumeId = resumeId;
                monsterCandidateDetail.MonsterResumeFileName = resumeFileName;
                monsterCandidateDetail.MonsterResumeModifiedDate = resumeModifiedDate;
                monsterCandidateDetail.ResumeLocalFilePath = candidateResumeFilePath;

                collection.InsertOne(monsterCandidateDetail);
                return monsterCandidateDetail._id;
            }
            catch (Exception ex)
            {
                throw;
            }

            return result;
        }

        #endregion Public Methods

        #region private methods

        private string GetMonsterSalaryTypeId(string salaryType)
        {
            if (salaryType == "Per year" || salaryType == "Yearly")
            {
                return "1";
            }
            if (salaryType == "Per hour" || salaryType == "Hourly")
            {
                return "2";
            }
            if (salaryType == "Per week" || salaryType == "Weekly")
            {
                return "3";
            }
            if (salaryType == "Per month" || salaryType == "Monthly")
            {
                return "4";
            }

            return string.Empty;
        }

        private static MonsterLocation ParseLocationFromLine(string line)
        {
            string[] parts = line.Split(',');
            return new MonsterLocation
            {
                LocationID = parts.ElementAtOrDefault(0) != null ? parts[0] : string.Empty,
                ContinentName = parts.ElementAtOrDefault(1) != null ? parts[1] : string.Empty,
                CountryAbbrev = parts.ElementAtOrDefault(2) != null ? parts[2] : string.Empty,
                CountryName = parts.ElementAtOrDefault(3) != null ? parts[3] : string.Empty,
                StateAbbrev = parts.ElementAtOrDefault(4) != null ? parts[4] : string.Empty,
                StateName = parts.ElementAtOrDefault(5) != null ? parts[5] : string.Empty,
                CityName = parts.ElementAtOrDefault(6) != null ? parts[6] : string.Empty
            };
        }

        private static MonsterCurrency ParseCurrencyFromLine(string line)
        {
            string[] parts = line.Split(',');
            return new MonsterCurrency
            {
                CurrencyID = parts.ElementAtOrDefault(0) != null ? parts[0] : string.Empty,
                Currency = parts.ElementAtOrDefault(1) != null ? parts[1] : string.Empty,
                Descritpion = parts.ElementAtOrDefault(2) != null ? parts[2] : string.Empty
            };
        }

        private string GetMonsterYearsExperienceId(decimal expLvl)
        {
            if (expLvl < 1)
            {
                return "1";
            }
            if (expLvl >= 1 && expLvl < 2)
            {
                return "2";
            }
            if (expLvl >= 2 && expLvl < 5)
            {
                return "3";
            }
            if (expLvl >= 5 && expLvl < 7)
            {
                return "4";
            }
            if (expLvl >= 7 && expLvl < 10)
            {
                return "5";
            }
            if (expLvl >= 10 && expLvl < 15)
            {
                return "6";
            }
            if (expLvl >= 15)
            {
                return "7";
            }

            return string.Empty;
        }

        private string CreateCandidateResume(string base64InputString, string resumeFileName)
        {
            string filePath = Constants.monsterResumeLocalCopyPath + resumeFileName;

            File.WriteAllBytes(filePath, Convert.FromBase64String(base64InputString));

            return filePath;
        }

        #endregion private methods
    }

    public class MonsterCandidateDetail
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MonsterResumeId { get; set; }
        public string MonsterResumeFileName { get; set; }
        public string MonsterResumeModifiedDate { get; set; }
        public string ResumeLocalFilePath { get; set; }
    }

    public class MonsterLocation
    {
        public string LocationID { get; set; }
        public string ContinentName { get; set; }
        public string CountryAbbrev { get; set; }
        public string CountryName { get; set; }
        public string StateAbbrev { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
    }

    public class MonsterCurrency
    {
        public string CurrencyID { get; set; }
        public string Currency { get; set; }
        public string Descritpion { get; set; }
    }
}