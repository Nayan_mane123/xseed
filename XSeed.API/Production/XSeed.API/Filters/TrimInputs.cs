﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace XSeed.API.Filters
{
    public class TrimInputs : ActionFilterAttribute, IActionFilter 
    {
        public override void OnActionExecuting(HttpActionContext actionExecutedContext)
        {
            var objectContent = actionExecutedContext.Response.Content as ObjectContent;
            if (objectContent != null)
            {
                var type = objectContent.ObjectType; //type of the returned object
                var value = objectContent.Value; //holding the returned value
            }



            Object obj = actionExecutedContext.ControllerContext.RouteData; 
            List<string> stringPropertyNamesAndValues = obj.GetType().GetProperties().Where(pi => (pi.PropertyType == typeof(string))).Select(pi => pi.Name).ToList();
            foreach (var propertyName in stringPropertyNamesAndValues)
            {
                var propertyValue = obj.GetType().GetProperty(propertyName).GetValue(obj, null);
                obj.GetType().GetProperty(propertyName).SetValue(obj, propertyValue.ToString().Trim());
            }
        }
    }
     
}