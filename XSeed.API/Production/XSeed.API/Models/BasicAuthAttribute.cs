﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using XSeed.Business.Registration;

namespace XSeed.API.Models
{
    public class BasicAuthAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            /* Get Header */
            var headers = actionContext.Request.Headers;

            if (headers != null && headers.Contains("UserId") && headers.Contains("Token"))
            {
                var userId = headers.GetValues("UserId").First();
                var token = headers.GetValues("Token").First();

                RegisterCandidate registerCandidate = new RegisterCandidate();
                ////Check if user is already registered
                if (!registerCandidate.IsAuthenticated(userId, token))
                {
                    throw new UnauthorizedAccessException();
                };

                return;
            }
            else
            {
                throw new UnauthorizedAccessException();
            }
        }
    }
}