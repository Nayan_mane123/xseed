﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XSeed.Business.BooleanQuery;
using XSeed.Data.ViewModel.BooleanSearch;

namespace XSeed.API.Controllers
{
    public class BooleanSearchController : BaseController
    {

        /// <summary>
        /// Get List of all boolean queries which are saved by the all organization users
        /// </summary>        
        /// <param name="organizationId">OrganizationId</param>
        /// <returns>List of boolean queries</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public IHttpActionResult Get(Guid organizationId)
        {
            var model = BooleanQueryInfo.ListAllBooleanQueries(organizationId);
            return Ok(model.ToList());
        }

        /// <summary>
        /// Save boolean query which is execued by an organization user
        /// </summary>
        /// <param name="booleanQueryModel">BooleanQueryModel</param>
        /// <remarks>Save boolean query which is executed</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>  
        public IHttpActionResult Post(BooleanQueryModel booleanQueryModel)
        {
            if (ModelState.IsValid)
            {
                BooleanQueryInfo.SaveBooleanQuery(booleanQueryModel);
                return Ok();
            }
            else
            {
                return BadRequest(ModelState);
            }
        }
    }
}
