﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XSeed.Business.ResumeUpload;
using XSeed.Data.ViewModel.Common;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Resume Upload Controller
    /// </summary>
    public class ResumeUploadController : BaseController
    {
        /// <summary>
        /// Resume Bulk Upload
        /// </summary>
        /// <param name="Resumes">List of Resumes</param>
        /// <param name="Id">Id</param>
        /// <returns>Count of uploaded files</returns>
        [HttpPost]
        public IHttpActionResult ResumeBulkUpload(List<XSeedFileEntity> Resumes, string Id = null)
        {
            ResumeUploadInfo model = new ResumeUploadInfo();
            return Ok(model.SaveResumeUploadFiles(Resumes, Id));
        }

        /// <summary>
        /// Get Parsed Resumes
        /// </summary>
        /// <returns>List of Parsed Resumes</returns>
        [HttpGet]
        public IHttpActionResult GetParsedResumes(int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc")
        {
            ResumeUploadInfo model = new ResumeUploadInfo();

            /* Set Pagination Info */
            var paginationInfo = model.GetPaginationInfo(pageSize);

            var result = new
            {
                TotalCount = paginationInfo.TotalCount,
                TotalPages = paginationInfo.TotalPages,
                ParsedResumes = model.GetParsedResumes(pageSize, pageNumber, sortBy, sortOrder)
            };
            return Ok(result); // return parsed resume list
        }

        /// <summary>
        /// Reject Parsed Resume
        /// </summary>
        /// <param name="Id">Parsed Resume Id</param>
        /// <returns></returns>
        [HttpPut]
        public IHttpActionResult AcceptRejectParsedResume(string Id, bool IsValidResume = false)
        {
            ResumeUploadInfo model = new ResumeUploadInfo();
            model.AcceptRejectParsedResume(Id, IsValidResume);
            return Ok();
        }
    }
}
