﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using XSeed.API.Models;
using XSeed.Business.Candidate;
using XSeed.Data.ViewModel.Candidate;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Candidate API
    /// </summary>
    public class CandidateController : BaseController
    {
        /// <summary>
        /// Get list of candidate(s)
        /// </summary>        
        /// <param name="Id">Id</param>
        /// <remarks>Get user/ users of the organization 
        /// - Id is an optional parameter
        /// - Id with value as zero will return list of users belongs to an organization
        /// - Id with value as positive integer will return single users details
        ///</remarks>
        /// <returns>Candidate(s)</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Candidate_Read", ClaimValue = "True")]
        public IHttpActionResult Get(Guid? Id = null)
        {
            CandidateInfo model = new CandidateInfo();

            if (Id == null)
            {
                return Ok(model.ListCandidates()); // return candidate list
            }
            else
            {
                return Ok(model.GetCandidateDetail(Id)); // return single candidate
            }
        }

        /// <summary>
        /// Get list of candidates using server side pagination
        /// </summary>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>        
        /// <returns>Candidate List</returns>
        // GET: api/Candidate/pageSize/pageNumber/
        // [Route("{pageSize:int}/{pageNumber:int}")]
        [ClaimsAuthorization(ClaimType = "Candidate_Read", ClaimValue = "True")]
        public IHttpActionResult Get(int pageSize, int pageNumber)
        {
            CandidateInfo model = new CandidateInfo();

            /* Set Pagination Info */
            var paginationInfo = model.GetPaginationInfo(pageSize);

            var result = new
            {
                TotalCount = paginationInfo.TotalCount,
                TotalPages = paginationInfo.TotalPages,
                Candidates = model.ListCandidates(pageSize, pageNumber)
            };
            return Ok(result); // return candidate list
        }

        /// <summary>
        /// Create a new candidate
        /// </summary>
        /// <param name="candidateDetailModel">Candidate detail model </param>
        /// <remarks>Create a new candidate</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>   
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Candidate_Create", ClaimValue = "True")]
        public IHttpActionResult Post(CandidateDetailModel candidateDetailModel)
        {
            CandidateInfo model = new CandidateInfo();
            return Ok(model.CreateCandidateDetail(candidateDetailModel));
        }

        /// <summary>
        /// Update the details for a candidate
        /// </summary>
        /// <param name="candidateDetailModel">Candidate detail model </param>
        /// <remarks>Update the details about the Candidate</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPut]
        [ClaimsAuthorization(ClaimType = "Candidate_Update", ClaimValue = "True")]
        public IHttpActionResult Put(CandidateDetailModel candidateDetailModel)
        {
            CandidateInfo model = new CandidateInfo();
            model.UpdateCandidateDetail(candidateDetailModel);
            return Ok();
        }
    }
}
