﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XSeed.Business.APILogger;

namespace XSeed.API.Controllers
{
    public class ApiLogController : ApiController
    {
        [HttpGet]
        public IHttpActionResult Get(string Id = null)
        {
            ApiLogInfo model = new ApiLogInfo();

            if (Id == null)
            {
                //return Ok(model.ListApiLog()); // return candidate list
            }
            else
            {
                return Ok(model.GetApiLogDetail(Id)); // return single candidate
            }
            return Ok();
        }

        /// <summary>
        /// Get list of candidates using server side pagination
        /// </summary>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>        
        /// <returns>Candidate List</returns>
        ///GET: api/Candidate/pageSize/pageNumber/
        [HttpGet]
        public IHttpActionResult Get(int pageSize, int pageNumber)
        {
            ApiLogInfo model = new ApiLogInfo();

            /* Set Pagination Info */
            var paginationInfo = model.GetPaginationInfo(pageSize);

            var result = new
            {
                TotalCount = paginationInfo.TotalCount,
                TotalPages = paginationInfo.TotalPages,
                ApiLog = model.ListApiLog(pageSize, pageNumber)
            };
            return Ok(result); // return candidate list
        }
    }
}
