﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using XSeed.Business.ResumeBlaster;
using XSeed.Data.Entity;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Notification;
using XSeed.Data.ViewModel.ResumeBlaster;
using XSeed.Utility;

namespace XSeed.API.Controllers
{
    public class ResumeBlasterController : BaseController
    {
        private XSeedEntities db = new XSeedEntities();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public IHttpActionResult Get(Guid? Id = null)
        {
            ResumeBlasterInfo model = new ResumeBlasterInfo();

            if (Id == null)
            {
                return Ok(model.ListResumeBlasters()); // return recruiter list
            }
            else
            {
                return Ok(model.GetResumeBlasterDetail(Id)); // return single recruiter
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public IHttpActionResult BlastMail(SendResumeMail model)
        {

            if (model.Resume != null)
            {
                ///* Get file data */
                //byte[] data = System.Convert.FromBase64String(model.Resume.data.Split(',')[1]);

                //using (MemoryStream stream = new MemoryStream(data, true))
                //{
                //    stream.Write(data, 0, data.Length);

                //    string mimeType = MimeMapping.GetMimeMapping(model.Resume.name);

                //    System.IO.MemoryStream ms = new System.IO.MemoryStream(data, true);

                //    System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(ms, model.Resume.name, mimeType);
                //    attach.ContentDisposition.FileName = model.Resume.name;

                    foreach (var item in model.ResumeBlasterList)
                    {
                        EmailNotificationModel emailNotificationModel = new EmailNotificationModel();
                        emailNotificationModel.ToEmailID = new List<string>();
                        emailNotificationModel.ToEmailID.Add(item);
                        emailNotificationModel.FromEmailID = ConfigurationManager.AppSettings["EmailFromUserName"];
                        emailNotificationModel.FromEmailPassword = ConfigurationManager.AppSettings["EmailFromPassword"];
                        //emailNotificationModel.mailAttachment = new Attachment(stream, model.Resume.name, mimeType);
                        emailNotificationModel.MailAttachment = new List<XSeedFileEntity>();
                        emailNotificationModel.MailAttachment.Add(model.Resume);
                        emailNotificationModel.TypeOfNotification = XSeed.Utility.Constants.typeOfNotificationResumeAttachment;
                        NotificationController notificationController = new NotificationController();
                        notificationController.SendEmailNotification(emailNotificationModel);
                    }

                   
                //}
            }

            return Ok(); // return single recruiter
        }

        // POST api/ResumeBlaster
        [HttpPost]
        public IHttpActionResult Post(ResumeBlasterDetailModel resumeBlasterDetailModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ResumeBlasterInfo model = new ResumeBlasterInfo();
            return Ok(model.CreateResumeBlasterDetail(resumeBlasterDetailModel));

        }


        // PUT api/ResumeBlaster/5
        [HttpPut]
        public IHttpActionResult Put(ResumeBlasterDetailModel resumeBlasterDetailModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ResumeBlasterInfo model = new ResumeBlasterInfo();
            model.UpdateResumeBlasterDetail(resumeBlasterDetailModel);
            return Ok();
        }

        private string GetMimeType(string fileName)
        {
            string mimeType = "application/unknown";
            string ext = System.IO.Path.GetExtension(fileName).ToLower();
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey != null && regKey.GetValue("Content Type") != null)
                mimeType = regKey.GetValue("Content Type").ToString();
            return mimeType;
        }


    }
}