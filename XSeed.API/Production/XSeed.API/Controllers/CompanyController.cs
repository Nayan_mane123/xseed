﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using XSeed.API.Models;
using XSeed.Business.Company;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Company;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Company API
    /// </summary>        
    public class CompanyController : BaseController
    {
        /// <summary>
        /// Get companies belongs to an organization
        /// </summary>        
        /// <param name="organizationId">Organization Id</param>
        /// <param name="Id">Id</param>
        /// <remarks>Get company/ Companies
        /// - Organization Id is required parameter
        /// - Id is an optional parameter
        /// - Id with value as zero will return list of companies belongs to an organization
        /// - Id with value as positive integer will return single company details
        ///</remarks>
        /// <returns>Companies</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>        
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Company_Read", ClaimValue = "True")]
        [ResponseType(typeof(List<CompanyDetailModel>))]
        public IHttpActionResult Get(Guid organizationId, Guid? Id = null)
        {
            CompanyInfo model = new CompanyInfo();

            if (Id == null)
            {
                return Ok(model.ListAllCompanies(organizationId)); // return company list
            }
            else
            {
                return Ok(model.GetCompanyDetail(organizationId, Id)); // return single company
            }
        }

        /// <summary>
        /// Create new company
        /// </summary>
        /// <param name="companyDetailModel">Company detail model </param>
        /// <remarks>Insert new company</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>        
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Company_Create", ClaimValue = "True")]
        public IHttpActionResult Post(CompanyDetailModel companyDetailModel)
        {
            CompanyInfo model = new CompanyInfo();
            return Ok(model.CreateCompanyDetail(companyDetailModel));
        }

        /// <summary>
        /// Update company
        /// </summary>
        /// <param name="companyDetailModel">Company detail model </param>
        /// <remarks>Update company</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPut]
        [ClaimsAuthorization(ClaimType = "Company_Update", ClaimValue = "True")]
        public IHttpActionResult Put(CompanyDetailModel companyDetailModel)
        {
            CompanyInfo model = new CompanyInfo();
            model.UpdateCompanyDetail(companyDetailModel);
            return Ok();
        }

        /// <summary>
        /// Create new company source
        /// </summary>
        /// <param name="lookUpModel">LookUp model </param>
        /// <param name="Id">OrganizationId</param>
        /// <remarks>Insert new company</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Company_Create", ClaimValue = "True")]
        public IHttpActionResult PostCompanySource(LookUpModel lookUpModel)
        {
            CompanyInfo model = new CompanyInfo();
            return Ok(model.CreateCompanySource(lookUpModel));
        }


        /// <summary>
        /// Update company source
        /// </summary>
        /// <param name="lookUpModel">LookUp model</param>
        /// <param name="Id"> OrganizationId </param>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPut]
        [ClaimsAuthorization(ClaimType = "Company_Update", ClaimValue = "True")]
        public async Task<IHttpActionResult> PutCompanySource(LookUpModel lookUpModel)
        {
            CompanyInfo.UpdateCompanySource(lookUpModel);
            return Ok();
        }

        /// <summary>
        /// Delete company source
        /// </summary>
        /// <param name="genericLookupId"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteCompanySource(Guid genericLookupId)
        {
            if (genericLookupId == null || genericLookupId == Guid.Empty)
            {
                throw new ArgumentNullException("CompanySourceId"); ;
            }

            CompanyInfo.DeleteCompanySource(genericLookupId);
            return Ok();
        }

        /// <summary>
        /// Search Company Information.
        /// </summary>
        /// <param name="searchText">searchText</param>
        /// <returns>List<CompanyDataModel></returns>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Company_Read", ClaimValue = "True")]
        public IHttpActionResult SearchCompany(string searchText)
        {
            CompanyInfo model = new CompanyInfo();
            return Ok(model.SearchCompanyData(searchText));
        }

        //[HttpGet]        
        //[AllowAnonymous]
        //public IHttpActionResult UpdateCompany()
        //{
        //    CompanyDataModel model = new CompanyDataModel();
        //    model.SaveCompanyLogo();
        //    return Ok();
        //}
    }
}