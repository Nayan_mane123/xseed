﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XSeed.Business.Notification;
using XSeed.Data.ViewModel.Notification;

namespace XSeed.API.Controllers
{
    public class NotificationController : BaseController
    {
        /// <summary>
        /// Sends Email Notification
        /// </summary>
        /// <param name="emailNotificationModel">EmailNotificationModel</param>
        /// <returns>Status Ok - 200</returns>
        public IHttpActionResult SendEmailNotification(EmailNotificationModel emailNotificationModel)
        {
            if (ModelState.IsValid)
            {
                NotificationInfo.SendEmailNotification(emailNotificationModel);
                return Ok();
            }
            return BadRequest(ModelState);
        }

        /// <summary>
        /// Sends Email Notification related to job
        /// </summary>
        /// <param name="emailNotificationModel">EmailNotificationModel</param>
        /// /// <param name="jobId">Job Id</param>
        /// <returns>Status Ok - 200</returns>
        public IHttpActionResult SendJobEmailNotification(EmailNotificationModel emailNotificationModel, Nullable<Guid> jobId)
        {
            if (ModelState.IsValid)
            {
                NotificationInfo.SendJobEmailNotification(emailNotificationModel, jobId);
                return Ok();
            }
            return BadRequest(ModelState);
        }

        /// <summary>
        /// Sends Email Notification related to candidate
        /// </summary>
        /// <param name="emailNotificationModel">EmailNotificationModel</param>
        /// /// <param name="jobId">Job Id</param>
        /// <returns>Status Ok - 200</returns>
        public IHttpActionResult SendCandidateEmailNotification(EmailNotificationModel emailNotificationModel, string candidateId)
        {
            if (ModelState.IsValid)
            {
                NotificationInfo.SendCandidateEmailNotification(emailNotificationModel, candidateId);
                return Ok();
            }
            return BadRequest(ModelState);
        }
    }
}
