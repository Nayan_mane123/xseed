﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using XSeed.API.Models;
using XSeed.Data.Entity;

namespace XSeed.API.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using XSeed.Data.Entity;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<JobDetail>("Requirement");
    builder.EntitySet<CityMaster>("CityMasters"); 
    builder.EntitySet<CountryMaster>("CountryMasters"); 
    builder.EntitySet<JobTypeMaster>("JobTypeMasters"); 
    builder.EntitySet<StateMaster>("StateMasters"); 
    builder.EntitySet<CompanyContact>("CompanyContacts"); 
    builder.EntitySet<CompanyDetail>("CompanyDetails"); 
    builder.EntitySet<JobStatusMaster>("JobStatusMasters"); 
    builder.EntitySet<JobTitleMaster>("JobTitleMasters"); 
    builder.EntitySet<SubmissionDetail>("SubmissionDetail"); 
    builder.EntitySet<DegreeMaster>("DegreeMaster"); 
    builder.EntitySet<OrganizationUserDetail>("OrganizationUserDetail"); 
    builder.EntitySet<SkillMaster>("SkillMaster"); 
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class RequirementController : ODataController
    {
        private XSeedEntities db = new XSeedEntities();

        // GET odata/Requirement
        [Queryable]
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")] 
        public IQueryable<JobDetail> GetRequirement()
        {
            return db.JobDetails;
        }

        // GET odata/Requirement(5)
        [Queryable]
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")] 
        public SingleResult<JobDetail> GetJobDetail([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.JobDetails.Where(jobdetail => jobdetail.Id == key));
        }

        // PUT odata/Requirement(5)
        [HttpPut]
        [ClaimsAuthorization(ClaimType = "Requirement_Update", ClaimValue = "True")] 
        public IHttpActionResult Put([FromODataUri] Guid key, JobDetail jobdetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (key != jobdetail.Id)
            {
                return BadRequest();
            }

            db.Entry(jobdetail).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JobDetailExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(jobdetail);
        }

        // POST odata/Requirement
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Requirement_Create", ClaimValue = "True")] 
        public IHttpActionResult Post(JobDetail jobdetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.JobDetails.Add(jobdetail);
            db.SaveChanges();

            return Created(jobdetail);
        }

        // PATCH odata/Requirement(5)
        [AcceptVerbs("PATCH", "MERGE")]
        [ClaimsAuthorization(ClaimType = "Requirement_Update", ClaimValue = "True")] 
        public IHttpActionResult Patch([FromODataUri] Guid key, Delta<JobDetail> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            JobDetail jobdetail = db.JobDetails.Find(key);
            if (jobdetail == null)
            {
                return NotFound();
            }

            patch.Patch(jobdetail);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JobDetailExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(jobdetail);
        }

        // DELETE odata/Requirement(5)
        [HttpDelete]
        [ClaimsAuthorization(ClaimType = "Requirement_Delete", ClaimValue = "True")] 
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            JobDetail jobdetail = db.JobDetails.Find(key);
            if (jobdetail == null)
            {
                return NotFound();
            }

            db.JobDetails.Remove(jobdetail);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET odata/Requirement(5)/CityMaster
        [Queryable]
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")] 
        public SingleResult<CityMaster> GetCityMaster([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.JobDetails.Where(m => m.Id == key).Select(m => m.CityMaster));
        }

        // GET odata/Requirement(5)/CountryMaster
        [Queryable]
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")] 
        public SingleResult<CountryMaster> GetCountryMaster([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.JobDetails.Where(m => m.Id == key).Select(m => m.CountryMaster));
        }

        // GET odata/Requirement(5)/JobTypeMaster
        [Queryable]
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")] 
        public SingleResult<JobTypeMaster> GetJobTypeMaster([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.JobDetails.Where(m => m.Id == key).Select(m => m.JobTypeMaster));
        }

        // GET odata/Requirement(5)/StateMaster
        [Queryable]
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")] 
        public SingleResult<StateMaster> GetStateMaster([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.JobDetails.Where(m => m.Id == key).Select(m => m.StateMaster));
        }

        // GET odata/Requirement(5)/CompanyContact
        [Queryable]
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")] 
        public SingleResult<CompanyContact> GetCompanyContact([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.JobDetails.Where(m => m.Id == key).Select(m => m.CompanyContact));
        }

        // GET odata/Requirement(5)/CompanyDetail
        [Queryable]
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")] 
        public SingleResult<CompanyDetail> GetCompanyDetail([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.JobDetails.Where(m => m.Id == key).Select(m => m.CompanyDetail));
        }

        // GET odata/Requirement(5)/JobStatusMaster
        [Queryable]
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")] 
        public SingleResult<JobStatusMaster> GetJobStatusMaster([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.JobDetails.Where(m => m.Id == key).Select(m => m.JobStatusMaster));
        }

        // GET odata/Requirement(5)/JobTitleMaster
        [Queryable]
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")] 
        public SingleResult<JobTitleMaster> GetJobTitleMaster([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.JobDetails.Where(m => m.Id == key).Select(m => m.JobTitleMaster));
        }

        // GET odata/Requirement(5)/SubmissionDetails
        [Queryable]
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")] 
        public IQueryable<SubmissionDetail> GetSubmissionDetails([FromODataUri] Guid key)
        {
            return db.JobDetails.Where(m => m.Id == key).SelectMany(m => m.SubmissionDetails);
        }

        // GET odata/Requirement(5)/DegreeMasters
        [Queryable]
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")] 
        public IQueryable<DegreeMaster> GetDegreeMasters([FromODataUri] Guid key)
        {
            return db.JobDetails.Where(m => m.Id == key).SelectMany(m => m.DegreeMasters);
        }

        // GET odata/Requirement(5)/OrganizationUserDetails
        [Queryable]
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")] 
        public IQueryable<OrganizationUserDetail> GetOrganizationUserDetails([FromODataUri] Guid key)
        {
            return db.JobDetails.Where(m => m.Id == key).SelectMany(m => m.OrganizationUserDetails);
        }

        // GET odata/Requirement(5)/SkillMasters
        [Queryable]
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Requirement_Read", ClaimValue = "True")] 
        public IQueryable<SkillMaster> GetSkillMasters([FromODataUri] Guid key)
        {
            return db.JobDetails.Where(m => m.Id == key).SelectMany(m => m.SkillMasters);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool JobDetailExists(Guid key)
        {
            return db.JobDetails.Count(e => e.Id == key) > 0;
        }
    }
}
