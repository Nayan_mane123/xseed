﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using XSeed.Business.Common;
using XSeed.Data.ViewModel.Common;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Lookup API
    /// </summary>
    [AllowAnonymous]
    public class LookUpController : BaseController
    {
        #region Common LookUp

        /// <summary>
        /// /// Get list of all countries
        /// </summary>        
        /// <remarks>list of all countries
        ///</remarks>
        /// <returns>Country list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public async Task<IHttpActionResult> GetCountries()
        {
            return Ok(LookUp.GetCountries().OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get state list by country
        /// </summary>        
        /// <param name="countryId">Country Id</param>
        /// <remarks>list of all states by country
        ///</remarks>
        /// <returns>State list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public async Task<IHttpActionResult> GetStates(Guid countryId)
        {
            return Ok(LookUp.GetStates(countryId).OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get city list by state
        /// </summary>        
        /// <param name="stateId">State Id</param>
        /// <remarks>list of all cities by state
        ///</remarks>
        /// <returns>City list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public async Task<IHttpActionResult> GetCities(Guid stateId)
        {
            return Ok(LookUp.GetCities(stateId).OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get industry type list
        /// </summary>        
        /// <remarks>Get industry type list
        ///</remarks>
        /// <returns>Industry type list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public async Task<IHttpActionResult> GetIndustryTypes()
        {
            return Ok(LookUp.GetIndustryTypes());
        }

        /// <summary>
        /// Get user roles list
        /// </summary>        
        /// <remarks>Get user roles list
        ///</remarks>
        /// <returns>User role list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public async Task<IHttpActionResult> GetUserRoles(Guid organizationId)
        {
            return Ok(LookUp.GetUserRoles(organizationId).OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get company types
        /// </summary>        
        /// <remarks>Get company types
        ///</remarks>
        /// <returns>Company types</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public async Task<IHttpActionResult> GetCompanyTypes()
        {
            return Ok(LookUp.GetCompanyTypes().OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get company sources
        /// </summary>        
        /// <remarks>Get company sources who brought the reference of a company
        ///</remarks>
        ///<param name="Id">OrganizationId</param>
        /// <returns>Company sources</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public async Task<IHttpActionResult> GetCompanySources(Guid organizationId)
        {
            return Ok(LookUp.GetCompanySources(organizationId));
        }

        /// <summary>
        /// Get title/ suffix list
        /// </summary>        
        /// <remarks>Get title/ suffix list
        ///</remarks>
        /// <returns>Title/ Suffix list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public async Task<IHttpActionResult> GetTitles()
        {
            return Ok(LookUp.GetTitles().OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get reporting authorities
        /// </summary>        
        /// <remarks> Get reporting authorities
        ///</remarks>
        /// <returns>Reporting authorities</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public async Task<IHttpActionResult> GetReportingAuthorities(Guid organizationId, Guid? roleId = null)
        {
            return Ok(LookUp.GetReportingAuthorities(organizationId, roleId).OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get job status
        /// </summary>        
        /// <remarks> Get job status like a job is active or closed
        ///</remarks>
        /// <returns>List of job status</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public async Task<IHttpActionResult> GetJobStatus()
        {
            return Ok(LookUp.GetJobStatus().OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get job title
        /// </summary>        
        /// <remarks> Get job title which are active or closed
        ///</remarks>
        /// <returns>List of job titles</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public async Task<IHttpActionResult> GetJobTitles(Guid organizationId)
        {
            return Ok(LookUp.GetJobTitles(organizationId));
        }

        [HttpPost]
        public async Task<IHttpActionResult> CreateJobTitles(LookUpModel lookUpModel)
        {
            LookUp.CreateJobTitles(lookUpModel);
            return Ok();
        }

        [HttpPut]
        public async Task<IHttpActionResult> UpdateJobTitles(LookUpModel lookUpModel)
        {
            LookUp.UpdateJobTitles(lookUpModel);
            return Ok();
        }

        [HttpDelete]
        public async Task<IHttpActionResult> DeleteJobTitles(Guid genericLookupId)
        {
            if (genericLookupId == null || genericLookupId == Guid.Empty)
            {
                throw new ArgumentNullException("JobTitleId"); ;
            }

            LookUp.DeleteJobTitles(genericLookupId);
            return Ok();
        }

        /// <summary>
        /// Get job type
        /// </summary>        
        /// <remarks> Get job type of like a job is full time or part time
        ///</remarks>
        /// <returns>List of job type</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public async Task<IHttpActionResult> GetJobTypes(Guid organizationId)
        {
            return Ok(LookUp.GetJobTypes(organizationId));
        }

        /// <summary>
        /// create job type
        /// </summary>
        /// <param name="lookUpModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> CreateJobTypes(LookUpModel lookUpModel)
        {
            LookUp.CreateJobTypes(lookUpModel);
            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lookUpModel"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IHttpActionResult> UpdateJobTypes(LookUpModel lookUpModel)
        {
            LookUp.UpdateJobTypes(lookUpModel);
            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="genericLookupId"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteJobTypes(Guid genericLookupId)
        {
            if (genericLookupId == null || genericLookupId == Guid.Empty)
            {
                throw new ArgumentNullException("JobTypeId"); ;
            }

            LookUp.DeleteJobTypes(genericLookupId);
            return Ok();
        }

        /// <summary>
        /// Get skills
        /// </summary>        
        /// <remarks> Get list of skills
        ///</remarks>
        /// <returns>List of skills</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public async Task<IHttpActionResult> GetSkills()
        {
            return Ok(LookUp.GetSkills());
        }

        /// <summary>
        /// Get degrees
        /// </summary>        
        /// <remarks> Get list of degrees 
        ///</remarks>
        /// <returns>List of degrees</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public async Task<IHttpActionResult> GetDegrees(Guid organizationId)
        {
            return Ok(LookUp.GetDegrees(organizationId));
        }

        /// <summary>
        /// Create Job Degrees
        /// </summary>
        /// <param name="lookUpModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> CreateJobDegrees(LookUpModel lookUpModel)
        {
            LookUp.CreateJobDegrees(lookUpModel);
            return Ok();
        }

        /// <summary>
        /// Update Job Degrees
        /// </summary>
        /// <param name="lookUpModel"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IHttpActionResult> UpdateJobDegrees(LookUpModel lookUpModel)
        {
            LookUp.UpdateJobDegrees(lookUpModel);
            return Ok();
        }

        /// <summary>
        /// Delete Job Degrees
        /// </summary>
        /// <param name="genericLookupId"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteJobDegrees(Guid genericLookupId)
        {
            if (genericLookupId == null || genericLookupId == Guid.Empty)
            {
                throw new ArgumentNullException("DegreeId"); ;
            }

            LookUp.DeleteJobDegrees(genericLookupId);
            return Ok();
        }


        /// <summary>
        /// Check for Duplicate Lookup Name
        /// </summary>
        /// <param name="lookupName"> lookup Name</param>
        /// <param name="lookupType"> lookup Type</param>
        /// <param name="organizationId"> organization Id</param>
        /// <returns>True/ False</returns>
        [HttpGet]
        public IHttpActionResult IsDuplicateLookupName(string lookupName, string lookupType, Guid organizationId, bool editedValue, string existingValue = "")
        {
            LookUp model = new LookUp();
            return Ok(model.IsDuplicateLookupName(lookupName, lookupType, organizationId, editedValue, existingValue));
        }

        /// <summary>
        /// Get genders
        /// </summary>        
        /// <remarks> Get list of genders 
        ///</remarks>
        /// <returns>List of genders</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public async Task<IHttpActionResult> GetGenders()
        {
            return Ok(LookUp.GetGenders().OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get marital status
        /// </summary>        
        /// <remarks> Get list of marital status
        ///</remarks>
        /// <returns>List of marital status</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        public async Task<IHttpActionResult> GetMaritalStatus()
        {
            return Ok(LookUp.GetMaritalStatus().OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get visa types
        /// </summary>        
        /// <remarks> Get list of visa types which are required to work in other countries
        ///</remarks>
        /// <returns>List of visa types</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        public async Task<IHttpActionResult> GetVisaTypes()
        {
            return Ok(LookUp.GetVisaTypes().OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get submission feedback status
        /// </summary>        
        /// <remarks> Get list submission feedback status required to fill the feedback of submission
        ///</remarks>
        /// <returns>Submission feedback status list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        public async Task<IHttpActionResult> GetSubmissionFeedbackStatus()
        {
            return Ok(LookUp.GetSubmissionFeedbackStatus().OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get client feedback Status
        /// </summary>        
        /// <remarks> Get list feedback status required to fill the feedback of client
        ///</remarks>
        /// <returns>Client feedback Status list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        public async Task<IHttpActionResult> GetClientFeedbackStatus()
        {
            return Ok(LookUp.GetClientFeedbackStatus().OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get candidate feedback status
        /// </summary>        
        /// <remarks> Get list feedback status required to fill the feedback of candidate
        ///</remarks>
        /// <returns>Candidate feedback status list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        public async Task<IHttpActionResult> GetCandidateFeedbackStatus()
        {
            return Ok(LookUp.GetCandidateFeedbackStatus().OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get company name for lookup
        /// </summary>        
        /// <remarks> Get company names of organization
        ///</remarks>
        /// <param name="organizationId">organization Id</param>
        /// <returns>Company names</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        public async Task<IHttpActionResult> GetCompanies(Guid organizationId)
        {
            return Ok(LookUp.GetCompanies(organizationId).OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get Organization Users
        /// </summary>  
        /// <param name="organizationId">Organization Id</param>
        /// <remarks> Get list of users belongs to an Organization
        ///</remarks>
        /// <returns>Organization user list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        public async Task<IHttpActionResult> GetOrganizationUsers(Guid organizationId)
        {
            return Ok(LookUp.GetOrganizationUsers(organizationId).OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get Business Units
        /// </summary>        
        /// <returns>Business Units</returns>
        public async Task<IHttpActionResult> GetBusinessUnits(Guid organizationId)
        {
            return Ok(LookUp.GetBusinessUnits(organizationId));
        }

        /// <summary>
        /// Get candidate status
        /// </summary>        
        /// <remarks> Get candidate status 
        ///</remarks>
        /// <returns>List of candidate status</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public async Task<IHttpActionResult> GetCandidateStatus()
        {
            return Ok(LookUp.GetCandidateStatus().OrderBy(l => l.Name));
        }

        /// <summary>
        /// Get candidate categories
        /// </summary>        
        /// <remarks>Get candidate categories
        ///</remarks>        
        /// <returns>categories</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public async Task<IHttpActionResult> GetCandidateCategories(Guid organizationId)
        {
            return Ok(LookUp.GetCandidateCategories(organizationId));
        }

        /// <summary>
        /// Create New Candidate Status
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpPost]
        public async Task<IHttpActionResult> CreateCandidateStatus(LookUpModel lookUpModel)
        {
            LookUp.CreateCandidateStatus(lookUpModel);
            return Ok();
        }

        /// <summary>
        /// Update Candidate Status
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpPut]
        public async Task<IHttpActionResult> UpdateCandidateStatus(LookUpModel lookUpModel)
        {
            LookUp.UpdateCandidateStatus(lookUpModel);
            return Ok();
        }

        /// <summary>
        /// Deletes Candidate Status
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteCandidateStatus(Guid genericLookupId)
        {
            if (genericLookupId == null || genericLookupId == Guid.Empty)
            {
                throw new ArgumentNullException("Id");
            }

            LookUp.DeleteCandidateStatus(genericLookupId);
            return Ok();
        }

        /// <summary>
        /// Create New Candidate Category
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpPost]
        public async Task<IHttpActionResult> CreateCandidateCategory(LookUpModel lookUpModel)
        {
            LookUp.CreateCandidateCategory(lookUpModel);
            return Ok();
        }

        /// <summary>
        /// Update Candidate Category
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpPut]
        public async Task<IHttpActionResult> UpdateCandidateCategory(LookUpModel lookUpModel)
        {
            LookUp.UpdateCandidateCategory(lookUpModel);
            return Ok();
        }

        /// <summary>
        /// Deletes Candidate Category
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteCandidateCategory(Guid genericLookupId)
        {
            if (genericLookupId == null || genericLookupId == Guid.Empty)
            {
                throw new ArgumentNullException("Id");
            }

            LookUp.DeleteCandidateCategory(genericLookupId);
            return Ok();
        }

        /// <summary>
        /// Get candidate groups
        /// </summary>        
        /// <remarks>Get candidate groups
        ///</remarks>        
        /// <returns>Groups</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public async Task<IHttpActionResult> GetCandidateGroups(Guid organizationId)
        {
            return Ok(LookUp.GetCandidateGroups(organizationId));
        }

        /// <summary>
        /// Create New Candidate Group
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpPost]
        public async Task<IHttpActionResult> CreateCandidateGroup(LookUpModel lookUpModel)
        {
            LookUp.CreateCandidateGroup(lookUpModel);
            return Ok();
        }

        /// <summary>
        /// Update Candidate Group
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpPut]
        public async Task<IHttpActionResult> UpdateCandidateGroup(LookUpModel lookUpModel)
        {
            LookUp.UpdateCandidateGroup(lookUpModel);
            return Ok();
        }

        /// <summary>
        /// Deletes Candidate Group
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteCandidateGroup(Guid genericLookupId)
        {
            if (genericLookupId == null || genericLookupId == Guid.Empty)
            {
                throw new ArgumentNullException("Id");
            }

            LookUp.DeleteCandidateGroup(genericLookupId);
            return Ok();
        }

        /// <summary>
        /// Get candidate sources
        /// </summary>        
        /// <returns>Sources</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public async Task<IHttpActionResult> GetCandidateSources(Guid organizationId)
        {
            return Ok(LookUp.GetCandidateSources(organizationId));
        }

        /// <summary>
        /// Create New Candidate Source
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpPost]
        public async Task<IHttpActionResult> CreateCandidateSource(LookUpModel lookUpModel)
        {
            LookUp.CreateCandidateSource(lookUpModel);
            return Ok();
        }

        /// <summary>
        /// Update Candidate Source
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpPut]
        public async Task<IHttpActionResult> UpdateCandidateSource(LookUpModel lookUpModel)
        {
            LookUp.UpdateCandidateSource(lookUpModel);
            return Ok();
        }

        /// <summary>
        /// Deletes Candidate Source
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteCandidateSource(Guid genericLookupId)
        {
            if (genericLookupId == null || genericLookupId == Guid.Empty)
            {
                throw new ArgumentNullException("Id");
            }

            LookUp.DeleteCandidateSource(genericLookupId);

            return Ok();
        }


        /// <summary>
        /// Get candidate Languages
        /// </summary>            
        /// <returns>Language</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public async Task<IHttpActionResult> GetLanguages()
        {
            return Ok(LookUp.GetLanguages().OrderBy(l => l.Name));
        }

        /// <summary>
        /// Create New Candidate Language
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpPost]
        public async Task<IHttpActionResult> CreateLanguage(LookUpModel lookUpModel)
        {
            LookUp.CreateLanguage(lookUpModel);
            return Ok();
        }

        /// <summary>
        /// Update Candidate Language
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpPut]
        public async Task<IHttpActionResult> UpdateLanguage(LookUpModel lookUpModel)
        {
            LookUp.UpdateLanguage(lookUpModel);
            return Ok();
        }

        /// <summary>
        /// Deletes Candidate Language
        /// </summary>
        /// <param name="lookUpModel">lookUp Model</param>
        /// <returns>HTTP OK Resopnse</returns>
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteLanguage(Guid genericLookupId)
        {
            if (genericLookupId == null || genericLookupId == Guid.Empty)
            {
                throw new ArgumentNullException("Id");
            }

            LookUp.DeleteLanguage(genericLookupId);

            return Ok();
        }


        /// <summary>
        /// Get Salary Types
        /// </summary>        
        /// <remarks> Get Salary Types
        ///</remarks>
        /// <returns>List of Salary Type</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public async Task<IHttpActionResult> GetSalaryType()
        {
            return Ok(LookUp.GetSalaryType());
        }

        /// <summary>
        /// Get Currency Types
        /// </summary>        
        /// <remarks> Get Currency Types
        ///</remarks>
        /// <returns>List of Currency Type</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        public async Task<IHttpActionResult> GetCurrencyType()
        {
            return Ok(LookUp.GetCurrencyType());
        }

        [HttpPost]
        public async Task<IHttpActionResult> CreateCity(string city, Guid stateId)
        {
            if (!string.IsNullOrEmpty(city) && stateId != null)
            {
                return Ok(LookUp.CreateCity(city, stateId));
            }
            return BadRequest();
        }

        #endregion

        #region Conditional LookUp

        /// <summary>
        /// Get Company Contacts
        /// </summary>
        /// <param name="companyId">CompanyId</param>
        /// <returns>Company contact list</returns>
        public async Task<IHttpActionResult> GetCompanyContacts(Guid companyId)
        {
            return Ok(LookUp.ListCompanyContacts(companyId)); // return company contact list
        }

        /// <summary>
        /// Get Company Contact emails
        /// </summary>
        /// <param name="companyId">CompanyId</param>
        /// <returns>Company contact email list</returns>
        public async Task<IHttpActionResult> GetSubmissionContactEmail(Guid companyId)
        {
            return Ok(LookUp.ListSubmissionContactEmail(companyId)); // return company contact list
        }

        /// <summary>
        /// Get City and State code based on country 
        /// </summary>
        /// <param name="country">country</param>
        /// <param name="city">city</param>
        /// <returns>City and State Code</returns>
        public IHttpActionResult GetCityStateCode(string country, string city)
        {
            var result = new
            {
                StateId = LookUp.GetStateCode(country, city),
                CityId = LookUp.GetCityCode(country, city)
            };

            return Ok(result); // return City and State code
        }

        #endregion
    }
}