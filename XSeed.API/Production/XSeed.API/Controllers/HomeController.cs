﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Home API
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Index method
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
    }
}
