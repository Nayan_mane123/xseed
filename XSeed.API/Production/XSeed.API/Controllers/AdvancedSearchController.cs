﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XSeed.Business.AdvancedSearch;
using XSeed.Data.ViewModel.AdvancedSearch;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Advanced Search API
    /// </summary>
    public class AdvancedSearchController : BaseController
    {
        /// <summary>
        /// Candidate Advanced Search API
        /// </summary>
        /// <param name="candidateSearchModel">Candidate Search Model</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>
        /// <returns>Candidate List</returns>
        public IHttpActionResult SearchCandidates(CandidateSearchModel candidateSearchModel, int pageSize, int pageNumber)
        {
            int TotalCount = 0; double TotalPages = 0.0;
            CandidateSearchInfo model = new CandidateSearchInfo();

            var candidateList = model.SearchCandidates(candidateSearchModel, pageSize, pageNumber, out TotalCount, out TotalPages);

            var result = new
            {
                TotalCount = TotalCount.ToString(),
                TotalPages = TotalPages.ToString(),
                Candidates = candidateList
            };

            return Ok(result); // return candidate list
        }

        /// <summary>
        /// Job Advanced Search API
        /// </summary>
        /// <param name="jobSearchModel">Job Search Model</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>
        /// <returns>Job List</returns>
        public IHttpActionResult SearchJobs(JobSearchModel jobSearchModel, int pageSize, int pageNumber)
        {
            int TotalCount = 0; double TotalPages = 0.0;
            JobSearchInfo model = new JobSearchInfo();

            var jobList = model.SearchJobs(jobSearchModel, pageSize, pageNumber, out TotalCount, out TotalPages);

            var result = new
            {
                TotalCount = TotalCount.ToString(),
                TotalPages = TotalPages.ToString(),
                Jobs = jobList
            };

            return Ok(result); // return job list
        }

        /// <summary>
        /// Job Advanced Search API
        /// </summary>
        /// <param name="jobAdvancedSearchModel">job Search Model</param>        
        /// <param name="pageSize">Page Size</param>
        /// <param name="pageNumber">Page Number</param>
        /// <param name="sortBy">Sort By</param>
        /// <param name="sortOrder">Sort Order</param>
        /// <returns>Job List</returns>
        public IHttpActionResult AdvanceSearchJobs(JobAdvancedSearchModel jobAdvancedSearchModel, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", bool isExport = false)
        {
            int TotalCount = 0; double TotalPages = 0.0;
            JobSearchInfo model = new JobSearchInfo();

            var jobList = model.SearchJobs(jobAdvancedSearchModel, pageSize, pageNumber, sortBy, sortOrder, out TotalCount, out TotalPages, isExport);

            var result = new
            {
                TotalCount = TotalCount.ToString(),
                TotalPages = TotalPages.ToString(),
                Jobs = jobList
            };

            return Ok(result); // return job list
        }

        /// <summary>
        /// Candidate Advanced Search API
        /// </summary>
        /// <param name="candidateAdvancedSearchModel">Candidate Advanced Search Model</param>        
        /// <param name="pageSize">Page Size</param>
        /// <param name="pageNumber">Page Number</param>
        /// <param name="sortBy">Sort By</param>
        /// <param name="sortOrder">Sort Order</param>
        /// <returns>Candidate List</returns>        
        public IHttpActionResult AdvanceSearchCandidates(CandidateAdvancedSearchModel candidateAdvancedSearchModel, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", bool isExport = false)
        {
            int TotalCount = 0; double TotalPages = 0.0;
            CandidateSearchInfo model = new CandidateSearchInfo();

            var candidateList = model.SearchCandidates(candidateAdvancedSearchModel, pageSize, pageNumber, sortBy, sortOrder, out TotalCount, out TotalPages, isExport);

            var result = new
            {
                TotalCount = TotalCount.ToString(),
                TotalPages = TotalPages.ToString(),
                Candidates = candidateList
            };

            return Ok(result); // return job list
        }

        /// <summary>
        /// Advance Search Submissions
        /// </summary>
        /// <param name="submissionAdvancedSearchModel">submissionAdvancedSearchModel</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>
        /// <param name="sortBy">sortBy</param>
        /// <param name="sortOrder">sortOrder</param>
        /// <param name="isExport">isExport</param>
        /// <returns>Submissions List</returns>
        public IHttpActionResult AdvanceSearchSubmissions(SubmissionAdvancedSearchModel submissionAdvancedSearchModel, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", bool isExport = false)
        {
            int TotalCount = 0; double TotalPages = 0.0;
            SubmissionSearchInfo model = new SubmissionSearchInfo();

            var submisssionList = model.SearchSubmissions(submissionAdvancedSearchModel, pageSize, pageNumber, sortBy, sortOrder, out TotalCount, out TotalPages, isExport);

            var result = new
            {
                TotalCount = TotalCount.ToString(),
                TotalPages = TotalPages.ToString(),
                Submissions = submisssionList
            };

            return Ok(result); // return job list
        }
    }
}
