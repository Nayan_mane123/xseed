﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XSeed.API.Models;
using XSeed.Business.Reports;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Report API
    /// </summary>
    public class ReportController : BaseController
    {
        /// <summary>
        /// Get Candidate Added by recruiters Report
        /// </summary>        
        /// <remarks>Get Candidate Added by recruiters Report
        ///</remarks>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Candidate added report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult GetCandidateAddedReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", int daysBefore = -7, bool isExport = false)
        {
            CandidateAddedReportInfo model = new CandidateAddedReportInfo();

            /* Set Pagination Info */
            var paginationInfo = model.GetPaginationInfo(organizationId, pageSize, daysBefore);

            /* return job list */
            var result = new
            {
                TotalCount = paginationInfo.TotalCount,
                TotalPages = paginationInfo.TotalPages,
                ReportData = model.GetCandidateAddedReport(organizationId, pageSize, pageNumber, sortBy, sortOrder,daysBefore, isExport)
            };

            return Ok(result);
        }

        /// <summary>
        /// Get Requirement Tracker Report
        /// </summary>        
        /// <remarks>Get Requirement Tracker Report
        ///</remarks>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Requirement tracker report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult GetRequirementTrackerReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", int daysBefore = -7, bool isExport = false)
        {
            RequirementTrackerReportInfo model = new RequirementTrackerReportInfo();

            /* Set Pagination Info */
            var paginationInfo = model.GetPaginationInfo(organizationId, pageSize, daysBefore);

            /* return job list */
            var result = new
            {
                TotalCount = paginationInfo.TotalCount,
                TotalPages = paginationInfo.TotalPages,
                ReportData = model.GetRequirementTrackerReport(organizationId, pageSize, pageNumber, sortBy, sortOrder, daysBefore, isExport)
            };

            return Ok(result);
        }

        /// <summary>
        /// Get Submission Report
        /// </summary>        
        /// <remarks>Get Submission Report
        ///</remarks>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Submission report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult GetSubmissionTrackerReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", int daysBefore = -7, bool isExport = false)
        {
            SubmissionReportInfo model = new SubmissionReportInfo();

            /* Set Pagination Info */
            var paginationInfo = model.GetPaginationInfo(organizationId, pageSize, daysBefore);

            /* return job list */
            var result = new
            {
                TotalCount = paginationInfo.TotalCount,
                TotalPages = paginationInfo.TotalPages,
                ReportData = model.GetSubmissionTrackerReport(organizationId, pageSize, pageNumber, sortBy, sortOrder, daysBefore, isExport)
            };

            return Ok(result);
        }

        /// <summary>
        /// Get Company Lead Report
        /// </summary>        
        /// <remarks>Get Company Lead Report </remarks>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Company Lead report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult GetCompanyLeadReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", int daysBefore = -7, bool isExport = false)
        {
            CompanyLeadReportInfo model = new CompanyLeadReportInfo();

            /* Set Pagination Info */
            var paginationInfo = model.GetPaginationInfo(organizationId, pageSize, daysBefore);

            /* return job list */
            var result = new
            {
                TotalCount = paginationInfo.TotalCount,
                TotalPages = paginationInfo.TotalPages,
                ReportData = model.GetCompanyLeadReport(organizationId, pageSize, pageNumber, sortBy, sortOrder, daysBefore, isExport)
            };

            return Ok(result);
        }

        /// <summary>
        /// Get Activity Report
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Activity Report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult GetActivityReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", int daysBefore = -7, bool isExport = false)
        {
            ActivityReportInfo model = new ActivityReportInfo();

            /* Set Pagination Info */
            var paginationInfo = model.GetPaginationInfo(organizationId, pageSize, daysBefore);

            /* return job list */
            var result = new
            {
                TotalCount = paginationInfo.TotalCount,
                TotalPages = paginationInfo.TotalPages,
                ReportData = model.GetActivityReport(organizationId, pageSize, pageNumber, sortBy, sortOrder, daysBefore, isExport)
            };

            return Ok(result);
        }

        /// <summary>
        /// Get Closure Report
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <returns>Closure Report</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Reports_Read", ClaimValue = "True")]
        public IHttpActionResult GetClosureReport(Guid organizationId, int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", int daysBefore = -7, bool isExport = false)
        {
            ClosureReportInfo model = new ClosureReportInfo();

            /* Set Pagination Info */
            var paginationInfo = model.GetPaginationInfo(organizationId, pageSize, daysBefore);

            /* return job list */
            var result = new
            {
                TotalCount = paginationInfo.TotalCount,
                TotalPages = paginationInfo.TotalPages,
                ReportData = model.GetClosureReport(organizationId, pageSize, pageNumber, sortBy, sortOrder, daysBefore, isExport)
            };

            return Ok(result);
        }
    }
}
