﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XSeed.API.Models;
using XSeed.Data.Submission;
using XSeed.Data.ViewModel.Submission;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Candidate feedback API
    /// </summary>
    public class CandidateFeedbackController : BaseController
    {

        /// <summary>
        /// Get candidate feedback list for particular submission
        /// </summary>        
        /// <param name="submissionId">Submission Id</param>
        /// <remarks>Get submission details for a candidate
        /// - Submission Id is required parameter
        ///</remarks>
        /// <returns>Candidate feedback list</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Submission_Read", ClaimValue = "True")]
        public IHttpActionResult Get(Guid submissionId)
        {
            CandidateFeedbackInfo model = new CandidateFeedbackInfo();
            return Ok(model.GetCandidateFeedbackList(submissionId));
        }

        /// <summary>
        /// Add candidate feedback
        /// </summary>
        /// <param name="candidateFeedbackModel">Candidate feedback model </param>
        /// <remarks>Add feedback from candidate</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpPost]
        [ClaimsAuthorization(ClaimType = "Submission_Update", ClaimValue = "True")]
        public IHttpActionResult Post(CandidateFeedbackModel candidateFeedbackModel)
        {
            CandidateFeedbackInfo model = new CandidateFeedbackInfo();
            model.AddCandidateFeedback(candidateFeedbackModel);
            return Ok();
        }
    }
}
