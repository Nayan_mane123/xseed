﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XSeed.API.Models;
using XSeed.Business.Registration;
using XSeed.Business.User.CandidateUserInfo;
using XSeed.Data.ViewModel.Common;
using XSeed.Data.ViewModel.Registration;
using XSeed.Data.ViewModel.User.CandidateUser;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Candidate User API
    /// </summary>
    [AllowAnonymous]
    public class CandidateUserController : BaseController
    {
        #region Candidate User

        /// <summary>
        /// Get list of candidate user(s)
        /// </summary>        
        /// <param name="Id">Id</param>
        /// <remarks>Get candidate user
        /// - Id is an optional parameter
        /// - Id with value as zero will return list of candidate users
        /// - Id with value will return single candidate user details
        ///</remarks>
        /// <returns>Candidate User(s)</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpGet]
        public IHttpActionResult Get(string Id = null)
        {
            CandidateUserInfo model = new CandidateUserInfo();

            if (Id == null)
            {
                return Ok(model.ListCandidateUsers()); // return candidate list
            }
            else
            {
                return Ok(model.GetCandidateUserDetail(Id)); // return single candidate
            }
        }

        /// <summary>
        /// Get list of candidates using server side pagination
        /// </summary>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>        
        /// <returns>Candidate List</returns>
        // GET: api/Candidate/pageSize/pageNumber/
        // [Route("{pageSize:int}/{pageNumber:int}")]       
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Candidate_Read", ClaimValue = "True")]
        public IHttpActionResult Get(int pageSize, int pageNumber, string sortBy = "CreatedOn", string sortOrder = "desc", bool isExport = false)
        {
            CandidateUserInfo model = new CandidateUserInfo();

            /* Set Pagination Info */
            var paginationInfo = model.GetPaginationInfo(pageSize);

            var result = new
            {
                TotalCount = paginationInfo.TotalCount,
                TotalPages = paginationInfo.TotalPages,
                Candidates = model.ListCandidateUsers(pageSize, pageNumber, sortBy, sortOrder, isExport)
            };
            return Ok(result); // return candidate list
        }

        /// <summary>
        /// Create a new candidate user
        /// </summary>
        /// <param name="candidateUserModel">Candidate user model </param>
        /// <remarks>Create a new candidate</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>   
        [HttpPost]
        public IHttpActionResult Post(CandidateUserModel candidateUserModel)
        {
            ////Check if user is already registered
            RegisterCandidate registerCandidate = new RegisterCandidate();

            //Build model to pass to function
            RegisterCandidateModel registerCandidateModel = new RegisterCandidateModel();
            registerCandidateModel.UserName = candidateUserModel.PrimaryEmail.ToLower();

            RegisterCandidateModel candidateUser = registerCandidate.CandidateAlreadyExists(registerCandidateModel);

            if (candidateUser != null)
            {
                if (candidateUser.IsRegistered == true)
                {
                    throw new Exception("Candidate is already registered with XSeed.Com");
                }
                else
                {
                    throw new Exception("Candidate is already exists.");
                }
            }
            else
            {
                CandidateUserInfo model = new CandidateUserInfo();
                return Ok(model.CreateCandidateUserDetail(candidateUserModel));
            }
        }

        /// <summary>
        /// Update candidate user detail
        /// </summary>
        /// <param name="candidateUserModel">Candidate user model </param>
        /// <remarks>Update candidate detail</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpPut]
        public IHttpActionResult Put(CandidateUserModel candidateUserModel)
        {
            CandidateUserInfo model = new CandidateUserInfo();
            model.UpdateCandidateUserDetail(candidateUserModel);
            return Ok();
        }

        /// <summary>
        /// Get Job openings related to candidate skills
        /// </summary>
        /// <param name="candidateId">Candidate Id</param>
        /// <param name="recordCount">Records to be displayed</param>
        /// <returns>Job Openings</returns>
        [BasicAuthAttribute]
        [HttpPost]
        public IHttpActionResult ListJobOpenings(string candidateId = null, int recordCount = 10)
        {
            CandidateUserInfo model = new CandidateUserInfo();
            return Ok(model.ListJobOpenings(candidateId, recordCount)); // return all Job opening list
        }

        /// <summary>
        /// Get Job Detail for candidate
        /// </summary>
        /// <param name="Id">Job Id</param>
        /// <returns>Job Detail</returns>
        [HttpGet]
        [BasicAuthAttribute]
        public IHttpActionResult GetJobDetail(Guid? Id = null)
        {
            CandidateUserInfo model = new CandidateUserInfo();
            return Ok(model.GetJobDetail(Id)); // return Job Detail
        }

        /// <summary>
        /// Add Candidate job application
        /// </summary>        
        /// <param name="applicationModel">Application Model</param>        
        /// <param name="Id">Candidate UserId</param>     
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpPut]
        [BasicAuthAttribute]
        public IHttpActionResult Apply(Application applicationModel, string Id = null)
        {
            CandidateUserInfo model = new CandidateUserInfo();
            model.Apply(applicationModel, Id);
            return Ok();
        }

        /// <summary>
        /// Get candidate job applications 
        /// </summary>
        /// <param name="Id">user Id</param>
        /// <returns>List of job applications</returns>
        [HttpGet]
        [BasicAuthAttribute]
        public IHttpActionResult GetCandidateJobApplications(string Id)
        {
            CandidateUserInfo model = new CandidateUserInfo();
            return Ok(model.GetCandidateJobApplications(Id)); // return Job Detail
        }

        /// <summary>
        /// Parse candidate resume
        /// </summary>
        /// <param name="resume">XSeedFileEntity model</param>
        /// <param name="Id">Candidate Id</param>
        /// <returns>Candidate User model</returns>
        [HttpPost]
        public IHttpActionResult ParseResume(XSeedFileEntity resume, string Id = null)
        {
            CandidateUserInfo model = new CandidateUserInfo();
            return Ok(model.ParseResume(resume, Id));
        }

        /// <summary>
        /// Get Candidates application by Job Id
        /// </summary>
        /// <param name="jobId">Job Id</param>
        /// <returns>List<CandidateUserModel></returns>
        [HttpGet]
        public IHttpActionResult GetCandidateByJob(Guid? jobId = null)
        {
            CandidateUserInfo model = new CandidateUserInfo();
            return Ok(model.GetCandidateByJob(jobId)); // return Job Detail
        }

        /// <summary>
        /// Update Application Status
        /// </summary>        
        /// <param name="applicationModel">Application Model</param>        
        /// <param name="Id">Candidate UserId</param>     
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpPut]
        public IHttpActionResult UpdateApplicationStatus(Application applicationModel, string Id = null)
        {
            CandidateUserInfo model = new CandidateUserInfo();
            model.UpdateApplicationStatus(applicationModel, Id);
            return Ok();
        }

        /// <summary>
        /// Save Quick Note related to candidate
        /// </summary>
        /// <param name="note">Note</param>
        /// <param name="candidateId">Candidate Id</param>
        /// <returns>Notes</returns>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPost]
        public IHttpActionResult SaveQuickNote(string note, string candidateId)
        {
            CandidateUserInfo model = new CandidateUserInfo();
            return Ok(model.SaveQuickNote(note, candidateId));
        }

        /// <summary>
        /// Get Quick Notes related to candidate
        /// </summary>
        /// <param name="candidateId">Candidate Id</param>
        /// <returns>Quick Notes</returns>
        [HttpGet]
        public IHttpActionResult GetQuickNotes(string candidateId)
        {
            CandidateUserInfo model = new CandidateUserInfo();
            return Ok(model.GetQuickNotes(candidateId));
        }

        /// <summary>
        /// Get Activity Log related to Candidate
        /// </summary>
        /// <param name="candidateId">Candidate Id</param>
        /// <returns>Activity Logs</returns>
        [HttpGet]
        public IHttpActionResult GetCandidateActivityLog(string candidateId)
        {
            CandidateUserInfo model = new CandidateUserInfo();
            return Ok(model.GetCandidateActivityLog(candidateId));
        }

        /// <summary>
        /// Get Candidate Id
        /// </summary>        
        /// <returns>Candidate Id</returns>
        [HttpGet]
        public IHttpActionResult GetCandidateId()
        {
            CandidateUserInfo model = new CandidateUserInfo();
            return Ok(model.GetCandidateId());
        }

        /// <summary>
        /// Check for Candidate Requisition Id
        /// </summary>        
        /// <param name="candidateId">Candidate Id</param>
        /// <returns>True/ False</returns>
        [HttpGet]
        public IHttpActionResult IsDuplicateCandidateId(string candidateId)
        {
            CandidateUserInfo model = new CandidateUserInfo();
            return Ok(model.IsDuplicateCandidateId(candidateId));
        }

        /// <summary>
        /// Get candidate resume file path
        /// </summary>        
        /// <returns>Candidate Id</returns>
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult ViewCandidateResume(string candidateId)
        {
            CandidateUserInfo model = new CandidateUserInfo();
            return Ok(model.ViewCandidateResume(candidateId));
        }

        /// <summary>
        /// View Monster candidate detail
        /// </summary>        
        /// <returns>Monster candidate detail</returns>
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult ViewMonsterCandidateDetail(string resumeId, string candidateId = null)
        {
            CandidateUserInfo model = new CandidateUserInfo();
            return Ok(model.ViewMonsterCandidateDetail(resumeId, candidateId));
        }

        /// <summary>
        /// Search Job openings to candidate
        /// </summary>
        /// <param name="searchModel">Candidate Job Search Model</param>
        /// <param name="recordCount">Record Count</param>
        /// <returns>Job List</returns>
        [BasicAuthAttribute]
        [HttpPost]
        public IHttpActionResult SearchJobOpenings(CandidateJobSearchModel searchModel, int recordCount = 50)
        {
            CandidateUserInfo model = new CandidateUserInfo();
            return Ok(model.SearchJobOpenings(searchModel, recordCount)); // return all Job opening list
        }


        /// <summary>
        /// View Dice Candidate Detail
        /// </summary>
        /// <param name="diceId">dice Id</param>
        /// <param name="candidateId">candidate Id</param>
        /// <returns>Dice Candidate Detail</returns>
        [HttpGet]
        public IHttpActionResult ViewDiceCandidateDetail(string diceId, string candidateId = null)
        {
            CandidateUserInfo model = new CandidateUserInfo();
            return Ok(model.ViewDiceCandidateDetail(diceId, candidateId));
        }

        #endregion

        #region Candidate Log [Google Tag]

        /// <summary>
        /// Get list/ single candidate log(s)
        /// </summary>
        /// <param name="Id"> Candidate Log Id</param>
        /// <returns>Candidate Log(s)</returns>
        [HttpGet]
        public IHttpActionResult GetCandidateLog(string Id = null)
        {
            CandidateUserInfo model = new CandidateUserInfo();

            if (string.IsNullOrEmpty(Id))
            {
                return Ok(model.ListCandidateLogs()); // return candidate list
            }
            else
            {
                return Ok(model.GetCandidateLogDetail(Id)); // return single candidate
            }
        }

        /// <summary>
        /// Update Candidate Log Detail
        /// </summary>
        /// <param name="candidateLogModel">Candidate Log Model</param>        
        [HttpPut]
        public IHttpActionResult PutCandidateLog(CandidateLog candidateLogModel)
        {
            CandidateUserInfo model = new CandidateUserInfo();
            model.PutCandidateLogDetail(candidateLogModel);
            return Ok();
        }

        /// <summary>
        /// Get UnProcessed Passive Candidates
        /// </summary>
        /// <returns>Passive Candidates</returns>
        [HttpGet]
        public IHttpActionResult GetUnprocessedPassiveCandidates()
        {
            CandidateUserInfo model = new CandidateUserInfo();

            return Ok(model.GetUnprocessedPassiveCandidates());

        }

        /// <summary>
        /// Update Passive Searched Candidate Status
        /// </summary>
        /// <param name="Id">Id</param>        
        [HttpPut]
        public IHttpActionResult UpdatePassiveSearchedCandidateStatus(PassiveSearchCandidateModel passiveSearchCandidate, bool status)
        {
            CandidateUserInfo model = new CandidateUserInfo();
            model.UpdatePassiveSearchedCandidateStatus(passiveSearchCandidate, status);
            return Ok();
        }

        /// <summary>
        /// Post Candidate Log Detail
        /// </summary>
        /// <param name="candidateLogModel">Candidate Log Model</param>
        [HttpPost]
        public IHttpActionResult PostCandidateLog(CandidateLog candidateLogModel)
        {
            CandidateUserInfo model = new CandidateUserInfo();
            model.PostCandidateLogDetail(candidateLogModel);
            return Ok();
        }


        #endregion
    }
}