﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using XSeed.API.Models;
using XSeed.Business.ResumeEmail;
using XSeed.Data.ViewModel.ResumeEmail;

namespace XSeed.API.Controllers
{
    public class ResumeEmailController : BaseController
    {
        /// <summary>
        /// Get List of Resume Emails
        /// Get Resume Email Detail
        /// </summary>
        /// <param name="organizationId">organizationId</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="pageNumber">pageNumber</param>
        /// <param name="sortBy">sortBy</param>
        /// <param name="sortOrder">sortOrder</param>
        /// <param name="Id">Id</param>
        /// <returns>List of Resume Emails</returns>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Vendor_Read", ClaimValue = "True")]
        public IHttpActionResult Get(Guid organizationId, int pageSize, int pageNumber, string sortBy = "Date", string sortOrder = "desc", Guid? Id = null)
        {
            ResumeInfo model = new ResumeInfo();

            if (Id == null)
            {
                /* Set Pagination Info */
                var paginationInfo = model.GetPaginationInfo(organizationId, pageSize);

                /* return job list */
                var result = new
                {
                    TotalCount = paginationInfo.TotalCount,
                    totalPages = paginationInfo.TotalPages,
                    ResumeEmail = model.ListAllResumeEmails(organizationId, pageSize, pageNumber, sortBy, sortOrder)
                };
                return Ok(result);
            }
            else
            {
                return Ok(model.GetResumeEmailDetail(organizationId, Id)); // return single resume email details
            }
        }

        /// <summary>
        /// Get Unread Mail Count 
        /// </summary>
        /// <param name="organizationId">OrganizationId</param>
        /// <returns>Unread Mail Count</returns>
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Vendor_Read", ClaimValue = "True")]
        public IHttpActionResult GetUnreadMailCount(Guid organizationId)
        {
            ResumeInfo model = new ResumeInfo();
            return Ok(model.GetUnreadMailCount(organizationId));
        }

        /// <summary>
        /// Update Resume Parsed Email
        /// </summary>
        /// <param name="resumeEmailParserModel">Resume Email Parser Model</param>
        /// <returns></returns>
        [HttpPut]
        [ClaimsAuthorization(ClaimType = "Vendor_Update", ClaimValue = "True")]
        public IHttpActionResult Put(Guid Id)
        {
            if (!ModelState.IsValid)
            {
                throw new ArgumentException();
            }

            ResumeInfo model = new ResumeInfo();
            model.UpdateResumeParsedEmail(Id);
            return Ok();
        }
    }
}
