﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XSeed.API.Models;
using XSeed.Business.Organization;
using XSeed.Data.ViewModel.Organization;

namespace XSeed.API.Controllers
{
    /// <summary>
    /// Organization API
    /// </summary>
    public class OrganizationController : BaseController
    {
        /// <summary>
        /// Get detail of the organization
        /// </summary>        
        /// <param name="organizationId">Organization Id</param>
        /// <remarks>Get the details of the organization
        ///</remarks>
        /// <returns>Organization detail</returns>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response> 
        [HttpGet]
        [ClaimsAuthorization(ClaimType = "Organization_Read", ClaimValue = "True")]
        public IHttpActionResult Get(Guid organizationId)
        {
            OrganizationInfo model = new OrganizationInfo();
            return Ok(model.GetOrganizationDetail(organizationId));
        }

        /// <summary>
        /// Update the details for the organization
        /// </summary>
        /// <param name="organizationDetailModel">Organization detail model</param>
        /// <remarks>Update the details about the organization</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPut]
        [ClaimsAuthorization(ClaimType = "Organization_Update", ClaimValue = "True")]
        public IHttpActionResult Put(OrganizationDetailModel organizationDetailModel)
        {
            OrganizationInfo model = new OrganizationInfo();
            model.UpdateOrganizationDetail(organizationDetailModel);
            return Ok();
        }

    }
}