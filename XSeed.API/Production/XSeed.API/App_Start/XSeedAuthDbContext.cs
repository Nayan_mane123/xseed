﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XSeed.API.App_Start
{
    public class XSeedAuthDbContext : IdentityDbContext
    {
        public XSeedAuthDbContext()
            : base("XSeedDbContext")
        {
        }
    }
}