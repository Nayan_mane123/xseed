﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XSeed.Utility
{
    public static class Constants
    {
        public const int UserTypeRecruiter = 1;
        public const bool IsActiveTrue = true;
        public const bool IsActiveFalse = false;
        public const int CreatedBySystemUser = 0;
        public const int ModifiedBySystemUser = 0;
        public const string DefaultPassword = "123456";

        //Report Names
        public const string reportCandidateAdded = "CandidateAdded";
        public const string reportRequirementTracker = "RequirementTracker";
        public const string reportSubmissionTracker = "SubmissionTracker";

        //constants for lookup duplicate value check
        public const string RequirementTitle = "Requirement Title";
        public const string RequirementType = "Requirement Type";
        public const string Degree = "Degree";
        public const string BusinessUnit = "Business Unit";
        public const string CompanySource = "Company Source";
        public const string CandidateSource = "Candidate Source";
        public const string CandidateCategories = "Candidate Categories";
        public const string CandidateGroups = "Candidate Groups";

        //Mail Related Constants

        //User Password Mail
        public const string PasswordMailSubject = "Welcome!!! Here are your login credentials for XSeed.com";
        public const string PasswordMailEmailTemplatePath = "~/Areas/NotificationTemplates/EmailTemplates/passwordmail.html";
        public const string typeOfNotificationUserPasswordMail = "PasswordMail";

        //Verify Email
        public const string VerifyEmailSubject = "Verify your email for XSeed.com";
        public const string VerifyEmailEmailTemplatePath = "~/Areas/NotificationTemplates/EmailTemplates/verifyEmail.html";
        public const string typeOfNotificationVerifyEmail = "VerifyEmail";

        //Reset Password
        public const string ResetPasswordSubject = "Reset your password for XSeed.com";
        public const string ResetPasswordEmailTemplatePath = "~/Areas/NotificationTemplates/EmailTemplates/resetpassword.html";
        public const string typeOfNotificationResetPassword = "ResetPassword";

        //Schedule
        public const string ScheduleSubject = "Scheduled event from XSeed.com";
        public const string ScheduleEmailTemplatePath = "~/Areas/NotificationTemplates/EmailTemplates/scheduleNotification.html";
        public const string typeOfNotificationSchedule = "Schedule";

        //Resume Blast
        public const string resumeAttachmentSubject = "Candidate available for hire";
        public const string ResumeAttachmentTemplatePath = "~/Areas/NotificationTemplates/EmailTemplates/resumeAttachment.html";
        public const string typeOfNotificationResumeAttachment = "ResumeAttachment";

        //Email Submission
        public const string EmailSubmissionAttachmentSubject = "Submission: Candidates resume.";
        public const string EmailSubmissionAttachmentTemplatePath = "~/Areas/NotificationTemplates/EmailTemplates/emailSubmission.html";
        public const string typeOfNotificationEmailSubmissionAttachment = "EmailSubmission";

        //regex
        public const string EmailExpression = @"^[a-zA-Z0-9][a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+(\.[a-zA-Z]{2,30})+$";
        public const string ZipExpression = @"^(\d{5}-\d{4}|\d{4}|\d{5}|\d{6}|\d{9})$";
        public const string MobileExpression = @"^(\+1[\-\s]?)?[0]?\(([0-9]{3})\)[0-9]{3}-[0-9]{4}$|^(\+1[\-\s]?)?[0]?[0-9]{3}-[0-9]{3}-[0-9]{4}$|^(\+91[\-\s]?)?[0]?(91)?[1-9]\d{9}$";
        public const string PhoneNoExpression = @"^(\+1[\-\s]?)?[0]?\(([0-9]{3})\)[0-9]{3}-[0-9]{4}$|^(\+1[\-\s]?)?[0]?[0-9]{3}-[0-9]{3}-[0-9]{4}$|^(\+91[\-\s]?)?([0]?91)?\((\d{2,4})\)[\-\s]?[0-9]{5,8}$|^(\+91[\-\s]?)?[0]?\+?\d{2,4}[\-\s]?(\-?)?\d{5,8}$";
    }

    public enum EmailPriority
    {
        Urgent = 1,
        High = 2,
        Medium = 3,
        Low = 4
    }
}
