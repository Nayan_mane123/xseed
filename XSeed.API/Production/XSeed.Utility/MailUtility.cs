﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace XSeed.Utility
{
    /// <summary>
    /// Mail Utility
    /// </summary>
    public static class MailUtility
    {
        /// <summary>
        /// Send Password Mail to User
        /// </summary>
        /// <param name="toEmailID">To Email ID</param>
        /// <param name="password">Password</param>
        /// <param name="subject">Subject</param>
        /// <param name="mailBody">Mail Body</param>
        /// <returns></returns>
        public static bool SendMail(string toEmailID, string subject, string mailBody)
        {
            string fromEmailID = Convert.ToString(ConfigurationManager.AppSettings["EmailFromUserName"]);
            string fromEmailPassword = Convert.ToString(ConfigurationManager.AppSettings["EmailFromPassword"]);

            if (toEmailID != String.Empty)
            {
                MailMessage objMail = new MailMessage();
                objMail.IsBodyHtml = true;
                objMail.Body = mailBody;
                //string createdByName = CreatedByName;

                SmtpClient objSMTP = new SmtpClient();
                objMail.From = new MailAddress(fromEmailID);
                objMail.To.Add(toEmailID);
                //objMail.CC.Add(createdByEmail);
                //objMail.Bcc.Add(strEmailFromId);
                objMail.Subject = subject;

                objSMTP.Host = "smtp.gmail.com";
                objSMTP.UseDefaultCredentials = false;
                objSMTP.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
                objSMTP.EnableSsl = true;
                objSMTP.DeliveryMethod = SmtpDeliveryMethod.Network;

                objSMTP.Credentials = new NetworkCredential(fromEmailID, fromEmailPassword);
                try
                {
                    objMail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                    objSMTP.Send(objMail);
                }
                catch (Exception ex)
                {
                    string tempExMessage = ex.Message;
                    tempExMessage += ex.ToString();
                }
                return true;
            }

            return false;
        }

        /// <summary>
        /// Password Mail Body
        /// </summary>
        /// <param name="userName">UserName</param>
        /// <param name="password">Password</param>
        /// <returns></returns>
        public static string PasswordMailBody(string userName, string password)
        {
            string mailBody = "Hello, your account admin at Xseed.com have created your account you can access it using following credintials" + " <br /><br />" +
                    "<table style='border-collapse:collapse; text-align:left; text-indent: 5px;'>" +
                        "<tr style='border:1px solid #999;line-height: 20px;'>" +
                            "<th style='border-right:1px solid #777;'>Email </th>" +
                            "<td>" + userName + "</td>" +
                        "</tr>" +
                        "<tr style='border:1px solid #999;line-height: 20px;'>" +
                            "<th style='border-right:1px solid #777;'>Password </th>" +
                            "<td>" + password + "</td>" +
                        "</tr>" +
                    "</table>";

            return mailBody;
        }
    }
}
