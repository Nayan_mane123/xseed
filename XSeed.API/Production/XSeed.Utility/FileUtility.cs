﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Diagnostics;

namespace XSeed.Utility
{
    public static class FileUtility
    {
        static string ImageFilePath = ConfigurationManager.AppSettings["ImageFilePath"];
        static string ResumeFilePath = ConfigurationManager.AppSettings["ParsedResumePath"];
        static string LocalResumeFilePath = ConfigurationManager.AppSettings["LocalResumePath"];
        static string PDFResumePath = ConfigurationManager.AppSettings["PDFResumePath"];
        static string PDFConverterPath = ConfigurationManager.AppSettings["PDFConverterPath"];
        static string CompanyDataImageFilePath = ConfigurationManager.AppSettings["CompanyDataImageFilePath"];
        static string BulkResumeUploadFilePath = ConfigurationManager.AppSettings["BulkResumeUploadPath"];
        static string videoResumeInitialPath = ConfigurationManager.AppSettings["VideoResumePath"];
        static string VideoResumeFilePath = ConfigurationManager.AppSettings["VideoResumeFilePath"];

        /// <summary>
        /// Save Image File
        /// </summary>
        /// <param name="fileName">File Name</param>
        /// <param name="fileType">File Type</param>
        /// <param name="filedata">File Data</param>
        /// <param name="entityName">Entity Name for Location</param>
        public static void SaveImageFile(string fileName, string fileType, string filedata, string entityName, string originalFileName)
        {
            string path = string.Empty;
            byte[] data = null;

            /* Get image format */
            ImageFormat format = GetImageFormat(fileType);

            /* Set file location */
            string entityPath = Path.Combine(ImageFilePath, entityName);

            if (!Directory.Exists(entityPath))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(entityPath));
            }

            /* Get file data */
            if (filedata == null && !string.IsNullOrEmpty(fileName))
            {
                path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(CompanyDataImageFilePath), originalFileName);
                if (File.Exists(path))
                {
                    Image image = Image.FromFile(path);
                    if (image != null)
                    {
                        var imageData = ImageToBase64(image, format);
                        data = imageData != null ? System.Convert.FromBase64String(imageData) : null;
                    }
                }
            }
            else
            {
                /* Get file data */
                data = filedata.Contains(',') ? System.Convert.FromBase64String(filedata.Split(',')[1]) : System.Convert.FromBase64String(filedata);
            }

            if (data != null)
            {
                using (MemoryStream memory = new MemoryStream(data))
                {
                    /* Save file */
                    Image img = Image.FromStream(memory);
                    path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(entityPath), fileName);
                    img.Save(path, format);
                }
            }
        }

        /// <summary>
        /// Returns Image Format
        /// </summary>
        /// <param name="fileType">File Type</param>
        /// <returns>ImageFormat</returns>
        private static ImageFormat GetImageFormat(string fileType)
        {
            switch (fileType)
            {
                case "image/jpeg":
                case "image/jpg":
                    return ImageFormat.Jpeg;

                case "image/png":
                    return ImageFormat.Png;

                case "image/bmp":
                    return ImageFormat.Bmp;

                default:
                    throw new UnsupportedMediaTypeException("Unsupported Media Type. ", new MediaTypeHeaderValue("text/html"));
            }
        }

        /// <summary>
        /// Rename file with Date time stamp
        /// </summary>
        /// <param name="fileName">File Name</param>
        /// <returns>File with Date Time stamp</returns>
        public static string RenameFile(string fileName)
        {
            return !string.IsNullOrEmpty(fileName) ? Path.GetFileNameWithoutExtension(fileName) + "-" + DateTime.UtcNow.Ticks.ToString() + Path.GetExtension(fileName) : string.Empty;
        }

        /// <summary>
        /// Save Resume File
        /// </summary>
        /// <param name="fileName">File Name</param>
        /// <param name="fileType">File Type</param>
        /// <param name="filedata">File data</param>
        /// <param name="entityName">Entity Name</param>
        /// <returns>Resume File Name</returns>
        public static string SaveResumeFile(string fileName, string fileType, string filedata, string entityName)
        {
            string path = string.Empty;
            string entityPath = string.Empty;

            /* Set file location */
            if (entityName != "ResumeUpload")
            {
                entityPath = entityName == "ParseResume" ? Path.Combine(ResumeFilePath) : entityName == "CandidateVideo" ? Path.Combine(VideoResumeFilePath) : Path.Combine(LocalResumeFilePath);
            }
            else if (entityName == "ResumeUpload")
            {
                entityPath = Path.Combine(BulkResumeUploadFilePath);
            }

            if (!Directory.Exists(HttpContext.Current.Server.MapPath(entityPath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(entityPath));
            }

            /* Get file data */
            byte[] data = filedata.Contains(',') ? System.Convert.FromBase64String(filedata.Split(',')[1]) : System.Convert.FromBase64String(filedata);

            path = HttpContext.Current.Server.MapPath(entityPath) + "\\" + fileName;

            File.WriteAllBytes(path, data);

            if (fileType != null)
            {
                if (fileType.Contains("mp4") || fileType.Contains("webm"))
                {
                    path = videoResumeInitialPath + fileName;
                }
            }
            
            return path;
        }

        /// <summary>
        /// Convert and Save Microsoft Word Document (.doc) to PDF
        /// </summary>
        /// <param name="fileName">Word File Name</param>
        /// <returns>Pdf File Path</returns>
        public static string SaveDocumentAsPDF(string fileName)
        {
            string pdfName = string.Empty;

            /* Set file location */
            string entityPath = Path.Combine(ResumeFilePath);

            /* convert .doc to .pdf */
            pdfName = ConvertToPDF(fileName, isParseDoc: true);

            return (HttpContext.Current.Server.MapPath(entityPath) + "\\" + pdfName);
        }

        /// <summary>
        /// Delete Resume File.
        /// </summary>
        /// <param name="fileName">fileName</param>
        public static void DeleteResumeFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
        }

        /// <summary>
        /// Save Image File From URL
        /// </summary>
        /// <param name="fileName">fileName</param>
        /// <param name="logoURL">URL</param>
        /// <param name="entityName">entityName</param>
        public static void SaveImageFileFromURL(string fileName, string logoURL, string entityName)
        {
            string url = logoURL;
            string entityPath = Path.Combine(ImageFilePath, entityName);
            string file_name = HttpContext.Current.Server.MapPath(entityPath) + "\\" + fileName + ".jpg";
            save_file_from_url(file_name, url);
        }

        /// <summary>
        /// Save Image file from URL
        /// </summary>
        /// <param name="file_name">file_name</param>
        /// <param name="url">url</param>
        public static void save_file_from_url(string file_name, string url)
        {
            byte[] content;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                WebResponse response = request.GetResponse();

                Stream stream = response.GetResponseStream();

                using (BinaryReader br = new BinaryReader(stream))
                {
                    content = br.ReadBytes(500000);
                    br.Close();
                }
                response.Close();

                FileStream fs = new FileStream(file_name, FileMode.Create);
                BinaryWriter bw = new BinaryWriter(fs);

                try
                {
                    bw.Write(content);
                }
                finally
                {
                    fs.Close();
                    bw.Close();
                }
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Function to Convert Image to Base64 string
        /// </summary>
        /// <param name="image">Image</param>
        /// <param name="format">ImageFormat</param>
        /// <returns>base64String</returns>
        public static string ImageToBase64(Image image, System.Drawing.Imaging.ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }

        /// <summary>
        /// Function to Convert Base64 to Image string
        /// </summary>
        /// <param name="base64String">base64String</param>
        /// <returns>Image</returns>
        public static Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }

        /// <summary>
        /// Convert resume file to PDF format
        /// </summary>
        /// <param name="path">file path</param>
        /// <param name="isParseDoc">IsParse Flag</param>
        /// <returns>PDF file path</returns>
        public static string ConvertToPDF(string path, bool isParseDoc = false)
        {
            if (!string.IsNullOrEmpty(path) && File.Exists(path))
            {
                string pdfFilePath = string.Empty;

                /* Set Path */
                string pdfPath = isParseDoc ? HttpContext.Current.Server.MapPath(ResumeFilePath) : PDFResumePath;

                /* Initiate Process */
                var pdfProcess = new Process();
                pdfProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                pdfProcess.StartInfo.FileName = "soffice.exe";
                pdfProcess.StartInfo.Arguments = "-norestore -nofirststartwizard -headless -convert-to pdf --outdir \"" + pdfPath + "\"  \"" + path + "\"";
                pdfProcess.StartInfo.WorkingDirectory = PDFConverterPath; //This is really important

                /* Run as Administrator */
                if (System.Environment.OSVersion.Version.Major >= 6)
                {
                    pdfProcess.StartInfo.Verb = "runas";
                }

                /* Execute */
                pdfProcess.Start();
                pdfProcess.WaitForExit();

                /* Get Pdf file name */
                pdfFilePath = Path.GetFileNameWithoutExtension(path) + ".pdf";

                return pdfFilePath;
            }
            throw new Exception("Resume not found!!!");
        }
    }
}
