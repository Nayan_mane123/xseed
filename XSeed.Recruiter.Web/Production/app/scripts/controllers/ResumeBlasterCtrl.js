﻿'use strict';

XSEED_APP.controller('ResumeBlasterCtrl', ResumeBlasterCtrl);

/**
 * @ngInject
 */
function ResumeBlasterCtrl($scope, $rootScope, $route, $routeParams, RequestContext, $timeout, $q, $location, $window, configuration, i18nFactory, ExceptionHandler, EventBus, $log, _, ipCookie, blockUI, ModalFactory, XSeedAlert, localStorageService, $uibModal, XSeedApiFactory, ngTableParams, $filter, ValidationService) {


    $scope.ResumeBlasterSubmissionModel = new API.ResumeBlasterSubmissionRequest();
    $scope.ResumeBlasterModel = new API.ResumeBlasterRequest();
    $scope.checkedCount = 0;

    $scope.resumeBlasterFilter = {};
    $scope.candidateSearch = {};
    $scope.ResumeBlasterAPIModel = {};
    $scope.resumePresent = false;

    init();

    var resumeBlasterSubmissionModal = ModalFactory.resumeBlasterSubmissionModal($scope);


    $scope.openResumeBlasterModal = function (type, recruiter) {
        $scope.resumeBlasterList = [];
        $scope.resumeFileName = "";
        $scope.maxSize = false;

        $scope.recruiter = recruiter;

        if (type == 'Single') {
            $scope.resumeBlasterList.push(recruiter);
        }
        else {
            angular.forEach($scope.checkboxes.items, function (item, Identifier) {
                if (item == true && $scope.resumeBlasterList.length < 20) {
                    var selectedRecruiter = _.find($scope.orderedResumeBlasterList, function (o) { return o.Id == Identifier; });
                    if (selectedRecruiter.PrimaryEmail)
                        $scope.resumeBlasterList.push(selectedRecruiter);
                }
            });
            //console.log($scope.resumeBlasterList);
            //console.log($scope.resumeBlasterList);
        }
        if ($scope.resumeBlasterList && $scope.resumeBlasterList.length > 0)
            resumeBlasterSubmissionModal.$promise.then(resumeBlasterSubmissionModal.show);
        else
            XSeedAlert.swal({
                title: 'Warning!',
                text: 'Select recruiter(s) with valid email id!',
                type: "warning",
                customClass: "xseed-error-alert",
                allowOutsideClick: false
            }).then(function () {
                $timeout(function () {
                    $location.path('resumeBlasterList');
                }, 0);
            }, function (dismiss) {
                blockUI.stop();
            });
    }

    $scope.closeResumeBlasterModal = function () {
        resumeBlasterSubmissionModal.$promise.then(resumeBlasterSubmissionModal.hide);
        $scope.ResumeBlasterAPIModel.Resume = undefined;
        $scope.resumePresent = false;
        $scope.common.uploadResumeFileName = null;
        $scope.common.invalidResumeFile = false;
        $scope.common.maxResumeFileSize = false;
    }


    //Initialization Function
    function init() {
        if ($scope.isUndefinedOrNull($scope.ResumeBlasterModel.Id) || $scope.isEmpty($scope.ResumeBlasterModel.Id)) {
            $location.path('resumeBlasterList');
        }

        $scope.ResumeBlasterModel.manipulationMode = "Create";
        //$scope.maxDate = new Date().toString();
        //$scope.minDate = new Date().toString();

        var date = new Date();
        $scope.maxDate = ("0" + (date.getMonth() + 1).toString()).substr(-2) + "/" + ("0" + date.getDate().toString()).substr(-2) + "/" + (date.getFullYear().toString());
        $scope.minDate = ("0" + (date.getMonth() + 1).toString()).substr(-2) + "/" + ("0" + date.getDate().toString()).substr(-2) + "/" + (date.getFullYear().toString());
    };


    $scope.createResumeBlasterProcess = function (createRecruiterForm) {
        if (new ValidationService().checkFormValidity(createRecruiterForm)) {
            blockUI.start();

            var promise = XSeedApiFactory.createRecruiter($scope.ResumeBlasterModel);
            promise.then(
              function (response) {
                  blockUI.stop();
                  XSeedAlert.swal({
                      title: 'Success!',
                      text: 'Recruiter created successfully!',
                      type: "success",
                      customClass: "xseed-error-alert",
                      allowOutsideClick: false
                  }).then(function () {
                      $scope.candidateModel = {};
                      $timeout(function () {
                          $location.path('resumeBlasterList');
                      }, 0);
                  }, function (dismiss) {
                      blockUI.stop();
                  });
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('ResumeBlaster', httpError.data.Status, httpError.data.Message);
              });
        };
    };



    $scope.createResumeBlasterView = function () {
        $scope.ResumeBlasterModel = {};
        $scope.ResumeBlasterModel.manipulationMode = 'Create';
        $location.path('createResumeBlaster');
    }


    //function get ResumeBlaster List() {
    $scope.getResumeBlasterList = function () {
        if ($scope.advanceSearchFlag) {
            return;
        }
        blockUI.start();

        //var organizationId = $rootScope.userDetails.OrganizationId;
        var promise = XSeedApiFactory.getResumeBlasterList();
        promise.then(
          function (response) {
              $scope.resumeBlasterTotalList = response[0].data;
              populateResumeBlasterList();
              blockUI.stop();
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('ResumeBlaster', httpError.data.Status, httpError.data.Message);
          });
    };

    //$scope.uploadResume = function (files) {
    //    $scope.resumePresent = false;
    //    if (files.length != 0) {
    //        var ext = files[0].name.match(/(?:\.([^.]+))?$/)[1];
    //        if (files[0].size >= 25000000) {
    //            $scope.maxSize = true;
    //        }
    //        if (angular.lowercase(ext) === 'jpg' || angular.lowercase(ext) === 'jpeg' || angular.lowercase(ext) === 'pdf' || angular.lowercase(ext) === 'doc' || angular.lowercase(ext) === 'docx' || angular.lowercase(ext) === 'txt' || angular.lowercase(ext) === 'png') {

    //            $scope.resumeFileName = files[0].name;
    //            $scope.resumePresent = false;
    //        }
    //        else {
    //            $scope.ResumeBlasterAPIModel.Resume = undefined;
    //            $scope.resumeFileName = "";
    //        }
    //    }
    //    else {
    //        $scope.maxSize = false;
    //        $scope.enableButton();

    //    }
    //}

    //$scope.enableButton = function () {
    //    $scope.resumePresent = false;
    //    $scope.maxSize = false;
    //    $scope.resumeFileName = "";
    //    //console.log($scope.resumeFileName);
    //    if ($scope.ResumeBlasterAPIModel && $scope.ResumeBlasterAPIModel.Resume) {
    //        $scope.ResumeBlasterAPIModel.Resume = "";
    //        $scope.resumePresent = true;
    //    }

    //}

    function populateResumeBlasterList() {
        if (angular.isDefined($scope.tblResumeBlasterList)) {
            $scope.tblResumeBlasterList.reload();
        }
        else {
            $scope.tblResumeBlasterList = new ngTableParams({
                page: 1,
                count: 10
                , sorting: {}
            }, {
                counts: [],
                total: $scope.resumeBlasterTotalList.length,
                getData: function ($defer, params) {
                    var filteredData = $filter('filter')($scope.resumeBlasterTotalList, $scope.resumeBlasterFilter);
                    $scope.orderedResumeBlasterList = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
                    params.total($scope.orderedResumeBlasterList.length);
                    //$defer.resolve($scope.slicedCandidateList = $scope.orderedCandidateList.slice((params.page() - 1) * params.count(), params.page() * params.count()));

                    $scope.slicedResumeBlasterList = $scope.orderedResumeBlasterList.slice((params.page() - 1) * params.count(), params.page() * params.count());
                    $defer.resolve($scope.slicedResumeBlasterList);
                }
                , $scope: $scope
            });
        }
    };





    $scope.$watch("resumeBlasterFilter.$", function () {
        if (angular.isDefined($scope.tblResumeBlasterList)) {
            $scope.tblResumeBlasterList.reload();

            if (!$scope.orderedResumeBlasterList) {
                return;
            }
            var checked = 0, unchecked = 0,
            total = $scope.orderedResumeBlasterList.length;
            angular.forEach($scope.orderedResumeBlasterList, function (item) {
                checked += ($scope.checkboxes.items[item.Id]) || 0;
                unchecked += (!$scope.checkboxes.items[item.Id]) || 0;
            });
            if ((unchecked == 0) || (checked == 0)) {
                $scope.checkboxes.checked = (checked == total);

            }

            // grayed checkbox
            angular.element(document.getElementById("select_all")).prop("indeterminate", (checked != 0 && unchecked != 0));


            $scope.tblResumeBlasterList.page(1);
        }
    });


    //Checkbox code-
    $scope.checkboxes = { 'checked': false, items: {} };

    // watch for check all checkbox
    $scope.$watch('checkboxes.checked', function (value) {
        angular.forEach($scope.orderedResumeBlasterList, function (item) {
            if (angular.isDefined(item.Id)) {
                $scope.checkboxes.items[item.Id] = value;
            }
        });
    });

    // watch for data checkboxes
    $scope.$watch('checkboxes.items', function (values) {
        if (!$scope.orderedResumeBlasterList) {
            return;
        }
        var checked = 0, unchecked = 0,
            total = $scope.orderedResumeBlasterList.length;
        angular.forEach($scope.orderedResumeBlasterList, function (item) {
            checked += ($scope.checkboxes.items[item.Id]) || 0;
            unchecked += (!$scope.checkboxes.items[item.Id]) || 0;
        });
        if ((unchecked == 0) || (checked == 0)) {
            $scope.checkboxes.checked = (checked == total);
        }

        $scope.checkedCount = checked;
        //alert(checked);
        //alert(unchecked);
        // grayed checkbox
        angular.element(document.getElementById("select_all")).prop("indeterminate", (checked != 0 && unchecked != 0));
    }
    , true
    );




    $scope.resumeBlasterProcess = function (resumeBlasterPopupForm) {
        if (new ValidationService().checkFormValidity(resumeBlasterPopupForm) && !$scope.common.maxResumeFileSize && !$scope.common.invalidResumeFile && $scope.ResumeBlasterAPIModel.Resume) {

            blockUI.start();
            var list = [];
            for (var i = 0; i < $scope.resumeBlasterList.length; i++) {
                list.push($scope.resumeBlasterList[i].PrimaryEmail);
            }

            $scope.ResumeBlasterAPIModel.resumeBlasterList = list;


            //console.log($scope.ResumeBlasterAPIModel.Resume);
            var promise = XSeedApiFactory.BlastMail($scope.ResumeBlasterAPIModel);
            $scope.resumePresent = false;
            promise.then(
              function (response) {
                  blockUI.stop();
                  $scope.closeResumeBlasterModal();
                  XSeedAlert.swal({
                      title: 'Success!',
                      text: 'Resume Blasted!',
                      type: "success",
                      customClass: "xseed-error-alert"
                  }).then(function () {
                      $timeout(function () {
                          angular.forEach($scope.checkboxes.items, function (item, Identifier) {
                              $scope.checkboxes.items[Identifier] = false;
                          });
                          $scope.common.uploadResumeFileName = null;
                      }, 0);
                  }, function (dismiss) {
                      blockUI.stop();
                  });
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('ResumeBlaster', httpError.data.Status, httpError.data.Message);
              });
        }


    };



    $scope.enableButtonResume = function (resumeFile) {
        $scope.common.uploadResumeFileName = null;
        if (resumeFile) {
            $scope.ResumeBlasterAPIModel.Resume = null;
        }
        $scope.common.maxResumeFileSize = false;
        $scope.common.invalidResumeFile = false;
    }

    $scope.submitPopupCancel = function () {
        //$scope.resumeBlasterList = [];
        $scope.ResumeBlasterAPIModel.Resume = "";
        $scope.closeResumeBlasterModal();
    }


    //ResumeBlasterDetail

    $scope.ResumeBlasterDetailView = function (recruiterId) {
        blockUI.start();
        var promise = XSeedApiFactory.getResumeBlasterDetail(recruiterId);
        promise.then(
          function (response) {
              //$timeout(function () {
              //return $scope.$apply(function () {
              $scope.ResumeBlasterModel = response[0].data;
              $scope.recruiterId = recruiterId;

              blockUI.stop();
              $location.path('ResumeBlasterDetail');
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('ResumeBlaster', httpError.data.Status, httpError.data.Message);
          });
    };



    $scope.ResumeBlasterDirectEditView = function (recruiterId) {
        blockUI.start();
        var promise = XSeedApiFactory.getResumeBlasterDetail(recruiterId);
        promise.then(
          function (response) {
              $scope.ResumeBlasterModel = response[0].data;
              $scope.ResumeBlasterModel.manipulationMode = "Edit";

              $scope.recruiterId = recruiterId;
              blockUI.stop();

              $location.path('editResumeBlaster');
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('ResumeBlaster', httpError.data.Status, httpError.data.Message);
          });
    };



    $scope.ResumeBlasterEditView = function () {
        blockUI.start();
        $scope.ResumeBlasterModel.manipulationMode = "Edit";
        $location.path('editResumeBlaster');
        blockUI.stop();
    }

    //ResumeBlaster edit

    $scope.editResumeBlasterProcess = function (createRecruiterForm) {
        if (new ValidationService().checkFormValidity(createRecruiterForm)) {
            blockUI.start();

            var promise = XSeedApiFactory.editResumeBlaster($scope.ResumeBlasterModel);
            promise.then(
              function (response) {
                  blockUI.stop();
                  XSeedAlert.swal({
                      title: 'Success!',
                      text: 'Recruiter details updated successfully!',
                      type: "success",
                      customClass: "xseed-error-alert",
                      allowOutsideClick: false
                  }).then(function () {
                      $timeout(function () {
                          $location.path('resumeBlasterList');
                      }, 0);
                  }, function (dismiss) {
                      blockUI.stop();
                  });
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('ResumeBlaster', httpError.data.Status, httpError.data.Message);
              });
        };
    };

    $scope.ResumeBlasterCancel = function () {
        $location.path('resumeBlasterList');
    };


    //***************************************************************************************//

    // Get the render context local to this controller (and relevant params).
    var renderContext = RequestContext.getRenderContext("standard.resumeBlaster");
    // The subview indicates which view is going to be rendered on the page.
    $scope.subview = renderContext.getNextSection();
    $scope.$on(
        "requestContextChanged",
        function () {
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {
                return;
            }
            // Update the view that is being rendered.
            $scope.subview = renderContext.getNextSection();
        }
    );
};
