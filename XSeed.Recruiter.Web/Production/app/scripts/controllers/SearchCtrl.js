﻿'use strict';

XSEED_APP.controller('SearchCtrl', SearchCtrl);

/**
 * @ngInject
 */
function SearchCtrl($scope, $rootScope, $route, $routeParams, RequestContext, $timeout, $q, $location, $window, configuration, i18nFactory, ExceptionHandler, EventBus, $log, _, ipCookie, blockUI, ModalFactory, XSeedAlert, localStorageService, $uibModal, XSeedApiFactory, ngTableParams, $filter, ValidationService, $sce) {
    var searchCandidateSubmissionModal = ModalFactory.searchCandidateSubmissionModal($scope);
    var candidateSubmissionEmailModal = ModalFactory.candidateSubmissionEmailModal($scope);

    // Requirement Type List
    $scope.requirementType = [
    {
        'Number': 1,
        'Name': "Full-time"
    },
    {
        'Number': 2,
        'Name': "Part-time"
    },
    {
        'Number': 3,
        'Name': "Contract"
    },
     {
         'Number': 4,
         'Name': "Commission"
     },
     {
         'Number': 5,
         'Name': "Temporary"
     },
     {
         'Number': 6,
         'Name': "Internship"
     },
     {
         'Number': 7,
         'Name': "Seasonal"
     },
     {
         'Number': 8,
         'Name': "Third Party - Contract Corp-to-Corp"
     },
     {
         'Number': 9,
         'Name': "Third Party - Contract-to-Hire Corp-to-Corp"
     }
    ];


    init();

    //Initialization Function
    function init() {
        $scope.getLookUpSkill();

        $scope.common.candidateSearch = {};
        $scope.common.candidateSearch.searchType = "Precision";
        $scope.checkedCount = 0;
        $scope.showSubmitMultiple = false;
        $scope.common.tblCandidateListBySource = undefined;
        $scope.common.candidateListBySource = undefined;
        $scope.common.tblCandidateListByBooleanSearch = undefined;
        $scope.common.candidateListByBooleanSearch = undefined;
        $scope.common.candidateNullLength = false;
        $scope.common.MonsterZipPresent = true;
    };

    //Checkbox code-
    $scope.checkboxes = { 'checked': false, items: {} };

    // watch for check all checkbox
    $scope.$watch('checkboxes.checked', function (value) {
        angular.forEach($scope.common.candidateListBySource, function (item) {
            if (angular.isDefined(item.Candidate_Id)) {
                $scope.checkboxes.items[item.Candidate_Id] = value;
            }
        });
    });

    // watch for data checkboxes
    $scope.$watch('checkboxes.items', function (values) {
        if (!$scope.common.candidateListBySource) {
            return;
        }
        var checked = 0, unchecked = 0,
            total = $scope.common.candidateListBySource.length;
        angular.forEach($scope.common.candidateListBySource, function (item) {
            if (item.Source == 'Local') {
                checked += ($scope.checkboxes.items[item.Candidate_Id]) || 0;
                unchecked += (!$scope.checkboxes.items[item.Candidate_Id]) || 0;
            }

            if (item.Source == 'Monster' || item.Source == 'Dice') {
                checked += ($scope.checkboxes.items[item.ProviderCandidateId]) || 0;
                unchecked += (!$scope.checkboxes.items[item.ProviderCandidateId]) || 0;
            }
        });
        if ((unchecked == 0) || (checked == 0)) {
            $scope.checkboxes.checked = (checked == total);
        }

        $scope.checkedCount = checked;
        //alert(checked);
        //alert(unchecked);
        //grayed checkbox
        angular.element(document.getElementById("select_all")).prop("indeterminate", (checked != 0 && unchecked != 0));
    }
    , true
    );

    $scope.showHideSubmitMultipleCandidate = function () {
        if (!$scope.showSubmitMultiple) {
            $scope.showSubmitMultiple = true;
        }
        else {
            $scope.showSubmitMultiple = false;
        }
    };

    $scope.showHideSubmitMultipleCandidateBoolean = function () {
        if (!$scope.showSubmitMultipleBoolean) {
            $scope.showSubmitMultipleBoolean = true;
            }
        else {
            $scope.showSubmitMultipleBoolean = false;
        }
    };

    /*****************************************Display Candidate***************************************/

    $scope.openSearchCandidatesBySource = function () {
        $scope.lookupCandidateSearchBySource = [];
        $scope.lookupCandidateSearchBySource.push(
            { Id: "Local", Name: "XSeed" }
            , { Id: "Monster", Name: "Monster" }
            , { Id: "Dice", Name: "Dice" }
            );
        $scope.common.candidateSearchBySourceModel = {};
        $scope.common.candidateSearchBySourceModel.Sources = ["Local", "Monster", "Dice"];
        buildLookupCandidateSearchBySource();
    };

    function buildLookupCandidateSearchBySource() {
        //Salary Type lookup
        var promise = XSeedApiFactory.getSalaryType();
        promise.then(
        function (response) {
            $scope.lookupSalaryType = response[0].data;
            blockUI.stop();
        },
        function (httpError) {
            blockUI.stop();
            ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
        });
    };

    function getMonsterCount() {
        var promise = XSeedApiFactory.getMonsterCount();
        promise.then(
        function (response) {
            $scope.monsterCandidateCountRemaining = response[0].data.AvailableCount;
            //console.log($scope.monsterCandidateCountRemaining);
            blockUI.stop();
        },
        function (httpError) {
            blockUI.stop();
            ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
        });
    };

    $scope.searchCandidatesBySource = function (candidateSearchBySourceForm) {
        if (new ValidationService().checkFormValidity(candidateSearchBySourceForm)) {
            blockUI.start();
            $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;;
            $scope.common.pageNumberShow = undefined;
            $scope.common.candidateSearchBySourceModel.CurrencyType = 'USD';
            searchSourceCandidates();
        }
    };

    function searchSourceCandidates() {
        var promise = XSeedApiFactory.searchCandidatesBySource($scope.common.candidateSearchBySourceModel, $scope.common.pageSizeShow, $scope.common.pageNumberShow);
        promise.then(
        function (response) {
            $scope.common.candidateListBySource = response[0].data.candidates;
            //$scope.common.totalCount = response[0].data.TotalCount;
            //$scope.common.totalPages = response[0].data.TotalPages;

            $scope.dummyDataCopy = {};
            $scope.dummyDataCopy = angular.copy($scope.common.candidateListBySource);

            $scope.localCandidateCount = 0;
            $scope.monsterCandidateCount = 0;
            $scope.diceCandidateCount = 0;

            if ($scope.common.candidateListBySource.length) {
                $scope.common.candidateNullLength = false;
                angular.forEach($scope.common.candidateListBySource, function (val, index) {
                    if (val.Source == 'Local') {
                        $scope.localCandidateCount = $scope.localCandidateCount + 1;
                    }
                    else if (val.Source == 'Monster') {
                        $scope.monsterCandidateCount = $scope.monsterCandidateCount + 1;
                    }
                    else if (val.Source == 'Dice') {
                        $scope.diceCandidateCount = $scope.diceCandidateCount + 1;
                    }
                });
            }
            else {
                $scope.common.candidateNullLength = true;
            }

            populateSearchCandidatesBySourceList();
            blockUI.stop();
        },
        function (httpError) {
            blockUI.stop();
            ExceptionHandler.handlerHTTPException('Search', httpError.data.Status, httpError.data.Message);
        });
    };

    function populateSearchCandidatesBySourceList() {
        if (angular.isDefined($scope.common.tblCandidateListBySource)) {
            $scope.common.tblCandidateListBySource.reload();
        }
        else {
            $scope.common.tblCandidateListBySource = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: $scope.common.ngTablePaginationCount,
                total: $scope.common.candidateListBySource.length,
                getData: function ($defer, params) {
                    var filteredData = $filter('filter')($scope.common.candidateListBySource, $scope.filter);
                    $scope.CandidateListBySourceData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
                    params.total($scope.CandidateListBySourceData.length);
                    $scope.CandidateListBySourceData = $scope.CandidateListBySourceData.slice((params.page() - 1) * params.count(), params.page() * params.count());

                    $defer.resolve($scope.CandidateListBySourceData);
                }
                , $scope: $scope
            });
        }
    }


    // Filter data - XSeed, Dice and Monster
    $scope.getFilterData = function (dataFrom) {
        $scope.XSeedData = {};
        $scope.XseedDataFilter = [];
        $scope.XSeedData = angular.copy($scope.dummyDataCopy);

        switch (dataFrom) {
            case 'Local':
                {
                    angular.forEach($scope.XSeedData, function (val, index) {
                        if (val.Source == "Local") {
                            $scope.XseedDataFilter.push(val);
                        }
                    })
                    $scope.common.candidateListBySource = $scope.XseedDataFilter;
                    $scope.common.tblCandidateListBySource = undefined;
                    populateSearchCandidatesBySourceList();
                    break;
                }

            case 'Dice':
                {
                    angular.forEach($scope.XSeedData, function (val, index) {
                        if (val.Source == "Dice") {
                            $scope.XseedDataFilter.push(val);
                        }
                    })
                    $scope.common.candidateListBySource = $scope.XseedDataFilter;
                    $scope.common.tblCandidateListBySource = undefined;
                    populateSearchCandidatesBySourceList();
                    break;
                }

            case 'Monster':
                {
                    angular.forEach($scope.XSeedData, function (val, index) {
                        if (val.Source == "Monster") {
                            $scope.XseedDataFilter.push(val);
                        }
                    })
                    $scope.common.candidateListBySource = $scope.XseedDataFilter;
                    $scope.common.tblCandidateListBySource = undefined;
                    populateSearchCandidatesBySourceList();
                    break;
                }

            case 'Total':
                {
                    $scope.common.candidateListBySource = $scope.XSeedData;
                    $scope.common.tblCandidateListBySource = undefined;
                    populateSearchCandidatesBySourceList();
                    break;
                }

            default:
                break;
        }
    };

    /*****************************************Display Candidate***************************************/

    /***************************************** Boolean search candidate model open after download ***************************************/

    $scope.BooleanMonsterCandidateDetailView = function (candidate) {
        if (candidate.Source == 'Local') {
            $scope.openCandidateFromOtherSection(candidate.Candidate_Id);
        }
        if (candidate.HasLocalCopy) {
            XSeedAlert.swal({
                title: "Are you sure?",
                text: "Local copy for this candidate is available, do you want to view updated profile?",
                type: "warning",
                showCancelButton: true,
                showCloseButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "View Updated",
                cancelButtonText: "View Local",
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function () {
                downloadBooleanSearchCandidateBySource(candidate);
            },
            //function (dismiss) {
            //    if (candidate.Candidate_Id) {
            //        $scope.openCandidateFromOtherSection(candidate.Candidate_Id);
            //    }
            //});
            function (dismiss) {
                if (dismiss == 'cancel') {
                    // function when cancel button is clicked
                    if (candidate.Candidate_Id) {
                        $scope.openCandidateFromOtherSection(candidate.Candidate_Id);
                    }
                }
                else if (dismiss == 'close') {
                    // function when close button is clicked
                }
            });
        }
        else {
            downloadBooleanSearchCandidateBySource(candidate);
        }
    };

    function downloadBooleanSearchCandidateBySource(candidate) {
        if (candidate.Source == 'Monster') {
            blockUI.start();
            var promise = XSeedApiFactory.monsterCandidateDetailView(candidate.ProviderCandidateId, candidate.Candidate_Id);
            promise.then(
              function (response) {
                  $scope.common.candidateModel = response[0].data;
                  angular.forEach($scope.common.candidateListByBooleanSearch, function (val, index) {
                      if (val.ProviderCandidateId == candidate.ProviderCandidateId) {
                          val.Candidate_Id = response[0].data._id;
                          val.HasLocalCopy = true;
                      }
                  });
                  $scope.common.tblCandidateListByBooleanSearch.reload();

                  $scope.common.candidateDetailModal.$promise.then($scope.common.candidateDetailModal.show);
                  blockUI.stop();
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Candidate', httpError.data.Status, httpError.data.Message);
              });
        }
        else if (candidate.Source == 'Dice') {
            blockUI.start();
            var promise = XSeedApiFactory.diceCandidateDetailView(candidate.ProviderCandidateId, candidate.Candidate_Id);
            promise.then(
              function (response) {
                  $scope.common.candidateModel = response[0].data;
                  angular.forEach($scope.common.candidateListByBooleanSearch, function (val, index) {
                      if (val.ProviderCandidateId == candidate.ProviderCandidateId) {
                          val.Candidate_Id = response[0].data._id;
                          val.HasLocalCopy = true;
                      }
                  });
                  $scope.common.tblCandidateListByBooleanSearch.reload();

                  $scope.common.candidateDetailModal.$promise.then($scope.common.candidateDetailModal.show);
                  blockUI.stop();
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Candidate', httpError.data.Status, httpError.data.Message);
              });
        }
    };


    /***************************************** Boolean search candidate model open after download ***************************************/

    /*****************************************boolean Search Monster ***************************************/

    $scope.openSearchCandidatesByBooleanSearch = function () {
        //getMonsterCount();

        //if ($scope.common.selectedJob) {
        $scope.common.candidateBooleanSearchBySourceModel = {};
            $scope.common.MonsterZipPresent = true;
            $scope.common.USLocationList = $rootScope.metadata.GetAncillaryInformationResponse.monsterUSLocation.monsterLocation;
        $scope.common.candidateBooleanSearchBySourceModel.LocationType = "byLocation";
            //buildLookupCandidateSearchByBooleanSearch();

            $scope.BooleanQuerySources = [];
            $scope.BooleanQuerySources.push(
                { Id: "Monster", Name: "Monster" }
                , { Id: "Dice", Name: "Dice" }
                );
        $scope.common.candidateBooleanSearchBySourceModel.Sources = ["Monster", "Dice"];
        //}
    };

    $scope.searchCandidatesByBooleanQuery = function (booleanSearchMonsterPopupForm) {
        if (new ValidationService().checkFormValidity(booleanSearchMonsterPopupForm)
            &&
            (($scope.common.candidateBooleanSearchBySourceModel.LocationType == "byRadius" && ($scope.common.candidateBooleanSearchBySourceModel.MonsterLocationZIPCode != undefined && $scope.common.candidateBooleanSearchBySourceModel.MonsterLocationZIPCode != ""))
            ||
            ($scope.common.candidateBooleanSearchBySourceModel.LocationType == "byLocation" && ($scope.common.candidateBooleanSearchBySourceModel.MonsterLocationZIPCode == undefined || $scope.common.candidateBooleanSearchBySourceModel.MonsterLocationZIPCode != undefined)))
            ) {
            blockUI.start();
            $scope.common.MonsterZipPresent = true;
            $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
            $scope.common.pageNumberShow = undefined;

            searchByBooleanQuery();
        }
        else {
            if ($scope.common.candidateBooleanSearchBySourceModel.LocationType == "byRadius" && ($scope.common.candidateBooleanSearchBySourceModel.MonsterLocationZIPCode == undefined || $scope.common.candidateBooleanSearchBySourceModel.MonsterLocationZIPCode == "")) {
                $scope.common.MonsterZipPresent = false;
            }
            else {
                $scope.common.MonsterZipPresent = true;
            }
        }
    };

    function searchByBooleanQuery() {
        var promise = XSeedApiFactory.searchCandidatesByBooleanQuery($scope.common.candidateBooleanSearchBySourceModel, $scope.common.pageSizeShow, $scope.common.pageNumberShow);
        promise.then(
        function (response) {
            $scope.checkboxesUSLocation.items = {};
            $scope.common.candidateListByBooleanSearch = response[0].data.candidates;

            $scope.candidateSearchDummyModel = {};
            $scope.candidateSearchDummyModel = $scope.common.candidateListByBooleanSearch;

            //$scope.common.totalCount = response[0].data.TotalCount;
            //$scope.common.totalPages = response[0].data.TotalPages;

            if ($scope.common.candidateListByBooleanSearch.length == 0) {
                $scope.common.candidateNullLength = true;
            }
            else {
                $scope.common.candidateNullLength = false;
            }

            $scope.diceBooleanCandidateCount = 0;
            $scope.monsterBooleanCandidateCount = 0;
            if ($scope.common.candidateListByBooleanSearch.length) {
                $scope.common.tblCandidateListByBooleanSearch = undefined;
                angular.forEach($scope.common.candidateListByBooleanSearch, function (val, index) {
                    if (val.Source == 'Monster') {
                        $scope.monsterBooleanCandidateCount = $scope.monsterBooleanCandidateCount + 1;
                    }
                    else if (val.Source == 'Dice') {
                        $scope.diceBooleanCandidateCount = $scope.diceBooleanCandidateCount + 1;
                    }
                });
            }
            populateBooleanSearchCandidatesBySourceList();
            blockUI.stop();
        },
        function (httpError) {
            if (httpError.data.Message == "Internal Server processing error, Recoverable.") {
                httpError.data.Message = "Monster server processing error.";
            }
            blockUI.stop();
            ExceptionHandler.handlerHTTPException('Search', httpError.data.Status, httpError.data.Message);
        });
    }

    function populateBooleanSearchCandidatesBySourceList() {
        if (angular.isDefined($scope.common.tblCandidateListByBooleanSearch)) {
            $scope.common.tblCandidateListByBooleanSearch.reload();
        }
        else {
            $scope.common.tblCandidateListByBooleanSearch = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: $scope.common.ngTablePaginationCount,
                total: $scope.common.candidateListByBooleanSearch.length,
                getData: function ($defer, params) {
                    var filteredData = $filter('filter')($scope.common.candidateListByBooleanSearch, $scope.filter);
                    $scope.CandidateListByBooleanSearchData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
                    params.total($scope.CandidateListByBooleanSearchData.length);
                    $scope.CandidateListByBooleanSearchData = $scope.CandidateListByBooleanSearchData.slice((params.page() - 1) * params.count(), params.page() * params.count());

                    $defer.resolve($scope.CandidateListByBooleanSearchData);
                }
                , $scope: $scope
            });
        }
    }

    // Filter data Total, Dice and Monster - Rohit
    $scope.getFilterDataForBooleanSearch = function (typeName) {
        $scope.XseedDataFilter = [];
        switch (typeName) {
            case 'Total':
                {
                    $scope.common.candidateListByBooleanSearch = $scope.candidateSearchDummyModel;
                    $scope.common.tblCandidateListByBooleanSearch = undefined;
                    populateBooleanSearchCandidatesBySourceList();
                    break;
                }
            case 'Monster':
                {
                    angular.forEach($scope.candidateSearchDummyModel, function (val, index) {
                        if (val.Source == "Monster") {
                            $scope.XseedDataFilter.push(val);
                        }
                    })
                    $scope.common.candidateListByBooleanSearch = $scope.XseedDataFilter;
                    $scope.common.tblCandidateListByBooleanSearch = undefined;
                    populateBooleanSearchCandidatesBySourceList();
                    break;
                }
            case 'Dice':
                {
                    angular.forEach($scope.candidateSearchDummyModel, function (val, index) {
                        if (val.Source == "Dice") {
                            $scope.XseedDataFilter.push(val);
                        }
                    })
                    $scope.common.candidateListByBooleanSearch = $scope.XseedDataFilter;
                    $scope.common.tblCandidateListByBooleanSearch = undefined;
                    populateBooleanSearchCandidatesBySourceList();
                    break;
                }
            default:
                break;
        }
    };

    //Checkbox code-
    $scope.checkboxesBooleanSearch = { 'checked': false, items: {} };

    // watch for check all checkbox
    $scope.$watch('checkboxesBooleanSearch.checked', function (value) {
        angular.forEach($scope.common.candidateListByBooleanSearch, function (item) {
            if (item.HasLocalCopy) {
                $scope.checkboxesBooleanSearch.items[item.Candidate_Id] = value;
            }
            if (!item.HasLocalCopy) {
                $scope.checkboxesBooleanSearch.items[item.ProviderCandidateId] = value;
            }
        });
    });

    // watch for data checkboxes
    $scope.$watch('checkboxesBooleanSearch.items', function (values) {
        if (!$scope.common.candidateListByBooleanSearch) {
            return;
        }
        var checked = 0, unchecked = 0,
            total = $scope.common.candidateListByBooleanSearch.length;
        angular.forEach($scope.common.candidateListByBooleanSearch, function (item) {
            if (item.HasLocalCopy) {
                checked += ($scope.checkboxesBooleanSearch.items[item.Candidate_Id]) || 0;
                unchecked += (!$scope.checkboxesBooleanSearch.items[item.Candidate_Id]) || 0;
            }

            if (!item.HasLocalCopy) {
                checked += ($scope.checkboxesBooleanSearch.items[item.ProviderCandidateId]) || 0;
                unchecked += (!$scope.checkboxesBooleanSearch.items[item.ProviderCandidateId]) || 0;
            }
        });


        $scope.BooleanSearchCheckedCount = checked;
        //alert(checked);
        //alert(unchecked);
        //grayed checkbox
        angular.element(document.getElementById("select_all")).prop("indeterminate", (checked != 0 && unchecked != 0));
    }
    , true
    );

    $scope.openCandidateBooleanSearchSubmissionModal = function (type, candidate) {

        $scope.candidateSubmissionList = [];

        $scope.candidate = candidate;

        if (type == 'Single') {
            //if (candidate.HasLocalCopy) {
            //    candidate.Source = "Local";
            //}
            $scope.candidateSubmissionList.push(candidate);
        }
        else {
            angular.forEach($scope.checkboxesBooleanSearch.items, function (item, Identifier) {
                if (item == true) {
                    var selectedCandidate = _.find($scope.common.candidateListByBooleanSearch, function (o) {
                        if (o.HasLocalCopy) {
                            //o.Source = "Local";
                            return o.Candidate_Id == Identifier;
                        }
                        else {
                            return o.ProviderCandidateId == Identifier;
                        }
                    });
                    $scope.candidateSubmissionList.push(selectedCandidate);
                }
            });
        }

        if ($scope.candidateSubmissionList.length == 0) {
            ExceptionHandler.handlerPopup('Candidate Submission', 'Please select Candidate', '', 'info');
        }
        else {
            //var organizationId = $rootScope.userDetails.OrganizationId;
            var promise = XSeedApiFactory.getSubmitSourcedCandidates($scope.candidateSubmissionList);
            promise.then(
              function (response) {
                  $scope.sourceCandidates = response[0].data;
                  if ($scope.sourceCandidates.length) {
                      getCompanyLookUpList();
                      $scope.CandidateSubmissionModel = {};
                      searchCandidateSubmissionModal.$promise.then(searchCandidateSubmissionModal.show);
                  }
              },
              function (httpError) {
                  //blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Search', httpError.data.Status, httpError.data.Message);
              });
        }
    };

    /***************************************** US Location for boolean Search Monster ***************************************/

    /***************************************** Checkbox code for US Location ***************************************/
    //Checkbox code-
    $scope.checkboxesUSLocation = { 'checked': false, items: {} };

    // watch for check all checkbox
    $scope.$watch('checkboxesUSLocation.checked', function (value) {
        angular.forEach($scope.common.USLocationList, function (item) {
            if (angular.isDefined(item.LocationID)) {
                $scope.checkboxesUSLocation.items[item.LocationID] = value;
            }
        });
    });

    // watch for data checkboxes
    $scope.$watch('checkboxesUSLocation.items', function (values) {
        if (!$scope.common.USLocationList) {
            return;
        }
        var checked = 0, unchecked = 0,
            total = $scope.common.USLocationList.length;
        angular.forEach($scope.common.USLocationList, function (item) {
            checked += ($scope.checkboxesUSLocation.items[item.LocationID]) || 0;
            unchecked += (!$scope.checkboxesUSLocation.items[item.LocationID]) || 0;
        });
        if ((unchecked == 0) || (checked == 0)) {
            $scope.checkboxesUSLocation.checked = (checked == total);
        }

        $scope.checkboxesUSLocationCheckedCount = checked;
        //alert(checked);
        //alert(unchecked);
        //grayed checkbox
        angular.element(document.getElementById("select_all")).prop("indeterminate", (checked != 0 && unchecked != 0));
    }
    , true
    );
    /***************************************** Checkbox code for US Location ***************************************/

    $scope.addUSLocationToList = function (type, USLocation) {
        $scope.common.candidateBooleanSearchBySourceModel.MonsterUSLocationList = [];
        $scope.common.candidateBooleanSearchBySourceModel.MonsterUSLocationID = [];
        $scope.common.candidateBooleanSearchBySourceModel.SearchLocations = [];

        $scope.USLocation = USLocation;

        if (type == 'Single') {
            $scope.common.candidateBooleanSearchBySourceModel.MonsterUSLocationList.push(USLocation);
        }
        else {
            angular.forEach($scope.checkboxesUSLocation.items, function (item, Identifier) {
                if (item == true) {
                    var selectedLocation = _.find($scope.common.USLocationList, function (o) { return o.LocationID == Identifier; });
                    if (selectedLocation) {
                        $scope.common.candidateBooleanSearchBySourceModel.MonsterUSLocationList.push(selectedLocation);
                        $scope.common.candidateBooleanSearchBySourceModel.SearchLocations.push(selectedLocation.StateName);
                    }
                }
            });
            angular.forEach($scope.common.candidateBooleanSearchBySourceModel.MonsterUSLocationList, function (value, key) {
                $scope.common.candidateBooleanSearchBySourceModel.MonsterUSLocationID.push(value.LocationID);
            });
        }
    };
    /***************************************** US Location for boolean Search Monster ***************************************/

    /*****************************************boolean Search Monster ***************************************/

    /*****************************************submission to client***************************************/
    $scope.getRequirementListByCompany = function (companyId) {
        var promise = XSeedApiFactory.getRequirementListByCompany(companyId);
        promise.then(
          function (response) {
              $scope.lookup.requirement = response[0].data;
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
          });
    };

    $scope.openCandidateSubmissionModal = function (type, candidate) {
        $scope.candidateSubmissionList = [];

        $scope.candidate = candidate;

        if (type == 'Single') {
            $scope.candidateSubmissionList.push(candidate);
        }
        else {
            angular.forEach($scope.checkboxes.items, function (item, Identifier) {
                if (item == true) {
                    var selectedCandidate = _.find($scope.common.candidateListBySource, function (o) {
                        if (o.Source == 'Local') {
                            return o.Candidate_Id == Identifier;
                        }
                        else {
                            return o.ProviderCandidateId == Identifier;
                        }
                    });
                    $scope.candidateSubmissionList.push(selectedCandidate);
                }
            });
        }

        if ($scope.candidateSubmissionList.length == 0) {
            ExceptionHandler.handlerPopup('Candidate Submission', 'Please select Candidate', '', 'info');
        }
        else {
            //var organizationId = $rootScope.userDetails.OrganizationId;
            var promise = XSeedApiFactory.getSubmitSourcedCandidates($scope.candidateSubmissionList);
            promise.then(
              function (response) {
                  $scope.sourceCandidates = response[0].data;
                  if ($scope.sourceCandidates.length) {
                      getCompanyLookUpList();
                      $scope.CandidateSubmissionModel = {};
                      searchCandidateSubmissionModal.$promise.then(searchCandidateSubmissionModal.show);
                  }
              },
              function (httpError) {
                  //blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Search', httpError.data.Status, httpError.data.Message);
              });
        }
    };

    $scope.closeSearchCandidateSubmissionModal = function () {
        searchCandidateSubmissionModal.$promise.then(searchCandidateSubmissionModal.hide);
        $scope.checkboxes.items = {};
        $scope.checkboxesBooleanSearch.items = {};
        //refresh source candidate list
        if ($scope.common.candidateListBySource && $scope.common.candidateSearch.searchType == "Precision") {
            searchSourceCandidates();
        }
        //refresh boolean search candidate list
        if ($scope.common.candidateListByBooleanSearch && $scope.common.candidateSearch.searchType == "Boolean") {
            searchByBooleanQuery();
        }
    };

    $scope.submitCandidateProcess = function (candidateSubmissionPopupForm) {
        if (new ValidationService().checkFormValidity(candidateSubmissionPopupForm)) {
            blockUI.start();
            $scope.CandidateSubmissionModel.CandidateList = $scope.sourceCandidates;

            $scope.CandidateSubmissionModel.OrganizationUserId = $rootScope.userDetails.UserId;
            var promise = XSeedApiFactory.submitCandidate($scope.CandidateSubmissionModel);
            promise.then(
              function (response) {
                  blockUI.stop();
                  $scope.closeSearchCandidateSubmissionModal();
                  $scope.dataSubmissiomEmail.successCandidates = response[0].data.successCandidates;
                  var textMsg = response[0].data.message != null ? response[0].data.message : "";
                  $scope.dataSubmissiomEmail.companyId = $scope.CandidateSubmissionModel.CompanyId;
                  $scope.dataSubmissiomEmail.jobId = $scope.CandidateSubmissionModel.JobId;
                  $scope.CandidateSubmissionModel.CompanyId = "";
                  $scope.CandidateSubmissionModel.JobId = "";
                  if ($scope.dataSubmissiomEmail.successCandidates.length > 0) {
                      XSeedAlert.swal({
                          title: 'Success!',
                          text: 'Candidate submitted! ' + textMsg,
                          type: "success",
                          customClass: "xseed-error-alert"
                      }).then(function () {
                          $scope.getSubmissionUrl($scope.dataSubmissiomEmail.companyId);
                          candidateSubmissionEmailModal.$promise.then(candidateSubmissionEmailModal.show);
                          $timeout(function () {
                              angular.forEach($scope.checkboxes.items, function (item, Identifier) {
                                  $scope.checkboxes.items[Identifier] = false;
                              });
                          }, 0);
                      }, function (dismiss) {
                          blockUI.stop();
                      })
                  }
                  else {
                      blockUI.stop();
                      ExceptionHandler.handlerHTTPException('Submission', "warning", response[0].data.message);
                  };
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
              });
        };
    };

    $scope.submitBySubmissionUrl = function () {
        $scope.isSubmitBySubmissionUrl = true;
        $scope.closeSearchCandidateSubmissionModal();
        candidateSubmissionEmailModal.$promise.then(candidateSubmissionEmailModal.hide);
        $location.path("submissionToClient");
    };

    $scope.openCandidateSubmissionEmailModal = function () {
        if ($scope.dataSubmissiomEmail.successCandidates.length > 0) {
            blockUI.start();
            //get company contacts
            var promise = XSeedApiFactory.getSubmissionContactEmail($scope.dataSubmissiomEmail.companyId);
            promise.then(
              function (response) {
                  $rootScope.commonEmailModel.candidateList = [];

                  //get company submissionUrl
                  var promise = XSeedApiFactory.getCompanyDetail($rootScope.userDetails.OrganizationId, $scope.dataSubmissiomEmail.companyId);
                  promise.then(
                    function (response) {
                        $scope.companyData = response[0].data;
                        $rootScope.commonEmailModel.companySubmissionUrl = $scope.companyData.SubmissionPageURL;
                    },
                    function (httpError) {
                        ExceptionHandler.handlerHTTPException('Company', httpError.data.Status, httpError.data.Message);
                    });
                  //get company contact email
                  if (response[0].data) {
                      $rootScope.commonEmailModel.ToEmailID = [];
                      angular.forEach(response[0].data, function (val, index) {
                          $rootScope.commonEmailModel.ToEmailID.push(val.Name);
                      });
                      $rootScope.commonEmailModel.ToEmailID = $rootScope.commonEmailModel.ToEmailID.join();
                  }
                  $rootScope.commonEmailModel.FromEmailID = $rootScope.userDetails.PrimaryEmail;
                  $rootScope.commonEmailModel.MailSubject = "Submissions";
                  $rootScope.commonEmailModel.MailBody = "We would like to submit following candidate(s) against the<br />Requirement: ";
                  //get job title
                  angular.forEach($scope.lookup.requirement, function (val, index) {
                      if ($scope.dataSubmissiomEmail.jobId == val.Id) {
                          $rootScope.commonEmailModel.MailBody = $rootScope.commonEmailModel.MailBody + val.JobTitle;
                      }
                  });
                  //get candidate names
                  $rootScope.commonEmailModel.MailBody = $rootScope.commonEmailModel.MailBody + " <br />following is/are the candidate(s)";
                  angular.forEach($scope.dataSubmissiomEmail.successCandidates, function (val, index) {
                      //var selectedCandidate = _.find($scope.common.candidateListBySource, function (o) { return o.Candidate_Id == val; });
                      $rootScope.commonEmailModel.candidateList.push(val);
                  });
                  angular.forEach($scope.CandidateSubmissionModel.CandidateList, function (val, index) {
                      index = index + 1
                      $rootScope.commonEmailModel.MailBody = $rootScope.commonEmailModel.MailBody + "<br />" + index + ". " + val.Name;
                  });
                  $rootScope.commonEmailModel.typeOfNotification = $scope.constants.submitCandidateByEmail;
                  $scope.common.commonEmailModal.$promise.then($scope.common.commonEmailModal.show);
                  blockUI.stop();
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
              });
        }
    }

    $scope.closeCandidateSubmissionEmailModal = function () {
        candidateSubmissionEmailModal.$promise.then(candidateSubmissionEmailModal.hide);
        $scope.checkboxes.items = {};
        $scope.checkboxesBooleanSearch.items = {};
    }

    function getCompanyLookUpList() {
        var organizationId = $rootScope.userDetails.OrganizationId;
        var promise = XSeedApiFactory.getCompanyList(organizationId);
        promise.then(
          function (response) {
              $scope.lookup.company = response[0].data;
          },
          function (httpError) {
              ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
          });
    };

    /*****************************************submission to client***************************************/
    //***************************************************************************************//

    // Get the render context local to this controller (and relevant params).
    var renderContext = RequestContext.getRenderContext("standard.searchCandidateByJob");
    // The subview indicates which view is going to be rendered on the page.
    $scope.subview = renderContext.getNextSection();
    $scope.$on(
        "requestContextChanged",
        function () {
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {
                return;
            }
            // Update the view that is being rendered.
            $scope.subview = renderContext.getNextSection();
        }
    );
};