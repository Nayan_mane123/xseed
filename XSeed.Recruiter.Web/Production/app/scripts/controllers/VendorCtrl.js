﻿'use strict';

XSEED_APP.controller('VendorCtrl', VendorCtrl);

/**
 * @ngInject
 */
function VendorCtrl($scope, $rootScope, $route, $routeParams, RequestContext, $timeout, $q, $location, $window, configuration, i18nFactory, ExceptionHandler, EventBus, $log, _, ipCookie, ModalFactory, XSeedAlert, localStorageService, $uibModal, XSeedApiFactory, ngTableParams, $filter, ValidationService, $http, blockUI, blockUIConfig, Constants) {

    init();
    var vendorDetailModal = ModalFactory.vendorDetailModal($scope);
    //Initialization Function
    function init() {
        $scope.vendorModel = {};
        var vendorListUrl = "/vendorList";

        if (($scope.isUndefinedOrNull($scope.vendorModel.Id) || $scope.isEmpty($scope.vendorModel.Id)) && ($location.path().toLowerCase() != vendorListUrl.toLowerCase())) {
            $location.path('vendorEmailList');
        }

        $scope.flag.advanceSearchFlag = false;
        $scope.vendorModel.manipulationMode = "Create";
        $scope.showEmailDetail = true;
        vendorLookupList();
    };

    function vendorLookupList() {
        var organizationId = $rootScope.userDetails.OrganizationId;
        var promise = XSeedApiFactory.submissionPopupLookupCall(organizationId);
        promise.then(
          function (response) {
              $scope.lookup.companyData = response[0].data;
          },
          function (httpError) {
              ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
          });
    };
    /* Vendor module start */
    $scope.getVendorList = function () {

        if ($rootScope.userHasToken == false) {
            $location.path('login');
            return false;
        }

        var vendorEmailListURL = '/vendorEmailList';
        var vendorListURL = '/vendorList';
        var createVendorURL = '/createVendor';
        var editVendorURL = '/editVendor';

        var dashboardURL = '/dashboard';

        if (($location.path().toLowerCase() == vendorEmailListURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Vendor_Read == "False") {
            $location.path('dashboard');
            return false;
        }
        if (($location.path().toLowerCase() == vendorListURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Vendor_Read == "False") {
            $location.path('dashboard');
            return false;
        }
        if (($location.path().toLowerCase() == createVendorURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Vendor_Create == "False") {
            $location.path('dashboard');
            return false;
        }
        if (($location.path().toLowerCase() == editVendorURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Vendor_Update == "False") {
            $location.path('dashboard');
            return false;
        }

        blockUI.start();
        var organizationId = $rootScope.userDetails.OrganizationId;
        var promise = XSeedApiFactory.getVendorList(organizationId);
        promise.then(
          function (response) {
              $scope.vendorList = response[0].data;
              populateVendorList();
              blockUI.stop();
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Company', httpError.data.Status, httpError.data.Message);
          });
    };

    function populateVendorList() {
        if (angular.isDefined($scope.tblVendorList)) {
            $scope.tblVendorList.reload();
        }
        else {
            $scope.tblVendorList = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: $scope.common.ngTablePaginationCount,
                total: $scope.vendorList.length,
                getData: function ($defer, params) {
                    var filteredData = $filter('filter')($scope.vendorList, $scope.filter);
                    $scope.vendorListData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
                    params.total($scope.vendorListData.length);
                    $scope.vendorListData = $scope.vendorListData.slice((params.page() - 1) * params.count(), params.page() * params.count());

                    $defer.resolve($scope.vendorListData);
                }
                , $scope: $scope
            });
        }
    };

    $scope.createVendorView = function () {
        $scope.vendorModel = {};
        $scope.vendorModel.DestinationEmailId = configuration.XSEED_EMAIL_PARSER_EMAILID;
        $scope.vendorModel.manipulationMode = 'Create';
        $location.path('createVendor');
    }

    $scope.createVendorProcess = function (createVendorForm) {
        if (new ValidationService().checkFormValidity(createVendorForm)) {
            blockUI.start();
            $scope.vendorModel.OrganizationId = $rootScope.userDetails.OrganizationId;

            var promise = XSeedApiFactory.createVendor($scope.vendorModel);
            promise.then(
              function (response) {
                  blockUI.stop();
                  XSeedAlert.swal({
                      title: 'Success!',
                      text: 'Vendor created successfully!',
                      type: "success",
                      customClass: "xseed-error-alert",
                      allowOutsideClick: false
                  }).then(function () {
                      $timeout(function () {
                          $location.path('vendorList');
                      }, 0);
                  }, function (dismiss) {
                      blockUI.stop();
                  });
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Vendor', httpError.data.Status, httpError.data.Message);
              });
        };
    };

    $scope.editVendorView = function (vendor) {
        blockUI.start();
        $scope.vendorModel = vendor;
        $scope.vendorModel.manipulationMode = 'Edit';
        $location.path('editVendor');
        blockUI.stop();
    };

    $scope.editVendorProcess = function (createVendorForm) {
        if (new ValidationService().checkFormValidity(createVendorForm)) {
            blockUI.start();
            $scope.vendorModel.OrganizationId = $rootScope.userDetails.OrganizationId;

            var promise = XSeedApiFactory.updateVendor($scope.vendorModel);
            promise.then(
              function (response) {
                  blockUI.stop();
                  XSeedAlert.swal({
                      title: 'Success!',
                      text: 'Vendor updated successfully!',
                      type: "success",
                      customClass: "xseed-error-alert",
                      allowOutsideClick: false
                  }).then(function () {
                      $timeout(function () {
                          $location.path('vendorList');
                      }, 0);
                  }, function (dismiss) {
                      blockUI.stop();
                  });
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Vendor', httpError.data.Status, httpError.data.Message);
              });
        };
    };

    $scope.viewVendorDetail = function (vendor) {
        $scope.vendorDetailModel = vendor;
        $scope.vendorDetailModel.SourceEmailList = $scope.vendorDetailModel.SourceEmailId.split(',');
        vendorDetailModal.$promise.then(vendorDetailModal.show);
    };

    $scope.closeVendorDetail = function (vendor) {
        $scope.vendorDetailModel = {};
        vendorDetailModal.$promise.then(vendorDetailModal.hide);
    };

    $scope.enableDisableVendor = function (vendor, isActive) {
        blockUI.start();
        $scope.vendorModel = {};
        $scope.vendorModel = vendor;
        $scope.vendorModel.OrganizationId = $rootScope.userDetails.OrganizationId;
        $scope.vendorModel.IsActive = isActive;
        var textMsg = isActive == true ? "enabled" : "disabled";
        var promise = XSeedApiFactory.updateVendor($scope.vendorModel);
        promise.then(
          function (response) {
              blockUI.stop();
              XSeedAlert.swal({
                  title: 'Success!',
                  text: 'Vendor ' + textMsg + ' successfully!',
                  type: "success",
                  customClass: "xseed-error-alert",
                  allowOutsideClick: false
              }).then(function () {
                  $timeout(function () {
                      $location.path('vendorList');
                  }, 0);
              }, function (dismiss) {
                  blockUI.stop();
              });
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Vendor', httpError.data.Status, httpError.data.Message);
          });
    };

    $scope.getVendorParsedEmailList = function () {

        if ($rootScope.userHasToken == false) {
            $location.path('login');
            return false;
        }

        var vendorEmailListURL = '/vendorEmailList';
        var vendorListURL = '/vendorList';
        var createVendorURL = '/createVendor';
        var editVendorURL = '/editVendor';

        var dashboardURL = '/dashboard';

        if (($location.path().toLowerCase() == vendorEmailListURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Vendor_Read == "False") {
            $location.path('dashboard');
            return false;
        }
        if (($location.path().toLowerCase() == vendorListURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Vendor_Read == "False") {
            $location.path('dashboard');
            return false;
        }
        if (($location.path().toLowerCase() == createVendorURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Vendor_Create == "False") {
            $location.path('dashboard');
            return false;
        }
        if (($location.path().toLowerCase() == editVendorURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Vendor_Update == "False") {
            $location.path('dashboard');
            return false;
        }

        blockUI.start();
        var organizationId = $rootScope.userDetails.OrganizationId;
        var firstPageFlag = 0;
        $scope.common.pageSizeShow = undefined;
        $scope.common.pageNumberShow = undefined;

        var promise = XSeedApiFactory.getVendorParsedEmailList(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow);
        promise.then(
          function (response) {
              $scope.vendorEmailList = response[0].data.VendorEmail;
              $scope.common.totalCount = response[0].data.TotalCount;
              $scope.common.totalPages = response[0].data.TotalPages;

              if ($scope.vendorEmailList) {
                  angular.forEach($scope.vendorEmailList, function (val, index) {
                      if (val.Html != null) {
                          val.Subject = val.Html.split('|')[0];
                          val.Body = val.Html.split('|')[1];
                      }
                  });

                  //to select first row by default
                  $scope.showVendorEmailDetails($scope.vendorEmailList[0]);
              }
              firstPageFlag = 1;
              populateVendorParsedEmailList(organizationId, $scope.vendorEmailList, $scope.common.totalCount, $scope.common.totalPages, firstPageFlag);

              blockUI.stop();
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Vendor', httpError.data.Status, httpError.data.Message);
          });
    };

    function populateVendorParsedEmailList(organizationId, vendorEmailList, totalCount, totalPages, pageFlag) {
        if (angular.isDefined($scope.tblVendorEmailList)) {
            $scope.tblVendorEmailList.reload();
        }
        else {
            $scope.tblVendorEmailList = new ngTableParams({
                page: 1,
                count: 15,
                sorting: {}
            }, {
                counts: [15, 20, 25],
                total: totalCount,
                getData: function ($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    /* sorting */
                    if (pageFlag != 1) {
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function (val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        ///* sorting */

                        var promise = XSeedApiFactory.getVendorParsedEmailList(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder);
                        promise.then(
                          function (response) {
                              $scope.vendorEmailList = response[0].data.VendorEmail;
                              $scope.common.totalCount = response[0].data.TotalCount;
                              $scope.common.totalPages = response[0].data.TotalPages;

                              //to select first row by default
                              $scope.showVendorEmailDetails($scope.vendorEmailList[0]);

                              //Manupulation of response
                              if ($scope.vendorEmailList) {
                                  angular.forEach($scope.vendorEmailList, function (val, index) {
                                      if (val.Html != null) {
                                          val.Subject = val.Html.split('|')[0];
                                          val.Body = val.Html.split('|')[1];
                                      }
                                  });
                              }

                              blockUI.stop();

                          },
                          function (httpError) {
                              blockUI.stop();
                              ExceptionHandler.handlerHTTPException('Vendor', httpError.data.Status, httpError.data.Message);
                          });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.vendorEmailList);
                }
                , $scope: $scope
            });
        }
    };

    $scope.showVendorEmailDetails = function (vendorEmail) {
        if (vendorEmail) {
            $scope.showEmailDetail = true;
            $scope.selectedVendorEmail = vendorEmail;
            $scope.updateVendorEmailDetails();
        }
    };

    $scope.updateVendorEmailDetails = function () {
        if ($rootScope.userDetails.Vendor_Read == "True") {
            if (!$scope.selectedVendorEmail.IsRead) {
                $scope.selectedVendorEmail.IsRead = true;

                var promise = XSeedApiFactory.updateVendorEmailDetails($scope.selectedVendorEmail);
                promise.then(
                  function (response) {
                      $scope.unreadMailCount = $scope.unreadMailCount - 1;
                  },
                  function (httpError) {
                      ExceptionHandler.handlerHTTPException('Vendor', httpError.data.Status, httpError.data.Message);
                  });
            }
        }
    };

    $scope.getUnreadMailCount = function () {
        if ($rootScope.userDetails.Vendor_Read == "True") {
            var promise = XSeedApiFactory.getUnreadMailCount($rootScope.userDetails.OrganizationId);
            promise.then(
              function (response) {
                  blockUI.stop();
                  $scope.unreadMailCount = response[0].data;
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Vendor', httpError.data.Status, httpError.data.Message);
              });
        }
    };

    $scope.cancelVendor = function () {
        $location.path('vendorList');
    };

    $scope.showHideEmail = function () {
        $scope.showEmailDetail = false;
    };

    //***************************************************************************************//

    // Get the render context local to this controller (and relevant params).
    var renderContext = RequestContext.getRenderContext("standard.vendor");
    // The subview indicates which view is going to be rendered on the page.
    $scope.subview = renderContext.getNextSection();
    $scope.$on(
        "requestContextChanged",
        function () {
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {
                return;
            }
            // Update the view that is being rendered.
            $scope.subview = renderContext.getNextSection();
        }
    );
};