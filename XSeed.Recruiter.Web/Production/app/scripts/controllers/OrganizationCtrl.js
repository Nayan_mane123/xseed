﻿'use strict';

XSEED_APP.controller('OrganizationCtrl', OrganizationCtrl);

/**
 * @ngInject
 */
function OrganizationCtrl($scope, $rootScope, $route, $routeParams, RequestContext, $timeout, $q, $location, $window, configuration, i18nFactory, ExceptionHandler, EventBus, $log, _, ipCookie, blockUI, ModalFactory, XSeedAlert, localStorageService, $uibModal, XSeedApiFactory, ngTableParams, $filter, ValidationService) {

    var xSeedValidation = new ValidationService();
    $scope.filter = {};
    $scope.systemUsersListfilter = {};

    $scope.common.newOrganizationUserDetails = {};
    $scope.common.currentOrganizationUserDetails = {};
    $scope.userEmailConfiguration = {};
    $scope.TransferRequirementsModel = {};
    $scope.TransferRequirementsModal = ModalFactory.TransferRequirementsModal($scope);
    $scope.common.minProfileImgFileSize = false;

    //$log.info(configuration.GET_METADATA_API_URL);

    $scope.$watch('location.search()', function () {
        init();
    }, true);

    //Initialization Function
    function init() {
        //$scope.userRoles = angular.copy($scope.metadata.GetAncillaryInformationResponse.userRoleList.userRole);
        var editUserURL = '/editUser';
        var userProfileURL = '/userProfile';


        if ($location.path().toLowerCase() == editUserURL.toLowerCase() || $location.path().toLowerCase() == userProfileURL.toLowerCase()) {
            $location.path('systemUsers');
        }

        //$scope.maxDate = new Date().toString();
        //$scope.minDate = new Date().toString();
        var date = new Date();
        $scope.maxDate = ("0" + (date.getMonth() + 1).toString()).substr(-2) + "/" + ("0" + date.getDate().toString()).substr(-2) + "/" + (date.getFullYear().toString());
        $scope.minDate = ("0" + (date.getMonth() + 1).toString()).substr(-2) + "/" + ("0" + date.getDate().toString()).substr(-2) + "/" + (date.getFullYear().toString());
    };

    //********************************************Users Section Start*****************************************
    function isRouteRedirect(route) {
        return (!route.current.action);
    };

    $scope.createNewOrganizationUser = function (newOrgUserForm) {
        if (new ValidationService().checkFormValidity(newOrgUserForm) && !$scope.common.maxProfileImgFileSize && !$scope.common.invalidProfileImgFile) {
            blockUI.start();
            $scope.common.maxProfileImgFileSize = false;
            $scope.common.invalidProfileImgFile = false;
            $scope.common.newOrganizationUserDetails.OrganizationId = $rootScope.userDetails.OrganizationId;
            $scope.common.newOrganizationUserDetails.UserId = $rootScope.userDetails.UserId;

            $scope.common.newOrganizationUserDetails.BirthDate = $filter('date')($scope.common.newOrganizationUserDetails.BirthDate, "yyyy-MM-dd");
            $scope.common.newOrganizationUserDetails.AnniversaryDate = $filter('date')($scope.common.newOrganizationUserDetails.AnniversaryDate, "yyyy-MM-dd");

            var promise = XSeedApiFactory.createNewOrganizationUser($scope.common.newOrganizationUserDetails);
            promise.then(
              function (response) {
                  blockUI.stop();

                  XSeedAlert.swal({
                      title: 'Success!',
                      text: 'User created successfully and login credentials have been sent to primary email address!',
                      type: "success",
                      customClass: "xseed-error-alert",
                      //showCancelButton: true,
                      //confirmButtonText: "OK",
                      //closeOnConfirm: true
                  }).then(function () {
                      //$location.path('systemUsers');
                      $scope.common.newOrganizationUserDetails = {};
                      $timeout(function () {
                          $location.path('systemUsers');
                      }, 0);
                  }, function (dismiss) {
                      // dismiss can be 'cancel', 'overlay', 'close', 'timer'
                      //if (dismiss == 'cancel'){alert('Close fired');}
                  });

              },
              function (httpError) {
                  if (httpError.data.Message == "Unsupported Media Type.") {
                      httpError.data.Message = "Plese select valid file from .jpg, .jpeg, .png or .bmp";
                  }
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('OrganizationUser', httpError.data.Status, httpError.data.Message);
              });
        };
    }

    /* get Organization users to Transfer Requirements */
    function getOrganizationUsers(OrganizationId, oldUser) {
        var promise = XSeedApiFactory.getLookUpUsersToTransfer(OrganizationId);
        promise.then(
          function (response) {
              $scope.lkUsers = response[0].data;
              $scope.OraganizationUsers = [];
              $scope.TransferRequirementsModel.FullName = oldUser.FirstName + " " + oldUser.LastName + " (" + oldUser.RoleName + ")";
              $scope.TransferRequirementsModel.UserId = oldUser.UserId;
              angular.forEach($scope.lkUsers, function (val, index) {
                  if (val.Id != oldUser.Id) {
                      $scope.NewUser = [];
                      $scope.NewUser.Id = val.Id;
                      $scope.NewUser.Name = val.Name + " (" + val.RoleName + ")";
                      $scope.OraganizationUsers.push($scope.NewUser);
                  }
              });
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
          });
    };

    $scope.openTransferRequirementModal = function (IsShow, OrganizationId, Olduser) {
        if (IsShow) {
            $scope.TransferRequirementsModel.TransferedUserId = "";
            getOrganizationUsers(OrganizationId, Olduser);
            $timeout(function () {

                if ($scope.OraganizationUsers.length > 0) {
                    $scope.TransferRequirementsModal.$promise.then($scope.TransferRequirementsModal.show);
                }
                else {
                    XSeedAlert.swal({
                        title: "Warning",
                        text: "Recruiters does not exists to transfer requirements.",
                        type: "warning",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ok"
                    }).then(function () {
                    }, function (dismiss) {
                    });
                }
            }, 500);
        }
    };

    $scope.closeTransferRequirementModal = function () {
        $scope.TransferRequirementsModal.$promise.then($scope.TransferRequirementsModal.hide);
        $scope.TransferRequirementsModel = {};
    };

    $scope.TransferRequirements = function (userId, transferedUserId, transferForm) {
        if (new ValidationService().checkFormValidity(transferForm)) {
            if (userId != null && transferedUserId != null) {
                XSeedAlert.swal({
                    title: "Are you sure?",
                    text: "You want to transfer requirements to this user?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    closeOnConfirm: false,
                    closeOnCancel: false
                }).then(function () {
                    var promise = XSeedApiFactory.TransferRequirements(userId, transferedUserId);

                    promise.then(
                      function (response) {
                          blockUI.stop();
                          var isShow = response[0].data;
                          XSeedAlert.swal({
                              title: 'Success!',
                              text: "Requirements transferred successfully. Now you can disable the user.",
                              type: "success",
                              customClass: "xseed-error-alert"
                          }).then(function () {
                              $scope.closeTransferRequirementModal();
                          }, function (dismiss) {
                              $scope.closeTransferRequirementModal();
                          });
                      },
                      function (httpError) {
                          blockUI.stop();
                          ExceptionHandler.handlerHTTPException('OrganizationUser', httpError.data.Status, httpError.data.Message);
                      });
                }, function (dismiss) {
                    $scope.closeTransferRequirementModal();
                });
            }
        }
    }


    /* Transfer Requirements Ends */

    /*Enable and disable the user from the system */
    $scope.EnableDisableUser = function (organizationUser, isActive) {
        if (organizationUser != undefined && isActive != undefined) {
            XSeedAlert.swal({
                title: "Are you sure?",
                text: isActive == true ? "You want to activate this user?" : "You want to deactivate this user?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function () {
                var promise = XSeedApiFactory.enableDisableUser(organizationUser.UserId, organizationUser.OrganizationId, isActive);

                promise.then(
                  function (response) {
                      blockUI.stop();
                      var isShow = response[0].data;
                      if (isShow) {
                          XSeedAlert.swal({
                              title: "Warning",
                              text: "There are requirements assigned to this user. First you need to transfer/assign requirements to other user. Do you want to transfer?",
                              type: "warning",
                              showCancelButton: true,
                              confirmButtonClass: "btn-danger",
                              confirmButtonText: "Yes",
                              cancelButtonText: "No",
                              closeOnConfirm: false,
                              closeOnCancel: false
                          }).then(function () { $scope.openTransferRequirementModal(isShow, organizationUser.OrganizationId, organizationUser); }).then(function () {

                          }, function (dismiss) {
                              $timeout(function () {
                                  $location.path('systemUsers');
                              }, 0);
                          });
                      }
                      else {
                          $scope.common.currentOrganizationUserDetails.IsActive = isActive;
                          XSeedAlert.swal({
                              title: 'Success!',
                              text: isActive == true ? "User activated successfully!" : "User deactivated successfully!",
                              type: "success",
                              customClass: "xseed-error-alert",

                          }).then(function () {
                          }, function (dismiss) {
                          });
                      }
                  },
                  function (httpError) {
                      blockUI.stop();
                      ExceptionHandler.handlerHTTPException('OrganizationUser', httpError.data.Status, httpError.data.Message);
                  });
            }, function (dismiss) {
            });

        }
    };

    $scope.saveEditOrgUserDetails = function (editOrgUserForm) {

        if (new ValidationService().checkFormValidity(editOrgUserForm) && !$scope.common.maxProfileImgFileSize && !$scope.common.invalidProfileImgFile) {

            $scope.common.maxProfileImgFileSize = false;
            $scope.common.invalidProfileImgFile = false;
            $scope.common.currentOrganizationUserDetails.UpdateUserPermissions = false;

            if ($scope.masterRoleId != $scope.common.currentOrganizationUserDetails.RoleId) {
                XSeedAlert.swal({
                    title: "Are you sure?",
                    text: "This action will update permissions for this user as per the selected role!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Update",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: false,
                    closeOnCancel: false
                }).then(function () {
                    $scope.common.currentOrganizationUserDetails.UpdateUserPermissions = true;
                    $scope.common.currentOrganizationUserDetails.BirthDate = $filter('date')($scope.common.currentOrganizationUserDetails.BirthDate, "MM/dd/yyyy");
                    $scope.common.currentOrganizationUserDetails.AnniversaryDate = $filter('date')($scope.common.currentOrganizationUserDetails.AnniversaryDate, "MM/dd/yyyy");

                    blockUI.start();
                    var promise = XSeedApiFactory.saveEditOrgUserDetails($scope.common.currentOrganizationUserDetails);
                    promise.then(
                      function (response) {
                          blockUI.stop();
                          //$scope.common.currentOrganizationUserDetailsMaster = $scope.common.currentOrganizationUserDetails;                  
                          $scope.common.currentOrganizationUserDetailsMaster = angular.copy($scope.common.currentOrganizationUserDetails);

                          XSeedAlert.swal({
                              title: 'Success!',
                              text: 'User details updated successfully.',
                              type: "success",
                              customClass: "xseed-error-alert",
                              //showCancelButton: true,
                              //confirmButtonText: "OK",
                              //closeOnConfirm: true
                          }).then(function () {
                              //$location.path('systemUsers');
                              $timeout(function () {
                                  $location.path('systemUsers');
                              }, 0);
                          }, function (dismiss) {
                              //console.log('dismiss: ' + dismiss);
                              // dismiss can be 'cancel', 'overlay', 'close', 'timer'
                              //if (dismiss == 'cancel'){alert('Close fired');}
                          })


                      },
                      function (httpError) {
                          blockUI.stop();
                          ExceptionHandler.handlerHTTPException('Organization', httpError.data.Status, httpError.data.Message);
                      });
                }, function (dismiss) {
                    return false;
                });
            }
            else {
                $scope.common.currentOrganizationUserDetails.BirthDate = $filter('date')($scope.common.currentOrganizationUserDetails.BirthDate, "yyyy-MM-dd");
                $scope.common.currentOrganizationUserDetails.AnniversaryDate = $filter('date')($scope.common.currentOrganizationUserDetails.AnniversaryDate, "yyyy-MM-dd");

                blockUI.start();
                var promise = XSeedApiFactory.saveEditOrgUserDetails($scope.common.currentOrganizationUserDetails);
                promise.then(
                  function (response) {
                      blockUI.stop();
                      //$scope.common.currentOrganizationUserDetailsMaster = $scope.common.currentOrganizationUserDetails;                  
                      $scope.common.currentOrganizationUserDetailsMaster = angular.copy($scope.common.currentOrganizationUserDetails);

                      XSeedAlert.swal({
                          title: 'Success!',
                          text: 'User details updated successfully.',
                          type: "success",
                          customClass: "xseed-error-alert",
                          //showCancelButton: true,
                          //confirmButtonText: "OK",
                          //closeOnConfirm: true
                      }).then(function () {
                          //$location.path('systemUsers');
                          $timeout(function () {
                              $location.path('systemUsers');
                          }, 0);
                      }, function (dismiss) {
                          //console.log('dismiss: ' + dismiss);
                          // dismiss can be 'cancel', 'overlay', 'close', 'timer'
                          //if (dismiss == 'cancel'){alert('Close fired');}
                      })
                  },
                  function (httpError) {
                      if (httpError.data.Message == "Unsupported Media Type.") {
                          httpError.data.Message = "Plese select valid file from .jpg, .jpeg, .png or .bmp";
                      }
                      blockUI.stop();
                      ExceptionHandler.handlerHTTPException('OrganizationUser', httpError.data.Status, httpError.data.Message);
                  });
            }
        }
    };

    $scope.enableButtonProfile = function (OrgUserProfile, mode) {
        if (OrgUserProfile) {
            if (mode == "edit") {
                $scope.common.currentOrganizationUserDetails.ProfileImageFile = null;
            }
            if (mode == "create") {
                $scope.common.newOrganizationUserDetails.ProfileImageFile = null;
            }
            if (mode == "") {
                $scope.organizationDetails.ProfileImageFile = null;
                $scope.common.uploadFileName = null;
            }

        }
        $scope.common.maxProfileImgFileSize = false;
        $scope.common.invalidProfileImgFile = false;
    }

    $scope.gotoSystemUsers = function () {
        $scope.common.newOrganizationUserDetails = {};
        $location.path('systemUsers');
    }

    $scope.getReportingAuthorities = function () {
        if ($rootScope.userDetails.OrganizationId) {
            blockUI.start();
            var promise = XSeedApiFactory.getReportingAuthorities($rootScope.userDetails.OrganizationId);
            promise.then(
              function (response) {
                  blockUI.stop();
                  $scope.reportingAuthorities = response[0].data;
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
              });
        }
    }

    $scope.getOrganizationUsers = function () {

        //$.$scope.systemUsersListfilter = {};
        $scope.common.newOrganizationUserDetails = {};

        if ($rootScope.userDetails.OrganizationId) {
            blockUI.start();
            var promise = XSeedApiFactory.getOrganizationUsers($rootScope.userDetails.OrganizationId);
            promise.then(
              function (response) {
                  blockUI.stop();
                  $scope.organizationUsers = response[0].data;
                  $scope.reportingAuthorities = response[1].data;
                  $rootScope.titles = response[2].data;
                  $rootScope.userRoles = response[3].data;

                  $scope.UsersIsActive = true;
                  $scope.UsersIsDisable = true;

                  angular.forEach($scope.organizationUsers, function (val, index) {
                      if (val.IsActive == true) {
                          $scope.UsersIsActive = false;

                      }

                      if (val.IsActive != true) {
                          $scope.UsersIsDisable = false;
                      }
                  });

                  $scope.reportingTo = function (id) {
                      return $scope.reportingAuthorities.filter(function (item) {
                          return (item.Id === id);
                      })[0].Name;
                  };

                  //populateSystemUsersList();
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('OrganizationUser', httpError.data.Status, httpError.data.Message);
              });
        }
    }

    function populateSystemUsersList() {
        if (angular.isDefined($scope.systemUsersList)) {
            $scope.systemUsersList.reload();
        }
        else {
            $scope.systemUsersList = new ngTableParams({
                page: 1,
                count: 10,
                sorting: {}
            }, {
                counts: [],
                total: $scope.organizationUsers.length,
                getData: function ($defer, params) {
                    var filteredData = $filter('filter')($scope.organizationUsers, $scope.systemUsersListfilter);
                    $scope.systemUsersListData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
                    params.total($scope.systemUsersListData.length);
                    $scope.systemUsersListData = $scope.systemUsersListData.slice((params.page() - 1) * params.count(), params.page() * params.count());

                    $defer.resolve($scope.systemUsersListData);
                }
                , $scope: $scope
            });
        }
    }

    $scope.getFullName = function (firstName, lastName) {
        var fname = firstName == null ? '' : firstName;
        var lname = lastName == null ? '' : lastName;
        return fname + ' ' + lname;
    }

    $scope.state = function (id) {
        if (angular.isDefined($scope.stateList) && $scope.stateList != null) {
            return $scope.stateList.filter(function (item) {
                return (item.Id === id);
            })[0].Name;
        }
    };

    $scope.city = function (id) {
        if (angular.isDefined($scope.cityList) && $scope.cityList != null) {
            return $scope.cityList.filter(function (item) {
                return (item.Id === id);
            })[0].Name;
        }
    };

    $scope.getReportingAuthoritiesByRole = function (roleId) {
        var promise = XSeedApiFactory.getReportingAuthoritiesByRole($rootScope.userDetails.OrganizationId, roleId);
        promise.then(
          function (response) {
              $scope.reportingAuthorities = response[0].data;
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
          });
    }

    $scope.getOrganizationCurrentUserDetails = function (email, userId, roleId, redirectTo) {

        localStorageService.set("userId", userId);
        $scope.common.maxProfileImgFileSize = false;
        $scope.common.invalidProfileImgFile = false;
        blockUI.start();
        var promise = XSeedApiFactory.getOrganizationUserDetails(email, $rootScope.userDetails.OrganizationId, userId, roleId);
        promise.then(
            function (response) {
                blockUI.stop();
                $scope.masterRoleId = response[0].data.RoleId;
                //alert($scope.masterRoleId);
                $scope.common.currentOrganizationUserDetails = response[0].data;
                //$scope.reportingAuthWithoutCurrUser = response[1].data;
                $scope.reportingAuthorities = response[1].data;

                $scope.common.currentOrganizationUserDetails.ProfileImagePath = configuration.XSEED_ORGUSER_IMAGE_PATH + $scope.common.currentOrganizationUserDetails.ProfileImage;
                $scope.common.currentOrganizationUserDetails.BirthDate = $filter('date')($scope.common.currentOrganizationUserDetails.BirthDate, "MM/dd/yyyy");
                $scope.common.currentOrganizationUserDetails.AnniversaryDate = $filter('date')($scope.common.currentOrganizationUserDetails.AnniversaryDate, "MM/dd/yyyy");
                //console.log($scope.common.currentOrganizationUserDetails.ProfileImagePath);

                if (angular.isDefined($scope.common.currentOrganizationUserDetails.CountryId) && $scope.common.currentOrganizationUserDetails.CountryId != null) {
                    $scope.country = function (id) {
                        return $rootScope.metadata.GetAncillaryInformationResponse.countryList.country.filter(function (item) {
                            return (item.Id === id);
                        })[0].Name;
                    };
                    $scope.getStateListByCountry($scope.common.currentOrganizationUserDetails.CountryId);
                }

                if (angular.isDefined($scope.common.currentOrganizationUserDetails.StateId) && $scope.common.currentOrganizationUserDetails.StateId != null) {
                    $scope.getCityListByState($scope.common.currentOrganizationUserDetails.StateId);
                }

                if ($scope.common.currentOrganizationUserDetails.ReportingTo) {
                    $scope.common.currentOrganizationUserDetails.ReportingToName = $scope.reportingTo($scope.common.currentOrganizationUserDetails.ReportingTo);
                }
                $location.path(redirectTo);
            },
            function (httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('OrganizationUser', httpError.data.Status, httpError.data.Message);
            });
    }

    $scope.reportingTo = function (id) {
        return $scope.reportingAuthorities.filter(function (item) {
            return (item.Id === id);
        })[0].Name;
    };

    function getUserDetails(userName) {
        blockUI.start();
        var promise = XSeedApiFactory.getUserDetails(userName);
        promise.then(
          function (response) {
              blockUI.stop();
              $rootScope.userDetails = response[0].data;
              localStorageService.set("userDetails", response[0].data);
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('OrganizationUser', httpError.data.Status, httpError.data.Message);
          });
    };

    $scope.saveUserEmailConfiguration = function (userEmailConfigurationForm) {

        if (new ValidationService().checkFormValidity(userEmailConfigurationForm)) {
            blockUI.start();
            $scope.userEmailConfiguration.UserId = $rootScope.userDetails.UserId;
            var promise = XSeedApiFactory.saveUserEmailConfiguration($scope.userEmailConfiguration);
            promise.then(
              function (response) {
                  blockUI.stop();

                  XSeedAlert.swal({
                      title: 'Success!',
                      text: 'Your email configurations have been saved successfully.',
                      type: "success",
                      customClass: "xseed-error-alert",
                  }).then(function () {
                      $timeout(function () {
                          $location.path('userEmailConfiguration');
                      }, 0);
                  }, function (dismiss) {
                  });

              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('User', httpError.data.Status, httpError.data.Message);
              });
        };
    };

    $scope.editUserEmailConfiguration = function (userEmailConfigurationForm) {

        if (new ValidationService().checkFormValidity(userEmailConfigurationForm)) {
            blockUI.start();
            $scope.userEmailConfiguration.UserId = $rootScope.userDetails.UserId;
            var promise = XSeedApiFactory.editUserEmailConfiguration($scope.userEmailConfiguration);
            promise.then(
              function (response) {
                  blockUI.stop();

                  XSeedAlert.swal({
                      title: 'Success!',
                      text: 'Your email configurations have been updated successfully.',
                      type: "success",
                      customClass: "xseed-error-alert",
                  }).then(function () {
                      $timeout(function () {
                          $location.path('userEmailConfiguration');
                      }, 0);
                  }, function (dismiss) {
                  });

              },
              function (httpError) {
                  blockUI.stop();
                  if (httpError) {
                      ExceptionHandler.handlerPopup('Warning', httpError.data, 'Warning', 'error');
                  }
                  else {
                      ExceptionHandler.handlerHTTPException('AccessTokenRequest', null, httpError.data);
                  }
              });
        };
    };

    $scope.getUserEmailConfiguration = function () {

        blockUI.start();
        var UserId = $rootScope.userDetails.UserId;
        var promise = XSeedApiFactory.getUserEmailConfiguration(UserId);
        promise.then(
          function (response) {
              blockUI.stop();
              $scope.userEmailConfiguration.EmailExist = response[0].data.Email;
              $scope.userEmailConfiguration.Email = response[0].data.Email;
              $scope.userEmailConfiguration.Password = response[0].data.Password;
          },
          function (httpError) {
              blockUI.stop();
              if (httpError) {
                  ExceptionHandler.handlerPopup('Warning', httpError.data, 'Warning', 'error');
              }
              else {
                  ExceptionHandler.handlerHTTPException('AccessTokenRequest', null, httpError.data);
              }
          });

    };

    //********************************************Users Section End*****************************************

    /* Organization */


    $scope.resetOrganizationDetail = function () {
        //$scope.organizationDetails = angular.copy($scope.organizationDetails);
        // Task Number 931 - File name keeps in Cache.
        $scope.common.uploadFileName = null;
        $location.path('organizationProfile');
    };

    $scope.getOrganizationDetails = function () {

        if ($rootScope.userHasToken == false) {
            $location.path('login');
            return false;
        }

        var organizationProfileURL = '/organizationProfile';
        var dashboardURL = '/dashboard';

        if (($location.path().toLowerCase() == organizationProfileURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Organization_Read == "False") {
            $location.path('dashboard');
            return false;
        }
        blockUI.start();
        $scope.invalid = false;
        $scope.maxSize = false;
        $scope.common.maxProfileImgFileSize = false;
        $scope.common.invalidProfileImgFile = false;
        $scope.orgLogoFileName = "";
        var promise = XSeedApiFactory.getOrganizationDetails($rootScope.userDetails.OrganizationId);
        promise.then(
          function (response) {
              blockUI.stop();
              $rootScope.organizationDetails = response[0].data;
              $timeout(function () {
                  $scope.getStateListByCountry($rootScope.organizationDetails.CountryId);
                  $scope.getCityListByState($rootScope.organizationDetails.StateId);
              }, 100);

              $timeout(function () {
                  $scope.stateValuebyId($rootScope.organizationDetails.StateId);
                  $scope.cityValuebyId($rootScope.organizationDetails.CityId);
              }, 450);

              if ($rootScope.organizationDetails.ProfileImage) {
                  $rootScope.organizationDetails.logoImagePath = configuration.XSEED_ORG_IMAGE_PATH + $rootScope.organizationDetails.ProfileImage;
              }
              else {
                  $rootScope.organizationDetails.logoImagePath = configuration.XSEED_NO_IMAGE_COMPANY_PATH;
              }
              //console.log($rootScope.organizationDetails.logoImagePath);
              $rootScope.organizationDetails.CountryList = $rootScope.metadata.GetAncillaryInformationResponse.countryList.country;
              $rootScope.filterExpr = { id: $rootScope.organizationDetails.CountryId };
              //console.log($rootScope.organizationDetails.CountryList);
              //console.log($rootScope.filterExpr);
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Organization', httpError.data.Status, httpError.data.Message);
          });
    };

    $scope.updateOrganization = function (frmUpdateOrganization, organizationDetails) {
        if (new ValidationService().checkFormValidity(frmUpdateOrganization) && !$scope.common.maxProfileImgFileSize && !$scope.common.invalidProfileImgFile && !$scope.common.minProfileImgFileSize) {
            $scope.common.minProfileImgFileSize = false;
            if (frmUpdateOrganization.ProfileImageFile != null)
                frmUpdateOrganization.ProfileImage = frmUpdateOrganization.ProfileImageFile.name;
            blockUI.start();
            var promise = XSeedApiFactory.updateOrganization(organizationDetails);
            promise.then(
              function (response) {
                  blockUI.stop();
                  XSeedAlert.swal({
                      title: 'Success!',
                      text: 'Organization details updated successfully!',
                      type: "success",
                      customClass: "xseed-error-alert",
                      allowOutsideClick: false
                  }).then(function () {
                      $timeout(function () {
                          $location.path('/organizationProfile')
                      }, 0);
                  }, function (dismiss) {
                      blockUI.stop();

                  });
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Organization', httpError.data.Status, httpError.data.Message);
              });
        }
    };
    /* Organization */


    // Listen for route changes so that we can trigger request-context change events.
    $scope.$on("$routeChangeSuccess", function (event) {
        // If this is a redirect directive, then there's no taction to be taken.

        if (isRouteRedirect($route)) {
            return;
        }

        $scope.currentLocation = $route.current.action;

        $scope.pageTitle = $route.current.pageTitle;
        // Update the current request action change.
        RequestContext.setContext($route.current.action, $routeParams);

        // Announce the change in render conditions.
        $scope.$broadcast("requestContextChanged", RequestContext);

    });

    //---------------------------------------------------------------------------//

    // Get the render context local to this controller (and relevant params).
    var renderContext = RequestContext.getRenderContext("standard.organization");
    // The subview indicates which view is going to be rendered on the page.
    $scope.subview = renderContext.getNextSection();
    $scope.$on(
        "requestContextChanged",
        function () {
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {
                return;
            }
            // Update the view that is being rendered.
            $scope.subview = renderContext.getNextSection();
        }
    );
};
