'use strict';

XSEED_APP.controller('LandingCtrl', LandingCtrl);

/**
 * @ngInject
 */
function LandingCtrl($scope, $rootScope, $route, $routeParams, RequestContext, localStorageService, $log, ExceptionHandler, $location, blockUI, XSeedApiFactory, ModalFactory, XSeedAlert, $filter, ValidationService, configuration, $timeout, $cookies, $window, $interval) {

    $scope.forgotPasswordModel = {};
    $scope.resetPasswordModel = {};
    $scope.registerRequest = {};
    $scope.loginRequest = {};
    $scope.showOtherLogin = false;
    getCookieStayLoggedIn();
    // Get the render context local to this controller (and relevant params).
    var renderContext = RequestContext.getRenderContext("landing");
    // The subview indicates which view is going to be rendered on the page.
    $scope.subview = renderContext.getNextSection();
    $scope.$on(
        "requestContextChanged",
        function () {
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {
                return;
            }
            // Update the view that is being rendered.
            $scope.subview = renderContext.getNextSection();
        }
    );

    $scope.showEmployerCandidateLogin = function () {
        if ($scope.showOtherLogin) {
            $scope.showOtherLogin = false;
        }
        else {
            $scope.showOtherLogin = true;
        }
    };

    $scope.gotoForgotPassword = function () {
        $scope.forgotPasswordModel = {};
        $scope.resetPasswordModel = {};
        $scope.registerRequest = {};
        $scope.loginRequest = {};
        $location.path('forgotPassword');
    }

    $scope.gotoRegister = function () {
        $scope.forgotPasswordModel = {};
        $scope.resetPasswordModel = {};
        $scope.registerRequest = {};
        $scope.loginRequest = {};
        $location.path('register');
    }

    $scope.gotoLogin = function () {
        $scope.forgotPasswordModel = {};
        $scope.resetPasswordModel = {};
        $scope.registerRequest = {};
        $scope.loginRequest = {};
        $location.path('login');
    }

    $scope.register = function (registrationForm) {
        if (new ValidationService().checkFormValidity(registrationForm)) {
            var request = new API.RegisterRequest();
            request.firstName = $scope.registerRequest.FirstName;
            request.lastName = $scope.registerRequest.LastName;
            request.userName = $scope.registerRequest.userName;
            request.password = $scope.registerRequest.password;
            request.confirmPassword = $scope.registerRequest.confirmPassword;
            request.organizationName = $scope.registerRequest.organizationName;
            request.Website = $scope.registerRequest.website;
            //request.Logo = $scope.Logo;
            //request.CountryId = $scope.CountryId;
            //request.Address1 = $scope.Address1;
            //request.Address2 = $scope.Address2;
            //request.Address3 = $scope.Address3;
            //request.PhoneNumber = $scope.PhoneNumber;
            //request.LinkedInURL = $scope.LinkedInURL;
            //request.TwitterURL = $scope.TwitterURL;

            //var request = 'userName=' + $scope.registerRequest.userName + '&password=' + $scope.registerRequest.password +
            //'&confirmPassword=' + $scope.registerRequest.confirmPassword + '&organizationName=' + $scope.registerRequest.organizationName;

            blockUI.start();
            var promise = XSeedApiFactory.register(request);
            promise.then(
              function (response) {
                  blockUI.stop();
                  $scope.registerResponse = response[0].data;

                  XSeedAlert.swal({
                      title: 'Success!',
                      text: 'Account created successfully!',
                      type: "success",
                      customClass: "xseed-error-alert",
                      //showCancelButton: true,
                      //confirmButtonText: "OK",
                      //closeOnConfirm: true
                  }).then(function () {
                      $scope.registerRequest = {};
                      //$location.path('login');
                      $timeout(function () {
                          $location.path('login');
                      }, 0);
                  }, function (dismiss) {
                      //console.log('dismiss: ' + dismiss);
                      // dismiss can be 'cancel', 'overlay', 'close', 'timer'
                      //if (dismiss == 'cancel'){alert('Close fired');}
                  });
              },
              function (httpError) {
                  blockUI.stop();
                  if (httpError.status == 400) {
                      ExceptionHandler.handlerPopup('Account', httpError.data, 'Warning', 'error');
                  }
                  else {
                      ExceptionHandler.handlerHTTPException('Account', httpError.data.Status, httpError.data.Message);
                  }
              });
        }
    };

    $scope.login = function (loginForm) {
        if (new ValidationService().checkFormValidity(loginForm)) {
            //var request = new API.LoginRequest();      
            //request.userName = $scope.loginRequest.userName;
            //request.password = $scope.loginRequest.password;


            /********************************************************** Encryption Code ***********************************************************/
            /* Code for Encryption UserName Start */
            var key = CryptoJS.enc.Utf8.parse('4263193851707158');
            var iv = CryptoJS.enc.Utf8.parse('4263193851707158');

            $scope.userNameEncrypted = $scope.loginRequest.userName;
            var timestamp = Date.parse(new Date());

            var encryptedlogin = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.userNameEncrypted), key,
            {
                keySize: 128 / 8,
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });

            $scope.userNameEncrypted = encryptedlogin;
            /* Code for Encryption UserName End */

            /* Code for Encryption Password Start */
            $scope.passwordEncrypted = $scope.loginRequest.password;
            var encryptedpassword = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.passwordEncrypted), key,
            {
                keySize: 128 / 8,
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });

            $scope.passwordEncrypted = encryptedpassword;

            /********************************************************** Encryption Code ***********************************************************/

            var request = 'userName=' + $scope.userNameEncrypted + '&password=' + $scope.passwordEncrypted + '&grant_type=password';

            blockUI.start();
            var promise = XSeedApiFactory.login(request);
            promise.then(
              function (response) {
                  blockUI.stop();
                  //console.log(response[0].data);
                  $rootScope.token.key = response[0].data.access_token;
                  localStorageService.set("token", response[0].data.access_token);
                  //getUserDetails(response[0].data.UserName); 
                  $rootScope.userDetails = response[0].data;
                  $rootScope.userDetails.ProfileImage = $rootScope.userDetails.ProfileImage.length == 0 ? "" : configuration.XSEED_ORGUSER_IMAGE_PATH + $rootScope.userDetails.ProfileImage;
                  localStorageService.set("userDetails", response[0].data);
                  setOrRemoveCookieStayLoggedIn($scope.loginRequest.userName);
                  $rootScope.isValidUser = $interval($scope.CheckValidUser, configuration.XSEED_USER_VALID_TIME_IN_SEC);
                  $location.path('dashboard');
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerPopup('Login', httpError.data.error_description, 'Warning', 'error');
              });
        }
    };

    $scope.forgotPassword = function (forgotPasswordForm) {
        if (new ValidationService().checkFormValidity(forgotPasswordForm)) {
            blockUI.start();

            var promise = XSeedApiFactory.forgotPassword($scope.forgotPasswordModel);
            promise.then(
              function (response) {
                  blockUI.stop();

                  XSeedAlert.swal({
                      title: 'Success!',
                      text: 'Please check your email. We\'ve sent you an email that contains your username information.If you do not see the email, check your "Spam" or "Junk" email folder.',
                      type: "success",
                      customClass: "xseed-error-alert",
                      //showCancelButton: true,
                      //confirmButtonText: "OK",
                      //closeOnConfirm: true
                  }).then(function () {
                      $scope.forgotPasswordModel = {};
                      //$location.path('login');
                      $timeout(function () {
                          $location.path('login');
                      }, 0);
                  }, function (dismiss) {
                      //console.log('dismiss: ' + dismiss);
                      // dismiss can be 'cancel', 'overlay', 'close', 'timer'
                      //if (dismiss == 'cancel'){alert('Close fired');}
                  });
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerPopup('Forgot Password', 'Please check username.', 'Reset password unsuccessful!', 'error');
              });
        };
    }

    $scope.resetPassword = function (resetPasswordForm) {
        if (new ValidationService().checkFormValidity(resetPasswordForm)) {
            blockUI.start();

            $scope.resetPasswordModel.UserId = $routeParams.userId;
            $scope.resetPasswordModel.Token = $routeParams.code;

            // Copy of resetPasswordModel
            $scope.tempResetPasswordModel = {};
            $scope.tempResetPasswordModel = angular.copy($scope.resetPasswordModel);

            /********************************************************** Encryption Code ***********************************************************/

            /* Code for Encryption New Password Start */
            var key = CryptoJS.enc.Utf8.parse('4263193851707158');
            var iv = CryptoJS.enc.Utf8.parse('4263193851707158');

            $scope.newPasswordEncrypted = $scope.tempResetPasswordModel.NewPassword;
            var timestamp = Date.parse(new Date());

            var encryptedlogin = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.newPasswordEncrypted), key,
            {
                keySize: 128 / 8,
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });

            $scope.newPasswordEncrypted = encryptedlogin;
            /* Code for Encryption New Password End */

            /* Confirm Password */
            $scope.confirmPasswordEncrypted = $scope.tempResetPasswordModel.ConfirmPassword;
            var encryptedpassword = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.confirmPasswordEncrypted), key,
            {
                keySize: 128 / 8,
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });

            $scope.confirmPasswordEncrypted = encryptedpassword;
            /* Confirm Password */

            /********************************************************** Encryption Code ***********************************************************/

            // Dump data to dummy model
            $scope.tempResetPasswordModel.NewPassword = '' + $scope.newPasswordEncrypted + '';
            $scope.tempResetPasswordModel.ConfirmPassword = '' + $scope.confirmPasswordEncrypted + '';

            var promise = XSeedApiFactory.resetPassword($scope.tempResetPasswordModel);
            promise.then(
              function (response) {
                  blockUI.stop();

                  XSeedAlert.swal({
                      title: 'Success!',
                      text: 'Your password has been reset successfully!',
                      type: "success",
                      customClass: "xseed-error-alert",
                      //showCancelButton: true,
                      //confirmButtonText: "OK",
                      //closeOnConfirm: true
                  }).then(function () {
                      $scope.resetPasswordModel = {};
                      $location.search({});
                      //$location.path('login');
                      $timeout(function () {
                          $location.path('login');
                      }, 0);
                  }, function (dismiss) {
                      //console.log('dismiss: ' + dismiss);
                      // dismiss can be 'cancel', 'overlay', 'close', 'timer'
                      //if (dismiss == 'cancel'){alert('Close fired');}
                  });
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Account', httpError.data.Status, httpError.data.Message);
              });
        };
    }

    function setOrRemoveCookieStayLoggedIn(userName) {
        if ($scope.loginRequest.stayLoggedIn) {
            var expireDate = new Date();
            expireDate.setDate(expireDate.getDate() + 15);
            $cookies.put('userName', userName, { 'expires': expireDate });
        }
        else {
            $cookies.remove('userName');
        }
    };

    function getCookieStayLoggedIn() {
        $scope.loginRequest.userName = $cookies.get('userName');
        if ($scope.loginRequest.userName) {
            $scope.loginRequest.stayLoggedIn = true;
        }
    };

    $scope.candidateLogin = function (loginForm) {
        //if (new ValidationService().checkFormValidity(loginForm)) 
        {

            //var request = 'userName=' + $scope.loginRequest.userName + '&password=' + $scope.loginRequest.password + '&grant_type=password&userType=Candidate';
            var request = 'userName=' + $scope.loginRequest.userName + '&password=' + $scope.loginRequest.password;

            blockUI.start();
            var promise = XSeedApiFactory.candidateLogin(request, $scope.loginRequest.userName, $scope.loginRequest.password);
            promise.then(
              function (response) {
                  blockUI.stop();
                  $rootScope.token.key = response[0].data.access_token;
                  localStorageService.set("token", response[0].data.access_token);

                  $rootScope.userDetails = response[0].data;
                  //$rootScope.userDetails.ProfileImage = $rootScope.userDetails.ProfileImage.length == 0 ? "" : $rootScope.userDetails.ProfileImage.data;
                  localStorageService.set("userDetails", response[0].data);
                  setOrRemoveCookieStayLoggedIn($scope.loginRequest.userName);
                  //$location.path('home');
                  $window.location.href = 'http://localhost:63087/app/#/home';
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Account', httpError.data.Status, httpError.data.Message);
              });
        }
    };
};
