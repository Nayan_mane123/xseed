'use strict';


XSEED_APP.factory('ModalFactory', ModalFactory);
/**
 * @ngInject
 */
function ModalFactory($rootScope, $modal, configuration, $translate) {
    var xseedModals = {};


    xseedModals.getModalInstance = function (template, scope) {
        return $modal({
            animation: 'am-fade-and-scale',
            backdropAnimation: 'slide-bottom',
            templateUrl: template,
            show: false,
            backdrop: false,
            keyboard: false,
            scope: scope
        });
    };

    var modals = [];

    $rootScope.$on('modal.show', function (e, $modal) {
        // if modal is not already in list
        if (modals.indexOf($modal) === -1) {
            modals.push($modal);
        }
    });

    $rootScope.$on('modal.hide', function (e, $modal) {
        var modalIndex = modals.indexOf($modal);
        modals.splice(modalIndex, 1);
    });


    xseedModals.hideAllOpenedModals = function () {
        if (modals.length) {
            angular.forEach(modals, function ($modal) {
                $modal.$promise.then($modal.hide);
            });
            modals = [];
        }
    };

    xseedModals.addressSuggestionsModal = function (scope) {
        return this.getModalInstance('views/modals/addressSuggestions.html', scope);
    };

    xseedModals.timeoutWarningModal = function (scope) {
        return this.getModalInstance('views/templates/timeOutWarningModal.html', scope);
    };

    xseedModals.timeoutModal = function (scope) {
        return this.getModalInstance('views/templates/timedOutModal.html', scope);
    };

    xseedModals.candidateSubmissionModal = function (scope) {
        return this.getModalInstance('views/candidate/submissionModal.html', scope);
    };

    xseedModals.candidateSubmissionEmailModal = function (scope) {
        return this.getModalInstance('views/modals/submissionEmailModal.html', scope);
    };

    xseedModals.requirementMoreModal = function (scope) {
        return this.getModalInstance('views/requirement/requirementMoreModal.html', scope);
    };

    xseedModals.companyContactModal = function (scope) {
        return this.getModalInstance('views/company/companyContactModal.html', scope);
    };

    xseedModals.resumeBlasterSubmissionModal = function (scope) {
        return this.getModalInstance('views/resumeBlaster/resumeBlasterModel.html', scope);
    };

    xseedModals.commonEmailModal = function (scope) {
        return this.getModalInstance('views/modals/commonEmailModal.html', scope);
    };

    xseedModals.commonScheduleModal = function (scope) {
        return this.getModalInstance('views/modals/commonScheduleModal.html', scope);
    };

    xseedModals.commonReminderModal = function (scope) {
        return this.getModalInstance('views/modals/commonReminderModal.html', scope);
    };

    xseedModals.genericMasterModal = function (scope) {
        return this.getModalInstance('views/modals/genericMasterModal.html', scope);
    };

    xseedModals.genericCreateEditMasterModal = function (scope) {
        return this.getModalInstance('views/modals/genericCreateEditMasterModal.html', scope);
    };

    xseedModals.genericMasterMultiValueModal = function (scope) {
        return this.getModalInstance('views/modals/genericMasterMultiValue.html', scope);
    };

    xseedModals.advanceSearchModal = function (scope) {
        return this.getModalInstance('views/modals/advanceSearchModal.html', scope);
    };

    xseedModals.requirementDetailModal = function (scope) {
        return this.getModalInstance('views/modals/requirementDetailModal.html', scope);
    };

    xseedModals.candidateDetailModal = function (scope) {
        return this.getModalInstance('views/modals/candidateDetailModal.html', scope);
    };

    xseedModals.editSubmissionStatusModal = function (scope) {
        return this.getModalInstance('views/submission/editSubmissionStatusModal.html', scope);
    };

    xseedModals.candidatesToSubmitModal = function (scope) {
        return this.getModalInstance('views/modals/candidatesToSubmitModal.html', scope);
    };

    xseedModals.searchCandidateSubmissionModal = function (scope) {
        return this.getModalInstance('views/modals/searchCandidateSubmissionModal.html', scope);
    };

    xseedModals.resumePreviewModal = function (scope) {
        return this.getModalInstance('views/modals/resumePreview.html', scope);
    };

    xseedModals.vendorDetailModal = function (scope) {
        return this.getModalInstance('views/modals/vendorDetailModal.html', scope);
    };

    xseedModals.submitCandidateFromRequirementModal = function (scope) {
        return this.getModalInstance('views/modals/submitCandidateFromRequirementModal.html', scope);
    };

    xseedModals.addCityMasterModal = function (scope) {
        return this.getModalInstance('views/modals/addCityMasterModal.html', scope);
    };

    xseedModals.dashboardRequirementChartDetailsModal = function (scope) {
        return this.getModalInstance('views/modals/dashboardRequirementChartDetailsModal.html', scope);
    };

    xseedModals.dashboardSubmissionChartDetailsModal = function (scope) {
        return this.getModalInstance('views/modals/dashboardSubmissionChartDetailsModal.html', scope);
    };

    xseedModals.dashboardCandidateChartDetailsModal = function (scope) {
        return this.getModalInstance('views/modals/dashboardCandidateChartDetailsModal.html', scope);
    };

    xseedModals.TransferRequirementsModal = function (scope) {
        return this.getModalInstance('views/modals/TransferRequirements.html', scope);
    };
    return xseedModals;

};
