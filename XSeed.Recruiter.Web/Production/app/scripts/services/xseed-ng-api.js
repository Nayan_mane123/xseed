function XSeedRequest() {

    this.AddVehicleRequest = function () {
        var addVehicleRequest = {
            "vin": "",
            "nickName": "",
            "whereAddedFrom": "string"

        }
        return (addVehicleRequest);
    };

    this.ProductRequest = function () {
        var addProductRequest = { "Id": 0, "Name": "", "Category": "", "Price": 0 };
        return (addProductRequest);

    }

    this.RegisterRequest = function () {
        var registerRequest = {
            "firstName": "", "lastName": "", "userName": "", "password": "", "confirmPassword": "", "organizationName": "",
            "website": "", "logo": "", "countryId": "", "address1": "",
            "address2": "", "address3": "", "phoneNumber": "", "linkedInURL": "",
            "twitterURL": ""
        };
        return (registerRequest);
    }

    this.LoginRequest = function () {
        var loginRequest = { "userName": "", "password": "", "grant_type": "password" };
        return (loginRequest);
    }

    this.CompanyRequest = function () {
        var createCompanyRequest = {
            "OrganizationId": "",
            "Name": "", "Description": "", "Website": "", "CareerPageURL": "",
            "LinkedInURL": "", "TwitterURL": "", "FacebookURL": "", "GooglePlusURL": "", "Size": "",
            "Phone": "", "Fax": "", "Address1": "", "Address2": "",
            "Address3": "", "CountryId": "", "StateId": "", "CityId": "", "Zip": "",
            "CompanySourceId": "", "Via": "", "ViaWebsite": "", "IndustryTypes": "", "ProfileImageFile": "",
            "ProfileImage": "",

            "Id": ""
        };
        return (createCompanyRequest);
    }


    this.CompanyContactRequest = function () {
        var createCompanyContactRequest = {
            "CompanyId": "", "TitleId": "", "FirstName": "", "MiddleName": "", "LastName": "", "BirthDate": "", "AnniversaryDate": "", "ProfileImagePath": "", "ReportingTo": "", "Designation": "", "PracticeLine": "", "PrimaryEmail": "", "SecondaryEmail": "", "Mobile": "", "Phone": "", "Fax": "", "Address1": "", "Address2": "", "Address3": "", "CountryId": "", "StateId": "", "CityId": "", "Zip": ""
        };
        return (createCompanyContactRequest);
    }

    this.JobRequest = function () {
        var jobRequest = {
            "JobTitleId": "", "CompanyId": "", "OrganizationUserIdList": [],
            "OrganizationUser": "",
            "ClientJobCode": "", "CompanyContactId": "", "JobDescription": "",
            "Priority": "", "RequisitionId": "", "InterviewType": "",
            "DriveFromDate": "", "DriveToDate": "", "JobPostedDate": "", "JobClosedDate": "",
            "JobExpiryDate": "", "JobTypeId": "", "CountryId": "", "StateId": "", "CityId": "",
            "Location": "", "TotalPositions": "", "JobStatusId": "",
            "DegreeList": "", "SkillList": "", "ExperienceInYear": "", "ExperienceInMonth": ""
        };
        return (jobRequest);
    }

    this.CandidateRequest = function () {
        var candidateRequest = {
            "UserId": "",
            "TitleId": "", "FirstName": "", "MiddleName": "", "LastName": "",
            "BirthDate": "", "ProfileImage": "", "ProfileImageFile": "",
            "GenderId": "", "MaritalStatusId": "", "PrimaryEmail": "", "SecondaryEmail": "",
            "Mobile": "", "Phone": "", "Fax": "", "Address1": "", "Address2": "",
            "Address3": "", "CountryId": "", "StateId": "", "CityId": "",
            "Zip": "", "HavePassport": "", "PassportValidUpto": "", "VisaTypeId": "",
            "VisaValidUpto": "", "SendJobAlert": "", "IsExperienced": "", "DegreeList": "",
            "SkillList": "", "Languages": "",

            "logoImagePath": "", "FullName": "", "SkillIdList": "", "DegreeIdList": "",
            "Gender": "", "MaritalStatus": ""
        };
        return (candidateRequest);
    }


    this.CandidateSubmissionRequest = function () {
        var candidateRequest = {
            "CandidateId": "", "CompanyId": "", "JobId": ""
        };
        return (candidateRequest);
    }

    this.ResumeBlasterRequest = function () {
        var resumeBlasterRequest = {
            "Company": "", "RecruiterName": "",
            "PrimaryEmail": "",
            "Mobile": "", "Phone": "", "Address": "",
            "Website": ""

        };
        return (resumeBlasterRequest);

    }

    this.ResumeBlasterSubmissionRequest = function () {
        var resumeBlasterSubmissionRequest = {
            "resumeBlasterList": "", "Resume": ""

        };
        return (resumeBlasterSubmissionRequest);

    }


    //this.DashboardRequest = function () {
    //    var dashboardRequest = {
    //    };
    //    return (dashboardRequest);

    //}

}





var API = new XSeedRequest();

/**********************************************************************************************/

'use strict';

XSEED_APP.service('XSeedHTTPHeaderBuilder', XSeedHTTPHeaderBuilder);
/**
 * @ngInject
 */
function XSeedHTTPHeaderBuilder(configuration, $rootScope) {

    /*-----------------------------------------------------------------------------------*/
    // Build Token Service Headers
    function getHeadersForAccessTokenCandidate() {
        var headers = {
            'Content-Type': 'application/json'
            , 'Accept': 'application/json'
            , 'UserId': $rootScope.userDetails.UserId
            , 'Token': $rootScope.userDetails.Token
            //, 'Authorization': 'Basic' + ' ' + $rootScope.userDetails.Token
            //, 'Authorization': 'Bearer' + ' ' + $rootScope.token.key
            //, 'Authorization': 'Basic' + ' ' + 'f6d6ccdc-36db-4acf-9307-7da9e4102936'            
            //'CV-AppType': 'WEB'
            //'CV-ApiKey': configuration.API_KEY
        };
        return headers;
    };

    // Build Token Service Headers
    function getHeadersForAccessToken() {
        var headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'UserId': $rootScope.userDetails.UserId
            , 'Authorization': 'Bearer' + ' ' + $rootScope.token.key
            //'CV-AppType': 'WEB'
            //'CV-ApiKey': configuration.API_KEY
        };
        return headers;
    };

    function getHeadersForFileUpload() {
        var headers = {
            'Content-Type': undefined,
            'Accept': 'application/json'

            //'CV-AppType': 'WEB'
            //'CV-ApiKey': configuration.API_KEY
        };
        return headers;
    };
    /*-----------------------------------------------------------------------------------*/
    // Build MOBILE Api Headers
    function getHeaders() {
        var headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
            //'CV-AppType': 'WEB'
            //'CV-ApiKey': configuration.API_KEY,

            //'Authorization': 'Bearer' + ' ' + accessToken
            , 'Authorization': 'Bearer' + ' ' + $rootScope.token.key

        };
        return headers;
    };

    function getPDFHeaders() {
        var headers = {
            'Content-Type': 'application/pdf',
            'Accept': 'application/pdf',
            //'CV-AppType': 'WEB'
            //'CV-ApiKey': configuration.API_KEY,

            //'Authorization': 'Bearer' + ' ' + accessToken
            'Authorization': 'Bearer' + ' ' + $rootScope.token.key
        };
        return headers;
    };

    var getDeleteHeaders = function () {
        var headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            //'CV-AppType': 'WEB',
            'X-Http-Method-Override': 'DELETE'
            //'CV-ApiKey': services.configuration.API_KEY,

            //,'Authorization': 'Bearer' + ' ' + accessToken
            , 'Authorization': 'Bearer' + ' ' + $rootScope.token.key
        };
        return headers;
    };

    /*-----------------------------------------------------------------------------------*/

    return ({
        getHeadersForAccessToken: getHeadersForAccessToken,
        getHeaders: getHeaders,
        getPDFHeaders: getPDFHeaders,
        getDeleteHeaders: getDeleteHeaders,
        getHeadersForFileUpload: getHeadersForFileUpload,
    });
}

XSeedHTTPHeaderBuilder.$inject = ["configuration", "$rootScope"];


/**********************************************************************************************/

'use strict';

XSEED_APP.service('XSeedApi', XSeedApi);
/**
 * @ngInject
 */
function XSeedApi($http, configuration) {

    function GET(url, headers) {
        return $http({
            method: 'GET',
            url: url,
            dataType: "json",
            headers: headers,
            transformResponse: $http.defaults.transformResponse.concat([
                function (data, headersGetter) {

                    return data;
                }
            ])
        })
    };

    function GetPDF(url, headers) {
        return $http({
            method: 'GET',
            url: url,
            headers: headers,
            responseType: 'arraybuffer'
        })
    };

    function POST(url, headers, request) {
        return $http({
            method: 'POST',
            url: url,
            dataType: "json",
            headers: headers,
            transformResponse: $http.defaults.transformResponse.concat([
                function (data, headersGetter) {
                    return data;
                }
            ]),
            data: request
        })
    };

    function PUT(url, headers, request) {
        return $http({
            method: 'PUT',
            url: url,
            dataType: "json",
            headers: headers,
            transformResponse: $http.defaults.transformResponse.concat([
                function (data, headersGetter) {
                    return data;
                }
            ]),
            data: request
        })
    };

    function DELETE(url, headers) {
        return $http({
            method: 'DELETE',
            url: url,
            dataType: "json",
            headers: headers,
            transformResponse: $http.defaults.transformResponse.concat([
                function (data, headersGetter) {
                    return data;
                }
            ])
        })
    };

    return ({
        GET: GET,
        GetPDF: GetPDF,
        POST: POST,
        PUT: PUT,
        DELETE: DELETE
    });
}

XSeedApi.$inject = ["$http", "configuration"];

/**********************************************************************************************/

'use strict';

XSEED_APP.service('XSeedExceptionHandler', XSeedExceptionHandler);
/**
 * @ngInject
 */
function XSeedExceptionHandler($rootScope, $http, $translate, $timeout, $log) {

    var handler = {};
    handler.ExceptionMessage = {};
    handler.handlerHTTPException = function (apiName, status, responseMessage) {

        this.setErrorMessage(apiName, status, responseMessage);
        this.broadcastError();
    };

    handler.handleValidationException = function (type, val) {

        handler.ExceptionMessage.location = '/badRequest';
        this.broadcastError();
    };

    handler.handleNotSupportException = function (errorMessage, status) {

        handler.ExceptionMessage.location = '/notSupported';
        this.broadcastError();

    };

    handler.setErrorMessage = function (apiName, status, responseMessage) {
        handler.ExceptionMessage.message = "There was an error processing your request ( Error code : " +
            responseMessage.data.code + ", Error :" + responseMessage.data.message + " )";
    };

    handler.broadcastError = function () {
        $timeout(function () {
            $rootScope.$broadcast('handleError');
        }, 1000);
    };
    return handler;
}

XSeedExceptionHandler.$inject = ["$rootScope", "$http", "$translate", "$timeout", "$log"];


/**********************************************************************************************/

'use strict';

XSEED_APP.factory('XSeedApiFactory', XSeedApiFactory);

function XSeedApiFactory($q, _, $log, XSeedApi, XSeedHTTPHeaderBuilder, $rootScope, $http) {


    var services = {};

    services.configuration = {};

    services.setConfiguration = function (config) {
        services.configuration = config;
    }

    services.candidateLogin = function (request, username, password) {

        //var url = services.configuration.XSEED_LOGIN_API_URL;
        var url = services.configuration.XSEED_API_URL + 'Account/CandidateLogin?username=' + username + '&password=' + password;

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessTokenCandidate(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/
    services.getTokenInformation = function (requestParam) {
        var url = services.configuration.GET_TOKEN_INFO_API_URL + requestParam;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.updateVehicle = function (token, accountId, vin, request) {
        var url = services.configuration.ACCOUNT_API_URL + accountId + '/vehicles/' + vin;

        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeaders(), request)
            ]
        );
        return promise;

    };
    /*-----------------------------------------------------------------------------------*/
    services.deletePaymentMethodToAccount = function (token, accountId, paymentMethodId) {
        var url = services.configuration.ACCOUNT_API_URL + accountId + '/paymentMethods/' + paymentMethodId;

        var promise = $q.all(
            [
              XSeedApi.DELETE(url, XSeedHTTPHeaderBuilder.getHeaders())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/





    //Added  
    services.getMetaData = function () {
        var url = services.configuration.GET_METADATA_API_URL;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    //Add City Master  
    services.addCityMaster = function (city, stateId) {
        var url = services.configuration.XSEED_API_URL + 'LookUp/CreateCity?city=' + city + '&stateId=' + stateId;

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    //Register
    services.register = function (request) {
        var url = services.configuration.XSEED_API_URL + 'Account/Register';

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    //Login
    services.login = function (request) {

        //var url = services.configuration.XSEED_API_URL + 'Login';
        var url = services.configuration.XSEED_LOGIN_API_URL;

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getUserDetails = function (userName) {

        var url = services.configuration.XSEED_API_URL + "OrganizationUser/Get?userName=" + userName;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    //Company module
    services.getCompanyList = function (id) {

        var url = services.configuration.XSEED_API_URL + "Company/Get?organizationId=" + id;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.createCompany = function (request) {

        var url = services.configuration.XSEED_API_URL + "Company/Post";
        var promise = $q.all(
                    [
                      XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request),
                    ]
                );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/


    /*-----------------------------------------------------------------------------------*/

    services.getCompanyDetail = function (organizationId, companyId) {

        var url = services.configuration.XSEED_API_URL + "Company/Get/" + companyId +
            "?organizationId=" + organizationId;
        var promise = $q.all(
                   [
                     XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
                   ]
               );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getContactDetail = function (contactId, companyId) {

        var url = services.configuration.XSEED_API_URL + "CompanyContact/Get/" + contactId + "?companyId=" + companyId;
        var promise = $q.all(
                   [
                     XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
                   ]
               );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.editCompany = function (request) {

        var url = services.configuration.XSEED_API_URL + "Company/Put";
        var promise = $q.all(
                    [
                      XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
                    ]
                );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/


    services.getCompanyContacts = function (companyId) {

        var url = services.configuration.XSEED_API_URL + "CompanyContact/Get?companyId=" + companyId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.createContact = function (request) {
        var url = services.configuration.XSEED_API_URL + "CompanyContact/Post";
        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    services.updateContact = function (request) {
        var url = services.configuration.XSEED_API_URL + "CompanyContact/Put";
        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    services.getCompanyContact = function (companyId, contactId) {

        var url = services.configuration.XSEED_API_URL + "CompanyContact/Get/" + contactId + "?companyId=" + companyId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.doCompanySearch = function (searchText) {

        var url = services.configuration.XSEED_API_URL + "Company/SearchCompany?searchText=" + searchText;

        return $http({
            method: 'GET',
            url: url,
            dataType: "json",
            headers: XSeedHTTPHeaderBuilder.getHeadersForAccessToken(),
            transformResponse: $http.defaults.transformResponse.concat([
                function (data, headersGetter) {
                    var result = angular.fromJson(data);
                    if (!angular.isArray(result)) result = [result];
                    return result.map(function (company) {
                        return {
                            Name: company.name
                            , Website: company.website
                            , Size: company.employeeCount
                            , Description: company.description
                            , logoLink: company.logoLink
                            , Address1: company.streetAddress
                            , Address2: company.locality + ' ' + company.region
                            , Zip: company.postalCode
                            , country: company.country
                            , logoImagePath: services.configuration.XSEED_COMPANYDATA_IMAGE_PATH + company.LogoImagePath
                            , ProfileImage: company.LogoImagePath
                            , ProfileImageFile: buildImageItem(services.configuration.XSEED_COMPANYDATA_IMAGE_PATH + company.LogoImagePath, company.LogoImagePath)
                            , locality: company.locality
                            , manipulationMode: 'Create'
                        };
                    });
                }
            ])
        })
    };

    function buildImageItem(imgPath, name) {
        var item = {};
        var img = document.createElement("img");
        img.src = imgPath;
        img.crossOrigin = 'anonymous';

        img.onload = function () {
            item.lastModified = '';
            item.lastModifiedDate = Date();
            item.name = name;
            item.size = '';
            if (item.name.toLowerCase().endsWith("jpg") || item.name.toLowerCase().endsWith("jpeg"))
                item.type = 'image/jpg';
            else if (item.name.toLowerCase().endsWith("png"))
                item.type = 'image/png';
            else if (item.name.toLowerCase().endsWith("bmp"))
                item.type = 'image/bmp';
            item.data = 'data:image/jpeg;base64,' + getBase64Image(img);
        }

        return item;
    }

    function getBase64Image(img) {
        var canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0);
        var dataURL = canvas.toDataURL("image/png");
        return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
    }


    /*-----------------------------------------------------------------------------------*/

    services.getReportingAuthorities = function (organizationId) {

        var url = services.configuration.XSEED_API_URL + "LookUp/GetReportingAuthorities?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getReportingAuthoritiesByRole = function (organizationId, roleId) {

        var url = services.configuration.XSEED_API_URL + "LookUp/GetReportingAuthorities?organizationId=" + organizationId + "&roleId=" + roleId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getOrganizationUsers = function (organizationId) {

        var getOrganizationUsersurl = services.configuration.XSEED_API_URL + "OrganizationUser/Get?organizationId=" + organizationId;
        var getUserReportingToAuthurl = services.configuration.XSEED_API_URL + "LookUp/GetReportingAuthorities?organizationId=" + organizationId;
        var getTitleurl = services.configuration.XSEED_API_URL + "LookUp/GetTitles";
        var getUserRolesurl = services.configuration.XSEED_API_URL + "LookUp/GetUserRoles?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(getOrganizationUsersurl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getUserReportingToAuthurl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getTitleurl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getUserRolesurl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getOrganizationUserDetails = function (email, organizationId, userId, roleId) {

        var getUserDetailUrl = services.configuration.XSEED_API_URL + "OrganizationUser/Get?organizationId=" + organizationId + "&Id=" + userId;
        var getUserReportingToAuthurl = services.configuration.XSEED_API_URL + "LookUp/GetReportingAuthorities?organizationId=" + organizationId + "&roleId=" + roleId;

        var promise = $q.all(
            [
              XSeedApi.GET(getUserDetailUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getUserReportingToAuthurl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.createNewOrganizationUser = function (request) {

        var url = services.configuration.XSEED_API_URL + "OrganizationUser/Post";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.forgotPassword = function (request) {

        var url = services.configuration.XSEED_API_URL + "Account/ForgotPassword";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.resetPassword = function (request) {

        var url = services.configuration.XSEED_API_URL + "Account/SetPassword";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.changePassword = function (request) {

        var url = services.configuration.XSEED_API_URL + "Account/ChangePassword";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeaders(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.saveEditOrgUserDetails = function (request) {

        var url = services.configuration.XSEED_API_URL + "OrganizationUser/Put";
        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    /*Enable disable user*/
    services.enableDisableUser = function (userId, OrganizationId, isActive) {

        var url = services.configuration.XSEED_API_URL + "OrganizationUser/EnableDisableUser?userId=" + userId + "&oraganizationId=" + OrganizationId + "&isActive=" + isActive;
        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*Check if user is authorized */
    services.CheckValidUser = function (userId) {

        var url = services.configuration.XSEED_API_URL + "OrganizationUser/CheckValidUser?userId=" + userId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.getLookUpUsersToTransfer = function (organizationId) {
        var getOrganizationUserUrl = services.configuration.XSEED_API_URL + "LookUp/GetOrganizationUsers?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(getOrganizationUserUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };



    services.TransferRequirements = function (userId, TransferedUserId) {

        var url = services.configuration.XSEED_API_URL + "OrganizationUser/TransferRequirements?userId=" + userId + "&TransferedUserId=" + TransferedUserId;
        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    /*-----------------------------------------------------------------------------------*/

    services.saveUserEmailConfiguration = function (request) {

        var url = services.configuration.XSEED_API_URL + "User/SaveUserEmailConfiguration";
        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.editUserEmailConfiguration = function (request) {

        var url = services.configuration.XSEED_API_URL + "User/UpdateUserEmailConfiguration";
        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    services.getUserEmailConfiguration = function (request) {

        var url = services.configuration.XSEED_API_URL + "User/GetUserEmailConfiguration/" + request;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    //Vendor module
    services.getVendorList = function (organizationId) {
        var url = services.configuration.XSEED_API_URL + "Vendor/Get?organizationId=" + organizationId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.createVendor = function (request) {

        var url = services.configuration.XSEED_API_URL + "Vendor/Post";
        var promise = $q.all(
                    [
                      XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request),
                    ]
                );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.updateVendor = function (request) {

        var url = services.configuration.XSEED_API_URL + "Vendor/Put";
        var promise = $q.all(
                    [
                      XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request),
                    ]
                );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getVendorParsedEmailList = function (organizationId, pageSize, pageNumber, sortBy, sortOrder) {
        pageSize = pageSize === undefined ? 15 : pageSize;
        pageNumber = pageNumber === undefined ? 1 : pageNumber;
        sortBy = sortBy === undefined ? "Date" : sortBy;
        sortOrder = sortOrder === undefined ? "desc" : sortOrder;

        var url = services.configuration.XSEED_API_URL + "Vendor/GetVendorParsedEmail?organizationId=" + organizationId + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&sortBy=" + sortBy + "&sortOrder=" + sortOrder;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.updateVendorEmailDetails = function (request) {

        var url = services.configuration.XSEED_API_URL + "Vendor/UpdateVendorParsedEmail";
        var promise = $q.all(
                    [
                      XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request),
                    ]
                );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getUnreadMailCount = function (organizationId) {
        var url = services.configuration.XSEED_API_URL + "Vendor/GetUnreadMailCount?organizationId=" + organizationId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    //Resume Email module
    services.getResumeEmailList = function (organizationId, pageSize, pageNumber, sortBy, sortOrder, Id) {
        pageSize = pageSize === undefined ? 15 : pageSize;
        pageNumber = pageNumber === undefined ? 1 : pageNumber;
        sortBy = sortBy === undefined ? "Date" : sortBy;
        sortOrder = sortOrder === undefined ? "desc" : sortOrder;

        if (Id) {
            var url = services.configuration.XSEED_API_URL + "ResumeEmail/Get?organizationId=" + organizationId + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&sortBy=" + sortBy + "&sortOrder=" + sortOrder + "&Id=" + Id;
        }
        else {
            var url = services.configuration.XSEED_API_URL + "ResumeEmail/Get?organizationId=" + organizationId + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&sortBy=" + sortBy + "&sortOrder=" + sortOrder;
        }

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.updateResumeEmailDetails = function (Id) {

        var url = services.configuration.XSEED_API_URL + "ResumeEmail/Put/" + Id;
        var promise = $q.all(
                    [
                      XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
                    ]
                );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getUnreadResumeMailCount = function (organizationId) {
        var url = services.configuration.XSEED_API_URL + "ResumeEmail/GetUnreadMailCount?organizationId=" + organizationId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    // Download Agreement PDF
    services.getAgreementPDF = function (token, accountId, vin) {
        var url = services.configuration.ACCOUNT_API_URL + accountId + '/vehicles/' + vin + '/subscriptions';
        var promise = $q.all(
            [
              XSeedApi.GetPDF(url, XSeedHTTPHeaderBuilder.getPDFHeaders(token))
            ]
        );
        return promise;
    };



    /*---------------------------------------Orgnaization API Service--------------------------------------------*/

    services.getOrganizationDetails = function (organizationId) {

        var url = services.configuration.XSEED_API_URL + "Organization/Get?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.updateOrganization = function (frmUpdateOrganization) {
        var url = services.configuration.XSEED_API_URL + "Organization/Put";
        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), frmUpdateOrganization)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    /*---------------------------------Job/Requirements Start--------------------------------------------------*/

    services.getRequirementListByStatus = function (id, userId, requirementStatus, pageSize, pageNumber, sortBy, sortOrder) {
        pageSize = pageSize === undefined ? 5 : pageSize;
        pageNumber = pageNumber === undefined ? 1 : pageNumber;
        sortBy = sortBy === undefined ? "CreatedOn" : sortBy;
        sortOrder = sortOrder === undefined ? "desc" : sortOrder;

        if (requirementStatus != null) {
            var url = services.configuration.XSEED_API_URL + "Job/GetJobByUser?organizationId=" + id + "&userId=" + userId + "&status=" + requirementStatus + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&sortBy=" + sortBy + "&sortOrder=" + sortOrder;
        }
        else {
            var url = services.configuration.XSEED_API_URL + "Job/GetJobByUser?organizationId=" + id + "&userId=" + userId + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&sortBy=" + sortBy + "&sortOrder=" + sortOrder;
        }

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.getRequirementList = function (id, pageSize, pageNumber, sortBy, sortOrder, isExport) {

        pageSize = pageSize === undefined ? 5 : pageSize;
        pageNumber = pageNumber === undefined ? 1 : pageNumber;
        sortBy = sortBy === undefined ? "CreatedOn" : sortBy;
        sortOrder = sortOrder === undefined ? "desc" : sortOrder;
        isExport = isExport === undefined ? false : isExport;

        var url = services.configuration.XSEED_API_URL + "Job/Get?organizationId=" + id + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&sortBy=" + sortBy + "&sortOrder=" + sortOrder + "&isExport=" + isExport;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    //Duplicate LookUp Name check Calls

    services.IsDuplicateLookupName = function (lookupName, lookupType, organizationId, editedValue, existingValue) {
        var Url = services.configuration.XSEED_API_URL + "LookUp/IsDuplicateLookupName?lookupName=" + lookupName + "&lookupType=" + lookupType + "&organizationId=" + organizationId + "&editedValue=" + editedValue + "&existingValue=" + existingValue;

        var promise = $q.all(
            [
              XSeedApi.GET(Url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    //Requisition ID Calls

    services.getRequisitionID = function (organizationId) {
        var Url = services.configuration.XSEED_API_URL + "Job/GetRequisitionId?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(Url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.IsDuplicateRequisitionId = function (requisitionId, organizationId) {

        var Url = services.configuration.XSEED_API_URL + "Job/IsDuplicateRequisitionId?organizationId=" + organizationId + "&requisitionId=" + requisitionId;

        var promise = $q.all(
            [
              XSeedApi.GET(Url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/



    //Candidate ID Calls

    services.getCandidateId = function () {
        var Url = services.configuration.XSEED_API_URL + "CandidateUser/GetCandidateId";

        var promise = $q.all(
            [
              XSeedApi.GET(Url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/


    services.IsDuplicateCandidateId = function (candidateId) {

        var Url = services.configuration.XSEED_API_URL + "CandidateUser/IsDuplicateCandidateId?candidateId=" + candidateId;

        var promise = $q.all(
            [
              XSeedApi.GET(Url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    // Candidate source lookup Calls

    services.getCandidateSource = function (organizationId) {
        var Url = services.configuration.XSEED_API_URL + "LookUp/GetCandidateSources?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(Url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
            ]
        );
        return promise;
    };

    services.createCandidateSource = function (request) {
        var url = services.configuration.XSEED_API_URL + "LookUp/CreateCandidateSource";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };


    services.updateCandidateSource = function (request) {
        var url = services.configuration.XSEED_API_URL + "LookUp/UpdateCandidateSource";

        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };


    services.deleteCandidateSourceLookup = function (genericLookupId) {
        var url = services.configuration.XSEED_API_URL + 'LookUp/DeleteCandidateSource?genericLookupId=' + genericLookupId;
        var promise = $q.all(
            [
              XSeedApi.DELETE(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.candidateSecondaryLookupList = function (organizationId) {
        var getCompanyUrl = services.configuration.XSEED_API_URL + "LookUp/GetCompanies?organizationId=" + organizationId;
        var getOrganizationUserUrl = services.configuration.XSEED_API_URL + "LookUp/GetOrganizationUsers?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(getOrganizationUserUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getCompanyUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/



    // Candidate Categories lookup Calls

    services.getCandidateCategories = function (organizationId) {
        var Url = services.configuration.XSEED_API_URL + "LookUp/GetCandidateCategories?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(Url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
            ]
        );
        return promise;
    };

    services.createCandidateCategory = function (request) {
        var url = services.configuration.XSEED_API_URL + "LookUp/CreateCandidateCategory";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };


    services.updateCandidateCategory = function (request) {
        var url = services.configuration.XSEED_API_URL + "LookUp/UpdateCandidateCategory";

        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };


    services.deleteCandidateCategoryLookup = function (genericLookupId) {
        var url = services.configuration.XSEED_API_URL + 'LookUp/DeleteCandidateCategory?genericLookupId=' + genericLookupId;
        var promise = $q.all(
            [
              XSeedApi.DELETE(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    // Candidate Groups lookup Calls

    services.getCandidateGroups = function (organizationId) {
        var Url = services.configuration.XSEED_API_URL + "LookUp/GetCandidateGroups?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(Url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
            ]
        );
        return promise;
    };

    services.createCandidateGroup = function (request) {
        var url = services.configuration.XSEED_API_URL + "LookUp/CreateCandidateGroup";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };


    services.updateCandidateGroup = function (request) {
        var url = services.configuration.XSEED_API_URL + "LookUp/UpdateCandidateGroup";

        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };


    services.deleteCandidateGroupLookup = function (genericLookupId) {
        var url = services.configuration.XSEED_API_URL + 'LookUp/DeleteCandidateGroup?genericLookupId=' + genericLookupId;
        var promise = $q.all(
            [
              XSeedApi.DELETE(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/
    //Job Title Lookup Calls

    services.getJobTitleLookup = function (organizationId) {
        var getJobTitleUrl = services.configuration.XSEED_API_URL + "LookUp/GetJobTitles?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(getJobTitleUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
            ]
        );
        return promise;
    };

    services.updateJobTitleLookup = function (request) {
        var url = services.configuration.XSEED_API_URL + "LookUp/UpdateJobTitles";

        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    services.createJobTitleLookup = function (request) {
        var url = services.configuration.XSEED_API_URL + "LookUp/CreateJobTitles";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    services.deleteJobTitleLookup = function (genericLookupId) {
        var url = services.configuration.XSEED_API_URL + 'LookUp/DeleteJobTitles?genericLookupId=' + genericLookupId;
        var promise = $q.all(
            [
              XSeedApi.DELETE(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    //Job Type Lookup Calls

    services.getJobTypeLookup = function (organizationId) {
        var getJobTypeUrl = services.configuration.XSEED_API_URL + "LookUp/GetJobTypes?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(getJobTypeUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
            ]
        );
        return promise;
    };

    services.createJobTypeLookup = function (request) {
        var url = services.configuration.XSEED_API_URL + "LookUp/CreateJobTypes";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    services.updateJobTypeLookup = function (request) {
        var url = services.configuration.XSEED_API_URL + "LookUp/UpdateJobTypes";

        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    services.deleteJobTypeLookup = function (genericLookupId) {
        var url = services.configuration.XSEED_API_URL + 'LookUp/DeleteJobTypes?genericLookupId=' + genericLookupId;
        var promise = $q.all(
            [
              XSeedApi.DELETE(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    //Job Degrees Lookup Calls

    services.getJobDegreesLookup = function (organizationId) {
        var getDegreeUrl = services.configuration.XSEED_API_URL + "LookUp/GetDegrees?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(getDegreeUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
            ]
        );
        return promise;
    };

    services.createJobDegreesLookup = function (request) {
        var url = services.configuration.XSEED_API_URL + "LookUp/CreateJobDegrees";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    services.updateJobDegreesLookup = function (request) {
        var url = services.configuration.XSEED_API_URL + "LookUp/UpdateJobDegrees";

        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    services.deleteJobDegreesLookup = function (genericLookupId) {
        var url = services.configuration.XSEED_API_URL + 'LookUp/DeleteJobDegrees?genericLookupId=' + genericLookupId;
        var promise = $q.all(
            [
              XSeedApi.DELETE(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    // Company SourceLookup Calls

    services.createCompanySource = function (companySources) {

        var companySourceUrl = services.configuration.XSEED_API_URL + "Company/PostCompanySource";

        var promise = $q.all(
            [
              XSeedApi.POST(companySourceUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), companySources)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.updateCompanySource = function (companySources) {

        var companySourceUrl = services.configuration.XSEED_API_URL + "Company/PutCompanySource";

        console.log(companySourceUrl);

        var promise = $q.all(
            [
              XSeedApi.PUT(companySourceUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), companySources)
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.deleteCompanySourceLookup = function (genericLookupId) {
        var url = services.configuration.XSEED_API_URL + 'Company/DeleteCompanySource?genericLookupId=' + genericLookupId;
        var promise = $q.all(
            [
              XSeedApi.DELETE(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*------------------------------------------------------------------------------------------*/

    /*------------------------------ BusinessUnit LookUp --------------------------------------*/
    services.getBusinessUnit = function (organizationId) {

        var businessUnitUrl = services.configuration.XSEED_API_URL + "LookUp/GetBusinessUnits?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(businessUnitUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/
    services.createNewBusinessUnit = function (businessUnit) {

        var businessUnitUrl = services.configuration.XSEED_API_URL + "Job/AddBusinessUnit";

        var promise = $q.all(
            [
              XSeedApi.POST(businessUnitUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), businessUnit)
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.updateBusinessUnit = function (businessUnit) {

        var Url = services.configuration.XSEED_API_URL + "Job/PutBusinessUnit";

        var promise = $q.all(
            [
              XSeedApi.PUT(Url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), businessUnit)
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.deleteBusinessUnitLookup = function (genericLookupId) {
        var url = services.configuration.XSEED_API_URL + 'Job/DeleteBusinessUnit?genericLookupId=' + genericLookupId;
        var promise = $q.all(
            [
              XSeedApi.DELETE(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*------------------------------ BusinessUnit LookUp --------------------------------------*/

    //Job Lookup Call
    services.jobPagePrimaryLookupCall = function (organizationId) {
        var getJobTitleUrl = services.configuration.XSEED_API_URL + "LookUp/GetJobTitles?organizationId=" + organizationId;
        var getJobTypeUrl = services.configuration.XSEED_API_URL + "LookUp/GetJobTypes?organizationId=" + organizationId;
        var getJobStatusUrl = services.configuration.XSEED_API_URL + "LookUp/GetJobStatus";
        var getDegreeUrl = services.configuration.XSEED_API_URL + "LookUp/GetDegrees?organizationId=" + organizationId;
        var getVisaTypeUrl = services.configuration.XSEED_API_URL + "LookUp/GetVisaTypes";

        var promise = $q.all(
            [
              XSeedApi.GET(getJobTitleUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getJobTypeUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getJobStatusUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getDegreeUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getVisaTypeUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.jobPageSecondaryLookupCall = function (organizationId) {
        var getCompanyUrl = services.configuration.XSEED_API_URL + "LookUp/GetCompanies?organizationId=" + organizationId;
        var getOrganizationUserUrl = services.configuration.XSEED_API_URL + "LookUp/GetOrganizationUsers?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(getCompanyUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getOrganizationUserUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.createJob = function (request) {
        var url = services.configuration.XSEED_API_URL + "Job/Post";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.editJob = function (request) {
        var url = services.configuration.XSEED_API_URL + "Job/Put";

        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getJobDetail = function (requirementId, organizationId) {
        var url = services.configuration.XSEED_API_URL + "Job/Get/" + requirementId + "?organizationId=" + organizationId;
        var getAssociatedCandidateListUrl = services.configuration.XSEED_API_URL + "Job/GetAssociatedCandidateList?jobId=" + requirementId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getAssociatedCandidateListUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.getMonsterCandidates = function (skillSet) {
        //var url = services.configuration.XSEED_API_URL + "Job/Get/" + requirementId + "?organizationId=" + organizationId;
        //var url = services.configuration.XSEED_API_URL + "Job/GetMonsterCandidates/" + skillSet;
        var url = services.configuration.XSEED_API_URL + "Job/GetMonsterCandidates?skillSet=" + skillSet;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.downloadMonsterCandidate = function (resumeId, resumeModifiedDate, firstName, lastName) {
        var url = services.configuration.XSEED_API_URL + "Job/DownloadMonsterCandidate?resumeId=" + resumeId + "&resumeModifiedDate=" + resumeModifiedDate + "&firstName=" + firstName + "&lastName=" + lastName;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.getCandidateByJob = function (jobId) {
        var url = services.configuration.XSEED_API_URL + "CandidateUser/GetCandidateByJob?jobId=" + jobId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.getAssociatedCandidateList = function (requirementId) {
        var getAssociatedCandidateListUrl = services.configuration.XSEED_API_URL + "Job/GetAssociatedCandidateList?jobId=" + requirementId;

        var promise = $q.all(
            [
              XSeedApi.GET(getAssociatedCandidateListUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.getRequirementAdvanceSearchResult = function (request, pageSize, pageNumber, sortBy, sortOrder, daysBefore, isExport) {

        pageSize = pageSize === undefined ? 5 : pageSize;
        pageNumber = pageNumber === undefined ? 1 : pageNumber;
        sortBy = sortBy === undefined ? "CreatedOn" : sortBy;
        sortOrder = sortOrder === undefined ? "desc" : sortOrder;
        isExport = isExport === undefined ? false : isExport;
        daysBefore = daysBefore === undefined ? -7 : daysBefore;

        var url = services.configuration.XSEED_API_URL + "AdvancedSearch/AdvanceSearchJobs?pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&sortBy=" + sortBy + "&sortOrder=" + sortOrder + "&daysBefore=" + daysBefore + "&isExport=" + isExport;

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.getCompanyContactsLookup = function (companyId) {
        var url = services.configuration.XSEED_API_URL + "LookUp/GetCompanyContacts?companyId=" + companyId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.getRequirementQueueDataList = function (organizationUserId, pageSize, pageNumber) {

        pageSize = pageSize === undefined ? 10 : pageSize;
        pageNumber = pageNumber === undefined ? 1 : pageNumber;

        var parsedUrl = services.configuration.XSEED_API_URL + "Parser/GetParsedJobList?OrganizationUserId=" + organizationUserId + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber;
        var promise = $q.all(
            [
              XSeedApi.GET(parsedUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getRequirementQueueListCount = function (organizationUserId) {

        var parsedJobCountUrl = services.configuration.XSEED_API_URL + "Parser/GetParsedJobListCount?organizationUserId=" + organizationUserId;
        var promise = $q.all(
            [
              XSeedApi.GET(parsedJobCountUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getAcceptedRequirementList = function () {
        var url = services.configuration.XSEED_API_URL + "Parser/GetAcceptedParsedJob";
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/
    services.acceptRequirement = function (id, jobId, organizationUserId, isReviewed) {
        var url = services.configuration.XSEED_API_URL + "Parser/AcceptParsedJob?Id=" + id + "&JobId=" + jobId + "&OrganizationUserId=" + organizationUserId + "&isReviewed=" + isReviewed;
        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/
    services.rejectRequirement = function (id) {
        var url = services.configuration.XSEED_API_URL + "Parser/RejectParsedJob?Id=" + id;
        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getRequirementReview = function (id, organizationUserId) {
        var url = services.configuration.XSEED_API_URL + "Parser/ReviewParsedJob?Id=" + id + "&OrganizationUserId=" + organizationUserId;
        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/
    services.updateCandidateStatus = function (request, candidateId) {
        var url = services.configuration.XSEED_API_URL + "CandidateUser/UpdateApplicationStatus/" + candidateId;
        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    //Search LookUp Calls

    services.getSalaryType = function () {

        var url = services.configuration.XSEED_API_URL + "LookUp/GetSalaryType";

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.createQNote = function (note, requirementId) {
        var url = services.configuration.XSEED_API_URL + "Job/SaveQuickNote?note=" + note + "&jobId=" + requirementId;

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.getQNoteList = function (requirementId) {
        var url = services.configuration.XSEED_API_URL + "Job/GetQuickNotes?jobId=" + requirementId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.getActivityList = function (requirementId) {
        var url = services.configuration.XSEED_API_URL + "Job/GetJobActivityLog?jobId=" + requirementId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*---------------------------------Job/Requirements End--------------------------------------------------*/


    /*---------------------------------Candidate Start--------------------------------------------------*/
    //Candidate Lookup Call
    services.candidatePageLookupCall = function (organizationId) {
        var getTitleUrl = services.configuration.XSEED_API_URL + "LookUp/GetTitles";
        var getMaritalStatusUrl = services.configuration.XSEED_API_URL + "LookUp/GetMaritalStatus";
        var getVisaTypeUrl = services.configuration.XSEED_API_URL + "LookUp/GetVisaTypes";
        var getDegreeUrl = services.configuration.XSEED_API_URL + "LookUp/GetDegrees?organizationId=" + organizationId;
        var getGenderUrl = services.configuration.XSEED_API_URL + "LookUp/GetGenders";
        var getLanguageUrl = services.configuration.XSEED_API_URL + "LookUp/GetLanguages";
        var getJobTypeUrl = services.configuration.XSEED_API_URL + "LookUp/GetJobTypes?organizationId=" + organizationId;
        var getCandidateStatusUrl = services.configuration.XSEED_API_URL + "LookUp/GetCandidateStatus";

        var promise = $q.all(
            [
              XSeedApi.GET(getMaritalStatusUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getVisaTypeUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getDegreeUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getTitleUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getGenderUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getLanguageUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getJobTypeUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getCandidateStatusUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getCandidateList = function (pageSize, pageNumber, sortBy, sortOrder, isExport) {

        pageSize = pageSize === undefined ? 5 : pageSize;
        pageNumber = pageNumber === undefined ? 1 : pageNumber;
        sortBy = sortBy === undefined ? "CreatedOn" : sortBy;
        sortOrder = sortOrder === undefined ? "desc" : sortOrder;
        isExport = isExport === undefined ? false : isExport;

        var url = services.configuration.XSEED_API_URL + "CandidateUser/Get?pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&sortBy=" + sortBy + "&sortOrder=" + sortOrder + "&isExport=" + isExport;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.getListOfAllCandidates = function () {

        var url = services.configuration.XSEED_API_URL + "CandidateUser/Get";

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.createCandidate = function (request) {
        var url = services.configuration.XSEED_API_URL + "CandidateUser/Post";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.editCandidate = function (request) {
        var url = services.configuration.XSEED_API_URL + "CandidateUser/Put";

        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getCandidateDetail = function (id) {
        var url = services.configuration.XSEED_API_URL + "CandidateUser/Get/" + id;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.monsterCandidateDetailView = function (resumeId, candidateId) {
        var url = services.configuration.XSEED_API_URL + "CandidateUser/ViewMonsterCandidateDetail?resumeId=" + resumeId + "&candidateId=" + candidateId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.diceCandidateDetailView = function (diceId, candidateId) {
        var url = services.configuration.XSEED_API_URL + "CandidateUser/ViewDiceCandidateDetail?diceId=" + diceId + "&candidateId=" + candidateId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.submitCandidate = function (request) {
        var url = services.configuration.XSEED_API_URL + "Submission/Post";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.submitCandidateByEmail = function (request) {
        var url = services.configuration.XSEED_API_URL + "Submission/SendSubmissionMail";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getSubmissionContactEmail = function (companyId) {

        var url = services.configuration.XSEED_API_URL + "LookUp/GetSubmissionContactEmail?companyId=" + companyId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/



    services.getRequirementListByCompany = function (companyId) {
        var getRequirementUrl = services.configuration.XSEED_API_URL + "Job/GetJobsByCompany?CompanyId=" + companyId;

        var promise = $q.all(
            [
              XSeedApi.GET(getRequirementUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.getCandidateResumeParserCall = function (request) {
        var url = services.configuration.XSEED_API_URL + "CandidateUser/ParseResume/";
        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    services.createQNoteCandidate = function (note, candidateId) {
        var url = services.configuration.XSEED_API_URL + "CandidateUser/SaveQuickNote?note=" + note + "&candidateId=" + candidateId;

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.getQNoteListCandidate = function (candidateId) {
        var url = services.configuration.XSEED_API_URL + "CandidateUser/GetQuickNotes?candidateId=" + candidateId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.getActivityListCandidate = function (candidateId) {
        var url = services.configuration.XSEED_API_URL + "CandidateUser/GetCandidateActivityLog?candidateId=" + candidateId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.getCandidateAdvanceSearchResult = function (request, pageSize, pageNumber, sortBy, sortOrder, isExport) {

        pageSize = pageSize === undefined ? 5 : pageSize;
        pageNumber = pageNumber === undefined ? 1 : pageNumber;
        sortBy = sortBy === undefined ? "CreatedOn" : sortBy;
        sortOrder = sortOrder === undefined ? "desc" : sortOrder;
        isExport = isExport === undefined ? false : isExport;

        var url = services.configuration.XSEED_API_URL + "AdvancedSearch/AdvanceSearchCandidates?pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&sortBy=" + sortBy + "&sortOrder=" + sortOrder + "&isExport=" + isExport;

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    services.viewCandidateResume = function (candidateId) {

        var url = services.configuration.XSEED_API_URL + "CandidateUser/ViewCandidateResume?candidateId=" + candidateId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*---------------------------------Candidate End--------------------------------------------------*/

    /*---------------------------------SaerchCtrl Start--------------------------------------------------*/

    services.searchCandidatesBySource = function (request, pageSize, pageNumber, sortBy, sortOrder, isExport) {

        pageSize = pageSize === undefined ? 5 : pageSize;
        pageNumber = pageNumber === undefined ? 1 : pageNumber;
        sortBy = sortBy === undefined ? "CreatedOn" : sortBy;
        sortOrder = sortOrder === undefined ? "desc" : sortOrder;
        isExport = isExport === undefined ? false : isExport;

        var url = services.configuration.XSEED_API_URL + "Job/SourceCandidates?pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&sortBy=" + sortBy + "&sortOrder=" + sortOrder + "&isExport=" + isExport;

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };


    services.searchCandidatesByBooleanQuery = function (request, pageSize, pageNumber, sortBy, sortOrder, isExport) {

        pageSize = pageSize === undefined ? 5 : pageSize;
        pageNumber = pageNumber === undefined ? 1 : pageNumber;
        sortBy = sortBy === undefined ? "CreatedOn" : sortBy;
        sortOrder = sortOrder === undefined ? "desc" : sortOrder;
        isExport = isExport === undefined ? false : isExport;

        var url = services.configuration.XSEED_API_URL + "Job/SourceCandidates?pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&sortBy=" + sortBy + "&sortOrder=" + sortOrder + "&isExport=" + isExport;

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    services.getSubmitSourcedCandidates = function (request) {

        var url = services.configuration.XSEED_API_URL + "Submission/SubmitSourcedCandidates";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    services.getMonsterCount = function (request) {

        var url = services.configuration.XSEED_API_URL + "Job/GetMonsterLicenseInfo";

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*---------------------------------SaerchCtrl End--------------------------------------------------*/

    /*---------------------------------Query Builder Start--------------------------------------------------*/


    services.getBooleanQueries = function (organizationId) {

        var url = services.configuration.XSEED_API_URL + "BooleanSearch/Get?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.saveBooleanQueries = function (request) {

        var url = services.configuration.XSEED_API_URL + "BooleanSearch/Post";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    /*---------------------------------Query Builder End--------------------------------------------------*/


    /*---------------------------------Submission Start--------------------------------------------------*/
    services.getSubmissionById = function (organizationId, submissionId) {

        var getSubmissionsurl = services.configuration.XSEED_API_URL + "Submission/Get/" + submissionId + "?organizationId=" + organizationId;
        var promise = $q.all(
            [
              XSeedApi.GET(getSubmissionsurl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getSubmissions = function (organizationId, pageSize, pageNumber, sortBy, sortOrder, isExport) {
        pageSize = pageSize === undefined ? 5 : pageSize;
        pageNumber = pageNumber === undefined ? 1 : pageNumber;
        sortBy = sortBy === undefined ? "CreatedOn" : sortBy;
        sortOrder = sortOrder === undefined ? "desc" : sortOrder;
        isExport = isExport === undefined ? false : isExport;

        //var getSubmissionsurl = services.configuration.XSEED_API_URL + "Submission/Get?organizationId=" + organizationId;
        var getSubmissionsurl = services.configuration.XSEED_API_URL + "Submission/Get?organizationId=" + organizationId + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&sortBy=" + sortBy + "&sortOrder=" + sortOrder + "&isExport=" + isExport;;

        var promise = $q.all(
            [
              XSeedApi.GET(getSubmissionsurl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getSubmissionStatusLookUp = function () {

        var getSubmissionFeedbackStatusurl = services.configuration.XSEED_API_URL + "LookUp/GetSubmissionFeedbackStatus";
        var getClientFeedbackStatusurl = services.configuration.XSEED_API_URL + "LookUp/GetClientFeedbackStatus";
        var getCandidateFeedbackStatusurl = services.configuration.XSEED_API_URL + "LookUp/GetCandidateFeedbackStatus";

        var promise = $q.all(
            [
              XSeedApi.GET(getSubmissionFeedbackStatusurl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getClientFeedbackStatusurl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getCandidateFeedbackStatusurl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.saveFeedback = function (feedbackType, request) {
        var url;
        if (feedbackType == 'Internal Feedback') {
            url = services.configuration.XSEED_API_URL + "SubmissionStatus/Post";
        }
        else if (feedbackType == 'Candidate Feedback') {
            url = services.configuration.XSEED_API_URL + "CandidateFeedback/Post";
        }
        else if (feedbackType == 'Client Feedback') {
            url = services.configuration.XSEED_API_URL + "ClientFeedback/Post";
        }

        var promise = $q.all(
                [
                  XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
                ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getQNoteListSubmission = function (submissionId) {
        var url = services.configuration.XSEED_API_URL + "Submission/GetQuickNotes?submissionId=" + submissionId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.createQNoteSubmission = function (note, submissionId) {
        var url = services.configuration.XSEED_API_URL + "Submission/SaveQuickNote?note=" + note + "&submissionId=" + submissionId;

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.searchSubmissions = function (request, pageSize, pageNumber, sortBy, sortOrder, isExport) {
        pageSize = pageSize === undefined ? 5 : pageSize;
        pageNumber = pageNumber === undefined ? 1 : pageNumber;
        sortBy = sortBy === undefined ? "CreatedOn" : sortBy;
        sortOrder = sortOrder === undefined ? "desc" : sortOrder;
        isExport = isExport === undefined ? false : isExport;

        var url = services.configuration.XSEED_API_URL + "AdvancedSearch/AdvanceSearchSubmissions?pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&sortBy=" + sortBy + "&sortOrder=" + sortOrder + "&isExport=" + isExport;;

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.submissionPopupLookupCall = function (organizationId) {
        var getCompanyUrl = services.configuration.XSEED_API_URL + "LookUp/GetCompanies?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(getCompanyUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.submissionFilterLookUp = function (organizationId) {
        var getOrganizationUserUrl = services.configuration.XSEED_API_URL + "LookUp/GetOrganizationUsers?organizationId=" + organizationId;
        var getCompanyUrl = services.configuration.XSEED_API_URL + "LookUp/GetCompanies?organizationId=" + organizationId;
        var getJobTitleUrl = services.configuration.XSEED_API_URL + "LookUp/GetJobTitles?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(getOrganizationUserUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getCompanyUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(getJobTitleUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*---------------------------------Submission End--------------------------------------------------*/

    /*---------------------------------Notification Start--------------------------------------------------*/

    services.sendJobEmailNotification = function (request) {
        var Id = request.Id;
        var url = services.configuration.XSEED_API_URL + "Notification/SendJobEmailNotification?jobId=" + Id;

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.sendCandidateEmailNotification = function (request) {
        var Id = request.Id;
        var url = services.configuration.XSEED_API_URL + "Notification/SendCandidateEmailNotification?candidateId=" + Id;

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    /*---------------------------------Notification End--------------------------------------------------*/

    /*---------------------------------Schedule Start--------------------------------------------------*/

    services.saveSchedule = function (request) {
        var url = services.configuration.XSEED_API_URL + "Scheduler/PostScheduleLogDetail";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    services.getScheduleLogList = function (organizationUserId) {
        var url = services.configuration.XSEED_API_URL + "Scheduler/GetScheduleLog?OrganizationUserId=" + organizationUserId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.getScheduleLogNotifications = function (organizationUserId) {
        var url = services.configuration.XSEED_API_URL + "Scheduler/GetScheduleLogNotifications?OrganizationUserId=" + organizationUserId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.editSchedule = function (request) {
        var url = services.configuration.XSEED_API_URL + "Scheduler/PutScheduleLogDetail";

        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    /*---------------------------------Schedule End--------------------------------------------------*/

    /*---------------------------------Lookup Start--------------------------------------------------*/
    services.getStateListByCountry = function (countryId) {

        var url = services.configuration.XSEED_API_URL + "LookUp/GetStates?countryId=" + countryId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getCityListByState = function (stateId) {

        var url = services.configuration.XSEED_API_URL + "LookUp/GetCities?stateId=" + stateId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getCompanySourceList = function (organizationId) {

        var url = services.configuration.XSEED_API_URL + "LookUp/GetCompanySources?organizationId=" + organizationId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    //services.getCompanySourceListByCompanyId = function (companyId, organizationId) {

    //    var url = services.configuration.XSEED_API_URL + "Company/GetCompanySource/" + companyId + "?organizationId=" + organizationId;
    //    var promise = $q.all(
    //        [
    //          XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
    //        ]
    //    );
    //    return promise;
    //};
    /*-----------------------------------------------------------------------------------*/

    services.getCityStateCode = function (countryName, cityName) {

        var url = services.configuration.XSEED_API_URL + "LookUp/GetCityStateCode/?country=" + countryName + "&city=" + cityName;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*---------------------------------Lookup End--------------------------------------------------*/



    /*---------------------------------Reports--------------------------------------------------*/

    services.getCandidateAddedReport = function (OrganizationId, pageSize, pageNumber, sortBy, sortOrder, daysBefore, isExport) {
        pageSize = pageSize === undefined ? 5 : pageSize;
        pageNumber = pageNumber === undefined ? 1 : pageNumber;
        sortBy = sortBy === undefined ? "CreatedOn" : sortBy;
        sortOrder = sortOrder === undefined ? "desc" : sortOrder;
        isExport = isExport === undefined ? false : isExport;
        daysBefore = daysBefore === undefined ? -7 : daysBefore;

        var url = services.configuration.XSEED_API_URL + "Report/getCandidateAddedReport?OrganizationId=" + OrganizationId + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&sortBy=" + sortBy + "&sortOrder=" + sortOrder + "&daysBefore=" + daysBefore + "&isExport=" + isExport;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/
    services.getRequirementTrackerReport = function (OrganizationId, pageSize, pageNumber, sortBy, sortOrder, daysBefore, isExport) {
        pageSize = pageSize === undefined ? 5 : pageSize;
        pageNumber = pageNumber === undefined ? 1 : pageNumber;
        sortBy = sortBy === undefined ? "CreatedOn" : sortBy;
        sortOrder = sortOrder === undefined ? "desc" : sortOrder;
        isExport = isExport === undefined ? false : isExport;
        daysBefore = daysBefore === undefined ? -7 : daysBefore;

        var url = services.configuration.XSEED_API_URL + "Report/getRequirementTrackerReport?OrganizationId=" + OrganizationId + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&sortBy=" + sortBy + "&sortOrder=" + sortOrder + "&daysBefore=" + daysBefore + "&isExport=" + isExport;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/
    services.getSubmissionTrackerReport = function (OrganizationId, pageSize, pageNumber, sortBy, sortOrder, daysBefore, isExport) {
        pageSize = pageSize === undefined ? 5 : pageSize;
        pageNumber = pageNumber === undefined ? 1 : pageNumber;
        sortBy = sortBy === undefined ? "CreatedOn" : sortBy;
        sortOrder = sortOrder === undefined ? "desc" : sortOrder;
        isExport = isExport === undefined ? false : isExport;
        daysBefore = daysBefore === undefined ? -7 : daysBefore;

        var url = services.configuration.XSEED_API_URL + "Report/getSubmissionTrackerReport?OrganizationId=" + OrganizationId + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&sortBy=" + sortBy + "&sortOrder=" + sortOrder + "&daysBefore=" + daysBefore + "&isExport=" + isExport;;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/
    services.getClientLeadReport = function (OrganizationId, pageSize, pageNumber, sortBy, sortOrder, daysBefore, isExport) {
        pageSize = pageSize === undefined ? 5 : pageSize;
        pageNumber = pageNumber === undefined ? 1 : pageNumber;
        sortBy = sortBy === undefined ? "CreatedOn" : sortBy;
        sortOrder = sortOrder === undefined ? "desc" : sortOrder;
        isExport = isExport === undefined ? false : isExport;
        daysBefore = daysBefore === undefined ? -7 : daysBefore;

        var url = services.configuration.XSEED_API_URL + "Report/GetCompanyLeadReport?OrganizationId=" + OrganizationId + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&sortBy=" + sortBy + "&sortOrder=" + sortOrder + "&daysBefore=" + daysBefore + "&isExport=" + isExport;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/
    services.getActivityReport = function (OrganizationId, pageSize, pageNumber, sortBy, sortOrder, daysBefore, isExport) {
        pageSize = pageSize === undefined ? 5 : pageSize;
        pageNumber = pageNumber === undefined ? 1 : pageNumber;
        sortBy = sortBy === undefined ? "OrganizationUser" : sortBy;
        sortOrder = sortOrder === undefined ? "desc" : sortOrder;
        isExport = isExport === undefined ? false : isExport;
        daysBefore = daysBefore === undefined ? -7 : daysBefore;

        var url = services.configuration.XSEED_API_URL + "Report/GetActivityReport?OrganizationId=" + OrganizationId + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&sortBy=" + sortBy + "&sortOrder=" + sortOrder + "&daysBefore=" + daysBefore + "&isExport=" + isExport;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.getClosureReport = function (OrganizationId, pageSize, pageNumber, sortBy, sortOrder, daysBefore, isExport) {
        pageSize = pageSize === undefined ? 5 : pageSize;
        pageNumber = pageNumber === undefined ? 1 : pageNumber;
        sortBy = sortBy === undefined ? "CreatedOn" : sortBy;
        sortOrder = sortOrder === undefined ? "desc" : sortOrder;
        isExport = isExport === undefined ? false : isExport;
        daysBefore = daysBefore === undefined ? -7 : daysBefore;

        var url = services.configuration.XSEED_API_URL + "Report/GetClosureReport?OrganizationId=" + OrganizationId + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&sortBy=" + sortBy + "&sortOrder=" + sortOrder + "&daysBefore=" + daysBefore + "&isExport=" + isExport;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.getAdvanceSearchResultForReports = function (OrganizationId, request) {

        var url = services.configuration.XSEED_API_URL + "Search/SearchReports/" + OrganizationId;
        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    /*---------------------------------Reports--------------------------------------------------*/


    //----------------------------------UserRoles-----------------------------------------------

    services.getUserRoles = function (organizationId) {

        //var url = services.configuration.XSEED_API_URL + "User/GetAllUserRoles";
        var url = services.configuration.XSEED_API_URL + "User/GetAllUserRoles?organizationId=" + organizationId;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.createNewOrganizationUserRole = function (request) {

        var url = services.configuration.XSEED_API_URL + "User/CreateUserRole";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.deleteUserRole = function (id) {

        var url = services.configuration.XSEED_API_URL + 'User/DeleteUserRole?Id=' + id;
        var promise = $q.all(
            [
              XSeedApi.DELETE(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.updateUserRole = function (request) {

        var url = services.configuration.XSEED_API_URL + 'User/UpdateUserRole';

        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/


    /*---------------------------------ResumeBlaster Start--------------------------------------------------*/
    services.getResumeBlasterList = function (id) {
        var url = services.configuration.XSEED_API_URL + "ResumeBlaster/Get";
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/
    services.createRecruiter = function (request) {
        var url = services.configuration.XSEED_API_URL + "ResumeBlaster/Post";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/
    services.getResumeBlasterDetail = function (id) {
        var url = services.configuration.XSEED_API_URL + "ResumeBlaster/Get/" + id;

        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*-----------------------------------------------------------------------------------*/

    services.editResumeBlaster = function (request) {
        var url = services.configuration.XSEED_API_URL + "ResumeBlaster/Put";

        var promise = $q.all(
            [
              XSeedApi.PUT(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.BlastMail = function (request) {
        var url = services.configuration.XSEED_API_URL + "ResumeBlaster/BlastMail/";

        var promise = $q.all(
            [
              XSeedApi.POST(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    /*---------------------------------ResumeBlaster End--------------------------------------------------*/

    /*---------------------------------Dashboard Start--------------------------------------------------*/
    services.getDashboardDetail = function (OrganizationId, UserId) {
        var url = services.configuration.XSEED_API_URL + "Dashboard/Get?organizationId=" + OrganizationId + "&userId=" + UserId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };


    services.getDashboardRequirementVsSubmissionReportData = function (OrganizationId, UserId) {
        var url = services.configuration.XSEED_API_URL + "Dashboard/GetjobSubmissionChartDetail?organizationId=" + OrganizationId + "&userId=" + UserId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.getDashboardSubmissionReportData = function (OrganizationId, UserId) {
        var url = services.configuration.XSEED_API_URL + "Dashboard/GetSubmissionChartDetail?organizationId=" + OrganizationId + "&userId=" + UserId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.getDashboardCandidateAddedReportData = function (OrganizationId, UserId) {
        var url = services.configuration.XSEED_API_URL + "Dashboard/GetCandidateChartDetail?organizationId=" + OrganizationId + "&userId=" + UserId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.getDashboardRequirementStatusReportData = function (OrganizationId, UserId) {
        var url = services.configuration.XSEED_API_URL + "Dashboard/GetJobStatusChartDetail?organizationId=" + OrganizationId + "&userId=" + UserId;
        var promise = $q.all(
            [
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    /*---------------------------------LookUp Start--------------------------------------------------*/
    services.getLookUpSkill = function () {
        var getSkillUrl = services.configuration.XSEED_API_URL + "LookUp/GetSkills";
        var promise = $q.all(
            [
              XSeedApi.GET(getSkillUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };
    /*-----------------------------------------------------------------------------------*/

    services.getLookUpForSchedule = function (organizationId) {
        var getOrganizationUserUrl = services.configuration.XSEED_API_URL + "LookUp/GetOrganizationUsers?organizationId=" + organizationId;
        var url = services.configuration.XSEED_API_URL + "Company/Get?organizationId=" + organizationId;
        var promise = $q.all(
            [
              XSeedApi.GET(getOrganizationUserUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken()),
              XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*--------------------------------------LookUp End----------------------------------------------*/

    /*--------------------------------------Bulk Upload Start----------------------------------------------*/

    services.bulkResumeUpload = function (request) {
        var resumeUploadUrl = services.configuration.XSEED_API_URL + "ResumeUpload/ResumeBulkUpload";

        var promise = $q.all(
            [
              XSeedApi.POST(resumeUploadUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken(), request)
            ]
        );
        return promise;
    };

    services.getParsedResumeList = function (pageSize, pageNumber, sortBy, sortOrder) {
        pageSize = pageSize === undefined ? 8 : pageSize;
        pageNumber = pageNumber === undefined ? 1 : pageNumber;
        sortBy = sortBy === undefined ? "CreatedOn" : sortBy;
        sortOrder = sortOrder === undefined ? "desc" : sortOrder;

        var getParsedResumeUrl = services.configuration.XSEED_API_URL + "ResumeUpload/GetParsedResumes?pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&sortBy=" + sortBy + "&sortOrder=" + sortOrder;

        var promise = $q.all(
            [
              XSeedApi.GET(getParsedResumeUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    services.acceptRejectParsedResume = function (parsedResumeId, isValidResume) {
        var acceptRejectParsedResumeUrl = services.configuration.XSEED_API_URL + "ResumeUpload/AcceptRejectParsedResume?Id=" + parsedResumeId + "&IsValidResume=" + isValidResume;

        var promise = $q.all(
            [
              XSeedApi.PUT(acceptRejectParsedResumeUrl, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
            ]
        );
        return promise;
    };

    /*--------------------------------------Bulk Upload End----------------------------------------------*/

    /*-----------------------------------------------------------------------------------*/
    //Company search
    //services.companySearch = function (search) {
    //    console.log('Search-->' + search);

    //    //var url = services.configuration.XSEED_API_URL + "Company/Get?organizationId=" + id;
    //    var url = "http://10.33.2.204:3000/api/companySearch/info";        

    //    var promise = $q.all(
    //        [
    //          XSeedApi.GET(url, XSeedHTTPHeaderBuilder.getHeadersForAccessToken())
    //        ]
    //    );
    //    return promise;
    //};
    /*-----------------------------------------------------------------------------------*/

    return services;
}




XSeedApiFactory.$inject = ["$q", "_", "$log", "XSeedApi", "XSeedHTTPHeaderBuilder", "$rootScope", "$http"];

