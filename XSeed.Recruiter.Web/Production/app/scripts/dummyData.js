﻿XSEED_APP.controller('dummyData', dummyData);

function dummyData($scope) {
    $scope.candidates = [
        { 'name': 'John', 'jobTitle': 'Developer', 'skills': 'Problem Solving' },
    { 'name': 'Mark', 'jobTitle': 'Developer1', 'skills': 'Problem Solving1' },
    { 'name': 'John2', 'jobTitle': 'Developer2', 'skills': 'Problem Solving2' },
    { 'name': 'John3', 'jobTitle': 'Developer3', 'skills': 'Problem Solving3' },
    { 'name': 'John4', 'jobTitle': 'Developer4', 'skills': 'Problem Solving4' },
    { 'name': 'John5', 'jobTitle': 'Developer5', 'skills': 'Problem Solving5' }
    ]

    $scope.strings = [
        { 'stringName': '.Net Developer and C# and Javascript' },
    { 'stringName': 'Java Developer and Bootstrap and Javascript OR Jquery' },
    { 'stringName': '.Net Developer and C# and Javascript' },
    { 'stringName': 'Java Developer and Bootstrap and Javascript OR Jquery' },
    { 'stringName': '.Net Developer and C# and Javascript' },
    { 'stringName': 'Java Developer and Bootstrap and Javascript OR Jquery' }
    ]

    $scope.clients = [
        { 'no': '1', 'name': 'John', 'role': 'Recruiter', 'email': 'abc@xyz.com', 'phone': '9865471236', 'reporting': 'Manoj Shinde', 'status': 'Deactive' },
        { 'no': '2', 'name': 'Airi Satou', 'role': 'Recruiter', 'email': 'airi.satou@xyz.com', 'phone': '9865471236', 'reporting': 'Manoj Shinde', 'status': 'Active' },
        { 'no': '3', 'name': 'Angelica Ramos', 'role': 'Recruiter', 'email': 'abc@xyz.com', 'phone': '9865471236', 'reporting': 'Manoj Shinde', 'status': 'Active' },
        { 'no': '4', 'name': 'Ashton Cox', 'role': 'Recruiter', 'email': 'abc@xyz.com', 'phone': '9865471236', 'reporting': 'Manoj Shinde', 'status': 'Deactive' },
        { 'no': '5', 'name': 'John', 'role': 'Recruiter', 'email': 'abc@xyz.com', 'phone': '9865471236', 'reporting': 'Manoj Shinde', 'status': 'Deactive' },
        { 'no': '6', 'name': 'Airi Satou', 'role': 'Recruiter', 'email': 'abc@xyz.com', 'phone': '9865471236', 'reporting': 'Manoj Shinde', 'status': 'Active' },
        { 'no': '7', 'name': 'Angelica Ramos', 'role': 'Recruiter', 'email': 'abc@xyz.com', 'phone': '9865471236', 'reporting': 'Manoj Shinde', 'status': 'Active' },
        { 'no': '8', 'name': 'John', 'role': 'Recruiter', 'email': 'abc@xyz.com', 'phone': '9865471236', 'reporting': 'Manoj Shinde', 'status': 'Deactive' },
        { 'no': '9', 'name': 'Bruno Nash	', 'role': 'Recruiter', 'email': 'abc@xyz.com', 'phone': '9865471236', 'reporting': 'Manoj Shinde', 'status': 'Deactive' },
        { 'no': '10', 'name': 'Brielle Williamson', 'role': 'Recruiter', 'email': 'abc@xyz.com', 'phone': '9865471236', 'reporting': 'Manoj Shinde', 'status': 'Deactive' }
    ]

};