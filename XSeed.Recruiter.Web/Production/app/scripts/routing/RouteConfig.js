/// <reference path="../controllers/companyController.js" />
/// <reference path="../controllers/companyController.js" />

//'use strict';

XSEED_APP.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.caseInsensitiveMatch = true;
        $routeProvider
        .when('/', {
            //action: "landing"
            action: "home"
        })
        .when('/dashboard', {
            action: "standard.dashboard",
            pageTitle: "Dashboard"
        })
        .when('/job', {
            action: "standard.job"
        })
        .when('/test', {
            action: "standard.test"
        })
        .when('/rd', {
            action: "standard.recruiterDashboard"
        })
        .when('/candidate', {
            action: "standard.candidatesMain"
        })
        //.when('/registerOrganization', {
        //    action: "standard.regOrganization"
        //})
        .when('/login', {
            action: "landing.login"
        })
        //.when('/register', {
        //    action: "landing.register"
        //})
        .when('/changePassword', {
            action: "standard.changePassword",
            pageTitle: "Change Password"
        })
        .when('/forgotPassword', {
            action: "landing.forgotPassword",
        })
        .when('/resetPassword', {
            action: "landing.resetPassword",
        })

        /*#region organization*/
        .when('/organization', {
            action: "standard.organization",
            pageTitle: "Organization"
        })
        .when('/organizationProfile', {
            action: "standard.organization.organizationProfile",
            pageTitle: "Organization Profile"
        })
        .when('/editOrganization', {
            action: "standard.organization.editOrganization",
            pageTitle: "Edit Organization"

        })
        .when('/editOrganization', {
            action: "standard.organization.editOrganization",
            pageTitle: "Edit Organization"

        })
        .when('/systemUsers', {
            action: "standard.organization.systemUsers",
            pageTitle: "Users"
        })
            .when('/createNewUser', {
                action: "standard.organization.createNewUser",
                pageTitle: "Create User"
            })
        .when('/editUser', {
            action: "standard.organization.editUser",
            pageTitle: "Edit User"
        })
        .when('/userProfile', {
            action: "standard.organization.userProfile",
            pageTitle: "User Profile"
        })
        .when('/userEmailConfiguration', {
            action: "standard.organization.userEmailConfiguration",
            pageTitle: "Configure Email"
        })
        /*#endRegion organization*/

         /*#region Company*/
            .when('/company', {
                action: "standard.company",
                pageTitle: "Company"
            })
             .when('/companyList', {
                 action: "standard.company.companyList",
                 pageTitle: "Clients"
             })
            .when('/createCompany', {
                action: "standard.company.createCompany",
                pageTitle: "Add Client"
            })
            .when('/companyDetail', {
                action: "standard.company.companyDetail",
                pageTitle: "Client Details"
            })
            .when('/editCompany', {
                action: "standard.company.createCompany",
                pageTitle: "Edit Client"
            })

            .when('/createContact', {
                action: "standard.company.createContact",
                pageTitle: "Add Client Contact"
            })
            .when('/editContact', {
                action: "standard.company.editContact",
                pageTitle: "Edit Client Contact"
            })

            .when('/companySearch', {
                action: "standard.company.companySearch",
                pageTitle: "Search Client"
            })
            .when('/companySearchModified', {
                action: "standard.company.companySearchModified",
                pageTitle: "Search Client"
            })
            /*#endregion Company*/

            /*#region Vendor*/
            .when('/vendor', {
                action: "standard.vendor",
                pageTitle: "Vendor"
            })
             .when('/vendorList', {
                 action: "standard.vendor.vendorList",
                 pageTitle: "Client Profile List"
             })
            .when('/createVendor', {
                action: "standard.vendor.createVendor",
                pageTitle: "Add Client"
            })
            .when('/vendorDetail', {
                action: "standard.vendor.vendorDetail",
                pageTitle: "Vendor Details"
            })
            .when('/editVendor', {
                action: "standard.vendor.createVendor",
                pageTitle: "Edit Client"
            })
            .when('/vendorEmailList', {
                action: "standard.vendor.vendorEmailList",
                pageTitle: "Client Emails"
            })
            /*#endregion Vendor*/

             /*#region Resume Emails*/
            //.when('/resumeEmail', {
            //    action: "standard.resumeEmail",
            //    pageTitle: "Resume Email"
            //})
            //.when('/resumeEmailList', {
            //    action: "standard.resumeEmail.resumeEmailList",
            //    pageTitle: "Resume Emails"
            //})
            /*#endregion Resume Emails*/

            /*#Requirements*/
            .when('/requirementList', {
                action: "standard.requirement.requirementList",
                pageTitle: "Requirements"
            })
            .when('/editRequirement', {
                action: "standard.requirement.editRequirement",
                pageTitle: "Edit Requirement"
            })
            .when('/createRequirement', {
                action: "standard.requirement.createRequirement",
                pageTitle: "Add Requirement"
            })
            .when('/requirementDetail', {
                action: "standard.requirement.requirementDetail",
                pageTitle: "Requirement Details"
            })
            //.when('/requirementQueue', {
            //    action: "standard.requirement.requirementQueue",
            //    pageTitle: "Requirement Queue"
            //})
            .when('/appliedCandidates', {
                action: "standard.appliedCandidates",
                pageTitle: "Applied Candidates"
            })
             /*#endregion Requirements*/

             /*#submissionToClient*/

            .when('/submissionToClient', {
                action: "standard.submissionToClient",
                pageTitle: "Submit To Client"
            })
             /*#endregion submissionToClient*/

            /* Monster Boolean Query Result*/
            .when('/monsterBooleanQueryResult', {
                action: "standard.requirement.monsterBooleanQueryResult",
                pageTitle: "Candidate Search"
            })

            .when('/searchCandidate', {
                action: "standard.searchCandidate",
                pageTitle: "Candidate Search"
            })

            /*#endregion Monster Boolean Query Result*/

            /*#Candidate*/
            .when('/candidateList', {
                action: "standard.candidate.candidateList",
                pageTitle: "Candidates"
            })
            .when('/createCandidate', {
                action: "standard.candidate.createCandidate",
                pageTitle: "Add Candidate"
            })
            .when('/candidateDetail', {
                action: "standard.candidate.candidateDetail",
                pageTitle: "Candidate Details"
            })
            .when('/editCandidate', {
                action: "standard.candidate.createCandidate",
                pageTitle: "Edit Candidate"
            })
            .when('/resumeParser', {
                action: "standard.candidate.resumeParser",
                pageTitle: "Resume Parser"
            })
            .when('/parsedResumes', {
                action: "standard.candidate.parsedResumes",
                pageTitle: "Parsed Resumes"
            })
            .when('/uploadResumes', {
                action: "standard.uploadResumes",
                pageTitle: "Upload Resumes"
            })
            .when('/resumeEmails', {
                action: "standard.candidate.resumeEmails",
                pageTitle: "Resume Emails"
            })
        /*#endregion Candidate*/

            /*#queryBuilder*/
            .when('/queryBuilder', {
                action: "standard.queryBuilder.queryBuilder",
                pageTitle: "Query Builder"
            })
            /*#endregion queryBuilder*/

            /*#submission*/
            .when('/submission', {
                action: "standard.submission",
                pageTitle: "Submissions"
            })
            .when('/submissions', {
                action: "standard.submission.submissions",
                pageTitle: "Submissions"
            })
            /*#endregion submission*/

             /*#startregion reports*/
            .when('/reports', {
                action: "standard.reports.report",
                pageTitle: "Reports"
            })
            /*#endregion reports*/

            /*#ResumeBlaster*/
            .when('/resumeBlasterList', {
                action: "standard.resumeBlaster.resumeBlasterList",
                pageTitle: "Resume Blaster"
            })
            .when('/editResumeBlaster', {
                action: "standard.resumeBlaster.editResumeBlaster",
                pageTitle: "Edit Recruiter"
            })
            .when('/createResumeBlaster', {
                action: "standard.resumeBlaster.createResumeBlaster",
                pageTitle: "Add Recruiter"
            })
            .when('/resumeBlasterDetail', {
                action: "standard.resumeBlaster.ResumeBlasterDetail",
                pageTitle: "Recruiter Details"
            })
             /*#endregion ResumeBlaster*/


            /*#Manage User Role*/
                .when('/createUserRole', {
                    action: "standard.organization.createUserRole",
                    pageTitle: "Create User Role"
                })

                .when('/userRoles', {
                    action: "standard.organization.userRoles",
                    pageTitle: "User Role List"
                })
            /*#endregion Manage User Role*/

            .when('/switchView', {
                action: "standard.switchView"
            })

        .otherwise({
            redirectTo: '/'
            //action: "landing.login"

        });
    }
]);
