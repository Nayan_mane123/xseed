'use strict';

XSEED_APP.factory('XSeedAlert', XSeedAlert).factory("buildQuery", buildQuery)
    .factory("grouping", grouping)
    .factory("addQuotes", addQuotes);
/**
 * @ngInject
 */
function XSeedAlert($rootScope) {

    console.log($rootScope);
    var swal = window.swal;

    //public methods
    var self = {
        swal: function(arg1, arg2, arg3) {
            return swal(arg1, arg2, arg3);
        },
        success: function(title, message) {
            $rootScope.$evalAsync(function() {
                swal(title, message, 'success');
            });
        },
        error: function(title, message) {
            $rootScope.$evalAsync(function() {
                swal(title, message, 'error');
            });
        },
        warning: function(title, message) {
            $rootScope.$evalAsync(function() {
                swal(title, message, 'warning');
            });
        },
        info: function(title, message) {
            $rootScope.$evalAsync(function() {
                swal(title, message, 'info');
            });
        }
    };

    return self;
};

//factory for easy  buildQuery
function buildQuery(addQuotes) {
    return {
        getResult: function(keywords, boolOperator) {
            angular.forEach(keywords, function(value, index) {
                keywords[index] = addQuotes.addQuotesCheck(value);
            });
            if (keywords.length > 1) {
                var res = keywords.toString().replace(/,/g, boolOperator);
                var group = "(" + res + ")";
                return group;
            }
            return keywords.toString();
        }
    }
};

//factory for addQuotes.addQuotesCheck
function addQuotes() {
    return {
        addQuotesCheck: function(word) {
            var result = "";
            var result0 = word.split(" ");
            var result1 = word.split("-");
            var result2 = word.split("/");
            var result3 = word.split(".");
            var result4 = word.split("#");
            var result5 = word.split("+");

            if (result0.length > 1 || result1.length > 1 || result2.length > 1 || result3.length > 1 || result4.length > 1 || result5.length > 1) {
                result = ('"' + word + '"');
                return result;
            }
            return word;
        }
    }
};

//factory for grouping
function grouping() {
    return {
        buildGrouping: function(groupkeywords) {
            return ("(" + groupkeywords + ")");
        }
    }
};