(function (ExportExcel, undefined) {
	if (!ExportExcel) ExportExcel = {};
	
	function Workbook() {
	   	if(!(this instanceof Workbook)) return new Workbook();
	   	this.SheetNames = [];
	   	this.Sheets = {};
	   };
	   var wb = new Workbook();

	   function datenum(v, date1904) {
	   	if(date1904) v+=1462;
	   	var epoch = Date.parse(v);
	   	return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
	   };
	   
	   function sheet_from_array_of_arrays(data, opts) {
	   	var ws = {};
	   	var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};

	   	for(var R = 0; R != data.length; ++R) {
	   		for(var C = 0; C != data[R].length; ++C) {
	   			if(range.s.r > R) range.s.r = R;
	   			if(range.s.c > C) range.s.c = C;
	   			if(range.e.r < R) range.e.r = R;
	   			if(range.e.c < C) range.e.c = C;
	   			var cell = {v: data[R][C] };
	   			if(cell.v == null) continue;
	   			var cell_ref = XLSX.utils.encode_cell({c:C,r:R});

	   			if(typeof cell.v === 'number'){
	   				cell.t = 'n';
	   				//APPEND OR FOR COLUMNS FOR FORMATTING TO CURRENCY FIELD
	   				//ADDED FOR Maximum Yearly Payout WITH INDEX - 5
	   				if(C==5){
	   				 cell.t='c';
	   				 cell.v = XLSX.SSF.format('$0.00', cell.v);
	   				}
	   			}
	   			else if(typeof cell.v === 'boolean') cell.t = 'b';
	   			else if(cell.v instanceof Date) {
	   				cell.t = 'n'; cell.z = XLSX.SSF._table[14];
	   				cell.v = datenum(cell.v);
	   			}
	   			else cell.t = 's';
	   			ws[cell_ref] = cell;
	   		}
	   	}
	     	if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
	     	return ws;
	    };
	    
	    function convertObjArray(objarray,excelcols) {
	        try {	            
	            var keys = Object.keys(excelcols);	            
	        var columns = []	        
	        for (var key in keys)
	        {	            
	            columns.push(excelcols[keys[key]])
            }
	        	
	        var arrarr = [columns];
	        for (var n = 0; n < objarray.length; n++) {
	          var row = [];
	          var eachColValue = "";
	          for (var i in keys){
	        	  var col = parseObject(objarray[n],keys[i]);
	        	  if(col == undefined){
	        		  eachColValue = ""
	        	  }else if(col instanceof Array){
	        		  var summary = [];
	        		  for (j in col) {
							summary.push("$" + col[j]["amount"] + " in year ("
									+ col[j]["month"] + "/" + col[j]["year"] + ")");
	        		  }
	        		  eachColValue = summary.join("\r\n");
	        	  }else{
	        		  if(keys[i] == "gender"){
	        			  if(col == "F")col = "Female"
	        			  else col = "Male"	  
	        		  }
	        			  
	        		  eachColValue = col;
	        	  }
	        	  row.push(eachColValue);  
	          } 
	          arrarr.push(row);
	        }
	        console.log(arrarr.length + ' records');
	        return arrarr;
	      } catch (e) {	          	          
	        console.log(objarray);
	        return [
	          []
	        ];
	      }
	  };
	  

		function parseObject(o, path) {
			for (var a, p = path.split('.'), i = 0; o && (a = p[i++]); o = o[a]);
			return o;
		};
	  
	  
	  function s2ab(s) {
	    var buf = new ArrayBuffer(s.length);
	    var view = new Uint8Array(buf);
	    for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
	    return buf;
	  };
	  
	  ExportExcel. savetoexcel=function(filename,sheetName,data,columns){
	       var wb = new Workbook();
	       var ws_name = sheetName;
	       var twodarr = convertObjArray(data,columns);
	       var ws = sheet_from_array_of_arrays(twodarr);
	       wb.SheetNames.push(ws_name);
	       wb.Sheets[ws_name] = ws;
	       //XLSX.writeFile(wb, filename);
	       /* bookType can be 'xlsx' or 'xlsm' or 'xlsb' */
	       var wopts = { bookType:'xlsx', bookSST:true, type:'binary' };	       
	       var wbout = XLSX.write(wb,wopts);
	       saveAs(new Blob([s2ab(wbout)],{type:""}), filename);
	     }
}(window.ExportExcel = window.ExportExcel || {}));
