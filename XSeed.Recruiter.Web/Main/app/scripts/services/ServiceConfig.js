angular.module('services-config', [])
    .constant('configuration', {
        //Dev-
        //GET_METADATA_API_URL: 'data/MetaData.json',
        //XSEED_LOGIN_API_URL: 'http://10.33.2.176:8100/API/Dev/token',
        //XSEED_API_URL: 'http://10.33.2.176:8100/API/Dev/api/',

        ///*Constants*/
        //XSEED_ORG_IMAGE_PATH: 'http://10.33.2.176:8100/API/Dev/Documents/Images/Organization/',
        //XSEED_COMPANY_IMAGE_PATH: 'http://10.33.2.176:8100/API/Dev/Documents/Images/Company/',
        //XSEED_ORGUSER_IMAGE_PATH: 'http://10.33.2.176:8100/API/Dev/Documents/Images/User/',
        //XSEED_COMPANYCONTACT_IMAGE_PATH: 'http://10.33.2.176:8100/API/Dev/Documents/Images/CompanyContact/',
        //XSEED_CANDIDATE_IMAGE_PATH: 'http://10.33.2.176:8100/API/Dev/Documents/Images/Candidate/',
        //XSEED_COMPANYDATA_IMAGE_PATH: 'http://10.33.2.176:8100/API/Dev/Documents/Images/CompanyData/',
        //XSEED_DEFAULT_IMAGE_PATH: 'http://10.33.2.176:8100/web/dev/Images/',      
        //XSEED_NO_IMAGE_PATH: 'http://10.33.2.176:8100/web/dev/Images/noImage.jpg',
        //XSEED_NO_IMAGE_COMPANY_PATH: 'http://10.33.2.176:8100/web/dev/images/noImage_company.png',
        //XSEED_RESUME_PATH: 'http://10.33.2.176:8100/Pdf/',
        //XSEED_DEFAULT_RESUME_CUSTOME_PATH: 'http://10.33.2.176:8100/Documents/Resumes/Local/'
        //XSEED_EMAIL_PARSER_EMAILID: 'requirements@tcognition.com',
        ///*Constants*/

        //TIMEOUT_UI_IN_SEC: '1200000'



        ////Staging-
        //GET_METADATA_API_URL: 'data/MetaData.json',
        //XSEED_LOGIN_API_URL: 'http://10.33.2.176:81/API/Staging/token',
        //XSEED_API_URL: 'http://10.33.2.176:81/API/Staging/api/',

        ///*Constants*/
        //XSEED_ORG_IMAGE_PATH: 'http://10.33.2.176:8100/API/Staging/Documents/Images/Organization/',
        //XSEED_COMPANY_IMAGE_PATH: 'http://10.33.2.176:8100/API/Staging/Documents/Images/Company/',
        //XSEED_ORGUSER_IMAGE_PATH: 'http://10.33.2.176:8100/API/Staging/Documents/Images/User/',
        //XSEED_COMPANYCONTACT_IMAGE_PATH: 'http://10.33.2.176:8100/API/Staging/Documents/Images/CompanyContact/',
        //XSEED_CANDIDATE_IMAGE_PATH: 'http://10.33.2.176:8100/API/Staging/Documents/Images/Candidate/',
        //XSEED_COMPANYDATA_IMAGE_PATH: 'http://10.33.2.176:8100/API/Staging/Documents/Images/CompanyData/',
        //XSEED_DEFAULT_IMAGE_PATH: 'http://10.33.2.176:8100/web/Staging/images/',
        //XSEED_NO_IMAGE_PATH: 'http://10.33.2.176:8100/web/Staging/images/noImage.jpg',
        //XSEED_NO_IMAGE_COMPANY_PATH: 'http://10.33.2.176:8100/web/Staging/images/noImage_company.png',      
        //XSEED_RESUME_PATH: 'http://10.33.2.176:8100/Pdf/',
        //XSEED_EMAIL_PARSER_EMAILID: 'requirements@tcognition.com',
        ///*Constants*/

        //TIMEOUT_UI_IN_SEC: '1200'



        //Local-
        GET_METADATA_API_URL: 'data/MetaData.json',
        XSEED_LOGIN_API_URL: 'http://localhost:51655/token',
        XSEED_API_URL: 'http://localhost:51655/api/',

        /*Constants*/
        XSEED_ORG_IMAGE_PATH: 'http://localhost:51655/Documents/Images/Organization/',
        XSEED_COMPANY_IMAGE_PATH: 'http://localhost:51655/Documents/Images/Company/',
        XSEED_ORGUSER_IMAGE_PATH: 'http://localhost:51655/Documents/Images/User/',
        XSEED_COMPANYCONTACT_IMAGE_PATH: 'http://localhost:51655/Documents/Images/CompanyContact/',
        XSEED_CANDIDATE_IMAGE_PATH: 'http://localhost:51655/Documents/Images/Candidate/',
        XSEED_COMPANYDATA_IMAGE_PATH: 'http://localhost:51655/Documents/Images/CompanyData/',
        XSEED_NO_IMAGE_PATH: 'http://localhost:63086/app/images/noImage.png',
        XSEED_NO_IMAGE_COMPANY_PATH: 'http://localhost:63086/app/images/noImage_company.png',
        XSEED_DEFAULT_IMAGE_PATH: 'http://localhost:63086/app/images/',
        XSEED_RESUME_PATH: 'http://localhost:63086/Pdf/',
        XSEED_EMAIL_PARSER_EMAILID: 'requirements@tcognition.com',
        XSEED_HOME_LINK: 'http://localhost:63086/app/#/',
        XSEED_CANDIDATE_LOGIN_LINK: 'http://localhost:63087/app/#/',
        XSEED_USER_VALID_TIME_IN_SEC: 60000,
        TIMEOUT_UI_IN_SEC: '1200000',

        XSEED_DEFAULT_RESUME_CUSTOME_PATH: 'http://localhost:51655/API/Documents/Resumes/Local/'
    });
