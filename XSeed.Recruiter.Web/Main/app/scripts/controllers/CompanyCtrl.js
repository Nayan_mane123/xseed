﻿'use strict';

XSEED_APP.controller('CompanyCtrl', CompanyCtrl);

/**
 * @ngInject
 */
function CompanyCtrl($scope, $rootScope, $route, $routeParams, RequestContext, $timeout, $q, $location, $window, configuration, i18nFactory, ExceptionHandler, EventBus, $log, _, ipCookie, ModalFactory, XSeedAlert, localStorageService, $uibModal, XSeedApiFactory, ngTableParams, $filter, ValidationService, $http, blockUI, blockUIConfig, Constants) {

    //blockUIConfig.autoBlock = false;

    $scope.common.companyModel = new API.CompanyRequest();
    $scope.common.createCompanyContactRequest = new API.CompanyContactRequest();
    //$scope.editCompanyRequest = new API.CompanyRequest();
    $scope.onLoad;
    $scope.companySourceList = {};
    $scope.selected = {};
    $scope.flag.advanceSearchFlag = false;
    $scope.common.maxProfileImgFileSize = false;
    $scope.common.invalidProfileImgFile = false;
    $scope.common.minProfileImgFileSize = false;
    init();

    //Initialization Function
    function init() {
        if ($scope.isUndefinedOrNull($scope.common.companyModel.Id) || $scope.isEmpty($scope.common.companyModel.Id)) {
            $location.path('companyList');
        }
        $scope.onLoad = true;
        $scope.common.companyModel.manipulationMode = "Create";
        //$scope.maxDate = new Date().toString();
        var date = new Date();
        $scope.maxDate = ("0" + (date.getMonth() + 1).toString()).substr(-2) + "/" + ("0" + date.getDate().toString()).substr(-2) + "/" + (date.getFullYear().toString());
        //$scope.minDate = ("0" + (date.getMonth() + 1).toString()).substr(-2) + "/" + ("0" + date.getDate().toString()).substr(-2) + "/" + (date.getFullYear().toString());

    };


    /* Company module start */
    $scope.getCompanyListWithFilter = function() {
        blockUI.start();

        var organizationId = $rootScope.userDetails.OrganizationId;
        //var organizationId = 2;
        $scope.getCompanyListByOrganization(organizationId);
        if (!$scope.onLoad && $scope.common.companyModel.Id) {
            $scope.companyDetailView(organizationId, $scope.common.companyModel.Id);
        } else {
            $scope.onLoad = true;
        }
        blockUI.stop();
    };

    //Create new company source in angular-selectors options
    $scope.createFunction = function(input) {
        // format the option and return it
        $scope.common.companyModel.companySource = { 'Name': input };
        $scope.createCompanySource($scope.common.companyModel.companySource);
    };

    $scope.getCompanyListByOrganization = function(ForType) {

        if ($rootScope.userHasToken == false) {
            $location.path('login');
            return false;
        }
        $scope.filter.$ = "";

        var createCompanyURL = '/createCompany';
        var editCompanyURL = '/editCompany';
        var companyListURL = '/companyList';
        var companyDetailURL = '/companyDetail';
        var createContactURL = '/createContact';
        var editContactURL = '/editContact';

        var dashboardURL = '/dashboard';

        if (($location.path().toLowerCase() == createCompanyURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Company_Create == "False") {
            $location.path('dashboard');
            return false;
        }

        if (($location.path().toLowerCase() == editCompanyURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Company_Update == "False") {
            $location.path('dashboard');
            return false;
        }

        if (($location.path().toLowerCase() == companyListURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Company_Read == "False") {
            $location.path('dashboard');
            return false;
        }
        if (($location.path().toLowerCase() == companyDetailURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Company_Read == "False") {
            $location.path('dashboard');
            return false;
        }

        if (($location.path().toLowerCase() == createContactURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Company_Create == "False") {
            $location.path('dashboard');
            return false;
        }

        if (($location.path().toLowerCase() == editContactURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Company_Update == "False") {
            $location.path('dashboard');
            return false;
        }

        blockUI.start();
        var organizationId = $rootScope.userDetails.OrganizationId;

        var firstPageFlag = 0;
        $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
        if (ForType == 'create') {
            $scope.common.pageSizeShow = 0;
        }
        $scope.common.pageNumberShow = undefined;
            var promise = XSeedApiFactory.getCompanyList(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow);

            promise.then(
                function (response) {

                    $scope.companyList = response[0].data.companies;
                    console.log($scope.companyList);

                    $scope.companyList11 = response[0].data.companies;
                    $scope.common.totalCount = response[0].data.TotalCount;
                    $scope.common.totalPages = response[0].data.totalPages;

                    firstPageFlag = 1;
                    populateCompanyList(organizationId, $scope.companyList, $scope.common.totalCount, $scope.common.totalPages, firstPageFlag);
                        //populateCompanyList();
                    blockUI.stop();
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Company', httpError.data.Status, httpError.data.Message);
                });
    };

    $scope.InitializeAutocompleteTypeahead = function() {


        return $scope.companyList11;
    }

    function populateCompanyList(organizationId, companyList, totalCount, totalPages, pageFlag) {
        if (angular.isDefined($scope.tblCompanyList)) {
            $scope.tblCompanyList.reload();
        } else {
            $scope.tblCompanyList = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
           }, {
                counts: $scope.common.ngTablePaginationCount,
                total: totalCount,
                getData: function($defer, params) {
                    var sort, sortorder;
                 
                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();
                    
                    if (pageFlag != 1) {
                        /* sorting */
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function(val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */

                        var promise = XSeedApiFactory.getCompanyList(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder);
                        promise.then(
                            function (response) {
                                
                                $scope.companyList = response[0].data.companies;
                                $scope.common.totalCount = response[0].data.TotalCount;
                                $scope.common.totalPages = response[0].data.totalPages;
                                blockUIConfig.autoBlock = false;

                                blockUI.stop();

                            },
                            function(httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('CompanyList', httpError.data.Status, httpError.data.Message);
                            });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.companyList);
                },
                $scope: $scope
            });
        }

    };

    $scope.getCompanyLogoPath = function(imageName) {
        var logoImagePath;
        if (!$scope.isUndefinedOrNull(imageName) && !$scope.isEmpty(imageName)) {
            logoImagePath = configuration.XSEED_COMPANY_IMAGE_PATH + imageName;
        } else {
            logoImagePath = 'images/noImage_company.png';
        }

        return logoImagePath;
    };

    $scope.$watch("filter.$", function() {
        if (angular.isDefined($scope.tblCompanyList) && ($scope.filter.$ != "" || $scope.filter.$ != null)) {
            $scope.tblCompanyList.reload();
            $scope.itemTableParams.page(1);
        }
    });


    $scope.enableButtonProfile = function(profileImage, module) {
        if (profileImage) {
            if (module == 'company')
                $scope.common.companyModel.ProfileImageFile = null;

        }
        if (module == 'createContact') {
            $scope.common.createCompanyContactRequest.ProfileImageFile = null;
        }
        if (module == 'editContact') {
            $scope.common.contactDetail.ProfileImageFile = null;
        }
        $scope.common.maxProfileImgFileSize = false;
        $scope.common.invalidProfileImgFile = false;
    }

    $scope.createCompanyView = function() {
        $scope.companyDetailMaster = angular.copy($scope.common.companyModel);
        $scope.common.companyModel = {};
        $scope.common.maxProfileImgFileSize = false;
        $scope.common.invalidProfileImgFile = false;
        $scope.common.companyModel.manipulationMode = 'Create';
        $location.path('createCompany');
    }


    $scope.createCompanyProcess = function(createCompanyForm) {
        if (new ValidationService().checkFormValidity(createCompanyForm) && !$scope.common.maxProfileImgFileSize && !$scope.common.invalidProfileImgFile && !$scope.common.minProfileImgFileSize) {
            $scope.common.maxProfileImgFileSize = false;
            $scope.common.invalidProfileImgFile = false;
            $scope.common.minProfileImgFileSize = false;
            blockUI.start();
            $scope.createCompanyFormSubmitted = false;
            $scope.common.companyModel.OrganizationId = $rootScope.userDetails.OrganizationId;
            $scope.common.companyModel.CompanySourceId = $scope.common.companyModel.CompanySourceId;
            var list = [];
            for (var i = 0; i < $scope.common.companyModel.IndustryTypeIdList.length; i++) {
                var o = _.find($scope.metadata.GetAncillaryInformationResponse.industryTypeList.industryType, function(o) { return o.Id == $scope.common.companyModel.IndustryTypeIdList[i]; });
                list.push(o);
            }
            $scope.common.companyModel.IndustryTypeList = list;

            //$scope.common.companyModel.companySourceName = $scope.common.companyModel.CompanySources.Name;



            if ($scope.common.companyModel.ProfileImageFile != null)
                $scope.common.companyModel.ProfileImage = $scope.common.companyModel.ProfileImageFile.name;

            var promise = XSeedApiFactory.createCompany($scope.common.companyModel);
            promise.then(
                function(response) {
                    blockUI.stop();
                    XSeedAlert.swal({
                        title: 'Success!',
                        text: 'Company created successfully!',
                        type: "success",
                        customClass: "xseed-error-alert",
                        allowOutsideClick: false
                    }).then(function() {
                        $timeout(function() {
                            $scope.common.companyModel.Id = response[0].data;
                            blockUI.stop();
                            $scope.common.createCompanyContactRequest = {};
                            $scope.common.createCompanyContactRequest.Mode = 'Create';
                            $location.path('createContact');
                        }, 0);
                    }, function(dismiss) {
                        blockUI.stop();
                    });
                },
                function(httpError) {
                    if (httpError.data.Message == "Unsupported Media Type.") {
                        httpError.data.Message = "Plese select valid file from .jpg, .jpeg, .png or .bmp";
                    }
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Company', httpError.data.Status, httpError.data.Message);
                });
        };
    };



    //$scope.$on('masterData', function (event, masterData) {
    //    if (masterData.masterType === Constants.Masters['CompanySource']) {
    //        $scope.common.companyModel.CompanySourceId = masterData.masterId;
    //        $scope.common.companyModel.companySourceName = masterData.masterValue;
    //    }
    //    $scope.closeGenericMasterModal();
    //});


    $scope.companyDetailView = function (organizationId, companyId, location) {
        $(".tooltip").tooltip("hide");
        blockUI.start();
        $scope.companyId = companyId;
        $scope.common.createCompanyContactRequest.Mode = "Create";
        //$scope.common.companyModel.companyId = companyId;

        var promise = XSeedApiFactory.getCompanyDetail(organizationId, companyId);
        promise.then(
            function(response) {
                $scope.common.companyModel = response[0].data;
                console.log($scope.common.companyModel);
                var list = [];
                if ($scope.common.companyModel.IndustryTypeList) {
                    for (var i = 0; i < $scope.common.companyModel.IndustryTypeList.length; i++) {
                        list.push($scope.common.companyModel.IndustryTypeList[i].Id);
                    }
                    $scope.common.companyModel.IndustryTypeIdList = list;
                    $scope.common.companyModel.logoImagePath = $scope.getCompanyLogoPath($scope.common.companyModel.ProfileImage);


                    if (!$scope.isUndefinedOrNull($scope.common.companyModel.CountryId) && !$scope.isEmpty($scope.common.companyModel.CountryId)) {
                        $scope.getStateListByCountry($scope.common.companyModel.CountryId);
                    }

                    if (!$scope.isUndefinedOrNull($scope.common.companyModel.StateId) && !$scope.isEmpty($scope.common.companyModel.StateId)) {
                        $scope.getCityListByState($scope.common.companyModel.StateId);
                    }
                    $scope.common.companyModel.CompanyId = $scope.common.companyModel.Id;
                }
                $scope.buildIndustryStructuredData();
                $scope.common.companyModel.companySource = $scope.common.companyModel.CompanySourceName;
                $scope.common.createCompanyContactRequest.CompanyId = $scope.common.companyModel.Id;
                $scope.getCompanyContacts();
                $location.path(location);
                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Company', httpError.data.Status, httpError.data.Message);
            });
    };


    $scope.companyDirectEditView = function(organizationId, companyId) {
        blockUI.start();
        $scope.common.companyModelMaster = angular.copy($scope.common.companyModel);
        $scope.buildIndustryStructuredData();
        var promise = XSeedApiFactory.getCompanyDetail(organizationId, companyId);
        promise.then(
            function(response) {
                $scope.common.companyModel = response[0].data;
                $scope.common.companyModel.IndustryTypeIdList = {};
                var list = [];
                for (var i = 0; i < $scope.common.companyModel.IndustryTypeList.length; i++) {
                    list.push($scope.common.companyModel.IndustryTypeList[i].Id);
                }
                $scope.common.companyModel.IndustryTypeIdList = list;

                $scope.common.companyModel.logoImagePath = $scope.getCompanyLogoPath($scope.common.companyModel.ProfileImage);

                if (!$scope.isUndefinedOrNull($scope.common.companyModel.CountryId) && !$scope.isEmpty($scope.common.companyModel.CountryId)) {
                    $scope.getStateListByCountry($scope.common.companyModel.CountryId);
                }

                if (!$scope.isUndefinedOrNull($scope.common.companyModel.StateId) && !$scope.isEmpty($scope.common.companyModel.StateId)) {
                    $scope.getCityListByState($scope.common.companyModel.StateId);
                }

                $scope.common.companyModel.manipulationMode = "Edit";
                $location.path('editCompany');
                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Company', httpError.data.Status, httpError.data.Message);
            });
    };


    $scope.companyEditView = function() {
        blockUI.start();
        $scope.companyDetailMaster = angular.copy($scope.common.companyModel);
        $scope.common.companyModel.manipulationMode = "Edit";
        $scope.common.companyModel.companySourceName = $scope.companyDetailMaster.companySource;
        $scope.common.maxProfileImgFileSize = false;
        $scope.common.invalidProfileImgFile = false;
        $location.path('editCompany');
        blockUI.stop();
    }


    $scope.editCompanyProcess = function(createCompanyForm) {
        if (new ValidationService().checkFormValidity(createCompanyForm) && !$scope.common.maxProfileImgFileSize && !$scope.common.invalidProfileImgFile && !$scope.common.minProfileImgFileSize) {
            $scope.companyDetailMaster = angular.copy($scope.common.companyModel);
            $scope.common.maxProfileImgFileSize = false;
            $scope.common.invalidProfileImgFile = false;
            $scope.common.minProfileImgFileSize = false;
            blockUI.start();
            $scope.common.companyModel.OrganizationId = $rootScope.userDetails.OrganizationId;
            //$scope.common.companyModel.OrganizationId = 2;

            var list = [];
            for (var i = 0; i < $scope.common.companyModel.IndustryTypeIdList.length; i++) {
                var o = _.find($scope.metadata.GetAncillaryInformationResponse.industryTypeList.industryType, function(o) { return o.Id == $scope.common.companyModel.IndustryTypeIdList[i]; });
                list.push(o);
            }
            $scope.common.companyModel.IndustryTypeList = list;

            //if ($scope.common.companyModel.companySource) {
            //    $scope.common.companyModel.CompanySourceId = $scope.common.companyModel.companySource;
            //}

            if ($scope.common.companyModel.ProfileImageFile != null)
                $scope.common.companyModel.ProfileImage = $scope.common.companyModel.ProfileImageFile.name;

            var promise = XSeedApiFactory.editCompany($scope.common.companyModel);
            promise.then(
                function(response) {
                    //$scope.companyDetailMaster = angular.copy($scope.common.companyModel);                  
                    blockUI.stop();
                    XSeedAlert.swal({
                        title: 'Success!',
                        text: 'Company details updated successfully!',
                        type: "success",
                        customClass: "xseed-error-alert",
                        allowOutsideClick: false
                    }).then(function() {
                        $timeout(function() {
                            $scope.common.companyModel = angular.copy($scope.companyDetailMaster);
                            $scope.companyDetailView($scope.common.companyModel.OrganizationId, $scope.common.companyModel.Id);
                            //$location.path('companyList');
                            $location.path('companyDetail');
                        }, 0);
                    }, function(dismiss) {
                        blockUI.stop();
                    });
                },
                function(httpError) {
                    if (httpError.data.Message == "Unsupported Media Type.") {
                        httpError.data.Message = "Plese select valid file from .jpg, .jpeg, .png or .bmp";
                    }
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Company', httpError.data.Status, httpError.data.Message);
                });
        };
    };


    $scope.resetContact = function() {
        $scope.common.contactDetail = angular.copy($scope.common.contactDetail);
        $scope.common.invalidProfileImgFile = false;
        $scope.common.maxProfileImgFileSize = false;
        $scope.common.minProfileImgFileSize = false;
        $location.path('companyDetail');
        $scope.common.createCompanyContactRequest = new API.CompanyContactRequest();
        $scope.common.createCompanyContactRequest.CompanyId = $scope.common.companyModel.Id;
        $scope.common.createCompanyContactRequest.Mode = 'Create';
        //$scope.companyDetailView($scope.common.contactDetail.OrganizationId, $scope.common.contactDetail.Id);
    };

    $scope.cancelCompany = function() {
        if ($scope.companyDetailMaster.Id) {
            $scope.common.companyModel = angular.copy($scope.companyDetailMaster);
        } else {
            $scope.common.companyModel.Id = "";
        }

        // Task Number 932 - Validation msg keeps in Cache
        $scope.common.maxProfileImgFileSize = false;
        $scope.common.invalidProfileImgFile = false;
        $scope.common.minProfileImgFileSize = false;

        $location.path('companyList');
    };

    $scope.createCompanySource = function(comapnySources) {
        var promise = XSeedApiFactory.createCompanySource(comapnySources, $rootScope.userDetails.OrganizationId);
        promise.then(
            function(response) {
                $scope.getCompanySourceList();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Company', httpError.data.Status, httpError.data.Message);
            });
    }

    $scope.getCompanySourceList = function() {
        var promise = XSeedApiFactory.getCompanySourceList($rootScope.userDetails.OrganizationId);
        promise.then(
            function(response) {
                var list = [];
                angular.forEach(response[0].data, function(value, key) {
                    list.push({ 'value': value.Id, 'label': value.Name });
                });
                $scope.common.companyModel.companySourceList = list;
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
            });
    }

    $scope.getIndustryString = function(list) {
        var string = '';
        for (var i = 0; i < list.length; i++) {
            string = string + list[i].Name;
            if (i != (list.length - 1)) {
                string = string + ', ';
            }
        }

        return string;
    }

    $scope.companyCancelDetail = function () {
       

        $scope.common.uploadFileName = null;
        $location.path('companyList');
    };


    /* Company contacts module start */
    $scope.$watch("filter.$", function() {
        if (angular.isDefined($scope.tblCompanyContact) && ($scope.filter.$ != "" || $scope.filter.$ != null)) {
            $scope.tblCompanyContact.reload();
            $scope.tblCompanyContact.page(1);
        }
    });



    $scope.openCompanyContactModal = function(index) {
        $scope.common._dataCompanyContact = $scope.dataCompanyContact[index];
        $scope.common.companyContactModal.$promise.then($scope.common.companyContactModal.show);
    }

    $scope.getCompanyContacts = function() {
        blockUI.start();
        if ($scope.companyId == undefined) {
            $location.path('/companyList');
            blockUI.stop();
            return;
        }

        var promise = XSeedApiFactory.getCompanyContacts($scope.companyId);
        promise.then(
            function(response) {
                blockUI.stop();
                $scope.CompanyContacts = response[0].data;

                $scope.detailContactView($scope.CompanyContacts[0].Id, $scope.CompanyContacts[0].CompanyId);
                populateCompanyContactList();

            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('CompanyContact', httpError.data.Status, httpError.data.Message);
            });
    };

    function populateCompanyContactList() {
        if (angular.isDefined($scope.tblCompanyContact)) {
            $scope.tblCompanyContact.reload();
        } else {
            $scope.tblCompanyContact = new ngTableParams({
                page: 1,
                count: 10,
                sorting: {}
            }, {
                counts: [],
                total: $scope.CompanyContacts.length,
                getData: function($defer, params) {
                    var filteredData = $filter('filter')($scope.CompanyContacts, $scope.filter);
                    $scope.dataCompanyContact = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
                    params.total($scope.dataCompanyContact.length);
                    $scope.dataCompanyContact = $scope.dataCompanyContact.slice((params.page() - 1) * params.count(), params.page() * params.count());

                    $defer.resolve($scope.dataCompanyContact);
                },
                $scope: $scope
            });
        }
    };

    $scope.createContact = function(contact) {
        if (new ValidationService().checkFormValidity(contact) && !$scope.common.maxProfileImgFileSize && !$scope.common.invalidProfileImgFile && !$scope.common.minProfileImgFileSize) {
            $scope.common.minProfileImgFileSize = false;
            $scope.frmCreateContact = true;
            if (contact.$valid) {
                blockUI.start();
                $scope.frmCreateContact = false;
                $scope.common.createCompanyContactRequest.companyId = $scope.common.companyModel.Id;
                $scope.common.createCompanyContactRequest.BirthDate = $filter('date')($scope.common.createCompanyContactRequest.BirthDate, "yyyy-MM-dd");
                $scope.common.createCompanyContactRequest.AnniversaryDate = $filter('date')($scope.common.createCompanyContactRequest.AnniversaryDate, "yyyy-MM-dd");
                var promise = XSeedApiFactory.createContact($scope.common.createCompanyContactRequest);
                promise.then(
                    function(response) {
                        blockUI.stop();
                        XSeedAlert.swal({
                            title: 'Success!',
                            text: 'Contact created successfully!',
                            type: "success",
                            customClass: "xseed-error-alert",
                            allowOutsideClick: false
                        }).then(function() {
                            $timeout(function() {
                                $scope.common.createCompanyContactRequest = new API.CompanyContactRequest();
                                $scope.common.createCompanyContactRequest.CompanyId = $scope.common.companyModel.Id;
                                //$location.path('companyList');
                                $scope.getCompanyContacts();
                                $location.path('companyDetail');
                            }, 0);
                        }, function(dismiss) {
                            blockUI.stop();

                        });

                    },

                    function(httpError) {
                        if (httpError.data.Message == "Unsupported Media Type.") {
                            httpError.data.Message = "Plese select valid file from .jpg, .jpeg, .png or .bmp";
                        }
                        blockUI.stop();
                        ExceptionHandler.handlerHTTPException('CompanyContact', httpError.data.Status, httpError.data.Message);
                    });
            }
        }
    };

    $scope.updateContact = function(contact) {
        if (new ValidationService().checkFormValidity(contact) && !$scope.common.maxProfileImgFileSize && !$scope.common.invalidProfileImgFile && !$scope.common.minProfileImgFileSize) {
            $scope.common.minProfileImgFileSize = false;
            $scope.frmUpdateContact = true;
            if (contact.$valid) {

                blockUI.start();
                $scope.frmUpdateContact = false;
                if ($scope.common.contactDetail.ProfileImageFile != null)
                    $scope.common.contactDetail.ProfileImage = $scope.common.contactDetail.ProfileImageFile.name;

                $scope.common.contactDetail.BirthDate = $filter('date')($scope.common.contactDetail.BirthDate, "MM/dd/yyyy");
                $scope.common.contactDetail.AnniversaryDate = $filter('date')($scope.common.contactDetail.AnniversaryDate, "MM/dd/yyyy");

                var promise = XSeedApiFactory.updateContact($scope.common.contactDetail);
                promise.then(
                    function(response) {
                        blockUI.stop();
                        XSeedAlert.swal({
                            title: 'Success!',
                            text: 'Contact details updated successfully!',
                            type: "success",
                            customClass: "xseed-error-alert",
                            allowOutsideClick: false
                        }).then(function() {
                            $timeout(function() {
                                //$location.path('companyList');
                                $scope.getCompanyContacts();
                                $location.path('companyDetail');
                            }, 0);
                        }, function(dismiss) {
                            blockUI.stop();

                        });
                    },
                    function(httpError) {
                        if (httpError.data.Message == "Unsupported Media Type.") {
                            httpError.data.Message = "Plese select valid file from .jpg, .jpeg, .png or .bmp";
                        }
                        blockUI.stop();
                        ExceptionHandler.handlerHTTPException('CompanyContact', httpError.data.Status, httpError.data.Message);
                    });
            }
        }
    };

    $scope.editContactView = function(contactId, companyId) {
        var promise = XSeedApiFactory.getContactDetail(contactId, companyId);
        promise.then(
            function(response) {
                blockUI.stop();
                $scope.common.contactDetail = response[0].data;
                if ($scope.common.contactDetail.CountryId) {
                    $scope.getStateListByCountry($scope.common.contactDetail.CountryId);
                }

                if ($scope.common.contactDetail.StateId) {
                    $scope.getCityListByState($scope.common.contactDetail.StateId);
                }

                $scope.common.contactDetail.BirthDate = $filter('date')($scope.common.contactDetail.BirthDate, 'MM/dd/yyyy');
                $scope.common.contactDetail.AnniversaryDate = $filter('date')($scope.common.contactDetail.AnniversaryDate, 'MM/dd/yyyy');
                $scope.common.contactDetail.imagePath = configuration.XSEED_COMPANYCONTACT_IMAGE_PATH + $scope.common.contactDetail.ProfileImage;

                $scope.common.createCompanyContactRequest = $scope.common.contactDetail;
                $scope.common.createCompanyContactRequest.Mode = 'Edit';
                $location.path('editContact');
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('CompanyContact', httpError.data.Status, httpError.data.Message);
            });
    };

    $scope.detailContactView = function(contactId, companyId) {

        var promise = XSeedApiFactory.getContactDetail(contactId, companyId);
        promise.then(
            function(response) {
                blockUI.stop();
                $scope.common.contactDetail = response[0].data;

                if ($scope.common.contactDetail.CountryId) {
                    $scope.getStateListByCountry($scope.common.contactDetail.CountryId);
                }

                if ($scope.common.contactDetail.StateId) {
                    $scope.getCityListByState($scope.common.contactDetail.StateId);
                }

                $scope.common.contactDetail.BirthDate = $filter('date')($scope.common.contactDetail.BirthDate, 'MM/dd/yyyy');
                $scope.common.contactDetail.AnniversaryDate = $filter('date')($scope.common.contactDetail.AnniversaryDate, 'MM/dd/yyyy');
                $scope.common.contactDetail.imagePath = configuration.XSEED_COMPANYCONTACT_IMAGE_PATH + $scope.common.contactDetail.ProfileImage;
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('CompanyContact', httpError.data.Status, httpError.data.Message);
            });
    };
    /* Company contacts module end */

    $scope.companySearchModifiedProcess = function(search) {
        if (!search) return $q.resolve([]);
        blockUIConfig.autoBlock = false;

        var promise = XSeedApiFactory.doCompanySearch(search);

        console.log(promise.value.data)

        return promise;
    };

    $scope.companyCancel = function () {
        XSeedAlert.swal({
            title: "Are you sure you want to cancel?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function () {
            blockUI.start();
            $scope.common.companyModel.manipulationMode = "Edit";
            setTimeout(function () {
                $scope.resetCandidateAdvanceSearch();
            }, 100);
            $location.path('companyList');
            blockUI.stop();
        }, function (dismiss) { });
    };


    $scope.copySelectedCompanyData = function(selectedCompany) {
            console.log(selectedCompany)
            $scope.selected.company = selectedCompany;
            if (!$scope.isUndefinedOrNull($scope.selected.company)) {
                $scope.common.companyModel = angular.copy($scope.selected.company);
                if ($scope.common.companyModel.country != null) {
                    $scope.common.companyModel.CountryId = $scope.getCountryIdByName($scope.common.companyModel.country)

                    if ($scope.common.companyModel.locality != null) {
                        blockUI.start();
                        $scope.common.companyModel.locality = $scope.common.companyModel.locality.split(",")[0];
                        var promise = XSeedApiFactory.getCityStateCode($scope.common.companyModel.country, $scope.common.companyModel.locality);
                        promise.then(
                            function(response) {
                                blockUI.stop();
                                $scope.common.companyModel.StateId = response[0].data.StateId;
                                $scope.common.companyModel.CityId = response[0].data.CityId;
                                if ($scope.common.companyModel.CountryId) {
                                    $scope.getStateListByCountry($scope.common.companyModel.CountryId);
                                }
                                if ($scope.common.companyModel.StateId) {
                                    $scope.getCityListByState($scope.common.companyModel.StateId);
                                }
                            },
                            function(httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('Company', httpError.data.Status, httpError.data.Message);
                            });
                    }
                }
                //$scope.common.companyModel = {};
                //alert('Hit');
            }
        }
        /* Company search module end */


    //***************************************************************************************//

    // Get the render context local to this controller (and relevant params).
    var renderContext = RequestContext.getRenderContext("standard.company");
    // The subview indicates which view is going to be rendered on the page.
    $scope.subview = renderContext.getNextSection();
    $scope.$on(
        "requestContextChanged",
        function() {
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {
                return;
            }
            // Update the view that is being rendered.
            $scope.subview = renderContext.getNextSection();
        }
    );
};