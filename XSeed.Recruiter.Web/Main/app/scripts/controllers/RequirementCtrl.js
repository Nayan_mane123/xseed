'use strict';

XSEED_APP.controller('RequirementCtrl', RequirementCtrl);
XSEED_APP.constant('Constants', {
    Masters: {
        RequirementTitle: 'Requirement Title',
        RequirementType: 'Requirement Type',
        BusinessUnit: 'Business Unit',
        Degree: 'Degree'
    }
});
/**
 * @ngInject
 */
function RequirementCtrl($scope, $rootScope, $route, $routeParams, RequestContext, $timeout, $q, $location, $window, configuration, i18nFactory, ExceptionHandler, EventBus, $log, _, ipCookie, blockUI, ModalFactory, XSeedAlert, localStorageService, $uibModal, XSeedApiFactory, ngTableParams, $filter, ValidationService, $compile, Constants) {

    //$scope.common.jobList = [];
    $scope.skillList = [];
    $scope.common.requirementModel = new API.JobRequest();
    $scope.requirementFilter = {};
    $scope.requirementSearch = {};
    $scope.flag.advanceSearchFlag = false;
    $scope.candidateAdvanceSearchFlag = false;
    $scope.requirementSearchData = "";
    $scope.requirementOptions = {};
    $scope.requirementOptions.JobStatus = 'All';
    $scope.qNoteModel = {};
    $scope.showAddNewBusinessUnit = false;
    $scope.filterItemCount = 0;
    $scope.filterItemLable = [];
    $scope.genericLookup = {};
    $scope.common.advanceSearchModel = {};
    $scope.common.redirectedFrom = 'RequirementList';
    $scope.duplicateRequisitionId = false;
    $scope.checkedCount = 0;
    $scope.isInitRequirement = false;
    //$scope.common.requirementStatusFilter = "MyOpen";
    var submitCandidateFromRequirementModal = ModalFactory.submitCandidateFromRequirementModal($scope);
    var candidateSubmissionModal = ModalFactory.candidateSubmissionModal($scope);
    var requirementActivityQnotesModal = ModalFactory.requirementActivityQnotesModal($scope);
    var requirementActivityPreviewModal = ModalFactory.requirementActivityPreviewModal($scope);
    var requirementShareModal = ModalFactory.requirementShareModal($scope);
    init();

    //Initialization Function
    function init() {
        $scope.filterItemCount = 0;



        $scope.getLookUpSkill();
        var requirementQueueUrl = "/requirementQueue";
        if (($scope.isUndefinedOrNull($scope.common.requirementModel.Id) || $scope.isEmpty($scope.common.requirementModel.Id)) && ($location.path().toLowerCase() != requirementQueueUrl.toLowerCase())) {
            $location.path('requirementList');
        }

        //to avoid reload
        $scope.common.tblRequirementList = undefined;
        $scope.showPayScaleSalryCalculator = false;
        var date = new Date();
        $scope.maxDate = ("0" + (date.getMonth() + 1).toString()).substr(-2) + "/" + ("0" + date.getDate().toString()).substr(-2) + "/" + (date.getFullYear().toString());
        $scope.minDate = ("0" + (date.getMonth() + 1).toString()).substr(-2) + "/" + ("0" + date.getDate().toString()).substr(-2) + "/" + (date.getFullYear().toString());
        $scope.closedDate = ("0" + (date.getMonth() + 1).toString()).substr(-2) + "/" + ("0" + date.getDate().toString()).substr(-2) + "/" + ((date.getFullYear() + 10).toString());
    };

    $scope.createFunction = function(input) {
        // format the option and return it
        return {
            value: $scope.lookup.skillListData.length,
            label: input
        };
    };

    //Create new company source in angular-selectors options
    $scope.createBusinessUnit = function() {
        // format the option and return it
        $scope.common.requirementModel.BusinessUnit = { 'Name': $scope.common.requirementModel.NewBusinessUnit };
        $scope.createNewBusinessUnit($scope.common.requirementModel.BusinessUnit);
        $scope.showAddNewBusinessUnit = false;
    };

    //send mail to company contact
    $scope.sendMailToAssignedCompanyContact = function(job) {

        $rootScope.commonEmailModel = {};
        $rootScope.commonEmailModel.MailSubject = job.JobTitle + " " + (job.RequisitionId == null ? "" : "(" + job.RequisitionId + ")");
        $rootScope.commonEmailModel.MailBody = "";
        $rootScope.commonEmailModel.Id = job.Id;
        $rootScope.commonEmailModel.typeOfNotification = $scope.constants.sendJobMail;
        $rootScope.commonEmailModel.FromEmailID = $rootScope.userDetails.UserName;

        if (job.CompanyContactName != null) {
            $rootScope.commonEmailModel.ToEmailID = [];
            $rootScope.commonEmailModel.ToEmailID.push(job.CompanyContactName);
            $rootScope.commonEmailModel.ToEmailID = $rootScope.commonEmailModel.ToEmailID.join();
        };
        job = null;
        $scope.common.commonEmailModal.$promise.then($scope.common.commonEmailModal.show);
    };

    $scope.createRequirementView = function() {

        blockUI.start();
        $scope.common.requirementModel = new API.JobRequest();
        $scope.common.requirementModel.manipulationMode = 'Create';
        //$scope.getRequisitionID();
        $scope.jobLookupProcess();
        angular.forEach($scope.lookup.jobStatus, function(val, index) {
            if (val.Name == 'Select Status') {
                $scope.common.requirementModel.JobStatusId = val.Id;
            }
        });
        $location.path('createRequirement');
        blockUI.stop();
    }

    $scope.getRequisitionID = function() {
        var organizationId = $rootScope.userDetails.OrganizationId;
        var promise = XSeedApiFactory.getRequisitionID(organizationId);
        promise.then(
            function(response) {
                $scope.common.requirementModel.RequisitionId = response[0].data;
                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });

      
    };
    $scope.checksalary = function() {
        
        var minValue = document.getElementById("minsalExample").value;
        var maxValue = document.getElementById("maxsalExample").value;
        if (minValue > maxValue) {
            alert("Minimum value should not be greater than Maximum value");
        }
        else {
            
        }
    };
    
    $scope.checkDuplicateRequirementIdNSaveJob = function(JobForm) {
        $scope.duplicateRequisitionId = false;
        var organizationId = $rootScope.userDetails.OrganizationId;
        //if (requisitionId) {
        //var promise = XSeedApiFactory.IsDuplicateRequisitionId(requisitionId, organizationId);
        //promise.then(
        //function (response) {
        //    $scope.duplicateRequisitionId = response[0].data;

        //if (($scope.common.requirementModel.manipulationMode == "Edit") && (requisitionId == $scope.common.requisitionId)) {
        //    $scope.duplicateRequisitionId = false;
        //}

        //if (!$scope.duplicateRequisitionId) {
        if ($scope.common.requirementModel.manipulationMode == "Create") {

            // console.log( $scope.common.requirementModel)
            //     if($scope.common.requirementModel.VisaTypeNameList){
            //   $scope.common.requirementModel.VisaTypeNameList=  $scope.common.requirementModel.VisaTypeNameList.join();
            // }
            // if($scope.common.requirementModel.SkillList){
            //   $scope.common.requirementModel.SkillList=  $scope.common.requirementModel.SkillList.join();
            // }
            // if($scope.common.requirementModel.DegreeNameList){
            //   $scope.common.requirementModel.DegreeNameList=  $scope.common.requirementModel.DegreeNameList.join();
            // }
            //console.log($scope.common.requirementModel.JobType)
            //console.log(JobForm);
            //return;

            $scope.createJobProcess(JobForm);
        }
        if ($scope.common.requirementModel.manipulationMode == "Edit") {
            $scope.editJobProcess(JobForm)
        }
        //}
        blockUI.stop();
        //},
        //function (httpError) {
        //    blockUI.stop();
        //    ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
        //}
        //)
        //;
        //}
        //else {
        //    $scope.duplicateRequisitionId = false;
        //}
    };

    $scope.initRequirementList = function() {
        if (!$scope.isInitRequirement) {
            $scope.getJobsByStatus('MyAll');
            $scope.isInitRequirement = true;
            $scope.getCountofJobsByStatus();
        } else {
            $scope.OpenFilter('filter');
            $scope.getSelectedFilterItem();
        }
    }
    
    // $scope.OpenFilter = function(type) {
    //     $("#div_filter").toggle(500);

    //     $("#btn_filter").toggle();
    //     $("#btn_clearfilter").toggle();

    //     if (type == 'clear') {
    //         // $(".select2").each(function() { //added a each loop here
    //         //     $(this).val("");
    //         //     $(this).trigger("change");
    //         // });
    //         $(".btn-filter").removeClass("active");
    //         $(".btn-myall").addClass("active");
    //     }
    //     setTimeout(function() {
    //         $(".select2").each(function() { //added a each loop here
    //             //  $(this).val("");
    //             //  $(this).trigger("change");
    //         });
    //     }, 500);
    // }

    $scope.getCountofJobsByStatus = function () {
        var organizationId = $rootScope.userDetails.OrganizationId;
        var userId = $rootScope.userDetails.UserId;
        $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
        $scope.common.pageNumberShow = undefined;

        var promise = XSeedApiFactory.getRequirementList(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow);
        promise.then(
            function(response) {
                $scope.common.Count1= response[0].data.TotalCount;
                console.log("All Count" + $scope.common.Count1)
            },
                function (httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                });

        var promise = XSeedApiFactory.getRequirementListByStatus(organizationId, userId, null, $scope.common.pageSizeShow, $scope.common.pageNumberShow);
            promise.then(
                   function (response) {
                       $scope.common.joblist = response[0].data.Jobs;
                       console.log($scope.common.joblist)
                       $scope.common.Count2 = response[0].data.TotalCount;
                       console.log("MyAll Count" + $scope.common.Count2);
                       var OpenCount = 0, ClosedCount=0,PendingCount = 0, HoldCount = 0 ;
                       angular.forEach($scope.common.joblist, function (val, index) {
                           if (val.JobStatus == 'Open') {
                               OpenCount = OpenCount + 1;
                           }
                           if (val.JobStatus == 'Closed') {
                               ClosedCount = ClosedCount + 1;
                           }
                           if (val.JobStatus == 'Pending') {
                               PendingCount = PendingCount + 1;         
                           } 
                           if (val.JobStatus == 'Hold') {
                               HoldCount = HoldCount + 1;
                           }
                       });
                       $scope.common.Count3 = OpenCount;
                       $scope.common.Count4 = ClosedCount;
                       $scope.common.Count5 = PendingCount;
                       $scope.common.Count6 = HoldCount;
                   },
                     function (httpError) {
                         blockUI.stop();
                         ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                     });
    }


    $scope.getJobsByStatus = function(value) {
        $scope.selectedStatus = value;
        /* #889 Requirement List page -> Filter requirement issue. System does not load the list as per filter option selection */
        if ($scope.common.requirementModel.manipulationMode == "Edit") {
            $scope.common.requirementModel.manipulationMode = undefined;
            value = $scope.common.requirementStatusFilter;
        }

        if (value === 'All') {
            //$scope.showPendingApprovalRequirement = false;
            $scope.getRequirementList();
        }
        if (value === 'MyAll') {
            if ($scope.flag.advanceSearchFlag) {
                $scope.flag.advanceSearchFlag = false;
                $scope.resetRequirementAdvanceSearch();
            }

            $scope.getRequirementListByStatus(null);
        }
        if (value === 'MyOpen') {
            if ($scope.flag.advanceSearchFlag) {
                $scope.flag.advanceSearchFlag = false;
                $scope.resetRequirementAdvanceSearch();
            }
            $scope.common.requirementStatusFilter = "MyOpen";
            $timeout(function() {
                $scope.getRequirementListByStatus('Open');
            }, 1000);
        }
        if (value === 'MyClosed') {
            if ($scope.flag.advanceSearchFlag) {
                $scope.flag.advanceSearchFlag = false;
                $scope.resetRequirementAdvanceSearch();
            }
            $scope.getRequirementListByStatus('Closed');
        }
        if (value === 'MyPending') {
            if ($scope.flag.advanceSearchFlag) {
                $scope.flag.advanceSearchFlag = false;
                $scope.resetRequirementAdvanceSearch();
            }
            $scope.getRequirementListByStatus('Pending');
        }
        if (value === 'MyHold') {
            if ($scope.flag.advanceSearchFlag) {
                $scope.flag.advanceSearchFlag = false;
                $scope.resetRequirementAdvanceSearch();
            }
            $scope.getRequirementListByStatus('Hold');
        }

    };

    $scope.GetValue = function(fruit) {
        var reqId = $scope.requirementFilter.select_filterUser;
        var requId = $scope.requirementFilter.select_filterStatus;
        var requiId = $scope.requirementFilter.select_filterType;
        var requirId = $scope.requirementFilter.select_filterCompany;
        //var scr = $sce.parseAsHtml("asdfasdf");
        //$window.alert("Selected Status: " + filterId + "Selected Status: " + filtId);
        if (reqId != 0 || requId != 0 || requiId != 0 || requirId != 0) {
            $scope.filterItemCount = 1;
        } else {
            $scope.filterItemCount = 0;
        }
    };
  
    $scope.getRequirementListByStatus = function(requirementStatus) {

        if ($rootScope.userHasToken == false) {
            $location.path('login');
            return false;
        }

        var createRquirementURL = '/createRequirement';
        var editRequirementURL = '/editRequirement';
        var requirementListURL = '/requirementList';
        var requirementDetailURL = '/requirementDetail';

        var dashboardURL = '/dashboard';

        if (($location.path().toLowerCase() == createRquirementURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Requirement_Create == "False") {
            $location.path('dashboard');
            return false;
        }
        if (($location.path().toLowerCase() == editRequirementURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Requirement_Update == "False") {
            $location.path('dashboard');
            return false;
        }

        if (($location.path().toLowerCase() == requirementListURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Requirement_Read == "False") {
            $location.path('dashboard');
            return false;
        }
        if (($location.path().toLowerCase() == requirementDetailURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Requirement_Read == "False") {
            $location.path('dashboard');
            return false;
        }

        if ($scope.flag.advanceSearchFlag) {
            return;
        }
        blockUI.start();
        $scope.jobLookupProcess();
        var firstPageFlag = 0;
        var organizationId = $rootScope.userDetails.OrganizationId;
        //var organizationId = "AAC777C7-75E2-468E-A8CA-0448BF4CC4D4";
        var userId = $rootScope.userDetails.UserId;
        $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
        $scope.common.pageNumberShow = undefined;
        if ($scope.common.requirementModel.manipulationMode != "Edit") {
            var promise = XSeedApiFactory.getRequirementListByStatus(organizationId, userId, requirementStatus, $scope.common.pageSizeShow, $scope.common.pageNumberShow);
            promise.then(
                function(response) {
                    $scope.common.jobList = response[0].data.Jobs;

                    console.log($scope.common.jobList);
                    $scope.common.totalCount = response[0].data.TotalCount;
                    $scope.common.totalPages = response[0].data.TotalPages;
                  
                    if (response[0].data.Jobs.length > 0) {
                        $scope.firstRequisitionId = $scope.common.jobList[0].RequisitionId;

                        //Manupulation of response
                        angular.forEach($scope.common.jobList, function(val, index) {
                            if ($scope.common.jobList[index].TechnicalSkills != null) {
                                $scope.common.jobList[index].TechnicalSkills = $scope.common.jobList[index].TechnicalSkills.split("|");
                                $scope.common.jobList[index].TechnicalSkills = $scope.common.jobList[index].TechnicalSkills.join();
                            }
                        });

                        angular.forEach($scope.common.jobList, function (val, index) {
                            $scope.common.jobList[index].count = $scope.common.jobList[index].AssociateCandidate.length;
                        });

                        //to select first row by default
                        $scope.setClickedRow($scope.common.jobList[0]);

                        firstPageFlag = 1;
                        populateRequirementListByStatus(organizationId, userId, $scope.common.jobList, $scope.common.totalCount, $scope.common.totalPages, firstPageFlag, requirementStatus);

                        //to show pending requirement count in badge on requirement list
                        if ($rootScope.userDetails.RoleName == 'Application Administrator' || $rootScope.userDetails.RoleName == 'Delivery Manager' || $rootScope.userDetails.RoleName == 'Team Lead') {
                            getVendorUnreadMailCount();
                        }
                    } else {
                        $scope.common.tblRequirementList = new ngTableParams({});
                    }
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                });
        }
        $timeout(function() {
            $scope.populateFilterLookupList();
        }, 2000);

        blockUI.stop();
    };

    function populateRequirementListByStatus(organizationId, userId, jobList, totalCount, totalPages, pageFlag, requirementStatus) {
        //if (angular.isDefined($scope.common.tblRequirementList)) {
        //    $scope.common.tblRequirementList.reload();
        //}
        //else
        {
            $scope.common.tblRequirementList = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: $scope.common.ngTablePaginationCount,
                total: totalCount,
                getData: function($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    /* sorting */
                    if (pageFlag != 1) {
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function(val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */
                        //if (document.getElementById("select_all").checked == true) {
                        //    $("#select_all").click();
                        //}
                        var promise = XSeedApiFactory.getRequirementListByStatus(organizationId, userId, requirementStatus, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder);
                        promise.then(
                            function(response) {
                                $scope.common.jobList = response[0].data.Jobs;
                                $scope.common.totalCount = response[0].data.TotalCount;
                                $scope.common.totalPages = response[0].data.TotalPages;

                                //to select first row by default
                                if ($scope.common.requirementModel.manipulationMode != "Edit") {
                                    $scope.setClickedRow($scope.common.jobList[0]);
                                } else {
                                    //refresh activity
                                    if ($scope.common.selectedJob) {
                                        var IsRequirement = IsRequirementInList($scope.common.jobList);
                                        if (IsRequirement) {
                                            $scope.getActivityList();
                                        } else {
                                            $scope.setClickedRow($scope.common.jobList[0]);
                                        }
                                    }
                                    $scope.common.requirementModel.manipulationMode = undefined;
                                }
                                //Manupulation of response
                                angular.forEach($scope.common.jobList, function(val, index) {
                                    if (val.Id == $scope.common.selectedJob.Id) {
                                        $scope.common.selectedJob = val;
                                    }
                                    if ($scope.common.jobList[index].TechnicalSkills != null) {
                                        $scope.common.jobList[index].TechnicalSkills = $scope.common.jobList[index].TechnicalSkills.split("|");
                                    }
                                });

                                angular.forEach($scope.common.jobList, function (val, index) {
                                    $scope.common.jobList[index].count = $scope.common.jobList[index].AssociateCandidate.length;
                                });

                                blockUI.stop();

                            },
                            function(httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                            });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.common.jobList);
                },
                $scope: $scope
            });
        }
    };

    $scope.getRequirementList = function() {

        if ($rootScope.userHasToken == false) {
            $location.path('login');
            return false;
        }

        var createRquirementURL = '/createRequirement';
        var editRequirementURL = '/editRequirement';
        var requirementListURL = '/requirementList';
        var requirementDetailURL = '/requirementDetail';

        var dashboardURL = '/dashboard';

        if (($location.path().toLowerCase() == createRquirementURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Requirement_Create == "False") {
            $location.path('dashboard');
            return false;
        }
        if (($location.path().toLowerCase() == editRequirementURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Requirement_Update == "False") {
            $location.path('dashboard');
            return false;
        }

        if (($location.path().toLowerCase() == requirementListURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Requirement_Read == "False") {
            $location.path('dashboard');
            return false;
        }
        if (($location.path().toLowerCase() == requirementDetailURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Requirement_Read == "False") {
            $location.path('dashboard');
            return false;
        }

        if ($scope.flag.advanceSearchFlag) {
            return;
        }
        blockUI.start();
        $scope.jobLookupProcess();
        var firstPageFlag = 0;
        var organizationId = $rootScope.userDetails.OrganizationId;
        var userId = $rootScope.userDetails.UserId;
        $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
        //$scope.common.pageNumberShow = undefined;

        if ($scope.common.requirementModel.manipulationMode != "Edit") {

            var promise = XSeedApiFactory.getRequirementList(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow);
            promise.then(
                function(response) {
                    $scope.common.jobList = response[0].data.Jobs;
                    $scope.common.totalCount = response[0].data.TotalCount;
                    $scope.common.totalPages = response[0].data.TotalPages;

                    if ($scope.common.jobList.length) {
                        $scope.firstRequisitionId = $scope.common.jobList[0].RequisitionId;

                        //Manupulation of response
                        angular.forEach($scope.common.jobList, function(val, index) {
                            if ($scope.common.jobList[index].TechnicalSkills != null) {
                                $scope.common.jobList[index].TechnicalSkills = $scope.common.jobList[index].TechnicalSkills.split("|");
                                $scope.common.jobList[index].TechnicalSkills = $scope.common.jobList[index].TechnicalSkills.join();
                            }
                        });

                        angular.forEach($scope.common.jobList, function (val, index) {
                            $scope.common.jobList[index].count = $scope.common.jobList[index].AssociateCandidate.length;
                      
                        });

                        //to select first row by default
                        $scope.setClickedRow($scope.common.jobList[0]);

                        firstPageFlag = 1;
                        populateRequirementList(organizationId, $scope.common.jobList, $scope.common.totalCount, $scope.common.totalPages, firstPageFlag);

                        //to show pending requirement count in badge on requirement list
                        if ($rootScope.userDetails.RoleName == 'Application Administrator' || $rootScope.userDetails.RoleName == 'Delivery Manager' || $rootScope.userDetails.RoleName == 'Team Lead') {
                            if ($rootScope.userDetails.Vendor_Read == 'True') {
                                getVendorUnreadMailCount();
                            }
                        }
                    }
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                });
        }
        $timeout(function() {
            $scope.populateFilterLookupList();
        }, 2000);

        blockUI.stop();
    };

    function populateRequirementList(organizationId, jobList, totalCount, totalPages, pageFlag) {
        //if (angular.isDefined($scope.common.tblRequirementList)) {
        //    $scope.common.tblRequirementList.reload();
        //}
        //else
        {
            $scope.common.tblRequirementList = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: $scope.common.ngTablePaginationCount,
                total: totalCount,
                getData: function($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    /* sorting */
                    if (pageFlag != 1) {
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function(val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */
                        //if (document.getElementById("select_all").checked == true) {
                        //    $("#select_all").click();
                        //}
                        var promise = XSeedApiFactory.getRequirementList(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder);
                        promise.then(
                            function (response) {
                                $('.loading').fadeIn('fast');
                                $('.loading').fadeOut(1000);
                                $scope.common.jobList = response[0].data.Jobs;
                                $scope.common.totalCount = response[0].data.TotalCount;
                                $scope.common.totalPages = response[0].data.TotalPages;

                                //to select first row by default
                                if ($scope.common.requirementModel.manipulationMode != "Edit") {
                                    $scope.setClickedRow($scope.common.jobList[0]);
                                } else {
                                    //refresh activity
                                    if ($scope.common.selectedJob) {
                                        var IsRequirement = IsRequirementInList($scope.common.jobList);
                                        if (IsRequirement) {
                                            $scope.getActivityList();
                                        } else {
                                            $scope.setClickedRow($scope.common.jobList[0]);
                                        }
                                    }
                                    $scope.common.requirementModel.manipulationMode = undefined;
                                }
                                //Manupulation of response
                                angular.forEach($scope.common.jobList, function(val, index) {
                                    if (val.Id == $scope.common.selectedJob.Id) {
                                        $scope.common.selectedJob = val;
                                    }
                                    if ($scope.common.jobList[index].TechnicalSkills != null) {
                                        $scope.common.jobList[index].TechnicalSkills = $scope.common.jobList[index].TechnicalSkills.split("|");
                                    }
                                });

                                angular.forEach($scope.common.jobList, function (val, index) {
                                    $scope.common.jobList[index].count = $scope.common.jobList[index].AssociateCandidate.length;
                                });

                                blockUI.stop();

                            },
                            function(httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                            });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.common.jobList);
                },
                $scope: $scope
            });
        }
    };

    $scope.populateFilterLookupList = function() {
        $scope.requirementFilterLookup = [];
        var organizationUserArray = [];
        var self = angular.copy(_.find($scope.lookup.organizationUser, function(o) { return o.Id == $rootScope.userDetails.Id; }));
        if (self) {
            self.Name = "My";
            organizationUserArray.push(self);
        }

        angular.forEach($scope.lookup.organizationUser, function(val, index) {
            if ($scope.lookup.organizationUser[index].Id != $rootScope.userDetails.Id) {
                organizationUserArray.push($scope.lookup.organizationUser[index]);
            }
        });

        $scope.requirementFilterLookup.push({ "Users": organizationUserArray });
        $scope.requirementFilterLookup.push({ "Status": $scope.lookup.jobStatus });
        $scope.requirementFilterLookup.push({ "Type": $scope.lookup.jobType });
        $scope.requirementFilterLookup.push({ "Company": $scope.lookup.company });
        //to clear All Filter Items On Reload


        //var requirementTypeLookup = [];
        ////requirementTypeLookup.push({ "Id": "All", "Name": "All Requirements" });
        //requirementTypeLookup.push({ "Id": "All", "Name": "My Requirements" });
        //requirementTypeLookup.push({ "Id": "Open", "Name": "My Open" });
        //requirementTypeLookup.push({ "Id": "Closed", "Name": "My Closed" });
        //requirementTypeLookup.push({ "Id": "Posted", "Name": "My Posted" });
        //requirementTypeLookup.push({ "Id": "Pending", "Name": "My Pending" });
        //requirementTypeLookup.push({ "Id": "Hold", "Name": "My Hold" });

        //$scope.requirementFilterLookup.push({ "Requirement Type": requirementTypeLookup });
    }

    $scope.getSelectedFilterItem = function() {


        var Status = [];
        var Type = [];
        var Company = [];
        var OrganizationUser = [];

        Status = $scope.requirementFilter.select_filterStatus;
        Type = $scope.requirementFilter.select_filterType;
        Company = $scope.requirementFilter.select_filterCompany;
        OrganizationUser = $scope.requirementFilter.select_filterUser;

        $(".btn-filter").removeClass("active");
        $(".btn-default").addClass("active");

        var filterSelection = {};
        filterSelection.Status = Status;
        filterSelection.Type = Type;
        filterSelection.Company = Company;
        filterSelection.OrganizationUser = OrganizationUser;
        //filterSelection.RequirementType = RequirementType;
        filterSelection.OrganizationId = $rootScope.userDetails.OrganizationId;
        filterSelection.OrganizationUserId = $rootScope.userDetails.UserId;

        $scope.common.advanceSearchModel.filter = filterSelection;

        $scope.getRequirementFilterResult();
    }


    /* Job module start */
    $scope.common.jobPageLookupList = function() {
        var promise = XSeedApiFactory.jobPagePrimaryLookupCall($rootScope.userDetails.OrganizationId);
        promise.then(
            function(response) {
                $scope.lookup.jobTitle = response[0].data;
                $scope.lookup.jobType = response[1].data;
                $scope.lookup.jobStatus = response[2].data;
                $scope.lookup.degree = response[3].data;
                $scope.lookup.visaType = response[4].data;

                //to autocomplete jobTitle
                buildJobTitleStructuredData();

                $scope.flag.requirementLookupLoaded = true;
                angular.forEach($scope.lookup.jobStatus, function (val, index) {
                    if (val.Name == 'Closed') {
                        $scope.common.jobClosedStatusId = val.Id;
                    }
                });
            },
            function(httpError) {
                ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
            });
    };
    $scope.updateJobStatus = function (requirementId, JobStatusId, index) {
        console.log(JobStatusId);
        console.log(requirementId);
        blockUI.start();
        var promise = XSeedApiFactory.updateJobStatus(requirementId, JobStatusId);
        promise.then(
            function (response) {
                blockUI.stop();
                $scope.common.jobList[index].JobStatusId = JobStatusId;
                $scope.common.jobList[index].JobStatus = "Closed";
                XSeedAlert.swal({
                    title: 'Success!',
                    text: 'Requirement details updated successfully!',
                    type: "success",
                    customClass: "xseed-error-alert",
                    allowOutsideClick: false
                }).then(function () {
                    $timeout(function () {
                        // $scope.common.requirementModel = {};
                        // $scope.resetRequirementAdvanceSearch();
                        $route.reload();
                    }, 100);

                }, function (dismiss) {
                    blockUI.stop();

                });
            },
            function (httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });

    }
    function buildJobTitleStructuredData() {
        $scope.jobTitles = [];
        angular.forEach($scope.lookup.jobTitle, function(val, index) {
            $scope.jobTitles.push(val.Name)
        });
    };

    function jobPageSecondaryLookupList() {
        var organizationId = $rootScope.userDetails.OrganizationId;
        var promise = XSeedApiFactory.jobPageSecondaryLookupCall(organizationId);
        promise.then(
            function(response) {
                $scope.lookup.company = response[0].data;
                $scope.lookup.organizationUser = response[1].data;
            },
            function(httpError) {
                ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
            });
    };

    //$scope.propertyName = 'JobTitle';
    //$scope.reverse = true;

    //$scope.sortBy = function (propertyName) {
    //    $scope.reverse = ($scope.propertyName == propertyName) ? !$scope.reverse : false;
    //    $scope.propertyName = propertyName;
    //}

    $scope.propertyName = 'CreatedOn';
    $scope.reverse = true;

    $scope.sortBy = function (propertyName) {
        $scope.reverse = ($scope.propertyName == propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
        $scope.tblRequirementList.reload();
    }

    $scope.jobLookupProcess = function() {
        if ($scope.flag.requirementLookupLoaded == false) {
            $scope.common.jobPageLookupList();
        }

        jobPageSecondaryLookupList();
    }

    $scope.createJobProcess = function(createJobForm) {
        if (new ValidationService().checkFormValidity(createJobForm)) {

            var temp = 0;
            if ($("#location").val() == "") {
                $("#locationError").show();
                $timeout(function() {
                    $(".step3").click();
                })
                temp = 1;
            }

            if ($("#duration").val() == "") {
                $("#durationError").show();
                $timeout(function() {
                    $(".step3").click();
                })
                temp = 1;
            }
            if (temp == 1) {
                return;
            }


            blockUI.start();
            var list = [];
            $scope.createJobFormSubmitted = false;
            $scope.common.requirementModel.OrganizationId = $rootScope.userDetails.OrganizationId;
            $scope.common.requirementModel.OrganizationUserId = $rootScope.userDetails.UserId;



            $scope.common.requirementModel.ExperienceInYear = $('#experienceInYear').val();
            $scope.common.requirementModel.ExperienceInMonth = $('#experienceInMonth').val();
            $scope.common.requirementModel.USExperienceInYear = $('#USExperienceInYear').val();
            $scope.common.requirementModel.USExperienceInMonth = $('#USExperienceInMonth').val();

            if (angular.isDefined($scope.common.requirementModel.OrganizationUserIdList)) {
                for (var i = 0; i < $scope.common.requirementModel.OrganizationUserIdList.length; i++) {
                    var o = {};
                    o.Id = $scope.common.requirementModel.OrganizationUserIdList[i];
                    o.Name = '';
                    list.push(o);
                }

                $scope.common.requirementModel.OrganizationUser = list;
            }
            list = [];
            if (angular.isDefined($scope.common.requirementModel.DegreeList)) {
                for (var i = 0; i < $scope.common.requirementModel.DegreeList.length; i++) {
                    var o = _.find($scope.lookup.degree, function(o) { return o.Id == $scope.common.requirementModel.DegreeList[i]; });
                    list.push(o);
                }
                $scope.common.requirementModel.DegreeList = list;
            }

            var visa = [];
            if (angular.isDefined($scope.common.requirementModel.VisaTypeList)) {
                for (var i = 0; i < $scope.common.requirementModel.VisaTypeList.length; i++) {
                    var o = _.find($scope.lookup.visaType, function(o) { return o.Id == $scope.common.requirementModel.VisaTypeList[i]; });
                    visa.push(o);
                }
                $scope.common.requirementModel.VisaTypeList = visa;
            }

            if ($scope.common.requirementModel.SkillList.length > 0) {
                $scope.common.requirementModel.TechnicalSkills = "";
                for (var i = 0; i < ($scope.common.requirementModel.SkillList.length - 1); i++) {
                    $scope.common.requirementModel.TechnicalSkills = $scope.common.requirementModel.TechnicalSkills + $scope.common.requirementModel.SkillList[i].toString() + "|";
                }
                $scope.common.requirementModel.TechnicalSkills = $scope.common.requirementModel.TechnicalSkills + $scope.common.requirementModel.SkillList[$scope.common.requirementModel.SkillList.length - 1].toString();
            }

            $scope.common.requirementModel.DriveFromDate = $filter('date')($scope.common.requirementModel.DriveFromDate, "MM/dd/yyyy");
            $scope.common.requirementModel.DriveToDate = $filter('date')($scope.common.requirementModel.DriveToDate, "MM/dd/yyyy");
            $scope.common.requirementModel.JobPostedDate = $filter('date')($scope.common.requirementModel.JobPostedDate, "MM/dd/yyyy");
            $scope.common.requirementModel.JobClosedDate = $filter('date')($scope.common.requirementModel.JobClosedDate, "MM/dd/yyyy");
            $scope.common.requirementModel.JobExpiryDate = $filter('date')($scope.common.requirementModel.JobExpiryDate, "MM/dd/yyyy");
            $scope.common.requirementModel.HtmlText = null;

            $scope.common.requirementModel.CreatedBy = $rootScope.userDetails.UserId;
            $scope.common.requirementModel.CreatedByName = $rootScope.userDetails.FirstName + " " + $rootScope.userDetails.LastName;
            console.log($scope.common.requirementModel);
          
            var promise = XSeedApiFactory.createJob($scope.common.requirementModel);
            promise.then(
                function(response) {

                    //to submit QNote
                    if (response[0].data != null) {
                        $scope.common.selectedJob = {};
                        $scope.common.selectedJob.Id = response[0].data;
                        if ($scope.common.selectedJob.Id) {
                            $scope.qNoteModel.qNote = $scope.common.requirementModel.qNote;
                            $scope.submitQNote();
                        }
                    }
                    if ($scope.common.requirementModel.emailParserId) {
                        var promise = XSeedApiFactory.acceptRequirement($scope.common.requirementModel.emailParserId, response[0].data, $rootScope.userDetails.UserId, true);
                        promise.then(
                            function(response) {
                                blockUI.stop();
                            },
                            function(httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                            });
                    }

                    blockUI.stop();
                    XSeedAlert.swal({
                        title: 'Success!',
                        text: 'Requirement created successfully!',
                        type: "success",
                        customClass: "xseed-error-alert",
                        allowOutsideClick: false
                    }).then(function () {
                        //$scope.common.requirementModel = {};
                        $timeout(function() {
                            //to show newly created requirement selected
                            //$scope.common.tblRequirementList = undefined;
                            $location.path('candidateList');
                            getSearchedCandidateList();
                            $scope.reload();
                        }, 0);
                    }, function(dismiss) {
                        blockUI.stop();
                    });
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                });
        };
    };

    function getSearchedCandidateList() {
        blockUI.start();
        if ($scope.common.candidateModel.manipulationMode != "Edit") {
            $scope.flag.advanceSearchFlag = true;
            $scope.common.advanceSearchModel.OrganizationId = $rootScope.userDetails.OrganizationId;
            $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
            $scope.common.pageNumberShow = undefined;
            var firstPageFlagSearch = 0;
            var loc = $('#location').val();
            //for new plain search
            $scope.common.advanceSearchModel.SearchIn = ["CandidateId", "Status", "Name", "Email", "Contact", "JobTitle", "Skills", "Location"];
            $scope.common.advanceSearchModel.SearchAny = loc;
            var promise = XSeedApiFactory.getCandidateAdvanceSearchResult($scope.common.advanceSearchModel, $scope.common.pageSizeShow, $scope.common.pageNumberShow);
            promise.then(
                function (response) {
                    $scope.common.candidateList = response[0].data.Candidates;
                    $scope.common.totalCount = response[0].data.TotalCount;
                    $scope.common.totalPages = response[0].data.TotalPages;
                    console.log('candidateList');
                    console.log($scope.common.candidateList);
                    firstPageFlagSearch = 1;

                    $scope.populateAdvanceSearchCandidateList($scope.common.candidateList, $scope.common.totalCount, $scope.common.totalPages, firstPageFlagSearch);
                    $scope.closeAdvanceSearchModal();
                    if ($scope.common.candidateList.length) {
                        //to select first row by default
                        $scope.setClickedRowCandidate($scope.common.candidateList[0]);
                    }
                    //to show skills in tooltip as string
                    angular.forEach($scope.common.candidateList, function (val, index) {
                        if (val.Skills) {
                            val.Skills = val.Skills.join();
                        }
                    });
                    blockUI.stop();


                },
                function (httpError) {
                    $scope.closeAdvanceSearchModal();
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Search', httpError.data.Status, httpError.data.Message);
                });
        }
    }



    // Update Qnote requirement

    $scope.UpdateQNote = function(editid) {
        if ($scope.qNoteModel.qNote) {
            // XSeedAlert.swal({
            //     title: "Are you sure?",
            //     text: "This action will update the Quick Note!",
            //     type: "warning",
            //     showCancelButton: true,
            //     confirmButtonClass: "btn-danger",
            //     confirmButtonText: "Update",
            //     cancelButtonText: "Cancel",
            //     closeOnConfirm: false,
            //     closeOnCancel: false
            // }).then(function () {

            //     if (editid) {
            $scope.qNoteList[$scope.common.qNoteList.editposition].Description = $scope.qNoteModel.qNote;

            var promise = XSeedApiFactory.updateQNote($scope.qNoteList[$scope.common.qNoteList.editposition], editid);
            promise.then(
                function(response) {
                    $scope.common.qNoteList.EditID = null;
                    $('#summernote').summernote('code', '');
                    $scope.common.qNoteList.EditMode = false;

                    $scope.getQNoteList();

                },
                function(httpError) {
                    ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
                });
        } else {
            $("#error-summernote").show();
            return;
        }
        //     }
        // }
        // )
    };

    $scope.requirementDirectEditView = function(job) {
        blockUI.start();

        if (job) {
            $scope.common.selectedRow = job.Id;
            $scope.common.selectedJob = job;

            $scope.getRequirementDetails();
            $scope.common.requirementModel.manipulationMode = "Edit";
            $scope.common.redirectedFrom = 'RequirementList';
            //blockUI.stop();
            $location.path('editRequirement');
        }
    };

    $scope.editJobProcess = function(createJobForm) {
        if (new ValidationService().checkFormValidity(createJobForm)) {
            blockUI.start();
            $scope.common.requirementModel.OrganizationId = $rootScope.userDetails.OrganizationId;

            $scope.common.requirementModel.ExperienceInYear = $('#experienceInYear').val();
            $scope.common.requirementModel.ExperienceInMonth = $('#experienceInMonth').val();
            $scope.common.requirementModel.USExperienceInYear = $('#USExperienceInYear').val();
            $scope.common.requirementModel.USExperienceInMonth = $('#USExperienceInMonth').val();

            var list = [];
            if (angular.isDefined($scope.common.requirementModel.OrganizationUserIdList)) {
                for (var i = 0; i < $scope.common.requirementModel.OrganizationUserIdList.length; i++) {
                    var o = {};
                    o.Id = $scope.common.requirementModel.OrganizationUserIdList[i];
                    o.Name = '';
                    list.push(o);
                }

                $scope.common.requirementModel.OrganizationUser = list;
            }
            list = [];
            if (angular.isDefined($scope.common.requirementModel.DegreeList)) {
                for (var i = 0; i < $scope.common.requirementModel.DegreeList.length; i++) {
                    var o = _.find($scope.lookup.degree, function(o) { return o.Id == $scope.common.requirementModel.DegreeList[i]; });
                    list.push(o);
                }
                $scope.common.requirementModel.DegreeList = list;
            }

            var visa = [];
            if (angular.isDefined($scope.common.requirementModel.VisaTypeList)) {
                for (var i = 0; i < $scope.common.requirementModel.VisaTypeList.length; i++) {
                    var o = _.find($scope.lookup.visaType, function(o) { return o.Id == $scope.common.requirementModel.VisaTypeList[i]; });
                    visa.push(o);
                }
                $scope.common.requirementModel.VisaTypeList = visa;
            }


            if ($scope.common.requirementModel.SkillList && $scope.common.requirementModel.SkillList.length > 0) {
                $scope.common.requirementModel.TechnicalSkills = "";
                for (var i = 0; i < ($scope.common.requirementModel.SkillList.length - 1); i++) {
                    $scope.common.requirementModel.TechnicalSkills = $scope.common.requirementModel.TechnicalSkills + $scope.common.requirementModel.SkillList[i].toString() + "|";
                }
                $scope.common.requirementModel.TechnicalSkills = $scope.common.requirementModel.TechnicalSkills + $scope.common.requirementModel.SkillList[$scope.common.requirementModel.SkillList.length - 1].toString();
            }

            $scope.common.requirementModel.DriveFromDate = $filter('date')($scope.common.requirementModel.DriveFromDate, "MM/dd/yyyy");
            $scope.common.requirementModel.DriveToDate = $filter('date')($scope.common.requirementModel.DriveToDate, "MM/dd/yyyy");
            $scope.common.requirementModel.JobPostedDate = $filter('date')($scope.common.requirementModel.JobPostedDate, "MM/dd/yyyy");
            $scope.common.requirementModel.JobClosedDate = $filter('date')($scope.common.requirementModel.JobClosedDate, "MM/dd/yyyy");
            $scope.common.requirementModel.JobExpiryDate = $filter('date')($scope.common.requirementModel.JobExpiryDate, "MM/dd/yyyy");

            $scope.common.requirementModel.ModifiedByName = $rootScope.userDetails.FirstName + " " + $rootScope.userDetails.LastName;
            $scope.common.requirementModel.ModifiedBy = $rootScope.userDetails.UserId;

            console.log("UPDATE REQUIREMENT");
            console.log(console.log($scope.common.requirementModel));
            console.log($rootScope.userDetails);

            var promise = XSeedApiFactory.editJob($scope.common.requirementModel);
            promise.then(
                function(response) {
                    //to submit QNote
                    if ($scope.common.selectedJob.Id) {

                        $scope.qNoteModel.qNote = $scope.common.requirementModel.qNote;
                        $scope.submitQNote();
                    }

                    blockUI.stop();
                    XSeedAlert.swal({
                        title: 'Success!',
                        text: 'Requirement details updated successfully!',
                        type: "success",
                        customClass: "xseed-error-alert",
                        allowOutsideClick: false
                    }).then(function() {
                        $timeout(function() {
                            // $scope.common.requirementModel = {};
                            // $scope.resetRequirementAdvanceSearch();
                        }, 100);
                        $location.path('requirementList');
                    }, function(dismiss) {
                        blockUI.stop();

                    });
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                });
        };
    };

    $scope.cancelRequirement = function() {
        XSeedAlert.swal({
            title: "Are you sure you want to cancel?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function() {
            blockUI.start();
            if ($scope.common.redirectedFrom == 'RequirementList') {
                var editRquirementURL = '/editRequirement';
                if ($location.path().toLowerCase() == editRquirementURL.toLowerCase()) {
                    $scope.common.requirementModel.manipulationMode = "Edit";
                }
                $timeout(function() {
                    //$scope.resetRequirementAdvanceSearch();
                }, 100);

                $location.path('requirementList');
            } else {
                $location.path('requirementQueue');
            }
            blockUI.stop();
        }, function(dismiss) {});
    };
  
    $scope.goToRequirementList = function() {
        if ($scope.common.redirectedFrom == 'RequirementList') {
            $timeout(function() {
                // $scope.resetRequirementAdvanceSearch();
            }, 100);
            $location.path('requirementList');
        } else
            $location.path('requirementQueue');
    };

    //Advance Search
    $scope.resetRequirementAdvanceSearch = function() {
        $scope.common.advanceSearchModel = {};
        $scope.flag.advanceSearchFlag = false;
        $scope.common.tblRequirementList = undefined;
        $scope.common.requirementStatusFilter = "MyAll";
        $scope.getJobsByStatus('MyAll');
        // clearFilter();
        setTimeout(function() {
            $(".select2").each(function() { //added a each loop here
                $(this).val("");
                $(this).trigger("change");
            });
        }, 500);
    };


     $scope.cancelRequirementDetail = function () {
       
      $scope.common.uploadFileName = null;
        $location.path('requirementList');
    };




    /**************************submit Candidate From Requirement**************************/

    //Checkbox code-
    $scope.checkboxes = { 'checked': false, items: {} };

    // watch for check all checkbox
    $scope.$watch('checkboxes.checked', function(value) {
        angular.forEach($scope.common.candidateList, function(item) {
            if (angular.isDefined(item._id)) {
                $scope.checkboxes.items[item._id] = value;
            }
        });
    });

    // watch for data checkboxes
    $scope.$watch('checkboxes.items', function(values) {
        if (!$scope.common.candidateList) {
            return;
        }
        var checked = 0,
            unchecked = 0,
            total = $scope.common.candidateList.length;
        angular.forEach($scope.common.candidateList, function(item) {
            checked += ($scope.checkboxes.items[item._id]) || 0;
            unchecked += (!$scope.checkboxes.items[item._id]) || 0;
        });
        if ((unchecked == 0) || (checked == 0)) {
            $scope.checkboxes.checked = (checked == total);
        }

        $scope.checkedCount = checked;
        //alert(checked);
        //alert(unchecked);
        //grayed checkbox
        angular.element(document.getElementById("select_all")).prop("indeterminate", (checked != 0 && unchecked != 0));
    }, true);

    $scope.openCandidateSearchModal = function(job) {
        $scope.common.selectedJob = job;
        blockUI.start();
        $scope.getCandidateList();
        submitCandidateFromRequirementModal.$promise.then(submitCandidateFromRequirementModal.show);
        blockUI.stop();
    };

    $scope.closeCandidateSearchModal = function() {
        submitCandidateFromRequirementModal.$promise.then(submitCandidateFromRequirementModal.hide);
        $scope.checkboxes.items = {};
        $scope.common.advanceSearchModel = {};
        $scope.candidateAdvanceSearchFlag = false;
        $scope.common.tblCandidateList = undefined;
    };

    $scope.getCandidateList = function() {
        blockUI.start();
        var firstPageFlag = 0;
        $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
        $scope.common.pageNumberShow = undefined;

        var promise = XSeedApiFactory.getCandidateList($scope.common.pageSizeShow, $scope.common.pageNumberShow);
        promise.then(
            function(response) {
                $scope.common.candidateList = response[0].data.Candidates;
                $scope.common.totalCount = response[0].data.TotalCount;
                $scope.common.totalPages = response[0].data.TotalPages;

                //to show skills in tooltip as string
                angular.forEach($scope.common.candidateList, function(val, index) {
                    if (val.Skills) {
                        val.Skills = val.Skills.join();
                    }
                });

                firstPageFlag = 1;
                if ($scope.common.candidateList.length) {
                    populateCandidateList($scope.common.candidateList, $scope.common.totalCount, $scope.common.totalPages, firstPageFlag);
                }
                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Candidate', httpError.data.Status, httpError.data.Message);
            });
    };

    function populateCandidateList(candidateList, totalCount, totalPages, pageFlag) {
        if (angular.isDefined($scope.common.tblCandidateList)) {
            $scope.common.tblCandidateList.reload();
        } else {
            $scope.common.tblCandidateList = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: $scope.common.ngTablePaginationCount,
                total: totalCount,
                getData: function($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    if (pageFlag != 1) {


                        /* sorting */
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function(val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */
                        if (document.getElementById("select_all").checked == true) {
                            $("#select_all").click();
                        }
                        $('input[name=chkOrgRow]').each(function() {
                                if ($(this).prop("checked") == true) {
                                    $(this).click();
                                }
                            })
                            /* sorting */
                            //if (document.getElementById("select_all").checked == true) {
                            //    $("#select_all").click();
                            //}
                        var promise = XSeedApiFactory.getCandidateList($scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder);
                        promise.then(
                            function(response) {
                                $scope.common.candidateList = response[0].data.Candidates;
                                $scope.common.totalCount = response[0].data.TotalCount;
                                $scope.common.totalPages = response[0].data.TotalPages;

                                //to show skills in tooltip as string
                                angular.forEach($scope.common.candidateList, function(val, index) {
                                    if (val.Skills) {
                                        val.Skills = val.Skills.join();
                                    }
                                });
                                blockUI.stop();

                            },
                            function(httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('Candidate', httpError.data.Status, httpError.data.Message);
                            });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.common.candidateList);
                },
                $scope: $scope
            });
        }

    };

    $scope.resetCandidateAdvanceSearch = function() {
        $scope.common.advanceSearchModel = {};
        $scope.candidateAdvanceSearchFlag = false;
        $scope.common.tblCandidateList = undefined;
        $scope.getCandidateList();
    };

    $scope.openCandidateSubmissionModal = function(type, candidate) {
        $scope.candidateSubmissionList = [];

        $scope.candidate = candidate;

        if (type == 'Single') {
            $scope.candidateSubmissionList.push(candidate);
        } else {
            angular.forEach($scope.checkboxes.items, function(item, Identifier) {
                if (item == true) {
                    var selectedCandidate = _.find($scope.common.candidateList, function(o) { return o._id == Identifier; });
                    if (selectedCandidate) {
                        $scope.candidateSubmissionList.push(selectedCandidate);
                    }
                }
            });
        }


        if ($scope.candidateSubmissionList.length == 0) {
            ExceptionHandler.handlerPopup('Candidate Submission', 'Please select candidate from displayed page.', '', 'info');
        } else {
            var organizationId = $rootScope.userDetails.OrganizationId;
            var promise = XSeedApiFactory.submissionPopupLookupCall(organizationId);
            promise.then(
                function(response) {
                    $scope.lookup.company = response[0].data;
                    $scope.CandidateSubmissionModel = {};
                    if ($scope.common.selectedJob) {
                        $scope.CandidateSubmissionModel.CompanyId = $scope.common.selectedJob.CompanyId;
                        if ($scope.CandidateSubmissionModel.CompanyId) {
                            $scope.getRequirementListByCompany($scope.CandidateSubmissionModel.CompanyId);
                        }
                        $scope.CandidateSubmissionModel.JobId = $scope.common.selectedJob.Id;
                    }
                },
                function(httpError) {
                    //blockUI.stop();
                    ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
                });

            candidateSubmissionModal.$promise.then(candidateSubmissionModal.show);
        }
    }

    $scope.submitPopupCancel = function() {
        candidateSubmissionModal.$promise.then(candidateSubmissionModal.hide);
        $scope.checkboxes.items = {};
    }

    $scope.getRequirementListByCompany = function(companyId) {

        var promise = XSeedApiFactory.getRequirementListByCompany(companyId);
        promise.then(
            function(response) {
                $scope.lookup.requirement = response[0].data;
                console.log($scope.lookup.requirement);
                setTimeout(function() { $("select.select2").select2(); }, 500);
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });
    };

    $scope.submitCandidateProcess = function(candidateSubmissionPopupForm) {
        if (new ValidationService().checkFormValidity(candidateSubmissionPopupForm)) {
            blockUI.start();

            var list = [];
            for (var i = 0; i < $scope.candidateSubmissionList.length; i++) {
                var object = {};
                object.Id = $scope.candidateSubmissionList[i]._id;
                object.Name = '';

                list.push(object);
            }

            $scope.CandidateSubmissionModel.CandidateList = list;

            $scope.CandidateSubmissionModel.OrganizationUserId = $rootScope.userDetails.UserId;
            console.log($scope.CandidateSubmissionModel);
            var promise = XSeedApiFactory.submitCandidate($scope.CandidateSubmissionModel);
            promise.then(
                function(response) {
                    blockUI.stop();
                    $scope.submitPopupCancel();
                    $scope.dataSubmissiomEmail.successCandidates = response[0].data.successCandidates;
                    $scope.dataSubmissiomEmail.companyId = $scope.CandidateSubmissionModel.CompanyId;
                    $scope.dataSubmissiomEmail.jobId = $scope.CandidateSubmissionModel.JobId;
                    $scope.CandidateSubmissionModel.CompanyId = "";
                    $scope.CandidateSubmissionModel.JobId = "";

                    if ($scope.dataSubmissiomEmail.successCandidates.length > 0) {
                        $scope.closeCandidateSearchModal();
                        XSeedAlert.swal({
                            title: 'Success!',
                            text: 'Candidate(s) submitted!',
                            type: "success",
                            customClass: "xseed-error-alert"
                        }).then(function() {
                            $scope.getSubmissionUrl($scope.dataSubmissiomEmail.companyId);
                            $scope.openCandidateClientsubmissionEmailModal();
                        }, function(dismiss) {
                            blockUI.stop();
                        })
                    } else {
                        blockUI.stop();
                        ExceptionHandler.handlerPopup('Submission', response[0].data.message, 'Info!', 'info');
                    };
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
                });
        };
    };

    $scope.openCandidateClientsubmissionEmailModal = function () {
        if ($scope.dataSubmissiomEmail.successCandidates.length > 0) {
            blockUI.start();
            $scope.common.selectedJob.CompanyContactName;
            console.log($scope.common.selectedJob.CompanyContactName);

            //get company contacts
            var promise = XSeedApiFactory.getSubmissionContactEmail($scope.dataSubmissiomEmail.companyId);
            promise.then(
                function (response) {
                    $rootScope.commonEmailModel.candidateList = [];
                    //get company submissionUrl
                    var promise = XSeedApiFactory.getCompanyDetail($rootScope.userDetails.OrganizationId, $scope.dataSubmissiomEmail.companyId);
                    promise.then(
                        function (response) {
                            $scope.companyData = response[0].data;
                            $rootScope.commonEmailModel.companySubmissionUrl = $scope.companyData.SubmissionPageURL;
                        },
                        function (httpError) {
                            ExceptionHandler.handlerHTTPException('Company', httpError.data.Status, httpError.data.Message);
                        });
                    //get company contact email
                    $rootScope.commonEmailModel.ToEmailID = $scope.common.selectedJob.CompanyContactName;
                    $rootScope.commonEmailModel.FromEmailID = $rootScope.userDetails.PrimaryEmail;
                    $rootScope.commonEmailModel.MailSubject = "Submissions: Candidates Resume";
                    $rootScope.commonEmailModel.MailBody = "We would like to submit following candidate(s) against the<br />Requirement: ";
                    //get job title
                    angular.forEach($scope.lookup.requirement, function (val, index) {
                        if ($scope.dataSubmissiomEmail.jobId == val.Id) {
                            $rootScope.commonEmailModel.MailBody = $rootScope.commonEmailModel.MailBody + val.JobTitle;
                        }
                    });
                    //get candidate names
                    $rootScope.commonEmailModel.MailBody = $rootScope.commonEmailModel.MailBody + " <br />following is/are the candidate(s)";
                    angular.forEach($scope.dataSubmissiomEmail.successCandidates, function (val, index) {
                        var selectedCandidate = _.find($scope.common.candidateList, function (o) { return o._id == val; });
                        $rootScope.commonEmailModel.candidateList.push(selectedCandidate._id);
                        index = index + 1
                        $rootScope.commonEmailModel.MailBody = $rootScope.commonEmailModel.MailBody + "<br />" + index + ". " + selectedCandidate.FirstName + " " + selectedCandidate.LastName;
                    });
                    $rootScope.commonEmailModel.typeOfNotification = $scope.constants.submitCandidateByEmail;
                    $scope.common.commonEmailModal.$promise.then($scope.common.commonEmailModal.show);
                    blockUI.stop();
                },
                function (httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
                });
        }
    }
    $scope.getAdvanceSearchedCandidateList = function() {
        blockUI.start();
        $scope.candidateAdvanceSearchFlag = true;
        $scope.common.advanceSearchModel.OrganizationId = $rootScope.userDetails.OrganizationId;
        $scope.common.advanceSearchModel.SearchIn = ["CandidateId", "Name", "Email", "Contact", "CreatedBy"];
        $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
        $scope.common.pageNumberShow = undefined;
        var firstPageFlagSearch = 0;

        var promise = XSeedApiFactory.getCandidateAdvanceSearchResult($scope.common.advanceSearchModel, $scope.common.pageSizeShow, $scope.common.pageNumberShow);
        promise.then(
            function(response) {
                $scope.common.candidateList = response[0].data.Candidates;
                $scope.common.totalCount = response[0].data.TotalCount;
                $scope.common.totalPages = response[0].data.TotalPages;

                firstPageFlagSearch = 1;

                populateAdvanceSearchCandidateList($scope.common.candidateList, $scope.common.totalCount, $scope.common.totalPages, firstPageFlagSearch);

                //to show skills in tooltip as string
                angular.forEach($scope.common.candidateList, function(val, index) {
                    if (val.Skills) {
                        val.Skills = val.Skills.join();
                    }
                });
                blockUI.stop();
            },
            function(httpError) {
                $scope.closeAdvanceSearchModal();
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Search', httpError.data.Status, httpError.data.Message);
            });
    }

    function populateAdvanceSearchCandidateList(candidateList, totalCount, totalPages, pageFlag) {
        //if (angular.isDefined($scope.common.tblCandidateList)) {
        //    $scope.common.tblCandidateList.reload();
        //}
        //else 
        {
            $scope.common.tblCandidateList = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: $scope.common.ngTablePaginationCount,
                total: totalCount,
                getData: function($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    if (pageFlag != 1) {
                        /* sorting */
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function(val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */
                        //if (document.getElementById("select_all").checked == true) {
                        //    $("#select_all").click();
                        //}
                        var promise = XSeedApiFactory.getCandidateAdvanceSearchResult($scope.common.advanceSearchModel, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder);
                        promise.then(
                            function(response) {
                                $scope.common.candidateList = response[0].data.Candidates;
                                $scope.common.totalCount = response[0].data.TotalCount;
                                $scope.common.totalPages = response[0].data.TotalPages;

                                //to show skills in tooltip as string
                                angular.forEach($scope.common.candidateList, function(val, index) {
                                    if (val.Skills) {
                                        val.Skills = val.Skills.join();
                                    }
                                });
                                blockUI.stop();
                            },
                            function(httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('Search', httpError.data.Status, httpError.data.Message);
                            });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.common.candidateList);
                },
                $scope: $scope
            });
        }
    }

    /* Requirement Parser start */
    //$scope.getRequirementQueueListCount = function () {
    //    blockUI.start();

    //    var organizationUserId = $rootScope.userDetails.UserId;
    //    var promise = XSeedApiFactory.getRequirementQueueListCount(organizationUserId);
    //    promise.then(
    //      function (response) {
    //          $scope.requirementQueueCount = response[0].data;
    //          blockUI.stop();
    //      },
    //      function (httpError) {
    //          blockUI.stop();
    //          ExceptionHandler.handlerHTTPException('Parser', httpError.data.Status, httpError.data.Message);
    //      });
    //};

    function getVendorUnreadMailCount() {
        if ($rootScope.userDetails.Vendor_Read == 'True') {
            var promise = XSeedApiFactory.getUnreadMailCount($rootScope.userDetails.OrganizationId);
            promise.then(
                function(response) {
                    blockUI.stop();
                    $scope.requirementQueueCount = response[0].data;
                    //console.log($scope.requirementQueueCount);
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Vendor', httpError.data.Status, httpError.data.Message);
                });
        }
    };

    $scope.getRequirementQueueList = function() {
        blockUI.start();

        var organizationId = $rootScope.userDetails.OrganizationId;
        var userId = $rootScope.userDetails.UserId;
        $scope.common.pageSizeShow = undefined;
        $scope.common.pageNumberShow = undefined;

        var promise = XSeedApiFactory.getRequirementQueueDataList(userId, $scope.common.pageSizeShow, $scope.common.pageNumberShow);
        promise.then(
            function(response) {
                $scope.requirementQueue = response[0].data.parsedJobs;
                if ($scope.requirementQueue) {
                    angular.forEach($scope.requirementQueue, function(val, index) {
                        if (!$scope.isUndefinedOrNull($scope.requirementQueue[index].SkillList))
                            $scope.requirementQueue[index].SkillList = $scope.requirementQueue[index].SkillList.split("|");

                    });
                }
                var firstPageFlag = 0;
                $scope.requirementQueueTotalCount = response[0].data.TotalCount;
                $scope.requirementQueueTotalPages = response[0].data.TotalPages;
                firstPageFlag = 1;
                populateRequirementQueueList(userId, $scope.requirementQueue, $scope.requirementQueueTotalCount, $scope.requirementQueueTotalPages, firstPageFlag);
                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Parser', httpError.data.Status, httpError.data.Message);
            });
    };

    function populateRequirementQueueList(organizationUserId, requirementQueue, totalCount, totalPages, pageFlag) {
        //if (angular.isDefined($scope.tblRequirementList)) {
        //    $scope.tblRequirementList.reload();
        //}
        //else 
        {
            $scope.tblRequirementQueueList = new ngTableParams({
                page: 1,
                count: 10,
                sorting: {}
            }, {
                counts: [],
                total: totalCount,
                getData: function($defer, params) {
                    var sort, sortorder;

                    $scope.requirementQueuePageSizeShow = params.count();
                    $scope.requirementQueuePageNumberShow = params.page();
                    $scope.sortBy = params.sorting();

                    /* sorting */
                    if (pageFlag != 1) {
                        if ($scope.sortBy) {
                            angular.forEach($scope.sortBy, function(val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */
                        //if (document.getElementById("select_all").checked == true) {
                        //    $("#select_all").click();
                        //}
                        var promise = XSeedApiFactory.getRequirementQueueDataList(organizationUserId, $scope.requirementQueuePageSizeShow, $scope.requirementQueuePageNumberShow);
                        promise.then(
                            function(response) {
                                $scope.requirementQueue = response[0].data.parsedJobs;
                                $scope.requirementQueueTotalCount = response[0].data.TotalCount;
                                $scope.requirementQueueTotalPages = response[0].data.TotalPages;
                                if ($scope.requirementQueue) {
                                    angular.forEach($scope.requirementQueue, function(val, index) {
                                        if (!$scope.isUndefinedOrNull($scope.requirementQueue[index].SkillList))
                                            $scope.requirementQueue[index].SkillList = $scope.requirementQueue[index].SkillList.split("|");
                                    });
                                }

                                blockUI.stop();
                            },
                            function(httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('Parser', httpError.data.Status, httpError.data.Message);
                            });
                    }
                    pageFlag = 0;
                    params.total($scope.requirementQueueTotalCount);
                    $defer.resolve($scope.requirementQueue);
                },
                $scope: $scope
            });
        }
    }

    $scope.getAcceptedRequirementList = function() {
        blockUI.start();

        var promise = XSeedApiFactory.getAcceptedRequirementList($rootScope.userDetails.UserId);
        promise.then(
            function(response) {
                $scope.acceptedRequirementList = response[0].data;
                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });
    };


    $scope.acceptRequirement = function(id) {
        blockUI.start();
        var promise = XSeedApiFactory.acceptRequirement(id, $rootScope.userDetails.UserId, false);
        promise.then(
            function(response) {
                blockUI.stop();
                $timeout(function() {
                    $scope.getRequirementQueueList();
                    $scope.getRequirementList();
                }, 0);
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });
    };


    $scope.rejectRequirement = function(id) {
        XSeedAlert.swal({
            title: "Are you sure?",
            text: "You want to reject this requirement!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function() {
            blockUI.start();
            var promise = XSeedApiFactory.rejectRequirement(id);
            promise.then(
                function(response) {
                    blockUI.stop();
                    $timeout(function() {
                        $scope.getRequirementQueueList();
                    }, 0);
                    $location.path('requirementQueue');
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                });
        }, function(dismiss) {});
    };


    $scope.reviewRequirement = function(id) {
        var promise = XSeedApiFactory.getRequirementReview(id, $rootScope.userDetails.UserId);
        promise.then(
            function(response) {
                $scope.common.requirementModel = response[0].data;

                if (!$scope.isUndefinedOrNull($scope.common.requirementModel.CountryId) && !$scope.isEmpty($scope.common.requirementModel.CountryId)) {
                    $scope.getStateListByCountry($scope.common.requirementModel.CountryId);
                }

                if (!$scope.isUndefinedOrNull($scope.common.requirementModel.StateId) && !$scope.isEmpty($scope.common.requirementModel.StateId)) {
                    $scope.getCityListByState($scope.common.requirementModel.StateId);
                }

                if (!$scope.isUndefinedOrNull($scope.common.requirementModel.CompanyId) && !$scope.isEmpty($scope.common.requirementModel.CompanyId)) {
                    $scope.getCompanyContactsLookup($scope.common.requirementModel.CompanyId);
                }

                var list = [];
                for (var i = 0; i < $scope.common.requirementModel.OrganizationUser.length; i++) {
                    list.push($scope.common.requirementModel.OrganizationUser[i].Id);
                }
                $scope.common.requirementModel.OrganizationUserIdList = list;

                if (angular.isDefined($scope.common.requirementModel.OrganizationUserIdList)) {
                    for (var i = 0; i < $scope.common.requirementModel.OrganizationUserIdList.length; i++) {
                        var o = {};
                        o.Id = $scope.common.requirementModel.OrganizationUserIdList[i];
                        o.Name = '';
                        list.push(o);
                    }

                    $scope.common.requirementModel.OrganizationUser = list;
                }

                if ($scope.common.requirementModel.TechnicalSkills) {
                    $scope.common.requirementModel.SkillList = [];
                    $scope.common.requirementModel.SkillList = $scope.common.requirementModel.TechnicalSkills.split("|");
                    angular.forEach($scope.common.requirementModel.SkillList, function(val, index) {
                        $scope.lookup.skillListData.push({ value: val, label: val });
                    });
                }

                list = [];
                for (var i = 0; i < $scope.common.requirementModel.DegreeList.length; i++) {
                    list.push($scope.common.requirementModel.DegreeList[i].Id);
                }
                $scope.common.requirementModel.DegreeList = list;

                $scope.common.requirementModel.DriveFromDate = $filter('date')($scope.common.requirementModel.DriveFromDate, "MM/dd/yyyy");
                $scope.common.requirementModel.DriveToDate = $filter('date')($scope.common.requirementModel.DriveToDate, "MM/dd/yyyy");
                $scope.common.requirementModel.JobPostedDate = $filter('date')($scope.common.requirementModel.JobPostedDate, "MM/dd/yyyy");
                $scope.common.requirementModel.JobClosedDate = $filter('date')($scope.common.requirementModel.JobClosedDate, "MM/dd/yyyy");
                $scope.common.requirementModel.JobExpiryDate = $filter('date')($scope.common.requirementModel.JobExpiryDate, "MM/dd/yyyy");
                $scope.common.requirementModel.manipulationMode = "Create";
                $scope.common.requirementModel.reviewFlag = true;

                $scope.common.requirementModel.emailParserId = id;
                $scope.common.requirementModel.HtmlText = $scope.trustHtml($scope.common.requirementModel.HtmlText);
                $scope.getRequisitionID();

                $scope.jobLookupProcess();

                $scope.common.redirectedFrom = 'RequirementQueue';
                $location.path('createRequirement');
                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });
    };

    /* Requirement Parser end */

    /* QNote start */
    $scope.submitQNote = function() {


        if ($scope.qNoteModel.qNote) {
            var promise = XSeedApiFactory.createQNote(encodeURIComponent($scope.qNoteModel.qNote), $scope.common.selectedJob.Id);
            promise.then(
                function(response) {
                    blockUI.stop();
                    $scope.qNoteModel.qNote = '';
                    $scope.getQNoteList();
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                });
        } else {
            $("#error-summernote").show();
            return;
        }
    };
    /* QNote end */

    /************PayScaleExtension**************/

    $scope.displaySalaryCalculator = function() {
        if ($scope.common.selectedJob) {
            if ($scope.common.selectedJob.City != "") {
                return PayScaleExtension.displaySalaryCalculator($scope.common.selectedJob.JobTitle, $scope.common.selectedJob.City, $scope.common.selectedJob.State, $scope.common.selectedJob.Country);
            } else {
                if ($scope.showPayScaleSalryCalculator) {
                    $scope.showPayScaleSalryCalculator = false;
                } else {
                    $scope.PayScaleSalryCalculator = {};
                    $scope.PayScaleSalryCalculator.JobTitle = $scope.common.selectedJob.JobTitle;
                    $scope.PayScaleSalryCalculator.Country = $scope.common.selectedJob.Country;
                    $scope.PayScaleSalryCalculator.State = $scope.common.selectedJob.State;
                    $scope.PayScaleSalryCalculator.City = $scope.common.selectedJob.City;
                    $scope.showPayScaleSalryCalculator = true;
                }
            }
        }
    };

    $scope.salaryCalculator = function(salaryCalculatorForm) {
        if (new ValidationService().checkFormValidity(salaryCalculatorForm)) {
            return PayScaleExtension.displaySalaryCalculator($scope.PayScaleSalryCalculator.JobTitle, $scope.PayScaleSalryCalculator.City, $scope.PayScaleSalryCalculator.State, $scope.PayScaleSalryCalculator.Country);
        }
    };

    $scope.hidePayScaleSalryCalculator = function() {
        if ($scope.showPayScaleSalryCalculator) {
            $scope.PayScaleSalryCalculator = {};
            $scope.showPayScaleSalryCalculator = false;
        }
    };

    /************PayScaleExtension**************/

    /************ For Candidate Search **************/
    $scope.redirectToBooleanSearch = function() {
        $location.path('searchCandidate');
    };


    /************ For Candidate Search **************/

    /************ To Check Is Requirement In List After Edit **************/
    function IsRequirementInList(jobList) {
        angular.forEach(jobList, function(val, index) {
            if (val.Id == $scope.common.selectedJob.Id) {
                //console.log(val.Id + " = " + $scope.common.selectedJob.Id);
                return true;
            }
        });
        return false;
    }
    /************ To Check Is Requirement In List After Edit **************/

    /* Requirement module end */

    /* Activity QNotes Modal */

    $scope.openrequirementActivityQnotesModal = function(job) {
        $scope.common.selectedRow = job.Id;
        $scope.common.selectedJob = job;

        $scope.getQNoteList();
        $scope.getActivityList();
        requirementActivityQnotesModal.$promise.then(requirementActivityQnotesModal.show);
    }
    $scope.openrequirementPreviewModal = function () {

        requirementActivityPreviewModal.$promise.then(requirementActivityPreviewModal.show);
        
        console.log(reqTitle);
    }
    $scope.closerequirementActivityPreviewModal = function () {

        requirementActivityPreviewModal.$promise.then(requirementActivityPreviewModal.hide);
    }

    $scope.closerequirementActivityQnotesModal = function() {

        requirementActivityQnotesModal.$promise.then(requirementActivityQnotesModal.hide);
    }



    $scope.getAssociatedCandidateList = function() {
        var promise = XSeedApiFactory.getAssociatedCandidateList($scope.common.selectedJob.Id);
        promise.then(
            function(response) {
                $scope.common.associatedCandidates = response[0].data;
                console.log($scope.common.associatedCandidates);

            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });
    }

  $scope.requirementShareModal1 = function (SelectedJob) {
    $scope.common.selectedRow = SelectedJob.Id;
    $scope.common.selectedJob = SelectedJob;
      $scope.requirementShareModal();
  };

  $scope.requirementPostingModal1 = function (SelectedJob) {
    $scope.common.selectedRow = SelectedJob.Id;
    $scope.common.selectedJob = SelectedJob;
    $scope.requirementPostingModal();
  };

    /* Activity Qnotes Modal end */

  $scope.openRequirementDetailModal = function(SelectedJob) {
        $scope.common.selectedRow = SelectedJob.Id;
        $scope.common.selectedJob = SelectedJob;
        $scope.common.associatedCandidates = {};
        $scope.common.appliedCandidateList = {};
        if (!$('#' + SelectedJob.Id).parent('tr').next('.childtr').is(":visible")) {
            $scope.getcollpasingRequirementDetails();
        } else {
            $scope.bindcollapsingData($scope.common.requirementModel, $scope.common.selectedJob.Id);
      }
    };
    //***************************************************************************************//

    // Get the render context local to this controller (and relevant params).
    var renderContext = RequestContext.getRenderContext("standard.requirement");
    // The subview indicates which view is going to be rendered on the page.
    $scope.subview = renderContext.getNextSection();
    $scope.$on(
        "requestContextChanged",
        function() {
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {
                return;
            }
            // Update the view that is being rendered.
            $scope.subview = renderContext.getNextSection();
        }
    );

    $scope.collpaseAll = function() {
        $scope.requirementFilter.CollapseStatus = false;
        $('.childtr:visible').remove();

        $('.fa-chevron-down').removeClass("fa-chevron-down").addClass("fa-chevron-right");

        $('tr').removeClass("tr-collapse-color");

    }


    //delete QNote requirement
    $scope.deleteQNoterequirement = function(qnoteid, position) {

        XSeedAlert.swal({
            title: "Are you sure?",
            text: "This action will delete the Quick Note!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function() {

            if (qnoteid) {

                var promise = XSeedApiFactory.deleteQNoterequirement(qnoteid);
                promise.then(
                    function(response) {
                        $scope.qNoteList.splice(position, 1);

                    },
                    function(httpError) {
                        ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
                    });
            }
        })
    }


    $scope.CreateArrayToString = function(arr) {
        var names = "";
        for (var i = 0; i < arr.length; i++) {
            if (i >= 1) {
                names += ", " + arr[i].Name;
            }
            else {
                names += arr[i].Name;
            }
        }
        return names;

    }

    $scope.GetTechnicalSkillsRequirement = function (skills) {
        if (skills != null) {
            var commaskills = skills.replace(/,/g, ', ');
            return commaskills;

        }
    }

    $scope.showAppliedCandidates_click = function(candidateId) {
        $rootScope.appliedCandidateId = candidateId;
        $scope.showAppliedCandidates($scope.common.selectedJob);
    }





}
