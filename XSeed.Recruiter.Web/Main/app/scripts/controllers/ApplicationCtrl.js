'use strict';

XSEED_APP.controller('ApplicationCtrl', ApplicationCtrl);
XSEED_APP.constant('Constants', {
    Masters: {
        RequirementTitle: 'Requirement Title',
        RequirementType: 'Requirement Type',
        Degree: 'Degree',
        BusinessUnit: 'Business Unit',
        CompanySource: 'Company Source',
        CandidateSource: 'Candidate Source',
        CandidateCategories: 'Candidate Categories',
        CandidateGroups: 'Candidate Groups',
        SearchRequirementType: 'Search Requirement Type',
        VisaType: 'Visa Type'
    }
});

/**
 * @ngInject
 */
function ApplicationCtrl($scope, $rootScope, $route,$templateCache, $routeParams, RequestContext, $timeout, $q, $location, $window, configuration, i18nFactory, ExceptionHandler, EventBus, $log, _, ipCookie, blockUI, ModalFactory, XSeedAlert, localStorageService, $uibModal, XSeedApiFactory, ngTableParams, $filter, ValidationService, $sce, Constants, $interval, blockUIConfig, $compile) {
    var xSeedValidation = new ValidationService();
    xSeedValidation.setGlobalOptions({ debounce: 1500, scope: $scope, preValidateFormElements: true });

    $rootScope.token = {};
    $rootScope.userDetails = {};
    $rootScope.metadata = {};
    $scope.genericLookupFilter = {};
    $scope.genericLookup = {};
    $scope.editedValue = false;
    $rootScope.userHasToken = false;
    $scope.OraganizationUsers = {};
    $rootScope.isValidUser = {};
    $scope.skills = [{}];
    $scope.requirementFilter = {};
    $scope.requirementTitleModel = {};
    $scope.requirementTitleflag = false;
    $rootScope.requirementTitleList = {};

    $scope.requirementTypeList = {};
    $scope.visaTypeList = {};
    $scope.degreeList = {};
    $scope.businessUnitList = {};
    $scope.exportToExcelData = {};

  var requirementShareModal = ModalFactory.requirementShareModal($scope);
  var requirementPostingModal = ModalFactory.requirementPostingModal($scope);
    //$scope.common.candidateSearchBySourceModel = {};
    //Applied Candidate

    var vm = this;
    // Offcanvas and horizontal menu
    vm.menu = false;
    vm.toggleMenu = function() {
        vm.menu = !vm.menu;
    };

    if (angular.isUndefined(localStorageService.get("token"))) {
        $rootScope.token = {};
    } else {
        $rootScope.token.key = localStorageService.get("token");
    }

    if (angular.isUndefined(localStorageService.get("userDetails")) || localStorageService.get("userDetails") == null) {
        $rootScope.userDetails = {};
    } else {
        $rootScope.userDetails = localStorageService.get("userDetails");
        $rootScope.userDetails.ChildOrganizations = localStorageService.get("ChildOrganizations");
        $scope.FirstName = $rootScope.userDetails.FirstName;
        $scope.LastName = $rootScope.userDetails.LastName;
    }

    $scope.REGX_P = XSEED_HELPER.ValidationPatterns();
    XSeedApiFactory.setConfiguration(configuration);

    $scope.registerRequest = {};
    $scope.loginRequest = {};

    $scope.$watch('location.search()', function() {
        init();
    }, true);
    //Chat modal
    $scope.openChat = function() {
        var chatModalInstance = $uibModal.open({
            templateUrl: 'chatModal.html',
            controller: 'chatModalInstanceCtrl',
            controllerAs: 'chatmodal',
            windowClass: 'sidebar-modal chat-panel',
            backdropClass: 'chat-backdrop'
        });
    };

    setInterval(function() {
        if ($location.path() == "/organizationProfile" || $location.path() == "/editOrganization" || $location.path() == "/systemUsers" ||
            $location.path() == "/createNewuser" || $location.path() == "/userRoles" || $location.path() == "/createUserRole" || $location.path() == "/changePassword") {
            $scope.SubmenuMode = "open";
            // console.log($location.path());
            $("#" + $scope.SubmenuMode).addClass("open")
        } else {
            $scope.SubmenuMode = "";
        }
        if ($location.path() == "/requirementReport" || $location.path() == "/candidateReport" || $location.path() == "/activityReport" ||
        $location.path() == "/submissionReport" || $location.path() == "/clientReport" || $location.path() == "/closureReport") {
            $scope.SubmenuModeReport = "open";
            $("#" + $scope.SubmenuModeReport).addClass("open")
        } else {
            $scope.SubmenuModeReport = "";
        }
    }, 0);


    //Initialization Function
    function init() {

        $scope.date = new Date();

        getMetaData();

        $scope.filter = {};
        $scope.flag = { requirementLookupLoaded: false, candidateLookupLoaded: false, advanceSearchFlag: false, searchCandidatesBySource: false, isEditSchedule: false };
        $scope.constants = { sendJobMail: 'sendJobMail', sendCandidateMail: 'sendCandidateMail', showActivityEmail: 'showActivityEmail', submitCandidateByEmail: 'submitCandidateByEmail', sectionRequirements: 'Requirements', sectionCandidates: 'Candidates', sectionSubmissions: 'Submissions', submissionInternalFeedback: 'Internal Feedback', submissionCandidateFeedback: 'Candidate Feedback', submissionClientFeedback: 'Client Feedback', };
        $scope.saveModelState = {};
        //$scope.common.candidateModel = new API.CandidateRequest();

        $scope.lookup = {
            company: '',
            jobType: '',
            jobStatus: '',
            organizationUser: '', //Requirement
            skill: '',
            maritalStatus: '',
            visaType: '',
            degree: '',
            jobTitle: '',
            gender: '',
            requirement: '',
            skillListData: '',
            degreeListData: '',
            languageList: '' //Candidate
        };

        //Applied Candidate
        $scope.appliedCadidateData = {};

        $scope.dataSubmissiomEmail = {};

        $scope.IsDuplicateVisaType = false;

        $scope.appliedCandidateFilter = "";

        $scope.common = {};

        $scope.common.commonEmailModal = ModalFactory.commonEmailModal($scope);
        $scope.common.advanceSearchModal = ModalFactory.advanceSearchModal($scope);
        $scope.common.commonScheduleModal = ModalFactory.commonScheduleModal($scope);
        $scope.common.commonReminderModal = ModalFactory.commonReminderModal($scope);
        $scope.common.companyContactModal = ModalFactory.companyContactModal($scope);
        $scope.common.requirementDetailModal = ModalFactory.requirementDetailModal($scope);
        $scope.common.candidateDetailModal = ModalFactory.candidateDetailModal($scope);
        $scope.common.candidatesToSubmitModal = ModalFactory.candidatesToSubmitModal($scope);
        $scope.common.resumePreviewModal = ModalFactory.resumePreviewModal($scope);
        $scope.common.addCityMasterModal = ModalFactory.addCityMasterModal($scope);
        //$scope.common.bulkUploadCandidateModal = ModalFactory.bulkUploadCandidateModal($scope);

        var candidateSubmissionEmailModal = ModalFactory.candidateSubmissionEmailModal($scope);
        $scope.common.advanceSearchModel = {};
        $scope.common.ngTablePaginationPage = 8;
        $scope.common.ngTablePaginationCount = [15, 20, 25];

        $scope.common.qNoteList = {};
        //$timeout(function () {
        //    $scope.callAtInterval();
        //}, 5000);
        //$location.path('login');

        if (!$scope.isUndefinedOrNull($rootScope.userDetails.UserName) && $rootScope.userDetails.UserName != null) {
            $rootScope.isValidUser = $interval($scope.CheckValidUser, configuration.XSEED_USER_VALID_TIME_IN_SEC);
        } else {
            $interval.cancel($rootScope.isValidUser);
        }
        // $scope.GetActiveSubscriptionPlanDetails();
    };

    // $scope.GetActiveSubscriptionPlanDetails = function() {
    //     alert($rootScope.userDetails.OrganizationId);
    //     var promise = XSeedApiFactory.GetActiveSubscriptionPlanDetails($rootScope.userDetails.OrganizationId);
    //     promise.then(function(response) {
    //             console.log(response.data);
    //             $scope.requirementTitleList = response[0].data;
    //         },
    //         function(httpError) {
    //             ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
    //         });
    // }


    $scope.TrustAsHTML = function(string) {
        return $sce.trustAsHtml(string);
    }

    $scope.getRequirementTitleLookup = function() {

        //$scope.ClearFilter();
        var promise = XSeedApiFactory.getJobTitleLookup($rootScope.userDetails.OrganizationId);

        promise.then(function(response) {
                $scope.requirementTitleList = response[0].data;
                $scope.tableParams = new ngTableParams({ page: 1, count: 5 }, { data: response[0].data, counts: [] });
            },
            function(httpError) {
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });
    }

    $scope.getRequirementTypeLookup = function() {
        var promise = XSeedApiFactory.getJobTypeLookup($rootScope.userDetails.OrganizationId);

        promise.then(function(response) {
                $scope.requirementTypeList = response[0].data;
                $scope.tableParams = new ngTableParams({ page: 1, count: 5 }, { data: response[0].data, counts: [] });
            },
            function(httpError) {
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });
    }

    $scope.getBusinessUnitLookup = function() {
        var promise = XSeedApiFactory.getBusinessUnit($rootScope.userDetails.OrganizationId);

        promise.then(function(response) {
                $scope.businessUnitList = response[0].data;
                $scope.tableParams = new ngTableParams({ page: 1, count: 5 }, { data: response[0].data, counts: [] });
            },
            function(httpError) {
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });
    }

    $scope.getvisaTypeLookup = function() {
        var promise = XSeedApiFactory.getVisaTypeLookup();

        promise.then(
            function(response) {
                $scope.visaTypeList = response[0].data;
                blockUI.stop();
            },
            function(httpError) {
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });
    }

    $scope.getDegreeLookup = function() {
        var promise = XSeedApiFactory.getJobDegreesLookup($rootScope.userDetails.OrganizationId);

        promise.then(
            function(response) {
                $scope.degreeList = response[0].data;
                blockUI.stop();
            },
            function(httpError) {
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });
    }

    // Get the token for API Call
    function getMetaData() {

        blockUI.start();
        var promise = XSeedApiFactory.getMetaData();
        promise.then(
            function(response) {
                blockUI.stop();
                $rootScope.metadata = response[0].data;

            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('AccessTokenRequest', null, httpError);
            });
    };

    /*Check valid User for looging out the system if user is logged in*/
    $scope.CheckValidUser = function() {
        blockUIConfig.autoBlock = false;
        var promise = XSeedApiFactory.CheckValidUser($rootScope.userDetails.UserId);
        promise.then(
            function(response) {
                if (response[0].data != true) {
                    $location.path('login');
                    $interval.cancel($rootScope.isValidUser);
                    XSeedAlert.swal({
                        title: 'Warning!',
                        text: 'You are logged out from the system because you are no longer authorized to access the system. Please contact administrator for more details.',
                        type: "warning",
                        customClass: "xseed-error-alert"
                    }).then(function() {
                        localStorageService.clearAll();
                    }, function(dismiss) {
                        localStorageService.clearAll();
                    })
                } else {
                    return;
                }
            },
            function(httpError) {
                ExceptionHandler.handlerHTTPException('AccessTokenRequest', null, httpError);
            });
    };

    // Multiple Controller Call Start-
    //summernote options 
    $scope.summernoteOptions = {
        height: 150,
        popover: {},
        callbacks: {
            onChange: function(e) {
                $('#error-summernote').hide();
            }
        }
    };

    //currency options
    $scope.currencyOptions = {
        aSign: '$',
        vMin: '0.00'
    };

    //selector plugin create new function
    $scope.createFunction = function(input) {
        // format the option and return it
        return {
            value: $scope.lookup.skillListData.length,
            label: input
        };
    };
    /*------------------------------------ First row selection----------------------------------*/

    //for requirement
    $scope.setClickedRow = function(job) {
        if (job) {
            $scope.common.selectedRow = job.Id;
            $scope.common.selectedJob = job;

            //  $scope.getQNoteList();
            //   $scope.getActivityList();
            //  $scope.appliedCandidatesForJob($scope.common.selectedJob.Id);
        }
    };

    //for candidate
    $scope.setClickedRowCandidate = function(candidate) {
        if (candidate) {
            $scope.common.selectedRowCandidate = candidate._id;
            $scope.common.selectedCandidate = candidate;
            if (candidate) {
                $scope.getQNoteListCandidate();
                $scope.getActivityListCandidate();
            }

        }
    };

    /*------------------------------------ First row selection----------------------------------*/

    /* QNote start requirement */
    $scope.getQNoteList = function() {
        blockUI.start();
        var promise = XSeedApiFactory.getQNoteList($scope.common.selectedJob.Id);
        promise.then(
            function(response) {
                $scope.qNoteList = response[0].data;
                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });
    };
    /* QNote end requirement*/

    /* Activity start requirement*/
    $scope.getActivityList = function() {
        blockUI.start();
        var promise = XSeedApiFactory.getActivityList($scope.common.selectedJob.Id);
        promise.then(
            function(response) {
                $scope.activityList = response[0].data;
                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });
    };
    /* Activity end requirement*/

    /* QNote start candidate */
    $scope.getQNoteListCandidate = function() {
        blockUI.start();
        var promise = XSeedApiFactory.getQNoteListCandidate($scope.common.selectedCandidate._id);
        promise.then(
            function(response) {
                $scope.qNoteListCandidate = response[0].data;
                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Candidate', httpError.data.Status, httpError.data.Message);
            });
    };
    /* QNote end candidate*/

    /* Activity start candidate*/
    $scope.getActivityListCandidate = function() {
        blockUI.start();
        var promise = XSeedApiFactory.getActivityListCandidate($scope.common.selectedCandidate._id);
        promise.then(
            function(response) {
                $scope.activityListCandidate = response[0].data;
                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Candidate', httpError.data.Status, httpError.data.Message);
            });
    };
    /* Activity end candidate*/

    /* Add New City To Master*/
    $scope.openAddNewCityToMasterModal = function(countryId, stateId) {
        if (countryId && stateId) {
            $scope.common.addCityMaster = {};
            //show coutry and state selected
            $scope.common.addCityMaster.CountryId = countryId;
            $scope.common.addCityMaster.StateId = stateId;

            $scope.common.addCityMasterModal.$promise.then($scope.common.addCityMasterModal.show);

            setTimeout(function() {
                $("select.select2").select2({ allowClear: true });
            }, 1200);
        }
    };

    $scope.closeAddNewCityToMasterModal = function() {
        $scope.common.addCityMasterModal.$promise.then($scope.common.addCityMasterModal.hide);
        $scope.common.addCityMaster = {};
    };


    $scope.refresh = function () {
       
        if ($location.path() == "/dashboard")   {
            $location.path("/requirementList");
            setTimeout(function () {
                $location.path("/dashboard");
            });
        }
        else {
            $location.path("/dashboard");
        }        
        
               
    };

    $scope.addCityMaster = function(addCityMasterForm) {
        if (new ValidationService().checkFormValidity(addCityMasterForm)) {
            XSeedAlert.swal({
                title: "Are you sure you want add city " + $scope.common.addCityMaster.City + "?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes",
                cancelButtonText: "Cancel",
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function() {
                blockUI.start();
                var promise = XSeedApiFactory.addCityMaster($scope.common.addCityMaster.City, $scope.common.addCityMaster.StateId);
                promise.then(
                    function(response) {
                        var cityId = response[0].data;
                        if (cityId) {
                            $scope.cityList.push({ Id: cityId, Name: $scope.common.addCityMaster.City });
                        }

                        XSeedAlert.swal({
                            title: 'Success!',
                            text: 'City added successfully!',
                            type: "success",
                            customClass: "xseed-error-alert"
                        }).then(function() {
                            setCityMasterValueToModal(cityId);
                            $scope.closeAddNewCityToMasterModal();
                        }, function(dismiss) {
                            blockUI.stop();
                        })
                    },
                    function (httpError) {
                        //City Already Exists
                        blockUI.stop();
                        if (httpError.status == 400) {
                            ExceptionHandler.handlerPopup('LookUp', httpError.data, 'Warning', 'error');
                        } else {
                            blockUI.stop();
                            var data = httpError.data.Message;
                            XSeedAlert.swal({
                                title: "City "+ $scope.common.addCityMaster.City + " Already Exists",
                                text: data,
                                type: "warning",
                                customClass: "xseed-error-alert",
                            }).then(function (dismiss) {
                                $scope.addCityMasterForm;
                                $('#addcityid').val("");
                            }, function (dismiss) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
                            });
                        }
                            });
                blockUI.stop();
            }, function(dismiss) {});
        }
    };

    function setCityMasterValueToModal(cityId) {
        var createUserURL = '/createNewuser';
        var editUserURL = '/editUser';

        var createRequirementURL = '/createRequirement';
        var editRequirementURL = '/editRequirement';

        var createCandidateURL = '/createCandidate';
        var editCandidateURL = '/editCandidate';

        var createCompanyURL = '/createCompany';
        var editCompanyURL = '/editCompany';

        var createContactURL = '/createContact';
        var editContactURL = '/editContact';

        //organization user
        if ($location.path().toLowerCase() == createUserURL.toLowerCase()) {
            // $scope.common.OrganizationUserModel.CityId = cityId;
            $scope.common.newOrganizationUserDetails.CityId = cityId;
        }


        if ($location.path().toLowerCase() == editUserURL.toLowerCase()) {
            //$scope.common.OrganizationUserModel.CityId = cityId;
            $scope.common.currentOrganizationUserDetails.CityId = cityId;
        }

        //company
        if ($location.path().toLowerCase() == createCompanyURL.toLowerCase() || $location.path().toLowerCase() == editCompanyURL.toLowerCase()) {
            $scope.common.companyModel.CityId = cityId;
        }

        //company contact
        if ($location.path().toLowerCase() == createContactURL.toLowerCase()) {
            $scope.common.createCompanyContactRequest.CityId = cityId;
        }

        if ($location.path().toLowerCase() == editContactURL.toLowerCase()) {
            $scope.common.contactDetail.CityId = cityId;
        }

        //requirement
        if ($location.path().toLowerCase() == createRequirementURL.toLowerCase() || $location.path().toLowerCase() == editRequirementURL.toLowerCase()) {
            $scope.common.requirementModel.CityId = cityId;
        }

        //candidate
        if ($location.path().toLowerCase() == createCandidateURL.toLowerCase() || $location.path().toLowerCase() == editCandidateURL.toLowerCase()) {
            $scope.common.candidateModel.CityId = cityId;
        }
    };
    /* Add New City To Master*/

    /*------------------------------------ Generic master popup call----------------------------------*/


    var genericMasterModal = ModalFactory.genericMasterModal($scope);
    $scope.openGenericMasterModal = function(genericLookUpType) {

        $scope.genericLookUpType = genericLookUpType;
        var promise;

        if ($scope.genericLookUpType === Constants.Masters['RequirementTitle']) {
            promise = XSeedApiFactory.getJobTitleLookup($rootScope.userDetails.OrganizationId);
        }

        if ($scope.genericLookUpType === Constants.Masters['RequirementType']) {
            promise = XSeedApiFactory.getJobTypeLookup($rootScope.userDetails.OrganizationId);
        }

        if ($scope.genericLookUpType === Constants.Masters['SearchRequirementType']) {
            promise = XSeedApiFactory.getJobTypeLookup($rootScope.userDetails.OrganizationId);
        }

        //if ($scope.genericLookUpType === Constants.Masters['Degree']) {
        //    promise = XSeedApiFactory.getJobDegreesLookup();
        //}

        if ($scope.genericLookUpType === Constants.Masters['CompanySource']) {
            promise = XSeedApiFactory.getCompanySourceList($rootScope.userDetails.OrganizationId);
        }

        if ($scope.genericLookUpType === Constants.Masters['BusinessUnit']) {
            promise = XSeedApiFactory.getBusinessUnit($rootScope.userDetails.OrganizationId);
        }

        if ($scope.genericLookUpType === Constants.Masters['CandidateSource']) {
            promise = XSeedApiFactory.getCandidateSource($rootScope.userDetails.OrganizationId);
        }

        if ($scope.genericLookUpType === Constants.Masters['CandidateCategories']) {
            promise = XSeedApiFactory.getCandidateCategories($rootScope.userDetails.OrganizationId);
        }

        if ($scope.genericLookUpType === Constants.Masters['CandidateGroups']) {
            promise = XSeedApiFactory.getCandidateGroups($rootScope.userDetails.OrganizationId);
        }

        promise.then(
            function(response) {
                $scope.genericLookup = response[0].data;
                $scope.tableParams = new ngTableParams({ page: 1, count: 5 }, { data: response[0].data, counts: [] });
            },
            function(httpError) {
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });

        genericMasterModal.$promise.then(genericMasterModal.show);
    };

    $scope.closeGenericMasterModal = function() {
        clearLookUpValueTableParams();
        genericMasterModal.$promise.then(genericMasterModal.hide);
        if ($scope.genericLookUpType === Constants.Masters['RequirementTitle']) {
            if (document.getElementById('lookupJobTitle')) {
                document.getElementById("lookupJobTitle").focus();
            }
        }

        if ($scope.genericLookUpType === Constants.Masters['RequirementType']) {
            if (document.getElementById('lookupJobType')) {
                document.getElementById("lookupJobType").focus();
            }
        }

        if ($scope.genericLookUpType === Constants.Masters['BusinessUnit']) {
            if (document.getElementById('lookupBusinessUnit')) {
                document.getElementById("lookupBusinessUnit").focus();
            }
        }

        if ($scope.genericLookUpType === Constants.Masters['CompanySource']) {
            if (document.getElementById('lookupCompanySource')) {
                document.getElementById("lookupCompanySource").focus();
            }
        }

        if ($scope.genericLookUpType === Constants.Masters['CandidateSource']) {
            if (document.getElementById('lookupCandidateSource')) {
                document.getElementById("lookupCandidateSource").focus();
            }
        }

        if ($scope.genericLookUpType === Constants.Masters['CandidateCategories']) {
            if (document.getElementById('lookupCandidateCategories')) {
                document.getElementById("lookupCandidateCategories").focus();
            }
        }

        if ($scope.genericLookUpType === Constants.Masters['CandidateGroups']) {
            if (document.getElementById('lookupCandidateGroups')) {
                document.getElementById("lookupCandidateGroups").focus();
            }
        }

    };

    function clearLookUpValueTableParams() {
        if ($scope.genericLookUpType === Constants.Masters['Degree']) {
            $scope.tableMultiValueParams = new ngTableParams({ page: 1, count: 5 }, { data: null, counts: [] });
            $scope.tableMultiValueParams.reload();
        } else {
            $scope.tableParams = new ngTableParams({ page: 1, count: 5 }, { data: null, counts: [] });
            $scope.tableParams.reload();
        }
    };

    var genericCreateEditMasterModal = ModalFactory.genericCreateEditMasterModal($scope);
    $scope.openGenericCreateEditMasterModal = function(genericLookup, genericLookupEvent) {
        $scope.duplicateLookupName = false;
        $scope.common.LookupName = {};
        $scope.genericLookupEvent = genericLookupEvent;
        if ($scope.genericLookupEvent === 'Edit') {
            $scope.genericLookup.Id = genericLookup.Id;
            $scope.genericLookup.Name = genericLookup.Name;
            $scope.common.LookupName = genericLookup.Name;
            $scope.genericLookup.Description = genericLookup.Description;

            genericMasterModal.$promise.then(genericCreateEditMasterModal.show);
        } else if ($scope.genericLookupEvent === 'Delete') {
            $scope.genericLookup.Id = genericLookup.Id;

            $scope.deleteGenericMaster($scope.genericLookup.Id);
        } else {
            $scope.genericLookup.Id = "";
            $scope.genericLookup.Name = "";
            $scope.genericLookup.Description = "";

            genericMasterModal.$promise.then(genericCreateEditMasterModal.show);
        }
    }

    $scope.closeGenericCreateEditMasterModal = function() {
        genericMasterModal.$promise.then(genericCreateEditMasterModal.hide);
    }

    $scope.SaveLookUp = function(genericCreateEditLookupPopupForm) {
        if (new ValidationService().checkFormValidity(genericCreateEditLookupPopupForm)) {

            var request = {
                Id: $scope.genericLookup.Id,
                OrganizationId: $rootScope.userDetails.OrganizationId,
                Name: $scope.genericLookup.Name,
                Description: $scope.genericLookup.Description
            }

            var promise;
            if ($scope.genericLookupEvent === 'New') {
                if ($scope.genericLookUpType === Constants.Masters['RequirementTitle']) {
                    promise = XSeedApiFactory.createJobTitleLookup(request);
                    promise.then(
                        function(response) {
                            blockUI.stop();
                            SaveLookUpCompleteAction();
                        },
                        function(httpError) {
                            blockUI.stop();
                            ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                        });
                }

                if ($scope.genericLookUpType === Constants.Masters['RequirementType'] || $scope.genericLookUpType === Constants.Masters['SearchRequirementType']) {
                    promise = XSeedApiFactory.createJobTypeLookup(request);
                    promise.then(
                        function(response) {
                            blockUI.stop();
                            SaveLookUpCompleteAction();
                        },
                        function(httpError) {
                            blockUI.stop();
                            ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                        });
                }

                if ($scope.genericLookUpType === Constants.Masters['Degree']) {
                    promise = XSeedApiFactory.createJobDegreesLookup(request);
                    promise.then(
                        function(response) {
                            blockUI.stop();
                            SaveMultiValueLookUpCompleteAction();
                        },
                        function(httpError) {
                            blockUI.stop();
                            ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                        });
                }

                // Job lookup type
                if ($scope.genericLookUpType === Constants.Masters['VisaType']) {
                    promise = XSeedApiFactory.createJobVisaTypeLookup(request);
                    promise.then(
                        function(response) {
                            blockUI.stop();
                            SaveMultiValueLookUpCompleteAction();
                        },
                        function(httpError) {
                            blockUI.stop();
                            ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                        });
                }

                if ($scope.genericLookUpType === Constants.Masters['CompanySource']) {
                    promise = XSeedApiFactory.createCompanySource(request);
                    promise.then(
                        function(response) {
                            blockUI.stop();
                            SaveLookUpCompleteAction();
                        },
                        function(httpError) {
                            blockUI.stop();
                            ExceptionHandler.handlerHTTPException('Company', httpError.data.Status, httpError.data.Message);
                        });
                }


                if ($scope.genericLookUpType === Constants.Masters['BusinessUnit']) {
                    promise = XSeedApiFactory.createNewBusinessUnit(request);
                    promise.then(
                        function(response) {
                            blockUI.stop();
                            SaveLookUpCompleteAction();
                        },
                        function(httpError) {
                            blockUI.stop();
                            ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                        });
                }

                if ($scope.genericLookUpType === Constants.Masters['CandidateSource']) {
                    promise = XSeedApiFactory.createCandidateSource(request);
                    promise.then(
                        function(response) {
                            blockUI.stop();
                            SaveLookUpCompleteAction();
                        },
                        function(httpError) {
                            blockUI.stop();
                            ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
                        });
                }

                if ($scope.genericLookUpType === Constants.Masters['CandidateCategories']) {
                    promise = XSeedApiFactory.createCandidateCategory(request);
                    promise.then(
                        function(response) {
                            blockUI.stop();
                            SaveLookUpCompleteAction();
                        },
                        function(httpError) {
                            blockUI.stop();
                            ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
                        });
                }

                if ($scope.genericLookUpType === Constants.Masters['CandidateGroups']) {
                    promise = XSeedApiFactory.createCandidateGroup(request);
                    promise.then(
                        function(response) {
                            blockUI.stop();
                            SaveLookUpCompleteAction();
                        },
                        function(httpError) {
                            blockUI.stop();
                            ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
                        });
                }


            } else if ($scope.genericLookupEvent === 'Edit') {

                if ($scope.genericLookUpType === Constants.Masters['RequirementTitle']) {
                    promise = XSeedApiFactory.updateJobTitleLookup(request);
                    promise.then(
                        function(response) {
                            blockUI.stop();
                            SaveLookUpCompleteAction();
                        },
                        function(httpError) {
                            blockUI.stop();
                            ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                        });
                }

                if ($scope.genericLookUpType === Constants.Masters['RequirementType'] || $scope.genericLookUpType === Constants.Masters['SearchRequirementType']) {
                    promise = XSeedApiFactory.updateJobTypeLookup(request);
                    promise.then(
                        function(response) {
                            blockUI.stop();
                            SaveLookUpCompleteAction();
                        },
                        function(httpError) {
                            blockUI.stop();
                            ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                        });
                }

                if ($scope.genericLookUpType === Constants.Masters['Degree']) {
                    promise = XSeedApiFactory.updateJobDegreesLookup(request);
                    promise.then(
                        function(response) {
                            blockUI.stop();
                            SaveMultiValueLookUpCompleteAction();
                        },
                        function(httpError) {
                            blockUI.stop();
                            ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                        });
                }

                // Job visa type lookup
                if ($scope.genericLookUpType === Constants.Masters['VisaType']) {
                    promise = XSeedApiFactory.updateJobVisaTypeLookup(request);
                    promise.then(
                        function(response) {
                            blockUI.stop();
                            SaveMultiValueLookUpCompleteAction();
                        },
                        function(httpError) {
                            blockUI.stop();
                            ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                        });
                }

                if ($scope.genericLookUpType === Constants.Masters['CompanySource']) {
                    promise = XSeedApiFactory.updateCompanySource(request);
                    promise.then(
                        function(response) {
                            blockUI.stop();
                            SaveLookUpCompleteAction();
                        },
                        function(httpError) {
                            blockUI.stop();
                            ExceptionHandler.handlerHTTPException('Company', httpError.data.Status, httpError.data.Message);
                        });
                }


                if ($scope.genericLookUpType === Constants.Masters['BusinessUnit']) {
                    promise = XSeedApiFactory.updateBusinessUnit(request);
                    promise.then(
                        function(response) {
                            blockUI.stop();
                            SaveLookUpCompleteAction();
                        },
                        function(httpError) {
                            blockUI.stop();
                            ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
                        });
                }
                if ($scope.genericLookUpType === Constants.Masters['CandidateSource']) {
                    promise = XSeedApiFactory.updateCandidateSource(request);
                    promise.then(
                        function(response) {
                            blockUI.stop();
                            SaveLookUpCompleteAction();
                        },
                        function(httpError) {
                            blockUI.stop();
                            ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
                        });
                }

                if ($scope.genericLookUpType === Constants.Masters['CandidateCategories']) {
                    promise = XSeedApiFactory.updateCandidateCategory(request);
                    promise.then(
                        function(response) {
                            blockUI.stop();
                            SaveLookUpCompleteAction();
                        },
                        function(httpError) {
                            blockUI.stop();
                            ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
                        });
                }

                if ($scope.genericLookUpType === Constants.Masters['CandidateGroups']) {
                    promise = XSeedApiFactory.updateCandidateGroup(request);
                    promise.then(
                        function(response) {
                            blockUI.stop();
                            SaveLookUpCompleteAction();
                        },
                        function(httpError) {
                            blockUI.stop();
                            ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
                        });
                }

            }
        }
    }

    $scope.duplicateCheckNSaveLookUp = function(genericCreateEditLookupPopupForm, lookupName) {
        blockUI.stop();
        $scope.duplicateLookupName = false;
        $scope.existingValue = $scope.common.LookupName;
        var organizationId = $rootScope.userDetails.OrganizationId;
        if ($scope.genericLookupEvent === 'Edit') {
            $scope.editedValue = true;
        } else {
            $scope.editedValue = false;
            $scope.existingValue = null;
        }
        var promise;
        if (new ValidationService().checkFormValidity(genericCreateEditLookupPopupForm)) {
            if (lookupName) {
                promise = XSeedApiFactory.IsDuplicateLookupName(encodeURIComponent(lookupName), $scope.genericLookUpType, organizationId, $scope.editedValue, $scope.existingValue);


                promise.then(
                    function(response) {
                        $scope.duplicateLookupName = response[0].data;

                        if (($scope.genericLookupEvent === 'Edit') && ($scope.common.LookupName == lookupName)) {
                            $scope.duplicateLookupName = false;
                        }

                        if (!$scope.duplicateLookupName) {
                            $scope.SaveLookUp(genericCreateEditLookupPopupForm);
                        }
                        blockUI.stop();
                    },
                    function(httpError) {
                        blockUI.stop();
                        ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                    });
            } else {
                $scope.duplicateLookupName = false;
            }
        }
    };

    function SaveMultiValueLookUpCompleteAction() {
        $scope.genericLookup.Name = "";
        $scope.genericLookup.Description = "";
        $scope.closeGenericCreateEditMasterModal();
        $scope.closeGenericMasterMultiValueModal
        $scope.closeGenericMasterModal();
        $scope.tableMultiValueParams.reload();
        $scope.openGenericMasterMultiValueModal($scope.genericLookUpType);

    }

    function SaveLookUpCompleteAction() {
        $scope.genericLookup.Name = "";
        $scope.genericLookup.Description = "";
        $scope.closeGenericCreateEditMasterModal();
        $scope.closeGenericMasterModal();
        $scope.tableParams.reload();
        $scope.openGenericMasterModal($scope.genericLookUpType);

    }

    $scope.SetModalValue = function(value) {
        //var masterData = {};
        //masterData.masterId = value.Id;
        //masterData.masterValue = value.Name;
        //masterData.masterType = $scope.genericLookUpType;
        //$rootScope.$broadcast('masterData', masterData);

        if ($scope.genericLookUpType === Constants.Masters['RequirementTitle']) {

            $scope.common.requirementModel.JobTitle = value.Name;

        }
        if ($scope.genericLookUpType === Constants.Masters['RequirementType']) {
            $scope.common.requirementModel.JobType = value.Name;
            $scope.common.requirementModel.JobTypeId = value.Id;
        }

        if ($scope.genericLookUpType === Constants.Masters['SearchRequirementType']) {
            $scope.common.candidateSearchBySourceModel.Type = value.Name;
        }

        if ($scope.genericLookUpType === Constants.Masters['Degree']) {
            $scope.common.requirementModel.DegreeNameList = value.Name;

            var list = [];
            //$scope.common.requirementModel.DegreeList = [];
            list.push(value.Id);
            $scope.common.requirementModel.DegreeList = list;
        }

        // Job visa lookup 
        if ($scope.genericLookUpType === Constants.Masters['VisaType']) {
            $scope.common.requirementModel.VisaTypeNameList = value.Name;

            var list = [];
            list.push(value.Id);
            $scope.common.requirementModel.VisaTypeList = list;
        }

        if ($scope.genericLookUpType === Constants.Masters['BusinessUnit']) {
            $scope.common.requirementModel.BusinessUnit = value.Id;
            $scope.common.requirementModel.BusinessUnitName = value.Name;
        }

        if ($scope.genericLookUpType === Constants.Masters['CompanySource']) {
            $scope.common.companyModel.CompanySourceId = value.Id;
            $scope.common.companyModel.companySourceName = value.Name;
        }

        if ($scope.genericLookUpType === Constants.Masters['CandidateSource']) {
            $scope.common.candidateModel.SourceId = value.Id;
            $scope.common.candidateModel.Source = value.Name;
        }

        if ($scope.genericLookUpType === Constants.Masters['CandidateCategories']) {
            $scope.common.candidateModel.CategoryId = value.Id;
            $scope.common.candidateModel.Categories = value.Name;
        }

        if ($scope.genericLookUpType === Constants.Masters['CandidateGroups']) {
            $scope.common.candidateModel.GroupId = value.Id;
            $scope.common.candidateModel.Groups = value.Name;
        }
        $scope.closeGenericMasterModal();
        $scope.closeGenericMasterMultiValueModal();
    }

    $scope.deleteGenericMaster = function(genericLookupId) {
        var promise;
        XSeedAlert.swal({
            title: "Are you sure?",
            text: "This action will delete the lookup value!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function() {
            blockUI.start();
            if ($scope.genericLookUpType === Constants.Masters['RequirementTitle']) {
                promise = XSeedApiFactory.deleteJobTitleLookup(genericLookupId);
            }
            if ($scope.genericLookUpType === Constants.Masters['RequirementType'] || $scope.genericLookUpType === Constants.Masters['SearchRequirementType']) {
                promise = XSeedApiFactory.deleteJobTypeLookup(genericLookupId);
            }

            if ($scope.genericLookUpType === Constants.Masters['Degree']) {
                promise = XSeedApiFactory.deleteJobDegreesLookup(genericLookupId);
            }

            if ($scope.genericLookUpType === Constants.Masters['CompanySource']) {
                promise = XSeedApiFactory.deleteCompanySourceLookup(genericLookupId);
            }

            if ($scope.genericLookUpType === Constants.Masters['BusinessUnit']) {
                promise = XSeedApiFactory.deleteBusinessUnitLookup(genericLookupId);
            }
            if ($scope.genericLookUpType === Constants.Masters['CandidateSource']) {
                promise = XSeedApiFactory.deleteCandidateSourceLookup(genericLookupId);
            }

            if ($scope.genericLookUpType === Constants.Masters['CandidateCategories']) {
                promise = XSeedApiFactory.deleteCandidateCategoryLookup(genericLookupId);
            }

            if ($scope.genericLookUpType === Constants.Masters['CandidateGroups']) {
                promise = XSeedApiFactory.deleteCandidateGroupLookup(genericLookupId);
            }

            if ($scope.genericLookUpType === Constants.Masters['VisaType']) {
                promise = XSeedApiFactory.deleteJobVisaTypeLookup(genericLookupId);
            }
            promise.then(
                function(response) {
                    blockUI.stop();
                    XSeedAlert.swal({
                        title: 'Success!',
                        text: 'Look up value deleted successfully!',
                        type: "success",
                        customClass: "xseed-error-alert",
                    });

                    if ($scope.genericLookUpType === Constants.Masters['Degree']) {
                        SaveMultiValueLookUpCompleteAction();
                    } else if ($scope.genericLookUpType === Constants.Masters['VisaType']) {
                         SaveMultiValueLookUpCompleteAction();
                    }
                    else {
                        SaveLookUpCompleteAction();
                    }
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
                });

        }, function(dismiss) {});
    }

    /*------------------------------------ Generic master popup call----------------------------------*/

    /*------------------------------------ Multi value Generic master popup call----------------------------------*/


    var genericMasterMultiValueModal = ModalFactory.genericMasterMultiValueModal($scope);
    $scope.openGenericMasterMultiValueModal = function(genericLookUpType) {
        $scope.common.jobPageLookupList();

        $scope.genericLookUpType = genericLookUpType;
        var promise;

        if ($scope.genericLookUpType === Constants.Masters['Degree']) {
            promise = XSeedApiFactory.getJobDegreesLookup($rootScope.userDetails.OrganizationId);
        }

        if ($scope.genericLookUpType === Constants.Masters['VisaType']) {
            promise = XSeedApiFactory.getVisaTypeLookup();
        }

        promise.then(
            function(response) {
                $scope.genericLookupList = response[0].data;
                blockUI.stop();
                //$scope.tableMultiValueParams = new ngTableParams({ page: 1, count: 5 }, { data: response[0].data, counts: [] });
                $scope.tableMultiValueParams = new ngTableParams({
                    page: 1,
                    count: 5,
                    sorting: {}
                }, {
                    counts: [],
                    total: $scope.genericLookupList.length,
                    getData: function($defer, params) {
                        var filteredData = $filter('filter')($scope.genericLookupList, $scope.genericLookupFilter);
                        $scope.orderedGenericLookupList = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
                        params.total($scope.orderedGenericLookupList.length);
                        $scope.slicedGenericLookupList = $scope.orderedGenericLookupList.slice((params.page() - 1) * params.count(), params.page() * params.count());
                        $defer.resolve($scope.slicedGenericLookupList);
                    },
                    $scope: $scope
                });
            },
            function(httpError) {
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });

        if ($scope.genericLookUpType === Constants.Masters['Degree']) {
            if ($scope.common.requirementModel.DegreeList.length == 0) {
                $scope.checkboxes.checked = [];
            } else {
                angular.forEach($scope.common.requirementModel.DegreeList, function(item) {
                    if (angular.isDefined(item)) {
                        $scope.checkboxes.items[item] = true;
                    }

                });
            }
        }

        if ($scope.genericLookUpType === Constants.Masters['VisaType']) {
            if ($scope.common.requirementModel.VisaTypeList.length == 0) {
                $scope.checkboxes.checked = [];
            } else {
                angular.forEach($scope.common.requirementModel.VisaTypeList, function(item) {
                    if (angular.isDefined(item)) {
                        $scope.checkboxes.items[item] = true;
                    }
                });
            }
        }

        genericMasterMultiValueModal.$promise.then(genericMasterMultiValueModal.show);
    };

    $scope.closeGenericMasterMultiValueModal = function() {
        $scope.checkboxes.checked = [];
        clearLookUpValueTableParams();
        $scope.genericLookupFilter = {};
        genericMasterMultiValueModal.$promise.then(genericMasterMultiValueModal.hide);
        if ($scope.genericLookUpType === Constants.Masters['Degree']) {
            if (document.getElementById('lookupDegree')) {
                document.getElementById("lookupDegree").focus();
            }
        }

        if ($scope.genericLookUpType === Constants.Masters['VisaType']) {
            if (document.getElementById('lookupVisaType')) {
                document.getElementById("lookupVisaType").focus();
            }
        }

    }

    $scope.$watch("genericLookupFilter.$", function() {
        if (angular.isDefined($scope.tableMultiValueParams)) {
            $scope.tableMultiValueParams.reload();

            if (!$scope.orderedGenericLookupList) {
                return;
            }
            var checked = 0,
                unchecked = 0,
                total = $scope.orderedGenericLookupList.length;
            angular.forEach($scope.orderedGenericLookupList, function(item) {
                checked += ($scope.checkboxes.items[item.Id]) || 0;
                unchecked += (!$scope.checkboxes.items[item.Id]) || 0;
            });
            if ((unchecked == 0) || (checked == 0)) {
                $scope.checkboxes.checked = (checked == total);

            }

            // grayed checkbox
            angular.element(document.getElementById("select_all")).prop("indeterminate", (checked != 0 && unchecked != 0));


            $scope.tableMultiValueParams.page(1);
        }
    });

    //Checkbox code-
    $scope.checkboxes = { 'checked': false, items: {} };

    // watch for check all checkbox
    $scope.$watch('checkboxes.checked', function(value) {
        angular.forEach($scope.orderedGenericLookupList, function(item) {
            if (angular.isDefined(item.Id)) {
                $scope.checkboxes.items[item.Id] = value;
            }

        });
    });

    // watch for data checkboxes
    $scope.$watch('checkboxes.items', function(values) {

        if (!$scope.orderedGenericLookupList) {
            return;
        }
        var checked = 0,
            unchecked = 0,
            total = $scope.orderedGenericLookupList.length;
        angular.forEach($scope.orderedGenericLookupList, function(item) {
            checked += ($scope.checkboxes.items[item.Id]) || 0;
            unchecked += (!$scope.checkboxes.items[item.Id]) || 0;
        });
        if ((unchecked == 0) || (checked == 0)) {
            $scope.checkboxes.checked = (checked == total);
        }

        $scope.checkedCount = checked;

        //alert(unchecked);
        // grayed checkbox
        angular.element(document.getElementById("select_all")).prop("indeterminate", (checked != 0 && unchecked != 0));
    }, true);

    $scope.genericMasterMultiData = function(type, modalType) {
        if (modalType == 'Degree') {
            $scope.degreeIdList = [];
            $scope.degreeNameList = [];
            if (type == 'Single') {

                $scope.degreeList.push(degree);
            } else {
                angular.forEach($scope.checkboxes.items, function(item, Identifier) {
                    if (item == true) {
                        var selectedDegree = _.find($scope.genericLookupList, function(o) { return o.Id == Identifier; });
                        $scope.degreeIdList.push(selectedDegree.Id);
                        $scope.degreeNameList.push(selectedDegree.Name);
                    }
                });
                //$scope.common.requirementModel.DegreeIdList = $scope.degreeList;

            }



            //$scope.common.requirementModel.DegreeIdList = $scope.degreeIdList;
            $scope.common.requirementModel.DegreeList = $scope.degreeIdList;
            $scope.common.requirementModel.DegreeNameList = $scope.degreeNameList;
            $scope.closeGenericMasterMultiValueModal();
        }


        if (modalType == Constants.Masters['VisaType']) {
            $scope.visaTypeIdList = [];
            $scope.visaTypeNameList = [];
            if (type == 'Single') {
                $scope.visaTypeList.push(visaType);
            } else {

                angular.forEach($scope.checkboxes.items, function(item, Identifier) {
                    if (item == true) {
                        var selectedVisaType = _.find($scope.genericLookupList, function(o) { return o.Id == Identifier; });
                        $scope.visaTypeIdList.push(selectedVisaType.Id);
                        $scope.visaTypeNameList.push(selectedVisaType.Name);
                    }
                });
            }

            $scope.common.requirementModel.VisaTypeList = $scope.visaTypeIdList;
            $scope.common.requirementModel.VisaTypeNameList = $scope.visaTypeNameList;
            $scope.closeGenericMasterMultiValueModal();
        }

        blockUI.stop();

    };

    /*------------------------------------ Multi value Generic master popup call----------------------------------*/

    $scope.getCandidateLogoPath = function(imageName) {
        var logoImagePath;
        if (!$scope.isUndefinedOrNull(imageName) && !$scope.isEmpty(imageName)) {
            logoImagePath = configuration.XSEED_CANDIDATE_IMAGE_PATH + imageName;
        } else {
            logoImagePath = 'images/noImage.png';
        }

        return logoImagePath;
    };

    $scope.getCandidateFullName = function(title, firstName, middleName, lastName) {
        var titleValue = title == null ? '' : title;
        var fname = firstName == null ? '' : firstName;
        var mname = middleName == null ? '' : middleName;
        var lname = lastName == null ? '' : lastName;
        return titleValue + ' ' + fname + ' ' + mname + ' ' + lname;
    }

    $scope.trustSrc = function(src) {
        return $sce.trustAsResourceUrl(src);
    }

    $scope.trustHtml = function(html) {
        return $sce.trustAsHtml(html);
    };


    /******************************************Initialize Candidate Ratings ************************************/

    $scope.initializeCandidateRatings = function() {
            setTimeout(function() {
                var $input = $('input.rating');
                if ($input.length) {
                    $input.removeClass('rating-loading').addClass('rating-loading').rating();
                }
            }, 500);
        }
        /*****************************************Applied candidates***************************************/

    $scope.$watch("appliedCandidateFilter.$", function() {
        if (angular.isDefined($scope.tblRequirementList)) {
            $scope.tblRequirementList.reload();
            $scope.tblRequirementList.page(1);
        }
    });

    $scope.appliedCandidatesForJob = function(jobId) {
        if (!jobId) {
            $location.path('candidateList');
            return;
        }
        var promise = XSeedApiFactory.getCandidateByJob(jobId);
        promise.then(
            function(response) {
                $scope.common.appliedCandidateList = response[0].data;
                $scope.appliedCandidateListData = response[0].data;

                if ($scope.appliedCandidateListData) {

                    //..new functionality for open applied candidate section
                    if (!$rootScope.appliedCandidateId) {
                        $scope.openCandidateFromOtherSection($scope.appliedCandidateListData[0]._id);
                    } else {
                        $scope.openCandidateFromOtherSection($rootScope.appliedCandidateId);
                        $rootScope.appliedCandidateId = null;
                    }

                }
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });
    };

    $scope.onlyappliedCandidatesForJob = function(jobId) {

        var promise = XSeedApiFactory.getCandidateByJob(jobId);
        promise.then(
            function(response) {
                $scope.common.appliedCandidateList = response[0].data;

            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });
    };

    function populateAppliedCandidateList() {

        if (angular.isDefined($scope.tblAppliedCandidateList)) {
            $scope.tblAppliedCandidateList.reload();
        } else {

            $scope.tblAppliedCandidateList = new ngTableParams({
                page: 1,
                count: 10,
                sorting: {}
            }, {
                counts: [],
                total: $scope.common.appliedCandidateList.length,
                getData: function($defer, params) {
                    var filteredData = $filter('filter')($scope.common.appliedCandidateList, $scope.appliedCandidateFilter);
                    $scope.appliedCandidateListData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
                    params.total($scope.appliedCandidateListData.length);
                    $scope.appliedCandidateListData = $scope.appliedCandidateListData.slice((params.page() - 1) * params.count(), params.page() * params.count());

                    $defer.resolve($scope.appliedCandidateListData);

                    if ($scope.appliedCandidateListData) {

                        //..new functionality for open applied candidate section
                        $scope.openCandidateFromOtherSection($scope.appliedCandidateListData[0]._id);

                    }
                    //.............................................
                },
                $scope: $scope
            });
        }
    }

    $scope.showAppliedCandidates = function(job) {
        if (job != null || job != undefined) {
            $rootScope.isSingleDetail = false;
            $scope.common.candidateModel = {};
            $scope.common.selectedJob = job;
            $scope.common.appliedCandidateList = {};
            $scope.appliedCadidateData.JobId = job.Id;
            $scope.appliedCadidateData.JobTitle = job.JobTitle;
            $scope.appliedCadidateData.CompanyId = job.CompanyId;
            $location.path("appliedCandidates");
        }
    };

    $scope.acceptCandidate = function(candidate) {
        XSeedAlert.swal({
            title: "Are you sure you want submit?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function() {
            blockUI.start();
            $scope.appliedCadidateData.Status = "Shortlisted";

            var promise = XSeedApiFactory.updateCandidateStatus($scope.appliedCadidateData, candidate._id);
            promise.then(
                function(response) {
                    //$scope.appliedCandidatesForJob($scope.appliedCadidateData.JobId);
                    $scope.appliedCadidateData.candidate = candidate;
                    $scope.submitCandidateProcess();
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                });
            blockUI.stop();
        }, function(dismiss) {});
    };

    $scope.submitCandidateProcess = function() {
        blockUI.start();
        $scope.CandidateSubmissionModel = {};
        $scope.CandidateSubmissionModel.CandidateList = [];
        var candidate = { Id: $scope.appliedCadidateData.candidate._id, Name: $scope.appliedCadidateData.candidate.FirstName + " " + $scope.appliedCadidateData.candidate.LastName };
        $scope.CandidateSubmissionModel.CandidateList.push(candidate);
        $scope.CandidateSubmissionModel.JobId = $scope.appliedCadidateData.JobId;
        $scope.CandidateSubmissionModel.JobTitle = $scope.appliedCadidateData.JobTitle
        $scope.CandidateSubmissionModel.CompanyId = $scope.appliedCadidateData.CompanyId;
        $scope.CandidateSubmissionModel.OrganizationUserId = $rootScope.userDetails.UserId;
        var promise = XSeedApiFactory.submitCandidate($scope.CandidateSubmissionModel);
        promise.then(
            function(response) {
                $scope.dataSubmissiomEmail.successCandidates = response[0].data.successCandidates;
                if ($scope.dataSubmissiomEmail.successCandidates.length > 0) {

                    XSeedAlert.swal({
                        title: 'Success!',
                        text: 'Candidate submitted!',
                        type: "success",
                        customClass: "xseed-error-alert"
                    }).then(function() {
                        $scope.getSubmissionUrl($scope.CandidateSubmissionModel.CompanyId);
                        $scope.openCandidateSubmissionEmailModalRequirement();
                        $timeout(function() {}, 0);
                    }, function(dismiss) {
                        blockUI.stop();
                    })
                } else {
                    blockUI.stop();
                    ExceptionHandler.handlerPopup('Submission', response[0].data.message, 'Info!', 'info');
                };
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
            });
        blockUI.stop();
    };

    $scope.rejectCandidate = function(candidateId) {
        XSeedAlert.swal({
            title: "Are you sure you want to reject?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function() {
            blockUI.start();
            $scope.appliedCadidateData.Status = "Rejected";
            var jobId = $scope.appliedCadidateData.JobId;
            var promise = XSeedApiFactory.updateCandidateStatus($scope.appliedCadidateData, candidateId);
            promise.then(
                function(response) {
                    $scope.appliedCandidatesForJob(jobId);
                    XSeedAlert.swal({
                        title: 'Rejected!',
                        text: 'Candidate rejected!',
                        type: "success",
                        customClass: "xseed-error-alert"
                    }).then(function() {
                        $timeout(function() {}, 0);
                    }, function(dismiss) {
                        blockUI.stop();
                    })
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                });
            blockUI.stop();
        }, function(dismiss) {});
    };



    $scope.openCandidateSubmissionEmailModal = function() {
        $scope.common.commonEmailModal.$promise.then($scope.common.commonEmailModal.show);
    };

    $scope.openCandidateSubmissionEmailModalRequirement = function() {
        if ($scope.CandidateSubmissionModel.CandidateList.length > 0) {
            blockUI.start();

            //get company contacts
            var promise = XSeedApiFactory.getSubmissionContactEmail($scope.CandidateSubmissionModel.CompanyId);
            promise.then(
                function(response) {
                    $rootScope.commonEmailModel.candidateList = [];

                    //get company submissionUrl
                    var promise = XSeedApiFactory.getCompanyDetail($rootScope.userDetails.OrganizationId, $scope.CandidateSubmissionModel.CompanyId);
                    promise.then(
                        function(response) {
                            $scope.companyData = response[0].data;
                            $rootScope.commonEmailModel.companySubmissionUrl = $scope.companyData.SubmissionPageURL;
                        },
                        function(httpError) {
                            ExceptionHandler.handlerHTTPException('Company', httpError.data.Status, httpError.data.Message);
                        });
                    //get company contact email
                    if (response[0].data) {
                        $rootScope.commonEmailModel.ToEmailID = [];
                        angular.forEach(response[0].data, function(val, index) {
                            $rootScope.commonEmailModel.ToEmailID.push(val.Name);
                        });
                        $rootScope.commonEmailModel.ToEmailID = $rootScope.commonEmailModel.ToEmailID.join();
                    }
                    $rootScope.commonEmailModel.FromEmailID = $rootScope.userDetails.PrimaryEmail;
                    $rootScope.commonEmailModel.MailSubject = "Submissions";
                    $rootScope.commonEmailModel.MailBody = "We would like to submit following candidate(s) against the<br />Requirement: ";
                    //get job title
                    $rootScope.commonEmailModel.MailBody = $rootScope.commonEmailModel.MailBody + $scope.CandidateSubmissionModel.JobTitle;
                    //get candidate names
                    $rootScope.commonEmailModel.MailBody = $rootScope.commonEmailModel.MailBody + "<br />following is/are the candidate(s)";
                    angular.forEach($scope.CandidateSubmissionModel.CandidateList, function(val, index) {
                        $rootScope.commonEmailModel.candidateList.push(val.Id);
                        index = index + 1
                        $rootScope.commonEmailModel.MailBody = $rootScope.commonEmailModel.MailBody + "<br />" + index + ". " + val.Name;
                    });

                    candidateSubmissionEmailModal.$promise.then(candidateSubmissionEmailModal.show);
                    blockUI.stop();
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
                });
        }

        $scope.appliedCandidatesForJob($scope.appliedCadidateData.JobId);
    }

    $scope.closeCandidateSubmissionEmailModal = function() {
        candidateSubmissionEmailModal.$promise.then(candidateSubmissionEmailModal.hide);
        $scope.appliedCandidatesForJob($scope.appliedCadidateData.JobId);
        $scope.appliedCadidateData = {};
        $scope.CandidateSubmissionModel = {}
    }

    $scope.sendEmailNotification = function(commonEmailForm) {
        if (new ValidationService().checkFormValidity(commonEmailForm)) {
            blockUI.start();
            //common fields
 if ($rootScope.commonEmailModel.ToEmailID) {
                $rootScope.commonEmailModel.ToEmailID = $rootScope.commonEmailModel.ToEmailID.toString().split(",");
            }
            if ($rootScope.commonEmailModel.ToCC && $rootScope.commonEmailModel.ToCC != "") {
                $rootScope.commonEmailModel.ToCC = $rootScope.commonEmailModel.ToCC.toString().split(",");
            }
            if ($rootScope.commonEmailModel.ToBCC && $rootScope.commonEmailModel.ToBCC != "") {
                $rootScope.commonEmailModel.ToBCC = $rootScope.commonEmailModel.ToBCC.toString().split(",");
            }

            if ($rootScope.commonEmailModel.MailAttachment) {
                var attachments = [];
                angular.forEach($rootScope.commonEmailModel.MailAttachment, function(val, index) {
                    if (val != null) {
                        attachments.push(val);
                    }
                });
                $rootScope.commonEmailModel.MailAttachment = attachments;
            }

            if ($rootScope.userDetails) {
                $rootScope.commonEmailModel.FromDisplayName = $rootScope.userDetails.FirstName + " " + $rootScope.userDetails.LastName;
            }
            $rootScope.commonEmailModel.JobTitle = $scope.common.selectedJob.JobTitle;
            $rootScope.commonEmailModel.CompanyName = $scope.common.selectedJob.CompanyName;
            //API call by typeOfNotification
            if ($rootScope.commonEmailModel.typeOfNotification == $scope.constants.submitCandidateByEmail) {
                var promise = XSeedApiFactory.submitCandidateByEmail($rootScope.commonEmailModel);
                promise.then(
                    function(response) {
                        //$scope.appliedCandidatesForJob($scope.appliedCadidateData.JobId);
                        $rootScope.commonEmailModel.ToEmailID = $rootScope.commonEmailModel.ToEmailID.join();

                        if ($rootScope.commonEmailModel.ToCC && $rootScope.commonEmailModel.ToCC != "") {
                            $rootScope.commonEmailModel.ToCC = $rootScope.commonEmailModel.ToCC.join();
                        }
                        if ($rootScope.commonEmailModel.ToBCC && $rootScope.commonEmailModel.ToBCC != "") {
                            $rootScope.commonEmailModel.ToBCC = $rootScope.commonEmailModel.ToBCC.join();
                        }

                        XSeedAlert.swal({
                            title: 'Success!',
                            text: 'Candidate(s) submitted to client.',
                            type: "success",
                            showCancelButton: false,
                            customClass: "xseed-error-alert"

                        }).then(function() {
                            $scope.closeCommonEmailModal(),
                                $timeout(function() {}, 0);
                        }, function(dismiss) {
                            blockUI.stop();
                        })
                    },
                    function(httpError) {
                        blockUI.stop();
                        ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
                    });
                blockUI.stop();
            } else if ($rootScope.commonEmailModel.typeOfNotification == $scope.constants.sendJobMail) {
                var promise = XSeedApiFactory.sendJobEmailNotification($rootScope.commonEmailModel);
                promise.then(
                    function(response) {
                        $rootScope.commonEmailModel.ToEmailID = $rootScope.commonEmailModel.ToEmailID.join();

                        if ($rootScope.commonEmailModel.ToCC && $rootScope.commonEmailModel.ToCC != "") {
                            $rootScope.commonEmailModel.ToCC = $rootScope.commonEmailModel.ToCC.join();
                        }
                        if ($rootScope.commonEmailModel.ToBCC && $rootScope.commonEmailModel.ToBCC != "") {
                            $rootScope.commonEmailModel.ToBCC = $rootScope.commonEmailModel.ToBCC.join();
                        }
                        XSeedAlert.swal({
                            title: 'Success!',
                            text: 'Mail sent!',
                            type: "success",
                            customClass: "xseed-error-alert",
                            showCancelButton: false,
                        }).then(function() {
                            if ($scope.common.selectedJob.Id) {
                                $scope.getActivityList();
                            }
                            $scope.closeCommonEmailModal(),
                                $timeout(function() {}, 0);
                        }, function(dismiss) {
                            blockUI.stop();
                        })
                    },
                    function(httpError) {
                        blockUI.stop();
                        ExceptionHandler.handlerHTTPException('Notification', httpError.data.Status, httpError.data.Message);
                    });
                blockUI.stop();
            } else if ($rootScope.commonEmailModel.typeOfNotification == $scope.constants.sendCandidateMail) {
                var promise = XSeedApiFactory.sendCandidateEmailNotification($rootScope.commonEmailModel);
                promise.then(
                    function(response) {
                        $rootScope.commonEmailModel.ToEmailID = $rootScope.commonEmailModel.ToEmailID.join();

                        if ($rootScope.commonEmailModel.ToCC && $rootScope.commonEmailModel.ToCC != "") {
                            $rootScope.commonEmailModel.ToCC = $rootScope.commonEmailModel.ToCC.join();
                        }
                        if ($rootScope.commonEmailModel.ToBCC && $rootScope.commonEmailModel.ToBCC != "") {
                            $rootScope.commonEmailModel.ToBCC = $rootScope.commonEmailModel.ToBCC.join();
                        }
                        XSeedAlert.swal({
                            title: 'Success!',
                            text: 'Mail sent!',
                            type: "success",
                            customClass: "xseed-error-alert"
                        }).then(function() {
                            if ($scope.common.selectedCandidate._id) {
                                $scope.getActivityListCandidate();
                            }
                            $scope.closeCommonEmailModal(),
                                $timeout(function() {}, 0);
                        }, function(dismiss) {
                            blockUI.stop();
                        })
                    },
                    function(httpError) {
                        blockUI.stop();
                        ExceptionHandler.handlerHTTPException('Notification', httpError.data.Status, httpError.data.Message);
                    });
                blockUI.stop();
            }
        };
    };

    $scope.submitBySubmissionUrl = function() {
        $scope.closeCandidateSubmissionEmailModal();
        $location.path("submissionToClient");
    };

    $scope.getSubmissionUrl = function(companyId) {
        //get company submissionUrl
        blockUI.start();
        $rootScope.commonEmailModel = {};
        var promise = XSeedApiFactory.getCompanyDetail($rootScope.userDetails.OrganizationId, companyId);
        promise.then(
            function(response) {
                $scope.companyData = response[0].data;
                $rootScope.commonEmailModel.companySubmissionUrl = $scope.companyData.SubmissionPageURL;

            },
            function(httpError) {
                ExceptionHandler.handlerHTTPException('Company', httpError.data.Status, httpError.data.Message);
            });
        blockUI.stop();
    };
    /*****************************************Applied Candidates***************************************/

    /*****************************************common email modal***************************************/
    $scope.showActivityEmail = function(emailActivity) {
        $rootScope.commonEmailModel = {};
        $rootScope.commonEmailModel.typeOfNotification = $scope.constants.showActivityEmail;
        if (emailActivity) {
            $rootScope.commonEmailModel.ToEmailID = emailActivity.EmailActivity.To;
            $rootScope.commonEmailModel.ToCC = emailActivity.EmailActivity.Cc;
            $rootScope.commonEmailModel.ToBCC = emailActivity.EmailActivity.Bcc;
            $rootScope.commonEmailModel.FromEmailID = emailActivity.CreatedByEmail;
            $rootScope.commonEmailModel.MailSubject = emailActivity.EmailActivity.EmailSubject;
            $rootScope.commonEmailModel.MailBody = emailActivity.EmailActivity.EmailContent;
            $rootScope.commonEmailModel.MailAttachment = emailActivity.EmailActivity.Attachments;
        }

        $scope.common.commonEmailModal.$promise.then($scope.common.commonEmailModal.show);
    };

    $scope.closeCommonEmailModal = function() {

        $scope.common.commonEmailModal.$promise.then($scope.common.commonEmailModal.hide).then(function() {
            $rootScope.commonEmailModel = {};

            $('#summernote').summernote('destroy')
        });

    }

    $scope.removeAttachement = function(index) {
        $rootScope.commonEmailModel.MailAttachment[index] = undefined;
    };
    /*****************************************common email modal***************************************/

    /*****************************************to print grid data***************************************/
    $scope.printDiv = function(divName, title) {
            var printContents = document.getElementById(divName).outerHTML;
            var popupWin = window.open('', '_blank', 'scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            popupWin.window.focus();
            popupWin.document.open();
            popupWin.document.write('<!DOCTYPE html><html><head><title>' + title + '</title>' +
                '</head><body onload="window.print(); window.close();"><div>' +
                printContents + '</div></html>');
            popupWin.document.close();
        }
        /*****************************************to print grid data***************************************/

    /*****************************************Advance search modal***************************************/
    $scope.openAdvanceSearchModal = function(currentSection) {
        $scope.common.currentSection = currentSection;
        $scope.buildLookUpBySection($scope.common.currentSection);
        if ($scope.common.requirementModel) {
            $scope.common.requirementModel.manipulationMode = undefined;
        }
        $scope.common.advanceSearchModal.$promise.then($scope.common.advanceSearchModal.show);
    };

    $scope.closeAdvanceSearchModal = function() {
        $scope.common.advanceSearchModal.$promise.then($scope.common.advanceSearchModal.hide);
        //$scope.common.advanceSearchModel = {};
    };

    $scope.buildLookUpBySection = function(section) {
        if (section == $scope.constants.sectionRequirements) {
            $scope.common.advanceSearchLookUpBySection = [];
            $scope.common.advanceSearchLookUpBySection.push({ Id: "Title", Name: "Requirement Title" }, { Id: "Status", Name: "Requirement Status" }, { Id: "Type", Name: "Requirement Type" }, { Id: "AssignedTo", Name: "Assigned Recruiter" }, { Id: "Company", Name: "Company" }, { Id: "RequisitionId", Name: "Requisition Id" }

            );
        } else if (section == $scope.constants.sectionCandidates) {
            $scope.common.advanceSearchLookUpBySection = [];
            $scope.common.advanceSearchLookUpBySection.push({ Id: "CandidateId", Name: "Candidate Id" }, { Id: "Status", Name: "Status" }, { Id: "Name", Name: "Name" }, { Id: "Email", Name: "Email" }, { Id: "Contact", Name: "Contact" }, { Id: "JobTitle", Name: "Profile Title" }, { Id: "Skills", Name: "Skills" }, { Id: "Location", Name: "Location" }, { Id: "Availability", Name: "Availability" }, { Id: "Experience", Name: "Experience" }, { Id: "Company", Name: "Company" }, { Id: "CreatedBy", Name: "Created By" });
        } else if (section == 'Submissions') {
            $scope.common.advanceSearchLookUpBySection = [];
            $scope.common.advanceSearchLookUpBySection.push({ Id: "CandidateName", Name: "Candidate Name" }, { Id: "CompanyName", Name: "Company Name" }, { Id: "Recruiter", Name: "Assigned Recruiter" });
        }
    };
    /*****************************************Advance search modal***************************************/

    /*****************************************Export to excel***************************************/

    $scope.exportToExcel = function(sectionFlag) {
        if (sectionFlag == $scope.constants.sectionRequirements) {
            getExportToExcelDataRequirement();
        } else if (sectionFlag == $scope.constants.sectionCandidates) {
            getExportToExcelDataCandidate();
        } else if (sectionFlag == $scope.constants.sectionSubmissions) {
            getExportToExcelDataSubmission();
        }
    };

    /* Requirement */

    function getExportToExcelDataRequirement() {
        var isExport = true;

        // Test Browser
        $scope.isSafariBrowser = false;
        $scope.BrowserDetection();
        if ($rootScope.currentBrowserName != 'safari') {
            $scope.isSafariBrowser = false;
        } else if ($rootScope.currentBrowserName == 'safari') {
            $scope.isSafariBrowser = true;
        }

        blockUI.start();
        /* sorting */
        var sort, sortorder;
        if ($scope.common.sortBy) {
            angular.forEach($scope.common.sortBy, function(val, index) {
                sort = index;
                sortorder = val;
            });
        }
        /* sorting */
        if ($scope.flag.advanceSearchFlag) {
            var promise = XSeedApiFactory.getRequirementAdvanceSearchResult($scope.common.advanceSearchModel, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder, isExport);
            promise.then(
                function(response) {
                    $scope.exportToExcelData = response[0].data.Jobs;
                    if ($scope.exportToExcelData.length > 0) {
                        buildRequiementReportGridData();
                    }
                },
                function(httpError) {
                    ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                });
        } else {

            var organizationId = $rootScope.userDetails.OrganizationId;
            var promise = XSeedApiFactory.getRequirementList(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder, isExport);
            promise.then(
                function(response) {
                    $scope.exportToExcelData = response[0].data.Jobs;
                    if ($scope.exportToExcelData.length > 0) {
                        buildRequiementReportGridData();
                    }
                },
                function(httpError) {
                    ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                });
        }
        blockUI.stop();
    };

    function buildRequiementReportGridData() {
        blockUI.start();
        //header in report
        $scope.common.requirementReportGridHeader = {
            "JobStatus": "Requirement Status",
            "RequisitionId": "Internal Requirement Id",
            "JobTitle": "Requirement Title",
            "JobType": "Requirement Type",
            "Location": "Location",
            "CompanyName": "Company",
            "MinSalary": "Min Salary",
            "MaxSalary": "Max Salary",
            "ClientRate": "Client Rate",
            "PayRate": "Pay Rate",
            "TechnicalSkills": "Skills",
            "CompanyContactName": "Contact(s)",
            "AppliedCandidateCount": "Applied Candidates",
            "OrganizationUser": "Assigned Recruiter(s)",
            "Created": "Created",
            "Modified": "Modified"
        };

        //structured data in report
        $scope.common.requirementReportGridData = [];
        angular.forEach($scope.exportToExcelData, function(val, index) {
            $scope.common.requirementReportGridData[index] = {};
            $scope.common.requirementReportGridData[index].JobStatus = $scope.exportToExcelData[index].JobStatus;
            $scope.common.requirementReportGridData[index].RequisitionId = $scope.exportToExcelData[index].RequisitionId;
            $scope.common.requirementReportGridData[index].JobTitle = $scope.exportToExcelData[index].JobTitle;
            $scope.common.requirementReportGridData[index].JobType = $scope.exportToExcelData[index].JobType;
            $scope.common.requirementReportGridData[index].Location = $scope.exportToExcelData[index].City + " " + $scope.exportToExcelData[index].State + " " + $scope.exportToExcelData[index].Country;
            $scope.common.requirementReportGridData[index].CompanyName = $scope.exportToExcelData[index].CompanyName;
            $scope.common.requirementReportGridData[index].MinSalary = $scope.exportToExcelData[index].MinSalary;
            $scope.common.requirementReportGridData[index].MaxSalary = $scope.exportToExcelData[index].MaxSalary;
            $scope.common.requirementReportGridData[index].ClientRate = $scope.exportToExcelData[index].ClientRate;
            $scope.common.requirementReportGridData[index].PayRate = $scope.exportToExcelData[index].PayRate;
            $scope.common.requirementReportGridData[index].TechnicalSkills = $scope.exportToExcelData[index].TechnicalSkills == null ? "" : $scope.exportToExcelData[index].TechnicalSkills.split("|").toString();
            $scope.common.requirementReportGridData[index].CompanyContactName = $scope.exportToExcelData[index].CompanyContactName;
            $scope.common.requirementReportGridData[index].AppliedCandidateCount = $scope.exportToExcelData[index].AppliedCandidateCount;

            if ($scope.exportToExcelData[index].OrganizationUser.length > 0) {
                $scope.common.requirementReportGridData[index].OrganizationUser = "";
                for (var i = 0; i < ($scope.exportToExcelData[index].OrganizationUser.length - 1); i++) {
                    $scope.common.requirementReportGridData[index].OrganizationUser = $scope.common.requirementReportGridData[index].OrganizationUser + $scope.exportToExcelData[index].OrganizationUser[i].Name + ", ";
                }
                $scope.common.requirementReportGridData[index].OrganizationUser = $scope.common.requirementReportGridData[index].OrganizationUser + $scope.exportToExcelData[index].OrganizationUser[$scope.exportToExcelData[index].OrganizationUser.length - 1].Name;
            } else {
                $scope.common.requirementReportGridData[index].OrganizationUser = "";
            }

            $scope.common.requirementReportGridData[index].Created = $scope.exportToExcelData[index].CreatedBy + " on " + $scope.exportToExcelData[index].CreatedOn;

            if ($scope.exportToExcelData[index].ModifiedOn != null) {
                $scope.common.requirementReportGridData[index].Modified = $scope.exportToExcelData[index].ModifiedBy + " on " + $scope.exportToExcelData[index].ModifiedOn;
            }
        });
        if ($scope.common.requirementReportGridData.length > 0) {
            saveToExcel('Requirement.xlsx', 'HotRequirement', $scope.common.requirementReportGridData, $scope.common.requirementReportGridHeader);
        }
        blockUI.stop();
    };

    /* Requirement */

    /* Candidate */

    function getExportToExcelDataCandidate() {
        $scope.exportToExcelData = $scope.common.candidateList;
        var isExport = true;

        // Test browser
        $scope.isSafariBrowser = false;
        $scope.BrowserDetection();
        if ($rootScope.currentBrowserName != 'safari') {
            $scope.isSafariBrowser = false;
        } else if ($rootScope.currentBrowserName == 'safari') {
            $scope.isSafariBrowser = true;
        }

        blockUI.start();
        /* sorting */
        var sort, sortorder;
        if ($scope.common.sortBy) {
            angular.forEach($scope.common.sortBy, function(val, index) {
                sort = index;
                sortorder = val;
            });
        }
        ///* sorting */
        //if ($scope.flag.advanceSearchFlag) {
        //    var promise = XSeedApiFactory.getCandidateAdvanceSearchResult($scope.common.advanceSearchModel, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder, isExport);
        //    promise.then(
        //        function (response) {
        //            console.log(response);
        //            $scope.exportToExcelData = response[0].data.Candidates;
        //            if ($scope.exportToExcelData.length > 0) {
        //                buildCandidateReportGridData();
        //            }
        //        },
        //        function(httpError) {
        //            ExceptionHandler.handlerHTTPException('Candidate', httpError.data.Status, httpError.data.Message);
        //        });
        //} else {
        //    var organizationId = $rootScope.userDetails.OrganizationId;
        //    var promise = XSeedApiFactory.getCandidateList($scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder, isExport);
        //    promise.then(
        //        function (response) {
        //            console.log(response);
        //            $scope.exportToExcelData = response[0].data.Candidates;
        //            if ($scope.exportToExcelData.length > 0) {
        //                buildCandidateReportGridData();
        //            }
        //        },
        //        function(httpError) {
        //            ExceptionHandler.handlerHTTPException('Candidate', httpError.data.Status, httpError.data.Message);
        //        });
        //}
        buildCandidateReportGridData();
        blockUI.stop();
    };

    function buildCandidateReportGridData() {
        blockUI.start();
        //header in report
        //data:column name
        $scope.common.candidateReportGridHeader = {
            "Status": "Status",
            "CandidateId": "Candidate Id",
            "Name": "Name",
            "Mobile": "Mobile",
            "HomePhone": "Home Phone",
            "WorkPhone": "Work Phone",
            "CompanyName": "Company Name",
            "Email": "Email",
            "JobTitle": "Job Title",
            "Skill": "Skill",
            "TotalExperience": "Total Experience",
            "USExperience": "US Experience",
            "SSN": "SSN",
            "Categories": "Categories",
            "Groups": "Groups",
            "CreatedBy": "Created By",
            "CreatedDate": "Created Date",
            "EditedDate": "Edited Date"
        };
        //structured data in report
        $scope.common.candidateReportGridData = [];
        angular.forEach($scope.exportToExcelData, function(val, index) {
            $scope.common.candidateReportGridData[index] = {};
            $scope.common.candidateReportGridData[index].Status = $scope.exportToExcelData[index].Status;
            $scope.common.candidateReportGridData[index].CandidateId = $scope.exportToExcelData[index].CandidateId;
            $scope.common.candidateReportGridData[index].Name = $scope.exportToExcelData[index].FirstName + " " + $scope.exportToExcelData[index].LastName;
            $scope.common.candidateReportGridData[index].Mobile = $scope.exportToExcelData[index].Mobile;
            $scope.common.candidateReportGridData[index].HomePhone = $scope.exportToExcelData[index].Phone;
            $scope.common.candidateReportGridData[index].WorkPhone = $scope.exportToExcelData[index].WorkPhone;
            $scope.common.candidateReportGridData[index].CompanyName = $scope.exportToExcelData[index].CompanyName;
            $scope.common.candidateReportGridData[index].Email = $scope.exportToExcelData[index].PrimaryEmail;
            $scope.common.candidateReportGridData[index].JobTitle = $scope.exportToExcelData[index].ProfileTitle;
            $scope.common.candidateReportGridData[index].Skill = $scope.exportToExcelData[index].Skills == null ? "" : $scope.exportToExcelData[index].Skills.toString();
            $scope.common.candidateReportGridData[index].TotalExperience = $scope.exportToExcelData[index].TotalExperience;
            $scope.common.candidateReportGridData[index].USExperience = $scope.exportToExcelData[index].USExperience;
            $scope.common.candidateReportGridData[index].SSN = $scope.exportToExcelData[index].SSN;
            $scope.common.candidateReportGridData[index].Categories = $scope.exportToExcelData[index].Categories;
            $scope.common.candidateReportGridData[index].Groups = $scope.exportToExcelData[index].Groups;
            $scope.common.candidateReportGridData[index].CreatedBy = $scope.exportToExcelData[index].CreatedByName;
            $scope.common.candidateReportGridData[index].CreatedDate = $scope.exportToExcelData[index].CreatedOn;
            $scope.common.candidateReportGridData[index].EditedDate = $scope.exportToExcelData[index].ModifiedOn;
        });
        if ($scope.common.candidateReportGridData.length > 0) {
            saveToExcel('Candidate.xlsx', 'Candidate', $scope.common.candidateReportGridData, $scope.common.candidateReportGridHeader);
        }
        blockUI.stop();
    };

    /* Candidate */

    /* Submission */

    function getExportToExcelDataSubmission() {
        var isExport = true;

        // Test Browser
        $scope.isSafariBrowser = false;
        $scope.BrowserDetection();
        if ($rootScope.currentBrowserName != 'safari') {
            $scope.isSafariBrowser = false;
        } else if ($rootScope.currentBrowserName == 'safari') {
            $scope.isSafariBrowser = true;
        }

        blockUI.start();
        /* sorting */
        var sort, sortorder;
        if ($scope.common.sortBy) {
            angular.forEach($scope.common.sortBy, function(val, index) {
                sort = index;
                sortorder = val;
            });
        }
        /* sorting */
        if ($scope.flag.advanceSearchFlag) {
            var promise = XSeedApiFactory.searchSubmissions($scope.common.advanceSearchModel, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder, isExport);
            promise.then(
                function(response) {
                    $scope.exportToExcelData = response[0].data.Submissions;
                    if ($scope.exportToExcelData.length > 0) {
                        buildSubmissionReportGridData();
                    }
                },
                function(httpError) {
                    ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
                });
        } else {
            var organizationId = $rootScope.userDetails.OrganizationId;
            var promise = XSeedApiFactory.getSubmissions(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder, isExport);
            promise.then(
                function(response) {
                    $scope.exportToExcelData = response[0].data.Submissions;
                    if ($scope.exportToExcelData.length > 0) {
                        buildSubmissionReportGridData();
                    }
                },
                function(httpError) {
                    ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
                });
        }
        blockUI.stop();
    };

    function buildSubmissionReportGridData() {
        blockUI.start();
        //header in report
        //data:column name
        $scope.common.submissionReportGridHeader = {
            "CandidateName": "Candidate Name",
            "CompanyName": "Client Name",
            "ClientEmail": "Client Email",
            "JobTitle": "Requirement Title",
            "JobCode": "Internal Requirement Id",
            "JobType": "Requirement Type",
            "Status": "Status",
            "OrganizationUserName": "Recruiter",
            "AssignedDate": "Submitted Date",
            "CreatedBy": "Submitted By",
            "ModifiedOn": "Last Updated Date",
            "ModifiedBy": "Updated By"
        };
        //structured data in report
        $scope.common.submissionReportGridData = [];
        angular.forEach($scope.exportToExcelData, function(val, index) { //$scope.submissions[index].CandidateList[0].Name
            $scope.common.submissionReportGridData[index] = {};
            $scope.common.submissionReportGridData[index].CandidateName = $scope.exportToExcelData[index].CandidateList[0].Name;
            $scope.common.submissionReportGridData[index].CompanyName = $scope.exportToExcelData[index].CompanyName;
            $scope.common.submissionReportGridData[index].ClientEmail = $scope.exportToExcelData[index].ContactEmailList == null ? "" : $scope.exportToExcelData[index].ContactEmailList.toString();
            $scope.common.submissionReportGridData[index].JobTitle = $scope.exportToExcelData[index].JobTitle;
            $scope.common.submissionReportGridData[index].JobCode = $scope.exportToExcelData[index].JobCode;
            $scope.common.submissionReportGridData[index].JobType = $scope.exportToExcelData[index].JobType;
            $scope.common.submissionReportGridData[index].Status = $scope.exportToExcelData[index].Status;
            $scope.common.submissionReportGridData[index].OrganizationUserName = $scope.exportToExcelData[index].OrganizationUserName;
            $scope.common.submissionReportGridData[index].AssignedDate = $scope.exportToExcelData[index].AssignedDate;
            $scope.common.submissionReportGridData[index].CreatedBy = $scope.exportToExcelData[index].CreatedBy;
            $scope.common.submissionReportGridData[index].ModifiedOn = $scope.exportToExcelData[index].ModifiedOn;
            $scope.common.submissionReportGridData[index].ModifiedBy = $scope.exportToExcelData[index].ModifiedBy;
        });
        if ($scope.common.submissionReportGridData.length > 0) {
            saveToExcel('Submissions.xlsx', 'Submissions', $scope.common.submissionReportGridData, $scope.common.submissionReportGridHeader);
        }
        blockUI.stop();
    };

    /* Submission */

    function saveToExcel(fileName, sheetName, data, headerMapping) {
        blockUI.start();

        ExportExcel.savetoexcel(fileName, sheetName, data, headerMapping);

        blockUI.stop();
    };

    /*****************************************Export to excel***************************************/

    /*****************************************Advance search requirements and candidates***************************************/
    $scope.getAdvanceSearchResult = function(advanceSearchForm, currentSection) {
        if (new ValidationService().checkFormValidity(advanceSearchForm)) {
            if (currentSection == $scope.constants.sectionRequirements) {
                $scope.common.advanceSearchModel.filter = null;
                getSearchedRequirementList();
            } else if (currentSection == $scope.constants.sectionCandidates) {
                $scope.common.advanceSearchModel.filter = null;
                getSearchedCandidateList();
            }
        };
    };

    $scope.getRequirementFilterResult = function() {
        $scope.common.requirementStatusFilter = "All";

        $(".btn-filter").removeClass("active");
        $(".btn-default").addClass("active");

        getSearchedRequirementList();
    };

    $scope.getCandidateFilterResult = function() {
        getSearchedCandidateList();
    };

    function getSearchedRequirementList() {
        blockUI.start();
        if ($scope.common.requirementModel.manipulationMode != "Edit") {
            $scope.flag.advanceSearchFlag = true;
            $scope.common.advanceSearchModel.OrganizationId = $rootScope.userDetails.OrganizationId;
            $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
            $scope.common.pageNumberShow = undefined;
            var firstPageFlagSearch = 0;
            //for new plain search
            $scope.common.advanceSearchModel.SearchIn = ["Title", "Status", "Type", "AssignedTo", "Company", "RequisitionId"];

            var promise = XSeedApiFactory.getRequirementAdvanceSearchResult($scope.common.advanceSearchModel, $scope.common.pageSizeShow, $scope.common.pageNumberShow);
            promise.then(
                function(response) {
                    $scope.common.jobList = response[0].data.Jobs;
                    $scope.common.totalCount = response[0].data.TotalCount;
                    $scope.common.totalPages = response[0].data.TotalPages;

                    //to select first row by default
                    $scope.setClickedRow($scope.common.jobList[0]);

                    //Manupulation of response
                    angular.forEach($scope.common.jobList, function(val, index) {
                        if ($scope.common.jobList[index].TechnicalSkills != null) {
                            $scope.common.jobList[index].TechnicalSkills = $scope.common.jobList[index].TechnicalSkills.split("|");
                            $scope.common.jobList[index].TechnicalSkills = $scope.common.jobList[index].TechnicalSkills.join();
                        }
                    });

                    firstPageFlagSearch = 1;
                    populateAdvanceSearchRequirementList($scope.common.jobList, $scope.common.totalCount, $scope.common.totalPages, firstPageFlagSearch);
                    $scope.closeAdvanceSearchModal();
                    blockUI.stop();


                },
                function(httpError) {
                    $scope.closeAdvanceSearchModal();
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Search', httpError.data.Status, httpError.data.Message);
                });
        }
    }

    function checkSelectedRequirementIsOnSamePage() {
        angular.forEach($scope.common.jobList, function(val, index) {
            if (val.Id == $scope.common.selectedJob.Id) {
                $scope.selectedIsPresent = true;
            }
        });
        if (!$scope.selectedIsPresent) {
            $scope.setClickedRow($scope.common.jobList[0]);
            $scope.selectedIsPresent = false;
        }
    };

    function populateAdvanceSearchRequirementList(jobList, totalCount, totalPages, pageFlag) {
        //if (angular.isDefined($scope.tblRequirementList)) {
        //    $scope.tblRequirementList.reload();
        //}
        //else 
        {
            $scope.common.tblRequirementList = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: $scope.common.ngTablePaginationCount,
                total: totalCount,
                getData: function($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    if (pageFlag != 1) {
                        /* sorting */
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function(val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */

                        var promise = XSeedApiFactory.getRequirementAdvanceSearchResult($scope.common.advanceSearchModel, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder);
                        promise.then(
                            function(response) {
                                $scope.common.jobList = response[0].data.Jobs;
                                $scope.common.totalCount = response[0].data.TotalCount;
                                $scope.common.totalPages = response[0].data.TotalPages;

                                //to select first row by default
                                if ($scope.common.requirementModel.manipulationMode != "Edit") {
                                    $scope.setClickedRow($scope.common.jobList[0]);
                                } else {
                                    if ($scope.common.selectedJob) {
                                        $scope.getActivityList();
                                    }
                                    $scope.common.requirementModel.manipulationMode = undefined;
                                }
                                checkSelectedRequirementIsOnSamePage();
                                //Manupulation of response
                                angular.forEach($scope.common.jobList, function(val, index) {
                                    if (val.Id == $scope.common.selectedJob.Id) {
                                        $scope.common.selectedJob = val;
                                    }
                                    if ($scope.common.jobList[index].TechnicalSkills != null) {
                                        $scope.common.jobList[index].TechnicalSkills = $scope.common.jobList[index].TechnicalSkills.split("|");
                                        $scope.common.jobList[index].TechnicalSkills = $scope.common.jobList[index].TechnicalSkills.join();
                                    }
                                });

                                blockUI.stop();
                            },
                            function(httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('Search', httpError.data.Status, httpError.data.Message);
                            });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.common.jobList);
                },
                $scope: $scope
            });
        }
    };

    //candidate
    function getSearchedCandidateList() {
        blockUI.start();
        if ($scope.common.candidateModel.manipulationMode != "Edit") {
            $scope.flag.advanceSearchFlag = true;
            $scope.common.advanceSearchModel.OrganizationId = $rootScope.userDetails.OrganizationId;
            $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
            $scope.common.pageNumberShow = undefined;
            var firstPageFlagSearch = 0;
            //for new plain search
            $scope.common.advanceSearchModel.SearchIn = ["CandidateId", "Status", "Name", "Email", "Contact", "JobTitle", "Skills", "Location"];

            var promise = XSeedApiFactory.getCandidateAdvanceSearchResult($scope.common.advanceSearchModel, $scope.common.pageSizeShow, $scope.common.pageNumberShow);
            promise.then(
                function(response) {
                    $scope.common.candidateList = response[0].data.Candidates;
                    $scope.common.totalCount = response[0].data.TotalCount;
                    $scope.common.totalPages = response[0].data.TotalPages;
                    firstPageFlagSearch = 1;

                    $scope.populateAdvanceSearchCandidateList($scope.common.candidateList, $scope.common.totalCount, $scope.common.totalPages, firstPageFlagSearch);
                    $scope.closeAdvanceSearchModal();
                    if ($scope.common.candidateList.length) {
                        //to select first row by default
                        $scope.setClickedRowCandidate($scope.common.candidateList[0]);
                    }
                    //to show skills in tooltip as string
                    angular.forEach($scope.common.candidateList, function(val, index) {
                        if (val.Skills) {
                            val.Skills = val.Skills.join();
                        }
                    });
                    blockUI.stop();


                },
                function(httpError) {
                    $scope.closeAdvanceSearchModal();
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Search', httpError.data.Status, httpError.data.Message);
                });
        }
    }

    $scope.populateAdvanceSearchCandidateList = function(candidateList, totalCount, totalPages, pageFlag) {
        //if (angular.isDefined($scope.common.tblCandidateList)) {
        //    $scope.common.tblCandidateList.reload();
        //}
        //else 
        {
            $scope.common.tblCandidateList = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: $scope.common.ngTablePaginationCount,
                total: totalCount,
                getData: function($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    if (pageFlag != 1) {
                        /* sorting */
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function(val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */

                        var promise = XSeedApiFactory.getCandidateAdvanceSearchResult($scope.common.advanceSearchModel, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder);
                        promise.then(
                            function(response) {
                                $scope.common.candidateList = response[0].data.Candidates;
                                $scope.common.totalCount = response[0].data.TotalCount;
                                $scope.common.totalPages = response[0].data.TotalPages;

                                //to select first row by default

                                if ($scope.common.candidateModel.manipulationMode != "Edit") {
                                    $scope.setClickedRowCandidate($scope.common.candidateList[0]);
                                } else {
                                    if ($scope.common.selectedCandidate) {
                                        $scope.getActivityListCandidate();
                                    }
                                    $scope.common.candidateModel.manipulationMode = undefined;
                                }
                                //to show skills in tooltip as string
                                angular.forEach($scope.common.candidateList, function(val, index) {
                                    if (val.Skills) {
                                        val.Skills = val.Skills.join();
                                    }
                                });
                                //$scope.candidatePageLookupList();
                                blockUI.stop();

                            },
                            function(httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('Search', httpError.data.Status, httpError.data.Message);
                            });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.common.candidateList);
                },
                $scope: $scope
            });
        }
    }


    /*****************************************Advance search requirements and candidates***************************************/

    /*****************************************Bulk upload resume***************************************/
    $scope.uploadAllResumes = function() {
        if ($scope.common.bulkUploadResume) {
            var validResumes = checkIsValidResumeFiles();
            if (validResumes.length) {
                uploadResumesToServer(validResumes)
            }
        }
    };

    function uploadResumesToServer(validResumes) {
        //check all files
        var resumeFiles = [];
        angular.forEach(validResumes, function(val, index) {
            if (val) {
                resumeFiles.push(val);
            }
        });

        ////check max 10 files allowed
        //if (resumeFiles.length > 10) {
        //    $scope.common.maxUploadFileCount = true;
        //}

        if (!$scope.common.invalidBulkUploadResume && !$scope.common.maxUploadSize) {
            blockUI.start();
            $scope.common.bulkUploadResume = resumeFiles;
            var promise = XSeedApiFactory.bulkResumeUpload($scope.common.bulkUploadResume);
            promise.then(
                function(response) {
                    blockUI.stop();
                    var uploadCount = response[0].data;

                    //update progress
                    $scope.common.bulkUploadResume.progress = 100;

                    XSeedAlert.swal({
                        title: 'Success!',
                        text: uploadCount + ' Resumes uploaded!',
                        type: "success",
                        customClass: "xseed-error-alert"
                    }).then(function() {
                        $timeout(function() {
                            //$scope.common.bulkUploadResume = undefined;
                            //$location.path('candidateList');
                        }, 0);
                    }, function(dismiss) {
                        blockUI.stop();
                    })
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('ResumeUpload', httpError.data.Status, httpError.data.Message);
                });
        }
    };

    function checkIsValidResumeFiles() {
        var attachments = [];

        var maxUploadSizeLimit = 0;
        $scope.common.invalidBulkUploadResume = false;
        //$scope.common.maxUploadFileCount = false;
        $scope.common.maxUploadSize = false;

        angular.forEach($scope.common.bulkUploadResume, function(val, index) {
            if (val) {
                var ext = val.name.match(/(?:\.([^.]+))?$/)[1];
                //for file size
                if (val) {
                    maxUploadSizeLimit = maxUploadSizeLimit + val.size;
                }

                //for file type
                if (angular.lowercase(ext) === 'pdf' || angular.lowercase(ext) === 'doc' || angular.lowercase(ext) === 'docx' || angular.lowercase(ext) === 'txt') {
                    val.invalidResumeFile = false;
                } else {
                    val.invalidResumeFile = true;
                    $scope.common.invalidBulkUploadResume = true;
                }
                attachments.push(val);
            }
        });
        //check total upload size is < 10mb
        if (maxUploadSizeLimit >= 10500000) {
            $scope.common.maxUploadSize = true;
        } else {
            $scope.common.maxUploadSize = false;
        }
        return attachments;
    };

    function checkIsInvalidFileInQueue() {
        for (var i = 0; i < $scope.common.bulkUploadResume.length; i++) {
            if ($scope.common.bulkUploadResume[i] && $scope.common.bulkUploadResume[i].invalidResumeFile) {
                $scope.common.invalidBulkUploadResume = true;
                break;
            } else {
                $scope.common.invalidBulkUploadResume = false;
            }
        }
    };

    $scope.removeSelectedResume = function(index) {
        //$scope.common.invalidBulkUploadResume = false;
        if ($scope.common.bulkUploadResume[index]) {
            $scope.common.bulkUploadResume.totalFileSize = $scope.common.bulkUploadResume.totalFileSize - $scope.common.bulkUploadResume[index].size;
            if ($scope.common.bulkUploadResume.totalFileSize <= 10500000) {
                $scope.common.maxUploadSize = false;
            }
            $scope.common.bulkUploadResume[index] = undefined;
            checkIsInvalidFileInQueue();
        }
    };

    $scope.clearAllResumes = function() {
        $scope.common.invalidBulkUploadResume = false;
        $scope.common.maxUploadSize = false;
        $scope.common.bulkUploadResume = undefined;

        $(".file-info").html("Upload resume files");
    };

    $scope.viewParsedResume = function() {
        $location.path('parsedResumes');
    };

    $scope.getParsedResumeList = function() {

        blockUI.start();
        var organizationId = $rootScope.userDetails.OrganizationId;
        var firstPageFlag = 0;
        $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
        $scope.common.pageNumberShow = undefined;

        var promise = XSeedApiFactory.getParsedResumeList($scope.common.pageSizeShow, $scope.common.pageNumberShow);
        promise.then(
            function(response) {
                console.log(response);
                $scope.common.parsedResumeList = response[0].data.ParsedResumes;
                $scope.common.totalCount = response[0].data.TotalCount;
                $scope.common.totalPages = response[0].data.TotalPages;

                //for download resume
                angular.forEach($scope.common.parsedResumeList, function(val, index) {
                    if (val.ResumeData) {
                        val.ResumeData.data = val.ResumeData.data.includes(",") ? val.ResumeData.data : ("data:;base64," + val.ResumeData.data);
                    }
                });

                firstPageFlag = 1;
                populateParsedResumeList($scope.common.parsedResumeList, $scope.common.totalCount, $scope.common.totalPages, firstPageFlag);
                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('ResumeUpload', httpError.data.Status, httpError.data.Message);
            });
    };

    function populateParsedResumeList(candidateList, totalCount, totalPages, pageFlag) {
        if (angular.isDefined($scope.common.tblParsedResumeList)) {
            $scope.common.tblParsedResumeList.reload();
        } else {
            $scope.common.tblParsedResumeList = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: $scope.common.ngTablePaginationCount,
                total: totalCount,
                getData: function($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    if (pageFlag != 1) {
                        /* sorting */
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function(val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */

                        var promise = XSeedApiFactory.getParsedResumeList($scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder);
                        promise.then(
                            function(response) {
                                $scope.common.parsedResumeList = response[0].data.ParsedResumes;
                                $scope.common.totalCount = response[0].data.TotalCount;
                                $scope.common.totalPages = response[0].data.TotalPages;

                                //for download resume
                                angular.forEach($scope.common.parsedResumeList, function(val, index) {
                                    if (val.ResumeData) {
                                        val.ResumeData.data = val.ResumeData.data.includes(",") ? val.ResumeData.data : ("data:;base64," + val.ResumeData.data);
                                    }
                                });

                                blockUI.stop();
                            },
                            function(httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('Resume Upload', httpError.data.Status, httpError.data.Message);
                            });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.common.parsedResumeList);
                },
                $scope: $scope
            });
        }

    };

    $scope.redirectToCandidateCreateView = function(parsedCandidate) {

        blockUI.start();
        $scope.common.candidateModel = parsedCandidate;
        $scope.common.candidateModel.Resume = parsedCandidate.ResumeData;
        $scope.common.candidateModel.ResumeContent = parsedCandidate.ResumeData.data;
        $scope.common.candidateModel.PrimaryEmail = parsedCandidate.EmailAddress;

        if ($scope.common.candidateModel.CountryId) {
            $scope.getStateListByCountry($scope.common.candidateModel.CountryId);
        }

        if ($scope.common.candidateModel.StateId) {
            $scope.getCityListByState($scope.common.candidateModel.StateId);
        }

        if ($scope.common.candidateModel.VisaType == null) {
            $scope.common.candidateModel.VisaType = "NA"
        }
        $scope.common.resumePresent = false;
        $scope.common.invalid = false;
        $scope.common.maxSize = false;
        $scope.common.invalidResume = false;
        $scope.common.displayValidationSummary = false;
        $scope.common.maxResumeSize = false;
        $scope.common.candidateModel.manipulationMode = "CreateParsed";
        blockUI.stop();

        $location.path("createCandidate");
    }

    $scope.rejectParsedResumeWithConfirmation = function(parsedResumeId) {
        blockUI.start();

        XSeedAlert.swal({
            title: "Are you sure you want to reject?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function() {
            blockUI.start();
            //IsValidResume is false
            $scope.acceptRejectParsedResume(parsedResumeId, false);
            blockUI.stop();
        }, function(dismiss) {});
        blockUI.stop();
    };

    $scope.acceptRejectParsedResume = function(parsedResumeId, isValidResume) {
        var promise = XSeedApiFactory.acceptRejectParsedResume(parsedResumeId, isValidResume);
        promise.then(
            function(response) {
                //change status
                angular.forEach($scope.common.parsedResumeList, function(val, index) {
                    if (val._id == parsedResumeId) {
                        val.IsValidResume = true;

                        if (val.manipulationMode == "CreateParsed") {
                            $location.path('parsedResumes');
                        }
                    }
                });
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('ResumeUpload', httpError.data.Status, httpError.data.Message);
            });
    };

    $scope.goToParsedResume = function() {
        $location.path('parsedResumes');
    };

    /*****************************************Bulk upload resume***************************************/

    /*****************************************Resume Emails***************************************/
    $scope.getResumeEmailList = function() {
        if ($rootScope.userHasToken == false) {
            $location.path('login');
            return false;
        }

        var resumeEmailListURL = '/resumeEmails';

        var dashboardURL = '/dashboard';

        if (($location.path().toLowerCase() == resumeEmailListURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Vendor_Read == "False") {
            $location.path('dashboard');
            return false;
        }

        blockUI.start();
        var organizationId = $rootScope.userDetails.OrganizationId;
        var firstPageFlag = 0;
        $scope.common.pageSizeShow = undefined;
        $scope.common.pageNumberShow = undefined;

        var promise = XSeedApiFactory.getResumeEmailList(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow);
        promise.then(
            function(response) {

                $scope.resumeEmailList = response[0].data.ResumeEmail
                $scope.common.totalCount = response[0].data.TotalCount;
                $scope.common.totalPages = response[0].data.TotalPages;
                if ($scope.resumeEmailList.length) {
                    //to select first row by default
                    $scope.showResumeEmailDetails($scope.resumeEmailList[0].Id);
                }
                firstPageFlag = 1;
                populateResumeEmailList(organizationId, $scope.resumeEmailList, $scope.common.totalCount, $scope.common.totalPages, firstPageFlag);

                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Resume', httpError.data.Status, httpError.data.Message);
            });
    };

    function populateResumeEmailList(organizationId, resumeEmailList, totalCount, totalPages, pageFlag) {
        if (angular.isDefined($scope.tblResumeEmailList)) {
            $scope.tblResumeEmailList.reload();
        } else {
            $scope.tblResumeEmailList = new ngTableParams({
                page: 1,
                count: 15,
                sorting: {}
            }, {
                counts: [15, 20, 25],
                total: totalCount,
                getData: function($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    /* sorting */
                    if (pageFlag != 1) {
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function(val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        ///* sorting */

                        var promise = XSeedApiFactory.getResumeEmailList(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder);
                        promise.then(
                            function(response) {
                                $scope.resumeEmailList = response[0].data.ResumeEmail;
                                $scope.common.totalCount = response[0].data.TotalCount;
                                $scope.common.totalPages = response[0].data.TotalPages;

                                //to select first row by default
                                $scope.showResumeEmailDetails($scope.resumeEmailList[0].Id);
                                blockUI.stop();
                            },
                            function(httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('Resume', httpError.data.Status, httpError.data.Message);
                            });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.resumeEmailList);
                },
                $scope: $scope
            });
        }
    };

    $scope.showResumeEmailDetails = function(resumeEmailId) {
        $scope.showEmailDetail = true;
        $scope.selectedResumeEmail = {};

        getResumeEmailDetails(resumeEmailId);
    };

    function getResumeEmailDetails(resumeEmailId) {
        var organizationId = $rootScope.userDetails.OrganizationId;
        blockUI.start();
        var promise = XSeedApiFactory.getResumeEmailList(organizationId, undefined, undefined, undefined, undefined, resumeEmailId);
        console.log(promise);
        promise.then(
            function(response) {
                console.log(response);
                $scope.selectedResumeEmail = response[0].data;
                blockUI.stop();
                updateResumeEmailDetails($scope.selectedResumeEmail.IsRead);
            },
            function(httpError) {
                ExceptionHandler.handlerHTTPException('Resume', httpError.data.Status, httpError.data.Message);
            });
    };

    function updateResumeEmailDetails(IsRead) {
        if (!IsRead) {
            $scope.selectedResumeEmail.IsRead = true;
            var promise = XSeedApiFactory.updateResumeEmailDetails($scope.selectedResumeEmail.Id);
            promise.then(
                function(response) {
                    $scope.unreadMailCount = $scope.unreadMailCount - 1;
                    angular.forEach($scope.resumeEmailList, function(val, index) {
                        if (val.Id == $scope.selectedResumeEmail.Id) {
                            val.IsRead = $scope.selectedResumeEmail.IsRead;
                        }
                    })
                },
                function(httpError) {
                    ExceptionHandler.handlerHTTPException('Resume', httpError.data.Status, httpError.data.Message);
                });
        }
    };

    $scope.getUnreadMailCount = function() {
        var promise = XSeedApiFactory.getUnreadResumeMailCount($rootScope.userDetails.OrganizationId);
        promise.then(
            function(response) {
                blockUI.stop();
                $scope.unreadMailCount = response[0].data;
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('ResumeEmail', httpError.data.Status, httpError.data.Message);
            });
    };

    $scope.showHideEmail = function() {
        $scope.showEmailDetail = false;
    };

    /*****************************************Resume Emails***************************************/

    /*****************************************Common schedule modal***************************************/

    $scope.openCommonScheduleModal = function(typeOfSchedule, currentSection) {

        $scope.hstep = 1;
        $scope.mstep = 15;

        $scope.common.commonScheduleModel = {};
        $scope.common.commonScheduleModel.Type = typeOfSchedule;
        $scope.common.commonScheduleModel.StartTime = new Date();
        $scope.common.commonScheduleModel.EndTime = new Date();

        $scope.common.currentSection = currentSection;
        if ($scope.common.selectedCandidate) {
            $scope.common.commonScheduleModel.Candidates = [];
            $scope.common.commonScheduleModel.CandidateNames = [];

            $scope.common.commonScheduleModel.Candidates.push($scope.common.selectedCandidate._id);
            $scope.common.commonScheduleModel.CandidateNames.push($scope.common.selectedCandidate.FirstName + " " + $scope.common.selectedCandidate.LastName);
        }

        $scope.common.commonScheduleModal.$promise.then($scope.common.commonScheduleModal.show);
    };

    $scope.closeCommonScheduleModal = function() {
        $scope.common.commonScheduleModal.$promise.then($scope.common.commonScheduleModal.hide);
        $scope.flag.isEditSchedule = false;
    };

    $scope.saveSchedule = function(commonScheduleForm, currentSection) {
        if (new ValidationService().checkFormValidity(commonScheduleForm)) {

            if ($scope.common.commonScheduleModel.ReminderTime) {
                $scope.common.commonScheduleModel.IsSetReminder = true;
            }

            //if (currentSection == $scope.constants.sectionCandidates) 
            {
                //$scope.closeCommonScheduleModal();
                var promise = XSeedApiFactory.saveSchedule($scope.common.commonScheduleModel);
                promise.then(
                    function(response) {
                        XSeedAlert.swal({
                            title: 'Success!',
                            text: 'Schelduled!',
                            type: "success",
                            customClass: "xseed-error-alert"
                        }).then(function() {
                            $scope.closeCommonScheduleModal(),
                                $timeout(function() {}, 0);
                        }, function(dismiss) {
                            blockUI.stop();
                        })
                    },
                    function(httpError) {
                        blockUI.stop();
                        ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
                    });
                blockUI.stop();
            }
        }
    };

    $scope.getScheduleLogList = function() {
        blockUIConfig.autoBlock = false;
        var organizationUserId = $rootScope.userDetails.UserId;
        var promise = XSeedApiFactory.getScheduleLogList(organizationUserId);
        promise.then(
            function(response) {
                $scope.common.scheduleLogList = response[0].data;
                blockUIConfig.autoBlock = true;
            },
            function(httpError) {
                blockUIConfig.autoBlock = true;
                ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
            });
    };

    $scope.editScheduleView = function(selectedSchedule) {
        $scope.hstep = 1;
        $scope.mstep = 15;
        if (selectedSchedule) {
            $scope.flag.isEditSchedule = true;
            $scope.common.currentSection = $scope.constants.sectionCandidates;
            $scope.common.commonScheduleModel = selectedSchedule;
            //get other lookup
            $scope.getCompanyContactsLookup(selectedSchedule.CompanyId);
            $scope.getRequirementListByCompany(selectedSchedule.CompanyId);
            $scope.closeCommonReminderModal();
            $scope.common.commonScheduleModal.$promise.then($scope.common.commonScheduleModal.show);
        }
    };

    $scope.editSchedule = function(commonScheduleForm, currentSection) {
        if (new ValidationService().checkFormValidity(commonScheduleForm)) {
            if (currentSection == $scope.constants.sectionCandidates) {
                var promise = XSeedApiFactory.editSchedule($scope.common.commonScheduleModel);
                promise.then(
                    function(response) {
                        XSeedAlert.swal({
                            title: 'Success!',
                            text: 'Schelduled!',
                            type: "success",
                            customClass: "xseed-error-alert"
                        }).then(function() {
                            $scope.closeCommonScheduleModal(),
                                $timeout(function() {
                                    $scope.getScheduleLogList();
                                }, 0);
                        }, function(dismiss) {
                            blockUI.stop();
                        })
                    },
                    function(httpError) {
                        blockUI.stop();
                        ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
                    });
                blockUI.stop();
            }
        }
    };

    $scope.buildLookUpBySectionForSchedule = function(section) {
        $scope.common.scheduleLookUpBySection = {};
        //for common lookups
        commonLookUpBySectionForSchedule();
        //for other lookups
        getLookUpForSchedule();
    };

    function commonLookUpBySectionForSchedule() {
        $scope.common.scheduleLookUpBySection.priorityLookUp = [];
        $scope.common.scheduleLookUpBySection.priorityLookUp.push({ Id: "Normal", Name: "Normal" }, { Id: "Medium", Name: "Medium" }, { Id: "High", Name: "High" });

        $scope.common.scheduleLookUpBySection.AutoFollowUpLookUp = [];
        $scope.common.scheduleLookUpBySection.AutoFollowUpLookUp.push({ Id: "1day", Name: "1 day" }, { Id: "2day", Name: "2 day" }, { Id: "3day", Name: "3 day" }, { Id: "1weeks", Name: "1 weeks" }, { Id: "2weeks", Name: "2 weeks" }, { Id: "3weeks", Name: "3 weeks" }, { Id: "1Month", Name: "1 Month" }, { Id: "2Month", Name: "2 Month" }, { Id: "3Month", Name: "3 Month" }, { Id: "1Year", Name: "1 Year" }, { Id: "2Year", Name: "2 Year" }, { Id: "3Year", Name: "3 Year" });

        $scope.common.scheduleLookUpBySection.ReminderLookUp = [];
        $scope.common.scheduleLookUpBySection.ReminderLookUp.push({ Id: "5Minutes", Name: "5 Minutes" }, { Id: "10Minutes", Name: "10 Minutes" }, { Id: "15Minutes", Name: "15 Minutes" }, { Id: "30Minutes", Name: "30 Minutes" }, { Id: "45Minutes", Name: "45 Minutes" }, { Id: "1Hours", Name: "1 Hours" }, { Id: "2Hours", Name: "2 Hours" }, { Id: "3Hours", Name: "3 Hours" }, { Id: "1Day", Name: "1 Day" }, { Id: "2Days", Name: "2 Days" }, { Id: "3Days", Name: "3 Days" });
    };

    function getLookUpForSchedule() {
        var organizationId = $rootScope.userDetails.OrganizationId;
        var promise = XSeedApiFactory.getLookUpForSchedule(organizationId);
        promise.then(
            function(response) {
                $scope.common.scheduleLookUpBySection.contactsLookUp = response[0].data;
                $scope.common.scheduleLookUpBySection.companyLookUp = response[1].data;
            },
            function(httpError) {
                ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
            });
    };

    $scope.getRequirementListByCompany = function(companyId) {
        var promise = XSeedApiFactory.getRequirementListByCompany(companyId);
        promise.then(
            function(response) {
                $scope.common.scheduleLookUpBySection.requirementLookUp = response[0].data;
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });
    };

    /*****************************************Common schedule modal***************************************/

    /*****************************************Common notification and reminder update***************************************/
    $scope.callAtInterval = function() {
        $scope.getScheduleLogList();
    }

    //$interval(function () { $scope.callAtInterval(); }, 300000);

    $scope.openCommonReminderModal = function(scheduleDetails) {
        $scope.common.commonReminderModel = scheduleDetails;

        //Manupulation of data
        //if ($scope.common.commonScheduleModel.StartTime) {
        //    $scope.common.commonScheduleModel.StartTime = $scope.common.commonScheduleModel.StartTime.toLocaleTimeString();
        //}
        //if ($scope.common.commonScheduleModel.EndTime) {
        //    $scope.common.commonScheduleModel.EndTime = $scope.common.commonScheduleModel.EndTime.toLocaleTimeString();
        //}
        $scope.common.commonReminderModal.$promise.then($scope.common.commonReminderModal.show);
    };

    $scope.closeCommonReminderModal = function() {
        $scope.common.commonReminderModal.$promise.then($scope.common.commonReminderModal.hide);
        //$scope.common.advanceSearchModel = {};
    };

    /*****************************************Common notification and reminder update***************************************/

    /*****************************************Display company contact in modal***************************************/
    $scope.closeCompanyContactModal = function() {
        $scope.common.companyContactModal.$promise.then($scope.common.companyContactModal.hide);
    };

    $scope.getCompanyContact = function(companyId, contactId) {
        if (companyId && contactId) {
            blockUI.start();
            var promise = XSeedApiFactory.getCompanyContact(companyId, contactId);
            promise.then(
                function(response) {
                    blockUI.stop();
                    $scope.common._dataCompanyContact = response[0].data;
                    $scope.common.companyContactModal.$promise.then($scope.common.companyContactModal.show);
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Company', httpError.data.Status, httpError.data.Message);
                });
        }
    };

    /*****************************************Display company contact in modal***************************************/


    /*****************************************Display Candidate to Submit Modal***************************************/

    $scope.closeCandidatesToSubmitModal = function() {
        $scope.common.candidatesToSubmitModal.$promise.then($scope.common.candidatesToSubmitModal.hide);
        //$scope.common.advanceSearchModel = {};
    };

    $scope.getMonsterCandidates = function() {
        var skillset = $scope.common.selectedJob.TechnicalSkills;
        var promise = XSeedApiFactory.getMonsterCandidates(skillset);
        promise.then(
            function(response) {
                $scope.common.monsterCandidate = response[0].data;

            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });
    };

    $scope.downloadMonsterCandidate = function(resumeId, resumeModifiedDate, firstName, lastName) {
            var promise = XSeedApiFactory.downloadMonsterCandidate(resumeId, resumeModifiedDate, firstName, lastName);
            promise.then(
                function(response) {
                    alert(response[0].data);

                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                });
        }
        /*****************************************Display Candidate to Submit Modal***************************************/



    /*****************************************Display requirement details in modal***************************************/

    $scope.openRequirementDetailModal = function() {
        $scope.common.currentSection = $scope.constants.sectionRequirements;
        $scope.getRequirementDetails();
        $scope.common.requirementDetailModal.$promise.then($scope.common.requirementDetailModal.show);
    };

    $scope.closeRequirementDetailModal = function(callFromEdit) {
        $scope.common.requirementDetailModal.$promise.then($scope.common.requirementDetailModal.hide);
        //$scope.common.advanceSearchModel = {};
        if (!callFromEdit) {
            $scope.common.requirementModel = {};
        }

    };

    $scope.getcollpasingRequirementDetails = function() {
        blockUI.start();
        var organizationId = $rootScope.userDetails.OrganizationId;
        $scope.common.requisitionId = {};
        var promise = XSeedApiFactory.getcollpasingJobDetail($scope.common.selectedJob.Id, organizationId);
        promise.then(
            function(response) {

                blockUI.stop();

                $scope.common.requirementModel = response[0].data;
                console.log($scope.common.requirementModel);
                $scope.common.requisitionId = $scope.common.requirementModel.RequisitionId;

                if ($scope.common.requirementModel.TechnicalSkills) {
                    $scope.common.requirementModel.SkillList = [];

                    $scope.common.requirementModel.SkillList = $scope.common.requirementModel.TechnicalSkills.split("|");
                    angular.forEach($scope.common.requirementModel.SkillList, function(val, index) {
                        $scope.lookup.skillListData.push({ value: val, label: val });
                    });


                }
                setTimeout(function() {
                    $('select.select2').select2();
                }, 0);

                var list = [];
                var nameList = [];
                for (var i = 0; i < $scope.common.requirementModel.DegreeList.length; i++) {
                    list.push($scope.common.requirementModel.DegreeList[i].Id);
                    nameList.push($scope.common.requirementModel.DegreeList[i].Name);
                }
                $scope.common.requirementModel.DegreeList = list;
                $scope.common.requirementModel.DegreeNameList = nameList;

                var Visalist = [];
                var VisaNameList = [];
                for (var i = 0; i < $scope.common.requirementModel.VisaTypeList.length; i++) {
                    Visalist.push($scope.common.requirementModel.VisaTypeList[i].Id);
                    VisaNameList.push($scope.common.requirementModel.VisaTypeList[i].Name);
                }
                $scope.common.requirementModel.VisaTypeList = Visalist;
                $scope.common.requirementModel.VisaTypeNameList = VisaNameList;

                $scope.common.requirementModel.JobPostedDate = $filter('date')($scope.common.requirementModel.JobPostedDate, "MM/dd/yyyy");
                $scope.common.requirementModel.JobClosedDate = $filter('date')($scope.common.requirementModel.JobClosedDate, "MM/dd/yyyy");
                $scope.common.requirementModel.JobExpiryDate = $filter('date')($scope.common.requirementModel.JobExpiryDate, "MM/dd/yyyy");

                $scope.common.requirementModel.manipulationMode = "Edit";



                $scope.bindcollapsingData($scope.common.requirementModel, $scope.common.selectedJob.Id);


            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });
    }

    $scope.getRequirementDetails = function() {
        blockUI.start();
        var organizationId = $rootScope.userDetails.OrganizationId;
        $scope.common.requisitionId = {};
        var promise = XSeedApiFactory.getJobDetail($scope.common.selectedJob.Id, organizationId);
        promise.then(
            function(response) {

                blockUI.stop();

                $scope.common.requirementModel = response[0].data;
                console.log($scope.common.requirementModel);
                $scope.common.requisitionId = $scope.common.requirementModel.RequisitionId;

                if (!$scope.isUndefinedOrNull($scope.common.requirementModel.CountryId) && !$scope.isEmpty($scope.common.requirementModel.CountryId)) {
                    $scope.getStateListByCountry($scope.common.requirementModel.CountryId);
                }

                if (!$scope.isUndefinedOrNull($scope.common.requirementModel.StateId) && !$scope.isEmpty($scope.common.requirementModel.StateId)) {
                    $scope.getCityListByState($scope.common.requirementModel.StateId);
                }

                if (!$scope.isUndefinedOrNull($scope.common.requirementModel.CompanyId) && !$scope.isEmpty($scope.common.requirementModel.CompanyId)) {
                    $scope.getCompanyContactsLookup($scope.common.requirementModel.CompanyId);
                }

                var list = [];



                for (var i = 0; i < $scope.common.requirementModel.OrganizationUser.length; i++) {
                    list.push($scope.common.requirementModel.OrganizationUser[i].Id);
                }



                $scope.common.requirementModel.OrganizationUserIdList = list;

                if ($scope.common.requirementModel.TechnicalSkills) {
                    $scope.common.requirementModel.SkillList = [];

                    $scope.common.requirementModel.SkillList = $scope.common.requirementModel.TechnicalSkills.split("|");
                    angular.forEach($scope.common.requirementModel.SkillList, function(val, index) {
                        $scope.lookup.skillListData.push({  label: val });
                    });


                }
                setTimeout(function() {
                    $('select.select2').select2({allowClear:true});
                }, 0);

                var list = [];
                var nameList = [];
                for (var i = 0; i < $scope.common.requirementModel.DegreeList.length; i++) {
                    list.push($scope.common.requirementModel.DegreeList[i].Id);
                    nameList.push($scope.common.requirementModel.DegreeList[i].Name);
                }
                $scope.common.requirementModel.DegreeList = list;
                $scope.common.requirementModel.DegreeNameList = nameList;
                var Visalist = [];
                var VisaNameList = [];
                for (var i = 0; i < $scope.common.requirementModel.VisaTypeList.length; i++) {
                    Visalist.push($scope.common.requirementModel.VisaTypeList[i].Id);
                    VisaNameList.push($scope.common.requirementModel.VisaTypeList[i].Name);
                }
                $scope.common.requirementModel.VisaTypeList = Visalist;
                $scope.common.requirementModel.VisaTypeNameList = VisaNameList;

                $scope.common.requirementModel.JobPostedDate = $filter('date')($scope.common.requirementModel.JobPostedDate, "MM/dd/yyyy");
                $scope.common.requirementModel.JobClosedDate = $filter('date')($scope.common.requirementModel.JobClosedDate, "MM/dd/yyyy");
                $scope.common.requirementModel.JobExpiryDate = $filter('date')($scope.common.requirementModel.JobExpiryDate, "MM/dd/yyyy");

                $scope.common.requirementModel.manipulationMode = "Edit";




            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
            });
    };

    $scope.bindcollapsingData = function(SelectedJob, ID) {
        SelectedJob.Id = ID;
        $scope.requirementFilter.CollapseStatus = true;

        $scope.appliedCandidateCount = $scope.common.appliedCandidateList && $scope.common.appliedCandidateList.length ? $scope.common.appliedCandidateList.length : 0;
        console.log('appliedCandidateCount' + $scope.appliedCandidateCount);
        $scope.associatedCandidateCount = SelectedJob.AssociateCandidate && SelectedJob.AssociateCandidate.length ? SelectedJob.AssociateCandidate.length : 0;
        $scope.common.currentSection = $scope.constants.sectionRequirements;

        //Remove
        $('#' + SelectedJob.Id).find('.fa-chevron-down').removeClass("fa-chevron-down").addClass("fa-chevron-right");

        if ($('#' + SelectedJob.Id).parent('tr').next('.childtr').is(":visible")) {
            $('#' + SelectedJob.Id).find('.fa-chevron-down').removeClass("fa-chevron-down").addClass("fa-chevron-right");
            $('#' + SelectedJob.Id).parent('tr').next('.childtr').remove();
            $('#' + SelectedJob.Id).parent('tr').removeClass("tr-collapse-color");

            return;
        } else {
            $('#' + SelectedJob.Id).parent('tr').addClass("tr-collapse-color");

            //Bind HTML
            var $curRow = $('#' + SelectedJob.Id).closest('tr');
            var graphHtml = '<tr class=\'card panel-heading disp-none childtr\'>' +
                '                        <td data-title=\'Requirement\' colspan=\'12\' class=\'chart-td\'>' +
                '                            <div class=\'row\'>' +
                '                                <div class=\'col-md-12\'>' +
                '                                    <div class=\'col-md-2\'>                                                          <label class=\'labelauty-label\'>                                                                  <p class=\'card-title display-flex font-12\'>';

            if ($scope.appliedCandidateCount == 0 && $scope.associatedCandidateCount == 0) {
                graphHtml += '<div id=\'canvas-holder\'><img src=\'images/no_candidate.png\' style=\' width:126px;\'><h6 class=\'color-gray\'>No candidates</h6></canvas></div>';

            } else {
                graphHtml += '<div id=\'canvas-holder\'><canvas  id=\'chartarea' + SelectedJob.Id + '\' width=\'160\' height=\'160\' class=\'chartarea\'></canvas></div>';

            }

            graphHtml += '</p>                            </label>                                                      </div>                                                        <div class=\'col-md-10\'>' +
                '                                        <div class=\'container-fluid\'>' +
                '                                            <ul class=\'nav nav-tabs\' role=\'tablist\'>                                                                  <li class=\'nav-item\'><a class=\'nav-link active\' data-toggle=\'tab\' href=\'#one1' + SelectedJob.Id + '\'  target=\'_self\' role=\'tab\' aria-expanded=\'true\'>General</a></li>                                                                  <li class=\'nav-item \'><a class=\'nav-link\' data-toggle=\'tab\' href=\'#three1' + SelectedJob.Id + '\' target=\'_self\' role=\'tab\' aria-expanded=\'false\' ng-click=\'onlyappliedCandidatesForJob(' + '"' + SelectedJob.Id + '"' + ')\'>Candidate</a></li>                                                                  <li class=\'nav-item \'><a class=\'nav-link\' data-toggle=\'tab\' href=\'#five1' + SelectedJob.Id + '\' target=\'_self\' role=\'tab\' aria-expanded=\'false\'>Qualification</a></li>                                                                  <li class=\'nav-item \'><a class=\'nav-link\' data-toggle=\'tab\' href=\'#two1' + SelectedJob.Id + '\' target=\'_self\' role=\'tab\' aria-expanded=\'false\'>Rate</a></li>                                                                  <li class=\'nav-item \'><a class=\'nav-link\' data-toggle=\'tab\' href=\'#four1' + SelectedJob.Id + '\' target=\'_self\' role=\'tab\' aria-expanded=\'false\'>Other</a></li>        <li class=\'nav-item \'><a class=\'nav-link\' data-toggle=\'tab\' href=\'#six1' + SelectedJob.Id + '\' target=\'_self\' role=\'tab\' aria-expanded=\'false\'>Description</a></li>                                                      </ul>                                                                <div class=\'tab-content\'>' +
                '                                                <div id=\'one1' + SelectedJob.Id + '\' class=\'tab-pane active \'  role=\'tabpanel\' aria-expanded=\'true\'>' +
                '                                                    <div class=\'row\'>' +
                '                                                        <div class=\'col-md-6\'>' +
                '                                                            <div class=\'card disp-block\'>' +
                '                                                                <div class=\'card-header f-w-500\'><i class=\'material-icons font-21 m-r-xs color-dark-gray\' aria-hidden=\'true\'>business </i>Company Detail</div>                                                                                  <div class=\'card-block\'>' +
                '                                                                    <div>' +
                '                                                                        <div class=\'col-md-4 p-0 f-w-600\'>' +
                '                                                                            Contact Email' +
                '                                                                        </div>                                                                                          <div class=\'col-md-8 p-0\'>' +
                '                                                                            ' + (SelectedJob.CompanyContactName ? SelectedJob.CompanyContactName : '-') +
                '                                                                        </div>' +
                '                                                                    </div>                                                                                        <div class=\'col-md-4 p-0 f-w-600\'>' +
                '                                                                        Ext. Req. Id' +
                '                                                                    </div>                                                                                      <div class=\'col-md-3 p-0\'>' +
                '                                                                        ' + (SelectedJob.ClientJobCode ? SelectedJob.ClientJobCode : '-') +
                '                                                                    </div>                                                                                      <div class=\'col-md-3 p-0 f-w-600\'>' +
                '                                                                        Openings' +
                '                                                                    </div>                                                                                      <div class=\'col-md-2 p-0\'>' +
                '                                                                        ' + (SelectedJob.TotalPositions ? SelectedJob.TotalPositions : '-') +
                '                                                                    </div>' +
                '                                                                </div>' +
                '                                                            </div>' +
                '                                                        </div>                                                                          <div class=\'col-md-6\'>                                                                              <div class=\'card disp-block\'>                                                                                  <div class=\'card-header f-w-500\'><i class=\'material-icons font-21 m-r-xs color-dark-gray\' aria-hidden=\'true\'>access_time </i>Duration</div>                                                     <div class=\'card-block\'>                                                                                      <div class=\'col-md-4\'>                                                                                          <div class=\'col-md-6 p-0 f-w-600\'>                                                       <div>Duration</div>                                                                                  <div>Weekly</div>                                                                                          </div>                                                                                          <div class=\'col-md-6 p-0\'>                                                                        <div>' + (SelectedJob.Duration ? SelectedJob.Duration : '-') + '</div>                                                                                    <div>' + (SelectedJob.WeeklyHours ? SelectedJob.WeeklyHours + ' Hours' : '-') + '</div>                                                                                   </div>                                                                                      </div>                                                                                      <div class=\'col-md-8\'>                                                                                        <div class=\'col-md-5 p-0 f-w-600\'>                                                                   <div>Position Expiry</div>                                                                          <div>Total</div>                                                                               </div>                                                                                          <div class=\'col-md-7\'>                                                                 <div>' + (SelectedJob.JobExpiryDate ? SelectedJob.JobExpiryDate : '-') + '</div>                                                   <div><span>' + (SelectedJob.TotalHours ? SelectedJob.TotalHours + ' Hours' : '-') + '</span></div>                                                                              </div>                                                                                      </div>                                                                                      </div>                                                                              </div>                                                                          </div>' +
                '                                                    </div>' +
                '                                                </div>                                                                    <div id=\'three1' + SelectedJob.Id + '\' class=\'tab-pane  \' role=\'tabpanel\' aria-expanded=\'false\'>' +
                '                                                    <div class=\'row\'>' +
                '                                                        <div class=\'col-md-6\'>' +
                '                                                            <div class=\'disp-block\'>' +
                '                                                                <div class=\'card-header f-w-500\'><i class=\'material-icons font-21 m-r-xs color-dark-gray color-dark-gray\' aria-hidden=\'true\'>people </i>Associated Candidate(s)</div>' +
                '                                                                <div class=\'card-block internal-div-height-overflow\'><div class=\'table-responsive scroll slimScroll\'>';

            //----------------------------------------------------------------------
            //APPEND Candidate data
            //----------------------------------------------------------------------
            if ($scope.associatedCandidateCount > 0) {
                for (var j = 0; j < SelectedJob.AssociateCandidate.length; j++) {

                    if (SelectedJob.AssociateCandidate) {
                        if (j >= SelectedJob.AssociateCandidate.length) {
                            break;
                        }
                        graphHtml += '<a ng-click=\'openSingleCandidateDetailView(' + '"' + SelectedJob.AssociateCandidate[j].Id + '"' + ')\' class=\'col-lg-6 p-b-3px\' href=> <span class=\'badge_large bg-primary bg-grey-dark \'>' + SelectedJob.AssociateCandidate[j].Name + '</span></a>'
                    }


                }
            }
            //----------------------------------------------------------------------
            //Applied candidates
            //----------------------------------------------------------------------
            graphHtml += '   </div> </div>' +
                '                                                            </div>' +
                '                                                        </div>                                                                          <div class=\'col-md-6\'>                                                                              <div class=\'disp-block\'>                                                                                  <div class=\'card-header f-w-500\'><i class=\'material-icons font-21 m-r-xs color-dark-gray color-dark-gray\' aria-hidden=\'true\'>people </i>Applied Candidate(s)</div>                                                                                  <div id=\'appliedcandidate' + SelectedJob.Id + '\' class=\'card-block\'>';

            if ($scope.appliedCandidateCount > 0) {
                for (var j = 0; j < 3; j++) {
                    if ($scope.common.appliedCandidateList && j >= $scope.common.appliedCandidateList.length) {
                        break;
                    } else if (j == 2) {
                        graphHtml += '<a href ng-click=\'showAppliedCandidates_click(null)\'> <span class=\'badge_large bg-primary bg-grey-dark \'>Show more</span></a>';
                        break;
                    }

                    graphHtml += '<a href ng-click=\'showAppliedCandidates_click(' + '"' + $scope.common.appliedCandidateList[j]._id + '"' + ')\'> <span class=\'badge_large bg-primary bg-grey-dark \'>' + $scope.common.appliedCandidateList[j].FirstName + ' ' + $scope.common.appliedCandidateList[j].LastName + '</span></a>';

                }
            }
            //----------------------------------------------------------------------
            graphHtml += '</div>                                                                              </div>                                                                          </div>' +
                '                                                    </div>' +
                '                                                </div>                                                                  <div class=\'tab-pane\' id=\'two1' + SelectedJob.Id + '\' role=\'tabpanel\' aria-expanded=\'false\'>' +
                '                                                    <div class=\'row card-block p-l-0\'>' +
                '                                                        <div class=\'col-md-12\'>' +
                '                                                            <div class=\'\'>' +
                '                                                                <div class=\'col-md-3 p-0\' >' +
                '                                                                    <div class=\'col-md-6 p-0 f-w-600\'>' +
                '                                                                        <div>' +
                '                                                                            Business Unit' +
                '                                                                        </div>                                                                                          <div>Per Diem</div>                                                                                          <div>Client Rate</div>' +
                '                                                                    </div>                                                                                      <div class=\'col-md-6 p-0\'>                                                                                          <div>' + (SelectedJob.BusinessUnitName ? SelectedJob.BusinessUnitName : '-') + '</div>                                                                                          <div>' + (SelectedJob.PerDiem ? SelectedJob.PerDiem : '-') + '</div>                                                                                          <div>' + (SelectedJob.ClientRate ? SelectedJob.ClientRate : '-') + '</div>                                                                                        </div>' +
              '                                                                </div>                                                                                  <div class=\'col-md-3\'>                                                                                      <div class=\'col-md-6 p-0 f-w-600\'>                                                                                          <div>Client Rate Type</div>                                                                                          <div>Margin</div>                                                                                          <div>Pay Rate</div>                                                                                      </div>                                                                                      <div class=\'col-md-6 p-0\'>                                                                                          <div>' + (SelectedJob.ClientRateType ? SelectedJob.ClientRateType : '-') + '</div>                                                                                          <div>' + (SelectedJob.Margin ? SelectedJob.Margin : '-') + '</div>                                                                                          <div>' + (SelectedJob.PayRate ? SelectedJob.PayRate : '-') + '</div>                                                                                      </div>                                                                                  </div>                                                                                  <div class=\'col-md-3\'>                                                                                      <div class=\'col-md-6 p-0 f-w-600\'>                                                                                          <div>Pay Rate Type</div>                                                                                          <div>Fee %</div>                                                                                          <div>Salary $</div>                                                                                        </div>                                                                                      <div class=\'col-md-6 p-0\'>                                                                                          <div>' + (SelectedJob.PayRateType ? SelectedJob.PayRateType : '-') + '</div>                                                                                          <div>' + (SelectedJob.FeePercent ? SelectedJob.FeePercent + '%' : '-') + '</div>                                                                                          <div>' + (SelectedJob.MinSalary ? SelectedJob.MinSalary : '-') + '-' + (SelectedJob.MaxSalary ? SelectedJob.MaxSalary : '-') + '</div>                                                                                      </div>                                                                                  </div><div class=\'col-md-3\'>                                                                                      <div class=\'col-md-6 p-0 f-w-600\'>                                                                                          <div>Client Name</div>                                                                                          <div>Client Details</div>                                                                                                                                                                                </div>                                                                                      <div class=\'col-md-6 p-0\'>                                                                                          <div>' + (SelectedJob.CompanyName ? SelectedJob.CompanyName : '-') + '</div>                                                                                          <div>' + (SelectedJob.CompanyContactName ? SelectedJob.CompanyContactName : '-') + '</div><div>' + (SelectedJob.Location ? SelectedJob.Location : '-') + '</div>                                                                                       </div>                                                                                  </div>' +
                '                                                            </div>' +
                '                                                        </div>' +
                '                                                    </div>' +
              '                                                </div>                                                                  <div class=\'tab-pane\' id=\'six1' + SelectedJob.Id + '\' role=\'tabpanel\' aria-expanded=\'false\'>' +
                '                                                    <div class=\'row card-block p-l-0\'>' +
                '                                                        <div class=\'col-md-12\'>' +
                '                                                            <div class=\'\'>' +
                '                                                                <div class=\'row\' >' +
                '                                                                    <div class=\'col-md-2 p-0 f-w-600\'>' +
                '                                                                        <div>' +
                '                                                                            Description' +
                '                                                                        </div> ' +
                '                                                                    </div>                                                                                      <div class=\'col-md-10 p-0\'>                                                                                          <div>' + (SelectedJob.JobDescription ? SelectedJob.JobDescription : '-') + '</div>                                                                                                                                                                      </div>' +
                '                                                                 </div> ' +
                '                                                                 <div class=\'row\' >' +
                '                                                               <div class=\'col-md-2 p-0 f-w-600\'>' +
                '                                                                        <div>' +
                '                                                                            Techincal Skills' +
                '                                                                        </div> '+                                                                                       
                '                                                                    </div>                                               <div class=\'col-md-10 p-0\'>        <div>' + (SelectedJob.SkillList ? SelectedJob.SkillList : '-') + '</div>             </div>' +
                '                                                               </div> ' +
                '                                                            </div>' +
                '                                                        </div>' +
                '                                                    </div>' +
              '                                                </div>                                                                  <div id=\'four1' + SelectedJob.Id + '\'  class=\'tab-pane \' role=\'tabpanel\' aria-expanded=\'false\'>                                                                      <div class=\'row card-block p-l-0\'>                                                                                      <div class=\'col-md-4\'>                                                                              <div class=\'col-md-6 p-0 f-w-600\'>                                                                                    <div>Communication</div>                                                                              </div>                                                                              <div class=\'col-md-6 p-0\'>                                                                                    <div>' + (SelectedJob.CommunicationSkills ? SelectedJob.CommunicationSkills : '-') + '</div>                                                                              </div></div></div>                                                                      <div class=\'row\'>                                                                          <div class=\'col-md-12 p-0\'>                                                                              <br>                                                                              <div class=\'card disp-block\'>                                                                                  <div class=\'card-header f-w-500\'><i class=\'material-icons font-21 m-r-xs color-dark-gray\' aria-hidden=\'true\'>security </i>' + (SelectedJob.SecurityClearance ? SelectedJob.SecurityClearance : '-') + '</span></div>                                                                                </div>                                                                          </div>                                                                          </div>                                                                  </div><div id=\'five1' + SelectedJob.Id + '\' class=\'tab-pane \'  role=\'tabpanel\' aria-expanded=\'false\'><div class=\'row\'><div class=\'col-md-6\'><div class=\'card disp-block\'><div class=\'card-header f-w-500\'><i class=\'material-icons font-21 m-r-xs color-dark-gray\' aria-hidden=\'true\'>school </i>Degree</div><div class=\'card-block internal-div-height-overflow\'>';

            //----------------------------------------------------------------------
            //Bind Degrees
            //----------------------------------------------------------------------
            if (SelectedJob.DegreeNameList && SelectedJob.DegreeNameList.length > 0) {
                for (var m = 0; m < SelectedJob.DegreeNameList.length; m++) {
                    graphHtml += '<div>- ' + SelectedJob.DegreeNameList[m] + '</div>';
                }
            } else {
                graphHtml += '<div>- </div>';
            }
            //----------------------------------------------------------------------
            graphHtml += '</div></div></div><div class=\'col-md-6\'><div class=\'card disp-block\'><div class=\'card-header f-w-500\'><i class=\'material-icons font-21 m-r-xs color-dark-gray\' aria-hidden=\'true\'>assignment </i>Certification</div><div class=\'card-block internal-div-height-overflow\'><div>' + (SelectedJob.Qualification ? SelectedJob.Qualification : '-') + '</div></div></div></div></div></div>' +
                '                                            </div>' +
                '                                        </div>' +
                '                                    </div>' +
                '                                </div>' +
                '                            </div>' +
                '                        </td>' +
                '                        <td class=\'hidden\'></td>' +
                '                        <td class=\'hidden\'></td>' +
                '                        <td class=\'hidden\'></td>' +
                '                        <td class=\'hidden\'></td>' +
                '                        <td class=\'hidden\'></td>' +
                '                        <td class=\'hidden\'></td>' +
                '                        <td class=\'hidden\'></td>' +
                '                        <td class=\'hidden\'></td>' +
                '                    </tr>';

            //----------------------------------------------------------------------
            //Compile and bind HTML
            //----------------------------------------------------------------------
            var temp = $compile(graphHtml)($scope);
            $curRow.after(temp);

            $('#' + SelectedJob.Id).find('.fa-chevron-right').removeClass("fa-chevron-right").addClass("fa-chevron-down");
            //----------------------------------------------------------------------
          
           var _hs = {
            size: 5,
            partner: "https://xseed.com/"
          };
          (function () {
            var h = document.createElement('script'); h.type = 'text/javascript'; h.async = true;
            h.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'dtirydke3kdq7.cloudfront.net/hootlet.js?v=1';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(h, s);
          })();


          $scope.shareSocial = function () {
            var text = $scope.common.requirementModel;
            text = JSON.stringify(text);
            document.getElementById("reqDetails").value = text;
          }
        }
        //----------------------------------------------------------------------
        //Append PIE Chart
        //----------------------------------------------------------------------
        $('#' + SelectedJob.Id).parent('tr').next('.childtr').toggle("fast");

        if ($scope.appliedCandidateCount != 0 || $scope.associatedCandidateCount != 0) {

            var config = {

                type: 'doughnut',
                radius: "20%",
                innerRadius: "20%",
                data: {
                    datasets: [{
                        data: [
                            $scope.common.selectedJob.AppliedCandidateCount,
                            $scope.associatedCandidateCount
                        ],
                        backgroundColor: [
                            window.chartColors.blue,
                            window.chartColors.purple
                        ],
                        label: 'Dataset 1'
                    }],
                    labels: [
                        'Applied',
                        'Submitted'
                    ]
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                        display: false
                    },
                    title: {
                        display: true,
                        text: 'Candidates'
                    },

                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            };
            //----------------------------------------------------------------------
            // BIND THE CHART WITH CANVAS ELEMENT
            //----------------------------------------------------------------------
            var mycancvas = $('#' + SelectedJob.Id).parent('tr').next('.childtr').find('td.chart-td').find('#chartarea' + SelectedJob.Id);
            var ctx = mycancvas[0].getContext('2d');
            window.myDoughnut = new Chart(ctx, config);
            var colorNames = Object.keys(window.chartColors);
            //----------------------------------------------------------------------

        }
    }
    

    $scope.requirementShareModal = function () {
        requirementShareModal.$promise.then(requirementShareModal.show);
        setTimeout(function () {
            var USexp = $scope.common.selectedJob.USExperienceInYear;
            if (USexp == null) {
                USexp = "NA";
            }
            var TotalExp = $scope.common.selectedJob.ExperienceInYear;
            if (TotalExp == null) {
                TotalExp = "NA";
            }
            var Skills = $scope.common.selectedJob.TechnicalSkills;
            if (Skills == null) {
                Skills = "NA";
            }
            var NoPositions = $scope.common.selectedJob.TotalPositions;
            if (NoPositions == null) {
                NoPositions = "NA";
            }
            var CompanyName = $scope.common.selectedJob.CompanyName;
            if (CompanyName == null) {
                CompanyName = "NA";
            }
            var text = "Job Title: " + $scope.common.selectedJob.JobTitle
              + "\nJob Type: " + $scope.common.selectedJob.JobType
              + "\nNumber of positions: " + NoPositions
              + "\nSkills: " + Skills
              + "\nTotal Experience: " + TotalExp + " Years"
              + "\nUS Experience: " + USexp + " Years"
              + "\nWork Location: " + $scope.common.selectedJob.Location
              + "\nCompany: " + CompanyName;
            document.getElementById("reqDetails").value = text;
            var jobid = $scope.common.selectedJob.Id;
            var orgid = $rootScope.userDetails.OrganizationId;
            var shareUrl = "http://107.180.78.163:8090/index.html?jobId=" + jobid + "--" + orgid;
            document.getElementById("socialLink").href = shareUrl;
            (function () {
                var h = document.createElement('script'); h.type = 'text/javascript'; h.async = true;
                h.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'dtirydke3kdq7.cloudfront.net/hootlet.js?v=1';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(h, s);
            })();
        }, 500);
    };

    $scope.requirementFacebookModal = function () {
        var copyText = document.getElementById("reqDetails");
        copyText.select();
        requirementShareModal.$promise.then(requirementShareModal.show);
        setTimeout(function () {
            var USexp = $scope.common.selectedJob.USExperienceInYear;
            if (USexp == null) {
                USexp = "NA";
            }
            var TotalExp = $scope.common.selectedJob.ExperienceInYear;
            if (TotalExp == null) {
                TotalExp = "NA";
            }
            var Skills = $scope.common.selectedJob.TechnicalSkills;
            if (Skills == null) {
                Skills = "NA";
            }
            var NoPositions = $scope.common.selectedJob.TotalPositions;
            if (NoPositions == null) {
                NoPositions = "NA";
            }
            var CompanyName = $scope.common.selectedJob.CompanyName;
            if (CompanyName == null) {
                CompanyName = "NA";
            }
            // var text = copyText.value;
            var text = "Job Title: " + $scope.common.selectedJob.JobTitle
              + "\nJob Type: " + $scope.common.selectedJob.JobType
              + "\nNumber of positions: " + NoPositions
              + "\nSkills: " + Skills
              + "\nTotal Experience: " + TotalExp + " Years"
              + "\nUS Experience: " + USexp + " Years"
              + "\nWork Location: " + $scope.common.selectedJob.Location
              + "\nCompany: " + CompanyName;
            //alert(text);
            document.getElementById("reqDetails").value = copyText.value;
            var jobid = $scope.common.selectedJob.Id;
            var orgid = $rootScope.userDetails.OrganizationId;
            var shareUrl = "https://www.facebook.com/dialog/feed?app_id=1113210682534228&display=popup&description=" + text + "&link=http://xseed.com/index.html?jobId=" + jobid + "--" + orgid + "&redirect_uri=http://xseed.com/index.html?jobId=" + jobid + "--" + orgid + "&quote=" + encodeURIComponent(text);
            window.open(shareUrl, "_blank", "top=500,left=500,width=400,height=400");
        }, 500);
    };

    $scope.requirementWhatsAppModal = function () {
        var copyText = document.getElementById("reqDetails");
        copyText.select();
        copyText.setSelectionRange(0, 99999)
        document.execCommand("copy");
        requirementShareModal.$promise.then(requirementShareModal.show);
        setTimeout(function () {
            var USexp = $scope.common.selectedJob.USExperienceInYear;
            if (USexp == null) {
                USexp = "NA";
            }
            var TotalExp = $scope.common.selectedJob.ExperienceInYear;
            if (TotalExp == null) {
                TotalExp = "NA";
            }
            var Skills = $scope.common.selectedJob.TechnicalSkills;
            if (Skills == null) {
                Skills = "NA";
            }
            var NoPositions = $scope.common.selectedJob.TotalPositions;
            if (NoPositions == null) {
                NoPositions = "NA";
            }
            var CompanyName = $scope.common.selectedJob.CompanyName;
            if (CompanyName == null) {
                CompanyName = "NA";
            }

            //alert(pastedData);
            //var text = copyText.value;
            var text = "Job Title: " + $scope.common.selectedJob.JobTitle
              + "\nJob Type: " + $scope.common.selectedJob.JobType
              + "\nNumber of positions: " + NoPositions
              + "\nSkills: " + Skills
              + "\nTotal Experience: " + TotalExp + " Years"
              + "\nUS Experience: " + USexp + " Years"
              + "\nWork Location: " + $scope.common.selectedJob.Location
              + "\nCompany: " + CompanyName;
            alert(encodeURIComponent(text));
            document.getElementById("reqDetails").value = copyText.value;
            var jobid = $scope.common.selectedJob.Id;
            var orgid = $rootScope.userDetails.OrganizationId;
            var shareUrl = "https://web.whatsapp.com/send?&text=" + encodeURIComponent(text) + "\n URL:" + "http://xseed.com/index.html?jobId=" + jobid + "--" + orgid;
            window.open(shareUrl, "_blank", "top=500,left=500,width=400,height=400");
        }, 500);
    };

    $scope.requirementLinkedinModal = function () {
        requirementShareModal.$promise.then(requirementShareModal.show);
        setTimeout(function () {
            var USexp = $scope.common.selectedJob.USExperienceInYear;
            if (USexp == null) {
                USexp = "NA";
            }
            var TotalExp = $scope.common.selectedJob.ExperienceInYear;
            if (TotalExp == null) {
                TotalExp = "NA";
            }
            var Skills = $scope.common.selectedJob.TechnicalSkills;
            if (Skills == null) {
                Skills = "NA";
            }
            var NoPositions = $scope.common.selectedJob.TotalPositions;
            if (NoPositions == null) {
                NoPositions = "NA";
            }
            var CompanyName = $scope.common.selectedJob.CompanyName;
            if (CompanyName == null) {
                CompanyName = "NA";
            }
            var text = "Job Title: " + $scope.common.selectedJob.JobTitle
              + "\nJob Type: " + $scope.common.selectedJob.JobType
              + "\nNumber of positions: " + NoPositions
              + "\nSkills: " + Skills
              + "\nTotal Experience: " + TotalExp + " Years"
              + "\nUS Experience: " + USexp + " Years"
              + "\nWork Location: " + $scope.common.selectedJob.Location
              + "\nCompany: " + CompanyName;
            document.getElementById("reqDetails").value = text;
            var jobid = $scope.common.selectedJob.Id;
            var orgid = $rootScope.userDetails.OrganizationId;
            var shareUrl = "http://www.linkedin.com/shareArticle?mini=true&url=http://xseed.com/index.html?jobId=" + jobid + "--" + orgid + "&summary=" + text + "&title=" + text + "&source=" + text;
            window.open(shareUrl, "_blank", "top=500,left=500,width=400,height=400");
        }, 500);
    };


  $scope.closerequirementShareModal = function () {
    requirementShareModal.$promise.then(requirementShareModal.hide);
  };


  $scope.requirementPostingModal = function () {
    requirementPostingModal.$promise.then(requirementPostingModal.show);
  };


  $scope.closerequirementPostingModal = function () {
    requirementPostingModal.$promise.then(requirementPostingModal.hide);
  };

  $scope.requirementShareModal1 = function (SelectedJob) {
    $scope.common.selectedRow = SelectedJob.Id;
    $scope.common.selectedJob = SelectedJob;
    $scope.requirementShareModal();
  };

  $scope.telegramSend = function () {

    var organizationId = $rootScope.userDetails.OrganizationId;
    $scope.common.requisitionId = {};
    var promise1 = XSeedApiFactory.getcollpasingJobDetail($scope.common.selectedJob.Id, organizationId);
    promise1.then(
      function (response) {
        $scope.common.requirementModel = response[0].data;
        console.log($scope.common.requirementModel);
        $scope.common.requisitionId = $scope.common.requirementModel.RequisitionId;
        $scope.common.requirementModel.OrganizationId = $rootScope.userDetails.OrganizationId;
        if ($scope.common.requirementModel.TechnicalSkills) {
          $scope.common.requirementModel.SkillList = [];
          $scope.common.requirementModel.SkillList = $scope.common.requirementModel.TechnicalSkills.split("|");
        }
        var promise = XSeedApiFactory.Telegram($scope.common.requirementModel);
        promise.then(
          function (response) {
            alert("Message send to telegram!!!!");
          },
          function (httpError) {
            alert("Error sending Message telegram!!!!");
            ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
          });
      },
      function (httpError) {
        blockUI.stop();
        ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
      });

  };
    $scope.requirementEditView = function() {
        blockUI.start();
        $scope.closeRequirementDetailModal(true);
        //$scope.getRequirementDetails();
        $scope.common.requirementModel.manipulationMode = "Edit";
        //$scope.jobLookupProcess();

        $scope.common.redirectedFrom = 'RequirementList';
        $location.path('editRequirement');

        blockUI.stop();
    }

    $scope.openRequirementFromOtherSection = function(requirementId) {
        $scope.common.selectedJob = {};
        $scope.common.selectedJob.Id = requirementId;
        $scope.getRequirementDetails();
        $scope.common.requirementDetailModal.$promise.then($scope.common.requirementDetailModal.show);
    };

    /*****************************************Display requirement details in modal***************************************/

    /*****************************************Display candidate details in modal***************************************/

    $scope.openCandidateDetailModal = function() {
        $scope.common.currentSection = $scope.constants.sectionCandidates;
        $scope.candidateDetailView();
        $scope.common.candidateDetailModal.$promise.then($scope.common.candidateDetailModal.show);
    };



    $scope.closeCandidateDetailModal = function(callFromEdit) {
        $scope.common.candidateDetailModal.$promise.then($scope.common.candidateDetailModal.hide);
        //$scope.common.advanceSearchModel = {};
        if (!callFromEdit) {
            $scope.common.candidateModel = {};
        }
    };

    $scope.openSingleCandidateDetailView = function(id) {
        $scope.common.selectedCandidate = {};
        $scope.common.selectedCandidate._id = id;
        $scope.common.candidateModel = {};
        $location.path('candidateDetail');
        $rootScope.isSingleDetail = true;
    }
    $scope.candidateDetailView = function() {

        if (!$scope.common.selectedCandidate._id) {
            $location.path('candidateList');
        }
        $scope.WorkExperience = [];
        var EmployementHistoryList = [];
        var EducationalList = [];
        blockUI.start();
        var promise = XSeedApiFactory.getCandidateDetail($scope.common.selectedCandidate._id);
        promise.then(
            function(response) {
                console.log(response[0].data);
                $scope.common.candidateModel = response[0].data;

                if ($scope.common.candidateModel.CountryId) {
                    $scope.getStateListByCountry($scope.common.candidateModel.CountryId);
                }

                if ($scope.common.candidateModel.StateId) {
                    $scope.getCityListByState($scope.common.candidateModel.StateId);
                }

                if ($scope.common.candidateModel.ProfileImageFile != null) {
                    $rootScope.userDetails.ProfileImageFile = $scope.common.candidateModel.ProfileImageFile.data;
                }
                $scope.common.candidateModel.BirthDate = $filter('date')($scope.common.candidateModel.BirthDate, "MM/dd/yyyy");
                $scope.common.candidateModel.PassportValidUpto = $filter('date')($scope.common.candidateModel.PassportValidUpto, "MM/dd/yyyy");
                $scope.common.candidateModel.VisaValidUpto = $filter('date')($scope.common.candidateModel.VisaValidUpto, "MM/dd/yyyy");
                if ($scope.common.candidateModel.ResumeList != null && $scope.common.candidateModel.ResumeList.length > 0) {
                    $scope.common.candidateModel.Resume = $scope.common.candidateModel.ResumeList[$scope.common.candidateModel.ResumeList.length - 1];
                    $scope.common.candidateModel.ResumeContent = $scope.common.candidateModel.ResumeList[$scope.common.candidateModel.ResumeList.length - 1].data.includes(",") ? $scope.common.candidateModel.ResumeList[$scope.common.candidateModel.ResumeList.length - 1].data : ("data:;base64," + $scope.common.candidateModel.ResumeList[$scope.common.candidateModel.ResumeList.length - 1].data);
                }

                if ($scope.common.candidateModel.Educations) {
                    for (var i = 0; i < $scope.common.candidateModel.Educations.length; i++) {
                        if ($scope.common.candidateModel.Educations[i].CountryId) {
                            $scope.setStateListByCountry($scope.common.candidateModel.Educations[i]);
                        }

                        if ($scope.common.candidateModel.Educations[i].StateId) {
                            $scope.setCityListByState($scope.common.candidateModel.Educations[i]);
                        }
                        EducationalList.push($scope.common.candidateModel.Educations[i]);
                    }
                    $scope.common.candidateModel.Educations = EducationalList;
                }

                angular.forEach($scope.common.candidateModel.visaTypeList, function(val, index) {
                    val.ValidUpto = $filter('date')(val.ValidUpto, "MM/dd/yyyy");
                })

                if ($scope.common.candidateModel.EmployementHistory) {
                    for (var i = 0; i < $scope.common.candidateModel.EmployementHistory.length; i++) {
                        if ($scope.common.candidateModel.EmployementHistory[i].CountryId) {
                            $scope.setStateListByCountry($scope.common.candidateModel.EmployementHistory[i]);
                        }

                        if ($scope.common.candidateModel.EmployementHistory[i].StateId) {
                            $scope.setCityListByState($scope.common.candidateModel.EmployementHistory[i]);
                        }
                        EmployementHistoryList.push($scope.common.candidateModel.EmployementHistory[i]);
                    }
                    $scope.common.candidateModel.EmployementHistory = EmployementHistoryList;
                }

                //$scope.candidateId = candidateId;

                //EventBus.switchView = "candidateDetail";
                //$location.path("switchView").replace();

                setTimeout(function() {
                    var $input = $('input.rating');
                    if ($input.length) {
                        $input.removeClass('rating-loading').addClass('rating-loading').rating();
                    }
                }, 0);
                blockUI.stop();


            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Candidate', httpError.data.Status, httpError.data.Message);
            });
    };

    $scope.candidateDetailViewBySource = function(candidate) {
        if (candidate.Source == 'Local') {
            $scope.openCandidateFromOtherSection(candidate.Candidate_Id);
        }
        if (candidate.HasLocalCopy) {
            XSeedAlert.swal({
                title: "Are you sure?",
                text: "Local copy for this candidate is available, do you want to view updated profile?",
                type: "warning",
                showCancelButton: true,
                showCloseButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "View Updated",
                cancelButtonText: "View Local",
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function() {
                    downloadCandidateBySource(candidate);
                },
                //function (dismiss) {
                //    if (candidate.Candidate_Id) {
                //        //$scope.openCandidateFromOtherSection(candidate.Candidate_Id);
                //    }
                //});

                function(dismiss) {
                    if (dismiss == 'cancel') {
                        // function when cancel button is clicked
                        if (candidate.Candidate_Id) {
                            $scope.openCandidateFromOtherSection(candidate.Candidate_Id);
                        }
                    } else if (dismiss == 'close') {
                        // function when close button is clicked
                    }
                });
        } else {
            downloadCandidateBySource(candidate);
        }
    };

    function downloadCandidateBySource(candidate) {
        if (candidate.Source == 'Monster') {
            blockUI.start();
            var promise = XSeedApiFactory.monsterCandidateDetailView(candidate.ProviderCandidateId, candidate.Candidate_Id);
            promise.then(
                function(response) {
                    $scope.common.candidateModel = response[0].data;
                    angular.forEach($scope.common.candidateListBySource, function(val, index) {
                        if (val.ProviderCandidateId == candidate.ProviderCandidateId) {
                            val.Candidate_Id = response[0].data._id;
                            val.HasLocalCopy = true;
                        }
                    });
                    $scope.common.tblCandidateListBySource.reload();

                    $scope.common.candidateDetailModal.$promise.then($scope.common.candidateDetailModal.show);
                    blockUI.stop();
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Candidate', httpError.data.Status, httpError.data.Message);
                });
        } else if (candidate.Source == 'Dice') {
            blockUI.start();
            var promise = XSeedApiFactory.diceCandidateDetailView(candidate.ProviderCandidateId, candidate.Candidate_Id);
            promise.then(
                function(response) {
                    $scope.common.candidateModel = response[0].data;
                    angular.forEach($scope.common.candidateListBySource, function(val, index) {
                        if (val.ProviderCandidateId == candidate.ProviderCandidateId) {
                            val.Candidate_Id = response[0].data._id;
                            val.HasLocalCopy = true;
                        }
                    });
                    $scope.common.tblCandidateListBySource.reload();

                    $scope.common.candidateDetailModal.$promise.then($scope.common.candidateDetailModal.show);
                    blockUI.stop();
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Candidate', httpError.data.Status, httpError.data.Message);
                });
        }
    };

    $scope.candidateDirectEditView = function(candidate) {
        $scope.common.selectedRowCandidate = candidate._id;
        $scope.common.selectedCandidate = candidate;

        blockUI.start();
        $scope.common.resumePresent = false;
        $scope.common.invalid = false;
        $scope.common.maxSize = false;
        $scope.common.invalidResume = false;
        $scope.common.maxResumeSize = false;
        $scope.closeCandidateDetailModal(true);
        $scope.common.displayValidationSummary = false;
        $scope.common.maxProfileImgFileSize = false;
        $scope.common.invalidProfileImgFile = false;
        $scope.common.candidateId = {};
        var promise = XSeedApiFactory.getCandidateDetail($scope.common.selectedCandidate._id);
        promise.then(
            function(response) {
                $scope.common.candidateModel = response[0].data;

                $scope.common.candidateId = $scope.common.candidateModel.CandidateId;
                if ($scope.common.candidateModel.CountryId) {
                    $scope.getStateListByCountry($scope.common.candidateModel.CountryId);
                }

                if ($scope.common.candidateModel.StateId) {
                    $scope.getCityListByState($scope.common.candidateModel.StateId);
                }

                if ($scope.common.candidateModel.ProfileImageFile != null) {
                    $scope.common.candidateModel.ProfileImageData = $scope.common.candidateModel.ProfileImageFile.data;
                }
                $scope.common.candidateModel.BirthDate = $filter('date')($scope.common.candidateModel.BirthDate, "MM/dd/yyyy");
                $scope.common.candidateModel.PassportValidUpto = $filter('date')($scope.common.candidateModel.PassportValidUpto, "MM/dd/yyyy");
                $scope.common.candidateModel.VisaValidUpto = $filter('date')($scope.common.candidateModel.VisaValidUpto, "MM/dd/yyyy");
                if ($scope.common.candidateModel.ResumeList) {
                    $scope.common.candidateModel.Resume = $scope.common.candidateModel.ResumeList[$scope.common.candidateModel.ResumeList.length - 1];
                    $scope.common.candidateModel.ResumeContent = $scope.common.candidateModel.ResumeList[$scope.common.candidateModel.ResumeList.length - 1].data.indexOf(",") > 1 ? $scope.common.candidateModel.ResumeList[$scope.common.candidateModel.ResumeList.length - 1].data : ("data:;base64," + $scope.common.candidateModel.ResumeList[$scope.common.candidateModel.ResumeList.length - 1].data);
                }

                if ($scope.common.candidateModel.Educations) {
                    for (var i = 0; i < $scope.common.candidateModel.Educations.length; i++) {
                        if ($scope.common.candidateModel.Educations[i].CountryId) {
                            $scope.setStateListByCountry($scope.common.candidateModel.Educations[i]);
                        }

                        if ($scope.common.candidateModel.Educations[i].StateId) {
                            $scope.setCityListByState($scope.common.candidateModel.Educations[i]);
                        }
                    }
                }

                angular.forEach($scope.common.candidateModel.VisaTypeList, function(val, index) {
                    val.ValidUpto = $filter('date')(val.ValidUpto, "MM/dd/yyyy");
                })

                if ($scope.common.candidateModel.EmployementHistory) {
                    for (var i = 0; i < $scope.common.candidateModel.EmployementHistory.length; i++) {
                        if ($scope.common.candidateModel.EmployementHistory[i].CountryId) {
                            $scope.setStateListByCountry($scope.common.candidateModel.EmployementHistory[i]);
                        }

                        if ($scope.common.candidateModel.EmployementHistory[i].StateId) {
                            $scope.setCityListByState($scope.common.candidateModel.EmployementHistory[i]);
                        }
                    }
                }

                if ($scope.common.candidateModel.VisaType == null) {
                    $scope.common.candidateModel.VisaType = "NA"
                }

                if ($scope.common.candidateModel.Skills) {
                    //$scope.common.candidateModel.Skills = [];

                    //$scope.common.candidateModel.Skills = $scope.common.candidateModel.Skills.split("|");
                    //$scope.lookup.skillListData = [];
                    angular.forEach($scope.common.candidateModel.Skills, function(val, index) {
                        $scope.lookup.skillListData.push({ value: val, label: val });
                    });
                }

                //$scope.candidateId = candidateId;
                $scope.common.candidateModel.manipulationMode = "Edit";
                $location.path('editCandidate');
                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Candidate', httpError.data.Status, httpError.data.Message);
            });
    };

    $scope.candidateEditView = function() {

        $scope.closeCandidateDetailModal(true);

        blockUI.start();
        if ($scope.common.candidateModel.VisaType == null) {
            $scope.common.candidateModel.VisaType = "NA"
        }
        $scope.common.resumePresent = false;
        $scope.common.invalid = false;
        $scope.common.maxSize = false;
        $scope.common.invalidResume = false;
        $scope.common.displayValidationSummary = false;
        $scope.common.maxResumeSize = false;
        $scope.common.candidateModel.manipulationMode = "Edit";
        blockUI.stop();

        $location.path("editCandidate");
    }

    $scope.openCandidateFromOtherSection = function(candidateId) {
        $scope.common.selectedCandidate = {};
        $scope.common.currentSection = undefined;
        $scope.common.selectedCandidate._id = candidateId;
        $scope.candidateDetailView();
        // $scope.common.candidateDetailModal.$promise.then($scope.common.candidateDetailModal.show);
    };

    /*****************************************Display candidate details in modal***************************************/

    /*****************************************Display candidate Resume in modal***************************************/

    $scope.openResumePreviewModal = function(candidateId) {

        var promise = XSeedApiFactory.viewCandidateResume(candidateId);
        promise.then(
            function(response) {
                blockUI.start();
                $scope.resumeLink = response[0].data;
                if ($scope.resumeLink) {
                    $scope.resumeLink = configuration.XSEED_RESUME_PATH + $scope.resumeLink;
                    //$scope.common.resumePreviewModal.$promise.then($scope.common.resumePreviewModal.show);
                    $window.open($scope.resumeLink.toString());
                }
                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Candidate', httpError.data.Status, httpError.data.Message);
            });
    };

    /*****************************************Display candidate Resume in modal***************************************/

    // Multiple Controller Call End-


    /***************************************** User access check call***************************************/

    $scope.checkUserAccess = function() {

        if (angular.isUndefined(localStorageService.get("userDetails")) || localStorageService.get("userDetails") == null) {
            $rootScope.userHasToken = false;
            $location.path('login');

            return false;
        } else {
            $rootScope.userHasToken = true;
        }
        var createRquirementURL = '/createRequirement';
        var editRequirementURL = '/editRequirement';
        var requirementListURL = '/requirementList';
        var requirementDetailURL = '/requirementDetail';

        var createCandidateURL = '/createCandidate';
        var editCandidateURL = '/editCandidate';
        var candidateListURL = '/candidateList';
        var candidateDetailURL = '/candidateDetail';

        var createCompanyURL = '/createCompany';
        var editCompanyURL = '/editCompany';
        var companyListURL = '/companyList';
        var companyDetailURL = '/companyDetail';
        var createContactURL = '/createContact';
        var editContactURL = '/editContact';

        var organizationProfileURL = '/organizationProfile';
        var editOrganizationURL = '/editOrganization';
        var userRoleURL = '/userRoles';

        var submissionsURL = '/submissions';

        var reportsURL = '/reports';

        var vendorEmailListURL = '/vendorEmailList';
        var vendorListURL = '/vendorList';
        var createVendorURL = '/createVendor';
        var editVendorURL = '/editVendor';


        /* -------------------------- Requirement -------------*/
        if ($location.path().toLowerCase() == createRquirementURL.toLowerCase() && $rootScope.userDetails.Requirement_Create == "False") {
            $location.path('dashboard');
        }
        if ($location.path().toLowerCase() == editRequirementURL.toLowerCase() && $rootScope.userDetails.Requirement_Update == "False") {
            $location.path('dashboard');
        }

        if ($location.path().toLowerCase() == requirementListURL.toLowerCase() && $rootScope.userDetails.Requirement_Read == "False") {
            $location.path('dashboard');
        }
        if ($location.path().toLowerCase() == requirementDetailURL.toLowerCase() && $rootScope.userDetails.Requirement_Read == "False") {
            $location.path('dashboard');
        }
        /* -------------------------- Requirement -------------*/


        /* -------------------------- Candidate -------------*/
        if ($location.path().toLowerCase() == createCandidateURL.toLowerCase() && $rootScope.userDetails.Candidate_Create == "False") {
            $location.path('dashboard');
        }

        if ($location.path().toLowerCase() == editCandidateURL.toLowerCase() && $rootScope.userDetails.Candidate_Update == "False") {
            $location.path('dashboard');
        }

        /* Edit Candidate Page -> on page refresh, all data gets disappeared. */
        if ($location.path().toLowerCase() == editCandidateURL.toLowerCase() && ($scope.common.candidateModel != null && $scope.common.candidateModel.FirstName == "")) {
            $location.path('candidateList');
        }

        if ($location.path().toLowerCase() == candidateListURL.toLowerCase() && $rootScope.userDetails.Candidate_Read == "False") {
            $location.path('dashboard');
        }

        if ($location.path().toLowerCase() == candidateDetailURL.toLowerCase() && $rootScope.userDetails.Candidate_Read == "False") {
            $location.path('dashboard');
        }
        /* -------------------------- Candidate -------------*/


        /* -------------------------- Company -------------*/
        if ($location.path().toLowerCase() == createCompanyURL.toLowerCase() && $rootScope.userDetails.Company_Create == "False") {
            $location.path('dashboard');
        }

        if ($location.path().toLowerCase() == editCompanyURL.toLowerCase() && $rootScope.userDetails.Company_Update == "False") {
            $location.path('dashboard');
        }

        if ($location.path().toLowerCase() == companyListURL.toLowerCase() && $rootScope.userDetails.Company_Read == "False") {
            $location.path('dashboard');
        }
        if ($location.path().toLowerCase() == companyDetailURL.toLowerCase() && $rootScope.userDetails.Company_Read == "False") {
            $location.path('dashboard');
        }
        if ($location.path().toLowerCase() == createContactURL.toLowerCase() && $rootScope.userDetails.Company_Create == "False") {
            $location.path('dashboard');
        }
        if ($location.path().toLowerCase() == editContactURL.toLowerCase() && $rootScope.userDetails.Company_Update == "False") {
            $location.path('dashboard');
        }
        /* -------------------------- Company -------------*/

        /* -------------------------- Organization -------------*/

        if ($location.path().toLowerCase() == organizationProfileURL.toLowerCase() && $rootScope.userDetails.Organization_Read == "False") {
            $location.path('dashboard');
        }
        if ($location.path().toLowerCase() == editOrganizationURL.toLowerCase() && $rootScope.userDetails.Organization_Update == "False") {
            $location.path('dashboard');
        }

        if ($location.path().toLowerCase() == userRoleURL.toLowerCase() && $rootScope.userDetails.RoleName != 'Application Administrator') {
            $location.path('dashboard');
        }
        /* -------------------------- Organization -------------*/

        /* -------------------------- Submission -------------*/
        if ($location.path().toLowerCase() == submissionsURL.toLowerCase() && $rootScope.userDetails.Submission_Read == "False") {
            $location.path('dashboard');
        }
        /* -------------------------- Submission -------------*/


        /* -------------------------- Reports -------------*/
        if ($location.path().toLowerCase() == reportsURL.toLowerCase() && $rootScope.userDetails.Reports_Read == "False") {
            $location.path('dashboard');
        }
        /* -------------------------- Reports -------------*/

        /* -------------------------- Vendor -------------*/
        if ($location.path().toLowerCase() == vendorEmailListURL.toLowerCase() && $rootScope.userDetails.Vendor_Read == "False") {
            $location.path('dashboard');
        }

        if ($location.path().toLowerCase() == vendorListURL.toLowerCase() && $rootScope.userDetails.Vendor_Read == "False") {
            $location.path('dashboard');
        }

        if ($location.path().toLowerCase() == createVendorURL.toLowerCase() && $rootScope.userDetails.Vendor_Create == "False") {
            $location.path('dashboard');
        }
        if ($location.path().toLowerCase() == editVendorURL.toLowerCase() && $rootScope.userDetails.Vendor_Update == "False") {
            $location.path('dashboard');
        }
        /* -------------------------- Vendor -------------*/

    };







    /***************************************** User access check call***************************************/

    /* Common call start */
    $scope.getStateListByCountry = function(countryId) {
        var promise = XSeedApiFactory.getStateListByCountry(countryId);
        promise.then(
            function(response) {
                $scope.stateList = response[0].data;
                $scope.cityList = response[0].data;
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
            });
    };

    $scope.setStateListByCountry = function(object) {
        var promise = XSeedApiFactory.getStateListByCountry(object.CountryId);
        promise.then(
            function(response) {
                object.stateList = response[0].data;

            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
            });
    };


    $scope.getCityListByState = function(stateId) {
        if (stateId != null || stateId != undefined) {
            var promise = XSeedApiFactory.getCityListByState(stateId);
            promise.then(
                function(response) {
                    $scope.cityList = response[0].data;
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
                });
        }
    };

    $scope.setCityListByState = function(object) {
        var promise = XSeedApiFactory.getCityListByState(object.StateId);
        promise.then(
            function(response) {
                object.cityList = response[0].data;
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
            });
    };

    $scope.getCompanyContactListByCompany = function(companyId) {
        var promise = XSeedApiFactory.getCompanyContacts(companyId);
        promise.then(
            function(response) {
                $scope.companyContactLookupList = response[0].data;
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('CompanyContact', httpError.data.Status, httpError.data.Message);
            });
    };




    $scope.getCountryNameById = function(countryId) {
        var countryList = $rootScope.metadata.GetAncillaryInformationResponse.countryList.country;
        if (!countryId)
            return countryId;
        else {
            var selectedCountry = _.filter(countryList, function(country) {
                return country.Id === countryId;
            });

            return selectedCountry[0] == undefined ? "" : selectedCountry[0].Name;
        }
    }

    $scope.getCountryIdByName = function(countryName) {
        var countryList = $rootScope.metadata.GetAncillaryInformationResponse.countryList.country;
        if (!countryName)
            return countryName;
        else {
            var selectedCountry = _.filter(countryList, function(country) {
                return angular.lowercase(country.Name) === angular.lowercase(countryName);
            });

            return selectedCountry[0] == undefined ? "" : selectedCountry[0].Id;
        }
    }

    $scope.getLookUpSkill = function() {
        var promise = XSeedApiFactory.getLookUpSkill();
        promise.then(
            function(response) {
                $scope.lookup.skill = response[0].data;
                $scope.buildSkillStructuredData();
            },
            function(httpError) {
                ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
            });
    };

    $scope.buildSkillStructuredData = function() {
        var arrSkill = [];
        var res = $scope.lookup.skill;
        angular.forEach(res, function(val, index) {
            arrSkill.push({ value: val.Name, label: val.Name });
        });

        blockUI.stop();
        $scope.lookup.skillListData = arrSkill;
        $scope.skills = arrSkill;
    }


    $scope.buildIndustryStructuredData = function() {
        var bindArray = [];
        var bindRecord = {};
        bindRecord.children = [];

        for (var i = 0; i < $scope.metadata.GetAncillaryInformationResponse.industryTypeList.industryType.length; i++) {
            var record = $scope.metadata.GetAncillaryInformationResponse.industryTypeList.industryType[i];

            if (record.ParentId === null) {
                if (bindRecord.children.length > 0) {
                    bindArray.push(bindRecord);
                    bindRecord = {};
                    bindRecord.children = [];
                }

                bindRecord.text = record.Name;
            } else {
                var child = {};
                child.id = record.Id;
                child.text = record.Name;

                bindRecord.children.push(child);
            }
        };

        $scope.industryStructuredData = bindArray;
    }


    $scope.isUndefinedOrNull = function(val) {
        return angular.isUndefined(val) || val === null
    }


    $scope.isEmpty = function(obj) {
        if (obj.length === 0) {
            return true;
        } else {
            return false;
        }
    }

    $scope.goToCandidateList = function() {
        $location.path("candidateList");
    };

    //Conditional Lookup
    $scope.getCompanyContactsLookup = function(companyId) {
        var promise = XSeedApiFactory.getCompanyContactsLookup(companyId);
        promise.then(
            function(response) {
                $scope.companyContactList = response[0].data;
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
            });
    };
    /* Common call end */

    //*************************************************************************************


    $scope.onBlurValidFunc = function(val) {
        val.$onblurInvalid = val.$invalid;
    };

    $scope.onFocusFunc = function(val) {
        val.$onblurInvalid = false;
    };

    $scope.sessionTimeout = function(location) {

        $window.location.href = location;
    };


    function isRouteRedirect(route) {
        return (!route.current.action);
    };

    // Fires after Exception handler call
    $scope.$on('handleError', function() {
        var exceptionMessage = ExceptionHandler.ExceptionMessage;
        $scope.locationPath = exceptionMessage.location;
        $scope.ErrorMessage = exceptionMessage.message;
        console.log($scope.ErrorMessage);
        if ($scope.ErrorMessage == "Authorization has been denied for this request.") {

                 // $scope.sessionTimeout("sessionTimeout.html");
            return;
        }

        showErrorModal();
    });

    // Fires after Exception handler call
    $scope.$on('handlePopup', function() {
        $scope.popupTitle = ExceptionHandler.ExceptionMessage.title;
        $scope.ErrorMessage = ExceptionHandler.ExceptionMessage.message;
        $scope.popupType = ExceptionHandler.ExceptionMessage.type;
        showPopup();
    });

    // Fires after Exception handler call
    $scope.$on('handleAccountException', function() {
        var exceptionMessage = ExceptionHandler.ExceptionMessage;
        alert('3')
        $scope.sessionTimeout("sessionTimeout.html");
        // $scope.sessionTimeout(exceptionMessage.location);
    });


    function showPopup() {
        //errorModal.$promise.then(errorModal.show);
        XSeedAlert.swal({
            title: $scope.popupTitle,
            text: $scope.ErrorMessage,
            type: $scope.popupType,
            customClass: 'xseed-error-alert',
            allowOutsideClick: false,
            allowEscapeKey: false
        });
    };

    function showErrorModal() {
        //errorModal.$promise.then(errorModal.show);
        XSeedAlert.swal({
            title: i18nFactory.ERROR_TITLE,
            text: $scope.ErrorMessage,
            type: 'error',
            customClass: 'xseed-error-alert',
            allowOutsideClick: false,
            allowEscapeKey: false
        });
    };


    $scope.showNoty = function(msg) {
        var type = 'success';
        var position = 'topRight';
        noty({
            theme: 'app-noty',
            text: msg,
            type: type,
            timeout: 3000,
            layout: position,
            closeWith: ['button', 'click'],
            animation: {
                open: 'animated fadeInDown', // Animate.css class names
                close: 'animated fadeOutUp', // Animate.css class names
            }
        });
    };


    $scope.hideErrorModal = function() {
        $location.$$search = {};
    };



    $scope.UserIdlestarted = false;
    var timeoutWarningModal = ModalFactory.timeoutWarningModal($scope);
    var timeoutModal = ModalFactory.timeoutModal($scope);

    function closeTimeoutModals() {
        timeoutWarningModal.$promise.then(timeoutWarningModal.hide);
        timeoutModal.$promise.then(timeoutModal.hide);
    };

    $scope.$on('IdleStart', function() {
        closeTimeoutModals();
        timeoutWarningModal.$promise.then(timeoutWarningModal.show);
    });

    $scope.$on('IdleEnd', function() {
        closeTimeoutModals();
        $log.warn('Idle time ending');
    });

    $scope.$on('IdleTimeout', function() {
        closeTimeoutModals();
        ModalFactory.hideAllOpenedModals();
        $scope.sessionTimeout("sessionTimeout.html");
        // timeoutModal.$promise.then(timeoutModal.show);
    });

    //---------------------------------------------------------------------------//

    // Get the render context local to this controller (and relevant params).

    var renderContext = RequestContext.getRenderContext();

    // The subview indicates which view is going to be rendered on the page.

    $scope.subview = renderContext.getNextSection();

    $scope.$on("requestContextChanged",

        function() {
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {


                return;
            }
            // Update the view that is being rendered.
            $scope.subview = renderContext.getNextSection();
        }
    );

    // Get State name from State Id
    $scope.stateValuebyId = function (Id) {
        angular.forEach($scope.stateList, function(val, index) {
            if (val.Id == Id) {
                $rootScope.organizationDetails.State = val.Name;
            }
        });
    };

    // Get City name from City Id
    $scope.cityValuebyId = function (Id) {
        angular.forEach($scope.cityList, function(val, index) {
            if (val.Id == Id) {
                $rootScope.organizationDetails.City = val.Name;
            }
        });
    };

    /****************************** Identify Browser Code - Rohit **********************************************/
    $scope.BrowserDetection = function() {

            $rootScope.currentBrowserName = null;

            if (navigator.userAgent.search("MSIE") >= 0) {
                $rootScope.currentBrowserName = "internet";

            } else if (navigator.userAgent.search("Chrome") >= 0) {
                $rootScope.currentBrowserName = "chrome";

            } else if (navigator.userAgent.search("Firefox") >= 0) {
                $rootScope.currentBrowserName = "firefox";

            } else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
                $rootScope.currentBrowserName = "safari";

            } else if (navigator.userAgent.search("Opera") >= 0) {
                $rootScope.currentBrowserName = "opera";

            } else {
                $rootScope.currentBrowserName = "Unable To Detect Browser";
            }
        }
        /****************************** Identify Browser **********************************************/


    // Listen for route changes so that we can trigger request-context change events.
    $scope.$on("$routeChangeSuccess", function(event) {
        // If this is a redirect directive, then there's no taction to be taken.

        if (isRouteRedirect($route)) {
            return;
        }

        $scope.currentLocation = $route.current.action;

        $scope.pageTitle = $route.current.pageTitle;
        // Update the current request action change.

        RequestContext.setContext($route.current.action, $routeParams);

        // Announce the change in render conditions.
        $scope.$broadcast("requestContextChanged", RequestContext);

    });



    //..................Edit quicknote start................................

    $scope.StartEditQNotes = function(note, position) {

        $('#summernote').summernote('code', note.Description);
        $scope.common.qNoteList.EditID = note.Id;
        $scope.common.qNoteList.editposition = position;
        $scope.common.qNoteList.EditMode = true;

        $('.modal-body').animate({
            scrollTop: $("#summernote").offset().top
        }, 1000)
    }

    $scope.CancelUpdateQNoteCandidate = function() {
        $scope.common.qNoteList.EditID = null;
        $('#summernote').summernote('code', '');
        $scope.common.qNoteList.EditMode = false;
    }


    //..................Edit quicknote ends..................................


    //---------------------------------------------------------------------------//

    $rootScope.$on("$includeContentLoaded", function(event, templateName) {

        $('[data-toggle="popover"]').popover();
        $('[data-toggle="tooltip"]').tooltip({
            trigger: 'hover'
        })
        $("select.select2").select2({ allowClear: true });
        $(".dropdown-toggle").dropdown();

        $(".nav-tabs .nav-item  a").click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });
    });


    $scope.ChangeAppClass = function() {

        $('.app').removeClass('expanding fixed-header');
        $('.app').addClass('no-padding no-footer layout-static');

    }

    $scope.InializePopoverAndTooltip = function() {

        $('[data-toggle="popover"]').popover();
        $('[data-toggle="tooltip"]').tooltip({
            trigger: 'hover'
        })
        $('.dropdown-toggle').dropdown();
    }


    $scope.CallSelect2 = function() {
        setTimeout(function() {
            $("select.select2").select2({ allowClear: true });
        }, 50);

    }

    $scope.InitDragScroll = function() {
        StartDragScroll();
    }



  $scope.step_nextBtn = function(stepcount, FormS, formtype) {
        if (formtype == 'candidate') {

            if (new ValidationService().checkFormValidity(FormS) &&
                $scope.common.candidateModel.Resume &&
                $scope.common.candidateModel.Resume.name &&
                !$scope.common.invalidProfileImgFile &&
                !$scope.common.maxProfileImgFileSize) {

                if ($("#ProfileSummary").is(":visible") && $("#ProfileSummary").val().length > 2000) {
                    $("#ProfileSummaryError").show();
                    return;
                }

                GoToNextStep();
            } else {
                if ($scope.common.candidateModel.Resume === undefined) {
                    $scope.common.resumePresent = true;
                  $scope.common.candidateModel.Resume = {};
                  var temp = 0;
                  if ($("#CountryId").is(":visible") && $("#CountryId").val() == "") {

                    $("#CountryIdError").show();
                    temp = 1;
                  }
                  if ($("#StateId").is(":visible") && $("#StateId").val() == "") {
                    $("#StateIdError").show();
                    temp = 1;
                  }
                }
                $scope.common.displayValidationSummary = true;
            }
        } else if (formtype == 'requirement') {
            if (new ValidationService().checkFormValidity(FormS)) {
                var temp = 0;
                if ($("#location").is(":visible") && $("#location").val() == "") {
                    $("#locationError").show();
                    temp = 1;
                }
                if ($("#duration").is(":visible") && $("#duration").val() == "") {
                    $("#durationError").show();
                    temp = 1;
                }
                if ($("#CountryId").is(":visible") && $("#CountryId").val() == "") {

                    $("#CountryIdError").show();
                    temp = 1;
                }
                if ($("#StateId").is(":visible") && $("#StateId").val() == "") {
                    $("#StateIdError").show();
                    temp = 1;
                }
                if (temp == 1) {
                    return;
                }
                GoToNextStep();
            }
        } else if (formtype == 'company') {
            if (new ValidationService().checkFormValidity(FormS) && !$scope.common.maxProfileImgFileSize && !$scope.common.invalidProfileImgFile && !$scope.common.minProfileImgFileSize) {

                GoToNextStep();
            }

        } else if (formtype == 'newuser') {
            if (new ValidationService().checkFormValidity(FormS) && !$scope.common.maxProfileImgFileSize && !$scope.common.invalidProfileImgFile) {
                var temp = 0;
                if ($("#FirstName").is(":visible") && $("#FirstName").val() == "") {
                    $("#ErrorFirstName").show();
                    temp = 1;
                }
                if ($("#LastName").is(":visible") && $("#LastName").val() == "") {
                    $("#ErrorLastName").show();
                    temp = 1;
                }

                if ($("#PrimaryEmail").is(":visible") && $("#PrimaryEmail").val() == "") {
                    $("#ErrorPrimaryEmail").show();
                    temp = 1;
                }

                if ($("#Mobile").is(":visible") && $("#Mobile").val() == "") {
                    $("#ErrorPrimaryMobile").show();
                    temp = 1;
                }

                if (temp == 1) {
                    return;
                }
                GoToNextStep();
            }
        } else if (formtype == "editOrganization") {
            if (new ValidationService().checkFormValidity(FormS) && !$scope.common.maxProfileImgFileSize && !$scope.common.invalidProfileImgFile && !$scope.common.minProfileImgFileSize) {
                if ($("#CountryId").is(":visible") && $("#CountryId").val() == "") {

                    $("#CountryIdError").show();
                    temp = 1;
                }
                if ($("#StateId").is(":visible") && $("#StateId").val() == "") {
                    $("#StateIdError").show();
                    temp = 1;
                }
                GoToNextStep();
            }
        } else if (formtype == "createcompanycontact") {
            if (new ValidationService().checkFormValidity(FormS) && !$scope.common.maxProfileImgFileSize && !$scope.common.invalidProfileImgFile && !$scope.common.minProfileImgFileSize) {
                GoToNextStep();
            }
        }


        function GoToNextStep() {

            var activeli = $('.gsi-step-indicator li.current');
            var nextli = $(activeli).next();
            $(activeli).removeClass('current');
            $(nextli).addClass('current');

            var activeContent = $('.tsf-content div.active');
            var nextContent = $(activeContent).next();
            $(activeContent).removeClass('active');
            $(nextContent).addClass('active');

            if ($('li.current').data('target') == stepcount) {
                $("#nextBtn").hide();
                $("#preview").show();
                //$("#nextSbmt").show();
            } else {
                $("#nextBtn").show();
                $("#preview").hide();
                //$("#nextSbmt").hide();
            }

            $(".select2").select2({ allowClear: true });
            $("#preBtn").show();

            $('#skills,#SkillList').select2({
                tags: true,
                allowClear: true
            });

        }

    }

    $scope.step_tabclick = function(stepcount, $event, FormS, formtype) {



        if (formtype == 'candidate') {

            if (new ValidationService().checkFormValidity(FormS) &&
                $scope.common.candidateModel.Resume &&
                $scope.common.candidateModel.Resume.name &&
                !$scope.common.invalidProfileImgFile &&
                !$scope.common.maxProfileImgFileSize) {
              var temp = 0;
                if ($("#ProfileSummary").is(":visible") && $("#ProfileSummary").val().length > 2000) {
                    $("#ProfileSummaryError").show();
                    return;
                }
                GoToNextStep();
            } else {
                if ($scope.common.candidateModel.Resume === undefined) {
                    $scope.common.resumePresent = true;
                  $scope.common.candidateModel.Resume = {};
                  if ($("#CountryId").is(":visible") && $("#CountryId").val() == "") {

                    $("#CountryIdError").show();
                    temp = 1;
                  }
                  if ($("#StateId").is(":visible") && $("#StateId").val() == "") {
                    $("#StateIdError").show();
                    temp = 1;
                  }
                }
              $scope.common.displayValidationSummary = true;
            }
        } else if (formtype == 'requirement') {
            

            if (new ValidationService().checkFormValidity(FormS)) {
                var temp = 0;
                if ($("#location").is(":visible") && $("#location").val() == "") {
                    $("#locationError").show();
                    temp = 1;
                }
                if ($("#duration").is(":visible") && $("#duration").val() == "") {
                    $("#durationError").show();
                    temp = 1;
                }
                if ($("#CountryId").is(":visible") && $("#CountryId").val() == "") {

                    $("#CountryIdError").show();
                    temp = 1;
                }
                if ($("#StateId").is(":visible") && $("#StateId").val() == "") {
                    $("#StateIdError").show();
                    temp = 1;
                }
                if (temp == 1) {
                    return;
                }
                GoToNextStep();
            }
        } else if (formtype == 'company') {
            if (new ValidationService().checkFormValidity(FormS) && !$scope.common.maxProfileImgFileSize && !$scope.common.invalidProfileImgFile && !$scope.common.minProfileImgFileSize) {

                GoToNextStep();
            }

        } else if (formtype == 'newuser') {
            if (new ValidationService().checkFormValidity(FormS) && !$scope.common.maxProfileImgFileSize && !$scope.common.invalidProfileImgFile) {


                var temp = 0;
                if ($("#FirstName").is(":visible") && $("#FirstName").val() == "") {
                    $("#ErrorFirstName").show();
                    temp = 1;
                }
                if ($("#LastName").is(":visible") && $("#LastName").val() == "") {
                    $("#ErrorLastName").show();
                    temp = 1;
                }

                if ($("#PrimaryEmail").is(":visible") && $("#PrimaryEmail").val() == "") {
                    $("#ErrorPrimaryEmail").show();
                    temp = 1;
                }

                if ($("#Mobile").is(":visible") && $("#Mobile").val() == "") {
                    $("#ErrorPrimaryMobile").show();
                    temp = 1;
                }
                if ($("#CountryId").is(":visible") && $("#CountryId").val() == "") {
                    $("#CountryIdError").show();
                    temp = 1;
                }
                if ($("#StateId").is(":visible") && $("#StateId").val() == "") {
                    $("#StateIdError").show();
                    temp = 1;
                }
                if (temp == 1) {
                    return;
                }

                GoToNextStep();
            }
        } else if (formtype == "editOrganization") {
            if (new ValidationService().checkFormValidity(FormS) && !$scope.common.maxProfileImgFileSize && !$scope.common.invalidProfileImgFile && !$scope.common.minProfileImgFileSize) {
                if ($("#CountryId").is(":visible") && $("#CountryId").val() == "") {

                    $("#CountryIdError").show();
                    temp = 1;
                }
                if ($("#StateId").is(":visible") && $("#StateId").val() == "") {
                    $("#StateIdError").show();
                    temp = 1;
                }
                if (temp == 1) {
                    return;
                }

                GoToNextStep();
            }
        } else if (formtype == "createcompanycontact") {
            if (new ValidationService().checkFormValidity(FormS) && !$scope.common.maxProfileImgFileSize && !$scope.common.invalidProfileImgFile && !$scope.common.minProfileImgFileSize) {
                GoToNextStep();
            }
        }


        function GoToNextStep() {
            var activeli = $('.gsi-step-indicator li.current');
            var nextli = $(activeli).next();
            $(activeli).removeClass('current');
            $($event.currentTarget).addClass('current');

            var activeContent = $('.tsf-content div.active');

            var nextContent = $($event.currentTarget).data('target');

            $(activeContent).removeClass('active');
            $("." + nextContent).addClass('active');

            if ($('li.current').data('target') == stepcount) {
                $("#nextBtn").hide();
                //$("#nextSbmt").show();
            } else {
                $("#nextBtn").show();
                //$("#nextSbmt").hide();
            }

            $("#preBtn").show();
            $(".select2").select2({ allowClear: true });

            if ($('li.current').data('target') == 1) {
                $("#preBtn").hide();
            } else {
                $("#preBtn").show();
            }
                
            $('#skills,#SkillList').select2({
                tags: true,
                allowClear: true
            });
        }

    }


    $scope.step_preBtn = function() {

        var activeli = $('.gsi-step-indicator li.current');
        var nextli = $(activeli).prev();
        $(activeli).removeClass('current');
        $(nextli).addClass('current');

        var activeContent = $('.tsf-content div.active');
        var nextContent = $(activeContent).prev();
        $(activeContent).removeClass('active');
        $(nextContent).addClass('active');

        $("#nextBtn").show();
        $("#preview").hide();
        //$("#nextSbmt").hide();

        $(".select2").select2({ allowClear: true });
        if ($('li.current').data('target') == 1) {
            $("#preBtn").hide();
        } else {
            $("#preBtn").show();
        }

        $('#skills,#SkillList').select2({
            tags: true,
            allowClear: true
        });
    }

    $scope.getCandidateRatingDetails = function() {

        if (!$scope.common.selectedCandidate) {

            $location.path('candidateList');
            //blockUI.stop();
            return;
            // }
        }
        var promise = XSeedApiFactory.GetCandidateRatingDetails($scope.common.selectedCandidate._id);
        promise.then(
            function(response) {
                //console.log(response);
                if (response != null) {
                    $scope.CandidateRatingDetails = response[0].data;
                    // console.log($scope.CandidateRatingDetails);
                    setTimeout(function() {
                        var $input = $('input.rating');
                        if ($input.length) {
                            $input.removeClass('rating-loading').addClass('rating-loading').rating();
                        }
                    }, 0);
                }
                // blockUI.stop();
            },

            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('CandidateUser', httpError.data.Status, httpError.data.Message);
            });
    }

    $scope.CheckScreenSize = function() {
        var screenWidth = $window.innerWidth;
        if (screenWidth < 992) {
            return "Smallcontact-view";
        } else {
            return "";
        }
    }

};
$(document).on('click', ".sub-menu a", function(e) {

    e.stopPropagation();
})
