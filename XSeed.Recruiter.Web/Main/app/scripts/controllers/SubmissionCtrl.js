'use strict';

XSEED_APP.controller('SubmissionCtrl', SubmissionCtrl);

/**
 * @ngInject
 */
function SubmissionCtrl($scope, $rootScope, $route, $routeParams, RequestContext, $timeout, $q, $location, $window, configuration, i18nFactory, ExceptionHandler, EventBus, $log, _, ipCookie, blockUI, ModalFactory, XSeedAlert, localStorageService, $uibModal, XSeedApiFactory, ngTableParams, $filter, ValidationService) {

    $scope.submissionFeedback = {};

    init();

    var candidateSubmissionEmailModal = ModalFactory.candidateSubmissionEmailModal($scope);
    var editSubmissionStatusModal = ModalFactory.editSubmissionStatusModal($scope);

    var submissionActivityQnotesModal = ModalFactory.submissionActivityQnotesModal($scope);

    //Initialization Function
    function init() {
        $scope.checkedCount = 0;
        $rootScope.commonEmailModel = {};
        $scope.candidateSubmissionList = [];
        $scope.qNoteModelSubmission = {};
        $scope.flag.advanceSearchFlag = false;
        $scope.filterItemLable = [];
        $scope.filterItemCount = 0;
        $scope.submissionFilter = {};
        $scope.common.advanceSearchModel = {};
        $scope.common.selectedCandidate = {};
        $scope.filterItemCount = 0;
    };

    //Submission To Client

    //Checkbox code-
    $scope.checkboxes = { 'checked': false, items: {} };

    // watch for check all checkbox
    $scope.$watch('checkboxes.checked', function(value) {
        angular.forEach($scope.submissions, function(item) {
            if (angular.isDefined(item.Id)) {
                $scope.checkboxes.items[item.Id] = value;
            }
        });
    });

    // watch for data checkboxes
    $scope.$watch('checkboxes.items', function(values) {
        if (!$scope.submissions) {
            return;
        }
        var checked = 0,
            unchecked = 0,
            total = $scope.submissions.length;
        angular.forEach($scope.submissions, function(item) {
            checked += ($scope.checkboxes.items[item.Id]) || 0;
            unchecked += (!$scope.checkboxes.items[item.Id]) || 0;
        });
        if ((unchecked == 0) || (checked == 0)) {
            $scope.checkboxes.checked = (checked == total);
        }

        $scope.checkedCount = checked;
        //alert(checked);
        //alert(unchecked);
        //grayed checkbox
        angular.element(document.getElementById("select_all")).prop("indeterminate", (checked != 0 && unchecked != 0));
    }, true);


    $scope.submissionToClientSingle = function(submission) {
        $scope.common.selectedRowSubmission = submission.Id;
        $scope.common.selectedSubmission = submission;
        $scope.submissionToClient('single', submission)
    }

    $scope.submissionToClient = function(type, submission) {
        $scope.candidateSubmissionList = [];

        angular.forEach($scope.checkboxes.items, function(item, Identifier) {
            if (item == true) {
                var selectedSubmission = _.find($scope.submissions, function(o) { return o.Id == Identifier; });
                $scope.candidateSubmissionList.push(selectedSubmission);
            }
        });
        if ($scope.candidateSubmissionList.length > 0) {
            $scope.isSameJob = true;
            for (var i = 1; i < $scope.candidateSubmissionList.length; i++) {
                if ($scope.candidateSubmissionList[0].JobId != $scope.candidateSubmissionList[i].JobId) {
                    $scope.isSameJob = false;
                    XSeedAlert.swal({
                        title: 'Info!',
                        text: 'Please select submissions against single requirement!',
                        type: "info",
                        customClass: "xseed-error-alert"
                    }).then(function() {
                        $timeout(function() {}, 0);
                    }, function(dismiss) {})
                }
            }

            if ($scope.isSameJob) {
                $scope.getSubmissionUrl($scope.candidateSubmissionList[0].CompanyId);
                candidateSubmissionEmailModal.$promise.then(candidateSubmissionEmailModal.show);
            }
        } else if (type = 'single') {
            $scope.candidateSubmissionList.push(submission);
            $scope.getSubmissionUrl(submission.CompanyId);
            candidateSubmissionEmailModal.$promise.then(candidateSubmissionEmailModal.show);
        }
    };

    $scope.openCandidateSubmissionEmailModal = function() {

        if ($scope.candidateSubmissionList.length > 0) {
            blockUI.start();
            //get company contacts
            var promise = XSeedApiFactory.getSubmissionContactEmail($scope.candidateSubmissionList[0].CompanyId);
            promise.then(
                function(response) {
                    $rootScope.commonEmailModel.candidateList = [];

                    //get company contact email
                    if (response[0].data) {
                        $rootScope.commonEmailModel.ToEmailID = [];
                        angular.forEach(response[0].data, function(val, index) {
                            $rootScope.commonEmailModel.ToEmailID.push(val.Name);

                        });
                        $rootScope.commonEmailModel.ToEmailID = $rootScope.commonEmailModel.ToEmailID.join();
                    }
                    $rootScope.commonEmailModel.FromEmailID = $rootScope.userDetails.PrimaryEmail;
                    $rootScope.commonEmailModel.MailSubject = "Submissions";
                    $rootScope.commonEmailModel.MailBody = "We would like to submit following candidate(s) against the<br />Requirement: ";
                    //get job title
                    $rootScope.commonEmailModel.MailBody = $rootScope.commonEmailModel.MailBody + $scope.candidateSubmissionList[0].JobTitle;
                    //get candidate names
                    $rootScope.commonEmailModel.MailBody = $rootScope.commonEmailModel.MailBody + " <br />following is/are the candidate(s)";
                    angular.forEach($scope.candidateSubmissionList, function(val, index) {
                        $rootScope.commonEmailModel.candidateList.push(val.CandidateList[0].Id);
                        index = index + 1
                        $rootScope.commonEmailModel.MailBody = $rootScope.commonEmailModel.MailBody + "<br />" + index + ". " + val.CandidateList[0].Name;
                    });
                    $rootScope.commonEmailModel.typeOfNotification = $scope.constants.submitCandidateByEmail;
                    $scope.common.commonEmailModal.$promise.then($scope.common.commonEmailModal.show);
                    blockUI.stop();
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
                });
        }

    }

    $scope.closeCandidateSubmissionEmailModal = function() {
        candidateSubmissionEmailModal.$promise.then(candidateSubmissionEmailModal.hide);
        $scope.checkboxes.items = {};
    }

    $scope.submitBySubmissionUrl = function() {
        $scope.closeCandidateSubmissionEmailModal();
        $location.path("submissionToClient");
    };

    $scope.GetValue = function (fruit) {
        var subId = $scope.submissionFilter.select_filterSubmittedBy;
        var submId = $scope.submissionFilter.select_filterCompany;
        var submiId = $scope.submissionFilter.select_filterTitle;
        //var fruitkey = angular.element("#disb");
        //var scr = $sce.parseAsHtml("asdfasdf");
        //$window.alert("Selected Status: " + filterId + "Selected Status: " + filtId);
        if (subId != 0 || submId != 0 || submiId != 0) {
            $scope.filterItemCount = 1;
        } else {
            $scope.filterItemCount = 0;
        }
    };
    ////------Sumission functions------------
    /*------------------------------------ First row selection----------------------------------*/

    //for submission
    $scope.setClickedRowSubmission = function(submission) {
        if (submission) {
            $scope.common.selectedRowSubmission = submission.Id;
            $scope.common.selectedSubmission = submission;
            // $scope.getSubmissionById();
            // $scope.getQNoteListSubmission();
            // //$scope.getActivityListSubmission();
        }
    };

    $scope.GetActivities_notes_for_Submission = function(submission) {
        if (submission) {
            $scope.common.selectedRowSubmission = submission.Id;
            $scope.common.selectedSubmission = submission;
            $scope.getSubmissionById();
            $scope.getQNoteListSubmission();
            submissionActivityQnotesModal.$promise.then(submissionActivityQnotesModal.show);


        }
    }
  $scope.closesubmissionActivityQnotesModal = function () {
    debugger;
        submissionActivityQnotesModal.$promise.then(submissionActivityQnotesModal.hide);
    }

    /*------------------------------------ First row selection----------------------------------*/

    /* QNote start submissions */
    $scope.getQNoteListSubmission = function() {
        blockUI.start();
        var promise = XSeedApiFactory.getQNoteListSubmission($scope.common.selectedSubmission.Id);
        promise.then(
            function(response) {
                $scope.qNoteList = response[0].data;
                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
            });
    };
    //delete QNote submission
    $scope.deleteQNotesubmission = function(qnoteid, position) {

        XSeedAlert.swal({
            title: "Are you sure?",
            text: "This action will delete the Quick Note!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function() {

            if (qnoteid) {

                var promise = XSeedApiFactory.deleteQNotesubmission(qnoteid);
                promise.then(
                    function(response) {
                        $scope.qNoteList.splice(position, 1);

                    },
                    function(httpError) {
                        ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
                    });
            }
        })
    }

    $scope.UpdateQNoteSubmission = function(editid) {
        if ($scope.qNoteModelSubmission.qNote) {
            // XSeedAlert.swal({
            //     title: "Are you sure?",
            //     text: "This action will update the Quick Note!",
            //     type: "warning",
            //     showCancelButton: true,
            //     confirmButtonClass: "btn-danger",
            //     confirmButtonText: "Update",
            //     cancelButtonText: "Cancel",
            //     closeOnConfirm: false,
            //     closeOnCancel: false
            // }).then(function () {

            //     if (editid) {
            $scope.qNoteList[$scope.common.qNoteList.editposition].Description = $scope.qNoteModelSubmission.qNote;
            var promise = XSeedApiFactory.UpdateQNoteSubmission($scope.qNoteList[$scope.common.qNoteList.editposition], editid);
            promise.then(
                function(response) {
                    $scope.common.qNoteList.EditID = null;
                    $('#summernote').summernote('code', '');
                    $scope.common.qNoteList.EditMode = false;

                    $scope.getQNoteListSubmission();

                },
                function(httpError) {
                    ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
                });
        } else {
            $("#error-summernote").show();
            return;
        }
        //     }
        // }
        // )
    }

    $scope.submitQNoteSubmission = function() {
        if ($scope.qNoteModelSubmission.qNote) {
            var promise = XSeedApiFactory.createQNoteSubmission(encodeURIComponent($scope.qNoteModelSubmission.qNote), $scope.common.selectedSubmission.Id);
            promise.then(
                function(response) {
                    blockUI.stop();
                    $scope.qNoteModelSubmission.qNote = '';
                    $scope.getQNoteListSubmission();
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                });
        }else
        {
            $("#error-summernote").show();
            return;
        }
       
    };
    /* QNote end submissions*/

    $scope.openCandidateDetailView = function(candidateid) {
        $scope.common.selectedCandidate._id = candidateid;
        $location.path('candidateDetail');
        $rootScope.isSingleDetail = true;
    }
    $scope.ShowAssociatedCandidateProfile = function(candidateid) {
        $scope.closeRequirementDetailModal(false);
        $scope.openCandidateDetailView(candidateid);
    }

    $scope.getSubmissionById = function() {

        if ($scope.common.selectedSubmission.Id) {
            blockUI.start();
            var promise = XSeedApiFactory.getSubmissionById($rootScope.userDetails.OrganizationId, $scope.common.selectedSubmission.Id);
            promise.then(
                function(response) {
                    blockUI.stop();
                    $scope.SubmissionStatusList = response[0].data.SubmissionStatusList;
                    $scope.ClientFeedbackList = response[0].data.ClientFeedbackList;
                    $scope.CandidateFeedbackList = response[0].data.CandidateFeedbackList;
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
                });
        }
    }

    $scope.getSubmissions = function() {

        if ($rootScope.userHasToken == false) {
            $location.path('login');
            return false;
        }

        var submissionsURL = '/submissions';
        var dashboardURL = '/dashboard';

        if (($location.path().toLowerCase() == submissionsURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Submission_Read == "False") {
            $location.path('dashboard');
            return false;
        }

        if ($rootScope.userDetails.OrganizationId) {
            blockUI.start();
            $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
            $scope.common.pageNumberShow = undefined;
            var firstPageFlag = 0;

            var promise = XSeedApiFactory.getSubmissions($rootScope.userDetails.OrganizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow);
            promise.then(
                function(response) {
                    blockUI.stop();


                    $scope.submissions = response[0].data.Submissions;
                    console.log($scope.submissions);

                    $scope.submissionsTotalCount = response[0].data.TotalCount;
                    $scope.submissionsTotalPages = response[0].data.TotalPages;

                    if ($scope.submissions.length) {
                        //to select first row by default
                        $scope.setClickedRowSubmission($scope.submissions[0]);
                        $scope.getSubmissionStatusLookUp();
                    }
                    //to show skills in tooltip as string
                    angular.forEach($scope.submissions, function(val, index) {
                        if (val.ContactEmailList) {
                            val.ContactEmailList = val.ContactEmailList.join();
                        }
                    });

                    firstPageFlag = 1;
                    populateSubmissionList($scope.submissions, $scope.common.totalCount, $scope.common.totalPages, firstPageFlag);
                    submissionFilterLookUp();
                    $timeout(function() {
                        $scope.populateFilterLookupList();
                    }, 2000);
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
                });
        }
    }

    function populateSubmissionList(submissions, totalCount, totalPages, pageFlag) {



        if (angular.isDefined($scope.submissionList)) {
            $scope.submissionList.reload();
        } else {
            $scope.submissionList = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: $scope.common.ngTablePaginationCount,
                total: $scope.submissionsTotalCount,
                getData: function($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    if (pageFlag != 1) {
                        /* sorting */
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function(val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */
                        if (document.getElementById("select_all").checked == true) {
                            $("#select_all").click();
                        }
                        $('input[name=SelectBox]').each(function() {
                            if ($(this).prop("checked") == true) {
                                $(this).click();
                            }
                        })

                        var promise = XSeedApiFactory.getSubmissions($rootScope.userDetails.OrganizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder);
                        promise.then(
                            function(response) {


                                // angular.element(document.getElementById("select_all")).prop("indeterminate", (checked != 0 && unchecked != 0));

                                $scope.submissions = response[0].data.Submissions;
                                $scope.submissionsTotalCount = response[0].data.TotalCount;
                                $scope.submissionsTotalPages = response[0].data.TotalPages;

                                //to select first row by default
                                $scope.setClickedRowSubmission($scope.submissions[0]);

                                //to show skills in tooltip as string
                                angular.forEach($scope.submissions, function(val, index) {
                                    if (val.ContactEmailList) {
                                        val.ContactEmailList = val.ContactEmailList.join();
                                    }
                                });

                                blockUI.stop();
                            },
                            function(httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
                            });

                    }
                    pageFlag = 0;
                    params.total($scope.submissionsTotalCount);
                    $defer.resolve($scope.submissions);
                },
                $scope: $scope
            });
        }
    }


    //Filter
    $scope.getSubmissionStatusLookUp = function() {
        var promise = XSeedApiFactory.getSubmissionStatusLookUp();
        promise.then(
            function(response) {
                $scope.submissionFeedbackStatusList = response[0].data;
                $scope.submissionClientFeedbackStatusList = response[1].data;
                $scope.submissionCandidateFeedbackStatusList = response[2].data;
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
            });
    };

    // $scope.addFilterItemButton = function(type, id, name, selected) {
    //     if (selected == true) {
    //         $scope.filterItemLable.push({ Id: id, Name: name });
    //         $scope.filterItemCount = $scope.filterItemCount + 1;
    //     } else {
    //         $scope.removeFilterItemButton(id);
    //     }
    // };

    // $scope.removeFilterItemButton = function(id) {
    //     angular.forEach($scope.filterItemLable, function(val, index) {
    //         if (val && val.Id == id) {
    //             $scope.filterItemLable[index] = undefined;
    //         }
    //     });

    //     removeItemFromFilterList(id);
    //     $scope.filterItemCount = $scope.filterItemCount - 1;
    // };

    // function removeItemFromFilterList(id) {
    //     angular.forEach($scope.submissionFilterLookup, function(val1, index1) {
    //         angular.forEach(val1, function(val2, index2) {
    //             angular.forEach(val2, function(val3, index3) {
    //                 if (val3.Id == id) {
    //                     val3.selected = false;
    //                 }
    //             });
    //         });
    //     });
    // };

    // function clearFilter() {
    //     angular.forEach($scope.submissionFilterLookup, function(val1, index1) {
    //         angular.forEach(val1, function(val2, index2) {
    //             angular.forEach(val2, function(val3, index3) {
    //                 if (val3.selected == true) {
    //                     $scope.removeFilterItemButton(val3.Id);
    //                 }
    //             });
    //         });
    //     });
    // };

    //Advance search
    $scope.searchSubmissions = function() {
        if ($scope.common.advanceSearchModel) {
            blockUI.start();
            $scope.flag.advanceSearchFlag = true;
            $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
            $scope.common.pageNumberShow = undefined;
            var firstPageFlag = 0;

            $scope.common.advanceSearchModel.OrganizationId = $rootScope.userDetails.OrganizationId;

            var promise = XSeedApiFactory.searchSubmissions($scope.common.advanceSearchModel, $scope.common.pageSizeShow, $scope.common.pageNumberShow);
            promise.then(
                function(response) {
                    blockUI.stop();

                    $scope.submissions = response[0].data.Submissions;
                    console.log($scope.submissions);
                    $scope.submissionsTotalCount = response[0].data.TotalCount;
                    $scope.submissionsTotalPages = response[0].data.TotalPages;
                    angular.forEach($scope.submissions, function(val, index) {
                        if (val.ContactEmailList) {
                            val.ContactEmailList = val.ContactEmailList.join();
                        }
                    });
                    if ($scope.submissions.length) {
                        //to select first row by default
                        $scope.setClickedRowSubmission($scope.submissions[0]);
                        //$scope.getSubmissionStatusLookUp();
                    }
                    firstPageFlag = 1;
                    populateSearchSubmissionList($scope.submissions, $scope.common.totalCount, $scope.common.totalPages, firstPageFlag);
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
                });
        }
    }

    function populateSearchSubmissionList(submissions, totalCount, totalPages, pageFlag) {
        //if (angular.isDefined($scope.submissionList)) {
        //    $scope.submissionList.reload();
        //}
        //else
        {
            $scope.submissionList = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: $scope.common.ngTablePaginationCount,
                total: $scope.submissionsTotalCount,
                getData: function($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    if (pageFlag != 1) {
                        /* sorting */
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function(val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */

                        if (document.getElementById("select_all").checked == true) {
                            $("#select_all").click();
                        }
                        $('input[name=SelectBox]').each(function() {
                            if ($(this).prop("checked") == true) {
                                $(this).click();
                            }
                        })
                        var promise = XSeedApiFactory.searchSubmissions($scope.common.advanceSearchModel, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder);
                        promise.then(
                            function(response) {
                                $scope.submissions = response[0].data.Submissions;
                                $scope.submissionsTotalCount = response[0].data.TotalCount;
                                $scope.submissionsTotalPages = response[0].data.TotalPages;

                                //to select first row by default
                                $scope.setClickedRowSubmission($scope.submissions[0]);

                                blockUI.stop();
                            },
                            function(httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
                            });

                    }
                    pageFlag = 0;
                    params.total($scope.submissionsTotalCount);
                    $defer.resolve($scope.submissions);
                },
                $scope: $scope
            });
        }
    };

    $scope.resetSubmissionSearch = function() {
        $scope.submissionList = undefined;
        $scope.common.advanceSearchModel = {};
        $scope.flag.advanceSearchFlag = false;
        $scope.getSubmissions();
        // clearFilter();
    };

    function submissionFilterLookUp() {
        var organizationId = $rootScope.userDetails.OrganizationId;
        var promise = XSeedApiFactory.submissionFilterLookUp(organizationId);
        promise.then(
            function(response) {
                $scope.lookup.organizationUser = response[0].data;
                $scope.lookup.company = response[1].data;
                $scope.lookup.jobTitle = response[2].data;
            },
            function(httpError) {
                ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
            });
    };

    $scope.populateFilterLookupList = function() {
        $scope.submissionFilterLookup = [];
        var organizationUserArray = [];
        var self = angular.copy(_.find($scope.lookup.organizationUser, function(o) { return o.Id == $rootScope.userDetails.Id; }));
        if (self) {
            self.Name = "Me";
            organizationUserArray.push(self);
        }

        angular.forEach($scope.lookup.organizationUser, function(val, index) {
            if ($scope.lookup.organizationUser[index].Id != $rootScope.userDetails.Id) {
                organizationUserArray.push($scope.lookup.organizationUser[index]);
            }
        });

        $scope.submissionFilterLookup.push({ "Submitted_By": organizationUserArray });
        $scope.submissionFilterLookup.push({ "Company": $scope.lookup.company });
        $scope.submissionFilterLookup.push({ "Requirement_Title": $scope.lookup.jobTitle });
    }

    $scope.getSelectedFilterItem = function() {
        var OrganizationUser = [];
        var Company = [];
        var JobTitle = [];

        //angular.forEach($scope.submissionFilterLookup, function(val1, index1) {
        //    angular.forEach(val1, function(val2, index2) {
        //        angular.forEach(val2, function(val3, index3) {
        //            if (index1 == 0 && val3.selected == true)
        //                OrganizationUser.push(val3.Id);
        //            else if (index1 == 1 && val3.selected == true)
        //                Company.push(val3.Id);
        //            else if (index1 == 2 && val3.selected == true)
        //                JobTitle.push(val3.Name);
        //        });
        //    });
        //});

        if ($scope.submissionFilter.select_filterSubmittedBy) {
            OrganizationUser = $scope.submissionFilter.select_filterSubmittedBy;
        }

        if ($scope.submissionFilter.select_filterCompany) {
            Company = $scope.submissionFilter.select_filterCompany;
        }

        if ($scope.submissionFilter.select_filterTitle) {
            JobTitle = $scope.submissionFilter.select_filterTitle;
        }

        console.log($scope.submissionFilter.select_filterTitle)
        console.log(JobTitle);

        var filterSelection = {};
        filterSelection.OrganizationUser = OrganizationUser;
        filterSelection.Company = Company;
        filterSelection.JobTitle = JobTitle;
        filterSelection.OrganizationId = $rootScope.userDetails.OrganizationId;
        filterSelection.OrganizationUserId = $rootScope.userDetails.UserId;
        $scope.common.advanceSearchModel.filter = filterSelection;

        $scope.searchSubmissions();
    };

    $scope.$watch("submissionListfilter.$", function() {
        if (angular.isDefined($scope.submissionList)) {
            $scope.submissionList.reload();
            $scope.submissionList.page(1);
        }
    });

    ////-------------------------------------

    $scope.showInterviewType = function(submissionFeedback) {
        if (submissionFeedback.submissionFeedbackStatusId == getSubmissionFeedbackInterviewStatusId()) {
            submissionFeedback.internalStatusIsIneterview = true;
        } else {
            submissionFeedback.interviewType;
            submissionFeedback.internalStatusIsIneterview = false;
        }
    }

    function getSubmissionFeedbackInterviewStatusId() {
        var item = _.findWhere($scope.submissionFeedbackStatusList, { Name: 'Interview' });
        return item.Id;
    }

    /****************************************Change submission status*********************************/

    $scope.openEditSubmissionStatusModal = function(submission) {
        ////LookUp call
        //$scope.getSubmissionStatusLookUp();
        $scope.toggleFeedback = 'Internal';
        editSubmissionStatusModal.$promise.then(editSubmissionStatusModal.show);

        $scope.common.selectedRowSubmission = submission.Id;
        $scope.common.selectedSubmission = submission;

        setTimeout(function() {
            var $input = $('input.rating');
            if ($input.length) {
                $input.removeClass('rating-loading').addClass('rating-loading').rating();
            }
        }, 0);

    };

    $scope.closeEditSubmissionStatusModal = function() {
        $scope.submissionFeedback = {};
        editSubmissionStatusModal.$promise.then(editSubmissionStatusModal.hide);
        //$scope.common.advanceSearchModel = {};
    };

    $scope.ConvertToArray = function(emails) {
        return emails.split(',');
    }

    $scope.showAppliedCandidates_click = function(candidateId) {
        $rootScope.appliedCandidateId = candidateId;
        $scope.showAppliedCandidates($scope.common.selectedJob);
    }

    /******************************************Initialize Ratings ************************************/

    $scope.initializeRatings = function() {
        $scope.submissionFeedback.communicationRating = 1.5;
        $scope.submissionFeedback.technicalRating = 2.0;
        $scope.submissionFeedback.experianceRating = 2.5;
        $scope.submissionFeedback.educationalRating = 3.0;
    }

    /****************************************Change submission status*********************************/

    $scope.saveFeedback = function(editSubmissionStatusForm, feedbackType, statusId, remark, interviewType) {
        console.log($scope.submissionFeedback.communicationRating);
        console.log($scope.common.selectedSubmission.CandidateList[0].Id);
        if (new ValidationService().checkFormValidity(editSubmissionStatusForm)) {

            if (feedbackType == $scope.constants.submissionInternalFeedback) {
                var feedbackModel = {
                    "SubmissionId": $scope.common.selectedSubmission.Id,
                    "StatusId": $scope.submissionFeedback.submissionFeedbackStatusId,
                    "Remark": $scope.submissionFeedback.submissionFeedbackRemark,
                    "InterviewType": $scope.submissionFeedback.interviewType,
                    "FeedbackType": feedbackType,
                    "Date": new Date()
                }
            } else if (feedbackType == $scope.constants.submissionCandidateFeedback) {
                var feedbackModel = {
                    "SubmissionId": $scope.common.selectedSubmission.Id,
                    "StatusId": $scope.submissionFeedback.candidateFeedbackStatusId,
                    "Remark": $scope.submissionFeedback.candidateFeedbackRemark,
                    "FeedbackType": feedbackType,
                    "Date": new Date(),
                    "CommunicationRating": $scope.submissionFeedback.communicationRating,
                    "TechnicalRating": $scope.submissionFeedback.technicalRating,
                    "ExperianceRating": $scope.submissionFeedback.experianceRating,
                    "EducationalRating": $scope.submissionFeedback.educationalRating,
                    "CandidateId": $scope.common.selectedSubmission.CandidateList[0].Id

                }
            } else if (feedbackType == $scope.constants.submissionClientFeedback) {
                var feedbackModel = {
                    "SubmissionId": $scope.common.selectedSubmission.Id,
                    "StatusId": $scope.submissionFeedback.clientFeedbackStatusId,
                    "Remark": $scope.submissionFeedback.clientFeedbackRemark,
                    "InterviewType": $scope.submissionFeedback.submissionFeedbackStatusId,
                    "FeedbackType": feedbackType,
                    "Date": new Date()

                }
            }

            if (feedbackModel) {
                blockUI.start();
                var promise = XSeedApiFactory.saveFeedback(feedbackType, feedbackModel);
                promise.then(
                    function(response) {
                        blockUI.stop();

                        XSeedAlert.swal({
                            title: 'Success!',
                            text: feedbackType + ' saved successfully.',
                            type: "success",
                            customClass: "xseed-error-alert",
                        }).then(function() {
                            if ($scope.common.selectedSubmission.Id) {
                                $scope.getSubmissionById();
                            }
                            $scope.closeEditSubmissionStatusModal();
                           
                            // Refresh Page contents
                            EventBus.switchView = "submissions";
                            $location.path("switchView").replace();


                        }, function(dismiss) {})
                    },
                    function(httpError) {
                        blockUI.stop();

                        if (feedbackType == 'Submission Feedback') {
                            ExceptionHandler.handlerHTTPException('SubmissionStatus', httpError.data.Status, httpError.data.Message);
                        } else if (feedbackType == 'Candidate Feedback') {
                            ExceptionHandler.handlerHTTPException('CandidateFeedback', httpError.data.Status, httpError.data.Message);
                        } else if (feedbackType == 'Client Feedback') {
                            ExceptionHandler.handlerHTTPException('ClientFeedback', httpError.data.Status, httpError.data.Message);
                        }
                    });
            }
        }
    }

    //***************************************************************************************//

    // Get the render context local to this controller (and relevant params).
    var renderContext = RequestContext.getRenderContext("standard.submission");
    // The subview indicates which view is going to be rendered on the page.
    $scope.subview = renderContext.getNextSection();
    $scope.$on(
        "requestContextChanged",
        function() {
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {
                return;
            }
            // Update the view that is being rendered.
            $scope.subview = renderContext.getNextSection();
        }
    );
};
