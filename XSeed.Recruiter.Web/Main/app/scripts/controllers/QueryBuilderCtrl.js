﻿'use strict';

XSEED_APP.controller('QueryBuilderCtrl', QueryBuilderCtrl);

function QueryBuilderCtrl($scope, $rootScope, $filter, buildQuery, grouping, addQuotes, XSeedApiFactory, configuration, blockUI, ExceptionHandler, ValidationService, XSeedAlert, ngTableParams, $timeout) {

    init();

    //Initialization Function
    function init() {
        //  blockUI.start();

        $scope.saveBooleanQueryModel = {};
         //$timeout(function() {
         //$scope.getLookUpSkill();
         //    blockUI.stop();
         //}, 1000);
    };

    /******** Common ***********/

    $scope.createFunction = function(input) {
        // format the option and return it
        return {
            value: $scope.skills.length,
            label: input
        };
    };

    $scope.dropdownplaceholder = function () {
        $timeout(function() {
        $(".select2").each(function () { //added a each loop here
            $(this).val("");
            $(this).trigger("change");
        });
        }, 0);
    }
    XSeedApiFactory.setConfiguration(configuration);

    $scope.$watch("filter.$", function() {
        if (angular.isDefined($scope.tblBooleanQueryList)) {
            $scope.tblBooleanQueryList.reload();
            $scope.tblBooleanQueryList.page(1);
        }
    });


    /******** For Easy Query Builder ***********/

    $scope.generateQuery = function(andSearchText, orSearchText, notSearchText) {
        var result = [];
        var andSearch, orSearch, notSearch = "";

        andSearch = andSearchText.length === 0 ? undefined : result.push(buildQuery.getResult(andSearchText, " AND "));
        orSearch = orSearchText.length === 0 ? undefined : result.push(buildQuery.getResult(orSearchText, " OR "));
        notSearch = notSearchText.length === 0 ? undefined : result.push("NOT " + buildQuery.getResult(notSearchText, " OR "));

        $scope.queryString = result.toString().replace(/,/g, " AND ");
        setTimeout(function() {
            $("select.select2").each(function() { //added a each loop here
                $(this).val("");
                $(this).trigger("change");
            });
        }, 100);
    }

    $scope.resetText = function() {
        $scope.queryString = undefined;
    }

    /******** For Advance Query Builder ***********/
    var queryStringText = "";
    $scope.advanceSearchText;

    $scope.advanceResetText = function() {
        queryStringText = "";
        $scope.advanceQueryString = undefined;
    }

    $scope.fillModal = function(advanceQueryString) {

        $scope.saveBooleanQueryModel.Query = advanceQueryString;
        if ($scope.saveBooleanQueryModel.Query) {
            $("#AdvanceQueryModal").modal("show");
        }

    };

    $scope.saveAdvanceQuery = function(advanceQueryTag, advanceQuery) {

    };

    /********Boolean Operations*************/
    //andOperation
    $scope.andOperation = function(keywords) {
        var andQuery = "";

        if (keywords[0] !== "") {
            if (queryStringText == "") {
                if (keywords.length > 0) {
                    for (var i = 0; i < (keywords.length - 1); i++) {
                        andQuery = andQuery + addQuotes.addQuotesCheck(keywords[i]) + " AND ";
                    }
                    andQuery = andQuery + (addQuotes.addQuotesCheck(keywords[keywords.length - 1]));
                    queryStringText = queryStringText + andQuery;
                }
            } else {
                if (keywords.length > 0) {
                    for (var i = 0; i < (keywords.length - 1); i++) {
                        andQuery = andQuery + addQuotes.addQuotesCheck(keywords[i]) + " AND ";
                    }
                    andQuery = andQuery + (addQuotes.addQuotesCheck(keywords[keywords.length - 1]));
                    queryStringText = queryStringText + " AND " + andQuery;
                }
            }
            $scope.advanceQueryString = queryStringText;
            $scope.advanceSearchText = "";
        }

        setTimeout(function() {
            $("select.select2").each(function() { //added a each loop here
                $(this).val("");
                $(this).trigger("change");
            });
        }, 100);
    }

    //orOperation
    $scope.orOperation = function(keywords) {
        var orQuery = "";

        if (keywords[0] !== "") {
            if (queryStringText == "") {
                if (keywords.length > 1) {
                    for (var i = 0; i < (keywords.length - 1); i++) {
                        orQuery = orQuery + addQuotes.addQuotesCheck(keywords[i]) + " OR ";
                    }
                    orQuery = orQuery + (addQuotes.addQuotesCheck(keywords[keywords.length - 1]));
                    queryStringText = queryStringText + grouping.buildGrouping(orQuery);
                }
            } else {
                if (keywords.length > 1) {
                    for (var i = 0; i < (keywords.length - 1); i++) {
                        orQuery = orQuery + addQuotes.addQuotesCheck(keywords[i]) + " OR ";
                    }
                    orQuery = orQuery + (addQuotes.addQuotesCheck(keywords[keywords.length - 1]));
                    queryStringText = queryStringText + " AND " + grouping.buildGrouping(orQuery);
                }
            }
            $scope.advanceQueryString = queryStringText;
        }
    }

    //notOperation
    $scope.notOperation = function(keywords) {
        var notQuery = "";

        if (keywords[0] !== "") {
            if (queryStringText == "") {
                if (keywords.length > 0) {
                    for (var i = 0; i < (keywords.length - 1); i++) {
                        notQuery = notQuery + addQuotes.addQuotesCheck(keywords[i]) + " OR ";
                    }
                    notQuery = notQuery + (addQuotes.addQuotesCheck(keywords[keywords.length - 1]));

                    if (keywords.length > 1) {
                        queryStringText = queryStringText + "NOT" + grouping.buildGrouping(notQuery);
                    } else {
                        queryStringText = queryStringText + " NOT " + notQuery;
                    }
                }
            } else {
                if (keywords.length > 0) {
                    for (var i = 0; i < (keywords.length - 1); i++) {
                        notQuery = notQuery + addQuotes.addQuotesCheck(keywords[i]) + " OR ";
                    }
                    notQuery = notQuery + (addQuotes.addQuotesCheck(keywords[keywords.length - 1]));
                    if (keywords.length > 1) {
                        queryStringText = queryStringText + " AND NOT " + grouping.buildGrouping(notQuery);
                    } else {
                        queryStringText = queryStringText + " AND NOT " + notQuery;
                    }
                }
            }
            $scope.advanceQueryString = queryStringText;
        }
    }

    //nearOperation
    $scope.nearOperation = function(keywords) {
        var nearQuery = "";

        if (keywords[0] !== "") {
            if (queryStringText == "") {
                if (keywords.length > 1) {
                    for (var i = 0; i < (keywords.length - 1); i++) {
                        nearQuery = nearQuery + addQuotes.addQuotesCheck(keywords[i]) + " NEAR ";
                    }
                    nearQuery = nearQuery + (addQuotes.addQuotesCheck(keywords[keywords.length - 1]));
                    queryStringText = queryStringText + grouping.buildGrouping(nearQuery);
                }
            } else {
                if (keywords.length > 1) {
                    for (var i = 0; i < (keywords.length - 1); i++) {
                        nearQuery = nearQuery + addQuotes.addQuotesCheck(keywords[i]) + " NEAR ";
                    }
                    nearQuery = nearQuery + (addQuotes.addQuotesCheck(keywords[keywords.length - 1]));
                    queryStringText = queryStringText + " AND " + grouping.buildGrouping(nearQuery);
                }
            }
            $scope.advanceQueryString = queryStringText;
        }
    }



    /**********Decode query starts***********/

    $scope.advanceDecodeQuery = function(advanceEncodedQueryString) {

        $scope.query = {}
        $scope.andQuerySearchText = "";
        $scope.andTextString = "";
        var NotInString = "";
        var ANDInString = "";
        var ORInString = "";
        var OptionalORString = "";

        var All = [],
            Any = [],
            NotIn = [],
            count = 0;
        advanceEncodedQueryString = advanceEncodedQueryString.replace("AND NOT", "NOT");
        var _advanceEncodedQueryString = extractText(advanceEncodedQueryString);

        if (_advanceEncodedQueryString != advanceEncodedQueryString) {
            angular.forEach(_advanceEncodedQueryString, function(value, index) {
                advanceEncodedQueryString = advanceEncodedQueryString.replace(value, value.replace(' ', '_'));
            });
        }


        var arrBooleanQuery = advanceEncodedQueryString.split(' ');

        var flagAndNot = false;
        var flagOr = false;
        var flagAnd = false;

        for (var index = 0; index < arrBooleanQuery.length; index++) {
            var value = arrBooleanQuery[index];
            if (value == "AND" || value == "OR") {
                continue;
            }


            //console.log("index=>value ::  " + index + "=>" + value);
            if (value == "NOT") {
                flagAndNot = true;
                continue;
            }
            if (flagAndNot) {
                if (value.indexOf(')') > -1) {
                    flagAndNot = false;
                }

                if (value != 'OR')
                    NotIn.push(value);
                continue;
            }

            if (value.indexOf('(') < 0 && value.indexOf(')') < 0) {
                if (arrBooleanQuery[index + 1] == "AND" || arrBooleanQuery[index + 1] == "NOT") {
                    All.push(value);
                }
            } else if (value.indexOf('(') > -1) {
                if (!flagOr && !flagAnd) {
                    if (arrBooleanQuery[index + 1] == "OR") {
                        flagOr = true;
                        Any.push(value);
                        index++;
                        value = arrBooleanQuery[index];
                    }
                    if (arrBooleanQuery[index + 1] == "AND") {
                        flagAnd = true;
                        All.push(value);
                        index++;
                        value = arrBooleanQuery[index];
                    }
                }
                while (true) {
                    if (flagOr) {
                        if (value.indexOf(')') > -1) {
                            flagOr = false;
                        }

                        if (value != 'OR')
                            Any.push(value);

                    }
                    if (flagAnd) {
                        if (value.indexOf(')') > -1) {
                            flagAnd = false;
                        }

                        if (value != 'AND')
                            All.push(value);
                    }

                    if (value.indexOf(')') > -1) {
                        break;
                    }
                    index++;
                    value = arrBooleanQuery[index];
                }

            }
            count++;
        }

        ANDInString = All.toString() === undefined ? undefined : All.toString();
        $scope.query.advanceAndQuerySearchText = ANDInString.replace(/\(|\)/g, "").replace(/,/g, " , ").replace(/_/g, " ");


        OptionalORString = Any.toString() === undefined ? undefined : Any.toString();
        $scope.query.advanceOrOptionalQuerySearchText = OptionalORString.replace(/,/g, " , ").replace(/_/g, " ");

        ORInString = Any.toString() === undefined ? undefined : Any.toString();
        $scope.query.advanceOrSearchText = ORInString.replace(/\(|\)/g, "").replace(/,/g, " , ").replace(/_/g, " ");

        NotInString = NotIn.toString() === undefined ? undefined : NotIn.toString();
        $scope.query.advanceNotSearchText = NotInString.replace(/\(|\)/g, "").replace(/,/g, " , ").replace(/_/g, " ");
        //console.log("count=>" + count);

    }

    function extractText(str) {
        var ret = "";
        if (/"/.test(str)) {
            ret = str.match(/"(.*?)"/g);
        } else {
            ret = str;
        }
        //console.log("ret=> ::  " + ret);
        return ret;
    }


    $scope.decode = {};
    $scope.resetDecode = function() {
        $scope.decode.advanceEncodedQueryString = undefined;
        $scope.query.advanceAndQuerySearchText = undefined;
        $scope.query.advanceOrOptionalQuerySearchText = undefined;
        $scope.query.advanceOrSearchText = undefined;
        $scope.query.advanceNotSearchText = undefined;
    };

    /**********Decode query ends***********/




    //Get saved boolean queries
    $scope.getBooleanQueries = function() {
        $scope.filter.$ = "";
        var organizationId = $rootScope.userDetails.OrganizationId;
        blockUI.start();
        var promise = XSeedApiFactory.getBooleanQueries(organizationId);
        promise.then(
            function(response) {
                $scope.booleanQueryList = response[0].data;
                populateBooleanQueryList();
                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('BooleanSearch', httpError.data.Status, httpError.data.Message);
            });
    };

    //Save boolean queries
    $scope.saveBooleanQueries = function(saveBooleanQueryForm) {
        if (new ValidationService().checkFormValidity(saveBooleanQueryForm)) {
            var organizationId = $rootScope.userDetails.OrganizationId;
            blockUI.start();
            if ($scope.saveBooleanQueryModel) {
                $scope.saveBooleanQueryModel.organizationId = organizationId;
                var promise = XSeedApiFactory.saveBooleanQueries($scope.saveBooleanQueryModel);
                promise.then(
                    function(response) {
                        $scope.saveBooleanQueryModel = {};
                        blockUI.stop();
                        XSeedAlert.swal({
                            title: 'Success!',
                            text: 'Query saved successfully!',
                            type: "success",
                            customClass: "xseed-error-alert",
                        });
                        $('.modal').modal('hide');
                    },
                    function(httpError) {
                        blockUI.stop();
                        ExceptionHandler.handlerHTTPException('BooleanSearch', httpError.data.Status, httpError.data.Message);
                    });
            }
        }
    };

    //Close boolean query modal
    $scope.closeSaveBooleanQueryModal = function() {
        $scope.saveBooleanQueryModel = {};
    };

    //Populate Boolean Query List
    function populateBooleanQueryList() {
        if (angular.isDefined($scope.tblBooleanQueryList)) {
            $scope.tblBooleanQueryList.reload();
        } else {
            $scope.tblBooleanQueryList = new ngTableParams({
                page: 1,
                //count: $scope.companyList.length,
                count: 6,
                sorting: {}
            }, {
                counts: [],
                total: $scope.booleanQueryList.length,
                getData: function($defer, params) {
                    var filteredData = $filter('filter')($scope.booleanQueryList, $scope.filter);
                    $scope.dataBooleanQueryList = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
                    params.total($scope.dataBooleanQueryList.length);
                    $scope.dataBooleanQueryList = $scope.dataBooleanQueryList.slice((params.page() - 1) * params.count(), params.page() * params.count());

                    $defer.resolve($scope.dataBooleanQueryList);
                },
                $scope: $scope
            });
        }
    };

    $scope.generateQueryGoogletag = function(googleTagFileTypeText, googleTagInTitle, googleTagInURL, googleTagSkillText, googleTagLocation, googleTagCountryText) {

        var result = [];
        var fileType, InTtitle, InURL, Skills, Location, Country = "";


        
        var fileTypeFormat = "";
        if (googleTagFileTypeText) {
            for (var i = 0; i < googleTagFileTypeText.length; i++) {
                // fileTypeFormat += "filetype:" + googleTagFileTypeText[i] + " ";

                if (i + 1 == googleTagFileTypeText.length) {
                    fileTypeFormat += "filetype:" + googleTagFileTypeText[i];
                } else {
                    fileTypeFormat += "filetype:" + googleTagFileTypeText[i] + " OR ";
                }

            }
        }
        //console.log(fileTypeFormat);

        
        var InTtitleFormat = "";
        if (googleTagInTitle) {
            for (var i = 0; i < googleTagInTitle.length; i++) {
                if (i + 1 == googleTagInTitle.length) {
                    InTtitleFormat += "intitle:" + googleTagInTitle[i];
                } else {
                    InTtitleFormat += "intitle:" + googleTagInTitle[i] + " OR ";
                }
            }
        }
        //console.log(InTtitleFormat);
        
        
        var InURLFormat = "";
        if (googleTagInURL) {
            for (var i = 0; i < googleTagInURL.length; i++) {

                if (i + 1 == googleTagInURL.length) {
                    InURLFormat += "inurl:" + googleTagInURL[i];
                } else {
                    InURLFormat += "inurl:" + googleTagInURL[i] + " OR ";
                }
            }
        }
        //console.log(InURLFormat);

        Skills = googleTagSkillText.join(" ");
        Country = googleTagCountryText.join(" ");

        //Location = googleTagLocation.replace(/,/g, " ");

        if (googleTagLocation) {
            Location = googleTagLocation.split(",");
        }

      
        var LocationFormat = "";
        if (Location) {
        for (var i = 0; i < Location.length; i++) {
            LocationFormat += '"' + Location[i] + '" ';

        }
    }


        if (fileTypeFormat || InTtitleFormat || InURLFormat || Skills || LocationFormat || Country) {
            $scope.queryStringGoogletag = fileTypeFormat + " (" + InTtitleFormat + " OR " + InURLFormat + ") " + Skills + " " + LocationFormat + " " + Country;
        }
        else {
            XSeedAlert.swal({
                title: 'Warning!',
                text: 'Please select at least one field.',
                type: "warning",
                customClass: "xseed-error-alert",
            });
        }
    }
    $scope.resetGoogleQueryText = function() {
        $scope.queryStringGoogletag = undefined;
    }

};