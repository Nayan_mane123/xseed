'use strict';

XSEED_APP.controller('ReportsCtrl', ReportsCtrl);

/**
 * @ngInject
 */
function ReportsCtrl($scope, $rootScope, $route, $routeParams, RequestContext, $timeout, $q, $location, $window, configuration, i18nFactory, ExceptionHandler, EventBus, $log, _, ipCookie, blockUI, ModalFactory, XSeedAlert, localStorageService, $uibModal, XSeedApiFactory, ngTableParams, $filter, ValidationService) {

    init();
    $scope.reportCandidateAdded = {};
    $scope.reportRequirementTracker = {};
    $scope.reportSubmission = {};
    var activityReportActivitinoteModal = ModalFactory.activityReportActivitinoteModal($scope);

    function init() {
     
        $scope.reportFilterList = [
            { Id: 'W', Name: 'Weekly' },
            { Id: 'M', Name: 'Monthly' },
            { Id: 'Q', Name: 'Quarterly' },
            { Id: 'HY', Name: 'Half Yearly' },
            { Id: 'Y', Name: 'Yearly'},
            { Id: 'D', Name: 'Daily'}
        ];
        $scope.common.advanceSearchModel = {}
        $scope.common.advanceSearchModel.filter = {}
        $scope.common.advanceSearchModel.filter.days = $scope.reportFilterList[0].Id;
        $scope.common.advanceSearchModel.filter.graphtype = $scope.reportFilterList[0].Id;
        //Init Qnote to appen Lod more
        $scope.activityList = [];
        $scope.CandidateActivityList=[];
        //Set paginate
        $scope.common.ativityPageNo = 0;
        $scope.common.listDataMode = true;
    };

    $scope.resetFilter = function() {
        $scope.isSafariBrowser = false;
        $scope.common.advanceSearchModel.filter.days = $scope.reportFilterList[0].Id;
        $scope.common.advanceSearchModel.filter.graphtype = $scope.reportFilterList[0].Id;

    };
    /* Candidate Tracker */

    $scope.getCandidateReport = function() {
        if( $scope.common.listDataMode == false){
            return;
        }
        $scope.common.listDataMode = false;
        blockUI.start();
        var firstPageFlag = 0;
        $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
        $scope.common.pageNumberShow = undefined;

        var promise = XSeedApiFactory.getCandidateAddedReport($rootScope.userDetails.OrganizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, undefined, undefined, $scope.common.advanceSearchModel.filter.days);
        promise.then(
            function(response) {
                $scope.candidateAddedList = response[0].data.ReportData;
                $scope.common.totalCount = response[0].data.TotalCount;
                $scope.common.totalPages = response[0].data.TotalPages;

                firstPageFlag = 1;
                populateCandidateList($scope.common.totalCount, $scope.common.totalPages, firstPageFlag);

                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
            });
    };

    function populateCandidateList(totalCount, totalPages, pageFlag) {
        //if (angular.isDefined($scope.candidateAddedTableParams)) {
        //    $scope.candidateAddedTableParams.reload();
        //}
        //else 
        {
            $scope.candidateAddedTableParams = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: [],
                total: totalCount,
                getData: function($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    /* sorting */
                    if (pageFlag != 1) {
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function(val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */

                        var promise = XSeedApiFactory.getCandidateAddedReport($rootScope.userDetails.OrganizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder, $scope.common.advanceSearchModel.filter.days);
                        promise.then(
                            function(response) {
                                $scope.candidateAddedList = response[0].data.ReportData;
                                $scope.common.totalCount = response[0].data.TotalCount;
                                $scope.common.totalPages = response[0].data.TotalPages;

                                blockUI.stop();

                            },
                            function(httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
                            });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.candidateAddedList);
                },
                $scope: $scope
            });
        }
    }

    $scope.getExportToExcelDataCandidateReport = function() {
        var isExport = true;
        $scope.isSafariBrowser = false;
        //Check Browser
        $scope.BrowserDetection();
        // If browser if not Safari then allow user to download excel file.
        if ($rootScope.currentBrowserName != 'safari') {
            $scope.isSafariBrowser = false;
        } else if ($rootScope.currentBrowserName == 'safari') { //// If browser if Safari then don't allow user to download excel file.
            $scope.isSafariBrowser = true;
        }

        blockUI.start();
        /* sorting */
        var sort, sortorder;
        if ($scope.common.sortBy) {
            angular.forEach($scope.common.sortBy, function(val, index) {
                sort = index;
                sortorder = val;
            });
        }
        /* sorting */
        var organizationId = $rootScope.userDetails.OrganizationId;
        if ($scope.flag.advanceSearchFlag) {
            var promise = XSeedApiFactory.getCandidateAddedReport(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder, $scope.common.advanceSearchModel.filter.days, isExport);
            promise.then(
                function(response) {
                    $scope.exportToExcelData = response[0].data.ReportData;
                    if ($scope.exportToExcelData.length > 0) {
                        buildCandidateReport();
                    }
                },
                function(httpError) {
                    ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
                });
        } else {
            var promise = XSeedApiFactory.getCandidateAddedReport(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder, $scope.common.advanceSearchModel.filter.days, isExport);
            promise.then(
                function(response) {
                    $scope.exportToExcelData = response[0].data.ReportData;
                    if ($scope.exportToExcelData.length > 0) {
                        buildCandidateReport();
                    }
                },
                function(httpError) {
                    ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
                });
        }
        blockUI.stop();
    };

    function buildCandidateReport() {
        blockUI.start();
        //header in report

        $scope.candidateReportHeader = { Recruiter: 'Recruiter', CreatedOn: 'Created On', ManuallyCreatedCount: 'Local', MonsterCount: 'Monster', DiceCount: 'Dice', Total: 'Total' };

        //structured data in report
        $scope.candidateReportData = [];
        angular.forEach($scope.exportToExcelData, function(val, index) {
            $scope.candidateReportData[index] = {};
            $scope.candidateReportData[index].Recruiter = $scope.exportToExcelData[index].Recruiter;
            $scope.candidateReportData[index].CreatedOn = $scope.exportToExcelData[index].CreatedOn;
            $scope.candidateReportData[index].ManuallyCreatedCount = $scope.exportToExcelData[index].ManuallyCreatedCount;
            $scope.candidateReportData[index].MonsterCount = $scope.exportToExcelData[index].MonsterCount;
            $scope.candidateReportData[index].DiceCount = $scope.exportToExcelData[index].DiceCount;
            $scope.candidateReportData[index].Total = $scope.exportToExcelData[index].Total.toString();
        });
        if ($scope.candidateReportData.length > 0) {
            saveToExcel('CandidateAddedReport.xlsx', 'Candidate Added Report', $scope.candidateReportData, $scope.candidateReportHeader);
        }
        blockUI.stop();
    };

    /* Candidate Tracker */

    $scope.getCandidateGraphReport = function() {
        if ($rootScope.userHasToken == false) {
            $location.path('login');
            return false;
        }
        if (($location.path() == '/reports' || $location.path() == '/dashboard') && $rootScope.userDetails.Reports_Read == "False") {
            $location.path('dashboard');
            return false;
        }

        blockUI.start();
        var promise = XSeedApiFactory.getCandidateGraphReport($rootScope.userDetails.OrganizationId, $scope.common.advanceSearchModel.filter.graphtype);
        promise.then(
            function(response) {
                console.log(response);
                $scope.graphData = response[0].data;
                var months = [];
                var counts = [];
                angular.forEach($scope.graphData, function(key, value) {
                    months.push(key.Label);
                    counts.push(key.Count);
                });

                if (window.myLine) {
                    window.myLine.destroy()
                }

                var config1 = {
                    type: 'bar',
                    data: {
                        labels: months,
                        datasets: [{
                            label: 'Candidates',
                            borderColor: "#7fc35c",
                            backgroundColor: "#7fc35c",
                            data: counts
                        }]
                    },
                    options: {
                        responsive: true,
                        title: {
                            display: false,

                        },
                        tooltips: {
                            mode: 'index',
                        },
                        hover: {
                            mode: 'index'
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }],
                            xAxes: [{
                                ticks: {
                                    autoSkip: false
                                }
                            }]
                        }

                    }
                };

                var ctx1 = document.getElementById('canvas').getContext('2d');
                window.myLine = new Chart(ctx1, config1);

                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
            });
    }

    /* Requirement Tracker */

    $scope.getRequirementTrackerGraphReport = function () {
        if ($rootScope.userHasToken == false) {
            $location.path('login');
            return false;
        }
        if (($location.path() == '/reports' || $location.path() == '/dashboard') && $rootScope.userDetails.Reports_Read == "False") {
            $location.path('dashboard');
            return false;
        }
        blockUI.start();
        var promise = XSeedApiFactory.getRequirementTrackerGraphReport($rootScope.userDetails.OrganizationId, $scope.common.advanceSearchModel.filter.graphtype);
        promise.then(
          function (response) {
              //console.log(response);
              $scope.graphData = response[0].data;
              var months = [];
              var counts = [];
              angular.forEach($scope.graphData, function (key, value) {
                  months.push(key.Label);
                  counts.push(key.Count);
              });

                if (window.myLine) {
                    window.myLine.destroy()
                }

                var config1 = {
                    type: 'bar',
                    data: {
                        labels: months,
                        datasets: [{
                            label: 'Requirements',
                            borderColor: "#7fc35c",
                            backgroundColor: "#7fc35c",
                            data: counts
                        }]
                    },
                    options: {
                        responsive: true,
                        title: {
                            display: false,

                        },
                        tooltips: {
                            mode: 'index',
                        },
                        hover: {
                            mode: 'index'
                        }

                    }
                };

                var ctx1 = document.getElementById('canvas').getContext('2d');
                window.myLine = new Chart(ctx1, config1);

                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
            });
    }

    $scope.getRequirementTrackerReport = function() {

        if( $scope.common.listDataMode == false){
            return;
        }
        $scope.common.listDataMode = false;

        if ($rootScope.userHasToken == false) {
            $location.path('login');
            return false;
        }
        if (($location.path() == '/reports' || $location.path() == '/dashboard') && $rootScope.userDetails.Reports_Read == "False") {
            $location.path('dashboard');
            return false;
        }

        blockUI.start();
        var firstPageFlag = 0;
        $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
        $scope.common.pageNumberShow = undefined;
        console.log($scope.common.advanceSearchModel.filter.days);
        var promise = XSeedApiFactory.getRequirementTrackerReport($rootScope.userDetails.OrganizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, undefined, undefined, $scope.common.advanceSearchModel.filter.days);
        promise.then(
            function(response) {
                $scope.requirementTrackerList = response[0].data.ReportData;
                $scope.common.totalCount = response[0].data.TotalCount;
                $scope.common.totalPages = response[0].data.TotalPages;
                if ($scope.requirementTrackerList.length) {
                    $scope.requirementTrackerColumnsCheckboxes = {
                        date: true,
                        title: true,
                        status: true,
                        company: true,
                        companyContact: true,
                        internalSubmissions: true
                    };
                }

                firstPageFlag = 1;
                populateRequirementTracker($scope.common.totalCount, $scope.common.totalPages, firstPageFlag);

                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
            });
    };


   

    function populateRequirementTracker(totalCount, totalPages, pageFlag) {
        //if (angular.isDefined($scope.requirementTrackerTableParams)) {
        //    $scope.requirementTrackerTableParams.reload();
        //}
        //else 
        {
            $scope.requirementTrackerTableParams = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: [],
                total: totalCount,
                getData: function($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();
                    /* sorting */
                    if (pageFlag != 1) {
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function(val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */

                        var promise = XSeedApiFactory.getRequirementTrackerReport($rootScope.userDetails.OrganizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder, $scope.common.advanceSearchModel.filter.days);
                        promise.then(
                            function(response) {
                                $scope.requirementTrackerList = response[0].data.ReportData;
                                $scope.common.totalCount = response[0].data.TotalCount;
                                $scope.common.totalPages = response[0].data.TotalPages;

                                blockUI.stop();

                            },
                            function(httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
                            });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.requirementTrackerList);
                },
                $scope: $scope
            });
        }
    }

    $scope.getExportToExcelDataRequirementTrackerReport = function() {
        var isExport = true;
        $scope.isSafariBrowser = false;
        $scope.BrowserDetection();
        if ($rootScope.currentBrowserName != 'safari') {
            $scope.isSafariBrowser = false;
        } else if ($rootScope.currentBrowserName == 'safari') {
            $scope.isSafariBrowser = true;
        }

        blockUI.start();
        /* sorting */
        var sort, sortorder;
        if ($scope.common.sortBy) {
            angular.forEach($scope.common.sortBy, function(val, index) {
                sort = index;
                sortorder = val;
            });
        }
        /* sorting */
        if ($scope.flag.advanceSearchFlag) {
            var promise = XSeedApiFactory.getRequirementAdvanceSearchResult($scope.common.advanceSearchModel, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder, $scope.common.advanceSearchModel.filter.days, isExport);
            promise.then(
                function(response) {
                    $scope.exportToExcelData = response[0].data.ReportData;
                    if ($scope.exportToExcelData.length > 0) {
                        buildRequiementTrackerReport();
                    }
                },
                function(httpError) {
                    ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
                });
        } else {
            var organizationId = $rootScope.userDetails.OrganizationId;
            var promise = XSeedApiFactory.getRequirementTrackerReport(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder, $scope.common.advanceSearchModel.filter.days, isExport);
            promise.then(
                function(response) {
                    $scope.exportToExcelData = response[0].data.ReportData;
                    if ($scope.exportToExcelData.length > 0) {
                        buildRequiementTrackerReport();
                    }
                },
                function(httpError) {
                    ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
                });
        }
        blockUI.stop();

    };

    function buildRequiementTrackerReport() {
        blockUI.start();
        //header in report

        $scope.requiementTrackerReportHeader = {};
        if ($scope.requirementTrackerColumnsCheckboxes.date) {
            $scope.requiementTrackerReportHeader.RequirementDate = "Requirement Date";
        }
        if ($scope.requirementTrackerColumnsCheckboxes.title) {
            $scope.requiementTrackerReportHeader.JobTitle = "Requirement Title";
        }
        if ($scope.requirementTrackerColumnsCheckboxes.status) {
            $scope.requiementTrackerReportHeader.JobType = "Requirement Type";
        }
        if ($scope.requirementTrackerColumnsCheckboxes.company) {
            $scope.requiementTrackerReportHeader.Company = "Company";
        }
        if ($scope.requirementTrackerColumnsCheckboxes.companyContact) {
            $scope.requiementTrackerReportHeader.CompanyContact = "Company Contact";
        }
        if ($scope.requirementTrackerColumnsCheckboxes.internalSubmissions) {
            $scope.requiementTrackerReportHeader.InternalSubmissions = "Internal Submissions";
        }

        //structured data in report
        $scope.requiementTrackerReportData = [];
        angular.forEach($scope.exportToExcelData, function(val, index) {
            $scope.requiementTrackerReportData[index] = {};
            $scope.requiementTrackerReportData[index].RequirementDate = $scope.exportToExcelData[index].RequirementDate;
            $scope.requiementTrackerReportData[index].JobTitle = $scope.exportToExcelData[index].JobTitle;
            $scope.requiementTrackerReportData[index].JobType = $scope.exportToExcelData[index].Status;
            $scope.requiementTrackerReportData[index].Company = $scope.exportToExcelData[index].Company;
            $scope.requiementTrackerReportData[index].CompanyContact = $scope.exportToExcelData[index].CompanyContact;
            $scope.requiementTrackerReportData[index].InternalSubmissions = $scope.exportToExcelData[index].InternalSubmissions.toString();
        });
        if ($scope.requiementTrackerReportData.length > 0) {
            saveToExcel('RequirementTrackerReport.xlsx', 'Requirement Tracker Report', $scope.requiementTrackerReportData, $scope.requiementTrackerReportHeader);
        }
        blockUI.stop();
    };

    /* Requirement Tracker */

    /* Submission Tracker */

    $scope.getSubmissionTrackerReport = function() {
        if( $scope.common.listDataMode == false){
            return;
        }
        $scope.common.listDataMode = false;
        blockUI.start();
        var firstPageFlag = 0;
        $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
        $scope.common.pageNumberShow = undefined;

        var promise = XSeedApiFactory.getSubmissionTrackerReport($rootScope.userDetails.OrganizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, undefined, undefined, $scope.common.advanceSearchModel.filter.days);
        promise.then(
            function(response) {
                $scope.submissionTrackerList = response[0].data.ReportData;
                $scope.common.totalCount = response[0].data.TotalCount;
                $scope.common.totalPages = response[0].data.TotalPages;

                firstPageFlag = 1;
                populateSubmissionTracker($scope.common.totalCount, $scope.common.totalPages, firstPageFlag);
                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
            });
    };


    /* Submission Tracker Graph */

    $scope.getSubmissionTrackerGraphReport = function () {
        if ($rootScope.userHasToken == false) {
            $location.path('login');
            return false;
        }
        if (($location.path() == '/reports' || $location.path() == '/dashboard') && $rootScope.userDetails.Reports_Read == "False") {
            $location.path('dashboard');
            return false;
        }
        blockUI.start();
        var promise = XSeedApiFactory.getSubmissionTrackerGraphReport($rootScope.userDetails.OrganizationId, $scope.common.advanceSearchModel.filter.graphtype);
        promise.then(
          function (response) {
              console.log(response);
              $scope.graphData = response[0].data;
              var months = [];
              var counts = [];
              angular.forEach($scope.graphData, function (key, value) {
                  months.push(key.Label);
                  counts.push(key.Count);
              });

              if (window.myLine) {
                  window.myLine.destroy()
              }

              var config1 = {
                  type: 'bar',
                  data: {
                      labels: months,
                      datasets: [{
                          label: 'Submissions',
                          borderColor: "#7fc35c",
                          backgroundColor: "#7fc35c",
                          data: counts
                      }]
                  },
                  options: {
                      responsive: true,
                      title: {
                          display: false,

                      },
                      tooltips: {
                          mode: 'index',
                      },
                      hover: {
                          mode: 'index'
                      }

                  }
              };

              var ctx1 = document.getElementById('canvas').getContext('2d');
              window.myLine = new Chart(ctx1, config1);

              blockUI.stop();
          },
            function (httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
            });
    }

    /* Client Graph */

    $scope.getClientTrackerGraphReport = function () {
        if ($rootScope.userHasToken == false) {
            $location.path('login');
            return false;
        }
        if (($location.path() == '/reports' || $location.path() == '/dashboard') && $rootScope.userDetails.Reports_Read == "False") {
            $location.path('dashboard');
            return false;
        }
        blockUI.start();
        var promise = XSeedApiFactory.getClientTrackerGraphReport($rootScope.userDetails.OrganizationId, $scope.common.advanceSearchModel.filter.graphtype);
        promise.then(
          function (response) {
              console.log(response);
              $scope.graphData = response[0].data;
              var months = [];
              var counts = [];
              angular.forEach($scope.graphData, function (key, value) {
                  months.push(key.Label);
                  counts.push(key.Count);
              });

              if (window.myLine) {
                  window.myLine.destroy()
              }

              var config1 = {
                  type: 'bar',
                  data: {
                      labels: months,
                      datasets: [{
                          label: 'Client(s)',
                          borderColor: "#7fc35c",
                          backgroundColor: "#7fc35c",
                          data: counts
                      }]
                  },
                  options: {
                      responsive: true,
                      title: {
                          display: false,

                      },
                      tooltips: {
                          mode: 'index',
                      },
                      hover: {
                          mode: 'index'
                      }

                  }
              };

              var ctx1 = document.getElementById('canvas').getContext('2d');
              window.myLine = new Chart(ctx1, config1);

              blockUI.stop();
          },
            function (httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
            });
    }


    /* Closure Graph */

    $scope.getClosureTrackerGraphReport = function () {
        if ($rootScope.userHasToken == false) {
            $location.path('login');
            return false;
        }
        if (($location.path() == '/reports' || $location.path() == '/dashboard') && $rootScope.userDetails.Reports_Read == "False") {
            $location.path('dashboard');
            return false;
        }
        blockUI.start();
        var promise = XSeedApiFactory.getClosureTrackerGraphReport($rootScope.userDetails.OrganizationId, $scope.common.advanceSearchModel.filter.graphtype);
        promise.then(
          function (response) {
              //console.log(response);
              $scope.graphData = response[0].data;
              var months = [];
              var counts = [];
              angular.forEach($scope.graphData, function (key, value) {
                  months.push(key.Label);
                  counts.push(key.Count);
              });

              if (window.myLine) {
                  window.myLine.destroy()
              }

              var config1 = {
                  type: 'bar',
                  data: {
                      labels: months,
                      datasets: [{
                          label: 'Closure',
                          borderColor: "#7fc35c",
                          backgroundColor: "#7fc35c",
                          data: counts
                      }]
                  },
                  options: {
                      responsive: true,
                      title: {
                          display: false,

                      },
                      tooltips: {
                          mode: 'index',
                      },
                      hover: {
                          mode: 'index'
                      }

                  }
              };

              var ctx1 = document.getElementById('canvas').getContext('2d');
              window.myLine = new Chart(ctx1, config1);

              blockUI.stop();
          },
            function (httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
            });
    }


    $scope.getSubmissionAllCounts=function()
    {
        var promise = XSeedApiFactory.getSubmissionAllCounts($rootScope.userDetails.OrganizationId);
        promise.then(
            function (response) {
                //console.log(response[0].data);
                $scope.RightSideSubmissionCounts = response[0].data.SubmissionAllCount;
                blockUI.stop();
            },
            function (httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
            });

    }

    $scope.getClosureAllCounts=function()
    {
        var promise = XSeedApiFactory.getClosureAllCounts($rootScope.userDetails.OrganizationId);
        promise.then(
            function (response) {

                //console.log(response[0].data);
                $scope.RightSideColsureCounts = response[0].data.ClosureAllCount;
                blockUI.stop();
            },
            function (httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
            });

    }

    $scope.getClientAllCounts=function()
    {
        var promise = XSeedApiFactory.getClientAllCounts($rootScope.userDetails.OrganizationId);
        promise.then(
            function (response) {

                console.log(response[0].data);
                $scope.RightSideClientCounts = response[0].data.ClientAllCount;
                blockUI.stop();
            },
            function (httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
            });

    }

    $scope.getRequirementAllCounts = function () {
        var promise = XSeedApiFactory.getRequirementAllCounts($rootScope.userDetails.OrganizationId);
        promise.then(
            function (response) {
                console.log(response[0].data);
                $scope.RightSideRequirementCounts = response[0].data.RequirementAllCount;
                blockUI.stop();
            },
            function (httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
            });

    }

    function populateSubmissionTracker(totalCount, totalPages, pageFlag) {
        //if (angular.isDefined($scope.submissionTrackerTableParams)) {
        //    $scope.submissionTrackerTableParams.reload();
        //}
        //else 
        {
            $scope.submissionTrackerTableParams = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: [],
                total: totalCount,
                getData: function($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    /* sorting */
                    if (pageFlag != 1) {
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function(val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */

                        var promise = XSeedApiFactory.getSubmissionTrackerReport($rootScope.userDetails.OrganizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder, $scope.common.advanceSearchModel.filter.days);
                        promise.then(
                            function(response) {
                                $scope.submissionTrackerList = response[0].data.ReportData;
                                $scope.common.totalCount = response[0].data.TotalCount;
                                $scope.common.totalPages = response[0].data.TotalPages;

                                blockUI.stop();

                            },
                            function(httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
                            });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.submissionTrackerList);
                },
                $scope: $scope
            });
        }
    }

    $scope.getExportToExcelDataSubmissionTrackerReport = function() {
        var isExport = true;
        $scope.isSafariBrowser = false;
        $scope.BrowserDetection();
        if ($rootScope.currentBrowserName != 'safari') {
            $scope.isSafariBrowser = false;
        } else if ($rootScope.currentBrowserName == 'safari') {
            $scope.isSafariBrowser = true;
        }

        blockUI.start();
        /* sorting */
        var sort, sortorder;
        if ($scope.common.sortBy) {
            angular.forEach($scope.common.sortBy, function(val, index) {
                sort = index;
                sortorder = val;
            });
        }
        /* sorting */
        var organizationId = $rootScope.userDetails.OrganizationId;
        if ($scope.flag.advanceSearchFlag) {
            var promise = XSeedApiFactory.getSubmissionTrackerReport(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder, $scope.common.advanceSearchModel.filter.days, isExport);
            promise.then(
                function(response) {
                    $scope.exportToExcelData = response[0].data.ReportData;
                    if ($scope.exportToExcelData.length > 0) {
                        buildSubmissionTrackerReport();
                    }
                },
                function(httpError) {
                    ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
                });
        } else {
            var promise = XSeedApiFactory.getSubmissionTrackerReport(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder, $scope.common.advanceSearchModel.filter.days, isExport);
            promise.then(
                function(response) {
                    $scope.exportToExcelData = response[0].data.ReportData;
                    if ($scope.exportToExcelData.length > 0) {
                        buildSubmissionTrackerReport();
                    }
                },
                function(httpError) {
                    ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
                });
        }
        blockUI.stop();
    };

    function buildSubmissionTrackerReport() {
        blockUI.start();
        //header in report

        $scope.submissionTrackerReportHeader = { RequirementDate: 'Requirement Date', SubmissionDate: 'Submission Date', Recruiter: 'Recruiter', Company: 'Company', Candidate: 'Candidate', RequirementTitle: 'Requirement Title', RequirementStatus: 'Requirement Status', Remark: 'Remark' };

        //structured data in report
        $scope.submissionTrackerReportData = [];
        angular.forEach($scope.exportToExcelData, function(val, index) {
            $scope.submissionTrackerReportData[index] = {};
            $scope.submissionTrackerReportData[index].RequirementDate = $scope.exportToExcelData[index].RequirementDate;
            $scope.submissionTrackerReportData[index].SubmissionDate = $scope.exportToExcelData[index].SubmissionDate;
            $scope.submissionTrackerReportData[index].Recruiter = $scope.exportToExcelData[index].Recruiter;
            $scope.submissionTrackerReportData[index].Company = $scope.exportToExcelData[index].Company;
            $scope.submissionTrackerReportData[index].Candidate = $scope.exportToExcelData[index].Candidate;
            $scope.submissionTrackerReportData[index].RequirementTitle = $scope.exportToExcelData[index].JobTitle;
            $scope.submissionTrackerReportData[index].RequirementStatus = $scope.exportToExcelData[index].Status;
            $scope.submissionTrackerReportData[index].Remark = $scope.exportToExcelData[index].Remark;
        });
        if ($scope.submissionTrackerReportData.length > 0) {
            saveToExcel('SubmissionTrackerReport.xlsx', 'Submission Tracker Report', $scope.submissionTrackerReportData, $scope.submissionTrackerReportHeader);
        }
        blockUI.stop();
    };

    /* Submission Tracker */

    /* Client Tracker */

    $scope.getClientLeadReport = function() {
        if( $scope.common.listDataMode == false){
            return;
        }
        $scope.common.listDataMode = false;
        blockUI.start();
        var firstPageFlag = 0;
        
        $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
        $scope.common.pageNumberShow = undefined;

        var promise = XSeedApiFactory.getClientLeadReport($rootScope.userDetails.OrganizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, undefined, undefined, $scope.common.advanceSearchModel.filter.days);
        promise.then(
            function (response) {
                console.log(response);
                $scope.clientLeadList = response[0].data.ReportData;
                $scope.common.totalCount = response[0].data.TotalCount;
                $scope.common.totalPages = response[0].data.TotalPages;

                firstPageFlag = 1;
                populateClientLeadList($scope.common.totalCount, $scope.common.totalPages, firstPageFlag);
                blockUI.stop();

                //populate filters
                $timeout(function() {
                    $scope.populateSubmissionTracker();

                }, 2000);
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
            });
    };

    function populateClientLeadList(totalCount, totalPages, pageFlag) {
        //if (angular.isDefined($scope.clientLeadTableParams)) {
        //    $scope.clientLeadTableParams.reload();
        //}
        //else 
        {
            $scope.clientLeadTableParams = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: [],
                total: totalCount,
                getData: function($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    /* sorting */
                    if (pageFlag != 1) {
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function(val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */
                        console.log("called");
                        var promise = XSeedApiFactory.getClientLeadReport($rootScope.userDetails.OrganizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder, $scope.common.advanceSearchModel.filter.days);
                        promise.then(
                            function(response) {
                                $scope.clientLeadList = response[0].data.ReportData;
                                $scope.common.totalCount = response[0].data.TotalCount;
                                $scope.common.totalPages = response[0].data.TotalPages;

                                blockUI.stop();

                            },
                            function(httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
                            });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.clientLeadList);
                },
                $scope: $scope
            });
        }
    }

    $scope.getExportToExcelDataClientLeadReport = function() {
        var isExport = true;
        $scope.isSafariBrowser = false;
        $scope.BrowserDetection();
        if ($rootScope.currentBrowserName != 'safari') {
            $scope.isSafariBrowser = false;
        } else if ($rootScope.currentBrowserName == 'safari') {
            $scope.isSafariBrowser = true;
        }

        blockUI.start();
        /* sorting */
        var sort, sortorder;
        if ($scope.common.sortBy) {
            angular.forEach($scope.common.sortBy, function(val, index) {
                sort = index;
                sortorder = val;
            });
        }
        /* sorting */
        var organizationId = $rootScope.userDetails.OrganizationId;
        if ($scope.flag.advanceSearchFlag) {
            var promise = XSeedApiFactory.getClientLeadReport(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder, $scope.common.advanceSearchModel.filter.days, isExport);
            promise.then(
                function(response) {
                    $scope.exportToExcelData = response[0].data.ReportData;
                    if ($scope.exportToExcelData.length > 0) {
                        buildClientLeadReport();
                    }
                },
                function(httpError) {
                    ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
                });
        } else {
            var promise = XSeedApiFactory.getClientLeadReport(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder, $scope.common.advanceSearchModel.filter.days, isExport);
            promise.then(
                function(response) {
                    $scope.exportToExcelData = response[0].data.ReportData;
                    if ($scope.exportToExcelData.length > 0) {
                        buildClientLeadReport();
                    }
                },
                function(httpError) {
                    ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
                });
        }
        blockUI.stop();

    };

    function buildClientLeadReport() {
        blockUI.start();
        //header in report

        $scope.clientLeadReportHeader = { SourceName: 'Source Name', CompanytName: 'Company', CompanyType: 'Company Type', ViaWebsite: 'Via', JobTitle: 'Requirement Title', Positions: 'Positions', RequirementDate: 'Requirement Date', Submissions: 'Submissions', JobStatus: 'Status' };

        //structured data in report
        $scope.clientLeadReportData = [];
        angular.forEach($scope.exportToExcelData, function(val, index) {
            $scope.clientLeadReportData[index] = {};
            $scope.clientLeadReportData[index].SourceName = $scope.exportToExcelData[index].SourceName;
            $scope.clientLeadReportData[index].CompanytName = $scope.exportToExcelData[index].CompanytName;
            $scope.clientLeadReportData[index].CompanyType = $scope.exportToExcelData[index].CompanyType;
            $scope.clientLeadReportData[index].ViaWebsite = $scope.exportToExcelData[index].ViaWebsite;
            $scope.clientLeadReportData[index].JobTitle = $scope.exportToExcelData[index].JobTitle;
            $scope.clientLeadReportData[index].Positions = $scope.exportToExcelData[index].Positions != null ? $scope.exportToExcelData[index].Positions.toString() : "";
            $scope.clientLeadReportData[index].RequirementDate = $scope.exportToExcelData[index].RequirementDate;
            $scope.clientLeadReportData[index].Submissions = $scope.exportToExcelData[index].Submissions;
            $scope.clientLeadReportData[index].JobStatus = $scope.exportToExcelData[index].JobStatus;
        });
        if ($scope.clientLeadReportData.length > 0) {
            saveToExcel('ClientLeadReport.xlsx', 'Client Lead Report', $scope.clientLeadReportData, $scope.clientLeadReportHeader);
        }
        blockUI.stop();
    };

    /* Client Tracker */



    /* Activity Tracker */

    $scope.getActivityTrackerGraphReport = function () {
        if ($rootScope.userHasToken == false) {
            $location.path('login');
            return false;
        }
        if (($location.path() == '/reports' || $location.path() == '/dashboard') && $rootScope.userDetails.Reports_Read == "False") {
            $location.path('dashboard');
            return false;
        }
        blockUI.start();
        var promise = XSeedApiFactory.getActivityTrackerGraphReport($rootScope.userDetails.OrganizationId, $scope.common.advanceSearchModel.filter.graphtype);
        promise.then(
          function (response) {
              
              $scope.graphData = response[0].data;
              var users = [];
              var rcounts = [];
              var scounts = [];
              var cccounts = [];
              var ccounts = [];
              var sfccounts = [];
              var sfcounts = [];
              angular.forEach($scope.graphData, function (key, value) {
                  users.push(key.OrganizationUser);
                  rcounts.push(key.RequirementCount);
                  scounts.push(key.SubmissionCount);
                  cccounts.push(key.CompanyContactCount);
                  ccounts.push(key.CompanyCount);
                  sfccounts.push(key.SubmissionFeedbackClosedCount);
                  sfcounts.push(key.SubmissionFeedbackCount);
              });

              if (window.myLine) {
                  window.myLine.destroy()
              }

                  //......activity bar chart start

                  var barChartData = {
                      labels: users,
                      datasets: [{
                          label: 'Requirements',
                          backgroundColor: window.chartColors.red,
                          data: rcounts
                      }, {
                          label: 'Submissions',
                          backgroundColor: window.chartColors.blue,
                          data: scounts
                      }, {
                          label: 'Company Contact Count',
                          backgroundColor: '#4bc0c0',
                          data: cccounts
                      }, {
                          label: 'Company Count',
                          backgroundColor: '#ffcd56',
                          data: ccounts
                      }, {
                          label: 'Submission Feedback Closed Count',
                          backgroundColor: '#ff9f40',
                          data: sfccounts
                      }, {
                          label: 'Submission Feedback Count',
                          backgroundColor: '#9966ff',
                          data: sfcounts
                      }]

                  };

                  var ctx = document.getElementById('canvas').getContext('2d');
                  window.myBar = new Chart(ctx, {
                      type: 'bar',
                      data: barChartData,
                      options: {
                          tooltips: {
                              mode: 'index',
                              intersect: false
                          },
                          responsive: true,
                          scales: {
                              xAxes: [{
                                  stacked: true,
                              }],
                              yAxes: [{
                                  stacked: true
                              }]
                          }
                      }
                  });

                  //......activity bar chart ends

              blockUI.stop();
          },
            function (httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
            });
    }




    /* Activity Tracker */

    //$scope.GetActivityGraphData = function() {

    //    //blockUI.start();


    //    // var promise = XSeedApiFactory.getActivityReportGraphData($rootScope.userDetails.OrganizationId, $scope.common.advanceSearchModel.filter.days);
    //    // promise.then(
    //    //     function(response) {
    //    //$scope.activityGraphData = response[0].data;

    //    //......activity bar chart start

    //    var barChartData = {
    //        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    //        datasets: [{
    //            label: 'Dataset 1',
    //            backgroundColor: window.chartColors.red,
    //            data: [
    //                randomScalingFactor(),
    //                randomScalingFactor(),
    //                randomScalingFactor(),
    //                randomScalingFactor(),
    //                randomScalingFactor(),
    //                randomScalingFactor(),
    //                randomScalingFactor()
    //            ]
    //        }, {
    //            label: 'Dataset 2',
    //            backgroundColor: window.chartColors.blue,
    //            data: [
    //                randomScalingFactor(),
    //                randomScalingFactor(),
    //                randomScalingFactor(),
    //                randomScalingFactor(),
    //                randomScalingFactor(),
    //                randomScalingFactor(),
    //                randomScalingFactor()
    //            ]
    //        }, {
    //            label: 'Dataset 3',
    //            backgroundColor: window.chartColors.green,
    //            data: [
    //                randomScalingFactor(),
    //                randomScalingFactor(),
    //                randomScalingFactor(),
    //                randomScalingFactor(),
    //                randomScalingFactor(),
    //                randomScalingFactor(),
    //                randomScalingFactor()
    //            ]
    //        }]

    //    };

    //    var ctx = document.getElementById('canvas').getContext('2d');
    //    window.myBar = new Chart(ctx, {
    //        type: 'bar',
    //        data: barChartData,
    //        options: {
    //            tooltips: {
    //                mode: 'index',
    //                intersect: false
    //            },
    //            responsive: true,
    //            scales: {
    //                xAxes: [{
    //                    stacked: true,
    //                }],
    //                yAxes: [{
    //                    stacked: true
    //                }]
    //            }
    //        }
    //    });

    //    //......activity bar chart ends

    //    //     blockUI.stop();
    //    // },
    //    // function(httpError) {
    //    //     blockUI.stop();
    //    //     ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
    //    // });





    //}

    $scope.getActivityReport = function() {
        if( $scope.common.listDataMode == false){
            return;
        }
        $scope.common.listDataMode = false;
        blockUI.start();
        var firstPageFlag = 0;
        $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
        $scope.common.pageNumberShow = undefined;


        var promise = XSeedApiFactory.getActivityReport($rootScope.userDetails.OrganizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, undefined, undefined, $scope.common.advanceSearchModel.filter.days);
        promise.then(
            function (response) {
                console.log(response);
                $scope.activityReportList = response[0].data.ReportData;
                $scope.common.totalCount = response[0].data.TotalCount;
                $scope.common.totalPages = response[0].data.TotalPages;

                firstPageFlag = 1;
                populateActivityReportList($scope.common.totalCount, $scope.common.totalPages, firstPageFlag);

                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
            });
    };

    function populateActivityReportList(totalCount, totalPages, pageFlag) {
        //if (angular.isDefined($scope.activityReportTableParams)) {
        //    $scope.activityReportTableParams.reload();
        //}
        //else 
        {
            $scope.activityReportTableParams = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: [],
                total: totalCount,
                getData: function($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    /* sorting */
                    if (pageFlag != 1) {
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function(val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */

                        var promise = XSeedApiFactory.getActivityReport($rootScope.userDetails.OrganizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder, $scope.common.advanceSearchModel.filter.days);
                        promise.then(
                            function(response) {
                                $scope.activityReportList = response[0].data.ReportData;
                                $scope.common.totalCount = response[0].data.TotalCount;
                                $scope.common.totalPages = response[0].data.TotalPages;

                                blockUI.stop();

                            },
                            function(httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
                            });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.activityReportList);
                },
                $scope: $scope
            });
        }
    }

    $scope.getExportToExcelDataActivityReport = function() {
        var isExport = true;
        $scope.isSafariBrowser = false;
        $scope.BrowserDetection();
        if ($rootScope.currentBrowserName != 'safari') {
            $scope.isSafariBrowser = false;
        } else if ($rootScope.currentBrowserName == 'safari') {
            $scope.isSafariBrowser = true;
        }

        blockUI.start();
        /* sorting */
        var sort, sortorder;
        if ($scope.common.sortBy) {
            angular.forEach($scope.common.sortBy, function(val, index) {
                sort = index;
                sortorder = val;
            });
        }
        /* sorting */
        var organizationId = $rootScope.userDetails.OrganizationId;
        if ($scope.flag.advanceSearchFlag) {
            var promise = XSeedApiFactory.getActivityReport(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder, $scope.common.advanceSearchModel.filter.days, isExport);
            promise.then(
                function(response) {
                    $scope.exportToExcelData = response[0].data.ReportData;
                    if ($scope.exportToExcelData.length > 0) {
                        buildActivityReport();
                    }
                },
                function(httpError) {
                    ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
                });
        } else {
            var promise = XSeedApiFactory.getActivityReport(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder, $scope.common.advanceSearchModel.filter.days, isExport);
            promise.then(
                function(response) {
                    $scope.exportToExcelData = response[0].data.ReportData;
                    if ($scope.exportToExcelData.length > 0) {
                        buildActivityReport();
                    }
                },
                function(httpError) {
                    ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
                });
        }
        blockUI.stop();
    };

    function buildActivityReport() {
        blockUI.start();
        //header in report

        $scope.activityReportHeader = { OrganizationUser: 'User', CompanyCount: 'Company Contact', CompanyContactCount: 'Recruiter', RequirementCount: 'Requirement', CandidateCount: 'Candidate', SubmissionCount: 'Submitted', SubmissionFeedbackCount: 'Feedback', SubmissionFeedbackClosedCount: 'Placements', QNoteActivityCount: 'Note' };

        //structured data in report
        $scope.activityReportData = [];
        angular.forEach($scope.exportToExcelData, function(val, index) {
            $scope.activityReportData[index] = {};
            $scope.activityReportData[index].OrganizationUser = $scope.exportToExcelData[index].OrganizationUser;
            $scope.activityReportData[index].CompanyCount = $scope.exportToExcelData[index].CompanyCount;
            $scope.activityReportData[index].CompanyContactCount = $scope.exportToExcelData[index].CompanyContactCount;
            $scope.activityReportData[index].RequirementCount = $scope.exportToExcelData[index].RequirementCount;
            $scope.activityReportData[index].CandidateCount = $scope.exportToExcelData[index].CandidateCount;
            $scope.activityReportData[index].SubmissionCount = $scope.exportToExcelData[index].SubmissionCount.toString();
            $scope.activityReportData[index].SubmissionFeedbackCount = $scope.exportToExcelData[index].SubmissionFeedbackCount;
            $scope.activityReportData[index].SubmissionFeedbackClosedCount = $scope.exportToExcelData[index].SubmissionFeedbackClosedCount;
            $scope.activityReportData[index].QNoteActivityCount = $scope.exportToExcelData[index].QNoteActivityCount;
        });
        if ($scope.activityReportData.length > 0) {
            saveToExcel('ActivityReport.xlsx', 'Activity Report', $scope.activityReportData, $scope.activityReportHeader);
        }
        blockUI.stop();
    };

    /* Activity Tracker */


    $scope.OpenActivityNoteModal = function (SelectedOrganizationUserId,SelectedOrganizationUser) {
        //alert('Start');
        blockUI.start();
        //Reset prev Page No
        $scope.common.ativityPageNo=0;
        //Append data to model
        $scope.GetActivityNotesData(SelectedOrganizationUserId);

        $scope.SelectedOrganizationUser=SelectedOrganizationUser;

        //Bind the model
        activityReportActivitinoteModal.$promise.then(activityReportActivitinoteModal.show);

    }

    $scope.GetActivityNotesData = function (SelectedOrganizationUserId, pageLenght = 10) {
    //function GetActivityNotesData(SelectedOrganizationUserId, pageLenght=10)
        //Get activityDetails
        $scope.LoadMoreMode=true;
        $scope.SelectedOrganizationUserId = SelectedOrganizationUserId;

        //var promise = XSeedApiFactory.getActivityReportActivityNote($scope.SelectedOrganizationUserId, 
        var promise = XSeedApiFactory.getActivityReportActivityNote(SelectedOrganizationUserId, $scope.common.advanceSearchModel.filter.days,$scope.common.ativityPageNo, pageLenght);
        promise.then(
            function (response) {
                console.log(response[0].data.ActivityData);
                if(response[0].data.ActivityData.length==0)
                {
                    $scope.LoadMoreMode=false;
                }
                //Append the data to list for Load more purpose
                if(response[0].data)
                    $scope.common.ativityPageNo +=1;
                //$scope.activityList = response[0].data.ActivityData;
                //angular.extend($scope.activityList, response[0].data.ActivityData);
                
                if(response[0].data.ActivityData.length>0)
                {
                    for(var j=0;j<response[0].data.ActivityData.length;j++)
                    {
                        $scope.activityList.push(response[0].data.ActivityData[j]);
                    }
                }
                if(response[0].data.ActivityDataCanididate.length>0)
                {
                    for(var j=0;j<response[0].data.ActivityDataCanididate.length;j++)
                    {
                        $scope.CandidateActivityList.push(response[0].data.ActivityDataCanididate[j]);
                    }
                }
                
                console.log($scope.activityList);
                //console.log('ativity List PageNo'+$scope.common.ativityPageNo);
                //console.log($scope.activityList);
                
                blockUI.stop();
            },
            function (httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
            });
    }
    $scope.closeActivityNoteModal = function() {
        $scope.activityList = [];
        $scope.CandidateActivityList=[];
        activityReportActivitinoteModal.$promise.then(activityReportActivitinoteModal.hide);
    }

    /* Submission Tracker */

    $scope.getClosureReport = function() {
        if( $scope.common.listDataMode == false){
            return;
        }
        $scope.common.listDataMode = false;
        blockUI.start();
        var firstPageFlag = 0;
        $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
        $scope.common.pageNumberShow = undefined;

        var promise = XSeedApiFactory.getClosureReport($rootScope.userDetails.OrganizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, undefined, undefined, $scope.common.advanceSearchModel.filter.days);
        promise.then(
            function (response) {
                //console.log(response);
                $scope.closureList = response[0].data.ReportData;
                $scope.common.totalCount = response[0].data.TotalCount;
                $scope.common.totalPages = response[0].data.TotalPages;

                firstPageFlag = 1;
                populateClosureReport($scope.common.totalCount, $scope.common.totalPages, firstPageFlag);
                blockUI.stop();
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
            });
    };

    function populateClosureReport(totalCount, totalPages, pageFlag) {
        console.log(totalCount+" "+ totalPages+" "+pageFlag);
        //if (angular.isDefined($scope.submissionTrackerTableParams)) {
        //    $scope.submissionTrackerTableParams.reload();
        //}
        //else 
        {
            
            $scope.closureTableParams = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: [],
                total: totalCount,
                getData: function ($defer, params) {
                    var sort, sortorder;
                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    /* sorting */
                    if (pageFlag != 1) {
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function (val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */

                        var promise = XSeedApiFactory.getClosureReport($rootScope.userDetails.OrganizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, undefined, undefined, $scope.common.advanceSearchModel.filter.days);
                        promise.then(
                            function (response) {
                                $scope.closureList = response[0].data.ReportData;
                                $scope.common.totalCount = response[0].data.TotalCount;
                                $scope.common.totalPages = response[0].data.TotalPages;


                                blockUI.stop();

                            },
                            function (httpError) {
                                blockUI.stop();
                                ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
                            });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.closureList);
                },
                $scope: $scope
            });
        }
    }

    $scope.getExportToExcelDataClosureReport = function() {
        var isExport = true;
        $scope.isSafariBrowser = false;
        $scope.BrowserDetection();
        if ($rootScope.currentBrowserName != 'safari') {
            $scope.isSafariBrowser = false;
        } else if ($rootScope.currentBrowserName == 'safari') {
            $scope.isSafariBrowser = true;
        }

        blockUI.start();
        /* sorting */
        var sort, sortorder;
        if ($scope.common.sortBy) {
            angular.forEach($scope.common.sortBy, function(val, index) {
                sort = index;
                sortorder = val;
            });
        }
        /* sorting */
        var organizationId = $rootScope.userDetails.OrganizationId;
        if ($scope.flag.advanceSearchFlag) {
            var promise = XSeedApiFactory.getClosureReport(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder, $scope.common.advanceSearchModel.filter.days, isExport);
            promise.then(
                function(response) {
                    $scope.exportToExcelData = response[0].data.ReportData;
                    if ($scope.exportToExcelData.length > 0) {
                        buildClosureTrackerReport();
                    }
                },
                function(httpError) {
                    ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
                });
        } else {
            var promise = XSeedApiFactory.getClosureReport(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder, $scope.common.advanceSearchModel.filter.days, isExport);
            promise.then(
                function(response) {
                    $scope.exportToExcelData = response[0].data.ReportData;
                    if ($scope.exportToExcelData.length > 0) {
                        buildClosureTrackerReport();
                    }
                },
                function(httpError) {
                    ExceptionHandler.handlerHTTPException('Report', httpError.data.Status, httpError.data.Message);
                });
        }
        blockUI.stop();
    };

    function buildClosureTrackerReport() {
        blockUI.start();
        //header in report

        $scope.closureReportHeader = { RequirementDate: 'Requirement Date', SubmissionDate: 'Submission Date', Recruiter: 'Recruiter', Company: 'Company', Candidate: 'Candidate', RequirementTitle: 'Requirement Title', RequirementStatus: 'Requirement Status', SubmissionStatus: 'Submission Status', Remark: 'Remark' };

        //structured data in report
        $scope.closureReportData = [];
        angular.forEach($scope.exportToExcelData, function(val, index) {
            $scope.closureReportData[index] = {};
            $scope.closureReportData[index].RequirementDate = $scope.exportToExcelData[index].RequirementDate;
            $scope.closureReportData[index].SubmissionDate = $scope.exportToExcelData[index].SubmissionDate;
            $scope.closureReportData[index].Recruiter = $scope.exportToExcelData[index].Recruiter;
            $scope.closureReportData[index].Company = $scope.exportToExcelData[index].Company;
            $scope.closureReportData[index].Candidate = $scope.exportToExcelData[index].Candidate;
            $scope.closureReportData[index].RequirementTitle = $scope.exportToExcelData[index].JobTitle;
            $scope.closureReportData[index].RequirementStatus = $scope.exportToExcelData[index].Status;
            $scope.closureReportData[index].SubmissionStatus = $scope.exportToExcelData[index].SubmissionStatus;
            $scope.closureReportData[index].Remark = $scope.exportToExcelData[index].Remark;
        });
        if ($scope.closureReportData.length > 0) {
            saveToExcel('ClosureReport.xlsx', 'Closure Report', $scope.closureReportData, $scope.closureReportHeader);
        }
        blockUI.stop();
    };

    /* Submission Tracker */

    function saveToExcel(fileName, sheetName, data, headerMapping) {
        blockUI.start();

        ExportExcel.savetoexcel(fileName, sheetName, data, headerMapping);

        blockUI.stop();
    };

    function jobPageSecondaryLookupList() {
        var organizationId = $rootScope.userDetails.OrganizationId;
        var promise = XSeedApiFactory.jobPageSecondaryLookupCall(organizationId);
        promise.then(
            function(response) {
                $scope.lookup.company = response[0].data;
                $scope.lookup.organizationUser = response[1].data;
            },
            function(httpError) {
                ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
            });
    };

    //***************************************************************************************//

    // Get the render context local to this controller (and relevant params).
    var renderContext = RequestContext.getRenderContext("standard.reports");
    // The subview indicates which view is going to be rendered on the page.
    $scope.subview = renderContext.getNextSection();
    $scope.$on(
        "requestContextChanged",
        function() {
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {
                return;
            }
            // Update the view that is being rendered.
            $scope.subview = renderContext.getNextSection();
        }
    );
};
