'use strict';

XSEED_APP.controller( 'chatModalInstanceCtrl', chatModalInstanceCtrl );

/**
 * @ngInject
 */
function chatModalInstanceCtrl( $scope, $log, $uibModalInstance) {
   
    var vm = this;
    vm.ok = function() {
        $uibModalInstance.close();
    };
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
    vm.openMessage = function() {
        var messageModalInstance = $uibModal.open({
            templateUrl: 'conversationModal.html',
            controller: 'chatModalInstanceCtrl',
            controllerAs: 'conversation',
            windowTopClass: 'chat-message'
        });
    };

};

