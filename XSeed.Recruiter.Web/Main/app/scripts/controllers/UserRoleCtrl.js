﻿'use strict';

XSEED_APP.controller('UserRoleCtrl', UserRoleCtrl);

/**
 * @ngInject
 */
function UserRoleCtrl($scope, $rootScope, $route, $routeParams, RequestContext, $timeout, $q, $location, $window, configuration, i18nFactory, ExceptionHandler, EventBus, $log, _, ipCookie, blockUI, ModalFactory, XSeedAlert, localStorageService, $uibModal, XSeedApiFactory, ngTableParams, $filter, ValidationService) {

    init();

    //Initialization Function
    function init() {
        //if (angular.isUndefined(localStorageService.get("userRoleList"))) {
        //    $rootScope.userRoleList = [];
        //}
        //else {
        //    $rootScope.userRoleList = localStorageService.get("userRoleList");
        //}
    };


    ////////////////------------------------------Form Functionality----------------------------------

    $scope.getUserRoles = function() {
        if ($rootScope.userHasToken == false) {
            $location.path('login');
            return false;
        }

        var userRoleURL = '/userRoles';
        var dashboardURL = '/dashboard';

        if (($location.path().toLowerCase() == userRoleURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.RoleName != 'Application Administrator') {
            $location.path('dashboard');
            return false;
        }

        if ($rootScope.userDetails.OrganizationId) {
            blockUI.start();
            var promise = XSeedApiFactory.getUserRoles($rootScope.userDetails.OrganizationId);
            promise.then(
                function(response) {
                    blockUI.stop();
                    //$scope.masterUserRoleListCopy = angular.copy(response[0].data);
                    $scope.userRoleList = response[0].data;
                },
                function(httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('User', httpError.data.Status, httpError.data.Message);
                });
        }
    }

    $scope.newUserRole = {
        //"Id": null,
        "OrganizationId": $rootScope.userDetails.OrganizationId,
        "Role": null,
        "Description": null,
        "Precedence": null,
        "PermissionList": null,
        "AreaMapping": [{
                //"Id": null,
                "Name": "Organization",
                "Create": false,
                "Update": false,
                "Read": true,
                "Delete": false
            },
            {
                //"Id": null,
                "Name": "Company",
                "Create": false,
                "Update": false,
                "Read": true,
                "Delete": false
            },
            {
                //"Id": null,
                "Name": "Candidate",
                "Create": false,
                "Update": false,
                "Read": true,
                "Delete": false
            },
            {
                //"Id": null,
                "Name": "Requirement",
                "Create": false,
                "Update": false,
                "Read": true,
                "Delete": false
            },
            {
                //"Id": null,
                "Name": "Submission",
                "Create": false,
                "Update": false,
                "Read": true,
                "Delete": false
            },
            {
                //"Id": null,
                "Name": "Reports",
                "Create": false,
                "Update": false,
                "Read": true,
                "Delete": false
            },
            {
                //"Id": null,
                "Name": "Vendor",
                "Create": false,
                "Update": false,
                "Read": false,
                "Delete": false
            }
        ]
    };

    
    $scope.EditUserRoleCancel = function () {
        $location.path('userRoles');
        $scope.getUserRoles();
    }
  
    $scope.createUserRole = function(form) {

        if (new ValidationService().checkFormValidity(form)) {

            blockUI.start();

            var atLeastOnePermissionExist = false;
            angular.forEach($scope.newUserRole.AreaMapping, function(areaMapping, index) {
                if (!atLeastOnePermissionExist) {
                    if (areaMapping.Create == true || areaMapping.Update == true || areaMapping.Read == true || areaMapping.Delete == true) {
                        atLeastOnePermissionExist = true;
                    }
                }
            });

            if (atLeastOnePermissionExist) {
                var promise = XSeedApiFactory.createNewOrganizationUserRole($scope.newUserRole);
                promise.then(
                    function(response) {
                        blockUI.stop();

                        XSeedAlert.swal({
                            title: 'Success!',
                            text: 'New user role created successfully!',
                            type: "success",
                            customClass: "xseed-error-alert",
                            showCancelButton: false,
                            //confirmButtonText: "OK",
                            //closeOnConfirm: true
                        }).then(function() {
                            $scope.newOrganizationUserDetails = {};
                            $timeout(function() {
                                $location.path('userRoles');
                            }, 0);
                        }, function(dismiss) {
                            //console.log('dismiss: ' + dismiss);
                            // dismiss can be 'cancel', 'overlay', 'close', 'timer'
                            //if (dismiss == 'cancel'){alert('Close fired');}
                        });

                    },
                    function(httpError) {
                        blockUI.stop();
                        ExceptionHandler.handlerHTTPException('User', httpError.data.Status, httpError.data.Message);
                    });
            } else {
                blockUI.stop();

                XSeedAlert.swal({
                    title: 'Information!',
                    text: 'Please select atleast one permission.',
                    type: "info",
                });
            }
        }
    }

    $scope.deleteUserRole = function(roleId) {

        blockUI.start();
        var promise = XSeedApiFactory.deleteUserRole(roleId);
        promise.then(
            function(response) {
                blockUI.stop();
                //$scope.productResponse = response[0].data;

                XSeedAlert.swal({
                    title: 'Success!',
                    text: 'User role deleted successfully!',
                    type: "success",
                    customClass: "xseed-error-alert",
                    showCancelButton: false,
                    //confirmButtonText: "OK",
                    //closeOnConfirm: true
                }).then(function() {
                    $timeout(function() {
                        //$location.path('userRoles');
                        //$route.reload();
                        $scope.getUserRoles();
                    }, 0);
                }, function(dismiss) {
                    //console.log('dismiss: ' + dismiss);
                    // dismiss can be 'cancel', 'overlay', 'close', 'timer'
                    //if (dismiss == 'cancel'){alert('Close fired');}
                });
            },
            function(httpError) {
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('User', httpError.data.Status, httpError.data.Message);
            });
    };

    $scope.clearAll = function($event, selectedUserRole, selectedAreaMapping) {
        if ($event.target.checked === false) {
            angular.forEach($scope.userRoleList, function(userRole) {
                if (userRole == selectedUserRole) {
                    angular.forEach(userRole.AreaMapping, function(areaMapping) {
                        if (areaMapping == selectedAreaMapping) {
                            areaMapping.Create = false;
                            areaMapping.Update = false;
                            areaMapping.Read = false;
                            areaMapping.Delete = false;
                        }
                    });
                }
            });
        }
    };

    $scope.checkReadPermission = function($event, selectedUserRole, selectedAreaMapping) {
        if ($event.target.checked === true) {
            angular.forEach($scope.userRoleList, function(userRole) {
                if (userRole == selectedUserRole) {
                    angular.forEach(userRole.AreaMapping, function(areaMapping) {
                        if (areaMapping == selectedAreaMapping) {
                            if (areaMapping.Read == false) {
                                areaMapping.Read = true;
                            }
                        }
                    });
                }
            });
        }
    };

    $scope.updateUserRole = function(userRole) {

        var atLeastOnePermissionExist = false;
        angular.forEach(userRole.AreaMapping, function(areaMapping, index) {
            if (!atLeastOnePermissionExist) {
                if (areaMapping.Create == true || areaMapping.Update == true || areaMapping.Read == true || areaMapping.Delete == true) {
                    atLeastOnePermissionExist = true;
                }
            }
        });

        if (atLeastOnePermissionExist) {
            XSeedAlert.swal({
                title: "Are you sure?",
                text: "This action will update permissions for all the users associated with this role!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Update",
                cancelButtonText: "Cancel",
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function() {
                blockUI.start();
                var promise = XSeedApiFactory.updateUserRole(userRole);
                promise.then(
                    function(response) {
                        blockUI.stop();

                        XSeedAlert.swal({
                            title: 'Success!',
                            text: 'User role updated successfully!',
                            type: "success",
                            customClass: "xseed-error-alert",
                            showCancelButton: false,
                        }).then(function() {
                            $timeout(function() {
                                $scope.getUserRoles();
                            }, 0);
                        }, function(dismiss) {});
                    },
                    function(httpError) {
                        blockUI.stop();
                        ExceptionHandler.handlerHTTPException('User', httpError.data.Status, httpError.data.Message);
                    });

            }, function(dismiss) {});
        } else {
            XSeedAlert.swal({
                title: 'Information!',
                text: 'Please select atleast one permission.',
                type: "info",
            });
        }
    };

    $scope.cancelUserRoleUpdate = function() {
        $scope.getUserRoles();
        //$scope.userRoleList = $scope.masterUserRoleListCopy;
        //$scope.userRolesTable.reload();
    }

    //if (angular.isUndefined($rootScope.userRoleList) || $rootScope.userRoleList == null)
    //{
    //    $scope.userRoleList = [
    //        {
    //            "RoleId": 1,
    //            "RoleName": "Administrator",
    //            "RoleCode": "ADMIN",
    //            "RoleDescrition": "System Administrator",
    //            "PrecedenceLevel": 1,
    //            "AreaMappings": [
    //              {
    //                  "Id": 1,
    //                  "Name": "Organization",
    //                  "Permissions": {
    //                      "Create": true,
    //                      "Update": true,
    //                      "Read": true,
    //                      "Delete": true
    //                  }
    //              },
    //              {
    //                  "Id": 2,
    //                  "Name": "Company",
    //                  "Permissions": {
    //                      "Create": true,
    //                      "Update": true,
    //                      "Read": true,
    //                      "Delete": true
    //                  }
    //              },
    //              {
    //                  "Id": 3,
    //                  "Name": "Candidate",
    //                  "Permissions": {
    //                      "Create": true,
    //                      "Update": true,
    //                      "Read": true,
    //                      "Delete": true
    //                  }
    //              },
    //              {
    //                  "Id": 4,
    //                  "Name": "Requirement",
    //                  "Permissions": {
    //                      "Create": true,
    //                      "Update": true,
    //                      "Read": true,
    //                      "Delete": true
    //                  }
    //              },
    //              {
    //                  "Id": 5,
    //                  "Name": "Submission",
    //                  "Permissions": {
    //                      "Create": true,
    //                      "Update": true,
    //                      "Read": true,
    //                      "Delete": true
    //                  }
    //              },
    //              {
    //                  "Id": 6,
    //                  "Name": "Reports",
    //                  "Permissions": {
    //                      "Create": true,
    //                      "Update": true,
    //                      "Read": true,
    //                      "Delete": true
    //                  }
    //              }
    //            ]
    //        },

    //        {
    //            "RoleId": 2,
    //            "RoleName": "Delivery Manager",
    //            "RoleCode": "DM",
    //            "RoleDescrition": "Delivery Manager",
    //            "PrecedenceLevel": 2,
    //            "AreaMappings": [
    //              {
    //                  "Id": 1,
    //                  "Name": "Organization",
    //                  "Permissions": {
    //                      "Create": false,
    //                      "Update": false,
    //                      "Read": true,
    //                      "Delete": false
    //                  }
    //              },
    //              {
    //                  "Id": 2,
    //                  "Name": "Company",
    //                  "Permissions": {
    //                      "Create": true,
    //                      "Update": true,
    //                      "Read": true,
    //                      "Delete": true
    //                  }
    //              },
    //              {
    //                  "Id": 3,
    //                  "Name": "Candidate",
    //                  "Permissions": {
    //                      "Create": true,
    //                      "Update": true,
    //                      "Read": true,
    //                      "Delete": true
    //                  }
    //              },
    //              {
    //                  "Id": 4,
    //                  "Name": "Requirement",
    //                  "Permissions": {
    //                      "Create": true,
    //                      "Update": true,
    //                      "Read": true,
    //                      "Delete": true
    //                  }
    //              },
    //              {
    //                  "Id": 5,
    //                  "Name": "Submission",
    //                  "Permissions": {
    //                      "Create": true,
    //                      "Update": true,
    //                      "Read": true,
    //                      "Delete": true
    //                  }
    //              },
    //              {
    //                  "Id": 6,
    //                  "Name": "Reports",
    //                  "Permissions": {
    //                      "Create": true,
    //                      "Update": true,
    //                      "Read": true,
    //                      "Delete": true
    //                  }
    //              }
    //            ]
    //        },

    //        {
    //            "RoleId": 3,
    //            "RoleName": "Team Lead",
    //            "RoleCode": "TL",
    //            "RoleDescrition": "Team Leader",
    //            "PrecedenceLevel": 3,
    //            "AreaMappings": [
    //              {
    //                  "Id": 1,
    //                  "Name": "Organization",
    //                  "Permissions": {
    //                      "Create": false,
    //                      "Update": false,
    //                      "Read": true,
    //                      "Delete": false
    //                  }
    //              },
    //              {
    //                  "Id": 2,
    //                  "Name": "Company",
    //                  "Permissions": {
    //                      "Create": false,
    //                      "Update": false,
    //                      "Read": true,
    //                      "Delete": false
    //                  }
    //              },
    //              {
    //                  "Id": 3,
    //                  "Name": "Candidate",
    //                  "Permissions": {
    //                      "Create": true,
    //                      "Update": true,
    //                      "Read": true,
    //                      "Delete": true
    //                  }
    //              },
    //              {
    //                  "Id": 4,
    //                  "Name": "Requirement",
    //                  "Permissions": {
    //                      "Create": true,
    //                      "Update": true,
    //                      "Read": true,
    //                      "Delete": true
    //                  }
    //              },
    //              {
    //                  "Id": 5,
    //                  "Name": "Submission",
    //                  "Permissions": {
    //                      "Create": true,
    //                      "Update": true,
    //                      "Read": true,
    //                      "Delete": true
    //                  }
    //              },
    //              {
    //                  "Id": 6,
    //                  "Name": "Reports",
    //                  "Permissions": {
    //                      "Create": true,
    //                      "Update": true,
    //                      "Read": true,
    //                      "Delete": true
    //                  }
    //              }
    //            ]
    //        },

    //        {
    //            "RoleId": 4,
    //            "RoleName": "Recruiter",
    //            "RoleCode": "RCTR",
    //            "RoleDescrition": "Recruiter",
    //            "PrecedenceLevel": 4,
    //            "AreaMappings": [
    //              {
    //                  "Id": 1,
    //                  "Name": "Organization",
    //                  "Permissions": {
    //                      "Create": false,
    //                      "Update": false,
    //                      "Read": true,
    //                      "Delete": false
    //                  }
    //              },
    //              {
    //                  "Id": 2,
    //                  "Name": "Company",
    //                  "Permissions": {
    //                      "Create": false,
    //                      "Update": false,
    //                      "Read": true,
    //                      "Delete": false
    //                  }
    //              },
    //              {
    //                  "Id": 3,
    //                  "Name": "Candidate",
    //                  "Permissions": {
    //                      "Create": true,
    //                      "Update": true,
    //                      "Read": true,
    //                      "Delete": true
    //                  }
    //              },
    //              {
    //                  "Id": 4,
    //                  "Name": "Requirement",
    //                  "Permissions": {
    //                      "Create": false,
    //                      "Update": false,
    //                      "Read": true,
    //                      "Delete": false
    //                  }
    //              },
    //              {
    //                  "Id": 5,
    //                  "Name": "Submission",
    //                  "Permissions": {
    //                      "Create": true,
    //                      "Update": true,
    //                      "Read": true,
    //                      "Delete": true
    //                  }
    //              },
    //              {
    //                  "Id": 6,
    //                  "Name": "Reports",
    //                  "Permissions": {
    //                      "Create": false,
    //                      "Update": false,
    //                      "Read": true,
    //                      "Delete": false
    //                  }
    //              }
    //            ]
    //        }
    //    ]
    //}
    //else
    //{
    //    $scope.userRoleList = $rootScope.userRoleList;
    //}     

    //$scope.newUserRole = 
    //      {
    //          "RoleId": null,
    //          "RoleName": null,
    //          "RoleCode": null,
    //          "RoleDescrition": null,
    //          "PrecedenceLevel": null,
    //          "AreaMappings": [
    //            {
    //                "Id": 1,
    //                "Name": "Organization",
    //                "Permissions": {
    //                    "Create": false,
    //                    "Update": false,
    //                    "Read": true,
    //                    "Delete": false
    //                }
    //            },
    //            {
    //                "Id": 2,
    //                "Name": "Company",
    //                "Permissions": {
    //                    "Create": false,
    //                    "Update": false,
    //                    "Read": true,
    //                    "Delete": false
    //                }
    //            },
    //            {
    //                "Id": 3,
    //                "Name": "Candidate",
    //                "Permissions": {
    //                    "Create": false,
    //                    "Update": false,
    //                    "Read": true,
    //                    "Delete": false
    //                }
    //            },
    //            {
    //                "Id": 4,
    //                "Name": "Requirement",
    //                "Permissions": {
    //                    "Create": false,
    //                    "Update": false,
    //                    "Read": true,
    //                    "Delete": false
    //                }
    //            },
    //            {
    //                "Id": 5,
    //                "Name": "Submission",
    //                "Permissions": {
    //                    "Create": false,
    //                    "Update": false,
    //                    "Read": true,
    //                    "Delete": false
    //                }
    //            },
    //            {
    //                "Id": 6,
    //                "Name": "Reports",
    //                "Permissions": {
    //                    "Create": false,
    //                    "Update": false,
    //                    "Read": true,
    //                    "Delete": false
    //                }
    //            }
    //          ]
    //      };

    //$scope.createUserRole = function (form) {

    //    if (new ValidationService().checkFormValidity(form)) {
    //        blockUI.start();
    //        $scope.userRoleList.push(angular.copy($scope.newUserRole));
    //        localStorageService.set("userRoleList", $scope.userRoleList);
    //        $location.path('userRoles');
    //        blockUI.stop();
    //    }
    //}

    //$scope.delete = function (role) {
    //    var index = 0;
    //    angular.forEach($scope.userRoleList, function (roleInfo, key) {
    //        if (angular.equals(roleInfo, role)) {
    //            index = key;
    //        }
    //    });
    //    $scope.userRoleList.splice(index, 1);
    //    localStorageService.set("userRoleList", $scope.userRoleList);
    //    $location.path('userRoles');
    //}

    $scope.gotoUserRoleList = function() {
        $location.path('userRoles');
    }

    //////////////-----------------------------------------------------------------------------------

    // Get the render context local to this controller (and relevant params).
    var renderContext = RequestContext.getRenderContext();
    // The subview indicates which view is going to be rendered on the page.
    $scope.subview = renderContext.getNextSection();
    $scope.$on(
        "requestContextChanged",
        function() {
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {
                return;
            }
            // Update the view that is being rendered.
            $scope.subview = renderContext.getNextSection();
        }
    );
};