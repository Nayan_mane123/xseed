﻿<!doctype html>
<html xmlns:ng="http://angularjs.org" id="ng-app" ng-app="xseed-app" ng-controller="ApplicationCtrl as main" translate-cloak>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="Milestone">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Xseed">
    <meta name="theme-color" content="#4C7FF0">
    <meta http-equiv="Cache-control" content="no-cache">
    <meta http-equiv="Pragma" content="no-cache" />
    <link rel="stylesheet" href="" id="load_styles_before" />
    <style>
        [ng\:cloak],
        [ng-cloak],
        [data-ng-cloak],
        [x-ng-cloak],
        .ng-cloak,
        .x-ng-cloak {
            display: none !important;
        }
        
        .translate-cloak {
            display: none !important;
        }
    </style>
    <title class="ng-cloak">XSeed - {{pageTitle}}</title>
    <link type="image/x-icon" rel="shortcut icon" href="images/xseed.ico">


    <link href="https://fonts.googleapis.com/css?family=Work+Sans:500&display=swap" rel="stylesheet">

    <link href="styles/sweetalert2.css" rel="stylesheet" />

    <link rel="stylesheet" href="styles\gsi-step-indicator.css">
    <link rel="stylesheet" href="styles\tsf-step-form-wizard.css">
    <!-- <link rel="stylesheet" href="styles\app.min.css"> -->

    <link rel="stylesheet" href="vendor\jquery.tagsinput\src\jquery.tagsinput.css">
    <link rel="stylesheet" href="vendor\intl-tel-input\build\css\intlTelInput.css">
    <link rel="stylesheet" href="vendor\bootstrap-daterangepicker\daterangepicker.css">
    <link rel="stylesheet" href="vendor\bootstrap-datepicker\dist\css\bootstrap-datepicker3.css">
    <link rel="stylesheet" href="vendor\clockpicker\dist\bootstrap-clockpicker.min.css">
    <link rel="stylesheet" href="vendor\mjolnic-bootstrap-colorpicker\dist\css\bootstrap-colorpicker.min.css">
    <link rel="stylesheet" href="vendor\bootstrap-touchspin\dist\jquery.bootstrap-touchspin.min.css">
    <link rel="stylesheet" href="vendor\jquery-labelauty\source\jquery-labelauty.css">
    <link rel="stylesheet" href="vendor\multiselect\css\multi-select.css">
    <link rel="stylesheet" href="vendor\ui-select\dist\select.css">
    <!--<link rel="stylesheet" href="vendor\select2\select2.css">-->
    <link rel="stylesheet" href="vendor\select2\select2.min.css">
    <link rel="stylesheet" href="vendor\selectize\dist\css\selectize.css">

    <link rel="stylesheet" href="vendor\summernote\dist\summernote.css">
    <link rel="stylesheet" href="styles\app.min-logocolor.css">
    <link rel="stylesheet" href="styles\dt-buttons.css">

    <link rel="stylesheet" href="vendor\star-rating\star-rating.css">



</head>

<body ng-cloak>

    <div class="wrapper">
        <div ng-switch="subview">
            <div ng-switch-when="home" ng-controller="LandingCtrl">
                <div ng-include=" 'views/home.html' "></div>
            </div>
            <div ng-switch-when="landing" ng-controller="LandingCtrl">
                <div ng-include=" 'views/templates/landing.html' "></div>
            </div>
            <div ng-switch-when="standard" ng-controller="StandardCtrl">
                <div ng-include=" 'views/templates/standard.html' "></div>
            </div>
        </div>
    </div>

    <script src="scripts\app.min.js"></script>

    <!-- bower:js -->
    <script src="bower_components/angular/angular.js"></script>

    <script src="bower_components/angular-animate/angular-animate.js"></script>
    <script src="bower_components/angular-aria/angular-aria.js"></script>
    <script src="bower_components/angular-cookies/angular-cookies.js"></script>
    <script src="bower_components/angular-resource/angular-resource.js"></script>
    <script src="bower_components/angular-route/angular-route.js"></script>
    <script src="bower_components/angular-sanitize/angular-sanitize.js"></script>
    <script src="bower_components/angular-touch/angular-touch.js"></script>
    <script src="bower_components/angular-strap/dist/angular-strap.js"></script>
    <script src="bower_components/angular-strap/dist/angular-strap.tpl.js"></script>

    <script type="text/javascript" src="vendor/sweetalert2.js"></script>


    <script src="bower_components/lodash/dist/lodash.compat.js"></script>
    <script src="bower_components/angular-block-ui/dist/angular-block-ui.js"></script>
    <script src="bower_components/angular-translate/angular-translate.js"></script>
    <script src="bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.js"></script>
    <script src="bower_components/ng-table/dist/ng-table.min.js"></script>
    <script src="bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script src="bower_components/moment/moment.js"></script>
    <script src="bower_components/angular-moment/angular-moment.js"></script>
    <script src="bower_components/moment-timezone/builds/moment-timezone-with-data-2010-2020.js"></script>
    <script src="bower_components/angular-local-storage/dist/angular-local-storage.js"></script>
    <script src="bower_components/angular-bootstrap-toggle-switch/angular-toggle-switch.js"></script>
    <script src="bower_components/angular-bootstrap/ui-bootstrap-tpls.js"></script>
    <script src="bower_components/chart.js/dist/Chart.js"></script>
    <script src="bower_components/angular-chart.js/dist/angular-chart.js"></script>
    <script src="bower_components/ng-csv/build/ng-csv.min.js"></script>
    <script src="bower_components/bluebird/js/browser/bluebird.js"></script>
    <script src="bower_components/autoNumeric/autoNumeric.js"></script>
    <script src="bower_components/angular-currency/angular-currency.js"></script>
    <!-- endbower -->







    <script src="vendor/jquery-labelauty/source/jquery-labelauty.js"></script>

    <script type="text/javascript" src="scripts\dragscroll.js"></script>

    <script src="vendor\jquery.tagsinput\src\jquery.tagsinput.js"></script>
    <script src="vendor\intl-tel-input\build\js\intlTelInput.min.js"></script>
    <script src="vendor\moment\min\moment.min.js"></script>
    <script src="vendor\bootstrap-daterangepicker\daterangepicker.js"></script>
    <script src="vendor\bootstrap-datepicker\js\bootstrap-datepicker.js"></script>
    <script src="vendor\bootstrap-timepicker\js\bootstrap-timepicker.js"></script>
    <script src="vendor\clockpicker\dist\jquery-clockpicker.min.js"></script>
    <script src="vendor\mjolnic-bootstrap-colorpicker\dist\js\bootstrap-colorpicker.min.js"></script>
    <script src="vendor\bootstrap-touchspin\dist\jquery.bootstrap-touchspin.min.js"></script>
    <!--<script src="vendor\select2\select2.js"></script>-->
    <script src="vendor\select2\select2.full.min.js"></script>


    <script src="vendor\selectize\dist\js\standalone\selectize.min.js"></script>
    <script src="vendor\jquery-labelauty\source\jquery-labelauty.js"></script>
    <script src="vendor\bootstrap-maxlength\src\bootstrap-maxlength.js"></script>
    <script src="vendor\typeahead.js\dist\typeahead.bundle.js"></script>
    <!-- <script src="scripts\typeahead.bundle.js"></script> -->
    <script src="scripts\handlebars.min.js"></script>

    <script src="vendor\multiselect\js\jquery.multi-select.js"></script>
    <script src="scripts\forms\plugins.js"></script>
    <script src="vendor\summernote\dist\summernote.js"></script>
    <script src="scripts\validation\jquery.validate.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="scripts\dragscroll.js"></script>

    <script src="vendor\parsleyjs\dist\parsley.min.js"></script>
    <script src="scripts\helpers\tsf\js\tsf-wizard-plugin.js"></script>
    <script src="vendor\star-rating\star-rating.js"></script>
	<script src="scripts\keyup-validation.js"></script>    <script src="scripts\Chart.min.js"></script>
    <script src="scripts\utils.js"></script>

    <script src="vendor\noty\js\noty\packaged\jquery.noty.packaged.min.js"></script>
    <script src="scripts\helpers\noty-defaults.js"></script>
    <script src="scripts\ui\notifications.js"></script>


    <!-- endbuild -->
    <script type="text/javascript" src="scripts/services/ServiceConfig.js"></script>
    <script type="text/javascript" src="vendor/pace.min.js"></script>
    <script type="text/javascript" src="vendor/icheck.min.js"></script>
    <script type="text/javascript" src="vendor/ng-accordian.js"></script>
    <script type="text/javascript" src="vendor/jstz.js"></script>
    <script type="text/javascript" src="vendor/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="vendor/green.inputmask4angular.js"></script>
    <script type="text/javascript" src="vendor/ng-table-export.src.js"></script>
    <script type="text/javascript" src="vendor/select.js"></script>
    <script type="text/javascript" src="vendor/angular-idle.js"></script>
    <script type="text/javascript" src="vendor/jquery.nanoscroller.js"></script>
    <script type="text/javascript" src="vendor/scrollable.js"></script>
    <script type="text/javascript" src="vendor/angular-cookie.js"></script>
    <script type="text/javascript" src="vendor/focusIf.js"></script>
    <script type="text/javascript" src="vendor/xseed-app-routing.js"></script>
    <script type="text/javascript" src="vendor/xseed-app-wizard.js"></script>
    <script type="text/javascript" src="vendor/jquery.noty.packaged.min.js"></script>
    <script type="text/javascript" src="vendor/sweetalert2.js"></script>
    <script type="text/javascript" src="vendor/angular-selector.js"></script>
    <script type="text/javascript" src="vendor/clipboard.min.js"></script>
    <script type="text/javascript" src="vendor/ngclipboard.min.js"></script>
    <script type="text/javascript" src="vendor/angular-datepicker.min.js"></script>
    <script type="text/javascript" src="vendor/mask.js"></script>
    <script type="text/javascript" src="vendor/angular-validation.min.js"></script>
    <script type="text/javascript" src="vendor/summernote.js"></script>
    <script type="text/javascript" src="vendor/angular-summernote.js"></script>
    <script type="text/javascript" src="vendor/xls.full.min.js"></script>
    <script type="text/javascript" src="vendor/xlsx.full.min.js"></script>
    <script type="text/javascript" src="vendor/FileSaver.js"></script>
    <script type="text/javascript" src="vendor/aes.js"></script>

    <!-- build:js({.tmp,app}) scripts/scripts.js -->
    <script type="text/javascript" src="scripts/App.js"></script>
    <script type="text/javascript" src="scripts/main.js"></script>
    <script type="text/javascript" src="scripts/config/AppConfig.js"></script>
    <script type="text/javascript" src="scripts/routing/RouteConfig.js"></script>
    <script type="text/javascript" src="scripts/controllers/ApplicationCtrl.js"></script>
    <script type="text/javascript" src="scripts/controllers/StandardCtrl.js"></script>
    <script type="text/javascript" src="scripts/controllers/LandingCtrl.js"></script>
    <script type="text/javascript" src="scripts/controllers/ModalCtrl.js"></script>
    <script type="text/javascript" src="scripts/controllers/chatModalInstanceCtrl.js"></script>
    <script type="text/javascript" src="scripts/controllers/CompanyCtrl.js"></script>
    <script type="text/javascript" src="scripts/controllers/RequirementCtrl.js"></script>
    <script type="text/javascript" src="scripts/controllers/CandidateCtrl.js"></script>
    <script type="text/javascript" src="scripts/controllers/QueryBuilderCtrl.js"></script>
    <script type="text/javascript" src="scripts/controllers/SubmissionCtrl.js"></script>
    <script type="text/javascript" src="scripts/controllers/OrganizationCtrl.js"></script>
    <script type="text/javascript" src="scripts/controllers/ReportsCtrl.js"></script>
    <script type="text/javascript" src="scripts/controllers/UserRoleCtrl.js"></script>
    <script type="text/javascript" src="scripts/controllers/ResumeBlasterCtrl.js"></script>
    <script type="text/javascript" src="scripts/controllers/SwitchViewCtrl.js"></script>
    <script type="text/javascript" src="scripts/controllers/DashboardCtrl.js"></script>
    <script type="text/javascript" src="scripts/controllers/SearchCtrl.js"></script>
    <script type="text/javascript" src="scripts/controllers/VendorCtrl.js"></script>
    <script type="text/javascript" src="scripts/controllers/ResumeEmailCtrl.js"></script>
    <script type="text/javascript" src="scripts/model/ApiBeans.js"></script>
    <script type="text/javascript" src="scripts/services/ExceptionHandler.js"></script>
    <script type="text/javascript" src="scripts/services/EventBus.js"></script>
    <script type="text/javascript" src="scripts/directives/AppDirectives.js"></script>
    <script type="text/javascript" src="scripts/directives/UIFactory.js"></script>
    <script type="text/javascript" src="scripts/filters/AppFilters.js"></script>
    <script type="text/javascript" src="scripts/services/ModalPopupsFactory.js"></script>
    <script type="text/javascript" src="scripts/services/i18nFactory.js"></script>
    <script type="text/javascript" src="scripts/services/xseed-ng-api.js"></script>
    <script type="text/javascript" src="scripts/services/ExportExcel.js"></script>
    <script type="text/javascript" src="scripts/dummyData.js"></script>

    <!-- endbuild -->
    <script type="text/javascript" src="vendor/payscale.js"></script>


</body>

</html>