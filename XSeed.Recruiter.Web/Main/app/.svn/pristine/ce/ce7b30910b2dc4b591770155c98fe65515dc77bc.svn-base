'use strict';

XSEED_APP.directive('sideNavigation', sideNavigation);


function sideNavigation() {
    return {
        link: function(scope, element) {
            var controller = element.parent().controller();
            element.find('a').on('click', function(e) {
                var $this = angular.element(this),
                    links = $this.parents('li'),
                    parentLink = $this.closest('li'),
                    otherLinks = angular.element('.sidebar-panel nav li').not(links),
                    subMenu = $this.next();
                if (!subMenu.hasClass('sub-menu')) {
                    controller.menu = false;
                    return;
                }
                otherLinks.removeClass('open');
                if (subMenu.is('ul') && (subMenu.height() === 0)) {
                    parentLink.addClass('open');
                } else if (subMenu.is('ul') && (subMenu.height() !== 0)) {
                    parentLink.removeClass('open');
                }
                if (subMenu.is('ul')) {
                    return false;
                }
                e.stopPropagation();
                return true;
            });
            element.find('> li > .sub-menu').each(function() {
                if (angular.element(this).find('ul.sub-menu').length > 0) {
                    angular.element(this).addClass('multi-level');
                }
            });
        }
    };
}

XSEED_APP.directive('progressBar', progressBar);

function progressBar($timeout) {
    return {
        restrict: "EA",
        scope: {
            total: '=total',
            complete: '=complete',
            barClass: '@barClass',
            completedClass: '=?'
        },
        transclude: true,
        link: function(scope, elem, attrs) {

            scope.label = attrs.label;
            scope.completeLabel = attrs.completeLabel;
            scope.showPercent = (attrs.showPercent) || false;
            scope.completedClass = (scope.completedClass) || 'progress-bar-danger';

            scope.$watch('complete', function() {

                //change style at 100%
                var progress = scope.complete / scope.total;
                if (progress >= 1) {
                    $(elem).find('.progress-bar').addClass(scope.completedClass);
                } else if (progress < 1) {
                    $(elem).find('.progress-bar').removeClass(scope.completedClass);
                }

            });

        },
        template: "<div class='progress'>" +
            "   <div class='progress-bar {{barClass}}' title='{{complete/total * 100 | number:0 }}%' style='width:{{complete/total * 100}}%;'>{{showPercent ? (complete/total*100) : complete | number:0}} {{completeLabel}}</div>" +
            "</div>"
    };
};

XSEED_APP.directive('ngEnter', ngEnter);

function ngEnter() {

    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            if (event.which === 13) {
                scope.$apply(function() {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
};

XSEED_APP.directive('icheck', icheck);

function icheck($timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function($scope, element, $attrs, ngModel) {
            return $timeout(function() {
                var value;
                value = $attrs['value'];

                $scope.$watch($attrs['ngModel'], function(newValue) {
                    $(element).iCheck('update');
                })

                return $(element).iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    radioClass: 'iradio_flat-blue'

                }).on('ifChanged', function(event) {
                    if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
                        $scope.$apply(function() {
                            return ngModel.$setViewValue(event.target.checked);
                        });
                    }
                    if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
                        return $scope.$apply(function() {
                            return ngModel.$setViewValue(value);
                        });
                    }
                });
            });
        }
    };
}

//Number Only
XSEED_APP.directive('numberOnly', function() {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function(scope, element, attrs, ngModel) {
            if (!ngModel) return;

            ngModel.$parsers.unshift(function(inputValue) {
                if (!inputValue) return;
                else {
                    var digits = inputValue.split('').filter(function(s) {
                        return (!isNaN(s) && s != ' ');
                    }).join('');
                    ngModel.$viewValue = digits;
                    ngModel.$render();
                    return digits;
                }
            });
        }
    };
});

XSEED_APP.directive('tabs', tabs);

function tabs() {
    return {
        restrict: 'E',
        transclude: true,
        scope: {},
        controller: ["$scope", function($scope) {
            var panes = $scope.panes = [];

            $scope.select = function(pane) {
                angular.forEach(panes, function(pane) {
                    pane.selected = false;
                });
                pane.selected = true;
            }

            this.addPane = function(pane) {
                if (panes.length == 0) $scope.select(pane);
                panes.push(pane);
            }
        }],
        template: '<div class="tabbable">' +
            '<ul class="nav nav-tabs">' +
            '<li ng-repeat="pane in panes" ng-class="{active:pane.selected}">' +
            '<a href="" ng-click="select(pane)">{{pane.title}}</a>' +
            '</li>' +
            '</ul>' +
            '<div class="tab-content" ng-transclude></div>' +
            '</div>',
        replace: true
    };
};

XSEED_APP.directive('pane', pane);

function pane() {
    return {
        require: '^tabs',
        restrict: 'E',
        transclude: true,
        scope: { title: '@' },
        link: function(scope, element, attrs, tabsCtrl) {
            tabsCtrl.addPane(scope);
        },
        template: '<div class="tab-pane" ng-class="{active: selected}" ng-transclude>' +
            '</div>',
        replace: true
    };
};

XSEED_APP.directive('sidenavbar', function() {

    return {
        restrict: 'A',
        link: function(scope, element) {

            var dnl = $(".dash-navbar-left"),
                dnlBtnToggle = element,
                dnlBtnCollapse = $(".dnl-btn-collapse"),
                contentWrap = $(".content-wrap"),
                contentWrapEffect = contentWrap.data("effect"),
                windowHeight = $(window).height() - 61,
                windowWidth = $(window).width() < 767;


            element.click(function() {
                if (dnl.hasClass("dnl-hide")) {
                    dnlShow();
                } else {
                    dnlHide();
                }
            });


            // Functions
            function cwShowOverflow() {
                if (windowWidth) {
                    contentWrap.css({
                        height: windowHeight,
                        overflow: 'hidden'
                    });
                    $('html, body').css('overflow', 'hidden');
                }
            }

            function cwHideOverflow() {
                if (windowWidth) {
                    contentWrap.css({
                        height: 'auto',
                        overflow: 'auto'
                    });
                    $('html, body').css('overflow', 'auto');
                }
            }

            function dnlShow() {
                dnl.addClass("dnl-show").removeClass("dnl-hide");
                contentWrap.addClass(contentWrapEffect);
                cwShowOverflow();
                dnlBtnToggle.find("span").removeClass("fa-bars").addClass("fa-arrow-left");
            }

            function dnlHide() {
                dnl.removeClass("dnl-show").addClass("dnl-hide");
                contentWrap.removeClass(contentWrapEffect);
                cwHideOverflow();
                dnlBtnToggle.find("span").removeClass("fa-arrow-left").addClass("fa-bars");
            }

            // Toggle the edge navbar left
            dnl.addClass("dnl-hide");
            dnlBtnCollapse.click(function(e) {
                e.preventDefault();
                if (dnl.hasClass("dnl-collapsed")) {
                    dnl.removeClass("dnl-collapsed");
                    contentWrap.removeClass("dnl-collapsed");
                    $(this).find(".dnl-link-icon").removeClass("fa-arrow-right").addClass("fa-arrow-left");
                } else {
                    dnl.addClass("dnl-collapsed");
                    contentWrap.addClass("dnl-collapsed");
                    $(this).find(".dnl-link-icon").removeClass("fa-arrow-left").addClass("fa-arrow-right");
                }
            });

            // Close left navbar when top navbar opens
            $('.navbar-toggle').click(function() {
                if ($(this).hasClass('collapsed')) {
                    dnlHide();
                }
            });

            // Close top navbar when left navbar opens
            dnlBtnToggle.click(function() {
                $('.navbar-toggle').addClass('collapsed');
                $('.navbar-collapse').removeClass('in');
            });

            // Code credit: https://tr.im/CZzf4
            function isMobile() {
                try { document.createEvent("TouchEvent"); return true; } catch (e) { return false; }
            }

            // Swipe the navbar
            if (isMobile() == true) {
                $(window).swipe({
                    swipeRight: function() {
                        dnlShow();
                        $('.navbar-collapse').removeClass('in');
                    },
                    swipeLeft: function() {
                        dnlHide();
                    },
                    threshold: 75
                });
            }

            // Collapse navbar on content click
            $('.content-wrap').click(function() {
                dnlHide();
            });

        }
    };



})

XSEED_APP.directive('matchModel', matchModel);

function matchModel() {
    return {
        require: 'ngModel',
        restrict: 'A',
        scope: {
            matchModel: '='
        },
        link: function(scope, elem, attrs, ctrl) {
            scope.$watch(function() {
                var modelValue = ctrl.$modelValue || ctrl.$$invalidModelValue;
                var matchModel = scope.matchModel;
                var viewModel = ctrl.$viewValue;

                if (matchModel) {
                    matchModel = matchModel.toLowerCase();
                }
                if (viewModel) {
                    viewModel = viewModel.toLowerCase();
                }
                if (modelValue) {
                    modelValue = modelValue.toLowerCase();
                }
                return (ctrl.$pristine && angular.isUndefined(modelValue)) || matchModel === modelValue || matchModel === viewModel;
            }, function(currentValue) {
                ctrl.$setValidity('matchModel', currentValue);
            });
        }
    };
}

XSEED_APP.directive('blockClipboard', blockClipboard);

function blockClipboard() {
    return {
        require: '?ngModel',
        link: function(scope, elm, attrs, ngModel) {
            if (!ngModel) {
                return;
            };


            elm.on('cut copy paste', function(e) {
                e.preventDefault();
            });
        }
    }
}

XSEED_APP.directive('sliderToggle', function() {
    return {
        restrict: 'AE',
        link: function(scope, element, attrs) {
            var target = angular.element($("[slider]"))[0];

            var slideT = scope.$eval(attrs.sliderToggle);

            scope.$watch(attrs.sliderToggle, function(v) {
                var content = target.querySelector('#slideable_content');
                //console.log('Slide Toggle -->'  +v);
                if (angular.equals(false, v)) {
                    target.style.height = '0px';
                } else {
                    content.style.border = '1px solid rgba(0,0,0,0)';
                    var y = content.clientHeight;
                    content.style.border = 0;
                    target.style.height = y + 'px';
                }
            });

        }
    }
});

XSEED_APP.directive('slider', function() {
    return {
        restrict: 'A',
        compile: function(element, attr) {
            // wrap tag
            var contents = element.html();
            element.html('<div id="slideable_content" style="margin:0 !important; padding:0 !important;clear:both!important;" >' + contents + '</div>');

            return function postLink(scope, element, attrs) {
                // default properties
                attrs.duration = (!attrs.duration) ? '1s' : attrs.duration;
                attrs.easing = (!attrs.easing) ? 'ease-in-out' : attrs.easing;
                element.css({
                    'overflow': 'hidden',
                    'height': '0px',
                    'transitionProperty': 'height',
                    'transitionDuration': attrs.duration,
                    'transitionTimingFunction': attrs.easing
                });
            };
        }
    };
});

XSEED_APP.directive('validationNoAndWord', ['$log',
    function($log) {

        return {

            restrict: 'A',
            require: 'ngModel',
            link: function(scope, ele, attrs, ctrl) {
                ele.bind('blur', function() {
                    ctrl.$validators.validationNoAndWord = function(modelValue, viewValue) {
                        return validateAnd(viewValue);
                    }
                });

                var validateAnd = function(val) {

                    var regex = /\band\b/i;
                    return !regex.test(val);
                };

            }
        }
    }
]);

XSEED_APP.directive('validationWords', ['$log',
    function($log) {

        return {

            restrict: 'A',
            require: 'ngModel',
            link: function(scope, ele, attrs, ctrl) {
                ele.bind('blur', function() {
                    ctrl.$validators.validationWords = function(modelValue, viewValue) {
                        return validateAnd(viewValue);
                    }
                });

                var validateAnd = function(val) {
                    var valid = true;
                    if (val) {
                        var nameLength = val.match(/\S+/g).length;
                        if (nameLength > 3) {
                            valid = false;
                        }
                    }
                    return valid;
                };

            }
        }
    }
]);



XSEED_APP.directive('checkFileSize', function() {
    return {
        require: '^form',
        link: function(scope, elem, attr, formCtrl) {
            $(elem).bind('change', function() {
                if (this.files[0]) {
                    scope.common.invalidProfileImgFile = false;
                    scope.common.maxProfileImgFileSize = false;
                    scope.common.minProfileImgFileSize = false;
                    var filesize = this.files[0].size;
                    scope.common.uploadFileName = this.files[0].name;
                    var ext = this.files[0].name.match(/(?:\.([^.]+))?$/)[1];
                    if (filesize > 2000000) {
                        scope.common.maxProfileImgFileSize = true;
                        if (angular.lowercase(ext) === 'jpg' || angular.lowercase(ext) === 'jpeg' || angular.lowercase(ext) === 'png') {
                            scope.common.invalidProfileImgFile = false;
                        } else {
                            scope.common.invalidProfileImgFile = true;
                        }
                    }
                    if (filesize <= 0) {
                        scope.common.minProfileImgFileSize = true;
                    } else {
                        scope.common.maxProfileImgFileSize = false;
                        if (angular.lowercase(ext) === 'jpg' || angular.lowercase(ext) === 'jpeg' || angular.lowercase(ext) === 'png') {
                            scope.common.invalidProfileImgFile = false;
                        } else {
                            scope.common.invalidProfileImgFile = true;
                        }
                    }
                }
            });
        }
    }
});


XSEED_APP.directive('checkResumeFile', function() {
    return {
        require: '^form',
        link: function(scope, elem, attr, formCtrl) {
            $(elem).bind('change', function() {
                if (this.files[0]) {
                    scope.common.invalidResumeFile = false;
                    scope.common.maxResumeFileSize = false;
                    var filesize = this.files[0].size;
                    scope.common.uploadResumeFileName = this.files[0].name;
                    var ext = this.files[0].name.match(/(?:\.([^.]+))?$/)[1];
                    if (filesize > 5000000) {
                        scope.common.maxResumeFileSize = true;
                        if (angular.lowercase(ext) === 'jpg' || angular.lowercase(ext) === 'jpeg' || angular.lowercase(ext) === 'pdf' || angular.lowercase(ext) === 'doc' || angular.lowercase(ext) === 'docx' || angular.lowercase(ext) === 'txt' || angular.lowercase(ext) === 'png') {
                            scope.common.invalidResumeFile = false;
                        } else {
                            scope.common.invalidResumeFile = true;
                        }
                    } else {
                        scope.common.maxResumeFileSize = false;
                        if (angular.lowercase(ext) === 'jpg' || angular.lowercase(ext) === 'jpeg' || angular.lowercase(ext) === 'pdf' || angular.lowercase(ext) === 'doc' || angular.lowercase(ext) === 'docx' || angular.lowercase(ext) === 'txt' || angular.lowercase(ext) === 'png') {
                            scope.common.invalidResumeFile = false;
                        } else {
                            scope.common.invalidResumeFile = true;
                        }
                    }
                }
            });
        }
    }
});




XSEED_APP.directive('autoTabTo', [function() {
    return {
        restrict: "A",
        link: function(scope, el, attrs) {
            el.bind('keyup', function(e) {
                if (this.value.length === this.maxLength) {
                    var element = document.getElementById(attrs.autoTabTo);
                    if (element)
                        element.focus();
                }
            });
        }
    }
}]);

XSEED_APP.directive('autofocus', ['$timeout', function($timeout) {
    return {
        restrict: 'A',
        link: function($scope, $element) {
            $timeout(function() {
                $element[0].focus();
            });
        }
    }
}]);

XSEED_APP.directive('uiBlurFocus', function() {
    return {
        restrict: 'AE',
        require: 'ngModel',
        link: function(scope, elem, attrs) {
            elem.bind('blur', function() {
                var field = scope.$eval(attrs.uiBlurFocus); //ctrl.blurField;
                field.$onblurInvalid = field.$invalid;
                //console.log(field.$onblurInvalid);
            });
            elem.bind('focus', function() {
                var field = scope.$eval(attrs.uiBlurFocus); //ctrl.focusField;
                field.$onblurInvalid = false;
                //console.log(field.$onblurInvalid);
            });
        }
    };
});

function menuPanel($window) {
    return {
        scope: {
            show: '=',
        },
        link: function(scope, element, attr) {
            scope.$watch('show', function(newVal, oldVal) {
                var win = angular.element($window);
                if (win.width() < 991 || angular.element('.app').hasClass('offcanvas')) {
                    if (newVal && !oldVal) {
                        angular.element('.app').addClass('offscreen move-right');
                    } else {
                        angular.element('.app').removeClass('offscreen move-left move-right');
                    }
                }
            });
        }
    };
}

XSEED_APP.directive('menuPanel', menuPanel);

XSEED_APP.directive("ngFileModel", [function() {
    return {
        scope: {
            ngFileModel: "="
        },
        link: function(scope, element, attributes) {
                // if (changeEvent.target.files[0]){
                element.bind("change", function(changeEvent) {
                    var reader = new FileReader();
                    reader.onload = function(loadEvent) {
                            scope.$apply(function() {
                                scope.ngFileModel = {
                                    lastModified: changeEvent.target.files[0].lastModified,
                                    lastModifiedDate: changeEvent.target.files[0].lastModifiedDate,
                                    name: changeEvent.target.files[0].name,
                                    size: changeEvent.target.files[0].size,
                                    type: changeEvent.target.files[0].type,
                                    data: loadEvent.target.result
                                };
                            });
                        }
                        //console.log(scope)
                    if (changeEvent.target.files[0])
                        reader.readAsDataURL(changeEvent.target.files[0]);
                });
            }
            //}
    }
}]);

XSEED_APP.directive('ngMultiFileModel', [function() {
    return {
        scope: {
            ngMultiFileModel: "="
        },
        link: function(scope, element, attributes) {
            element.bind("change", function(changeEvent) {
                scope.$apply(function() {
                    if (scope.ngMultiFileModel) {
                        //console.log(scope.ngMultiFileModel.length);
                    } else {
                        scope.ngMultiFileModel = [];
                    }
                    angular.forEach(changeEvent.target.files, function(val, index) {
                        var reader = new FileReader();
                        if (changeEvent.target.files[index]) {
                            reader.readAsDataURL(changeEvent.target.files[index]);
                        }
                        reader.onload = function(loadEvent) {
                            scope.ngMultiFileModel.push({
                                lastModified: changeEvent.target.files[index].lastModified,
                                lastModifiedDate: changeEvent.target.files[index].lastModifiedDate,
                                name: changeEvent.target.files[index].name,
                                size: changeEvent.target.files[index].size,
                                type: changeEvent.target.files[index].type,
                                data: loadEvent.target.result
                            });
                        }
                    });
                });
                //console.log(scope)
            });
        }
    }
}]);

XSEED_APP.directive('ngBulkUpload', [function() {
    return {
        scope: {
            ngBulkUpload: "="
        },
        link: function(scope, element, attributes) {
            element.bind("change", function(changeEvent) {
                scope.$apply(function() {
                    //if (scope.ngMultiFileModel) {
                    //    //console.log(scope.ngMultiFileModel.length);
                    //}
                    //else {
                    if (changeEvent.target.files.length > 0) {
                        scope.ngBulkUpload = [];
                        var totalUploadSize = 0;
                        //}
                        angular.forEach(changeEvent.target.files, function(val, index) {
                            var reader = new FileReader();
                            if (changeEvent.target.files[index]) {
                                reader.readAsDataURL(changeEvent.target.files[index]);
                                totalUploadSize = totalUploadSize + changeEvent.target.files[index].size;
                            }
                            reader.onload = function(loadEvent) {
                                    scope.ngBulkUpload.push({
                                        lastModified: changeEvent.target.files[index].lastModified,
                                        lastModifiedDate: changeEvent.target.files[index].lastModifiedDate,
                                        name: changeEvent.target.files[index].name,
                                        size: changeEvent.target.files[index].size,
                                        type: changeEvent.target.files[index].type,
                                        data: loadEvent.target.result
                                    });
                                }
                                //if (maxUploadSize >= 10000000) {
                                //console.log(maxUploadSize);
                            scope.ngBulkUpload.totalFileSize = totalUploadSize;
                            //}
                        });
                    }
                });
                //console.log(scope)
            });
        }
    }
}]);

XSEED_APP.directive('autoComplete', function($timeout) {
    return function(scope, iElement, iAttrs) {
        iElement.autocomplete({
            source: scope[iAttrs.uiItems],
            select: function() {
                $timeout(function() {
                    iElement.trigger('input');
                }, 0);
            }
        });
    };
});

XSEED_APP.directive('sglclick', ['$parse', function($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attr) {
            var fn = $parse(attr['sglclick']);
            var delay = 300,
                clicks = 0,
                timer = null;
            element.on('click', function(event) {
                clicks++; //count clicks
                if (clicks === 1) {
                    timer = setTimeout(function() {
                        scope.$apply(function() {
                            fn(scope, { $event: event });
                        });
                        clicks = 0; //after action performed, reset counter
                    }, delay);
                } else {
                    clearTimeout(timer); //prevent single-click action
                    clicks = 0; //after action performed, reset counter
                }
            });
        }
    };
}]);


XSEED_APP.directive("repeatEnd", function() {
    return {
        restrict: "A",
        link: function(scope, element, attrs) {
            if (scope.$last) {
                scope.$eval(attrs.repeatEnd);
            }
        }
    };
});

XSEED_APP.filter('shortNumber', function() {
    return function(number) {
        if (number) {
            var abs = Math.abs(number);
            if (abs >= Math.pow(10, 12)) {
                // trillion
                number = (number / Math.pow(10, 12)).toFixed(1) + "T";
            } else if (abs < Math.pow(10, 12) && abs >= Math.pow(10, 9)) {
                // billion
                number = (number / Math.pow(10, 9)).toFixed(1) + "B";
            } else if (abs < Math.pow(10, 9) && abs >= Math.pow(10, 6)) {
                // million
                number = (number / Math.pow(10, 6)).toFixed(1) + "M";
            } else if (abs < Math.pow(10, 6) && abs >= Math.pow(10, 3)) {
                // thousand
                number = (number / Math.pow(10, 3)).toFixed(1) + "K";
            }
            return number;
        }
    };
});

XSEED_APP.filter('ContactFilter', function() {
    return function(tel) {

        tel = tel.replace(/-/g, '');

        if (!tel) { return ''; }

        var value = tel.toString().trim().replace(/^\+/, '');

        if (value.match(/[^0-9]/)) {
            return tel;
        }

        var country, city, number;

        switch (value.length) {
            case 10: // +1PPP####### -> C (PPP) ###-####
                country = 1;
                city = value.slice(0, 3);
                number = value.slice(3);
                break;

            case 11: // +CPPP####### -> CCC (PP) ###-####
                country = value[0];
                city = value.slice(1, 4);
                number = value.slice(4);
                break;

            case 12: // +CCCPP####### -> CCC (PP) ###-####
                country = value.slice(0, 3);
                city = value.slice(3, 5);
                number = value.slice(5);
                break;

            default:
                return tel;
        }

        if (country == 1) {
            country = "";
        }

        number = number.slice(0, 3) + '-' + number.slice(3);

        return (country + " (" + city + ") " + number).trim();
    };
});

XSEED_APP.filter('initialsFilter', function() {
    return function(name) {
        return (name.substring(0, 1).toUpperCase());
    };
});

XSEED_APP.filter("uniqueSkillList", function thisFilter() {
    return function(input) {
        var seen = { objectNames: [] };
        return input.filter(function(obj) {
            return !seen.objectNames.includes(obj.value) &&
                seen.objectNames.push(obj.value)
        })
    }
});

XSEED_APP.filter("uniquePricingPlan", function thisFilter() {
    return function(input) {
        var seen = { objectNames: [] };
        return input.filter(function(obj) {
            return !seen.objectNames.includes(obj.Id) &&
                seen.objectNames.push(obj.Id)
        })
    }
});
XSEED_APP.filter("uniquePricingFeature", function thisFilter() {
    return function(input) {
        var seen = { objectNames: [] };
        return input.filter(function(obj) {
            return !seen.objectNames.includes(obj.FeatureId) &&
                seen.objectNames.push(obj.FeatureId)
        })
    }
});

XSEED_APP.factory('safeApply', function ($rootScope) {
    return function ($scope, fn) {
        var phase = $scope.$root.$$phase;
        if (phase == '$apply' || phase == '$digest') {
            if (fn) {
                $scope.$eval(fn);
            }
        } else {
            if (fn) {
                $scope.$apply(fn);
            } else {
                $scope.$apply();
            }
        }
    }
});