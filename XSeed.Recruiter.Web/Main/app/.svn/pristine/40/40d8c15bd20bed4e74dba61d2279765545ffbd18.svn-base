﻿<div class="row">
    <form class="form-horizontal" name="editOrgUserForm" novalidate>
        <div class="card shadow">
            <div class="card-header p-a-xs clearfix">
                <a class="btn btn-danger btn-sm small pull-right" role="button" href="#/systemUsers">
                    <i class="fa fa-list m-r-xs" aria-hidden="true"></i>List
                </a>
            </div>
            <div class="card-block clearfix">
                <fieldset class="form-group row">
                    <div class="col-sm-12">
                        <h6 class="h6 font-weight-medium m-b-1"><i class="fa fa-tasks m-r-xs" aria-hidden="true"></i>Roles & Responsibilities</h6>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6 m-b-xs">
                        <label class="col-sm-3">Select User Role <span class="bold text-red">*</span></label>
                        <div class="col-sm-9">
                            <select tabindex="1"
                                    name="userRole"
                                    id="userRole"
                                    ui-select2="{allowClear: true}"
                                    data-placeholder="User Role"
                                    ng-model="common.currentOrganizationUserDetails.RoleId"
                                    validation="required"
                                    ng-disabled="common.currentOrganizationUserDetails.RoleName =='Application Administrator'"
                                    ng-click="getReportingAuthoritiesByRole(common.currentOrganizationUserDetails.RoleId)">
                                <option ng-repeat="userRole in userRoles" value="{{userRole.Id}}"
                                        ng-bind="userRole.Name"></option>
                            </select>
                            <div ng-if="userDetails.RoleName == 'Application Administrator'">
                                <a href="#/userRoles" style="color:blue">View Role Permissions</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6 m-b-xs">
                        <label class="col-sm-3">Reporting to</label>
                        <div class="col-sm-9">
                            <select tabindex="2"
                                    name="reportingTo" ui-select2="{allowClear: true}"
                                    ng-model="common.currentOrganizationUserDetails.ReportingTo"
                                    data-placeholder="Select User">
                                <option value=""></option>
                                <option ng-repeat="user in reportingAuthorities"
                                        value="{{user.Id}}"
                                        ng-bind="user.Name">
                                    {{user.Name}}
                                </option>
                            </select>
                        </div>
                    </div>
                </fieldset>
                <hr class="m-a-xs" />
                <fieldset class="form-group row">
                    <div class="col-sm-12">
                        <h6 class="h6 font-weight-medium m-b-1"><i class="fa fa-user m-r-xs" aria-hidden="true"></i>Personal Info</h6>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6 m-b-xs">
                        <label class="col-sm-3">Title</label>
                        <div class="col-sm-9">
                            <select tabindex="3"
                                    name="titleId" ui-select2="{allowClear: true}"
                                    ng-model="common.currentOrganizationUserDetails.TitleId"
                                    data-placeholder="Select Title">
                                <option value=""></option>
                                <option ng-repeat="title in titles"
                                        value="{{title.Id}}"
                                        ng-bind="title.Name"></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6 m-b-xs">
                        <label class="col-sm-3">First Name <span class="bold text-red">*</span></label>
                        <div class="col-sm-9">
                            <input tabindex="4"
                                   type="text" class="form-control" placeholder="Enter First Name"
                                   required
                                   ng-model="common.currentOrganizationUserDetails.FirstName"
                                   id="FirstName"
                                   name="FirstName"
                                   validation="required|min_len:2|max_len:50|xseed_alpha_dash_dot_apostrophe" />
                        </div>
                    </div>
                </fieldset>
                <fieldset class="form-group row">
                    <div class="col-sm-12 col-md-12 col-lg-6 m-b-xs">
                        <label class="col-sm-3">Middle Name</label>
                        <div class="col-sm-9">
                            <input tabindex="5"
                                   type="text" class="form-control" placeholder="Enter Middle Name"
                                   ng-model="common.currentOrganizationUserDetails.MiddleName"
                                   id="MiddleName"
                                   name="MiddleName"
                                   validation="max_len:50|xseed_alpha_dash_dot_apostrophe" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6 m-b-xs">
                        <label class="col-sm-3">Last Name <span class="bold text-red">*</span></label>
                        <div class="col-sm-9">
                            <input tabindex="6"
                                   type="text" class="form-control" placeholder="Enter Last Name"
                                   required
                                   ng-model="common.currentOrganizationUserDetails.LastName"
                                   id="LastName"
                                   name="LastName"
                                   validation="required|min_len:2|max_len:50|xseed_alpha_dash_dot_apostrophe" />
                        </div>
                    </div>
                </fieldset>
                <fieldset class="form-group row">
                    <div class="col-sm-12 col-md-12 col-lg-6 m-b-xs">
                        <label class="col-sm-3" for="BirthDate">Birth Date</label>
                        <div class="col-sm-9">
                            <datepicker date-format="MM/dd/yyyy"
                                        date-set-hidden="true"
                                        date-max-limit="{{maxDate}}">

                                <input tabindex="7"
                                       type="text"
                                       class="form-control"
                                       id="BirthDate"
                                       placeholder="Enter Birth Date"
                                       ng-model="common.currentOrganizationUserDetails.BirthDate"
                                       name="BirthDate"
                                       validation="xseed_date|xseed_date_compare:01/01/1900,{{maxDate}}"
                                       ui-mask="99/99/9999"
                                       ui-mask-placeholder
                                       ui-mask-placeholder-char="_"
                                       model-view-value="true" />
                            </datepicker>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6 m-b-xs">
                        <label class="col-sm-3" for="AnniversaryDate">Anniversary Date</label>
                        <div class="col-sm-9">
                            <datepicker date-format="MM/dd/yyyy"
                                        date-set-hidden="true"
                                        date-max-limit="{{maxDate}}">

                                <input tabindex="8"
                                       type="text"
                                       class="form-control"
                                       id="AnniversaryDate"
                                       placeholder="Enter Anniversary Date"
                                       ng-model="common.currentOrganizationUserDetails.AnniversaryDate"
                                       name="AnniversaryDate"
                                       validation="xseed_date|xseed_date_compare:01/01/1900,{{maxDate}}"
                                       ui-mask="99/99/9999"
                                       ui-mask-placeholder
                                       ui-mask-placeholder-char="_"
                                       model-view-value="true" />
                            </datepicker>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="form-group row">
                    <div class="col-sm-12 col-md-12 col-lg-6 m-b-xs">
                        <label class="col-sm-3" for="ProfileImageFile">Profile Image</label>
                        <div class="col-sm-7">
                            <label class="file width-100">
                                <input tabindex="9"
                                       type="file"
                                       accept=".jpg,.jpeg,.png"
                                       class="form-control-file"
                                       name="editOrgUserProfile" id="ProfileImageFile"
                                       ng-model="common.currentOrganizationUserDetails.ProfileImageFile"
                                       ng-file-model="common.currentOrganizationUserDetails.ProfileImageFile"
                                       ng-click="enableButtonProfile(common.currentOrganizationUserDetails.ProfileImageFile, 'edit')"
                                       check-file-size />
                                <span class="file-custom overflow-hidden">
                                    {{common.currentOrganizationUserDetails.ProfileImageFile.name}}
                                </span>
                            </label>
                            <i class="fa fa-info-circle text-muted">
                                <span class="text-muted">
                                    Please select files from jpg, jpeg or png.
                                </span>
                            </i>
                            <span ng-if="common.invalidProfileImgFile" class="validation text-danger">Please select valid format.</span>
                            <span ng-if="common.maxProfileImgFileSize" class="validation text-danger">Max upload size for file is 2 mb.</span>
                        </div>
                        <div class="col-sm-2">
                            <img ng-if="common.currentOrganizationUserDetails.ProfileImage" ng-src="{{common.currentOrganizationUserDetails.ProfileImagePath}}" lazy-src class="img-responsive logo-img profile-avatar">
                            <img ng-if="!common.currentOrganizationUserDetails.ProfileImage" ng-src="{{configuration.XSEED_NO_IMAGE_PATH}}" lazy-src class="avatar-3wh img-circle" title="user" />
                        </div>
                    </div>
                </fieldset>
                <hr class="m-a-xs" />
                <fieldset class="form-group row">
                    <div class="col-sm-12">
                        <h6 class="h6 font-weight-medium m-b-1"><i class="fa fa-phone-square m-r-xs" aria-hidden="true"></i>Contact Info</h6>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6 m-b-xs">
                        <label class="col-sm-3">Primary Email <span class="bold text-red">*</span></label>
                        <div class="col-sm-9">
                            <input tabindex="10" type="email" class="form-control" id="PrimaryEmail" placeholder="Enter email"
                                   ng-model="common.currentOrganizationUserDetails.PrimaryEmail"
                                   ng-init="" name="PrimaryEmail"
                                   readonly
                                   validation="required|xseed_email|max_len:100" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6 m-b-xs">
                        <label class="col-sm-3">Secondary Email</label>
                        <div class="col-sm-9">
                            <input tabindex="11"
                                   type="text"
                                   class="form-control"
                                   id="SecondaryEmail"
                                   placeholder="Enter secondary email"
                                   ng-model="common.currentOrganizationUserDetails.SecondaryEmail"
                                   ng-init=""
                                   name="SecondaryEmail"
                                   validation="xseed_email|max_len:100" />
                        </div>
                    </div>
                </fieldset>
                <fieldset class="form-group row">
                    <div class="col-sm-12 col-md-12 col-lg-6 m-b-xs">
                        <label class="col-sm-3" for="exampleInputPassword1">Mobile <span class="bold text-red">*</span></label>
                        <div class="col-sm-9">
                            <input tabindex="12"
                                   type="tel"
                                   class="form-control"
                                   id="Mobile"
                                   name="Mobile"
                                   ng-model="common.currentOrganizationUserDetails.Mobile"
                                   ui-mask="999-999-9999"
                                   ui-mask-placeholder
                                   ui-mask-placeholder-char="_"
                                   ng-blur="removeMask()"
                                   ng-focus="placeMask()"
                                   placeholder="enter mobile"
                                   validation="required|xseed_mobile" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6 m-b-xs">
                        <label class="col-sm-3" for="Phone">Phone</label>
                        <div class="col-sm-9">
                            <input tabindex="13"
                                   type="tel"
                                   class="form-control"
                                   id="Phone"
                                   name="Phone"
                                   placeholder="enter phone"
                                   name="mobile"
                                   ui-mask="999-999-9999"
                                   ui-mask-placeholder
                                   ui-mask-placeholder-char="_"
                                   ng-blur="removeMask()"
                                   ng-focus="placeMask()"
                                   ng-model="common.currentOrganizationUserDetails.Phone"
                                   validation="xseed_phone" />
                        </div>
                    </div>
                </fieldset>
                <fieldset class="form-group row">
                    <div class="col-sm-12 col-md-12 col-lg-6 m-b-xs">
                        <label class="col-sm-3" for="Fax">Fax</label>
                        <div class="col-sm-9">
                            <input tabindex="14"
                                   type="tel"
                                   class="form-control"
                                   id="Fax"
                                   name="Fax"
                                   placeholder="enter fax"
                                   ui-mask="(999) 999-9999"
                                   ui-mask-placeholder
                                   ui-mask-placeholder-char="_"
                                   ng-blur="removeMask()"
                                   ng-focus="placeMask()"
                                   ng-model="common.currentOrganizationUserDetails.Fax"
                                   validation="xseed_phone:alt=Please enter valid fax number." />
                        </div>
                    </div>
                </fieldset>
                <hr class="m-a-xs" />
                <fieldset class="form-group row">
                    <div class="col-sm-12">
                        <h6 class="h6 font-weight-medium m-b-1"><i class="fa fa-map-marker m-r-xs" aria-hidden="true"></i>Location Info</h6>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6 m-b-xs">
                        <label class="col-sm-3" for="Address1">Address 1</label>
                        <div class="col-sm-9">
                            <input tabindex="15"
                                   type="text"
                                   class="form-control"
                                   id="Address1"
                                   name="Address1"
                                   placeholder="Enter address1"
                                   ng-model="common.currentOrganizationUserDetails.Address1"
                                   validation="max_len:100" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6 m-b-xs">
                        <label class="col-sm-3" for="Address2">Address 2</label>
                        <div class="col-sm-9">
                            <input tabindex="16"
                                   type="text"
                                   class="form-control"
                                   id="Address2"
                                   name="Address2"
                                   placeholder="Enter address2"
                                   ng-model="common.currentOrganizationUserDetails.Address2"
                                   validation="max_len:100" />
                        </div>
                    </div>
                </fieldset>
                <fieldset class="form-group row">
                    <div class="col-sm-12 col-md-12 col-lg-6 m-b-xs">
                        <label class="col-sm-3" for="countryId">Country</label>
                        <div class="col-sm-9">
                            <select tabindex="17"
                                    name="countryId"
                                    ui-select2
                                    data-placeholder="Country"
                                    ng-model="common.currentOrganizationUserDetails.CountryId"
                                    ng-click="getStateListByCountry(common.currentOrganizationUserDetails.CountryId)">
                                <option value=""></option>
                                <option ng-repeat="country in metadata.GetAncillaryInformationResponse.countryList.country" value="{{country.Id}}"
                                        ng-bind="country.Name"></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6 m-b-xs">
                        <label class="col-sm-3" for="stateId">State</label>
                        <div class="col-sm-9">
                            <select tabindex="18"
                                    name="stateId"
                                    ui-select2
                                    data-placeholder="State"
                                    ng-model="common.currentOrganizationUserDetails.StateId"
                                    ng-click="getCityListByState(common.currentOrganizationUserDetails.StateId)">
                                <option value=""></option>
                                <option ng-repeat="state in stateList" value="{{state.Id}}"
                                        ng-bind="state.Name"></option>
                            </select>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="form-group row">
                    <div class="col-sm-12 col-md-12 col-lg-6 m-b-xs">
                        <label class="col-sm-3" for="cityId">City</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-9 p-r-0">
                                    <select tabindex="19"
                                            name="cityId"
                                            ui-select2="{allowClear: true}"
                                            data-placeholder="City"
                                            ng-model="common.currentOrganizationUserDetails.CityId">
                                        <option value=""></option>
                                        <option ng-repeat="city in cityList" value="{{city.Id}}"
                                                ng-bind="city.Name"></option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <button class="btn btn-sm small btn-primary" ng-disabled="!(common.currentOrganizationUserDetails.CountryId && common.currentOrganizationUserDetails.StateId)" ng-click="openAddNewCityToMasterModal(common.currentOrganizationUserDetails.CountryId, common.currentOrganizationUserDetails.StateId)"><i class="fa fa-plus"></i>Add</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6 m-b-xs">
                        <label class="col-sm-3" for="Zip">Zip</label>
                        <div class="col-sm-9">
                            <input tabindex="20"
                                   type="text"
                                   class="form-control"
                                   id="Zip"
                                   name="Zip"
                                   placeholder="Enter zip code"
                                   ng-model="common.currentOrganizationUserDetails.Zip"
                                   validation="xseed_zip" />
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="card-footer p-a-xs text-sm-center">
                <button type="submit" class="btn btn-primary" ng-click="saveEditOrgUserDetails(editOrgUserForm)"><i class="material-icons">save</i> Save</button>&nbsp;
                <button type="submit" class="btn btn-default" ng-click="gotoSystemUsers()">Cancel</button>
            </div>
        </div>
        <div class="card-footer p-a-xs">

        </div>
    </form>
</div>

<script>
    $(document).ready(function () {
        $(".js-example-basic-single").select2();
    });
</script>
<script type="text/javascript">
    //$('#alert-btn').click(function () {
    //    swal("Good job!", "Details saved successfully", "success");
    //});
</script>
