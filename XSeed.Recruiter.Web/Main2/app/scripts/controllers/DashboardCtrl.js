﻿'use strict';

XSEED_APP.controller('DashboardCtrl', DashboardCtrl);

/**
 * @ngInject
 */
function DashboardCtrl($scope, $rootScope, $route, $routeParams, RequestContext, $timeout, $q, $location, $window, configuration, i18nFactory, ExceptionHandler, EventBus, $log, _, ipCookie, blockUI, blockUIConfig, ModalFactory, XSeedAlert, localStorageService, $uibModal, XSeedApiFactory, ngTableParams, $filter, ValidationService) {    
    $scope.checkedCount = 0;
    $scope.DashboardFilter = {};
    $scope.DashboardAPIModel = {};
    $scope.common.advanceSearchModel = {};

    $scope.jobSubmissionLabels = [];
    $scope.jobSubmissionData = [];
    $scope.jobSubmissionSeries = ['Requirement', 'Submission'];
    $scope.datasetOverride = [{ yAxisID: 'y-axis' }];

    $scope.submissionsLabels = [];
    $scope.submissionsData = [];

    $scope.candidatesLabels = [];
    $scope.candidatesData = [];

    $scope.jobStatusLabels = [];
    $scope.jobStatusData = [];

    $scope.showRequirementVsSubmissionLoader = false;
    $scope.showSubmissionLoader = false;
    $scope.showCandidateAddedLoader = false;
    $scope.showRequirementStatusLoader = false;

    var OrganizationId = $rootScope.userDetails.OrganizationId;
    var UserId = $rootScope.userDetails.UserId;

    var recentRecordsToShow = 5;
    var recentPageSizeToShow = 1;

    var dashboardRequirementChartDetailsModal = ModalFactory.dashboardRequirementChartDetailsModal($scope);
    var dashboardSubmissionChartDetailsModal = ModalFactory.dashboardSubmissionChartDetailsModal($scope);
    var dashboardCandidateChartDetailsModal = ModalFactory.dashboardCandidateChartDetailsModal($scope);


    //Dashboard

    $scope.DashboardDetailView = function () {
        blockUIConfig.autoBlock = false;
        var promise = XSeedApiFactory.getDashboardDetail(OrganizationId, UserId);
        promise.then(
          function (response) {
              $scope.DashboardModel = response[0].data;

              //buildDashboardChart(response[0].data);
              //blockUI.stop();
              //$scope.showNoty('Welcome ' + $rootScope.userDetails.FirstName + ' ' + $rootScope.userDetails.LastName + ' to XSEED!');
              blockUIConfig.autoBlock = true;
              //$timeout(function () {
              //    $scope.callAtInterval();
              //}, 5000);
              
          },
          function (httpError) {
              blockUIConfig.autoBlock = true;
              ExceptionHandler.handlerHTTPException('Dashboard', httpError.data.Status, httpError.data.Message);
          });
    };

    $scope.getDashboardRequirementVsSubmissionReportData = function () {
        $scope.jobSubmissionChartModel = {};
        $scope.jobSubmissionChartModel.loader = true;

        var promise = XSeedApiFactory.getDashboardRequirementVsSubmissionReportData(OrganizationId, UserId);
        promise.then(
          function (response) {
              var jobSubmissionChartData = response[0].data;

              var jobs = [];
              var submissions = [];

              $scope.jobSubmissionChartModel.labels = [];
              $scope.jobSubmissionChartModel.data = [];

              angular.forEach(jobSubmissionChartData, function (value, key) {
                  $scope.jobSubmissionChartModel.labels.push(value.MonthName + " " + value.Year);
                  jobs.push(value.JobCount);
                  submissions.push(value.SubmissionCount);
              });

              $scope.jobSubmissionChartModel.data.push(jobs);
              $scope.jobSubmissionChartModel.data.push(submissions);
              $scope.jobSubmissionChartModel.chartColors = ['#AED6F1', '#85929E'];
              $scope.jobSubmissionChartModel.series = ['Requirement', 'Submission'];

              $scope.jobSubmissionChartModel.options = {
                  responsive: true,
                  scales: {
                      yAxes: [
                        {
                            id: 'y-axis',
                            type: 'linear',
                            display: true,
                            position: 'left'
                        }
                      ]
                  },
                  legend: {
                      display: true,
                      position: 'top'
                  }
              };

              $scope.jobSubmissionChartModel.loader = false;
          },
          function (httpError) {
              $scope.jobSubmissionChartModel.loader = false;
              ExceptionHandler.handlerHTTPException('Dashboard', httpError.data.Status, httpError.data.Message);
          });
    }

    $scope.getDashboardSubmissionReportData = function () {
        $scope.submissionsReportModel = {};
        $scope.submissionsReportModel.loader = true;
        var promise = XSeedApiFactory.getDashboardSubmissionReportData(OrganizationId, UserId);
        promise.then(
          function (response) {
              var submissions = response[0].data;

              $scope.submissionsReportModel.labels = [];
              $scope.submissionsReportModel.data = [];
              $scope.submissionsReportModel.organizationUserId = [];

              angular.forEach(submissions, function (value, key) {
                  $scope.submissionsReportModel.labels.push(value.Name);
                  $scope.submissionsReportModel.organizationUserId.push(value.UserId);
                  $scope.submissionsReportModel.data.push(value.Count);
              });

              $scope.submissionsReportModel.options = {
                  responsive: true,
                  scaleShowValues: true,
                  scales: {
                      yAxes: [{
                          ticks: {
                              beginAtZero: true
                          }
                      }],
                      xAxes: [{
                          ticks: {
                              autoSkip: false
                          }
                      }]
                  }
              };

              $scope.submissionsReportModel.loader = false;

              //get recent submissions
              getRecentSubmissions();
          },
          function (httpError) {
              $scope.submissionsReportModel.loader = false;
              ExceptionHandler.handlerHTTPException('Dashboard', httpError.data.Status, httpError.data.Message);
          });
    }

    $scope.getDashboardCandidateAddedReportData = function () {
        $scope.candidateAddedChartModel = {};
        $scope.candidateAddedChartModel.loader = true;

        var promise = XSeedApiFactory.getDashboardCandidateAddedReportData(OrganizationId, UserId);
        promise.then(
          function (response) {
              var candidates = response[0].data;

              $scope.candidateAddedChartModel.labels = [];
              $scope.candidateAddedChartModel.data = [];
              $scope.candidateAddedChartModel.organizationUserId = [];

              angular.forEach(candidates, function (value, key) {
                  $scope.candidateAddedChartModel.labels.push(value.Name);
                  $scope.candidateAddedChartModel.organizationUserId.push(value.UserId);
                  $scope.candidateAddedChartModel.data.push(value.Count);
              });

              $scope.candidateAddedChartModel.options = {
                  responsive: true,
                  scales: {
                      yAxes: [{
                          ticks: {
                              beginAtZero: true
                          }
                      }],
                      xAxes: [{
                          ticks: {
                              autoSkip: false
                          }
                      }]
                  }
              };

              $scope.candidateAddedChartModel.loader = false;

              //get recent candidates
              getRecentCandidates();
          },
          function (httpError) {
              $scope.candidateAddedChartModel.loader = false;
              ExceptionHandler.handlerHTTPException('Dashboard', httpError.data.Status, httpError.data.Message);
          });
    }

    $scope.getDashboardRequirementStatusReportData = function () {
        $scope.requirementStatusChartModel = {};
        $scope.requirementStatusChartModel.loader = true;

        var promise = XSeedApiFactory.getDashboardRequirementStatusReportData(OrganizationId, UserId);
        promise.then(
          function (response) {
              var jobStatus = response[0].data;

              $scope.requirementStatusChartModel.labels = [];
              $scope.requirementStatusChartModel.data = [];
              $scope.requirementStatusChartModel.chartColors = [];

              angular.forEach(jobStatus, function (value, key) {
                  $scope.requirementStatusChartModel.labels.push(value.Status);
                  $scope.requirementStatusChartModel.data.push(value.Count);
                  if (value.Status == 'Closed') {
                      $scope.requirementStatusChartModel.chartColors.push('#7fc35c');
                  }
                  else if (value.Status == 'Hold') {
                      $scope.requirementStatusChartModel.chartColors.push('#4cc3f0');
                  }
                  else if (value.Status == 'Archive') {
                      $scope.requirementStatusChartModel.chartColors.push('#e8e8e8');
                  }
                  else if (value.Status == 'Pending') {
                      $scope.requirementStatusChartModel.chartColors.push('#f0c54c');
                  }
                  else if (value.Status == 'Open') {
                      $scope.requirementStatusChartModel.chartColors.push('#d26d54');
                  }
              });
              $scope.requirementStatusChartModel.loader = false;

              //get recent requirements
              getRecentRequirements();
          },
          function (httpError) {
              $scope.requirementStatusChartModel.loader = false;
              ExceptionHandler.handlerHTTPException('Dashboard', httpError.data.Status, httpError.data.Message);
          });
        $scope.requirementStatusChartModel.options = {
            responsive: true,
            legend: {
                display: true,
                position: 'left'
            }
        };
    }

    //Notification Panel
    $scope.getNotificationFeeds = function () {
        blockUIConfig.autoBlock = false;
        getRequirementQueueListCount();
        if ($rootScope.userDetails.Vendor_Read == 'True') {
            getUnreadVendorMailCount();
            getUnreadResumeMailCount();
        }
        blockUIConfig.autoBlock = true;
    };

    function getRequirementQueueListCount() {
        blockUI.start();

        var organizationUserId = $rootScope.userDetails.UserId;
        var promise = XSeedApiFactory.getRequirementQueueListCount(organizationUserId);
        promise.then(
          function (response) {
              $scope.requirementQueueCount = response[0].data;
              blockUI.stop();
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Dashboard', httpError.data.Status, httpError.data.Message);
          });
    };

    function getUnreadVendorMailCount() {
        if ($rootScope.userDetails.Vendor_Read == 'True') {
            var promise = XSeedApiFactory.getUnreadMailCount($rootScope.userDetails.OrganizationId);
            promise.then(
              function (response) {
                  $scope.unreadVendorMailCount = response[0].data;
              },
              function (httpError) {
                  ExceptionHandler.handlerHTTPException('Dashboard', httpError.data.Status, httpError.data.Message);
              });
        }
    };

    function getUnreadResumeMailCount() {
        var promise = XSeedApiFactory.getUnreadResumeMailCount($rootScope.userDetails.OrganizationId);
        promise.then(
          function (response) {
              blockUI.stop();
              $scope.unreadResumeMailCount = response[0].data;
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Dashboard', httpError.data.Status, httpError.data.Message);
          });
    };

    function getRecentRequirements() {
        // if has read permission then only call the function
        if ($rootScope.userDetails.Requirement_Read != "False") {
            var promise = XSeedApiFactory.getRequirementList($rootScope.userDetails.OrganizationId, recentRecordsToShow, recentPageSizeToShow);
            promise.then(
              function (response) {
                  $scope.recentRequirementList = response[0].data.Jobs;
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Dashboard', httpError.data.Status, httpError.data.Message);
              });
        }
    };

    function getRecentSubmissions() {
        // if has read permission then only call the function
        if ($rootScope.userDetails.Submission_Read != "False") {
            var promise = XSeedApiFactory.getSubmissions($rootScope.userDetails.OrganizationId, recentRecordsToShow, recentPageSizeToShow);
            promise.then(
              function (response) {
                  $scope.recentSubmissionList = response[0].data.Submissions;
              },
              function (httpError) {
                  ExceptionHandler.handlerHTTPException('Dashboard', httpError.data.Status, httpError.data.Message);
              });
        }
    };

    function getRecentCandidates() {
        // if has read permission then only call the function
        if ($rootScope.userDetails.Candidate_Read != "False") {
            var promise = XSeedApiFactory.getCandidateList(recentRecordsToShow, recentPageSizeToShow);
            promise.then(
              function (response) {
                  $scope.recentCandidateList = response[0].data.Candidates;
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Dashboard', httpError.data.Status, httpError.data.Message);
              });
        }
    };


    //chart details
    //Requirement Status Chart
    $scope.openRequirementChartDetailsModal = function () {
        blockUI.start();
        dashboardRequirementChartDetailsModal.$promise.then(dashboardRequirementChartDetailsModal.show);
        blockUI.stop();
    };

    $scope.closeRequirementChartDetailsModal = function () {
        $scope.common.advanceSearchModel = {};
        dashboardRequirementChartDetailsModal.$promise.then(dashboardRequirementChartDetailsModal.hide);
    };

    $scope.getRequirementListByStatus = function (points, evt) {
        blockUI.start();
        //to get selected status as per index
        if (points.length) {
            $scope.requirementStatusChartModel.modalTitle = $scope.requirementStatusChartModel.labels[points[0]._index];
            $scope.common.advanceSearchModel.SearchAny = $scope.requirementStatusChartModel.labels[points[0]._index];
            getRequirementsByStatus();
        }
        blockUI.stop();
    };

    function getRequirementsByStatus() {
        blockUI.start();
        if ($scope.common.advanceSearchModel.SearchAny) {
            $scope.common.advanceSearchModel.OrganizationId = $rootScope.userDetails.OrganizationId;
            $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
            $scope.common.pageNumberShow = undefined;
            var firstPageFlagSearch = 0;
            //for new plain search
            $scope.common.advanceSearchModel.SearchIn = ["Status"];

            var promise = XSeedApiFactory.getRequirementAdvanceSearchResult($scope.common.advanceSearchModel, $scope.common.pageSizeShow, $scope.common.pageNumberShow);
            promise.then(
            function (response) {
                $scope.common.jobList = response[0].data.Jobs;
                $scope.common.totalCount = response[0].data.TotalCount;
                $scope.common.totalPages = response[0].data.TotalPages;

                //Manupulation of response
                angular.forEach($scope.common.jobList, function (val, index) {
                    if ($scope.common.jobList[index].TechnicalSkills != null) {
                        $scope.common.jobList[index].TechnicalSkills = $scope.common.jobList[index].TechnicalSkills.split("|");
                        $scope.common.jobList[index].TechnicalSkills = $scope.common.jobList[index].TechnicalSkills.join();
                    }
                });

                firstPageFlagSearch = 1;
                populateRequirementsByStatus($scope.common.totalCount, $scope.common.totalPages, firstPageFlagSearch);
                $scope.openRequirementChartDetailsModal();
                blockUI.stop();
            },
            function (httpError) {
                $scope.closeRequirementChartDetailsModal();
                blockUI.stop();
                ExceptionHandler.handlerHTTPException('Search', httpError.data.Status, httpError.data.Message);
            });
        }
    };

    function populateRequirementsByStatus(totalCount, totalPages, pageFlag) {
        //if (angular.isDefined($scope.common.tblRequirementList)) {
        //    $scope.common.tblRequirementList.reload();
        //}
        //else
        {
            $scope.common.tblRequirementList = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: $scope.common.ngTablePaginationCount,
                total: totalCount,
                getData: function ($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    if (pageFlag != 1) {
                        /* sorting */
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function (val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */

                        var promise = XSeedApiFactory.getRequirementAdvanceSearchResult($scope.common.advanceSearchModel, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder);
                        promise.then(
                          function (response) {
                              $scope.common.jobList = response[0].data.Jobs;
                              $scope.common.totalCount = response[0].data.TotalCount;
                              $scope.common.totalPages = response[0].data.TotalPages;

                              //Manupulation of response
                              angular.forEach($scope.common.jobList, function (val, index) {
                                  if ($scope.common.jobList[index].TechnicalSkills != null) {
                                      $scope.common.jobList[index].TechnicalSkills = $scope.common.jobList[index].TechnicalSkills.split("|");
                                      $scope.common.jobList[index].TechnicalSkills = $scope.common.jobList[index].TechnicalSkills.join();
                                  }
                              });

                              blockUI.stop();
                          },
                          function (httpError) {
                              blockUI.stop();
                              ExceptionHandler.handlerHTTPException('Search', httpError.data.Status, httpError.data.Message);
                          });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.common.jobList);
                }
                , $scope: $scope
            });
        }
    };

    //Submisssion Status Chart
    $scope.openSubmissionChartDetailsModal = function () {
        blockUI.start();
        dashboardSubmissionChartDetailsModal.$promise.then(dashboardSubmissionChartDetailsModal.show);
        blockUI.stop();
    };

    $scope.closeSubmissionChartDetailsModal = function () {
        $scope.common.advanceSearchModel = {};
        dashboardSubmissionChartDetailsModal.$promise.then(dashboardSubmissionChartDetailsModal.hide);
    };

    $scope.getSubmissionListByUser = function (points, evt) {
        blockUI.start();
        //to get selected status as per index
        if (points.length && $scope.submissionsReportModel.organizationUserId[points[0]._index]) {
            $scope.submissionsReportModel.modalTitle = $scope.submissionsReportModel.labels[points[0]._index];
            $scope.common.advanceSearchModel = {};
            $scope.common.advanceSearchModel.filter = {};
            $scope.common.advanceSearchModel.filter.OrganizationUser = [];
            $scope.common.advanceSearchModel.filter.OrganizationUser.push($scope.submissionsReportModel.organizationUserId[points[0]._index]);
            getSubmissionsByUser();
        }
        blockUI.stop();
    };

    function getSubmissionsByUser() {
        if ($scope.common.advanceSearchModel) {
            blockUI.start();

            $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
            $scope.common.pageNumberShow = undefined;
            var firstPageFlag = 0;
            $scope.common.advanceSearchModel.OrganizationId = $rootScope.userDetails.OrganizationId;

            var promise = XSeedApiFactory.searchSubmissions($scope.common.advanceSearchModel, $scope.common.pageSizeShow, $scope.common.pageNumberShow);
            promise.then(
              function (response) {
                  blockUI.stop();

                  $scope.submissions = response[0].data.Submissions;
                  $scope.submissionsTotalCount = response[0].data.TotalCount;
                  $scope.submissionsTotalPages = response[0].data.TotalPages;

                  firstPageFlag = 1;
                  populateSubmissionsByUser($scope.common.totalCount, $scope.common.totalPages, firstPageFlag);
                  $scope.openSubmissionChartDetailsModal();
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
              });
        }
    }

    function populateSubmissionsByUser(totalCount, totalPages, pageFlag) {
        //if (angular.isDefined($scope.submissionList)) {
        //    $scope.submissionList.reload();
        //}
        //else
        {
            $scope.submissionList = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: $scope.common.ngTablePaginationCount,
                total: $scope.submissionsTotalCount,
                getData: function ($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    if (pageFlag != 1) {
                        /* sorting */
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function (val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */

                        var promise = XSeedApiFactory.searchSubmissions($scope.common.advanceSearchModel, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder);
                        promise.then(
                          function (response) {
                              $scope.submissions = response[0].data.Submissions;
                              $scope.submissionsTotalCount = response[0].data.TotalCount;
                              $scope.submissionsTotalPages = response[0].data.TotalPages;

                              blockUI.stop();
                          },
                          function (httpError) {
                              blockUI.stop();
                              ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
                          });

                    }
                    pageFlag = 0;
                    params.total($scope.submissionsTotalCount);
                    $defer.resolve($scope.submissions);
                }
                , $scope: $scope
            });
        }
    };

    //Candidate Status Chart
    $scope.openCandidateChartDetailsModal = function () {
        blockUI.start();
        dashboardCandidateChartDetailsModal.$promise.then(dashboardCandidateChartDetailsModal.show);
        blockUI.stop();
    };

    $scope.closeCandidateChartDetailsModal = function () {
        $scope.common.advanceSearchModel = {};
        dashboardCandidateChartDetailsModal.$promise.then(dashboardCandidateChartDetailsModal.hide);
    };

    $scope.getCandidateListByUser = function (points, evt) {
        blockUI.start();
        //to get selected status as per index
        if (points.length && $scope.candidateAddedChartModel.organizationUserId[points[0]._index]) {
            $scope.candidateAddedChartModel.modalTitle = $scope.candidateAddedChartModel.labels[points[0]._index];
            $scope.common.advanceSearchModel = {};
            $scope.common.advanceSearchModel.filter = {};
            $scope.common.advanceSearchModel.filter.CreatedBy = [];
            $scope.common.advanceSearchModel.filter.CreatedBy.push($scope.candidateAddedChartModel.organizationUserId[points[0]._index]);
            getCandidateListByUser();
        }
        blockUI.stop();
    };

    function getCandidateListByUser() {
        blockUI.start();
        if ($scope.common.advanceSearchModel) {
            $scope.common.advanceSearchModel.OrganizationId = $rootScope.userDetails.OrganizationId;
            $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
            $scope.common.pageNumberShow = undefined;
            var firstPageFlagSearch = 0;

            var promise = XSeedApiFactory.getCandidateAdvanceSearchResult($scope.common.advanceSearchModel, $scope.common.pageSizeShow, $scope.common.pageNumberShow);
            promise.then(
            function (response) {
                $scope.common.candidateList = response[0].data.Candidates;
                $scope.common.totalCount = response[0].data.TotalCount;
                $scope.common.totalPages = response[0].data.TotalPages;

                firstPageFlagSearch = 1;

                $scope.populateSearchCandidateByUser($scope.common.totalCount, $scope.common.totalPages, firstPageFlagSearch);
                $scope.openCandidateChartDetailsModal();

                //to show skills in tooltip as string
                angular.forEach($scope.common.candidateList, function (val, index) {
                    if (val.Skills) {
                        val.Skills = val.Skills.join();
                    }
                });
                blockUI.stop();

            },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Search', httpError.data.Status, httpError.data.Message);
          });
        }
    }

    $scope.populateSearchCandidateByUser = function (totalCount, totalPages, pageFlag) {
        //if (angular.isDefined($scope.common.tblCandidateList)) {
        //    $scope.common.tblCandidateList.reload();
        //}
        //else 
        {
            $scope.common.tblCandidateList = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage
                , sorting: {}
            }, {
                counts: $scope.common.ngTablePaginationCount,
                total: totalCount,
                getData: function ($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    if (pageFlag != 1) {
                        /* sorting */
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function (val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */

                        var promise = XSeedApiFactory.getCandidateAdvanceSearchResult($scope.common.advanceSearchModel, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder);
                        promise.then(
                          function (response) {
                              $scope.common.candidateList = response[0].data.Candidates;
                              $scope.common.totalCount = response[0].data.TotalCount;
                              $scope.common.totalPages = response[0].data.TotalPages;

                              //to show skills in tooltip as string
                              angular.forEach($scope.common.candidateList, function (val, index) {
                                  if (val.Skills) {
                                      val.Skills = val.Skills.join();
                                  }
                              });
                              blockUI.stop();

                          },
                          function (httpError) {
                              blockUI.stop();
                              ExceptionHandler.handlerHTTPException('Search', httpError.data.Status, httpError.data.Message);
                          });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.common.candidateList);
                }
                , $scope: $scope
            });
        }
    };
    //Initialization Function
    function init() {
        $scope.DashboardDetailView();
    };

    init();

    //***************************************************************************************//

    // Get the render context local to this controller (and relevant params).
    var renderContext = RequestContext.getRenderContext("standard.resumeBlaster");
    // The subview indicates which view is going to be rendered on the page.
    $scope.subview = renderContext.getNextSection();
    $scope.$on(
        "requestContextChanged",
        function () {
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {
                return;
            }
            // Update the view that is being rendered.
            $scope.subview = renderContext.getNextSection();
        }
    );
};
