﻿'use strict';

XSEED_APP.controller('CandidateCtrl', CandidateCtrl);

/**
 * @ngInject
 */
function CandidateCtrl($scope, $rootScope, $route, $routeParams, RequestContext, $timeout, $q, $location, $window, configuration, i18nFactory, ExceptionHandler, EventBus, $log, _, ipCookie, blockUI, ModalFactory, XSeedAlert, localStorageService, $uibModal, XSeedApiFactory, ngTableParams, $filter, ValidationService, $sce, $compile, blockUIConfig) {

    $scope.common.candidateModel = new API.CandidateRequest();

    init();

    var candidateSubmissionModal = ModalFactory.candidateSubmissionModal($scope);
    var candidateSubmissionEmailModal = ModalFactory.candidateSubmissionEmailModal($scope);
    $scope.common.AddEditVisaTypeModal = ModalFactory.AddEditVisaTypeModal($scope);

    $scope.openCandidateSubmissionModal = function (type, candidate) {
        $scope.candidateSubmissionList = [];

        $scope.candidate = candidate;

        if (type == 'Single') {
            $scope.candidateSubmissionList.push(candidate);
        }
        else {
            angular.forEach($scope.checkboxes.items, function (item, Identifier) {
                if (item == true) {
                    var selectedCandidate = _.find($scope.common.candidateList, function (o) { return o._id == Identifier; });
                    if (selectedCandidate) {
                        $scope.candidateSubmissionList.push(selectedCandidate);
                    }
                }
            });
        }


        if ($scope.candidateSubmissionList.length == 0) {
            ExceptionHandler.handlerPopup('Candidate Submission', 'Please select candidate from displayed page.', '', 'info');
        }
        else {
            var organizationId = $rootScope.userDetails.OrganizationId;
            var promise = XSeedApiFactory.submissionPopupLookupCall(organizationId);
            promise.then(
              function (response) {
                  $scope.lookup.company = response[0].data;
              },
              function (httpError) {
                  //blockUI.stop();
                  ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
              });

            candidateSubmissionModal.$promise.then(candidateSubmissionModal.show);
        }
    }

    $scope.closeCandidateSubmissionModal = function () {
        candidateSubmissionModal.$promise.then(candidateSubmissionModal.hide);
        $scope.checkboxes.items = {};
    }

    //Initialization Function
    function init() {
        $scope.CandidateSubmissionModel = new API.CandidateSubmissionRequest();
        $scope.checkedCount = 0;
        $scope.candidateFilter = {};
        $scope.candidateSearch = {};
        $rootScope.commonEmailModel = {};
        $scope.dataSubmissiomEmail = {};
        $scope.candidateResumeInputModel = {};
        $scope.IsDuplicateVisaType = false;
        $scope.qNoteModelCandidate = {};
        $scope.isSubmitByEmail = false;
        $scope.isSubmitBySubmissionUrl = false;
        $scope.candidateSearchData = "";
        $scope.filterItemLable = [];
        $scope.flag.advanceSearchFlag = false;
        $scope.duplicateCandidateId = false;
        $scope.common.invalid = false;
        $scope.common.maxSize = false;
        $scope.common.invalidBulkUploadResume = false;
        $scope.common.maxUploadFileCount = false;

        $scope.getLookUpSkill();

        if (($scope.isUndefinedOrNull($scope.common.candidateModel._id) || $scope.isEmpty($scope.common.candidateModel._id))) {
            var resumeEmailsUrl = "/resumeEmailList";
            if ($location.path().toLowerCase() != resumeEmailsUrl.toLowerCase()) {
            }
            else {
                $location.path('candidateList');
            }
        }

        if ($scope.isUndefinedOrNull($scope.common.candidateModel._id) || $scope.isEmpty($scope.common.candidateModel._id)) {
            $location.path('candidateList');
        }

        $scope.common.tblCandidateList = undefined;
        $scope.filterItemCount = 0;

        var date = new Date();
        $scope.maxDate = ("0" + (date.getMonth() + 1).toString()).substr(-2) + "/" + ("0" + date.getDate().toString()).substr(-2) + "/" + (date.getFullYear().toString());
        $scope.minDate = ("0" + (date.getMonth() + 1).toString()).substr(-2) + "/" + ("0" + date.getDate().toString()).substr(-2) + "/" + (date.getFullYear().toString());
        $scope.availabilityDateLimit = ("0" + (date.getMonth() + 1).toString()).substr(-2) + "/" + ("0" + date.getDate().toString()).substr(-2) + "/" + ((date.getFullYear() + 1).toString());
        $scope.passportVisaValidDate = ("0" + (date.getMonth() + 1).toString()).substr(-2) + "/" + ("0" + date.getDate().toString()).substr(-2) + "/" + ((date.getFullYear() + 10).toString());
    };



    $scope.createFunctionDegree = function (input) {
        // format the option and return it
        return {
            value: $scope.lookup.degreeListData.length,
            label: input
        };
    };

    $scope.candidateLookupProcess = function () {
        $scope.candidatePageLookupList();
    };


    $scope.candidatePageLookupList = function () {
        var promise = XSeedApiFactory.candidatePageLookupCall($rootScope.userDetails.OrganizationId);
        promise.then(
          function (response) {
              $scope.lookup.maritalStatus = response[0].data;
              $scope.lookup.visaType = response[1].data;
              $scope.lookup.degree = response[2].data;
              $scope.lookup.title = response[3].data;
              $scope.lookup.gender = response[4].data;
              var res = response[5].data;
              $scope.lookup.jobType = response[6].data;
              $scope.lookup.candidateStatus = response[7].data;

              $scope.lookup.languageList = [];
              angular.forEach(res, function (val, index) {
                  $scope.lookup.languageList.push({ value: val.Name, label: val.Name });
              });

              candidateSecondaryLookupList();
          },
          function (httpError) {
              ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
          });
    };

    function candidateSecondaryLookupList() {
        var organizationId = $rootScope.userDetails.OrganizationId;
        var promise = XSeedApiFactory.candidateSecondaryLookupList(organizationId);
        promise.then(
          function (response) {
              $scope.lookup.organizationUser = response[0].data;
              $scope.lookup.company = response[1].data;
          },
          function (httpError) {
              ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
          });
    };

    $scope.uploadImage = function (files) {
        if (files.length != 0) {
            $scope.orgLogoFileName = files[0].name;
            var ext = files[0].name.match(/(?:\.([^.]+))?$/)[1];
            if (files[0].size >= 2000000) {
                $scope.common.maxSize = true;
            }
            else {
                if (angular.lowercase(ext) === 'jpg' || angular.lowercase(ext) === 'jpeg' || angular.lowercase(ext) === 'png') {
                    $scope.common.maxSize = false;
                    $scope.common.invalid = false;
                }
                else {
                    $scope.common.candidateModel.ProfileImageFile = "";
                    $scope.common.invalid = true;
                }
            }
        }
        else {
            $scope.enableButton();
            $scope.common.invalid = false;
            $scope.common.maxSize = false;
        }
    }


    $scope.enableButton = function (img) {
        if (img) {
            $scope.common.maxSize = false;
            var ext = img.name.match(/\.(.+)$/)[1];
            if (angular.lowercase(ext) === 'jpg' || angular.lowercase(ext) === 'jpeg' || angular.lowercase(ext) === 'png') {
                if ($scope.common.candidateModel.ProfileImageFile) {
                    $scope.common.candidateModel.ProfileImageFile.data = img.data;
                    $scope.common.invalid = false;
                }
                else {
                    $scope.common.candidateModel.ProfileImageFile.data = $scope.common.candidateModel.ProfileImageFile.data;
                }
                $scope.common.invalid = false;
                $scope.common.candidateModel.ProfileImageFile.data = "";
            }

        }
    }

    /* QNote start */
    $scope.submitQNoteCandidate = function () {
        if ($scope.qNoteModelCandidate.qNote) {
            var promise = XSeedApiFactory.createQNoteCandidate(encodeURIComponent($scope.qNoteModelCandidate.qNote), $scope.common.selectedCandidate._id);
            promise.then(
              function (response) {
                  blockUI.stop();
                  $scope.qNoteModelCandidate.qNote = '';
                  $scope.getQNoteListCandidate();
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Candidate', httpError.data.Status, httpError.data.Message);
              });
        }
    };


    /*      candidate open modal start              */
    $scope.openAddEditVisaModal = function (type, mode) {
        $scope.ManipulationMode = mode;
        if (mode === "Create") {
            $scope.common.AddEditVisaTypeModal.$promise.then($scope.common.AddEditVisaTypeModal.show);
            $scope.common.candidateModel.VisaType = '';
            $scope.common.candidateModel.VisaValidUpto = '';
            $scope.IsDuplicateVisaType = false;
        }
        else {

            var itemEditedData = _.find($scope.common.candidateModel.VisaTypeList, function (o) { return o.VisaType == type });
            $scope.IsDuplicateVisaType = false;
            $scope.common.candidateModel.VisaTypeId = itemEditedData.VisaTypeId;
            $scope.common.candidateModel.VisaType = itemEditedData.VisaType;
            $scope.common.candidateModel.VisaValidUpto = itemEditedData.ValidUpto;
            $scope.common.AddEditVisaTypeModal.$promise.then($scope.common.AddEditVisaTypeModal.show);
        }
    };

    $scope.removeVisaTypeRecord = function (index) {
        $scope.common.candidateModel.VisaTypeList.splice(index, 1);
    }

    $scope.addVisaType = function (candidateModel, mode, form) {
        if (mode === "Create") {
            if ($scope.IsDuplicateVisaType == false && new ValidationService().checkFormValidity(form)) {
                if ($scope.common.candidateModel.VisaTypeList == null) {
                    $scope.common.candidateModel.VisaTypeList = [];
                }

                var visaTypeDetails = _.find($scope.lookup.visaType, function (o) { return o.Name == candidateModel.VisaType });

                $scope.common.candidateModel.VisaTypeList.push({ "VisaTypeId": visaTypeDetails.Id, "VisaType": candidateModel.VisaType, "ValidUpto": $filter('date')(candidateModel.VisaValidUpto, "MM/dd/yyyy") })
                visaTypeDetails = {};
                $scope.common.AddEditVisaTypeModal.$promise.then($scope.common.AddEditVisaTypeModal.hide);
                candidateModel.VisaType = '';
                candidateModel.VisaValidUpto = '';
            }
        } else {
            if ($scope.IsDuplicateVisaType == false && candidateModel.VisaType != "") {
                var itemEditedData = _.find($scope.common.candidateModel.VisaTypeList, function (o) { return o.VisaTypeId == candidateModel.VisaTypeId });
                itemEditedData.VisaType = candidateModel.VisaType;
                itemEditedData.ValidUpto = candidateModel.VisaValidUpto;
                $scope.common.AddEditVisaTypeModal.$promise.then($scope.common.AddEditVisaTypeModal.hide);
            }
        }
    }

    $scope.checkDulicateVisaType = function (visaType) {
        if ($scope.common.candidateModel.VisaTypeList.length > 0) {
            var type = _.find($scope.common.candidateModel.VisaTypeList, function (o) { return o.VisaType == visaType; });
            if (type != undefined) {
                $scope.IsDuplicateVisaType = true;
            } else {
                $scope.IsDuplicateVisaType = false;
            }
        }
    }

    $scope.closeAddNewCityToMasterModal = function () {
        $scope.common.addCityMasterModal.$promise.then($scope.common.addCityMasterModal.hide);
        $scope.common.addCityMaster = {};
    };

    $scope.createCandidateProcess = function (createCandidateForm) {

        if (new ValidationService().checkFormValidity(createCandidateForm) &&
                                    $scope.common.candidateModel.Resume &&
                                    $scope.common.candidateModel.Resume.name &&
                                    !$scope.common.invalidProfileImgFile &&
                                    !$scope.common.maxProfileImgFileSize) {
            $scope.common.displayValidationSummary = false;
            $scope.common.maxProfileImgFileSize = false;
            $scope.common.invalidProfileImgFile = false;
            blockUI.start();
            $scope.cityValuebyId($scope.common.candidateModel.CityId);
            $scope.stateValuebyId($scope.common.candidateModel.StateId);
            $scope.countryValuebyId($scope.common.candidateModel.CountryId);
            $scope.common.candidateModel.BirthDate = $filter('date')($scope.common.candidateModel.BirthDate, "MM/dd/yyyy");
            $scope.common.candidateModel.PassportValidUpto = $filter('date')($scope.common.candidateModel.PassportValidUpto, "MM/dd/yyyy");
            $scope.common.candidateModel.VisaValidUpto = $filter('date')($scope.common.candidateModel.VisaValidUpto, "MM/dd/yyyy");
            $rootScope.skillSet = $scope.common.candidateModel.Skills;

            //image file path
            if ($scope.common.candidateModel.ProfileImageFile == null) {
                $scope.common.candidateModel.ProfileImagePath = null;
            }

            //To add resume
            if ($scope.common.candidateModel.Resume) {
                $scope.common.candidateModel.ResumeList = [];
                $scope.common.candidateModel.ResumeList.push($scope.common.candidateModel.Resume);
            }

            //for parsed candidate
            if ($scope.common.candidateModel.manipulationMode === 'CreateParsed' || $scope.common.candidateModel.manipulationMode === 'CreateParsedResume') {
                var parsedResumeId = $scope.common.candidateModel._id;
                console.log(parsedResumeId);
                $scope.common.candidateModel._id = undefined;
            }

            var promise = XSeedApiFactory.createCandidate($scope.common.candidateModel);
            promise.then(
              function (response) {
                  //for parsed candidate
                  if (($scope.common.candidateModel.manipulationMode === 'CreateParsed' || $scope.common.candidateModel.manipulationMode === 'CreateParsedResume') && parsedResumeId) {
                      //IsValidResume is true
                      $scope.acceptRejectParsedResume(parsedResumeId, true);
                  }
                  blockUI.stop();
                  XSeedAlert.swal({
                      title: 'Success!',
                      text: 'Candidate created successfully!',
                      type: "success",
                      customClass: "xseed-error-alert",
                      allowOutsideClick: false
                  }).then(function () {
                      $timeout(function () {
                          $location.path('candidateList');
                      }, 0);
                  }, function (dismiss) {
                      blockUI.stop();
                  });
              },
              function (httpError) {
                  blockUI.stop();
                  //to persists parsedResumeId of parsed canddate if email already exist.
                  if (parsedResumeId) {
                      $scope.common.candidateModel._id = parsedResumeId;
                  }
                  ExceptionHandler.handlerPopup('Candidate', httpError.data.Message, 'Info', 'info');
              });
        }
        else {
            //$scope.common.resumePresent = true;
            //if ((!$scope.common.candidateModel.Resume.data) || ($scope.common.candidateModel.Resume === undefined)) {
            if ($scope.common.candidateModel.Resume === undefined) {
                $scope.common.resumePresent = true;
                $scope.common.candidateModel.Resume = {};
            }
            $scope.common.displayValidationSummary = true;
        }
    };


    $scope.createCandidateView = function () {
        //$scope.common.candidateModel = {};
        $scope.common.resumePresent = false;
        $scope.common.invalid = false;
        $scope.common.maxSize = false;
        $scope.common.invalidResume = false;
        $scope.common.maxResumeSize = false;
        $scope.common.candidateModel = new API.CandidateRequest();
        $scope.common.candidateModel.VisaTypeList = [];
        $scope.common.candidateModel.manipulationMode = 'Create';
        //$scope.getCandidateId();
        $scope.common.candidateModel.Educations = [];
        $scope.common.candidateModel.EmployementHistory = [];
        $scope.common.candidateModel.VisaType = "NA";
        $scope.common.candidateModel.SendJobAlert = false;
        $scope.common.candidateModel.HavePassport = false;
        $scope.common.candidateModel.CanReLocate = false;
        $scope.common.displayValidationSummary = false;
        $scope.common.maxProfileImgFileSize = false;
        $scope.common.invalidProfileImgFile = false;

        $scope.candidateLookupProcess();
        $location.path('createCandidate');
    }

    $scope.getCandidateId = function () {
        var promise = XSeedApiFactory.getCandidateId();
        promise.then(
          function (response) {
              $scope.common.candidateModel.CandidateId = response[0].data;
              blockUI.stop();
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('CandidateUser', httpError.data.Status, httpError.data.Message);
          });


    };

    $scope.checkDuplicateCandidateId = function (candidateId) {
        $scope.duplicateCandidateId = false;
        if (candidateId) {
            var promise = XSeedApiFactory.IsDuplicateCandidateId(candidateId);
            promise.then(
              function (response) {
                  $scope.duplicateCandidateId = response[0].data;

                  if (($scope.common.candidateModel.manipulationMode == "Edit") && (candidateId == $scope.common.candidateModel.CandidateId)) {
                      $scope.duplicateCandidateId = false;
                  }
                  blockUI.stop();
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('CandidateUser', httpError.data.Status, httpError.data.Message);
              });
        }
        else {
            $scope.duplicateCandidateId = false;
        }
    };

    $scope.getCandidateList = function () {

        if ($rootScope.userHasToken == false) {
            $location.path('login');
            return false;
        }

        var createCandidateURL = '/createCandidate';
        var editCandidateURL = '/editCandidate';
        var candidateListURL = '/candidateList';
        var candidateDetailURL = '/candidateDetail';

        var dashboardURL = '/dashboard';

        if (($location.path().toLowerCase() == createCandidateURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Candidate_Create == "False") {
            $location.path('dashboard');
            return false;
        }

        if (($location.path().toLowerCase() == editCandidateURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Candidate_Update == "False") {
            $location.path('dashboard');
            return false;
        }

        if (($location.path().toLowerCase() == candidateListURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Candidate_Read == "False") {
            $location.path('dashboard');
            return false;
        }

        if (($location.path().toLowerCase() == candidateDetailURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Candidate_Read == "False") {
            $location.path('dashboard');
            return false;
        }

        //to reset applied candidate list
        $rootScope.appliedCandidateList = undefined;

        //to get candidate list
        if ($scope.flag.advanceSearchFlag) {
            return;
        }
        blockUI.start();
        var firstPageFlag = 0;
        $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
        $scope.common.pageNumberShow = undefined;

        if ($scope.common.candidateModel.manipulationMode != "Edit") {
            var promise = XSeedApiFactory.getCandidateList($scope.common.pageSizeShow, $scope.common.pageNumberShow);
            promise.then(
              function (response) {
                  $scope.common.candidateList = response[0].data.Candidates;
                  $scope.common.totalCount = response[0].data.TotalCount;
                  $scope.common.totalPages = response[0].data.TotalPages

                  //------------------------------------------------------
                  //APPLY ADDITIONAL SCRIPTS
                  //------------------------------------------------------
                  $(".select2").select2();
                  //------------------------------------------------------
                  

                  //to show skills in tooltip as string
                  angular.forEach($scope.common.candidateList, function (val, index) {
                      if (val.Skills) {
                          val.Skills = val.Skills.join();
                      }
                  });

                  firstPageFlag = 1;
                  if ($scope.common.candidateList.length) {
                      populateCandidateList($scope.common.candidateList, $scope.common.totalCount, $scope.common.totalPages, firstPageFlag);
                      $scope.candidatePageLookupList();

                      //to select first row by default
                      $scope.setClickedRowCandidate($scope.common.candidateList[0]);
                  }

                  blockUI.stop();

                  //populate filters
                  $timeout(function () {
                      $scope.populateCandidateFilterLookupList();

                  }, 2000);
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Candidate', httpError.data.Status, httpError.data.Message);
              });
        }
    };

    function populateCandidateList(candidateList, totalCount, totalPages, pageFlag) {
        if (angular.isDefined($scope.common.tblCandidateList)) {
            $scope.common.tblCandidateList.reload();
        }
        else {
            $scope.common.tblCandidateList = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: $scope.common.ngTablePaginationCount,
                total: totalCount,
                getData: function ($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    if (pageFlag != 1) {
                        /* sorting */
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function (val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */

                        var promise = XSeedApiFactory.getCandidateList($scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder);
                        promise.then(
                          function (response) {
                              $scope.common.candidateList = response[0].data.Candidates;
                              $scope.common.totalCount = response[0].data.TotalCount;
                              $scope.common.totalPages = response[0].data.TotalPages;
                              blockUIConfig.autoBlock = false;
                              //to select first row by default
                              if ($scope.common.candidateModel.manipulationMode != "Edit") {
                                  $scope.setClickedRowCandidate($scope.common.candidateList[0]);
                              }
                              else {
                                  if ($scope.common.selectedCandidate) {
                                      $scope.getActivityListCandidate();
                                  }
                                  $scope.common.candidateModel.manipulationMode = undefined;
                              }
                              //$scope.candidatePageLookupList();

                              //to show skills in tooltip as string
                              angular.forEach($scope.common.candidateList, function (val, index) {
                                  if (val.Skills) {
                                      val.Skills = val.Skills.join();
                                  }
                              });
                              blockUI.stop();

                          },
                          function (httpError) {
                              blockUI.stop();
                              ExceptionHandler.handlerHTTPException('Candidate', httpError.data.Status, httpError.data.Message);
                          });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.common.candidateList);
                }
                , $scope: $scope
            });
        }

    };

    $scope.populateCandidateFilterLookupList = function () {
        $scope.candidateFilterLookup = [];
        var organizationUserArray = [];
        var self = angular.copy(_.find($scope.lookup.organizationUser, function (o) { return o.Id == $rootScope.userDetails.Id; }));
        if (self) {
            self.Name = "Me";
        }
        organizationUserArray.push(self);

        angular.forEach($scope.lookup.organizationUser, function (val, index) {
            if ($scope.lookup.organizationUser[index].Id != $rootScope.userDetails.Id) {
                organizationUserArray.push($scope.lookup.organizationUser[index]);
            }
        });

        $scope.candidateFilterLookup.push({ "Status": $scope.lookup.candidateStatus });
        $scope.candidateFilterLookup.push({ "Created By": organizationUserArray });
        //$scope.candidateFilterLookup.push({ "Company": $scope.lookup.company });
    };

    $scope.getCandidateSelectedFilterItem = function () {
        var Status = [];
        var CreatedBy = [];
        var Company = [];

        angular.forEach($scope.candidateFilterLookup, function (val1, index1) {
            angular.forEach(val1, function (val2, index2) {
                angular.forEach(val2, function (val3, index3) {
                    if (index1 == 0 && val3.selected == true)
                        Status.push(val3.Name);
                    else if (index1 == 1 && val3.selected == true)
                        CreatedBy.push(val3.Id);
                    else if (index1 == 2 && val3.selected == true)
                        Company.push(val3.Name);
                });
            });
        });

        var filterSelection = {};
        filterSelection.Status = Status;
        filterSelection.CreatedBy = CreatedBy;
        filterSelection.Company = Company;
        filterSelection.OrganizationId = $rootScope.userDetails.OrganizationId;
        filterSelection.OrganizationUserId = $rootScope.userDetails.UserId;
        $scope.common.advanceSearchModel.filter = filterSelection;

        $scope.getCandidateFilterResult();
    }

    $scope.addFilterItemButton = function (type, id, name, selected) {
        if (selected == true) {
            $scope.filterItemLable.push({ Id: id, Name: name });
            $scope.filterItemCount = $scope.filterItemCount + 1;
        }
        else {
            $scope.removeFilterItemButton(id);
        }
    }

    $scope.removeFilterItemButton = function (id) {
        angular.forEach($scope.filterItemLable, function (val, index) {
            if (val && val.Id == id) {
                $scope.filterItemLable[index] = undefined;
            }
        });

        removeItemFromFilterList(id);
        $scope.filterItemCount = $scope.filterItemCount - 1;
    }

    function removeItemFromFilterList(id) {
        angular.forEach($scope.candidateFilterLookup, function (val1, index1) {
            angular.forEach(val1, function (val2, index2) {
                angular.forEach(val2, function (val3, index3) {
                    if (val3.Id == id) {
                        val3.selected = false;
                    }
                });
            });
        });
    }

    function clearFilter() {
        angular.forEach($scope.candidateFilterLookup, function (val1, index1) {
            angular.forEach(val1, function (val2, index2) {
                angular.forEach(val2, function (val3, index3) {
                    if (val3.selected == true) {
                        $scope.removeFilterItemButton(val3.Id);
                    }
                });
            });
        });
    }

    $scope.buildCandidateDegreeStructuredData = function () {
        $scope.degree = [{}];
        var arrDegree = [];
        var res = $scope.lookup.degree;
        angular.forEach(res, function (val, index) {
            arrDegree.push({ value: val.Name, label: val.Name });
        });

        blockUI.stop();
        $scope.lookup.degreeListData = arrDegree;
    }

    $scope.cityValuebyId = function (Id) {
        angular.forEach($scope.cityList, function (val, index) {
            if (val.Id == Id) {
                $scope.common.candidateModel.City = val.Name;
            }
        });
    };

    $scope.stateValuebyId = function (Id) {
        angular.forEach($scope.stateList, function (val, index) {
            if (val.Id == Id) {
                $scope.common.candidateModel.State = val.Name;
            }
        });
    };

    $scope.countryValuebyId = function (Id) {
        angular.forEach($rootScope.metadata.GetAncillaryInformationResponse.countryList.country, function (val, index) {
            if (val.Id == Id) {
                $scope.common.candidateModel.Country = val.Name;
            }
        });
    };

    //Checkbox code-
    $scope.checkboxes = { 'checked': false, items: {} };

    // watch for check all checkbox
    $scope.$watch('checkboxes.checked', function (value) {
        angular.forEach($scope.common.candidateList, function (item) {
            if (angular.isDefined(item._id)) {
                $scope.checkboxes.items[item._id] = value;
            }
        });
    });

    // watch for data checkboxes
    $scope.$watch('checkboxes.items', function (values) {
        if (!$scope.common.candidateList) {
            return;
        }
        var checked = 0, unchecked = 0,
            total = $scope.common.candidateList.length;
        angular.forEach($scope.common.candidateList, function (item) {
            checked += ($scope.checkboxes.items[item._id]) || 0;
            unchecked += (!$scope.checkboxes.items[item._id]) || 0;
        });
        if ((unchecked == 0) || (checked == 0)) {
            $scope.checkboxes.checked = (checked == total);
        }

        $scope.checkedCount = checked;
        //alert(checked);
        //alert(unchecked);
        //grayed checkbox
        angular.element(document.getElementById("select_all")).prop("indeterminate", (checked != 0 && unchecked != 0));
    }
    , true
    );

    $scope.enableFileButton = function (file, field) {
        if (file) {
            if (field == 'profileImage') {
                $scope.common.candidateModel.ProfileImageFile = null;
            }
            if (field == 'resumeFile') {
            }
        }
        $scope.common.maxProfileImgFileSize = false;
        $scope.common.invalidProfileImgFile = false;
    };

    $scope.editCandidateProcess = function (editCandidateForm) {
        if (new ValidationService().checkFormValidity(editCandidateForm) &&
                                    $scope.common.candidateModel.Resume &&
                                    $scope.common.candidateModel.Resume.name &&
                                    !$scope.common.invalidProfileImgFile &&
                                    !$scope.common.maxProfileImgFileSize) {
            $scope.common.displayValidationSummary = false;
            $scope.common.maxProfileImgFileSize = false;
            $scope.common.invalidProfileImgFile = false;
            blockUI.start();
            $scope.cityValuebyId($scope.common.candidateModel.CityId);
            $scope.stateValuebyId($scope.common.candidateModel.StateId);
            $scope.countryValuebyId($scope.common.candidateModel.CountryId);
            $scope.common.candidateModel.BirthDate = $filter('date')($scope.common.candidateModel.BirthDate, "MM/dd/yyyy");
            $scope.common.candidateModel.PassportValidUpto = $filter('date')($scope.common.candidateModel.PassportValidUpto, "MM/dd/yyyy");
            $scope.common.candidateModel.VisaValidUpto = $filter('date')($scope.common.candidateModel.VisaValidUpto, "MM/dd/yyyy");
            $rootScope.skillSet = $scope.common.candidateModel.Skills;

            //image file path
            if ($scope.common.candidateModel.ProfileImageFile == null) {
                $scope.common.candidateModel.ProfileImagePath = null;
            }

            // IsCandidate false
            $scope.common.candidateModel.IsCandidate = false;

            //To add resume

            if ($scope.common.candidateModel.Resume) {
                $scope.common.candidateModel.ResumeList = [];
                $scope.common.candidateModel.ResumeList.push($scope.common.candidateModel.Resume);
            }

            var promise = XSeedApiFactory.editCandidate($scope.common.candidateModel);
            promise.then(
              function (response) {
                  blockUI.stop();
                  XSeedAlert.swal({
                      title: 'Success!',
                      text: 'Candidate updated successfully!',
                      type: "success",
                      customClass: "xseed-error-alert",
                      allowOutsideClick: false
                  }).then(function () {
                      $timeout(function () {
                          $location.path('candidateList');
                      }, 0);
                  }, function (dismiss) {
                      blockUI.stop();
                  });
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Candidate', httpError.data.Status, httpError.data.Message);
              });
        }
        else {
            //if (!$scope.common.candidateModel.Resume.data)
            if ($scope.common.candidateModel.Resume === undefined) {
                $scope.common.resumePresent = true;
                $scope.common.candidateModel.Resume = {};
            }
            $scope.common.displayValidationSummary = true;
        }
    };


    $scope.submitCandidateProcess = function (candidateSubmissionPopupForm) {
        if (new ValidationService().checkFormValidity(candidateSubmissionPopupForm)) {
            blockUI.start();

            var list = [];
            for (var i = 0; i < $scope.candidateSubmissionList.length; i++) {
                var object = {};
                object.Id = $scope.candidateSubmissionList[i]._id;
                object.Name = '';

                list.push(object);
            }

            $scope.CandidateSubmissionModel.CandidateList = list;

            $scope.CandidateSubmissionModel.OrganizationUserId = $rootScope.userDetails.UserId;
            var promise = XSeedApiFactory.submitCandidate($scope.CandidateSubmissionModel);
            promise.then(
              function (response) {
                  blockUI.stop();
                  $scope.closeCandidateSubmissionModal();
                  $scope.dataSubmissiomEmail.successCandidates = response[0].data.successCandidates;
                  $scope.dataSubmissiomEmail.companyId = $scope.CandidateSubmissionModel.CompanyId;
                  $scope.dataSubmissiomEmail.jobId = $scope.CandidateSubmissionModel.JobId;
                  $scope.CandidateSubmissionModel.CompanyId = "";
                  $scope.CandidateSubmissionModel.JobId = "";
                  if ($scope.dataSubmissiomEmail.successCandidates.length > 0) {
                      XSeedAlert.swal({
                          title: 'Success!',
                          text: 'Candidate submitted!',
                          type: "success",
                          customClass: "xseed-error-alert"
                      }).then(function () {
                          $scope.getSubmissionUrl($scope.dataSubmissiomEmail.companyId);
                          candidateSubmissionEmailModal.$promise.then(candidateSubmissionEmailModal.show);
                          $timeout(function () {
                              angular.forEach($scope.checkboxes.items, function (item, Identifier) {
                                  $scope.checkboxes.items[Identifier] = false;
                              });
                          }, 0);
                      }, function (dismiss) {
                          blockUI.stop();
                      })
                  }
                  else {
                      blockUI.stop();
                      ExceptionHandler.handlerPopup('Submission', response[0].data.message, 'Info!', 'info');
                  };
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
              });
        };
    };



    $scope.submitBySubmissionUrl = function () {
        $scope.isSubmitBySubmissionUrl = true;
        $scope.closeCandidateSubmissionEmailModal();
        $location.path("submissionToClient");
    };


    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }
    $scope.openCandidateSubmissionEmailModal = function () {
        if ($scope.dataSubmissiomEmail.successCandidates.length > 0) {
            blockUI.start();
            //get company contacts
            var promise = XSeedApiFactory.getSubmissionContactEmail($scope.dataSubmissiomEmail.companyId);
            promise.then(
              function (response) {
                  $rootScope.commonEmailModel.candidateList = [];

                  //get company submissionUrl
                  var promise = XSeedApiFactory.getCompanyDetail($rootScope.userDetails.OrganizationId, $scope.dataSubmissiomEmail.companyId);
                  promise.then(
                    function (response) {
                        $scope.companyData = response[0].data;
                        $rootScope.commonEmailModel.companySubmissionUrl = $scope.companyData.SubmissionPageURL;
                    },
                    function (httpError) {
                        ExceptionHandler.handlerHTTPException('Company', httpError.data.Status, httpError.data.Message);
                    });
                  //get company contact email
                  if (response[0].data) {
                      $rootScope.commonEmailModel.ToEmailID = [];
                      angular.forEach(response[0].data, function (val, index) {
                          $rootScope.commonEmailModel.ToEmailID.push(val.Name);
                      });
                      $rootScope.commonEmailModel.ToEmailID = $rootScope.commonEmailModel.ToEmailID.join();
                  }
                  $rootScope.commonEmailModel.FromEmailID = $rootScope.userDetails.PrimaryEmail;
                  $rootScope.commonEmailModel.MailSubject = "Submissions";
                  $rootScope.commonEmailModel.MailBody = "We would like to submit following candidate(s) against the<br />Requirement: ";
                  //get job title
                  angular.forEach($scope.lookup.requirement, function (val, index) {
                      if ($scope.dataSubmissiomEmail.jobId == val.Id) {
                          $rootScope.commonEmailModel.MailBody = $rootScope.commonEmailModel.MailBody + val.JobTitle;
                      }

                  });
                  //get candidate names
                  $rootScope.commonEmailModel.MailBody = $rootScope.commonEmailModel.MailBody + " <br />following is/are the candidate(s)";
                  angular.forEach($scope.dataSubmissiomEmail.successCandidates, function (val, index) {
                      var selectedCandidate = _.find($scope.common.candidateList, function (o) { return o._id == val; });
                      $rootScope.commonEmailModel.candidateList.push(selectedCandidate._id);
                      index = index + 1
                      $rootScope.commonEmailModel.MailBody = $rootScope.commonEmailModel.MailBody + "<br />" + index + ". " + selectedCandidate.FirstName + " " + selectedCandidate.LastName;
                  });
                  $rootScope.commonEmailModel.typeOfNotification = $scope.constants.submitCandidateByEmail;
                  $scope.common.commonEmailModal.$promise.then($scope.common.commonEmailModal.show);
                  blockUI.stop();
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
              });
        }

    }

    $scope.closeCandidateSubmissionEmailModal = function () {
        candidateSubmissionEmailModal.$promise.then(candidateSubmissionEmailModal.hide);
        $scope.checkboxes.items = {};
    }

    $scope.submitPopupCancel = function () {
        $scope.candidateSubmissionList = [];
        $scope.CandidateSubmissionModel.CompanyId = "";
        $scope.CandidateSubmissionModel.JobId = "";
        $scope.closeCandidateSubmissionModal();
    }

    $scope.getRequirementListByCompany = function (companyId) {
        var promise = XSeedApiFactory.getRequirementListByCompany(companyId);
        promise.then(
          function (response) {
              $scope.lookup.requirement = response[0].data;
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
          });
    };

    $scope.candidateCancel = function () {
        XSeedAlert.swal({
            title: "Are you sure you want to cancel?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function () {
            blockUI.start();
            $scope.common.candidateModel.manipulationMode = "Edit";
            setTimeout(function () {
                $scope.resetCandidateAdvanceSearch();
            }, 100);
            $location.path('candidateList');
            blockUI.stop();
        }, function (dismiss) { });
    };

    $scope.goToCandidateList = function () {
        setTimeout(function () {
            $scope.resetCandidateAdvanceSearch();
        }, 100);

        $location.path('candidateList');
    };

    //Advance Search
    $scope.resetCandidateAdvanceSearch = function () {
        if ($scope.common.candidateModel.manipulationMode != "Edit") {
            $scope.common.advanceSearchModel = {};
            $scope.flag.advanceSearchFlag = false;
            $scope.common.tblCandidateList = undefined;
            $scope.getCandidateList();
            clearFilter();
        }
        blockUI.stop();
    };

    $scope.uploadResume = function (files) {
        $scope.common.resumePresent = false;
        $scope.common.maxResumeSize = false;
        $scope.common.invalidResume = false;
        if (files.length != 0) {
            $scope.resumeFileName = files[0].name;
            var ext = files[0].name.match(/(?:\.([^.]+))?$/)[1];
            if (files[0].size >= 2000000) {
                $scope.common.maxResumeSize = true;
            }
            else {
                if (angular.lowercase(ext) === 'pdf' || angular.lowercase(ext) === 'doc' || angular.lowercase(ext) === 'docx' || angular.lowercase(ext) === 'txt') {
                    $scope.common.maxResumeSize = false;
                    $scope.common.invalidResume = false;
                }
                else {
                    $scope.common.invalidResume = true;
                    $scope.common.candidateModel.Resume = undefined;
                    $scope.common.resumePresent = false;
                }
            }
        }
        else {
            $scope.common.invalidResume = false;
            $scope.common.maxResumeSize = false;
            $scope.enableButtonResume();
        }
    }

    $scope.enableButtonResume = function (resume) {
        if (resume && resume.name) {
            $scope.common.maxSize = false;
            var ext = resume.name.match(/\.(.+)$/)[1];
            if (angular.lowercase(ext) === 'pdf' || angular.lowercase(ext) === 'doc' || angular.lowercase(ext) === 'docx' || angular.lowercase(ext) === 'txt') {
                if ($scope.common.candidateModel.Resume) {
                    $scope.common.candidateModel.Resume.data = resume.data;
                    $scope.common.invalidResume = false;
                }
                else {
                    $scope.common.candidateModel.Resume.data = $scope.common.candidateModel.Resume.data;
                }
                $scope.common.invalidResume = false;
                //$scope.common.candidateModel.Resume.data = "";
            }

        }
        else {
            $scope.common.resumePresent = true;
            $scope.common.resumePresent = false;
            $scope.common.candidateModel.Resume = undefined;
        }


        //if (resume) {
        //    $scope.common.maxResumeSize = false;
        //    console.log("resume present ");
        //    if ($scope.common.candidateModel.Resume) {
        //        $scope.common.candidateModel.Resume.data = resume.data;
        //    }
        //    else {
        //        //$scope.common.candidateModel.Resume.data = $scope.common.candidateModel.Resume.data;
        //    }
        //}
        //else {
        //    console.log("resume not present ");
        //}
        //$scope.common.invalidResume = false;
    }

    //Resume parser-
    $scope.getCandidateResumeParserResult = function () {
        blockUI.start();
        var candidateId = $scope.common.candidateModel.CandidateId;
        var resume = $scope.common.candidateModel.Resume;
        var promise = XSeedApiFactory.getCandidateResumeParserCall($scope.common.candidateModel.Resume, $scope.common.candidateModel._id);
        promise.then(
        function (response) {
            $scope.common.candidateModel = response[0].data;
            if ($scope.common.candidateModel.CountryId) {
                $scope.getStateListByCountry($scope.common.candidateModel.CountryId);
            }

            if ($scope.common.candidateModel.StateId) {
                $scope.getCityListByState($scope.common.candidateModel.StateId);
            }

            var res = $scope.common.candidateModel.Skills;
            angular.forEach(res, function (val, index) {
                $scope.lookup.skillListData.push({ value: val, label: val });
            });
            $scope.common.candidateModel.Resume = resume;
            $scope.common.candidateModel.ResumeContent = $scope.common.candidateModel.Resume.data;

            //if ($scope.common.candidateModel.TotalExperience || $scope.common.candidateModel.RelevantExperience || $scope.common.candidateModel.CurrentSalary || $scope.common.candidateModel.ExpectedSalary || $scope.common.candidateModel.CurrentLocation || $scope.common.candidateModel.PreferredLocation || $scope.common.candidateModel.ProfileTitle || $scope.common.candidateModel.ProfileSummary || $scope.common.candidateModel.Designation) {
            //    $scope.common.candidateModel.IsExperienced = true;
            //}
            $scope.common.candidateModel.CandidateId = candidateId;
            $scope.common.candidateModel.manipulationMode = "Create";
            $location.path("createCandidate");
            blockUI.stop();
        },
        function (httpError) {
            blockUI.stop();
            if (httpError.data.Message == "Resume not parsed successfully!") {
                ExceptionHandler.handlerPopup('ResuemParser', httpError.data.Message, 'Info', 'warning');
            }
            else {
                ExceptionHandler.handlerHTTPException('ResuemParser', httpError.data.Status, httpError.data.Message);
            }
        });
    }


    $scope.standaloneCandidateResumeParserResult = function () {
        blockUI.start();
        $scope.candidateResumeInputModel.Id = null;
        //var promise = XSeedApiFactory.getCandidateResumeParserCall($scope.candidateResumeInputModel);
        var promise = XSeedApiFactory.getCandidateResumeParserCall($scope.candidateResumeInputModel.resume, $scope.candidateResumeInputModel.Id);
        promise.then(
        function (response) {
            $scope.candidateResumeOutputModel = response[0].data;
            $scope.candidateResumeOutputModel.SchoolList = [];
            $scope.candidateResumeOutputModel.DegreeList = [];
            angular.forEach($scope.candidateResumeOutputModel.Educations, function (item, Identifier) {
                $scope.candidateResumeOutputModel.SchoolList.push(item.School);
                $scope.candidateResumeOutputModel.DegreeList.push(item.Course);
            });
            blockUI.stop();
        },
        function (httpError) {
            blockUI.stop();
            ExceptionHandler.handlerHTTPException('ResuemParser', httpError.data.Status, httpError.data.Message);
        });
    }

    $scope.getResumeEmailParsingResult = function (selectedResumeEmail) {
        var isFileOk = false;
        // for each to check is attachments contains image files
        angular.forEach(selectedResumeEmail.EmailAttachment, function (val, index) {
            var ext = val.Name.match(/(?:\.([^.]+))?$/)[1];
            if (ext.toLowerCase() != 'jpg' && ext.toLowerCase() != 'jpeg' && ext.toLowerCase() != 'png') {
                selectedResumeEmail.EmailAttachment[0] = val;
                return isFileOk = true;
            }
        });

        if (isFileOk) {

            if (selectedResumeEmail.EmailAttachment[0]) {
                var promise = XSeedApiFactory.getCandidateResumeParserCall(selectedResumeEmail.EmailAttachment[0]);
                promise.then(
                function (response) {
                    $scope.common.candidateModel = response[0].data;
                    if ($scope.common.candidateModel.CountryId) {
                        $scope.getStateListByCountry($scope.common.candidateModel.CountryId);
                    }

                    if ($scope.common.candidateModel.StateId) {
                        $scope.getCityListByState($scope.common.candidateModel.StateId);
                    }

                    var res = $scope.common.candidateModel.Skills;
                    angular.forEach(res, function (val, index) {
                        $scope.lookup.skillListData.push({ value: val, label: val });
                    });

                    $scope.common.candidateModel.Resume = {};
                    $scope.common.candidateModel.Resume.name = selectedResumeEmail.EmailAttachment[0].Name;
                    $scope.common.candidateModel.Resume.data = selectedResumeEmail.EmailAttachment[0].Data.includes(",") ? selectedResumeEmail.EmailAttachment[0].Data : ("data:;base64," + selectedResumeEmail.EmailAttachment[0].Data);
                    $scope.common.candidateModel.ResumeContent = $scope.common.candidateModel.Resume.data;
                    console.log("$scope.common.candidateModel.Resume" + $scope.common.candidateModel.Resume.data);

                    $scope.common.resumePresent = false;
                    $scope.common.invalid = false;
                    $scope.common.maxSize = false;
                    $scope.common.invalidResume = false;
                    $scope.common.displayValidationSummary = false;
                    $scope.common.maxResumeSize = false;
                    $scope.common.candidateModel.manipulationMode = "CreateParsedResume";
                    blockUI.stop();

                    $location.path("createCandidate");

                },
                function (httpError) {
                    blockUI.stop();
                    ExceptionHandler.handlerHTTPException('ResuemParser', httpError.data.Status, httpError.data.Message);
                });
            }
        }
        else {
            blockUI.stop();
            ExceptionHandler.handlerPopup('ResuemParser', 'Image files are not supporrted.', 'Info', 'warning');
        }
    }

    $scope.addEducationRecord = function () {
        if (!$scope.common.candidateModel.Educations)
            $scope.common.candidateModel.Educations = [];
        $scope.common.candidateModel.Educations.push({ "StartDate": "", "EndDate": "", "DegreeDate": "", "SchoolName": "", "SchoolType": "", "DegreeName": "", "DegreeType": "", "Major": "" });
    }

    $scope.removeEducationRecord = function (index) {
        $scope.common.candidateModel.Educations.splice(index, 1);
    }

    $scope.addEmploymentHistoryRecord = function () {
        if (!$scope.common.candidateModel.EmployementHistory)
            $scope.common.candidateModel.EmployementHistory = [];
        $scope.common.candidateModel.EmployementHistory.push({ "StartDate": "", "EndDate": "", "CompanyName": "", "JobTitle": "", "Salary": "", "SalaryType": "", "AdditionalSalary": "", "AdditionalSalaryDescription": "", "ChangeReason": "", "IsCurrent": "", "EmployementType": "" });
    }

    $scope.removeEmploymentHistoryRecord = function (index) {
        $scope.common.candidateModel.EmployementHistory.splice(index, 1);
    }

    /****************************** send mail to candidate ************************************/

    $scope.sendMailToSelectedCandidate = function () {
        $rootScope.commonEmailModel = {};
        $rootScope.commonEmailModel.MailSubject = "";
        $rootScope.commonEmailModel.MailBody = "Hi " + $scope.common.selectedCandidate.FirstName;
        $rootScope.commonEmailModel.Id = $scope.common.selectedCandidate._id;
        $rootScope.commonEmailModel.typeOfNotification = $scope.constants.sendCandidateMail;

        if ($scope.common.selectedCandidate.PrimaryEmail != null) {
            $rootScope.commonEmailModel.ToEmailID = [];
            $rootScope.commonEmailModel.ToEmailID.push($scope.common.selectedCandidate.PrimaryEmail);
            $rootScope.commonEmailModel.ToEmailID = $rootScope.commonEmailModel.ToEmailID.join();
            $rootScope.commonEmailModel.FromEmailID = $rootScope.userDetails.UserName;
        };

        $scope.common.commonEmailModal.$promise.then($scope.common.commonEmailModal.show);
    };

    /****************************** send mail to candidate ************************************/

    //***************************************************************************************//

    // Get the render context local to this controller (and relevant params).
    var renderContext = RequestContext.getRenderContext("standard.candidate");
    // The subview indicates which view is going to be rendered on the page.
    $scope.subview = renderContext.getNextSection();
    $scope.$on(
        "requestContextChanged",
        function () {
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {
                return;
            }
            // Update the view that is being rendered.
            $scope.subview = renderContext.getNextSection();
        }
    );
};