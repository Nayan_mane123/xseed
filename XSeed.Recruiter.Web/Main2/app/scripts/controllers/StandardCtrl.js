'use strict';

XSEED_APP.controller('StandardCtrl', StandardCtrl);

/**
 * @ngInject
 */
function StandardCtrl($rootScope, $scope, RequestContext, $log, ExceptionHandler, i18nFactory, localStorageService, $window, $location, blockUI, XSeedApiFactory, ModalFactory, XSeedAlert, $filter, ValidationService, $timeout, $interval, $compile)
{


    $scope.changePasswordModel = {};
    
    // Get the render context local to this controller (and relevant params).
    var renderContext = RequestContext.getRenderContext("standard");
    // The subview indicates which view is going to be rendered on the page.
    $scope.subview = renderContext.getNextSection();
    $scope.$on(
        "requestContextChanged",
        function () {
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {
                return;
            }
            // Update the view that is being rendered.
            $scope.subview = renderContext.getNextSection();
        }
    );

    $scope.logOut = function () {
        localStorageService.clearAll();
        $interval.cancel($rootScope.isValidUser);
        $location.path('login');
    };

    $scope.changePassword = function (changePasswordForm) {

        if (new ValidationService().checkFormValidity(changePasswordForm)) {
            blockUI.start();

            $scope.changePasswordModel.Id = $rootScope.userDetails.UserId;

            /********************************************************** Encryption Code ***********************************************************/

            $scope.tempChangePasswordModel = {};
            $scope.tempChangePasswordModel = angular.copy($scope.changePasswordModel);

            /* Code for Encryption Old Password Start */
            var key = CryptoJS.enc.Utf8.parse('4263193851707158');
            var iv = CryptoJS.enc.Utf8.parse('4263193851707158');

            $scope.oldPasswordEncrypted = $scope.tempChangePasswordModel.OldPassword;
            var timestamp = Date.parse(new Date());

            var encryptedlogin = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.oldPasswordEncrypted), key,
            {
                keySize: 128 / 8,
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });

            $scope.oldPasswordEncrypted = encryptedlogin;
            /* Code for Encryption Old Password End */

            /* New Password */
            $scope.newPasswordEncrypted = $scope.tempChangePasswordModel.NewPassword;
            var encryptedpassword = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.newPasswordEncrypted), key,
            {
                keySize: 128 / 8,
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });

            $scope.newPasswordEncrypted = encryptedpassword;
            /* New Password */

            /* Code for Encryption Password Start */
            $scope.confirmPasswordEncrypted = $scope.tempChangePasswordModel.ConfirmPassword;
            var encryptedpassword = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.confirmPasswordEncrypted), key,
            {
                keySize: 128 / 8,
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });

            $scope.confirmPasswordEncrypted = encryptedpassword;

            /********************************************************** Encryption Code ***********************************************************/
            /* Push encrypted values */
            $scope.tempChangePasswordModel.OldPassword = '' + $scope.oldPasswordEncrypted + '';
            $scope.tempChangePasswordModel.NewPassword = '' + $scope.newPasswordEncrypted + '';
            $scope.tempChangePasswordModel.ConfirmPassword = '' + $scope.confirmPasswordEncrypted + '';

            var promise = XSeedApiFactory.changePassword($scope.tempChangePasswordModel);
            promise.then(
              function (response) {
                  blockUI.stop();

                  XSeedAlert.swal({
                      title: 'Success!',
                      text: 'Your password has been changed successfully!',
                      type: "success",
                      customClass: "xseed-error-alert",
                      //showCancelButton: true,
                      //confirmButtonText: "OK",
                      //closeOnConfirm: true
                  }).then(function () {
                      $scope.changePasswordModel = {}
                      $timeout(function () {
                          $location.path('dashboard');
                      }, 0);
                  }, function (dismiss) {
                      //console.log('dismiss: ' + dismiss);
                      // dismiss can be 'cancel', 'overlay', 'close', 'timer'
                      //if (dismiss == 'cancel'){alert('Close fired');}
                  });
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerPopup('Warning', httpError.data.Message, 'Warning!', 'error');
              });
        };
    }

    $scope.gotoHome = function () {
        $scope.changePasswordModel = {};
        $location.path('dashboard');
    }

    $scope.goToView = function (viewPath, qparam=[]) {
        if (qparam.length>0)
        {
            $location.path(viewPath).search(qparam[0], qparam[1]);
        }
        else
        {
            $location.path(viewPath).search({});
        }
    };
};



