﻿'use strict';

XSEED_APP.controller('RequirementCtrl', RequirementCtrl);
XSEED_APP.constant('Constants', {
    Masters: {
        RequirementTitle: 'Requirement Title',
        RequirementType: 'Requirement Type',
        BusinessUnit: 'Business Unit',
        Degree: 'Degree'
    }
});
/**
 * @ngInject
 */
function RequirementCtrl($scope, $rootScope, $route, $routeParams, RequestContext, $timeout, $q, $location, $window, configuration, i18nFactory, ExceptionHandler, EventBus, $log, _, ipCookie, blockUI, ModalFactory, XSeedAlert, localStorageService, $uibModal, XSeedApiFactory, ngTableParams, $filter, ValidationService, $compile, Constants) {

    //$scope.common.jobList = [];
    $scope.skillList = [];
    $scope.common.requirementModel = new API.JobRequest();
    $scope.requirementFilter = {};
    $scope.requirementSearch = {};
    $scope.flag.advanceSearchFlag = false;
    $scope.candidateAdvanceSearchFlag = false;
    $scope.requirementSearchData = "";
    $scope.requirementOptions = {};
    $scope.requirementOptions.JobStatus = 'All';
    $scope.qNoteModel = {};
    $scope.showAddNewBusinessUnit = false;
    $scope.filterItemCount = 0;
    $scope.filterItemLable = [];
    $scope.genericLookup = {};
    $scope.common.advanceSearchModel = {};
    $scope.common.redirectedFrom = 'RequirementList';
    $scope.duplicateRequisitionId = false;
    $scope.checkedCount = 0;
    //$scope.common.requirementStatusFilter = "MyOpen";
    var submitCandidateFromRequirementModal = ModalFactory.submitCandidateFromRequirementModal($scope);
    var candidateSubmissionModal = ModalFactory.candidateSubmissionModal($scope);

    init();

    //Initialization Function
    function init() {

        $scope.getLookUpSkill();
        var requirementQueueUrl = "/requirementQueue";
        if (($scope.isUndefinedOrNull($scope.common.requirementModel.Id) || $scope.isEmpty($scope.common.requirementModel.Id)) && ($location.path().toLowerCase() != requirementQueueUrl.toLowerCase())) {
            $location.path('requirementList');
        }

        //to avoid reload
        $scope.common.tblRequirementList = undefined;
        $scope.showPayScaleSalryCalculator = false;
        var date = new Date();
        $scope.maxDate = ("0" + (date.getMonth() + 1).toString()).substr(-2) + "/" + ("0" + date.getDate().toString()).substr(-2) + "/" + (date.getFullYear().toString());
        $scope.minDate = ("0" + (date.getMonth() + 1).toString()).substr(-2) + "/" + ("0" + date.getDate().toString()).substr(-2) + "/" + (date.getFullYear().toString());
        $scope.closedDate = ("0" + (date.getMonth() + 1).toString()).substr(-2) + "/" + ("0" + date.getDate().toString()).substr(-2) + "/" + ((date.getFullYear() + 10).toString());
    };

    $scope.createFunction = function (input) {
        // format the option and return it
        return {
            value: $scope.lookup.skillListData.length,
            label: input
        };
    };

    //Create new company source in angular-selectors options
    $scope.createBusinessUnit = function () {
        // format the option and return it
        $scope.common.requirementModel.BusinessUnit = { 'Name': $scope.common.requirementModel.NewBusinessUnit };
        $scope.createNewBusinessUnit($scope.common.requirementModel.BusinessUnit);
        $scope.showAddNewBusinessUnit = false;
    };

    //send mail to company contact
    $scope.sendMailToAssignedCompanyContact = function () {
        $rootScope.commonEmailModel = {};
        $rootScope.commonEmailModel.MailSubject = $scope.common.selectedJob.JobTitle + " " + ($scope.common.selectedJob.RequisitionId == null ? "" : "(" + $scope.common.selectedJob.RequisitionId + ")");
        $rootScope.commonEmailModel.MailBody = "";
        $rootScope.commonEmailModel.Id = $scope.common.selectedJob.Id;
        $rootScope.commonEmailModel.typeOfNotification = $scope.constants.sendJobMail;

        if ($scope.common.selectedJob.CompanyContactName != null) {
            $rootScope.commonEmailModel.ToEmailID = [];
            $rootScope.commonEmailModel.ToEmailID.push($scope.common.selectedJob.CompanyContactName);
            $rootScope.commonEmailModel.ToEmailID = $rootScope.commonEmailModel.ToEmailID.join();
            $rootScope.commonEmailModel.FromEmailID = $rootScope.userDetails.UserName;
        };

        $scope.common.commonEmailModal.$promise.then($scope.common.commonEmailModal.show);
    };

    $scope.createRequirementView = function () {

        blockUI.start();
        $scope.common.requirementModel = new API.JobRequest();
        $scope.common.requirementModel.manipulationMode = 'Create';
        $scope.getRequisitionID();
        $scope.jobLookupProcess();
        angular.forEach($scope.lookup.jobStatus, function (val, index) {
            if (val.Name == 'Open') {
                $scope.common.requirementModel.JobStatusId = val.Id;
            }
        });
        $location.path('createRequirement');
        blockUI.stop();
    }

    $scope.getRequisitionID = function () {
        var organizationId = $rootScope.userDetails.OrganizationId;
        var promise = XSeedApiFactory.getRequisitionID(organizationId);
        promise.then(
          function (response) {
              $scope.common.requirementModel.RequisitionId = response[0].data;
              blockUI.stop();
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
          });


    };

    $scope.checkDuplicateRequirementIdNSaveJob = function (requisitionId, JobForm) {
        $scope.duplicateRequisitionId = false;
        var organizationId = $rootScope.userDetails.OrganizationId;
        if (requisitionId) {
            var promise = XSeedApiFactory.IsDuplicateRequisitionId(requisitionId, organizationId);
            promise.then(
              function (response) {
                  $scope.duplicateRequisitionId = response[0].data;

                  if (($scope.common.requirementModel.manipulationMode == "Edit") && (requisitionId == $scope.common.requisitionId)) {
                      $scope.duplicateRequisitionId = false;
                  }

                  if (!$scope.duplicateRequisitionId) {
                      if ($scope.common.requirementModel.manipulationMode == "Create") {
                          $scope.createJobProcess(JobForm);
                      }
                      if ($scope.common.requirementModel.manipulationMode == "Edit") {
                          $scope.editJobProcess(JobForm)
                      }
                  }
                  blockUI.stop();
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
              });
        }
        else {
            $scope.duplicateRequisitionId = false;
        }
    };

    $scope.getJobsByStatus = function (value) {
        /* #889 Requirement List page -> Filter requirement issue. System does not load the list as per filter option selection */
        if ($scope.common.requirementModel.manipulationMode == "Edit") {
            $scope.common.requirementModel.manipulationMode = undefined;
            value = $scope.common.requirementStatusFilter;
        }

        if (value === 'All') {
            //$scope.showPendingApprovalRequirement = false;
            $scope.getRequirementList();
        }
        if (value === 'MyAll') {
            if ($scope.flag.advanceSearchFlag) {
                $scope.flag.advanceSearchFlag = false;
                $scope.resetRequirementAdvanceSearch();
            }

            $scope.getRequirementListByStatus(null);
        }
        if (value === 'MyOpen') {
            if ($scope.flag.advanceSearchFlag) {
                $scope.flag.advanceSearchFlag = false;
                $scope.resetRequirementAdvanceSearch();
            }
            $scope.common.requirementStatusFilter = "MyOpen";
            $timeout(function () {
                $scope.getRequirementListByStatus('Open');
            }, 1000);
        }
        if (value === 'MyClosed') {
            if ($scope.flag.advanceSearchFlag) {
                $scope.flag.advanceSearchFlag = false;
                $scope.resetRequirementAdvanceSearch();
            }
            $scope.getRequirementListByStatus('Closed');
        }
        if (value === 'MyPending') {
            if ($scope.flag.advanceSearchFlag) {
                $scope.flag.advanceSearchFlag = false;
                $scope.resetRequirementAdvanceSearch();
            }
            $scope.getRequirementListByStatus('Pending');
        }
        if (value === 'MyHold') {
            if ($scope.flag.advanceSearchFlag) {
                $scope.flag.advanceSearchFlag = false;
                $scope.resetRequirementAdvanceSearch();
            }
            $scope.getRequirementListByStatus('Hold');
        }
    };

    $scope.getRequirementListByStatus = function (requirementStatus) {

        if ($rootScope.userHasToken == false) {
            $location.path('login');
            return false;
        }

        var createRquirementURL = '/createRequirement';
        var editRequirementURL = '/editRequirement';
        var requirementListURL = '/requirementList';
        var requirementDetailURL = '/requirementDetail';

        var dashboardURL = '/dashboard';

        if (($location.path().toLowerCase() == createRquirementURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Requirement_Create == "False") {
            $location.path('dashboard');
            return false;
        }
        if (($location.path().toLowerCase() == editRequirementURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Requirement_Update == "False") {
            $location.path('dashboard');
            return false;
        }

        if (($location.path().toLowerCase() == requirementListURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Requirement_Read == "False") {
            $location.path('dashboard');
            return false;
        }
        if (($location.path().toLowerCase() == requirementDetailURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Requirement_Read == "False") {
            $location.path('dashboard');
            return false;
        }

        if ($scope.flag.advanceSearchFlag) {
            return;
        }
        blockUI.start();
        $scope.jobLookupProcess();
        var firstPageFlag = 0;
        var organizationId = $rootScope.userDetails.OrganizationId;
        var userId = $rootScope.userDetails.UserId;
        $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
        $scope.common.pageNumberShow = undefined;
        if ($scope.common.requirementModel.manipulationMode != "Edit") {
            var promise = XSeedApiFactory.getRequirementListByStatus(organizationId, userId, requirementStatus, $scope.common.pageSizeShow, $scope.common.pageNumberShow);
            promise.then(
              function (response) {
                  $scope.common.jobList = response[0].data.Jobs;
                  $scope.common.totalCount = response[0].data.TotalCount;
                  $scope.common.totalPages = response[0].data.TotalPages;

                  if (response[0].data.Jobs.length > 0) {
                      $scope.firstRequisitionId = $scope.common.jobList[0].RequisitionId;

                      //Manupulation of response
                      angular.forEach($scope.common.jobList, function (val, index) {
                          if ($scope.common.jobList[index].TechnicalSkills != null) {
                              $scope.common.jobList[index].TechnicalSkills = $scope.common.jobList[index].TechnicalSkills.split("|");
                              $scope.common.jobList[index].TechnicalSkills = $scope.common.jobList[index].TechnicalSkills.join();
                          }
                      });

                      //to select first row by default
                      $scope.setClickedRow($scope.common.jobList[0]);

                      firstPageFlag = 1;
                      populateRequirementListByStatus(organizationId, userId, $scope.common.jobList, $scope.common.totalCount, $scope.common.totalPages, firstPageFlag, requirementStatus);

                      //to show pending requirement count in badge on requirement list
                      if ($rootScope.userDetails.RoleName == 'Application Administrator' || $rootScope.userDetails.RoleName == 'Delivery Manager' || $rootScope.userDetails.RoleName == 'Team Lead') {
                          getVendorUnreadMailCount();
                      }
                  }
                  else {
                      $scope.common.tblRequirementList = new ngTableParams({});
                  }
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
              });
        }
        $timeout(function () {
            $scope.populateFilterLookupList();
        }, 2000);

        blockUI.stop();
    };

    function populateRequirementListByStatus(organizationId, userId, jobList, totalCount, totalPages, pageFlag, requirementStatus) {
        //if (angular.isDefined($scope.common.tblRequirementList)) {
        //    $scope.common.tblRequirementList.reload();
        //}
        //else
        {
            $scope.common.tblRequirementList = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: $scope.common.ngTablePaginationCount,
                total: totalCount,
                getData: function ($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    /* sorting */
                    if (pageFlag != 1) {
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function (val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */

                        var promise = XSeedApiFactory.getRequirementListByStatus(organizationId, userId, requirementStatus, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder);
                        promise.then(
                          function (response) {
                              $scope.common.jobList = response[0].data.Jobs;
                              $scope.common.totalCount = response[0].data.TotalCount;
                              $scope.common.totalPages = response[0].data.TotalPages;

                              //to select first row by default
                              if ($scope.common.requirementModel.manipulationMode != "Edit") {
                                  $scope.setClickedRow($scope.common.jobList[0]);
                              }
                              else {
                                  //refresh activity
                                  if ($scope.common.selectedJob) {
                                      var IsRequirement = IsRequirementInList($scope.common.jobList);
                                      if (IsRequirement) {
                                          $scope.getActivityList();
                                      }
                                      else {
                                          $scope.setClickedRow($scope.common.jobList[0]);
                                      }
                                  }
                                  $scope.common.requirementModel.manipulationMode = undefined;
                              }
                              //Manupulation of response
                              angular.forEach($scope.common.jobList, function (val, index) {
                                  if (val.Id == $scope.common.selectedJob.Id) {
                                      $scope.common.selectedJob = val;
                                  }
                                  if ($scope.common.jobList[index].TechnicalSkills != null) {
                                      $scope.common.jobList[index].TechnicalSkills = $scope.common.jobList[index].TechnicalSkills.split("|");
                                  }
                              });

                              blockUI.stop();

                          },
                          function (httpError) {
                              blockUI.stop();
                              ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                          });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.common.jobList);
                }
                , $scope: $scope
            });
        }
    };

    $scope.getRequirementList = function () {

        if ($rootScope.userHasToken == false) {
            $location.path('login');
            return false;
        }

        var createRquirementURL = '/createRequirement';
        var editRequirementURL = '/editRequirement';
        var requirementListURL = '/requirementList';
        var requirementDetailURL = '/requirementDetail';

        var dashboardURL = '/dashboard';

        if (($location.path().toLowerCase() == createRquirementURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Requirement_Create == "False") {
            $location.path('dashboard');
            return false;
        }
        if (($location.path().toLowerCase() == editRequirementURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Requirement_Update == "False") {
            $location.path('dashboard');
            return false;
        }

        if (($location.path().toLowerCase() == requirementListURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Requirement_Read == "False") {
            $location.path('dashboard');
            return false;
        }
        if (($location.path().toLowerCase() == requirementDetailURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Requirement_Read == "False") {
            $location.path('dashboard');
            return false;
        }

        if ($scope.flag.advanceSearchFlag) {
            return;
        }
        blockUI.start();
        $scope.jobLookupProcess();
        var firstPageFlag = 0;
        var organizationId = $rootScope.userDetails.OrganizationId;
        var userId = $rootScope.userDetails.UserId;
        $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
        $scope.common.pageNumberShow = undefined;

        if ($scope.common.requirementModel.manipulationMode != "Edit") {
            var promise = XSeedApiFactory.getRequirementList(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow);
            promise.then(
              function (response) {
                  $scope.common.jobList = response[0].data.Jobs;
                  $scope.common.totalCount = response[0].data.TotalCount;
                  $scope.common.totalPages = response[0].data.TotalPages;

                  if ($scope.common.jobList.length) {
                      $scope.firstRequisitionId = $scope.common.jobList[0].RequisitionId;

                      //Manupulation of response
                      angular.forEach($scope.common.jobList, function (val, index) {
                          if ($scope.common.jobList[index].TechnicalSkills != null) {
                              $scope.common.jobList[index].TechnicalSkills = $scope.common.jobList[index].TechnicalSkills.split("|");
                              $scope.common.jobList[index].TechnicalSkills = $scope.common.jobList[index].TechnicalSkills.join();
                          }
                      });

                      //to select first row by default
                      $scope.setClickedRow($scope.common.jobList[0]);

                      firstPageFlag = 1;
                      populateRequirementList(organizationId, $scope.common.jobList, $scope.common.totalCount, $scope.common.totalPages, firstPageFlag);

                      //to show pending requirement count in badge on requirement list
                      if ($rootScope.userDetails.RoleName == 'Application Administrator' || $rootScope.userDetails.RoleName == 'Delivery Manager' || $rootScope.userDetails.RoleName == 'Team Lead') {
                          if ($rootScope.userDetails.Vendor_Read == 'True') {
                              getVendorUnreadMailCount();
                          }
                      }
                  }
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
              });
        }
        $timeout(function () {
            $scope.populateFilterLookupList();
        }, 2000);

        blockUI.stop();
    };

    function populateRequirementList(organizationId, jobList, totalCount, totalPages, pageFlag) {
        //if (angular.isDefined($scope.common.tblRequirementList)) {
        //    $scope.common.tblRequirementList.reload();
        //}
        //else
        {
            $scope.common.tblRequirementList = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: $scope.common.ngTablePaginationCount,
                total: totalCount,
                getData: function ($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    /* sorting */
                    if (pageFlag != 1) {
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function (val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */

                        var promise = XSeedApiFactory.getRequirementList(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder);
                        promise.then(
                          function (response) {
                              $scope.common.jobList = response[0].data.Jobs;
                              $scope.common.totalCount = response[0].data.TotalCount;
                              $scope.common.totalPages = response[0].data.TotalPages;

                              //to select first row by default
                              if ($scope.common.requirementModel.manipulationMode != "Edit") {
                                  $scope.setClickedRow($scope.common.jobList[0]);
                              }
                              else {
                                  //refresh activity
                                  if ($scope.common.selectedJob) {
                                      var IsRequirement = IsRequirementInList($scope.common.jobList);
                                      if (IsRequirement) {
                                          $scope.getActivityList();
                                      }
                                      else {
                                          $scope.setClickedRow($scope.common.jobList[0]);
                                      }
                                  }
                                  $scope.common.requirementModel.manipulationMode = undefined;
                              }
                              //Manupulation of response
                              angular.forEach($scope.common.jobList, function (val, index) {
                                  if (val.Id == $scope.common.selectedJob.Id) {
                                      $scope.common.selectedJob = val;
                                  }
                                  if ($scope.common.jobList[index].TechnicalSkills != null) {
                                      $scope.common.jobList[index].TechnicalSkills = $scope.common.jobList[index].TechnicalSkills.split("|");
                                  }
                              });

                              blockUI.stop();

                          },
                          function (httpError) {
                              blockUI.stop();
                              ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                          });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.common.jobList);
                }
                , $scope: $scope
            });
        }
    };

    $scope.populateFilterLookupList = function () {
        $scope.requirementFilterLookup = [];
        var organizationUserArray = [];
        var self = angular.copy(_.find($scope.lookup.organizationUser, function (o) { return o.Id == $rootScope.userDetails.Id; }));
        if (self) {
            self.Name = "My";
            organizationUserArray.push(self);
        }

        angular.forEach($scope.lookup.organizationUser, function (val, index) {
            if ($scope.lookup.organizationUser[index].Id != $rootScope.userDetails.Id) {
                organizationUserArray.push($scope.lookup.organizationUser[index]);
            }
        });

        $scope.requirementFilterLookup.push({ "Users": organizationUserArray });
        $scope.requirementFilterLookup.push({ "Status": $scope.lookup.jobStatus });
        $scope.requirementFilterLookup.push({ "Type": $scope.lookup.jobType });
        $scope.requirementFilterLookup.push({ "Company": $scope.lookup.company });
        //to clear All Filter Items On Reload
        clearAllFilterItemsOnReload();

        //var requirementTypeLookup = [];
        ////requirementTypeLookup.push({ "Id": "All", "Name": "All Requirements" });
        //requirementTypeLookup.push({ "Id": "All", "Name": "My Requirements" });
        //requirementTypeLookup.push({ "Id": "Open", "Name": "My Open" });
        //requirementTypeLookup.push({ "Id": "Closed", "Name": "My Closed" });
        //requirementTypeLookup.push({ "Id": "Posted", "Name": "My Posted" });
        //requirementTypeLookup.push({ "Id": "Pending", "Name": "My Pending" });
        //requirementTypeLookup.push({ "Id": "Hold", "Name": "My Hold" });

        //$scope.requirementFilterLookup.push({ "Requirement Type": requirementTypeLookup });
    }

    $scope.getSelectedFilterItem = function () {
        var Status = [];
        var Type = [];
        var Company = [];
        var OrganizationUser = [];
        //var RequirementType = '';
        angular.forEach($scope.requirementFilterLookup, function (val1, index1) {
            angular.forEach(val1, function (val2, index2) {
                angular.forEach(val2, function (val3, index3) {
                    if (index1 == 0 && val3.selected == true)
                        OrganizationUser.push(val3.Id);
                    else if (index1 == 1 && val3.selected == true)
                        Status.push(val3.Id);
                    else if (index1 == 2 && val3.selected == true)
                        Type.push(val3.Id);
                    else if (index1 == 3 && val3.selected == true)
                        Company.push(val3.Id);
                    //else if (index1 == 4 && val3.selected == true)
                    //    RequirementType = val3.Id;
                });
            });
        });

        var filterSelection = {};
        filterSelection.Status = Status;
        filterSelection.Type = Type;
        filterSelection.Company = Company;
        filterSelection.OrganizationUser = OrganizationUser;
        //filterSelection.RequirementType = RequirementType;
        filterSelection.OrganizationId = $rootScope.userDetails.OrganizationId;
        filterSelection.OrganizationUserId = $rootScope.userDetails.UserId;

        $scope.common.advanceSearchModel.filter = filterSelection;

        $scope.getRequirementFilterResult();
    }

    $scope.addFilterItemButton = function (type, id, name, selected) {
        if (selected == true) {
            $scope.filterItemLable.push({ Id: id, Name: name });
            $scope.filterItemCount = $scope.filterItemCount + 1;
        }
        else {
            $scope.removeFilterItemButton(id);
        }
    };

    $scope.removeFilterItemButton = function (id) {
        angular.forEach($scope.filterItemLable, function (val, index) {
            if (val && val.Id == id) {
                $scope.filterItemLable[index] = undefined;
            }
        });

        removeItemFromFilterList(id);
        $scope.filterItemCount = $scope.filterItemCount - 1;
    };

    function removeItemFromFilterList(id) {
        angular.forEach($scope.requirementFilterLookup, function (val1, index1) {
            angular.forEach(val1, function (val2, index2) {
                angular.forEach(val2, function (val3, index3) {
                    if (val3.Id == id) {
                        val3.selected = false;
                    }
                });
            });
        });
    };

    function clearAllFilterItemsOnReload() {
        angular.forEach($scope.requirementFilterLookup, function (val1, index1) {
            angular.forEach(val1, function (val2, index2) {
                angular.forEach(val2, function (val3, index3) {
                    if (val3.selected) {
                        val3.selected = false;
                    }
                });
            });
        });
    }

    function clearFilter() {

        angular.forEach($scope.requirementFilterLookup, function (val1, index1) {
            angular.forEach(val1, function (val2, index2) {
                angular.forEach(val2, function (val3, index3) {
                    if (val3.selected == true) {
                        $scope.removeFilterItemButton(val3.Id);
                    }
                });
            });
        });
    };


    /* Job module start */
    $scope.common.jobPageLookupList = function () {
        var promise = XSeedApiFactory.jobPagePrimaryLookupCall($rootScope.userDetails.OrganizationId);
        promise.then(
          function (response) {
              $scope.lookup.jobTitle = response[0].data;
              $scope.lookup.jobType = response[1].data;
              $scope.lookup.jobStatus = response[2].data;
              $scope.lookup.degree = response[3].data;
              $scope.lookup.visaType = response[4].data;

              //to autocomplete jobTitle
              buildJobTitleStructuredData();

              $scope.flag.requirementLookupLoaded = true;
          },
          function (httpError) {
              ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
          });
    };

    function buildJobTitleStructuredData() {
        $scope.jobTitles = [];
        angular.forEach($scope.lookup.jobTitle, function (val, index) {
            $scope.jobTitles.push(val.Name)
        });
    };

    function jobPageSecondaryLookupList() {
        var organizationId = $rootScope.userDetails.OrganizationId;
        var promise = XSeedApiFactory.jobPageSecondaryLookupCall(organizationId);
        promise.then(
          function (response) {
              $scope.lookup.company = response[0].data;
              $scope.lookup.organizationUser = response[1].data;
          },
          function (httpError) {
              ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
          });
    };

    $scope.jobLookupProcess = function () {
        if ($scope.flag.requirementLookupLoaded == false) {
            $scope.common.jobPageLookupList();
        }

        jobPageSecondaryLookupList();
    }

    $scope.createJobProcess = function (createJobForm) {
        if (new ValidationService().checkFormValidity(createJobForm)) {
            blockUI.start();
            var list = [];
            $scope.createJobFormSubmitted = false;
            $scope.common.requirementModel.OrganizationId = $rootScope.userDetails.OrganizationId;
            $scope.common.requirementModel.OrganizationUserId = $rootScope.userDetails.UserId;

            if (angular.isDefined($scope.common.requirementModel.OrganizationUserIdList)) {
                for (var i = 0; i < $scope.common.requirementModel.OrganizationUserIdList.length; i++) {
                    var o = {};
                    o.Id = $scope.common.requirementModel.OrganizationUserIdList[i];
                    o.Name = '';
                    list.push(o);
                }

                $scope.common.requirementModel.OrganizationUser = list;
            }
            list = [];
            if (angular.isDefined($scope.common.requirementModel.DegreeList)) {
                for (var i = 0; i < $scope.common.requirementModel.DegreeList.length; i++) {
                    var o = _.find($scope.lookup.degree, function (o) { return o.Id == $scope.common.requirementModel.DegreeList[i]; });
                    list.push(o);
                }
                $scope.common.requirementModel.DegreeList = list;
            }

            var visa = [];
            if (angular.isDefined($scope.common.requirementModel.VisaTypeList)) {
                for (var i = 0; i < $scope.common.requirementModel.VisaTypeList.length; i++) {
                    var o = _.find($scope.lookup.visaType, function (o) { return o.Id == $scope.common.requirementModel.VisaTypeList[i]; });
                    visa.push(o);
                }
                $scope.common.requirementModel.VisaTypeList = visa;
            }

            if ($scope.common.requirementModel.SkillList.length > 0) {
                $scope.common.requirementModel.TechnicalSkills = "";
                for (var i = 0; i < ($scope.common.requirementModel.SkillList.length - 1) ; i++) {
                    $scope.common.requirementModel.TechnicalSkills = $scope.common.requirementModel.TechnicalSkills + $scope.common.requirementModel.SkillList[i].toString() + "|";
                }
                $scope.common.requirementModel.TechnicalSkills = $scope.common.requirementModel.TechnicalSkills + $scope.common.requirementModel.SkillList[$scope.common.requirementModel.SkillList.length - 1].toString();
            }

            $scope.common.requirementModel.DriveFromDate = $filter('date')($scope.common.requirementModel.DriveFromDate, "MM/dd/yyyy");
            $scope.common.requirementModel.DriveToDate = $filter('date')($scope.common.requirementModel.DriveToDate, "MM/dd/yyyy");
            $scope.common.requirementModel.JobPostedDate = $filter('date')($scope.common.requirementModel.JobPostedDate, "MM/dd/yyyy");
            $scope.common.requirementModel.JobClosedDate = $filter('date')($scope.common.requirementModel.JobClosedDate, "MM/dd/yyyy");
            $scope.common.requirementModel.JobExpiryDate = $filter('date')($scope.common.requirementModel.JobExpiryDate, "MM/dd/yyyy");
            $scope.common.requirementModel.HtmlText = null;

            var promise = XSeedApiFactory.createJob($scope.common.requirementModel);
            promise.then(
              function (response) {

                  //to submit QNote
                  if (response[0].data != null) {
                      $scope.common.selectedJob = {};
                      $scope.common.selectedJob.Id = response[0].data;
                      if ($scope.common.selectedJob.Id) {
                          $scope.qNoteModel.qNote = $scope.common.requirementModel.qNote;
                          $scope.submitQNote();
                      }
                  }
                  if ($scope.common.requirementModel.emailParserId) {
                      var promise = XSeedApiFactory.acceptRequirement($scope.common.requirementModel.emailParserId, response[0].data, $rootScope.userDetails.UserId, true);
                      promise.then(
                        function (response) {
                            blockUI.stop();
                        },
                        function (httpError) {
                            blockUI.stop();
                            ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
                        });
                  }

                  blockUI.stop();
                  XSeedAlert.swal({
                      title: 'Success!',
                      text: 'Requirement created successfully!',
                      type: "success",
                      customClass: "xseed-error-alert",
                      allowOutsideClick: false
                  }).then(function () {
                      $scope.common.requirementModel = {};
                      $timeout(function () {
                          //to show newly created requirement selected
                          $scope.common.tblRequirementList = undefined;
                          $location.path('requirementList');
                      }, 0);
                  }, function (dismiss) {
                      blockUI.stop();
                  });
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
              });
        };
    };

    $scope.requirementDirectEditView = function () {
        blockUI.start();
        $scope.getRequirementDetails();
        $scope.common.requirementModel.manipulationMode = "Edit";
        $scope.common.redirectedFrom = 'RequirementList';
        blockUI.stop();
        $location.path('editRequirement');
    };

    $scope.editJobProcess = function (createJobForm) {
        if (new ValidationService().checkFormValidity(createJobForm)) {
            blockUI.start();
            $scope.common.requirementModel.OrganizationId = $rootScope.userDetails.OrganizationId;

            var list = [];
            if (angular.isDefined($scope.common.requirementModel.OrganizationUserIdList)) {
                for (var i = 0; i < $scope.common.requirementModel.OrganizationUserIdList.length; i++) {
                    var o = {};
                    o.Id = $scope.common.requirementModel.OrganizationUserIdList[i];
                    o.Name = '';
                    list.push(o);
                }

                $scope.common.requirementModel.OrganizationUser = list;
            }
            list = [];
            if (angular.isDefined($scope.common.requirementModel.DegreeList)) {
                for (var i = 0; i < $scope.common.requirementModel.DegreeList.length; i++) {
                    var o = _.find($scope.lookup.degree, function (o) { return o.Id == $scope.common.requirementModel.DegreeList[i]; });
                    list.push(o);
                }
                $scope.common.requirementModel.DegreeList = list;
            }

            var visa = [];
            if (angular.isDefined($scope.common.requirementModel.VisaTypeList)) {
                for (var i = 0; i < $scope.common.requirementModel.VisaTypeList.length; i++) {
                    var o = _.find($scope.lookup.visaType, function (o) { return o.Id == $scope.common.requirementModel.VisaTypeList[i]; });
                    visa.push(o);
                }
                $scope.common.requirementModel.VisaTypeList = visa;
            }


            if ($scope.common.requirementModel.SkillList.length > 0) {
                $scope.common.requirementModel.TechnicalSkills = "";
                for (var i = 0; i < ($scope.common.requirementModel.SkillList.length - 1) ; i++) {
                    $scope.common.requirementModel.TechnicalSkills = $scope.common.requirementModel.TechnicalSkills + $scope.common.requirementModel.SkillList[i].toString() + "|";
                }
                $scope.common.requirementModel.TechnicalSkills = $scope.common.requirementModel.TechnicalSkills + $scope.common.requirementModel.SkillList[$scope.common.requirementModel.SkillList.length - 1].toString();
            }

            $scope.common.requirementModel.DriveFromDate = $filter('date')($scope.common.requirementModel.DriveFromDate, "MM/dd/yyyy");
            $scope.common.requirementModel.DriveToDate = $filter('date')($scope.common.requirementModel.DriveToDate, "MM/dd/yyyy");
            $scope.common.requirementModel.JobPostedDate = $filter('date')($scope.common.requirementModel.JobPostedDate, "MM/dd/yyyy");
            $scope.common.requirementModel.JobClosedDate = $filter('date')($scope.common.requirementModel.JobClosedDate, "MM/dd/yyyy");
            $scope.common.requirementModel.JobExpiryDate = $filter('date')($scope.common.requirementModel.JobExpiryDate, "MM/dd/yyyy");

            var promise = XSeedApiFactory.editJob($scope.common.requirementModel);
            promise.then(
              function (response) {
                  //to submit QNote
                  if ($scope.common.selectedJob.Id) {
                      $scope.qNoteModel.qNote = $scope.common.requirementModel.qNote;
                      $scope.submitQNote();
                  }

                  blockUI.stop();
                  XSeedAlert.swal({
                      title: 'Success!',
                      text: 'Requirement details updated successfully!',
                      type: "success",
                      customClass: "xseed-error-alert",
                      allowOutsideClick: false
                  }).then(function () {
                      $timeout(function () {
                          $scope.common.requirementModel = {};
                          $scope.resetRequirementAdvanceSearch();
                      }, 100);
                      $location.path('requirementList');
                  }, function (dismiss) {
                      blockUI.stop();

                  });
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
              });
        };
    };

    $scope.cancelRequirement = function () {
        XSeedAlert.swal({
            title: "Are you sure you want to cancel?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function () {
            blockUI.start();
            if ($scope.common.redirectedFrom == 'RequirementList') {
                var editRquirementURL = '/editRequirement';
                if ($location.path().toLowerCase() == editRquirementURL.toLowerCase()) {
                    $scope.common.requirementModel.manipulationMode = "Edit";
                }
                $timeout(function () {
                    $scope.resetRequirementAdvanceSearch();
                }, 100);

                $location.path('requirementList');
            }
            else {
                $location.path('requirementQueue');
            }
            blockUI.stop();
        }, function (dismiss) { });
    };

    $scope.goToRequirementList = function () {
        if ($scope.common.redirectedFrom == 'RequirementList') {
            $timeout(function () {
                $scope.resetRequirementAdvanceSearch();
            }, 100);
            $location.path('requirementList');
        }
        else
            $location.path('requirementQueue');
    };

    //Advance Search
    $scope.resetRequirementAdvanceSearch = function () {
        $scope.common.advanceSearchModel = {};
        $scope.flag.advanceSearchFlag = false;
        $scope.common.tblRequirementList = undefined;
        $scope.common.requirementStatusFilter = "MyOpen";
        $scope.getJobsByStatus('MyOpen');
        clearFilter();
    };

    /**************************submit Candidate From Requirement**************************/

    //Checkbox code-
    $scope.checkboxes = { 'checked': false, items: {} };

    // watch for check all checkbox
    $scope.$watch('checkboxes.checked', function (value) {
        angular.forEach($scope.common.candidateList, function (item) {
            if (angular.isDefined(item._id)) {
                $scope.checkboxes.items[item._id] = value;
            }
        });
    });

    // watch for data checkboxes
    $scope.$watch('checkboxes.items', function (values) {
        if (!$scope.common.candidateList) {
            return;
        }
        var checked = 0, unchecked = 0,
            total = $scope.common.candidateList.length;
        angular.forEach($scope.common.candidateList, function (item) {
            checked += ($scope.checkboxes.items[item._id]) || 0;
            unchecked += (!$scope.checkboxes.items[item._id]) || 0;
        });
        if ((unchecked == 0) || (checked == 0)) {
            $scope.checkboxes.checked = (checked == total);
        }

        $scope.checkedCount = checked;
        //alert(checked);
        //alert(unchecked);
        //grayed checkbox
        angular.element(document.getElementById("select_all")).prop("indeterminate", (checked != 0 && unchecked != 0));
    }
    , true
    );

    $scope.openCandidateSearchModal = function () {
        blockUI.start();
        $scope.getCandidateList();
        submitCandidateFromRequirementModal.$promise.then(submitCandidateFromRequirementModal.show);
        blockUI.stop();
    };

    $scope.closeCandidateSearchModal = function () {
        submitCandidateFromRequirementModal.$promise.then(submitCandidateFromRequirementModal.hide);
        $scope.checkboxes.items = {};
        $scope.common.advanceSearchModel = {};
        $scope.candidateAdvanceSearchFlag = false;
        $scope.common.tblCandidateList = undefined;
    };

    $scope.getCandidateList = function () {
        blockUI.start();
        var firstPageFlag = 0;
        $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
        $scope.common.pageNumberShow = undefined;

        var promise = XSeedApiFactory.getCandidateList($scope.common.pageSizeShow, $scope.common.pageNumberShow);
        promise.then(
          function (response) {
              $scope.common.candidateList = response[0].data.Candidates;
              $scope.common.totalCount = response[0].data.TotalCount;
              $scope.common.totalPages = response[0].data.TotalPages;

              //to show skills in tooltip as string
              angular.forEach($scope.common.candidateList, function (val, index) {
                  if (val.Skills) {
                      val.Skills = val.Skills.join();
                  }
              });

              firstPageFlag = 1;
              if ($scope.common.candidateList.length) {
                  populateCandidateList($scope.common.candidateList, $scope.common.totalCount, $scope.common.totalPages, firstPageFlag);
              }
              blockUI.stop();
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Candidate', httpError.data.Status, httpError.data.Message);
          });
    };

    function populateCandidateList(candidateList, totalCount, totalPages, pageFlag) {
        if (angular.isDefined($scope.common.tblCandidateList)) {
            $scope.common.tblCandidateList.reload();
        }
        else {
            $scope.common.tblCandidateList = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage,
                sorting: {}
            }, {
                counts: $scope.common.ngTablePaginationCount,
                total: totalCount,
                getData: function ($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    if (pageFlag != 1) {
                        /* sorting */
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function (val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */

                        var promise = XSeedApiFactory.getCandidateList($scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder);
                        promise.then(
                          function (response) {
                              $scope.common.candidateList = response[0].data.Candidates;
                              $scope.common.totalCount = response[0].data.TotalCount;
                              $scope.common.totalPages = response[0].data.TotalPages;

                              //to show skills in tooltip as string
                              angular.forEach($scope.common.candidateList, function (val, index) {
                                  if (val.Skills) {
                                      val.Skills = val.Skills.join();
                                  }
                              });
                              blockUI.stop();

                          },
                          function (httpError) {
                              blockUI.stop();
                              ExceptionHandler.handlerHTTPException('Candidate', httpError.data.Status, httpError.data.Message);
                          });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.common.candidateList);
                }
                , $scope: $scope
            });
        }

    };

    $scope.resetCandidateAdvanceSearch = function () {
        $scope.common.advanceSearchModel = {};
        $scope.candidateAdvanceSearchFlag = false;
        $scope.common.tblCandidateList = undefined;
        $scope.getCandidateList();
    };

    $scope.openCandidateSubmissionModal = function (type, candidate) {
        $scope.candidateSubmissionList = [];

        $scope.candidate = candidate;

        if (type == 'Single') {
            $scope.candidateSubmissionList.push(candidate);
        }
        else {
            angular.forEach($scope.checkboxes.items, function (item, Identifier) {
                if (item == true) {
                    var selectedCandidate = _.find($scope.common.candidateList, function (o) { return o._id == Identifier; });
                    if (selectedCandidate) {
                        $scope.candidateSubmissionList.push(selectedCandidate);
                    }
                }
            });
        }


        if ($scope.candidateSubmissionList.length == 0) {
            ExceptionHandler.handlerPopup('Candidate Submission', 'Please select candidate from displayed page.', '', 'info');
        }
        else {
            var organizationId = $rootScope.userDetails.OrganizationId;
            var promise = XSeedApiFactory.submissionPopupLookupCall(organizationId);
            promise.then(
              function (response) {
                  $scope.lookup.company = response[0].data;
                  $scope.CandidateSubmissionModel = {};
                  if ($scope.common.selectedJob) {
                      $scope.CandidateSubmissionModel.CompanyId = $scope.common.selectedJob.CompanyId;
                      if ($scope.CandidateSubmissionModel.CompanyId) {
                          $scope.getRequirementListByCompany($scope.CandidateSubmissionModel.CompanyId);
                      }
                      $scope.CandidateSubmissionModel.JobId = $scope.common.selectedJob.Id;
                  }
              },
              function (httpError) {
                  //blockUI.stop();
                  ExceptionHandler.handlerHTTPException('LookUp', httpError.data.Status, httpError.data.Message);
              });

            candidateSubmissionModal.$promise.then(candidateSubmissionModal.show);
        }
    }

    $scope.submitPopupCancel = function () {
        candidateSubmissionModal.$promise.then(candidateSubmissionModal.hide);
        $scope.checkboxes.items = {};
    }

    $scope.getRequirementListByCompany = function (companyId) {
        var promise = XSeedApiFactory.getRequirementListByCompany(companyId);
        promise.then(
          function (response) {
              $scope.lookup.requirement = response[0].data;
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
          });
    };

    $scope.submitCandidateProcess = function (candidateSubmissionPopupForm) {
        if (new ValidationService().checkFormValidity(candidateSubmissionPopupForm)) {
            blockUI.start();

            var list = [];
            for (var i = 0; i < $scope.candidateSubmissionList.length; i++) {
                var object = {};
                object.Id = $scope.candidateSubmissionList[i]._id;
                object.Name = '';

                list.push(object);
            }

            $scope.CandidateSubmissionModel.CandidateList = list;

            $scope.CandidateSubmissionModel.OrganizationUserId = $rootScope.userDetails.UserId;
            var promise = XSeedApiFactory.submitCandidate($scope.CandidateSubmissionModel);
            promise.then(
              function (response) {
                  blockUI.stop();
                  $scope.submitPopupCancel();
                  $scope.dataSubmissiomEmail.successCandidates = response[0].data.successCandidates;
                  $scope.dataSubmissiomEmail.companyId = $scope.CandidateSubmissionModel.CompanyId;
                  $scope.dataSubmissiomEmail.jobId = $scope.CandidateSubmissionModel.JobId;
                  $scope.CandidateSubmissionModel.CompanyId = "";
                  $scope.CandidateSubmissionModel.JobId = "";
                  if ($scope.dataSubmissiomEmail.successCandidates.length > 0) {
                      XSeedAlert.swal({
                          title: 'Success!',
                          text: 'Candidate(s) submitted!',
                          type: "success",
                          customClass: "xseed-error-alert"
                      }).then(function () {
                          $scope.getSubmissionUrl($scope.dataSubmissiomEmail.companyId);
                      }, function (dismiss) {
                          blockUI.stop();
                      })
                  }
                  else {
                      blockUI.stop();
                      ExceptionHandler.handlerPopup('Submission', response[0].data.message, 'Info!', 'info');
                  };
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Submission', httpError.data.Status, httpError.data.Message);
              });
        };
    };

    $scope.getAdvanceSearchedCandidateList = function () {
        blockUI.start();
        $scope.candidateAdvanceSearchFlag = true;
        $scope.common.advanceSearchModel.OrganizationId = $rootScope.userDetails.OrganizationId;
        $scope.common.advanceSearchModel.SearchIn = ["CandidateId", "Name", "Email", "Contact", "CreatedBy"];
        $scope.common.pageSizeShow = $scope.common.ngTablePaginationPage;
        $scope.common.pageNumberShow = undefined;
        var firstPageFlagSearch = 0;

        var promise = XSeedApiFactory.getCandidateAdvanceSearchResult($scope.common.advanceSearchModel, $scope.common.pageSizeShow, $scope.common.pageNumberShow);
        promise.then(
        function (response) {
            $scope.common.candidateList = response[0].data.Candidates;
            $scope.common.totalCount = response[0].data.TotalCount;
            $scope.common.totalPages = response[0].data.TotalPages;

            firstPageFlagSearch = 1;

            populateAdvanceSearchCandidateList($scope.common.candidateList, $scope.common.totalCount, $scope.common.totalPages, firstPageFlagSearch);

            //to show skills in tooltip as string
            angular.forEach($scope.common.candidateList, function (val, index) {
                if (val.Skills) {
                    val.Skills = val.Skills.join();
                }
            });
            blockUI.stop();
        },
        function (httpError) {
            $scope.closeAdvanceSearchModal();
            blockUI.stop();
            ExceptionHandler.handlerHTTPException('Search', httpError.data.Status, httpError.data.Message);
        });
    }

    function populateAdvanceSearchCandidateList(candidateList, totalCount, totalPages, pageFlag) {
        //if (angular.isDefined($scope.common.tblCandidateList)) {
        //    $scope.common.tblCandidateList.reload();
        //}
        //else 
        {
            $scope.common.tblCandidateList = new ngTableParams({
                page: 1,
                count: $scope.common.ngTablePaginationPage
                , sorting: {}
            }, {
                counts: $scope.common.ngTablePaginationCount,
                total: totalCount,
                getData: function ($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    if (pageFlag != 1) {
                        /* sorting */
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function (val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */

                        var promise = XSeedApiFactory.getCandidateAdvanceSearchResult($scope.common.advanceSearchModel, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder);
                        promise.then(
                          function (response) {
                              $scope.common.candidateList = response[0].data.Candidates;
                              $scope.common.totalCount = response[0].data.TotalCount;
                              $scope.common.totalPages = response[0].data.TotalPages;

                              //to show skills in tooltip as string
                              angular.forEach($scope.common.candidateList, function (val, index) {
                                  if (val.Skills) {
                                      val.Skills = val.Skills.join();
                                  }
                              });
                              blockUI.stop();
                          },
                          function (httpError) {
                              blockUI.stop();
                              ExceptionHandler.handlerHTTPException('Search', httpError.data.Status, httpError.data.Message);
                          });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.common.candidateList);
                }
                , $scope: $scope
            });
        }
    }

    /* Requirement Parser start */
    //$scope.getRequirementQueueListCount = function () {
    //    blockUI.start();

    //    var organizationUserId = $rootScope.userDetails.UserId;
    //    var promise = XSeedApiFactory.getRequirementQueueListCount(organizationUserId);
    //    promise.then(
    //      function (response) {
    //          $scope.requirementQueueCount = response[0].data;
    //          blockUI.stop();
    //      },
    //      function (httpError) {
    //          blockUI.stop();
    //          ExceptionHandler.handlerHTTPException('Parser', httpError.data.Status, httpError.data.Message);
    //      });
    //};

    function getVendorUnreadMailCount() {
        if ($rootScope.userDetails.Vendor_Read == 'True') {
            var promise = XSeedApiFactory.getUnreadMailCount($rootScope.userDetails.OrganizationId);
            promise.then(
              function (response) {
                  blockUI.stop();
                  $scope.requirementQueueCount = response[0].data;
                  //console.log($scope.requirementQueueCount);
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Vendor', httpError.data.Status, httpError.data.Message);
              });
        }
    };

    $scope.getRequirementQueueList = function () {
        blockUI.start();

        var organizationId = $rootScope.userDetails.OrganizationId;
        var userId = $rootScope.userDetails.UserId;
        $scope.common.pageSizeShow = undefined;
        $scope.common.pageNumberShow = undefined;

        var promise = XSeedApiFactory.getRequirementQueueDataList(userId, $scope.common.pageSizeShow, $scope.common.pageNumberShow);
        promise.then(
          function (response) {
              $scope.requirementQueue = response[0].data.parsedJobs;
              if ($scope.requirementQueue) {
                  angular.forEach($scope.requirementQueue, function (val, index) {
                      if (!$scope.isUndefinedOrNull($scope.requirementQueue[index].SkillList))
                          $scope.requirementQueue[index].SkillList = $scope.requirementQueue[index].SkillList.split("|");

                  });
              }
              var firstPageFlag = 0;
              $scope.requirementQueueTotalCount = response[0].data.TotalCount;
              $scope.requirementQueueTotalPages = response[0].data.TotalPages;
              firstPageFlag = 1;
              populateRequirementQueueList(userId, $scope.requirementQueue, $scope.requirementQueueTotalCount, $scope.requirementQueueTotalPages, firstPageFlag);
              blockUI.stop();
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Parser', httpError.data.Status, httpError.data.Message);
          });
    };

    function populateRequirementQueueList(organizationUserId, requirementQueue, totalCount, totalPages, pageFlag) {
        //if (angular.isDefined($scope.tblRequirementList)) {
        //    $scope.tblRequirementList.reload();
        //}
        //else 
        {
            $scope.tblRequirementQueueList = new ngTableParams({
                page: 1,
                count: 10,
                sorting: {}
            }, {
                counts: [],
                total: totalCount,
                getData: function ($defer, params) {
                    var sort, sortorder;

                    $scope.requirementQueuePageSizeShow = params.count();
                    $scope.requirementQueuePageNumberShow = params.page();
                    $scope.sortBy = params.sorting();

                    /* sorting */
                    if (pageFlag != 1) {
                        if ($scope.sortBy) {
                            angular.forEach($scope.sortBy, function (val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        /* sorting */

                        var promise = XSeedApiFactory.getRequirementQueueDataList(organizationUserId, $scope.requirementQueuePageSizeShow, $scope.requirementQueuePageNumberShow);
                        promise.then(
                          function (response) {
                              $scope.requirementQueue = response[0].data.parsedJobs;
                              $scope.requirementQueueTotalCount = response[0].data.TotalCount;
                              $scope.requirementQueueTotalPages = response[0].data.TotalPages;
                              if ($scope.requirementQueue) {
                                  angular.forEach($scope.requirementQueue, function (val, index) {
                                      if (!$scope.isUndefinedOrNull($scope.requirementQueue[index].SkillList))
                                          $scope.requirementQueue[index].SkillList = $scope.requirementQueue[index].SkillList.split("|");
                                  });
                              }

                              blockUI.stop();
                          },
                          function (httpError) {
                              blockUI.stop();
                              ExceptionHandler.handlerHTTPException('Parser', httpError.data.Status, httpError.data.Message);
                          });
                    }
                    pageFlag = 0;
                    params.total($scope.requirementQueueTotalCount);
                    $defer.resolve($scope.requirementQueue);
                }
                , $scope: $scope
            });
        }
    }

    $scope.getAcceptedRequirementList = function () {
        blockUI.start();

        var promise = XSeedApiFactory.getAcceptedRequirementList($rootScope.userDetails.UserId);
        promise.then(
          function (response) {
              $scope.acceptedRequirementList = response[0].data;
              blockUI.stop();
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
          });
    };


    $scope.acceptRequirement = function (id) {
        blockUI.start();
        var promise = XSeedApiFactory.acceptRequirement(id, $rootScope.userDetails.UserId, false);
        promise.then(
          function (response) {
              blockUI.stop();
              $timeout(function () {
                  $scope.getRequirementQueueList();
                  $scope.getRequirementList();
              }, 0);
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
          });
    };


    $scope.rejectRequirement = function (id) {
        XSeedAlert.swal({
            title: "Are you sure?",
            text: "You want to reject this requirement!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function () {
            blockUI.start();
            var promise = XSeedApiFactory.rejectRequirement(id);
            promise.then(
              function (response) {
                  blockUI.stop();
                  $timeout(function () {
                      $scope.getRequirementQueueList();
                  }, 0);
                  $location.path('requirementQueue');
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
              });
        }, function (dismiss) { });
    };


    $scope.reviewRequirement = function (id) {
        var promise = XSeedApiFactory.getRequirementReview(id, $rootScope.userDetails.UserId);
        promise.then(
          function (response) {
              $scope.common.requirementModel = response[0].data;

              if (!$scope.isUndefinedOrNull($scope.common.requirementModel.CountryId) && !$scope.isEmpty($scope.common.requirementModel.CountryId)) {
                  $scope.getStateListByCountry($scope.common.requirementModel.CountryId);
              }

              if (!$scope.isUndefinedOrNull($scope.common.requirementModel.StateId) && !$scope.isEmpty($scope.common.requirementModel.StateId)) {
                  $scope.getCityListByState($scope.common.requirementModel.StateId);
              }

              if (!$scope.isUndefinedOrNull($scope.common.requirementModel.CompanyId) && !$scope.isEmpty($scope.common.requirementModel.CompanyId)) {
                  $scope.getCompanyContactsLookup($scope.common.requirementModel.CompanyId);
              }

              var list = [];
              for (var i = 0; i < $scope.common.requirementModel.OrganizationUser.length; i++) {
                  list.push($scope.common.requirementModel.OrganizationUser[i].Id);
              }
              $scope.common.requirementModel.OrganizationUserIdList = list;

              if ($scope.common.requirementModel.TechnicalSkills) {
                  $scope.common.requirementModel.SkillList = [];
                  $scope.common.requirementModel.SkillList = $scope.common.requirementModel.TechnicalSkills.split("|");
                  angular.forEach($scope.common.requirementModel.SkillList, function (val, index) {
                      $scope.lookup.skillListData.push({ value: val, label: val });
                  });
              }

              list = [];
              for (var i = 0; i < $scope.common.requirementModel.DegreeList.length; i++) {
                  list.push($scope.common.requirementModel.DegreeList[i].Id);
              }
              $scope.common.requirementModel.DegreeList = list;

              $scope.common.requirementModel.DriveFromDate = $filter('date')($scope.common.requirementModel.DriveFromDate, "MM/dd/yyyy");
              $scope.common.requirementModel.DriveToDate = $filter('date')($scope.common.requirementModel.DriveToDate, "MM/dd/yyyy");
              $scope.common.requirementModel.JobPostedDate = $filter('date')($scope.common.requirementModel.JobPostedDate, "MM/dd/yyyy");
              $scope.common.requirementModel.JobClosedDate = $filter('date')($scope.common.requirementModel.JobClosedDate, "MM/dd/yyyy");
              $scope.common.requirementModel.JobExpiryDate = $filter('date')($scope.common.requirementModel.JobExpiryDate, "MM/dd/yyyy");
              $scope.common.requirementModel.manipulationMode = "Create";
              $scope.common.requirementModel.reviewFlag = true;

              $scope.common.requirementModel.emailParserId = id;
              $scope.common.requirementModel.HtmlText = $scope.trustHtml($scope.common.requirementModel.HtmlText);
              $scope.getRequisitionID();

              $scope.jobLookupProcess();

              $scope.common.redirectedFrom = 'RequirementQueue';
              $location.path('createRequirement');
              blockUI.stop();
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
          });
    };

    /* Requirement Parser end */

    /* QNote start */
    $scope.submitQNote = function () {
        if ($scope.qNoteModel.qNote) {
            var promise = XSeedApiFactory.createQNote(encodeURIComponent($scope.qNoteModel.qNote), $scope.common.selectedJob.Id);
            promise.then(
              function (response) {
                  blockUI.stop();
                  $scope.qNoteModel.qNote = '';
                  $scope.getQNoteList();
              },
              function (httpError) {
                  blockUI.stop();
                  ExceptionHandler.handlerHTTPException('Job', httpError.data.Status, httpError.data.Message);
              });
        }
    };
    /* QNote end */

    /************PayScaleExtension**************/

    $scope.displaySalaryCalculator = function () {
        if ($scope.common.selectedJob) {
            if ($scope.common.selectedJob.City != "") {
                return PayScaleExtension.displaySalaryCalculator($scope.common.selectedJob.JobTitle, $scope.common.selectedJob.City, $scope.common.selectedJob.State, $scope.common.selectedJob.Country);
            }
            else {
                if ($scope.showPayScaleSalryCalculator) {
                    $scope.showPayScaleSalryCalculator = false;
                }
                else {
                    $scope.PayScaleSalryCalculator = {};
                    $scope.PayScaleSalryCalculator.JobTitle = $scope.common.selectedJob.JobTitle;
                    $scope.PayScaleSalryCalculator.Country = $scope.common.selectedJob.Country;
                    $scope.PayScaleSalryCalculator.State = $scope.common.selectedJob.State;
                    $scope.PayScaleSalryCalculator.City = $scope.common.selectedJob.City;
                    $scope.showPayScaleSalryCalculator = true;
                }
            }
        }
    };

    $scope.salaryCalculator = function (salaryCalculatorForm) {
        if (new ValidationService().checkFormValidity(salaryCalculatorForm)) {
            return PayScaleExtension.displaySalaryCalculator($scope.PayScaleSalryCalculator.JobTitle, $scope.PayScaleSalryCalculator.City, $scope.PayScaleSalryCalculator.State, $scope.PayScaleSalryCalculator.Country);
        }
    };

    $scope.hidePayScaleSalryCalculator = function () {
        if ($scope.showPayScaleSalryCalculator) {
            $scope.PayScaleSalryCalculator = {};
            $scope.showPayScaleSalryCalculator = false;
        }
    };

    /************PayScaleExtension**************/

    /************ For Candidate Search **************/
    $scope.redirectToBooleanSearch = function () {
        $location.path('searchCandidate');
    };
    /************ For Candidate Search **************/

    /************ To Check Is Requirement In List After Edit **************/
    function IsRequirementInList(jobList) {
        angular.forEach(jobList, function (val, index) {
            if (val.Id == $scope.common.selectedJob.Id) {
                //console.log(val.Id + " = " + $scope.common.selectedJob.Id);
                return true;
            }
        });
        return false;
    }
    /************ To Check Is Requirement In List After Edit **************/

    /* Requirement module end */



    //***************************************************************************************//

    // Get the render context local to this controller (and relevant params).
    var renderContext = RequestContext.getRenderContext("standard.requirement");
    // The subview indicates which view is going to be rendered on the page.
    $scope.subview = renderContext.getNextSection();
    $scope.$on(
        "requestContextChanged",
        function () {
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {
                return;
            }
            // Update the view that is being rendered.
            $scope.subview = renderContext.getNextSection();
        }
    );



}