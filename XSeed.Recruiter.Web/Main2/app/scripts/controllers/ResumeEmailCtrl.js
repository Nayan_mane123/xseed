﻿'use strict';

XSEED_APP.controller('ResumeEmailCtrl', ResumeEmailCtrl);

/**
 * @ngInject
 */
function ResumeEmailCtrl($scope, $rootScope, $route, $routeParams, RequestContext, $timeout, $q, $location, $window, configuration, i18nFactory, ExceptionHandler, EventBus, $log, _, ipCookie, ModalFactory, XSeedAlert, localStorageService, $uibModal, XSeedApiFactory, ngTableParams, $filter, ValidationService, $http, blockUI, blockUIConfig, Constants) {
    init();
    //Initialization Function
    function init() {
        if ($scope.isUndefinedOrNull($scope.resumeEmailList)) {
            $location.path('resumeEmailList');
        }
        $scope.flag.advanceSearchFlag = false;
        $scope.showEmailDetail = true;
    };


    /* Resume module start */

    $scope.getResumeEmailList = function () {
        if ($rootScope.userHasToken == false) {
            $location.path('login');
            return false;
        }

        var resumeEmailListURL = '/resumeEmailList';

        var dashboardURL = '/dashboard';

        if (($location.path().toLowerCase() == resumeEmailListURL.toLowerCase() || $location.path().toLowerCase() == dashboardURL.toLowerCase()) && $rootScope.userDetails.Vendor_Read == "False") {
            $location.path('dashboard');
            return false;
        }

        blockUI.start();
        var organizationId = $rootScope.userDetails.OrganizationId;
        var firstPageFlag = 0;
        $scope.common.pageSizeShow = undefined;
        $scope.common.pageNumberShow = undefined;

        var promise = XSeedApiFactory.getResumeEmailList(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow);
        promise.then(
          function (response) {
              $scope.resumeEmailList = response[0].data.ResumeEmail;
              $scope.common.totalCount = response[0].data.TotalCount;
              $scope.common.totalPages = response[0].data.TotalPages;
              if ($scope.resumeEmailList.length) {
                  //to select first row by default
                  $scope.showResumeEmailDetails($scope.resumeEmailList[0].Id);
              }
              firstPageFlag = 1;
              populateResumeEmailList(organizationId, $scope.resumeEmailList, $scope.common.totalCount, $scope.common.totalPages, firstPageFlag);

              blockUI.stop();
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('Resume', httpError.data.Status, httpError.data.Message);
          });
    };

    function populateResumeEmailList(organizationId, resumeEmailList, totalCount, totalPages, pageFlag) {
        if (angular.isDefined($scope.tblResumeEmailList)) {
            $scope.tblResumeEmailList.reload();
        }
        else {
            $scope.tblResumeEmailList = new ngTableParams({
                page: 1,
                count: 15,
                sorting: {}
            }, {
                counts: [15, 20, 25],
                total: totalCount,
                getData: function ($defer, params) {
                    var sort, sortorder;

                    $scope.common.pageSizeShow = params.count();
                    $scope.common.pageNumberShow = params.page();
                    $scope.common.sortBy = params.sorting();

                    /* sorting */
                    if (pageFlag != 1) {
                        if ($scope.common.sortBy) {
                            angular.forEach($scope.common.sortBy, function (val, index) {
                                sort = index;
                                sortorder = val;
                            });
                        }
                        ///* sorting */

                        var promise = XSeedApiFactory.getResumeEmailList(organizationId, $scope.common.pageSizeShow, $scope.common.pageNumberShow, sort, sortorder);
                        promise.then(
                          function (response) {
                              $scope.resumeEmailList = response[0].data.ResumeEmail;
                              $scope.common.totalCount = response[0].data.TotalCount;
                              $scope.common.totalPages = response[0].data.TotalPages;

                              //to select first row by default
                              $scope.showResumeEmailDetails($scope.resumeEmailList[0].Id);
                              blockUI.stop();
                          },
                          function (httpError) {
                              blockUI.stop();
                              ExceptionHandler.handlerHTTPException('Resume', httpError.data.Status, httpError.data.Message);
                          });

                    }
                    pageFlag = 0;
                    params.total($scope.common.totalCount);
                    $defer.resolve($scope.resumeEmailList);
                }
                , $scope: $scope
            });
        }
    };

    $scope.showResumeEmailDetails = function (resumeEmailId) {
        $scope.showEmailDetail = true;
        $scope.selectedResumeEmail = {};
        getResumeEmailDetails(resumeEmailId);
    };

    function getResumeEmailDetails(resumeEmailId) {
        var organizationId = $rootScope.userDetails.OrganizationId;
        blockUI.start();
        var promise = XSeedApiFactory.getResumeEmailList(organizationId, undefined, undefined, undefined, undefined, resumeEmailId);
        promise.then(
          function (response) {
              $scope.selectedResumeEmail = response[0].data;
              blockUI.stop();
              updateResumeEmailDetails($scope.selectedResumeEmail.IsRead);
          },
          function (httpError) {
              ExceptionHandler.handlerHTTPException('Resume', httpError.data.Status, httpError.data.Message);
          });
    };

    function updateResumeEmailDetails(IsRead) {
        if (!IsRead) {
            $scope.selectedResumeEmail.IsRead = true;
            var promise = XSeedApiFactory.updateResumeEmailDetails($scope.selectedResumeEmail.Id);
            promise.then(
              function (response) {
                  $scope.unreadMailCount = $scope.unreadMailCount - 1;
                  angular.forEach($scope.resumeEmailList, function (val, index) {
                      if (val.Id == $scope.selectedResumeEmail.Id) {
                          val.IsRead = $scope.selectedResumeEmail.IsRead;
                      }
                  })
              },
              function (httpError) {
                  ExceptionHandler.handlerHTTPException('Resume', httpError.data.Status, httpError.data.Message);
              });
        }
    };

    $scope.getUnreadMailCount = function () {
        var promise = XSeedApiFactory.getUnreadResumeMailCount($rootScope.userDetails.OrganizationId);
        promise.then(
          function (response) {
              blockUI.stop();
              $scope.unreadMailCount = response[0].data;
          },
          function (httpError) {
              blockUI.stop();
              ExceptionHandler.handlerHTTPException('ResumeEmail', httpError.data.Status, httpError.data.Message);
          });
    };

    $scope.showHideEmail = function () {
        $scope.showEmailDetail = false;
    };

    //***************************************************************************************//

    // Get the render context local to this controller (and relevant params).
    var renderContext = RequestContext.getRenderContext("standard.resumeEmail");
    // The subview indicates which view is going to be rendered on the page.
    $scope.subview = renderContext.getNextSection();
    $scope.$on(
        "requestContextChanged",
        function () {
            // Make sure this change is relevant to this controller.
            if (!renderContext.isChangeRelevant()) {
                return;
            }
            // Update the view that is being rendered.
            $scope.subview = renderContext.getNextSection();
        }
    );
};